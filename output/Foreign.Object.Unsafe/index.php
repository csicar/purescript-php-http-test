<?php
require_once(dirname(__FILE__) . '/../Foreign.Object/index.php');
$PS__Foreign_Object_Unsafe = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    return [
        'unsafeIndex' => $__foreign['unsafeIndex']
    ];
})();
