<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Array.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.String.CodeUnits/index.php');
require_once(dirname(__FILE__) . '/../Data.String.NonEmpty.Internal/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Pattern/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Partial.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
require_once(dirname(__FILE__) . '/../Unsafe.Coerce/index.php');
$PS__Data_String_NonEmpty_CodeUnits = (function ()  use (&$PS__Unsafe_Coerce, &$PS__Data_Semigroup, &$PS__Data_String_CodeUnits, &$PS__Control_Semigroupoid, &$PS__Data_String_NonEmpty_Internal, &$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Partial_Unsafe, &$PS__Data_Array_NonEmpty, &$PS__Data_String_Unsafe, &$PS__Data_Semigroup_Foldable) {
    $toNonEmptyString = $PS__Unsafe_Coerce['unsafeCoerce'];
    $snoc = function ($c)  use (&$toNonEmptyString, &$PS__Data_Semigroup, &$PS__Data_String_CodeUnits) {
        return function ($s)  use (&$toNonEmptyString, &$PS__Data_Semigroup, &$PS__Data_String_CodeUnits, &$c) {
            return $toNonEmptyString($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($s)($PS__Data_String_CodeUnits['singleton']($c)));
        };
    };
    $singleton = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($toNonEmptyString)($PS__Data_String_CodeUnits['singleton']);
    $liftS = $PS__Unsafe_Coerce['unsafeCoerce'];
    $takeWhile = function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_String_NonEmpty_Internal, &$liftS, &$PS__Data_String_CodeUnits) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_String_NonEmpty_Internal['fromString'])($liftS($PS__Data_String_CodeUnits['takeWhile']($f)));
    };
    $lastIndexOf__prime = function ($pat)  use (&$PS__Control_Semigroupoid, &$liftS, &$PS__Data_String_CodeUnits) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($liftS)($PS__Data_String_CodeUnits['lastIndexOf\'']($pat));
    };
    $lastIndexOf = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($liftS)($PS__Data_String_CodeUnits['lastIndexOf']);
    $indexOf__prime = function ($pat)  use (&$PS__Control_Semigroupoid, &$liftS, &$PS__Data_String_CodeUnits) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($liftS)($PS__Data_String_CodeUnits['indexOf\'']($pat));
    };
    $indexOf = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($liftS)($PS__Data_String_CodeUnits['indexOf']);
    $fromNonEmptyString = $PS__Unsafe_Coerce['unsafeCoerce'];
    $length = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_String_CodeUnits['length'])($fromNonEmptyString);
    $splitAt = function ($i)  use (&$PS__Data_String_CodeUnits, &$fromNonEmptyString, &$PS__Data_String_NonEmpty_Internal) {
        return function ($nes)  use (&$PS__Data_String_CodeUnits, &$i, &$fromNonEmptyString, &$PS__Data_String_NonEmpty_Internal) {
            $v = $PS__Data_String_CodeUnits['splitAt']($i)($fromNonEmptyString($nes));
            return [
                'before' => $PS__Data_String_NonEmpty_Internal['fromString']($v['before']),
                'after' => $PS__Data_String_NonEmpty_Internal['fromString']($v['after'])
            ];
        };
    };
    $take = function ($i)  use (&$fromNonEmptyString, &$PS__Data_Ord, &$PS__Data_Maybe, &$toNonEmptyString, &$PS__Data_String_CodeUnits) {
        return function ($nes)  use (&$fromNonEmptyString, &$PS__Data_Ord, &$i, &$PS__Data_Maybe, &$toNonEmptyString, &$PS__Data_String_CodeUnits) {
            $s = $fromNonEmptyString($nes);
            $__local_var__8 = $PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($i)(1);
            if ($__local_var__8) {
                return $PS__Data_Maybe['Nothing']();
            };
            return $PS__Data_Maybe['Just']['constructor']($toNonEmptyString($PS__Data_String_CodeUnits['take']($i)($s)));
        };
    };
    $takeRight = function ($i)  use (&$fromNonEmptyString, &$PS__Data_Ord, &$PS__Data_Maybe, &$toNonEmptyString, &$PS__Data_String_CodeUnits) {
        return function ($nes)  use (&$fromNonEmptyString, &$PS__Data_Ord, &$i, &$PS__Data_Maybe, &$toNonEmptyString, &$PS__Data_String_CodeUnits) {
            $s = $fromNonEmptyString($nes);
            $__local_var__9 = $PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($i)(1);
            if ($__local_var__9) {
                return $PS__Data_Maybe['Nothing']();
            };
            return $PS__Data_Maybe['Just']['constructor']($toNonEmptyString($PS__Data_String_CodeUnits['takeRight']($i)($s)));
        };
    };
    $toChar = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_String_CodeUnits['toChar'])($fromNonEmptyString);
    $toCharArray = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_String_CodeUnits['toCharArray'])($fromNonEmptyString);
    $toNonEmptyCharArray = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Partial_Unsafe['unsafePartial'](function ($dictPartial)  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['fromJust']($dictPartial);
    }))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Array_NonEmpty['fromArray'])($toCharArray));
    $uncons = function ($nes)  use (&$fromNonEmptyString, &$PS__Data_String_Unsafe, &$PS__Data_String_NonEmpty_Internal, &$PS__Data_String_CodeUnits) {
        $s = $fromNonEmptyString($nes);
        return [
            'head' => $PS__Data_String_Unsafe['charAt'](0)($s),
            'tail' => $PS__Data_String_NonEmpty_Internal['fromString']($PS__Data_String_CodeUnits['drop'](1)($s))
        ];
    };
    $fromFoldable1 = function ($dictFoldable1)  use (&$PS__Control_Semigroupoid, &$PS__Data_Semigroup_Foldable, &$PS__Data_String_NonEmpty_Internal, &$PS__Unsafe_Coerce) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Semigroup_Foldable['fold1']($dictFoldable1)($PS__Data_String_NonEmpty_Internal['semigroupNonEmptyString']))($PS__Unsafe_Coerce['unsafeCoerce']);
    };
    $fromCharArray = function ($v)  use (&$PS__Data_Maybe, &$toNonEmptyString, &$PS__Data_String_CodeUnits) {
        if (count($v) === 0) {
            return $PS__Data_Maybe['Nothing']();
        };
        return $PS__Data_Maybe['Just']['constructor']($toNonEmptyString($PS__Data_String_CodeUnits['fromCharArray']($v)));
    };
    $fromNonEmptyCharArray = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Partial_Unsafe['unsafePartial'](function ($dictPartial)  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['fromJust']($dictPartial);
    }))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($fromCharArray)($PS__Data_Array_NonEmpty['toArray']));
    $dropWhile = function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_String_NonEmpty_Internal, &$liftS, &$PS__Data_String_CodeUnits) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_String_NonEmpty_Internal['fromString'])($liftS($PS__Data_String_CodeUnits['dropWhile']($f)));
    };
    $dropRight = function ($i)  use (&$fromNonEmptyString, &$PS__Data_Ord, &$PS__Data_String_CodeUnits, &$PS__Data_Maybe, &$toNonEmptyString) {
        return function ($nes)  use (&$fromNonEmptyString, &$PS__Data_Ord, &$i, &$PS__Data_String_CodeUnits, &$PS__Data_Maybe, &$toNonEmptyString) {
            $s = $fromNonEmptyString($nes);
            $__local_var__11 = $PS__Data_Ord['greaterThanOrEq']($PS__Data_Ord['ordInt'])($i)($PS__Data_String_CodeUnits['length']($s));
            if ($__local_var__11) {
                return $PS__Data_Maybe['Nothing']();
            };
            return $PS__Data_Maybe['Just']['constructor']($toNonEmptyString($PS__Data_String_CodeUnits['dropRight']($i)($s)));
        };
    };
    $drop = function ($i)  use (&$fromNonEmptyString, &$PS__Data_Ord, &$PS__Data_String_CodeUnits, &$PS__Data_Maybe, &$toNonEmptyString) {
        return function ($nes)  use (&$fromNonEmptyString, &$PS__Data_Ord, &$i, &$PS__Data_String_CodeUnits, &$PS__Data_Maybe, &$toNonEmptyString) {
            $s = $fromNonEmptyString($nes);
            $__local_var__12 = $PS__Data_Ord['greaterThanOrEq']($PS__Data_Ord['ordInt'])($i)($PS__Data_String_CodeUnits['length']($s));
            if ($__local_var__12) {
                return $PS__Data_Maybe['Nothing']();
            };
            return $PS__Data_Maybe['Just']['constructor']($toNonEmptyString($PS__Data_String_CodeUnits['drop']($i)($s)));
        };
    };
    $countPrefix = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($liftS)($PS__Data_String_CodeUnits['countPrefix']);
    $cons = function ($c)  use (&$toNonEmptyString, &$PS__Data_Semigroup, &$PS__Data_String_CodeUnits) {
        return function ($s)  use (&$toNonEmptyString, &$PS__Data_Semigroup, &$PS__Data_String_CodeUnits, &$c) {
            return $toNonEmptyString($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_String_CodeUnits['singleton']($c))($s));
        };
    };
    $charAt = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($liftS)($PS__Data_String_CodeUnits['charAt']);
    return [
        'fromCharArray' => $fromCharArray,
        'fromNonEmptyCharArray' => $fromNonEmptyCharArray,
        'singleton' => $singleton,
        'cons' => $cons,
        'snoc' => $snoc,
        'fromFoldable1' => $fromFoldable1,
        'toCharArray' => $toCharArray,
        'toNonEmptyCharArray' => $toNonEmptyCharArray,
        'charAt' => $charAt,
        'toChar' => $toChar,
        'indexOf' => $indexOf,
        'indexOf\'' => $indexOf__prime,
        'lastIndexOf' => $lastIndexOf,
        'lastIndexOf\'' => $lastIndexOf__prime,
        'uncons' => $uncons,
        'length' => $length,
        'take' => $take,
        'takeRight' => $takeRight,
        'takeWhile' => $takeWhile,
        'drop' => $drop,
        'dropRight' => $dropRight,
        'dropWhile' => $dropWhile,
        'countPrefix' => $countPrefix,
        'splitAt' => $splitAt
    ];
})();
