<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Semigroup_First = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Functor, &$PS__Data_Eq, &$PS__Data_Ord, &$PS__Control_Apply, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Control_Monad) {
    $First = function ($x) {
        return $x;
    };
    $showFirst = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(First ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $semigroupFirst = $PS__Data_Semigroup['Semigroup'](function ($x) {
        return function ($v)  use (&$x) {
            return $x;
        };
    });
    $ordFirst = function ($dictOrd) {
        return $dictOrd;
    };
    $functorFirst = $PS__Data_Functor['Functor'](function ($f) {
        return function ($m)  use (&$f) {
            return $f($m);
        };
    });
    $eqFirst = function ($dictEq) {
        return $dictEq;
    };
    $eq1First = $PS__Data_Eq['Eq1'](function ($dictEq)  use (&$PS__Data_Eq, &$eqFirst) {
        return $PS__Data_Eq['eq']($eqFirst($dictEq));
    });
    $ord1First = $PS__Data_Ord['Ord1'](function ()  use (&$eq1First) {
        return $eq1First;
    }, function ($dictOrd)  use (&$PS__Data_Ord, &$ordFirst) {
        return $PS__Data_Ord['compare']($ordFirst($dictOrd));
    });
    $boundedFirst = function ($dictBounded) {
        return $dictBounded;
    };
    $applyFirst = $PS__Control_Apply['Apply'](function ()  use (&$functorFirst) {
        return $functorFirst;
    }, function ($v) {
        return function ($v1)  use (&$v) {
            return $v($v1);
        };
    });
    $bindFirst = $PS__Control_Bind['Bind'](function ()  use (&$applyFirst) {
        return $applyFirst;
    }, function ($v) {
        return function ($f)  use (&$v) {
            return $f($v);
        };
    });
    $applicativeFirst = $PS__Control_Applicative['Applicative'](function ()  use (&$applyFirst) {
        return $applyFirst;
    }, $First);
    $monadFirst = $PS__Control_Monad['Monad'](function ()  use (&$applicativeFirst) {
        return $applicativeFirst;
    }, function ()  use (&$bindFirst) {
        return $bindFirst;
    });
    return [
        'First' => $First,
        'eqFirst' => $eqFirst,
        'eq1First' => $eq1First,
        'ordFirst' => $ordFirst,
        'ord1First' => $ord1First,
        'boundedFirst' => $boundedFirst,
        'showFirst' => $showFirst,
        'functorFirst' => $functorFirst,
        'applyFirst' => $applyFirst,
        'applicativeFirst' => $applicativeFirst,
        'bindFirst' => $bindFirst,
        'monadFirst' => $monadFirst,
        'semigroupFirst' => $semigroupFirst
    ];
})();
