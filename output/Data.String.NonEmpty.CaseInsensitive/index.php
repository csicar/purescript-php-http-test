<?php
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.String.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Data.String.NonEmpty.Internal/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_String_NonEmpty_CaseInsensitive = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_String_NonEmpty_Internal, &$PS__Data_Newtype, &$PS__Data_Eq, &$PS__Data_Ord) {
    $CaseInsensitiveNonEmptyString = function ($x) {
        return $x;
    };
    $showCaseInsensitiveNonEmptyString = $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$PS__Data_String_NonEmpty_Internal) {
        return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(CaseInsensitiveNonEmptyString ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_String_NonEmpty_Internal['showNonEmptyString'])($v))(')'));
    });
    $newtypeCaseInsensitiveNonEmptyString = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $CaseInsensitiveNonEmptyString);
    $eqCaseInsensitiveNonEmptyString = $PS__Data_Eq['Eq'](function ($v)  use (&$PS__Data_Eq, &$PS__Data_String_NonEmpty_Internal) {
        return function ($v1)  use (&$PS__Data_Eq, &$PS__Data_String_NonEmpty_Internal, &$v) {
            return $PS__Data_Eq['eq']($PS__Data_String_NonEmpty_Internal['eqNonEmptyString'])($PS__Data_String_NonEmpty_Internal['toLower']($v))($PS__Data_String_NonEmpty_Internal['toLower']($v1));
        };
    });
    $ordCaseInsensitiveNonEmptyString = $PS__Data_Ord['Ord'](function ()  use (&$eqCaseInsensitiveNonEmptyString) {
        return $eqCaseInsensitiveNonEmptyString;
    }, function ($v)  use (&$PS__Data_Ord, &$PS__Data_String_NonEmpty_Internal) {
        return function ($v1)  use (&$PS__Data_Ord, &$PS__Data_String_NonEmpty_Internal, &$v) {
            return $PS__Data_Ord['compare']($PS__Data_String_NonEmpty_Internal['ordNonEmptyString'])($PS__Data_String_NonEmpty_Internal['toLower']($v))($PS__Data_String_NonEmpty_Internal['toLower']($v1));
        };
    });
    return [
        'CaseInsensitiveNonEmptyString' => $CaseInsensitiveNonEmptyString,
        'eqCaseInsensitiveNonEmptyString' => $eqCaseInsensitiveNonEmptyString,
        'ordCaseInsensitiveNonEmptyString' => $ordCaseInsensitiveNonEmptyString,
        'showCaseInsensitiveNonEmptyString' => $showCaseInsensitiveNonEmptyString,
        'newtypeCaseInsensitiveNonEmptyString' => $newtypeCaseInsensitiveNonEmptyString
    ];
})();
