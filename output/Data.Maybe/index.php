<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Extend/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadZero/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor.Invariant/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Maybe = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Control_Alt, &$PS__Data_Functor, &$PS__Control_Applicative, &$PS__Data_Monoid, &$PS__Data_Unit, &$PS__Data_Function, &$PS__Data_Functor_Invariant, &$PS__Control_Category, &$PS__Control_Extend, &$PS__Data_Eq, &$PS__Data_Ord, &$PS__Data_Ordering, &$PS__Data_Bounded, &$PS__Control_Apply, &$PS__Control_Bind, &$PS__Control_Monad, &$PS__Control_Plus, &$PS__Control_Alternative, &$PS__Control_MonadZero) {
    $Nothing = function () {
        return [
            'type' => 'Nothing'
        ];
    };
    $Just = [
        'constructor' => function ($value0) {
            return [
                'type' => 'Just',
                'value0' => $value0
            ];
        },
        'create' => function ($value0)  use (&$Just) {
            return $Just['constructor']($value0);
        }
    ];
    $showMaybe = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            if ($v['type'] === 'Just') {
                return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Just ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v['value0']))(')'));
            };
            if ($v['type'] === 'Nothing') {
                return 'Nothing';
            };
            throw new \Exception('Failed pattern match at Data.Maybe line 206, column 1 - line 206, column 47: ' . var_dump([ $v['constructor']['name'] ]));
        });
    };
    $semigroupMaybe = function ($dictSemigroup)  use (&$PS__Data_Semigroup, &$Just) {
        return $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$Just, &$PS__Data_Semigroup, &$dictSemigroup) {
            return function ($v1)  use (&$v, &$Just, &$PS__Data_Semigroup, &$dictSemigroup) {
                if ($v['type'] === 'Nothing') {
                    return $v1;
                };
                if ($v1['type'] === 'Nothing') {
                    return $v;
                };
                if ($v['type'] === 'Just' && $v1['type'] === 'Just') {
                    return $Just['constructor']($PS__Data_Semigroup['append']($dictSemigroup)($v['value0'])($v1['value0']));
                };
                throw new \Exception('Failed pattern match at Data.Maybe line 175, column 1 - line 175, column 62: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
            };
        });
    };
    $optional = function ($dictAlternative)  use (&$PS__Control_Alt, &$PS__Data_Functor, &$Just, &$PS__Control_Applicative, &$Nothing) {
        return function ($a)  use (&$PS__Control_Alt, &$dictAlternative, &$PS__Data_Functor, &$Just, &$PS__Control_Applicative, &$Nothing) {
            return $PS__Control_Alt['alt'](($dictAlternative['Plus1']())['Alt0']())($PS__Data_Functor['map']((($dictAlternative['Plus1']())['Alt0']())['Functor0']())($Just['create'])($a))($PS__Control_Applicative['pure']($dictAlternative['Applicative0']())($Nothing()));
        };
    };
    $monoidMaybe = function ($dictSemigroup)  use (&$PS__Data_Monoid, &$semigroupMaybe, &$Nothing) {
        return $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupMaybe, &$dictSemigroup) {
            return $semigroupMaybe($dictSemigroup);
        }, $Nothing());
    };
    $maybe__prime = function ($v)  use (&$PS__Data_Unit) {
        return function ($v1)  use (&$v, &$PS__Data_Unit) {
            return function ($v2)  use (&$v, &$PS__Data_Unit, &$v1) {
                if ($v2['type'] === 'Nothing') {
                    return $v($PS__Data_Unit['unit']);
                };
                if ($v2['type'] === 'Just') {
                    return $v1($v2['value0']);
                };
                throw new \Exception('Failed pattern match at Data.Maybe line 231, column 1 - line 231, column 62: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'], $v2['constructor']['name'] ]));
            };
        };
    };
    $maybe = function ($v) {
        return function ($v1)  use (&$v) {
            return function ($v2)  use (&$v, &$v1) {
                if ($v2['type'] === 'Nothing') {
                    return $v;
                };
                if ($v2['type'] === 'Just') {
                    return $v1($v2['value0']);
                };
                throw new \Exception('Failed pattern match at Data.Maybe line 218, column 1 - line 218, column 51: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'], $v2['constructor']['name'] ]));
            };
        };
    };
    $isNothing = $maybe(true)($PS__Data_Function['const'](false));
    $isJust = $maybe(false)($PS__Data_Function['const'](true));
    $functorMaybe = $PS__Data_Functor['Functor'](function ($v)  use (&$Just, &$Nothing) {
        return function ($v1)  use (&$Just, &$v, &$Nothing) {
            if ($v1['type'] === 'Just') {
                return $Just['constructor']($v($v1['value0']));
            };
            return $Nothing();
        };
    });
    $invariantMaybe = $PS__Data_Functor_Invariant['Invariant']($PS__Data_Functor_Invariant['imapF']($functorMaybe));
    $fromMaybe__prime = function ($a)  use (&$maybe__prime, &$PS__Control_Category) {
        return $maybe__prime($a)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    };
    $fromMaybe = function ($a)  use (&$maybe, &$PS__Control_Category) {
        return $maybe($a)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    };
    $fromJust = function ($dictPartial) {
        return function ($v)  use (&$dictPartial) {
            $___unused = function ($dictPartial1) {
                return function ($__local_var__35) {
                    return $__local_var__35;
                };
            };
            return $___unused($dictPartial)((function ()  use (&$v) {
                if ($v['type'] === 'Just') {
                    return $v['value0'];
                };
                throw new \Exception('Failed pattern match at Data.Maybe line 269, column 1 - line 269, column 46: ' . var_dump([ $v['constructor']['name'] ]));
            })());
        };
    };
    $extendMaybe = $PS__Control_Extend['Extend'](function ()  use (&$functorMaybe) {
        return $functorMaybe;
    }, function ($v)  use (&$Nothing, &$Just) {
        return function ($v1)  use (&$Nothing, &$Just, &$v) {
            if ($v1['type'] === 'Nothing') {
                return $Nothing();
            };
            return $Just['constructor']($v($v1));
        };
    });
    $eqMaybe = function ($dictEq)  use (&$PS__Data_Eq) {
        return $PS__Data_Eq['Eq'](function ($x)  use (&$PS__Data_Eq, &$dictEq) {
            return function ($y)  use (&$x, &$PS__Data_Eq, &$dictEq) {
                if ($x['type'] === 'Nothing' && $y['type'] === 'Nothing') {
                    return true;
                };
                if ($x['type'] === 'Just' && $y['type'] === 'Just') {
                    return $PS__Data_Eq['eq']($dictEq)($x['value0'])($y['value0']);
                };
                return false;
            };
        });
    };
    $ordMaybe = function ($dictOrd)  use (&$PS__Data_Ord, &$eqMaybe, &$PS__Data_Ordering) {
        return $PS__Data_Ord['Ord'](function ()  use (&$eqMaybe, &$dictOrd) {
            return $eqMaybe($dictOrd['Eq0']());
        }, function ($x)  use (&$PS__Data_Ordering, &$PS__Data_Ord, &$dictOrd) {
            return function ($y)  use (&$x, &$PS__Data_Ordering, &$PS__Data_Ord, &$dictOrd) {
                if ($x['type'] === 'Nothing' && $y['type'] === 'Nothing') {
                    return $PS__Data_Ordering['EQ']();
                };
                if ($x['type'] === 'Nothing') {
                    return $PS__Data_Ordering['LT']();
                };
                if ($y['type'] === 'Nothing') {
                    return $PS__Data_Ordering['GT']();
                };
                if ($x['type'] === 'Just' && $y['type'] === 'Just') {
                    return $PS__Data_Ord['compare']($dictOrd)($x['value0'])($y['value0']);
                };
                throw new \Exception('Failed pattern match at Data.Maybe line 195, column 8 - line 195, column 51: ' . var_dump([ $x['constructor']['name'], $y['constructor']['name'] ]));
            };
        });
    };
    $eq1Maybe = $PS__Data_Eq['Eq1'](function ($dictEq)  use (&$PS__Data_Eq, &$eqMaybe) {
        return $PS__Data_Eq['eq']($eqMaybe($dictEq));
    });
    $ord1Maybe = $PS__Data_Ord['Ord1'](function ()  use (&$eq1Maybe) {
        return $eq1Maybe;
    }, function ($dictOrd)  use (&$PS__Data_Ord, &$ordMaybe) {
        return $PS__Data_Ord['compare']($ordMaybe($dictOrd));
    });
    $boundedMaybe = function ($dictBounded)  use (&$PS__Data_Bounded, &$ordMaybe, &$Nothing, &$Just) {
        return $PS__Data_Bounded['Bounded'](function ()  use (&$ordMaybe, &$dictBounded) {
            return $ordMaybe($dictBounded['Ord0']());
        }, $Nothing(), $Just['constructor']($PS__Data_Bounded['top']($dictBounded)));
    };
    $applyMaybe = $PS__Control_Apply['Apply'](function ()  use (&$functorMaybe) {
        return $functorMaybe;
    }, function ($v)  use (&$PS__Data_Functor, &$functorMaybe, &$Nothing) {
        return function ($v1)  use (&$v, &$PS__Data_Functor, &$functorMaybe, &$Nothing) {
            if ($v['type'] === 'Just') {
                return $PS__Data_Functor['map']($functorMaybe)($v['value0'])($v1);
            };
            if ($v['type'] === 'Nothing') {
                return $Nothing();
            };
            throw new \Exception('Failed pattern match at Data.Maybe line 67, column 1 - line 67, column 35: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
        };
    });
    $bindMaybe = $PS__Control_Bind['Bind'](function ()  use (&$applyMaybe) {
        return $applyMaybe;
    }, function ($v)  use (&$Nothing) {
        return function ($v1)  use (&$v, &$Nothing) {
            if ($v['type'] === 'Just') {
                return $v1($v['value0']);
            };
            if ($v['type'] === 'Nothing') {
                return $Nothing();
            };
            throw new \Exception('Failed pattern match at Data.Maybe line 126, column 1 - line 126, column 33: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
        };
    });
    $applicativeMaybe = $PS__Control_Applicative['Applicative'](function ()  use (&$applyMaybe) {
        return $applyMaybe;
    }, $Just['create']);
    $monadMaybe = $PS__Control_Monad['Monad'](function ()  use (&$applicativeMaybe) {
        return $applicativeMaybe;
    }, function ()  use (&$bindMaybe) {
        return $bindMaybe;
    });
    $altMaybe = $PS__Control_Alt['Alt'](function ()  use (&$functorMaybe) {
        return $functorMaybe;
    }, function ($v) {
        return function ($v1)  use (&$v) {
            if ($v['type'] === 'Nothing') {
                return $v1;
            };
            return $v;
        };
    });
    $plusMaybe = $PS__Control_Plus['Plus'](function ()  use (&$altMaybe) {
        return $altMaybe;
    }, $Nothing());
    $alternativeMaybe = $PS__Control_Alternative['Alternative'](function ()  use (&$applicativeMaybe) {
        return $applicativeMaybe;
    }, function ()  use (&$plusMaybe) {
        return $plusMaybe;
    });
    $monadZeroMaybe = $PS__Control_MonadZero['MonadZero'](function ()  use (&$alternativeMaybe) {
        return $alternativeMaybe;
    }, function ()  use (&$monadMaybe) {
        return $monadMaybe;
    });
    return [
        'Nothing' => $Nothing,
        'Just' => $Just,
        'maybe' => $maybe,
        'maybe\'' => $maybe__prime,
        'fromMaybe' => $fromMaybe,
        'fromMaybe\'' => $fromMaybe__prime,
        'isJust' => $isJust,
        'isNothing' => $isNothing,
        'fromJust' => $fromJust,
        'optional' => $optional,
        'functorMaybe' => $functorMaybe,
        'applyMaybe' => $applyMaybe,
        'applicativeMaybe' => $applicativeMaybe,
        'altMaybe' => $altMaybe,
        'plusMaybe' => $plusMaybe,
        'alternativeMaybe' => $alternativeMaybe,
        'bindMaybe' => $bindMaybe,
        'monadMaybe' => $monadMaybe,
        'monadZeroMaybe' => $monadZeroMaybe,
        'extendMaybe' => $extendMaybe,
        'invariantMaybe' => $invariantMaybe,
        'semigroupMaybe' => $semigroupMaybe,
        'monoidMaybe' => $monoidMaybe,
        'eqMaybe' => $eqMaybe,
        'eq1Maybe' => $eq1Maybe,
        'ordMaybe' => $ordMaybe,
        'ord1Maybe' => $ord1Maybe,
        'boundedMaybe' => $boundedMaybe,
        'showMaybe' => $showMaybe
    ];
})();
