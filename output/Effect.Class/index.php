<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Effect/index.php');
$PS__Effect_Class = (function ()  use (&$PS__Effect, &$PS__Control_Category) {
    $MonadEffect = function ($Monad0, $liftEffect) {
        return [
            'Monad0' => $Monad0,
            'liftEffect' => $liftEffect
        ];
    };
    $monadEffectEffect = $MonadEffect(function ()  use (&$PS__Effect) {
        return $PS__Effect['monadEffect'];
    }, $PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    $liftEffect = function ($dict) {
        return $dict['liftEffect'];
    };
    return [
        'liftEffect' => $liftEffect,
        'MonadEffect' => $MonadEffect,
        'monadEffectEffect' => $monadEffectEffect
    ];
})();
