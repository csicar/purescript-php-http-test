<?php

$exports['defer'] = function ($thunk) {
  $v = NULL;
  return function() use (&$thunk, &$v) {
    if ($thunk === NULL) return $v;

    $v = $thunk();
    $thunk = NULL; // eslint-disable-line no-param-reassign
    return $v;
  };
};

$exports['force'] = function ($l) {
  return $l();
};
