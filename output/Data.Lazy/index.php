<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad/index.php');
require_once(dirname(__FILE__) . '/../Control.Extend/index.php');
require_once(dirname(__FILE__) . '/../Control.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.BooleanAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.CommutativeRing/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.EuclideanRing/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.FoldableWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor.Invariant/index.php');
require_once(dirname(__FILE__) . '/../Data.FunctorWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.TraversableWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Lazy = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Semiring, &$PS__Data_Ring, &$PS__Data_Monoid, &$PS__Control_Lazy, &$PS__Data_Unit, &$PS__Data_Functor, &$PS__Data_FunctorWithIndex, &$PS__Data_Function, &$PS__Data_Functor_Invariant, &$PS__Data_Foldable, &$PS__Data_FoldableWithIndex, &$PS__Data_Traversable, &$PS__Control_Semigroupoid, &$PS__Data_TraversableWithIndex, &$PS__Data_Semigroup_Foldable, &$PS__Data_Semigroup_Traversable, &$PS__Control_Extend, &$PS__Data_Eq, &$PS__Data_Ord, &$PS__Control_Comonad, &$PS__Data_CommutativeRing, &$PS__Data_EuclideanRing, &$PS__Data_Bounded, &$PS__Control_Apply, &$PS__Control_Bind, &$PS__Data_HeytingAlgebra, &$PS__Data_BooleanAlgebra, &$PS__Control_Applicative, &$PS__Control_Monad) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $showLazy = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$__foreign) {
        return $PS__Data_Show['Show'](function ($x)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow, &$__foreign) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(defer \\_ -> ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($__foreign['force']($x)))(')'));
        });
    };
    $semiringLazy = function ($dictSemiring)  use (&$PS__Data_Semiring, &$__foreign) {
        return $PS__Data_Semiring['Semiring'](function ($a)  use (&$__foreign, &$PS__Data_Semiring, &$dictSemiring) {
            return function ($b)  use (&$__foreign, &$PS__Data_Semiring, &$dictSemiring, &$a) {
                return $__foreign['defer'](function ($v)  use (&$PS__Data_Semiring, &$dictSemiring, &$__foreign, &$a, &$b) {
                    return $PS__Data_Semiring['add']($dictSemiring)($__foreign['force']($a))($__foreign['force']($b));
                });
            };
        }, function ($a)  use (&$__foreign, &$PS__Data_Semiring, &$dictSemiring) {
            return function ($b)  use (&$__foreign, &$PS__Data_Semiring, &$dictSemiring, &$a) {
                return $__foreign['defer'](function ($v)  use (&$PS__Data_Semiring, &$dictSemiring, &$__foreign, &$a, &$b) {
                    return $PS__Data_Semiring['mul']($dictSemiring)($__foreign['force']($a))($__foreign['force']($b));
                });
            };
        }, $__foreign['defer'](function ($v)  use (&$PS__Data_Semiring, &$dictSemiring) {
            return $PS__Data_Semiring['one']($dictSemiring);
        }), $__foreign['defer'](function ($v)  use (&$PS__Data_Semiring, &$dictSemiring) {
            return $PS__Data_Semiring['zero']($dictSemiring);
        }));
    };
    $semigroupLazy = function ($dictSemigroup)  use (&$PS__Data_Semigroup, &$__foreign) {
        return $PS__Data_Semigroup['Semigroup'](function ($a)  use (&$__foreign, &$PS__Data_Semigroup, &$dictSemigroup) {
            return function ($b)  use (&$__foreign, &$PS__Data_Semigroup, &$dictSemigroup, &$a) {
                return $__foreign['defer'](function ($v)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$__foreign, &$a, &$b) {
                    return $PS__Data_Semigroup['append']($dictSemigroup)($__foreign['force']($a))($__foreign['force']($b));
                });
            };
        });
    };
    $ringLazy = function ($dictRing)  use (&$PS__Data_Ring, &$semiringLazy, &$__foreign) {
        return $PS__Data_Ring['Ring'](function ()  use (&$semiringLazy, &$dictRing) {
            return $semiringLazy($dictRing['Semiring0']());
        }, function ($a)  use (&$__foreign, &$PS__Data_Ring, &$dictRing) {
            return function ($b)  use (&$__foreign, &$PS__Data_Ring, &$dictRing, &$a) {
                return $__foreign['defer'](function ($v)  use (&$PS__Data_Ring, &$dictRing, &$__foreign, &$a, &$b) {
                    return $PS__Data_Ring['sub']($dictRing)($__foreign['force']($a))($__foreign['force']($b));
                });
            };
        });
    };
    $monoidLazy = function ($dictMonoid)  use (&$PS__Data_Monoid, &$semigroupLazy, &$__foreign) {
        return $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupLazy, &$dictMonoid) {
            return $semigroupLazy($dictMonoid['Semigroup0']());
        }, $__foreign['defer'](function ($v)  use (&$PS__Data_Monoid, &$dictMonoid) {
            return $PS__Data_Monoid['mempty']($dictMonoid);
        }));
    };
    $lazyLazy = $PS__Control_Lazy['Lazy'](function ($f)  use (&$__foreign, &$PS__Data_Unit) {
        return $__foreign['defer'](function ($v)  use (&$__foreign, &$f, &$PS__Data_Unit) {
            return $__foreign['force']($f($PS__Data_Unit['unit']));
        });
    });
    $functorLazy = $PS__Data_Functor['Functor'](function ($f)  use (&$__foreign) {
        return function ($l)  use (&$__foreign, &$f) {
            return $__foreign['defer'](function ($v)  use (&$f, &$__foreign, &$l) {
                return $f($__foreign['force']($l));
            });
        };
    });
    $functorWithIndexLazy = $PS__Data_FunctorWithIndex['FunctorWithIndex'](function ()  use (&$functorLazy) {
        return $functorLazy;
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Functor, &$functorLazy, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Functor['map']($functorLazy))($f($PS__Data_Unit['unit']));
    });
    $invariantLazy = $PS__Data_Functor_Invariant['Invariant']($PS__Data_Functor_Invariant['imapF']($functorLazy));
    $foldableLazy = $PS__Data_Foldable['Foldable'](function ($dictMonoid)  use (&$__foreign) {
        return function ($f)  use (&$__foreign) {
            return function ($l)  use (&$f, &$__foreign) {
                return $f($__foreign['force']($l));
            };
        };
    }, function ($f)  use (&$__foreign) {
        return function ($z)  use (&$f, &$__foreign) {
            return function ($l)  use (&$f, &$z, &$__foreign) {
                return $f($z)($__foreign['force']($l));
            };
        };
    }, function ($f)  use (&$__foreign) {
        return function ($z)  use (&$f, &$__foreign) {
            return function ($l)  use (&$f, &$__foreign, &$z) {
                return $f($__foreign['force']($l))($z);
            };
        };
    });
    $foldableWithIndexLazy = $PS__Data_FoldableWithIndex['FoldableWithIndex'](function ()  use (&$foldableLazy) {
        return $foldableLazy;
    }, function ($dictMonoid)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$foldableLazy, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$foldableLazy, &$dictMonoid, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Foldable['foldMap']($foldableLazy)($dictMonoid))($f($PS__Data_Unit['unit']));
        };
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$foldableLazy, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldl']($foldableLazy))($f($PS__Data_Unit['unit']));
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$foldableLazy, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldr']($foldableLazy))($f($PS__Data_Unit['unit']));
    });
    $traversableLazy = $PS__Data_Traversable['Traversable'](function ()  use (&$foldableLazy) {
        return $foldableLazy;
    }, function ()  use (&$functorLazy) {
        return $functorLazy;
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Control_Semigroupoid, &$__foreign, &$PS__Data_Function) {
        return function ($l)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Control_Semigroupoid, &$__foreign, &$PS__Data_Function) {
            return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($__foreign['defer'])($PS__Data_Function['const']))($__foreign['force']($l));
        };
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Control_Semigroupoid, &$__foreign, &$PS__Data_Function) {
        return function ($f)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Control_Semigroupoid, &$__foreign, &$PS__Data_Function) {
            return function ($l)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Control_Semigroupoid, &$__foreign, &$PS__Data_Function, &$f) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($__foreign['defer'])($PS__Data_Function['const']))($f($__foreign['force']($l)));
            };
        };
    });
    $traversableWithIndexLazy = $PS__Data_TraversableWithIndex['TraversableWithIndex'](function ()  use (&$foldableWithIndexLazy) {
        return $foldableWithIndexLazy;
    }, function ()  use (&$functorWithIndexLazy) {
        return $functorWithIndexLazy;
    }, function ()  use (&$traversableLazy) {
        return $traversableLazy;
    }, function ($dictApplicative)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$traversableLazy, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$traversableLazy, &$dictApplicative, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Traversable['traverse']($traversableLazy)($dictApplicative))($f($PS__Data_Unit['unit']));
        };
    });
    $foldable1Lazy = $PS__Data_Semigroup_Foldable['Foldable1'](function ()  use (&$foldableLazy) {
        return $foldableLazy;
    }, function ($dictSemigroup)  use (&$PS__Data_Semigroup_Foldable, &$foldable1Lazy) {
        return $PS__Data_Semigroup_Foldable['fold1Default']($foldable1Lazy)($dictSemigroup);
    }, function ($dictSemigroup)  use (&$__foreign) {
        return function ($f)  use (&$__foreign) {
            return function ($l)  use (&$f, &$__foreign) {
                return $f($__foreign['force']($l));
            };
        };
    });
    $traversable1Lazy = $PS__Data_Semigroup_Traversable['Traversable1'](function ()  use (&$foldable1Lazy) {
        return $foldable1Lazy;
    }, function ()  use (&$traversableLazy) {
        return $traversableLazy;
    }, function ($dictApply)  use (&$PS__Data_Functor, &$PS__Control_Semigroupoid, &$__foreign, &$PS__Data_Function) {
        return function ($l)  use (&$PS__Data_Functor, &$dictApply, &$PS__Control_Semigroupoid, &$__foreign, &$PS__Data_Function) {
            return $PS__Data_Functor['map']($dictApply['Functor0']())($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($__foreign['defer'])($PS__Data_Function['const']))($__foreign['force']($l));
        };
    }, function ($dictApply)  use (&$PS__Data_Functor, &$PS__Control_Semigroupoid, &$__foreign, &$PS__Data_Function) {
        return function ($f)  use (&$PS__Data_Functor, &$dictApply, &$PS__Control_Semigroupoid, &$__foreign, &$PS__Data_Function) {
            return function ($l)  use (&$PS__Data_Functor, &$dictApply, &$PS__Control_Semigroupoid, &$__foreign, &$PS__Data_Function, &$f) {
                return $PS__Data_Functor['map']($dictApply['Functor0']())($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($__foreign['defer'])($PS__Data_Function['const']))($f($__foreign['force']($l)));
            };
        };
    });
    $extendLazy = $PS__Control_Extend['Extend'](function ()  use (&$functorLazy) {
        return $functorLazy;
    }, function ($f)  use (&$__foreign) {
        return function ($x)  use (&$__foreign, &$f) {
            return $__foreign['defer'](function ($v)  use (&$f, &$x) {
                return $f($x);
            });
        };
    });
    $eqLazy = function ($dictEq)  use (&$PS__Data_Eq, &$__foreign) {
        return $PS__Data_Eq['Eq'](function ($x)  use (&$PS__Data_Eq, &$dictEq, &$__foreign) {
            return function ($y)  use (&$PS__Data_Eq, &$dictEq, &$__foreign, &$x) {
                return $PS__Data_Eq['eq']($dictEq)($__foreign['force']($x))($__foreign['force']($y));
            };
        });
    };
    $ordLazy = function ($dictOrd)  use (&$PS__Data_Ord, &$eqLazy, &$__foreign) {
        return $PS__Data_Ord['Ord'](function ()  use (&$eqLazy, &$dictOrd) {
            return $eqLazy($dictOrd['Eq0']());
        }, function ($x)  use (&$PS__Data_Ord, &$dictOrd, &$__foreign) {
            return function ($y)  use (&$PS__Data_Ord, &$dictOrd, &$__foreign, &$x) {
                return $PS__Data_Ord['compare']($dictOrd)($__foreign['force']($x))($__foreign['force']($y));
            };
        });
    };
    $eq1Lazy = $PS__Data_Eq['Eq1'](function ($dictEq)  use (&$PS__Data_Eq, &$eqLazy) {
        return $PS__Data_Eq['eq']($eqLazy($dictEq));
    });
    $ord1Lazy = $PS__Data_Ord['Ord1'](function ()  use (&$eq1Lazy) {
        return $eq1Lazy;
    }, function ($dictOrd)  use (&$PS__Data_Ord, &$ordLazy) {
        return $PS__Data_Ord['compare']($ordLazy($dictOrd));
    });
    $comonadLazy = $PS__Control_Comonad['Comonad'](function ()  use (&$extendLazy) {
        return $extendLazy;
    }, $__foreign['force']);
    $commutativeRingLazy = function ($dictCommutativeRing)  use (&$PS__Data_CommutativeRing, &$ringLazy) {
        return $PS__Data_CommutativeRing['CommutativeRing'](function ()  use (&$ringLazy, &$dictCommutativeRing) {
            return $ringLazy($dictCommutativeRing['Ring0']());
        });
    };
    $euclideanRingLazy = function ($dictEuclideanRing)  use (&$PS__Data_EuclideanRing, &$commutativeRingLazy, &$PS__Control_Semigroupoid, &$__foreign) {
        return $PS__Data_EuclideanRing['EuclideanRing'](function ()  use (&$commutativeRingLazy, &$dictEuclideanRing) {
            return $commutativeRingLazy($dictEuclideanRing['CommutativeRing0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_EuclideanRing['degree']($dictEuclideanRing))($__foreign['force']), function ($a)  use (&$__foreign, &$PS__Data_EuclideanRing, &$dictEuclideanRing) {
            return function ($b)  use (&$__foreign, &$PS__Data_EuclideanRing, &$dictEuclideanRing, &$a) {
                return $__foreign['defer'](function ($v)  use (&$PS__Data_EuclideanRing, &$dictEuclideanRing, &$__foreign, &$a, &$b) {
                    return $PS__Data_EuclideanRing['div']($dictEuclideanRing)($__foreign['force']($a))($__foreign['force']($b));
                });
            };
        }, function ($a)  use (&$__foreign, &$PS__Data_EuclideanRing, &$dictEuclideanRing) {
            return function ($b)  use (&$__foreign, &$PS__Data_EuclideanRing, &$dictEuclideanRing, &$a) {
                return $__foreign['defer'](function ($v)  use (&$PS__Data_EuclideanRing, &$dictEuclideanRing, &$__foreign, &$a, &$b) {
                    return $PS__Data_EuclideanRing['mod']($dictEuclideanRing)($__foreign['force']($a))($__foreign['force']($b));
                });
            };
        });
    };
    $boundedLazy = function ($dictBounded)  use (&$PS__Data_Bounded, &$ordLazy, &$__foreign) {
        return $PS__Data_Bounded['Bounded'](function ()  use (&$ordLazy, &$dictBounded) {
            return $ordLazy($dictBounded['Ord0']());
        }, $__foreign['defer'](function ($v)  use (&$PS__Data_Bounded, &$dictBounded) {
            return $PS__Data_Bounded['bottom']($dictBounded);
        }), $__foreign['defer'](function ($v)  use (&$PS__Data_Bounded, &$dictBounded) {
            return $PS__Data_Bounded['top']($dictBounded);
        }));
    };
    $applyLazy = $PS__Control_Apply['Apply'](function ()  use (&$functorLazy) {
        return $functorLazy;
    }, function ($f)  use (&$__foreign) {
        return function ($x)  use (&$__foreign, &$f) {
            return $__foreign['defer'](function ($v)  use (&$__foreign, &$f, &$x) {
                return $__foreign['force']($f)($__foreign['force']($x));
            });
        };
    });
    $bindLazy = $PS__Control_Bind['Bind'](function ()  use (&$applyLazy) {
        return $applyLazy;
    }, function ($l)  use (&$__foreign, &$PS__Data_Function) {
        return function ($f)  use (&$__foreign, &$PS__Data_Function, &$l) {
            return $__foreign['defer'](function ($v)  use (&$PS__Data_Function, &$__foreign, &$f, &$l) {
                return $PS__Data_Function['apply']($__foreign['force'])($f($__foreign['force']($l)));
            });
        };
    });
    $heytingAlgebraLazy = function ($dictHeytingAlgebra)  use (&$PS__Data_HeytingAlgebra, &$PS__Control_Apply, &$applyLazy, &$PS__Data_Functor, &$functorLazy, &$__foreign) {
        return $PS__Data_HeytingAlgebra['HeytingAlgebra'](function ($a)  use (&$PS__Control_Apply, &$applyLazy, &$PS__Data_Functor, &$functorLazy, &$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra) {
            return function ($b)  use (&$PS__Control_Apply, &$applyLazy, &$PS__Data_Functor, &$functorLazy, &$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$a) {
                return $PS__Control_Apply['apply']($applyLazy)($PS__Data_Functor['map']($functorLazy)($PS__Data_HeytingAlgebra['conj']($dictHeytingAlgebra))($a))($b);
            };
        }, function ($a)  use (&$PS__Control_Apply, &$applyLazy, &$PS__Data_Functor, &$functorLazy, &$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra) {
            return function ($b)  use (&$PS__Control_Apply, &$applyLazy, &$PS__Data_Functor, &$functorLazy, &$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$a) {
                return $PS__Control_Apply['apply']($applyLazy)($PS__Data_Functor['map']($functorLazy)($PS__Data_HeytingAlgebra['disj']($dictHeytingAlgebra))($a))($b);
            };
        }, $__foreign['defer'](function ($v)  use (&$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra) {
            return $PS__Data_HeytingAlgebra['ff']($dictHeytingAlgebra);
        }), function ($a)  use (&$PS__Control_Apply, &$applyLazy, &$PS__Data_Functor, &$functorLazy, &$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra) {
            return function ($b)  use (&$PS__Control_Apply, &$applyLazy, &$PS__Data_Functor, &$functorLazy, &$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$a) {
                return $PS__Control_Apply['apply']($applyLazy)($PS__Data_Functor['map']($functorLazy)($PS__Data_HeytingAlgebra['implies']($dictHeytingAlgebra))($a))($b);
            };
        }, function ($a)  use (&$PS__Data_Functor, &$functorLazy, &$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra) {
            return $PS__Data_Functor['map']($functorLazy)($PS__Data_HeytingAlgebra['not']($dictHeytingAlgebra))($a);
        }, $__foreign['defer'](function ($v)  use (&$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra) {
            return $PS__Data_HeytingAlgebra['tt']($dictHeytingAlgebra);
        }));
    };
    $booleanAlgebraLazy = function ($dictBooleanAlgebra)  use (&$PS__Data_BooleanAlgebra, &$heytingAlgebraLazy) {
        return $PS__Data_BooleanAlgebra['BooleanAlgebra'](function ()  use (&$heytingAlgebraLazy, &$dictBooleanAlgebra) {
            return $heytingAlgebraLazy($dictBooleanAlgebra['HeytingAlgebra0']());
        });
    };
    $applicativeLazy = $PS__Control_Applicative['Applicative'](function ()  use (&$applyLazy) {
        return $applyLazy;
    }, function ($a)  use (&$__foreign) {
        return $__foreign['defer'](function ($v)  use (&$a) {
            return $a;
        });
    });
    $monadLazy = $PS__Control_Monad['Monad'](function ()  use (&$applicativeLazy) {
        return $applicativeLazy;
    }, function ()  use (&$bindLazy) {
        return $bindLazy;
    });
    return [
        'semiringLazy' => $semiringLazy,
        'ringLazy' => $ringLazy,
        'commutativeRingLazy' => $commutativeRingLazy,
        'euclideanRingLazy' => $euclideanRingLazy,
        'eqLazy' => $eqLazy,
        'eq1Lazy' => $eq1Lazy,
        'ordLazy' => $ordLazy,
        'ord1Lazy' => $ord1Lazy,
        'boundedLazy' => $boundedLazy,
        'semigroupLazy' => $semigroupLazy,
        'monoidLazy' => $monoidLazy,
        'heytingAlgebraLazy' => $heytingAlgebraLazy,
        'booleanAlgebraLazy' => $booleanAlgebraLazy,
        'functorLazy' => $functorLazy,
        'functorWithIndexLazy' => $functorWithIndexLazy,
        'foldableLazy' => $foldableLazy,
        'foldableWithIndexLazy' => $foldableWithIndexLazy,
        'foldable1Lazy' => $foldable1Lazy,
        'traversableLazy' => $traversableLazy,
        'traversableWithIndexLazy' => $traversableWithIndexLazy,
        'traversable1Lazy' => $traversable1Lazy,
        'invariantLazy' => $invariantLazy,
        'applyLazy' => $applyLazy,
        'applicativeLazy' => $applicativeLazy,
        'bindLazy' => $bindLazy,
        'monadLazy' => $monadLazy,
        'extendLazy' => $extendLazy,
        'comonadLazy' => $comonadLazy,
        'showLazy' => $showLazy,
        'lazyLazy' => $lazyLazy,
        'defer' => $__foreign['defer'],
        'force' => $__foreign['force']
    ];
})();
