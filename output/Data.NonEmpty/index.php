<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.FoldableWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.FunctorWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.TraversableWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable1/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_NonEmpty = (function ()  use (&$PS__Data_Unfoldable1, &$PS__Data_Function, &$PS__Data_Tuple, &$PS__Data_Functor, &$PS__Data_Unfoldable, &$PS__Data_Maybe, &$PS__Control_Plus, &$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Control_Alt, &$PS__Control_Applicative, &$PS__Data_FunctorWithIndex, &$PS__Control_Semigroupoid, &$PS__Data_Foldable, &$PS__Data_FoldableWithIndex, &$PS__Data_Traversable, &$PS__Control_Apply, &$PS__Data_TraversableWithIndex, &$PS__Data_Semigroup_Foldable, &$PS__Control_Category, &$PS__Data_Eq, &$PS__Data_HeytingAlgebra, &$PS__Data_Ord, &$PS__Data_Ordering) {
    $NonEmpty = [
        'constructor' => function ($value0, $value1) {
            return [
                'type' => 'NonEmpty',
                'value0' => $value0,
                'value1' => $value1
            ];
        },
        'create' => function ($value0)  use (&$NonEmpty) {
            return function ($value1)  use (&$NonEmpty, &$value0) {
                return $NonEmpty['constructor']($value0, $value1);
            };
        }
    ];
    $unfoldable1NonEmpty = function ($dictUnfoldable)  use (&$PS__Data_Unfoldable1, &$PS__Data_Function, &$PS__Data_Tuple, &$NonEmpty, &$PS__Data_Functor, &$PS__Data_Unfoldable, &$PS__Data_Maybe) {
        return $PS__Data_Unfoldable1['Unfoldable1'](function ($f)  use (&$PS__Data_Function, &$PS__Data_Tuple, &$NonEmpty, &$PS__Data_Functor, &$PS__Data_Unfoldable, &$dictUnfoldable, &$PS__Data_Maybe) {
            return function ($b)  use (&$PS__Data_Function, &$PS__Data_Tuple, &$NonEmpty, &$PS__Data_Functor, &$PS__Data_Unfoldable, &$dictUnfoldable, &$PS__Data_Maybe, &$f) {
                return $PS__Data_Function['apply']($PS__Data_Tuple['uncurry']($NonEmpty['create']))($PS__Data_Functor['map']($PS__Data_Tuple['functorTuple'])($PS__Data_Unfoldable['unfoldr']($dictUnfoldable)($PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])($f)))($f($b)));
            };
        });
    };
    $tail = function ($v) {
        return $v['value1'];
    };
    $singleton = function ($dictPlus)  use (&$NonEmpty, &$PS__Control_Plus) {
        return function ($a)  use (&$NonEmpty, &$PS__Control_Plus, &$dictPlus) {
            return $NonEmpty['constructor']($a, $PS__Control_Plus['empty']($dictPlus));
        };
    };
    $showNonEmpty = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return function ($dictShow1)  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$dictShow) {
            return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow, &$dictShow1) {
                return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(NonEmpty ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v['value0']))($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])(' ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow1)($v['value1']))(')'))));
            });
        };
    };
    $oneOf = function ($dictAlternative)  use (&$PS__Control_Alt, &$PS__Control_Applicative) {
        return function ($v)  use (&$PS__Control_Alt, &$dictAlternative, &$PS__Control_Applicative) {
            return $PS__Control_Alt['alt'](($dictAlternative['Plus1']())['Alt0']())($PS__Control_Applicative['pure']($dictAlternative['Applicative0']())($v['value0']))($v['value1']);
        };
    };
    $head = function ($v) {
        return $v['value0'];
    };
    $functorNonEmpty = function ($dictFunctor)  use (&$PS__Data_Functor, &$NonEmpty) {
        return $PS__Data_Functor['Functor'](function ($f)  use (&$NonEmpty, &$PS__Data_Functor, &$dictFunctor) {
            return function ($m)  use (&$NonEmpty, &$f, &$PS__Data_Functor, &$dictFunctor) {
                return $NonEmpty['constructor']($f($m['value0']), $PS__Data_Functor['map']($dictFunctor)($f)($m['value1']));
            };
        });
    };
    $functorWithIndex = function ($dictFunctorWithIndex)  use (&$PS__Data_FunctorWithIndex, &$functorNonEmpty, &$NonEmpty, &$PS__Data_Maybe, &$PS__Control_Semigroupoid) {
        return $PS__Data_FunctorWithIndex['FunctorWithIndex'](function ()  use (&$functorNonEmpty, &$dictFunctorWithIndex) {
            return $functorNonEmpty($dictFunctorWithIndex['Functor0']());
        }, function ($f)  use (&$NonEmpty, &$PS__Data_Maybe, &$PS__Data_FunctorWithIndex, &$dictFunctorWithIndex, &$PS__Control_Semigroupoid) {
            return function ($v)  use (&$NonEmpty, &$f, &$PS__Data_Maybe, &$PS__Data_FunctorWithIndex, &$dictFunctorWithIndex, &$PS__Control_Semigroupoid) {
                return $NonEmpty['constructor']($f($PS__Data_Maybe['Nothing']())($v['value0']), $PS__Data_FunctorWithIndex['mapWithIndex']($dictFunctorWithIndex)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($PS__Data_Maybe['Just']['create']))($v['value1']));
            };
        });
    };
    $fromNonEmpty = function ($f) {
        return function ($v)  use (&$f) {
            return $f($v['value0'])($v['value1']);
        };
    };
    $foldl1 = function ($dictFoldable)  use (&$PS__Data_Foldable) {
        return function ($f)  use (&$PS__Data_Foldable, &$dictFoldable) {
            return function ($v)  use (&$PS__Data_Foldable, &$dictFoldable, &$f) {
                return $PS__Data_Foldable['foldl']($dictFoldable)($f)($v['value0'])($v['value1']);
            };
        };
    };
    $foldableNonEmpty = function ($dictFoldable)  use (&$PS__Data_Foldable, &$PS__Data_Semigroup) {
        return $PS__Data_Foldable['Foldable'](function ($dictMonoid)  use (&$PS__Data_Semigroup, &$PS__Data_Foldable, &$dictFoldable) {
            return function ($f)  use (&$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Foldable, &$dictFoldable) {
                return function ($v)  use (&$PS__Data_Semigroup, &$dictMonoid, &$f, &$PS__Data_Foldable, &$dictFoldable) {
                    return $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($f($v['value0']))($PS__Data_Foldable['foldMap']($dictFoldable)($dictMonoid)($f)($v['value1']));
                };
            };
        }, function ($f)  use (&$PS__Data_Foldable, &$dictFoldable) {
            return function ($b)  use (&$PS__Data_Foldable, &$dictFoldable, &$f) {
                return function ($v)  use (&$PS__Data_Foldable, &$dictFoldable, &$f, &$b) {
                    return $PS__Data_Foldable['foldl']($dictFoldable)($f)($f($b)($v['value0']))($v['value1']);
                };
            };
        }, function ($f)  use (&$PS__Data_Foldable, &$dictFoldable) {
            return function ($b)  use (&$f, &$PS__Data_Foldable, &$dictFoldable) {
                return function ($v)  use (&$f, &$PS__Data_Foldable, &$dictFoldable, &$b) {
                    return $f($v['value0'])($PS__Data_Foldable['foldr']($dictFoldable)($f)($b)($v['value1']));
                };
            };
        });
    };
    $foldableWithIndexNonEmpty = function ($dictFoldableWithIndex)  use (&$PS__Data_FoldableWithIndex, &$foldableNonEmpty, &$PS__Data_Semigroup, &$PS__Data_Maybe, &$PS__Control_Semigroupoid) {
        return $PS__Data_FoldableWithIndex['FoldableWithIndex'](function ()  use (&$foldableNonEmpty, &$dictFoldableWithIndex) {
            return $foldableNonEmpty($dictFoldableWithIndex['Foldable0']());
        }, function ($dictMonoid)  use (&$PS__Data_Semigroup, &$PS__Data_Maybe, &$PS__Data_FoldableWithIndex, &$dictFoldableWithIndex, &$PS__Control_Semigroupoid) {
            return function ($f)  use (&$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Maybe, &$PS__Data_FoldableWithIndex, &$dictFoldableWithIndex, &$PS__Control_Semigroupoid) {
                return function ($v)  use (&$PS__Data_Semigroup, &$dictMonoid, &$f, &$PS__Data_Maybe, &$PS__Data_FoldableWithIndex, &$dictFoldableWithIndex, &$PS__Control_Semigroupoid) {
                    return $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($f($PS__Data_Maybe['Nothing']())($v['value0']))($PS__Data_FoldableWithIndex['foldMapWithIndex']($dictFoldableWithIndex)($dictMonoid)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($PS__Data_Maybe['Just']['create']))($v['value1']));
                };
            };
        }, function ($f)  use (&$PS__Data_FoldableWithIndex, &$dictFoldableWithIndex, &$PS__Control_Semigroupoid, &$PS__Data_Maybe) {
            return function ($b)  use (&$PS__Data_FoldableWithIndex, &$dictFoldableWithIndex, &$PS__Control_Semigroupoid, &$f, &$PS__Data_Maybe) {
                return function ($v)  use (&$PS__Data_FoldableWithIndex, &$dictFoldableWithIndex, &$PS__Control_Semigroupoid, &$f, &$PS__Data_Maybe, &$b) {
                    return $PS__Data_FoldableWithIndex['foldlWithIndex']($dictFoldableWithIndex)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($PS__Data_Maybe['Just']['create']))($f($PS__Data_Maybe['Nothing']())($b)($v['value0']))($v['value1']);
                };
            };
        }, function ($f)  use (&$PS__Data_Maybe, &$PS__Data_FoldableWithIndex, &$dictFoldableWithIndex, &$PS__Control_Semigroupoid) {
            return function ($b)  use (&$f, &$PS__Data_Maybe, &$PS__Data_FoldableWithIndex, &$dictFoldableWithIndex, &$PS__Control_Semigroupoid) {
                return function ($v)  use (&$f, &$PS__Data_Maybe, &$PS__Data_FoldableWithIndex, &$dictFoldableWithIndex, &$PS__Control_Semigroupoid, &$b) {
                    return $f($PS__Data_Maybe['Nothing']())($v['value0'])($PS__Data_FoldableWithIndex['foldrWithIndex']($dictFoldableWithIndex)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($PS__Data_Maybe['Just']['create']))($b)($v['value1']));
                };
            };
        });
    };
    $traversableNonEmpty = function ($dictTraversable)  use (&$PS__Data_Traversable, &$foldableNonEmpty, &$functorNonEmpty, &$PS__Control_Apply, &$PS__Data_Functor, &$NonEmpty) {
        return $PS__Data_Traversable['Traversable'](function ()  use (&$foldableNonEmpty, &$dictTraversable) {
            return $foldableNonEmpty($dictTraversable['Foldable1']());
        }, function ()  use (&$functorNonEmpty, &$dictTraversable) {
            return $functorNonEmpty($dictTraversable['Functor0']());
        }, function ($dictApplicative)  use (&$PS__Control_Apply, &$PS__Data_Functor, &$NonEmpty, &$PS__Data_Traversable, &$dictTraversable) {
            return function ($v)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$NonEmpty, &$PS__Data_Traversable, &$dictTraversable) {
                return $PS__Control_Apply['apply']($dictApplicative['Apply0']())($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($NonEmpty['create'])($v['value0']))($PS__Data_Traversable['sequence']($dictTraversable)($dictApplicative)($v['value1']));
            };
        }, function ($dictApplicative)  use (&$PS__Control_Apply, &$PS__Data_Functor, &$NonEmpty, &$PS__Data_Traversable, &$dictTraversable) {
            return function ($f)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$NonEmpty, &$PS__Data_Traversable, &$dictTraversable) {
                return function ($v)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$NonEmpty, &$f, &$PS__Data_Traversable, &$dictTraversable) {
                    return $PS__Control_Apply['apply']($dictApplicative['Apply0']())($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($NonEmpty['create'])($f($v['value0'])))($PS__Data_Traversable['traverse']($dictTraversable)($dictApplicative)($f)($v['value1']));
                };
            };
        });
    };
    $traversableWithIndexNonEmpty = function ($dictTraversableWithIndex)  use (&$PS__Data_TraversableWithIndex, &$foldableWithIndexNonEmpty, &$functorWithIndex, &$traversableNonEmpty, &$PS__Control_Apply, &$PS__Data_Functor, &$NonEmpty, &$PS__Data_Maybe, &$PS__Control_Semigroupoid) {
        return $PS__Data_TraversableWithIndex['TraversableWithIndex'](function ()  use (&$foldableWithIndexNonEmpty, &$dictTraversableWithIndex) {
            return $foldableWithIndexNonEmpty($dictTraversableWithIndex['FoldableWithIndex1']());
        }, function ()  use (&$functorWithIndex, &$dictTraversableWithIndex) {
            return $functorWithIndex($dictTraversableWithIndex['FunctorWithIndex0']());
        }, function ()  use (&$traversableNonEmpty, &$dictTraversableWithIndex) {
            return $traversableNonEmpty($dictTraversableWithIndex['Traversable2']());
        }, function ($dictApplicative)  use (&$PS__Control_Apply, &$PS__Data_Functor, &$NonEmpty, &$PS__Data_Maybe, &$PS__Data_TraversableWithIndex, &$dictTraversableWithIndex, &$PS__Control_Semigroupoid) {
            return function ($f)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$NonEmpty, &$PS__Data_Maybe, &$PS__Data_TraversableWithIndex, &$dictTraversableWithIndex, &$PS__Control_Semigroupoid) {
                return function ($v)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$NonEmpty, &$f, &$PS__Data_Maybe, &$PS__Data_TraversableWithIndex, &$dictTraversableWithIndex, &$PS__Control_Semigroupoid) {
                    return $PS__Control_Apply['apply']($dictApplicative['Apply0']())($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($NonEmpty['create'])($f($PS__Data_Maybe['Nothing']())($v['value0'])))($PS__Data_TraversableWithIndex['traverseWithIndex']($dictTraversableWithIndex)($dictApplicative)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($PS__Data_Maybe['Just']['create']))($v['value1']));
                };
            };
        });
    };
    $foldable1NonEmpty = function ($dictFoldable)  use (&$PS__Data_Semigroup_Foldable, &$foldableNonEmpty, &$foldable1NonEmpty, &$PS__Control_Category, &$PS__Data_Foldable, &$PS__Data_Semigroup) {
        return $PS__Data_Semigroup_Foldable['Foldable1'](function ()  use (&$foldableNonEmpty, &$dictFoldable) {
            return $foldableNonEmpty($dictFoldable);
        }, function ($dictSemigroup)  use (&$PS__Data_Semigroup_Foldable, &$foldable1NonEmpty, &$dictFoldable, &$PS__Control_Category) {
            return $PS__Data_Semigroup_Foldable['foldMap1']($foldable1NonEmpty($dictFoldable))($dictSemigroup)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        }, function ($dictSemigroup)  use (&$PS__Data_Foldable, &$dictFoldable, &$PS__Data_Semigroup) {
            return function ($f)  use (&$PS__Data_Foldable, &$dictFoldable, &$PS__Data_Semigroup, &$dictSemigroup) {
                return function ($v)  use (&$PS__Data_Foldable, &$dictFoldable, &$PS__Data_Semigroup, &$dictSemigroup, &$f) {
                    return $PS__Data_Foldable['foldl']($dictFoldable)(function ($s)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$f) {
                        return function ($a1)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$s, &$f) {
                            return $PS__Data_Semigroup['append']($dictSemigroup)($s)($f($a1));
                        };
                    })($f($v['value0']))($v['value1']);
                };
            };
        });
    };
    $eqNonEmpty = function ($dictEq1)  use (&$PS__Data_Eq, &$PS__Data_HeytingAlgebra) {
        return function ($dictEq)  use (&$PS__Data_Eq, &$PS__Data_HeytingAlgebra, &$dictEq1) {
            return $PS__Data_Eq['Eq'](function ($x)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$dictEq, &$dictEq1) {
                return function ($y)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$dictEq, &$x, &$dictEq1) {
                    return $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Eq['eq']($dictEq)($x['value0'])($y['value0']))($PS__Data_Eq['eq1']($dictEq1)($dictEq)($x['value1'])($y['value1']));
                };
            });
        };
    };
    $ordNonEmpty = function ($dictOrd1)  use (&$PS__Data_Ord, &$eqNonEmpty, &$PS__Data_Ordering) {
        return function ($dictOrd)  use (&$PS__Data_Ord, &$eqNonEmpty, &$dictOrd1, &$PS__Data_Ordering) {
            return $PS__Data_Ord['Ord'](function ()  use (&$eqNonEmpty, &$dictOrd1, &$dictOrd) {
                return $eqNonEmpty($dictOrd1['Eq10']())($dictOrd['Eq0']());
            }, function ($x)  use (&$PS__Data_Ord, &$dictOrd, &$PS__Data_Ordering, &$dictOrd1) {
                return function ($y)  use (&$PS__Data_Ord, &$dictOrd, &$x, &$PS__Data_Ordering, &$dictOrd1) {
                    $v = $PS__Data_Ord['compare']($dictOrd)($x['value0'])($y['value0']);
                    if ($v['type'] === 'LT') {
                        return $PS__Data_Ordering['LT']();
                    };
                    if ($v['type'] === 'GT') {
                        return $PS__Data_Ordering['GT']();
                    };
                    return $PS__Data_Ord['compare1']($dictOrd1)($dictOrd)($x['value1'])($y['value1']);
                };
            });
        };
    };
    $eq1NonEmpty = function ($dictEq1)  use (&$PS__Data_Eq, &$eqNonEmpty) {
        return $PS__Data_Eq['Eq1'](function ($dictEq)  use (&$PS__Data_Eq, &$eqNonEmpty, &$dictEq1) {
            return $PS__Data_Eq['eq']($eqNonEmpty($dictEq1)($dictEq));
        });
    };
    $ord1NonEmpty = function ($dictOrd1)  use (&$PS__Data_Ord, &$eq1NonEmpty, &$ordNonEmpty) {
        return $PS__Data_Ord['Ord1'](function ()  use (&$eq1NonEmpty, &$dictOrd1) {
            return $eq1NonEmpty($dictOrd1['Eq10']());
        }, function ($dictOrd)  use (&$PS__Data_Ord, &$ordNonEmpty, &$dictOrd1) {
            return $PS__Data_Ord['compare']($ordNonEmpty($dictOrd1)($dictOrd));
        });
    };
    return [
        'NonEmpty' => $NonEmpty,
        'singleton' => $singleton,
        'foldl1' => $foldl1,
        'fromNonEmpty' => $fromNonEmpty,
        'oneOf' => $oneOf,
        'head' => $head,
        'tail' => $tail,
        'showNonEmpty' => $showNonEmpty,
        'eqNonEmpty' => $eqNonEmpty,
        'eq1NonEmpty' => $eq1NonEmpty,
        'ordNonEmpty' => $ordNonEmpty,
        'ord1NonEmpty' => $ord1NonEmpty,
        'functorNonEmpty' => $functorNonEmpty,
        'functorWithIndex' => $functorWithIndex,
        'foldableNonEmpty' => $foldableNonEmpty,
        'foldableWithIndexNonEmpty' => $foldableWithIndexNonEmpty,
        'traversableNonEmpty' => $traversableNonEmpty,
        'traversableWithIndexNonEmpty' => $traversableWithIndexNonEmpty,
        'foldable1NonEmpty' => $foldable1NonEmpty,
        'unfoldable1NonEmpty' => $unfoldable1NonEmpty
    ];
})();
