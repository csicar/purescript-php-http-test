<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Dual/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Multiplicative/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Semigroup_Traversable = (function ()  use (&$PS__Control_Category, &$PS__Data_Semigroup_Foldable, &$PS__Data_Traversable, &$PS__Data_Functor, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Multiplicative) {
    $Traversable1 = function ($Foldable10, $Traversable1, $sequence1, $traverse1) {
        return [
            'Foldable10' => $Foldable10,
            'Traversable1' => $Traversable1,
            'sequence1' => $sequence1,
            'traverse1' => $traverse1
        ];
    };
    $traverse1 = function ($dict) {
        return $dict['traverse1'];
    };
    $sequence1Default = function ($dictTraversable1)  use (&$traverse1, &$PS__Control_Category) {
        return function ($dictApply)  use (&$traverse1, &$dictTraversable1, &$PS__Control_Category) {
            return $traverse1($dictTraversable1)($dictApply)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        };
    };
    $traversableDual = $Traversable1(function ()  use (&$PS__Data_Semigroup_Foldable) {
        return $PS__Data_Semigroup_Foldable['foldableDual'];
    }, function ()  use (&$PS__Data_Traversable) {
        return $PS__Data_Traversable['traversableDual'];
    }, function ($dictApply)  use (&$sequence1Default, &$traversableDual) {
        return $sequence1Default($traversableDual)($dictApply);
    }, function ($dictApply)  use (&$PS__Data_Functor, &$PS__Data_Monoid_Dual) {
        return function ($f)  use (&$PS__Data_Functor, &$dictApply, &$PS__Data_Monoid_Dual) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApply, &$PS__Data_Monoid_Dual, &$f) {
                return $PS__Data_Functor['map']($dictApply['Functor0']())($PS__Data_Monoid_Dual['Dual'])($f($v));
            };
        };
    });
    $traversableMultiplicative = $Traversable1(function ()  use (&$PS__Data_Semigroup_Foldable) {
        return $PS__Data_Semigroup_Foldable['foldableMultiplicative'];
    }, function ()  use (&$PS__Data_Traversable) {
        return $PS__Data_Traversable['traversableMultiplicative'];
    }, function ($dictApply)  use (&$sequence1Default, &$traversableMultiplicative) {
        return $sequence1Default($traversableMultiplicative)($dictApply);
    }, function ($dictApply)  use (&$PS__Data_Functor, &$PS__Data_Monoid_Multiplicative) {
        return function ($f)  use (&$PS__Data_Functor, &$dictApply, &$PS__Data_Monoid_Multiplicative) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApply, &$PS__Data_Monoid_Multiplicative, &$f) {
                return $PS__Data_Functor['map']($dictApply['Functor0']())($PS__Data_Monoid_Multiplicative['Multiplicative'])($f($v));
            };
        };
    });
    $sequence1 = function ($dict) {
        return $dict['sequence1'];
    };
    $traverse1Default = function ($dictTraversable1)  use (&$sequence1, &$PS__Data_Functor) {
        return function ($dictApply)  use (&$sequence1, &$dictTraversable1, &$PS__Data_Functor) {
            return function ($f)  use (&$sequence1, &$dictTraversable1, &$dictApply, &$PS__Data_Functor) {
                return function ($ta)  use (&$sequence1, &$dictTraversable1, &$dictApply, &$PS__Data_Functor, &$f) {
                    return $sequence1($dictTraversable1)($dictApply)($PS__Data_Functor['map'](($dictTraversable1['Traversable1']())['Functor0']())($f)($ta));
                };
            };
        };
    };
    return [
        'sequence1' => $sequence1,
        'traverse1' => $traverse1,
        'Traversable1' => $Traversable1,
        'traverse1Default' => $traverse1Default,
        'sequence1Default' => $sequence1Default,
        'traversableDual' => $traversableDual,
        'traversableMultiplicative' => $traversableMultiplicative
    ];
})();
