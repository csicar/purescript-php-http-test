<?php
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadPlus/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadZero/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Boolean/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Either/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable1/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Partial.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Enum = (function ()  use (&$PS__Data_Unfoldable1, &$PS__Control_Apply, &$PS__Data_Tuple, &$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Ord, &$PS__Data_Newtype, &$PS__Data_Bounded, &$PS__Data_Eq, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$PS__Data_Ordering, &$PS__Data_Ring, &$PS__Data_Semiring, &$PS__Control_Bind, &$PS__Control_MonadZero, &$PS__Data_Boolean, &$PS__Partial_Unsafe, &$PS__Data_Unfoldable, &$PS__Data_Either, &$PS__Data_HeytingAlgebra, &$PS__Data_Unit) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Cardinality = function ($x) {
        return $x;
    };
    $Enum = function ($Ord0, $pred, $succ) {
        return [
            'Ord0' => $Ord0,
            'pred' => $pred,
            'succ' => $succ
        ];
    };
    $BoundedEnum = function ($Bounded0, $Enum1, $cardinality, $fromEnum, $toEnum) {
        return [
            'Bounded0' => $Bounded0,
            'Enum1' => $Enum1,
            'cardinality' => $cardinality,
            'fromEnum' => $fromEnum,
            'toEnum' => $toEnum
        ];
    };
    $toEnum = function ($dict) {
        return $dict['toEnum'];
    };
    $succ = function ($dict) {
        return $dict['succ'];
    };
    $upFromIncluding = function ($dictEnum)  use (&$PS__Data_Unfoldable1, &$PS__Control_Apply, &$PS__Data_Tuple, &$succ) {
        return function ($dictUnfoldable1)  use (&$PS__Data_Unfoldable1, &$PS__Control_Apply, &$PS__Data_Tuple, &$succ, &$dictEnum) {
            return $PS__Data_Unfoldable1['unfoldr1']($dictUnfoldable1)($PS__Control_Apply['apply']($PS__Control_Apply['applyFn'])($PS__Data_Tuple['Tuple']['create'])($succ($dictEnum)));
        };
    };
    $showCardinality = $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show) {
        return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Cardinality ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Show['showInt'])($v))(')'));
    });
    $pred = function ($dict) {
        return $dict['pred'];
    };
    $ordCardinality = $PS__Data_Ord['ordInt'];
    $newtypeCardinality = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Cardinality);
    $fromEnum = function ($dict) {
        return $dict['fromEnum'];
    };
    $toEnumWithDefaults = function ($dictBoundedEnum)  use (&$toEnum, &$PS__Data_Ord, &$fromEnum, &$PS__Data_Bounded) {
        return function ($low)  use (&$toEnum, &$dictBoundedEnum, &$PS__Data_Ord, &$fromEnum, &$PS__Data_Bounded) {
            return function ($high)  use (&$toEnum, &$dictBoundedEnum, &$PS__Data_Ord, &$fromEnum, &$PS__Data_Bounded, &$low) {
                return function ($x)  use (&$toEnum, &$dictBoundedEnum, &$PS__Data_Ord, &$fromEnum, &$PS__Data_Bounded, &$low, &$high) {
                    $v = $toEnum($dictBoundedEnum)($x);
                    if ($v['type'] === 'Just') {
                        return $v['value0'];
                    };
                    if ($v['type'] === 'Nothing') {
                        $__local_var__51 = $PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($x)($fromEnum($dictBoundedEnum)($PS__Data_Bounded['bottom']($dictBoundedEnum['Bounded0']())));
                        if ($__local_var__51) {
                            return $low;
                        };
                        return $high;
                    };
                    throw new \Exception('Failed pattern match at Data.Enum line 158, column 33 - line 160, column 62: ' . var_dump([ $v['constructor']['name'] ]));
                };
            };
        };
    };
    $eqCardinality = $PS__Data_Eq['eqInt'];
    $enumUnit = $Enum(function ()  use (&$PS__Data_Ord) {
        return $PS__Data_Ord['ordUnit'];
    }, $PS__Data_Function['const']($PS__Data_Maybe['Nothing']()), $PS__Data_Function['const']($PS__Data_Maybe['Nothing']()));
    $enumTuple = function ($dictEnum)  use (&$Enum, &$PS__Data_Tuple, &$PS__Data_Maybe, &$PS__Data_Functor, &$PS__Data_Function, &$PS__Data_Bounded, &$pred, &$PS__Control_Semigroupoid, &$succ) {
        return function ($dictBoundedEnum)  use (&$Enum, &$PS__Data_Tuple, &$dictEnum, &$PS__Data_Maybe, &$PS__Data_Functor, &$PS__Data_Function, &$PS__Data_Bounded, &$pred, &$PS__Control_Semigroupoid, &$succ) {
            return $Enum(function ()  use (&$PS__Data_Tuple, &$dictEnum, &$dictBoundedEnum) {
                return $PS__Data_Tuple['ordTuple']($dictEnum['Ord0']())(($dictBoundedEnum['Enum1']())['Ord0']());
            }, function ($v)  use (&$PS__Data_Maybe, &$PS__Data_Functor, &$PS__Data_Function, &$PS__Data_Tuple, &$PS__Data_Bounded, &$dictBoundedEnum, &$pred, &$dictEnum, &$PS__Control_Semigroupoid) {
                return $PS__Data_Maybe['maybe']($PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])($PS__Data_Function['flip']($PS__Data_Tuple['Tuple']['create'])($PS__Data_Bounded['top']($dictBoundedEnum['Bounded0']())))($pred($dictEnum)($v['value0'])))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['Just']['create'])($PS__Data_Tuple['Tuple']['create']($v['value0'])))($pred($dictBoundedEnum['Enum1']())($v['value1']));
            }, function ($v)  use (&$PS__Data_Maybe, &$PS__Data_Functor, &$PS__Data_Function, &$PS__Data_Tuple, &$PS__Data_Bounded, &$dictBoundedEnum, &$succ, &$dictEnum, &$PS__Control_Semigroupoid) {
                return $PS__Data_Maybe['maybe']($PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])($PS__Data_Function['flip']($PS__Data_Tuple['Tuple']['create'])($PS__Data_Bounded['bottom']($dictBoundedEnum['Bounded0']())))($succ($dictEnum)($v['value0'])))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['Just']['create'])($PS__Data_Tuple['Tuple']['create']($v['value0'])))($succ($dictBoundedEnum['Enum1']())($v['value1']));
            });
        };
    };
    $enumOrdering = $Enum(function ()  use (&$PS__Data_Ord) {
        return $PS__Data_Ord['ordOrdering'];
    }, function ($v)  use (&$PS__Data_Maybe, &$PS__Data_Ordering) {
        if ($v['type'] === 'LT') {
            return $PS__Data_Maybe['Nothing']();
        };
        if ($v['type'] === 'EQ') {
            return $PS__Data_Maybe['Just']['constructor']($PS__Data_Ordering['LT']());
        };
        if ($v['type'] === 'GT') {
            return $PS__Data_Maybe['Just']['constructor']($PS__Data_Ordering['EQ']());
        };
        throw new \Exception('Failed pattern match at Data.Enum line 72, column 1 - line 72, column 39: ' . var_dump([ $v['constructor']['name'] ]));
    }, function ($v)  use (&$PS__Data_Maybe, &$PS__Data_Ordering) {
        if ($v['type'] === 'LT') {
            return $PS__Data_Maybe['Just']['constructor']($PS__Data_Ordering['EQ']());
        };
        if ($v['type'] === 'EQ') {
            return $PS__Data_Maybe['Just']['constructor']($PS__Data_Ordering['GT']());
        };
        if ($v['type'] === 'GT') {
            return $PS__Data_Maybe['Nothing']();
        };
        throw new \Exception('Failed pattern match at Data.Enum line 72, column 1 - line 72, column 39: ' . var_dump([ $v['constructor']['name'] ]));
    });
    $enumMaybe = function ($dictBoundedEnum)  use (&$Enum, &$PS__Data_Maybe, &$pred, &$PS__Data_Bounded, &$PS__Data_Functor, &$succ) {
        return $Enum(function ()  use (&$PS__Data_Maybe, &$dictBoundedEnum) {
            return $PS__Data_Maybe['ordMaybe'](($dictBoundedEnum['Enum1']())['Ord0']());
        }, function ($v)  use (&$PS__Data_Maybe, &$pred, &$dictBoundedEnum) {
            if ($v['type'] === 'Nothing') {
                return $PS__Data_Maybe['Nothing']();
            };
            if ($v['type'] === 'Just') {
                return $PS__Data_Maybe['Just']['constructor']($pred($dictBoundedEnum['Enum1']())($v['value0']));
            };
            throw new \Exception('Failed pattern match at Data.Enum line 80, column 1 - line 80, column 54: ' . var_dump([ $v['constructor']['name'] ]));
        }, function ($v)  use (&$PS__Data_Maybe, &$PS__Data_Bounded, &$dictBoundedEnum, &$PS__Data_Functor, &$succ) {
            if ($v['type'] === 'Nothing') {
                return $PS__Data_Maybe['Just']['constructor']($PS__Data_Maybe['Just']['constructor']($PS__Data_Bounded['bottom']($dictBoundedEnum['Bounded0']())));
            };
            if ($v['type'] === 'Just') {
                return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])($PS__Data_Maybe['Just']['create'])($succ($dictBoundedEnum['Enum1']())($v['value0']));
            };
            throw new \Exception('Failed pattern match at Data.Enum line 80, column 1 - line 80, column 54: ' . var_dump([ $v['constructor']['name'] ]));
        });
    };
    $enumInt = $Enum(function ()  use (&$PS__Data_Ord) {
        return $PS__Data_Ord['ordInt'];
    }, function ($n)  use (&$PS__Data_Ord, &$PS__Data_Bounded, &$PS__Data_Maybe, &$PS__Data_Ring) {
        $__local_var__64 = $PS__Data_Ord['greaterThan']($PS__Data_Ord['ordInt'])($n)($PS__Data_Bounded['bottom']($PS__Data_Bounded['boundedInt']));
        if ($__local_var__64) {
            return $PS__Data_Maybe['Just']['constructor']($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($n)(1));
        };
        return $PS__Data_Maybe['Nothing']();
    }, function ($n)  use (&$PS__Data_Ord, &$PS__Data_Bounded, &$PS__Data_Maybe, &$PS__Data_Semiring) {
        $__local_var__65 = $PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($n)($PS__Data_Bounded['top']($PS__Data_Bounded['boundedInt']));
        if ($__local_var__65) {
            return $PS__Data_Maybe['Just']['constructor']($PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($n)(1));
        };
        return $PS__Data_Maybe['Nothing']();
    });
    $enumFromTo = function ($dictEnum)  use (&$PS__Data_Tuple, &$PS__Control_Bind, &$PS__Data_Maybe, &$PS__Data_Functor, &$PS__Control_MonadZero, &$PS__Data_Eq, &$PS__Data_Unfoldable1, &$PS__Data_Ord, &$succ, &$PS__Data_Boolean, &$pred) {
        return function ($dictUnfoldable1)  use (&$PS__Data_Tuple, &$PS__Control_Bind, &$PS__Data_Maybe, &$PS__Data_Functor, &$PS__Control_MonadZero, &$PS__Data_Eq, &$dictEnum, &$PS__Data_Unfoldable1, &$PS__Data_Ord, &$succ, &$PS__Data_Boolean, &$pred) {
            $go = function ($step)  use (&$PS__Data_Tuple, &$PS__Control_Bind, &$PS__Data_Maybe, &$PS__Data_Functor, &$PS__Control_MonadZero) {
                return function ($op)  use (&$PS__Data_Tuple, &$PS__Control_Bind, &$PS__Data_Maybe, &$step, &$PS__Data_Functor, &$PS__Control_MonadZero) {
                    return function ($to)  use (&$PS__Data_Tuple, &$PS__Control_Bind, &$PS__Data_Maybe, &$step, &$PS__Data_Functor, &$PS__Control_MonadZero, &$op) {
                        return function ($a)  use (&$PS__Data_Tuple, &$PS__Control_Bind, &$PS__Data_Maybe, &$step, &$PS__Data_Functor, &$PS__Control_MonadZero, &$op, &$to) {
                            return $PS__Data_Tuple['Tuple']['constructor']($a, $PS__Control_Bind['bind']($PS__Data_Maybe['bindMaybe'])($step($a))(function ($a__prime)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Control_MonadZero, &$op, &$to) {
                                return $PS__Data_Functor['voidLeft']($PS__Data_Maybe['functorMaybe'])($PS__Control_MonadZero['guard']($PS__Data_Maybe['monadZeroMaybe'])($op($a__prime)($to)))($a__prime);
                            }));
                        };
                    };
                };
            };
            return function ($v)  use (&$PS__Data_Eq, &$dictEnum, &$PS__Data_Unfoldable1, &$dictUnfoldable1, &$PS__Data_Ord, &$go, &$succ, &$PS__Data_Boolean, &$pred) {
                return function ($v1)  use (&$PS__Data_Eq, &$dictEnum, &$v, &$PS__Data_Unfoldable1, &$dictUnfoldable1, &$PS__Data_Ord, &$go, &$succ, &$PS__Data_Boolean, &$pred) {
                    if ($PS__Data_Eq['eq'](($dictEnum['Ord0']())['Eq0']())($v)($v1)) {
                        return $PS__Data_Unfoldable1['singleton']($dictUnfoldable1)($v);
                    };
                    if ($PS__Data_Ord['lessThan']($dictEnum['Ord0']())($v)($v1)) {
                        return $PS__Data_Unfoldable1['unfoldr1']($dictUnfoldable1)($go($succ($dictEnum))($PS__Data_Ord['lessThanOrEq']($dictEnum['Ord0']()))($v1))($v);
                    };
                    if ($PS__Data_Boolean['otherwise']) {
                        return $PS__Data_Unfoldable1['unfoldr1']($dictUnfoldable1)($go($pred($dictEnum))($PS__Data_Ord['greaterThanOrEq']($dictEnum['Ord0']()))($v1))($v);
                    };
                    throw new \Exception('Failed pattern match at Data.Enum line 183, column 14 - line 187, column 51: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
                };
            };
        };
    };
    $enumFromThenTo = function ($dictUnfoldable)  use (&$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Semiring, &$PS__Data_Boolean, &$PS__Partial_Unsafe, &$fromEnum, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$toEnum, &$PS__Data_Unfoldable, &$PS__Data_Ring) {
        return function ($dictFunctor)  use (&$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Semiring, &$PS__Data_Boolean, &$PS__Partial_Unsafe, &$fromEnum, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$toEnum, &$PS__Data_Unfoldable, &$dictUnfoldable, &$PS__Data_Ring) {
            return function ($dictBoundedEnum)  use (&$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Semiring, &$PS__Data_Boolean, &$PS__Partial_Unsafe, &$fromEnum, &$PS__Data_Functor, &$dictFunctor, &$PS__Control_Semigroupoid, &$toEnum, &$PS__Data_Unfoldable, &$dictUnfoldable, &$PS__Data_Ring) {
                $go = function ($step)  use (&$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Semiring, &$PS__Data_Boolean) {
                    return function ($to)  use (&$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Semiring, &$step, &$PS__Data_Boolean) {
                        return function ($e)  use (&$PS__Data_Ord, &$to, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Semiring, &$step, &$PS__Data_Boolean) {
                            if ($PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])($e)($to)) {
                                return $PS__Data_Maybe['Just']['constructor']($PS__Data_Tuple['Tuple']['constructor']($e, $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($e)($step)));
                            };
                            if ($PS__Data_Boolean['otherwise']) {
                                return $PS__Data_Maybe['Nothing']();
                            };
                            throw new \Exception('Failed pattern match at Data.Enum line 214, column 5 - line 216, column 28: ' . var_dump([ $step['constructor']['name'], $to['constructor']['name'], $e['constructor']['name'] ]));
                        };
                    };
                };
                return $PS__Partial_Unsafe['unsafePartial'](function ($dictPartial)  use (&$fromEnum, &$dictBoundedEnum, &$PS__Data_Functor, &$dictFunctor, &$PS__Control_Semigroupoid, &$toEnum, &$PS__Data_Maybe, &$PS__Data_Unfoldable, &$dictUnfoldable, &$go, &$PS__Data_Ring) {
                    return function ($a)  use (&$fromEnum, &$dictBoundedEnum, &$PS__Data_Functor, &$dictFunctor, &$PS__Control_Semigroupoid, &$toEnum, &$PS__Data_Maybe, &$dictPartial, &$PS__Data_Unfoldable, &$dictUnfoldable, &$go, &$PS__Data_Ring) {
                        return function ($b)  use (&$fromEnum, &$dictBoundedEnum, &$a, &$PS__Data_Functor, &$dictFunctor, &$PS__Control_Semigroupoid, &$toEnum, &$PS__Data_Maybe, &$dictPartial, &$PS__Data_Unfoldable, &$dictUnfoldable, &$go, &$PS__Data_Ring) {
                            return function ($c)  use (&$fromEnum, &$dictBoundedEnum, &$b, &$a, &$PS__Data_Functor, &$dictFunctor, &$PS__Control_Semigroupoid, &$toEnum, &$PS__Data_Maybe, &$dictPartial, &$PS__Data_Unfoldable, &$dictUnfoldable, &$go, &$PS__Data_Ring) {
                                $c__prime = $fromEnum($dictBoundedEnum)($c);
                                $b__prime = $fromEnum($dictBoundedEnum)($b);
                                $a__prime = $fromEnum($dictBoundedEnum)($a);
                                return $PS__Data_Functor['map']($dictFunctor)($PS__Control_Semigroupoid['composeFlipped']($PS__Control_Semigroupoid['semigroupoidFn'])($toEnum($dictBoundedEnum))($PS__Data_Maybe['fromJust']($dictPartial)))($PS__Data_Unfoldable['unfoldr']($dictUnfoldable)($go($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($b__prime)($a__prime))($c__prime))($a__prime));
                            };
                        };
                    };
                });
            };
        };
    };
    $enumEither = function ($dictBoundedEnum)  use (&$Enum, &$PS__Data_Either, &$PS__Data_Maybe, &$PS__Control_Semigroupoid, &$pred, &$PS__Data_Bounded, &$succ) {
        return function ($dictBoundedEnum1)  use (&$Enum, &$PS__Data_Either, &$dictBoundedEnum, &$PS__Data_Maybe, &$PS__Control_Semigroupoid, &$pred, &$PS__Data_Bounded, &$succ) {
            return $Enum(function ()  use (&$PS__Data_Either, &$dictBoundedEnum, &$dictBoundedEnum1) {
                return $PS__Data_Either['ordEither'](($dictBoundedEnum['Enum1']())['Ord0']())(($dictBoundedEnum1['Enum1']())['Ord0']());
            }, function ($v)  use (&$PS__Data_Maybe, &$PS__Control_Semigroupoid, &$PS__Data_Either, &$pred, &$dictBoundedEnum, &$PS__Data_Bounded, &$dictBoundedEnum1) {
                if ($v['type'] === 'Left') {
                    return $PS__Data_Maybe['maybe']($PS__Data_Maybe['Nothing']())($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['Just']['create'])($PS__Data_Either['Left']['create']))($pred($dictBoundedEnum['Enum1']())($v['value0']));
                };
                if ($v['type'] === 'Right') {
                    return $PS__Data_Maybe['maybe']($PS__Data_Maybe['Just']['constructor']($PS__Data_Either['Left']['constructor']($PS__Data_Bounded['top']($dictBoundedEnum['Bounded0']()))))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['Just']['create'])($PS__Data_Either['Right']['create']))($pred($dictBoundedEnum1['Enum1']())($v['value0']));
                };
                throw new \Exception('Failed pattern match at Data.Enum line 86, column 1 - line 86, column 75: ' . var_dump([ $v['constructor']['name'] ]));
            }, function ($v)  use (&$PS__Data_Maybe, &$PS__Data_Either, &$PS__Data_Bounded, &$dictBoundedEnum1, &$PS__Control_Semigroupoid, &$succ, &$dictBoundedEnum) {
                if ($v['type'] === 'Left') {
                    return $PS__Data_Maybe['maybe']($PS__Data_Maybe['Just']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Bounded['bottom']($dictBoundedEnum1['Bounded0']()))))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['Just']['create'])($PS__Data_Either['Left']['create']))($succ($dictBoundedEnum['Enum1']())($v['value0']));
                };
                if ($v['type'] === 'Right') {
                    return $PS__Data_Maybe['maybe']($PS__Data_Maybe['Nothing']())($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['Just']['create'])($PS__Data_Either['Right']['create']))($succ($dictBoundedEnum1['Enum1']())($v['value0']));
                };
                throw new \Exception('Failed pattern match at Data.Enum line 86, column 1 - line 86, column 75: ' . var_dump([ $v['constructor']['name'] ]));
            });
        };
    };
    $enumBoolean = $Enum(function ()  use (&$PS__Data_Ord) {
        return $PS__Data_Ord['ordBoolean'];
    }, function ($v)  use (&$PS__Data_Maybe) {
        if ($v) {
            return $PS__Data_Maybe['Just']['constructor'](false);
        };
        return $PS__Data_Maybe['Nothing']();
    }, function ($v)  use (&$PS__Data_Maybe) {
        if (!$v) {
            return $PS__Data_Maybe['Just']['constructor'](true);
        };
        return $PS__Data_Maybe['Nothing']();
    });
    $downFromIncluding = function ($dictEnum)  use (&$PS__Data_Unfoldable1, &$PS__Control_Apply, &$PS__Data_Tuple, &$pred) {
        return function ($dictUnfoldable1)  use (&$PS__Data_Unfoldable1, &$PS__Control_Apply, &$PS__Data_Tuple, &$pred, &$dictEnum) {
            return $PS__Data_Unfoldable1['unfoldr1']($dictUnfoldable1)($PS__Control_Apply['apply']($PS__Control_Apply['applyFn'])($PS__Data_Tuple['Tuple']['create'])($pred($dictEnum)));
        };
    };
    $diag = function ($a)  use (&$PS__Data_Tuple) {
        return $PS__Data_Tuple['Tuple']['constructor']($a, $a);
    };
    $downFrom = function ($dictEnum)  use (&$PS__Data_Unfoldable, &$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_Maybe, &$diag, &$pred) {
        return function ($dictUnfoldable)  use (&$PS__Data_Unfoldable, &$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_Maybe, &$diag, &$pred, &$dictEnum) {
            return $PS__Data_Unfoldable['unfoldr']($dictUnfoldable)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])($diag))($pred($dictEnum)));
        };
    };
    $upFrom = function ($dictEnum)  use (&$PS__Data_Unfoldable, &$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_Maybe, &$diag, &$succ) {
        return function ($dictUnfoldable)  use (&$PS__Data_Unfoldable, &$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_Maybe, &$diag, &$succ, &$dictEnum) {
            return $PS__Data_Unfoldable['unfoldr']($dictUnfoldable)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])($diag))($succ($dictEnum)));
        };
    };
    $defaultToEnum = function ($dictBounded)  use (&$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Eq, &$PS__Data_Bounded, &$PS__Data_Boolean, &$PS__Control_Bind, &$defaultToEnum, &$PS__Data_Ring, &$succ) {
        return function ($dictEnum)  use (&$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Eq, &$PS__Data_Bounded, &$dictBounded, &$PS__Data_Boolean, &$PS__Control_Bind, &$defaultToEnum, &$PS__Data_Ring, &$succ) {
            return function ($n)  use (&$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Eq, &$PS__Data_Bounded, &$dictBounded, &$PS__Data_Boolean, &$PS__Control_Bind, &$defaultToEnum, &$dictEnum, &$PS__Data_Ring, &$succ) {
                if ($PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($n)(0)) {
                    return $PS__Data_Maybe['Nothing']();
                };
                if ($PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($n)(0)) {
                    return $PS__Data_Maybe['Just']['constructor']($PS__Data_Bounded['bottom']($dictBounded));
                };
                if ($PS__Data_Boolean['otherwise']) {
                    return $PS__Control_Bind['bind']($PS__Data_Maybe['bindMaybe'])($defaultToEnum($dictBounded)($dictEnum)($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($n)(1)))($succ($dictEnum));
                };
                throw new \Exception('Failed pattern match at Data.Enum line 281, column 1 - line 281, column 65: ' . var_dump([ $n['constructor']['name'] ]));
            };
        };
    };
    $defaultSucc = function ($toEnum__prime)  use (&$PS__Data_Semiring) {
        return function ($fromEnum__prime)  use (&$toEnum__prime, &$PS__Data_Semiring) {
            return function ($a)  use (&$toEnum__prime, &$PS__Data_Semiring, &$fromEnum__prime) {
                return $toEnum__prime($PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($fromEnum__prime($a))(1));
            };
        };
    };
    $defaultPred = function ($toEnum__prime)  use (&$PS__Data_Ring) {
        return function ($fromEnum__prime)  use (&$toEnum__prime, &$PS__Data_Ring) {
            return function ($a)  use (&$toEnum__prime, &$PS__Data_Ring, &$fromEnum__prime) {
                return $toEnum__prime($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($fromEnum__prime($a))(1));
            };
        };
    };
    $defaultFromEnum = function ($dictEnum)  use (&$PS__Control_Semigroupoid, &$PS__Data_Maybe, &$PS__Data_Semiring, &$defaultFromEnum, &$pred) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['maybe'](0)(function ($prd)  use (&$PS__Data_Semiring, &$defaultFromEnum, &$dictEnum) {
            return $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($defaultFromEnum($dictEnum)($prd))(1);
        }))($pred($dictEnum));
    };
    $defaultCardinality = function ($dictBounded)  use (&$PS__Control_Semigroupoid, &$PS__Data_Maybe, &$PS__Data_Semiring, &$succ, &$PS__Data_Function, &$Cardinality, &$PS__Data_Bounded) {
        return function ($dictEnum)  use (&$PS__Control_Semigroupoid, &$PS__Data_Maybe, &$PS__Data_Semiring, &$succ, &$PS__Data_Function, &$Cardinality, &$PS__Data_Bounded, &$dictBounded) {
            $defaultCardinality__prime = function ($i)  use (&$PS__Control_Semigroupoid, &$PS__Data_Maybe, &$defaultCardinality__prime, &$PS__Data_Semiring, &$succ, &$dictEnum) {
                return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['maybe']($i)($defaultCardinality__prime($PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($i)(1))))($succ($dictEnum));
            };
            return $PS__Data_Function['apply']($Cardinality)($defaultCardinality__prime(1)($PS__Data_Bounded['bottom']($dictBounded)));
        };
    };
    $charToEnum = function ($v)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Ord, &$PS__Data_Bounded, &$PS__Data_Maybe, &$__foreign) {
        if ($PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Ord['greaterThanOrEq']($PS__Data_Ord['ordInt'])($v)($PS__Data_Bounded['bottom']($PS__Data_Bounded['boundedInt'])))($PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])($v)($PS__Data_Bounded['top']($PS__Data_Bounded['boundedInt'])))) {
            return $PS__Data_Maybe['Just']['constructor']($__foreign['fromCharCode']($v));
        };
        return $PS__Data_Maybe['Nothing']();
    };
    $enumChar = $Enum(function ()  use (&$PS__Data_Ord) {
        return $PS__Data_Ord['ordChar'];
    }, $defaultPred($charToEnum)($__foreign['toCharCode']), $defaultSucc($charToEnum)($__foreign['toCharCode']));
    $cardinality = function ($dict) {
        return $dict['cardinality'];
    };
    $boundedEnumUnit = $BoundedEnum(function ()  use (&$PS__Data_Bounded) {
        return $PS__Data_Bounded['boundedUnit'];
    }, function ()  use (&$enumUnit) {
        return $enumUnit;
    }, 1, $PS__Data_Function['const'](0), function ($v)  use (&$PS__Data_Maybe, &$PS__Data_Unit) {
        if ($v === 0) {
            return $PS__Data_Maybe['Just']['constructor']($PS__Data_Unit['unit']);
        };
        return $PS__Data_Maybe['Nothing']();
    });
    $boundedEnumOrdering = $BoundedEnum(function ()  use (&$PS__Data_Bounded) {
        return $PS__Data_Bounded['boundedOrdering'];
    }, function ()  use (&$enumOrdering) {
        return $enumOrdering;
    }, 3, function ($v) {
        if ($v['type'] === 'LT') {
            return 0;
        };
        if ($v['type'] === 'EQ') {
            return 1;
        };
        if ($v['type'] === 'GT') {
            return 2;
        };
        throw new \Exception('Failed pattern match at Data.Enum line 137, column 1 - line 137, column 53: ' . var_dump([ $v['constructor']['name'] ]));
    }, function ($v)  use (&$PS__Data_Maybe, &$PS__Data_Ordering) {
        if ($v === 0) {
            return $PS__Data_Maybe['Just']['constructor']($PS__Data_Ordering['LT']());
        };
        if ($v === 1) {
            return $PS__Data_Maybe['Just']['constructor']($PS__Data_Ordering['EQ']());
        };
        if ($v === 2) {
            return $PS__Data_Maybe['Just']['constructor']($PS__Data_Ordering['GT']());
        };
        return $PS__Data_Maybe['Nothing']();
    });
    $boundedEnumChar = $BoundedEnum(function ()  use (&$PS__Data_Bounded) {
        return $PS__Data_Bounded['boundedChar'];
    }, function ()  use (&$enumChar) {
        return $enumChar;
    }, $PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($__foreign['toCharCode']($PS__Data_Bounded['top']($PS__Data_Bounded['boundedChar'])))($__foreign['toCharCode']($PS__Data_Bounded['bottom']($PS__Data_Bounded['boundedChar']))), $__foreign['toCharCode'], $charToEnum);
    $boundedEnumBoolean = $BoundedEnum(function ()  use (&$PS__Data_Bounded) {
        return $PS__Data_Bounded['boundedBoolean'];
    }, function ()  use (&$enumBoolean) {
        return $enumBoolean;
    }, 2, function ($v) {
        if (!$v) {
            return 0;
        };
        if ($v) {
            return 1;
        };
        throw new \Exception('Failed pattern match at Data.Enum line 118, column 1 - line 118, column 51: ' . var_dump([ $v['constructor']['name'] ]));
    }, function ($v)  use (&$PS__Data_Maybe) {
        if ($v === 0) {
            return $PS__Data_Maybe['Just']['constructor'](false);
        };
        if ($v === 1) {
            return $PS__Data_Maybe['Just']['constructor'](true);
        };
        return $PS__Data_Maybe['Nothing']();
    });
    return [
        'Enum' => $Enum,
        'succ' => $succ,
        'pred' => $pred,
        'BoundedEnum' => $BoundedEnum,
        'cardinality' => $cardinality,
        'toEnum' => $toEnum,
        'fromEnum' => $fromEnum,
        'toEnumWithDefaults' => $toEnumWithDefaults,
        'Cardinality' => $Cardinality,
        'enumFromTo' => $enumFromTo,
        'enumFromThenTo' => $enumFromThenTo,
        'upFrom' => $upFrom,
        'upFromIncluding' => $upFromIncluding,
        'downFrom' => $downFrom,
        'downFromIncluding' => $downFromIncluding,
        'defaultSucc' => $defaultSucc,
        'defaultPred' => $defaultPred,
        'defaultCardinality' => $defaultCardinality,
        'defaultToEnum' => $defaultToEnum,
        'defaultFromEnum' => $defaultFromEnum,
        'enumBoolean' => $enumBoolean,
        'enumInt' => $enumInt,
        'enumChar' => $enumChar,
        'enumUnit' => $enumUnit,
        'enumOrdering' => $enumOrdering,
        'enumMaybe' => $enumMaybe,
        'enumEither' => $enumEither,
        'enumTuple' => $enumTuple,
        'boundedEnumBoolean' => $boundedEnumBoolean,
        'boundedEnumChar' => $boundedEnumChar,
        'boundedEnumUnit' => $boundedEnumUnit,
        'boundedEnumOrdering' => $boundedEnumOrdering,
        'newtypeCardinality' => $newtypeCardinality,
        'eqCardinality' => $eqCardinality,
        'ordCardinality' => $ordCardinality,
        'showCardinality' => $showCardinality
    ];
})();
