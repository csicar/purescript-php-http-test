<?php

$exports['_copyST'] = function ($m) {
  return function () {
    $r = $m;
    return $r;
  };
};

$exports['empty'] = [];

$exports['runST'] = function ($f) {
  return $f();
};

$exports['_fmapObject'] = function ($m0, $f) {
  $m = [];
  foreach ($m0 as $k => $val) {
    $m[$k] = $f($val);
  }
  return $m;
};

$exports['_mapWithKey'] = function ($m0, $f) {
  $m = [];
  foreach ($m0 as $k => $val) {
    $m[$k] = $f($k)($val);
  }
  return $m;
};

$exports['_foldM'] = function ($bind) {
  return function ($f) use (&$bind) {
    return function ($mz) use (&$bind, &$f) {
      return function ($m) use (&$bind, $f, &$mz) {
        $acc = $mz;
        $g =function($k) use (&$f, &$m) {
          return function ($z) use (&$f, &$m) {
            return f($z)($k)($m[$k]);
          };
        };
        foreach ($m as $k => $val) {
          $acc = $bind($acc)($g($k));
        }
        return $acc;
      };
    };
  };
};

$exports['_foldSCObject'] = function ($m, $z, $f, $fromMaybe) {
  $acc = $z;
  foreach ($m as $k => $val) {
    $maybeR = $f($acc)($k)($val);
    $r = $fromMaybe(NULL)($maybeR);
    if ($r === NULL) return $acc;
    else $acc = $r;
  }
  return $acc;
};

$exports['all'] = function ($f) {
  return function ($m) use (&$f) {
    foreach ($m as $k => $val) {
      if (!$f($k)($val)) return false;
    }
    return true;
  };
};

$exports['size'] = function ($m) {
  $s = 0;
  foreach ($m as $k => $val) {
    ++$s;
  }
  return $s;
};

$exports['_lookup'] = function ($no, $yes, $k, $m) {
  return array_key_exists($k, $m) ? $yes($m[$k]) : $no;
};

$exports['_unsafeDeleteObject'] = function ($m, $k) {
  unset($m[$k]);
  return $m;
};

$exports['_lookupST'] = function ($no, $yes, $k, $m) {
  return function () use (&$no, &$yes, &$k, &$m) {
    return array_key_exists($k, $m) ? $yes($m[$k]) : $no;
  };
};



$exports['toArrayWithKey'] = function($f) {
  return function ($m) use (&$f) {
    $r = [];
    foreach ($m as $k => $val) {
      $r[] = f($k)($val);
    }
    return $r;
  };
};

$exports['keys'] = function($a) {
  return array_keys($a);
};
