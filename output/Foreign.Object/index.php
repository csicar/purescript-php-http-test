<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.ST/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.ST.Internal/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Array/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.FoldableWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Function.Uncurried/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.FunctorWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.TraversableWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Foreign.Object.ST/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Foreign_Object = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Data_Array, &$PS__Data_Tuple, &$PS__Data_Ord, &$PS__Data_Unfoldable, &$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Foreign_Object_ST, &$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Control_Applicative, &$PS__Data_Function_Uncurried, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Data_Eq, &$PS__Data_Functor, &$PS__Data_FunctorWithIndex, &$PS__Data_Foldable, &$PS__Data_Monoid, &$PS__Data_FoldableWithIndex, &$PS__Data_TraversableWithIndex, &$PS__Control_Apply, &$PS__Data_Traversable, &$PS__Control_Category, &$PS__Data_HeytingAlgebra) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $values = $__foreign['toArrayWithKey'](function ($v) {
        return function ($v1) {
            return $v1;
        };
    });
    $toUnfoldable = function ($dictUnfoldable)  use (&$PS__Control_Semigroupoid, &$PS__Data_Array, &$__foreign, &$PS__Data_Tuple) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Array['toUnfoldable']($dictUnfoldable))($__foreign['toArrayWithKey']($PS__Data_Tuple['Tuple']['create']));
    };
    $toAscUnfoldable = function ($dictUnfoldable)  use (&$PS__Control_Semigroupoid, &$PS__Data_Array, &$PS__Data_Ord, &$PS__Data_Tuple, &$__foreign) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Array['toUnfoldable']($dictUnfoldable))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Array['sortWith']($PS__Data_Ord['ordString'])($PS__Data_Tuple['fst']))($__foreign['toArrayWithKey']($PS__Data_Tuple['Tuple']['create'])));
    };
    $toAscArray = $toAscUnfoldable($PS__Data_Unfoldable['unfoldableArray']);
    $toArray = $__foreign['toArrayWithKey']($PS__Data_Tuple['Tuple']['create']);
    $thawST = $__foreign['_copyST'];
    $singleton = function ($k)  use (&$__foreign, &$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Foreign_Object_ST) {
        return function ($v)  use (&$__foreign, &$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Foreign_Object_ST, &$k) {
            return $__foreign['runST']($PS__Control_Bind['bindFlipped']($PS__Control_Monad_ST_Internal['bindST'])($PS__Foreign_Object_ST['poke']($k)($v))($PS__Foreign_Object_ST['new']));
        };
    };
    $showObject = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Tuple, &$toArray) {
        return $PS__Data_Show['Show'](function ($m)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$PS__Data_Tuple, &$dictShow, &$toArray) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(fromFoldable ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Show['showArray']($PS__Data_Tuple['showTuple']($PS__Data_Show['showString'])($dictShow)))($toArray($m)))(')'));
        });
    };
    $mutate = function ($f)  use (&$__foreign, &$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$thawST, &$PS__Control_Applicative) {
        return function ($m)  use (&$__foreign, &$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$thawST, &$f, &$PS__Control_Applicative) {
            return $__foreign['runST']($PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($thawST($m))(function ($v)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$f, &$PS__Control_Applicative) {
                return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($f($v))(function ($v1)  use (&$PS__Control_Applicative, &$PS__Control_Monad_ST_Internal, &$v) {
                    return $PS__Control_Applicative['pure']($PS__Control_Monad_ST_Internal['applicativeST'])($v);
                });
            }));
        };
    };
    $member = $PS__Data_Function_Uncurried['runFn4']($__foreign['_lookup'])(false)($PS__Data_Function['const'](true));
    $mapWithKey = function ($f)  use (&$PS__Data_Function_Uncurried, &$__foreign) {
        return function ($m)  use (&$PS__Data_Function_Uncurried, &$__foreign, &$f) {
            return $PS__Data_Function_Uncurried['runFn2']($__foreign['_mapWithKey'])($m)($f);
        };
    };
    $lookup = $PS__Data_Function_Uncurried['runFn4']($__foreign['_lookup'])($PS__Data_Maybe['Nothing']())($PS__Data_Maybe['Just']['create']);
    $isSubmap = function ($dictEq)  use (&$PS__Data_Function_Uncurried, &$__foreign, &$PS__Data_Eq) {
        return function ($m1)  use (&$PS__Data_Function_Uncurried, &$__foreign, &$PS__Data_Eq, &$dictEq) {
            return function ($m2)  use (&$PS__Data_Function_Uncurried, &$__foreign, &$PS__Data_Eq, &$dictEq, &$m1) {
                $f = function ($k)  use (&$PS__Data_Function_Uncurried, &$__foreign, &$PS__Data_Eq, &$dictEq, &$m2) {
                    return function ($v)  use (&$PS__Data_Function_Uncurried, &$__foreign, &$PS__Data_Eq, &$dictEq, &$k, &$m2) {
                        return $PS__Data_Function_Uncurried['runFn4']($__foreign['_lookup'])(false)($PS__Data_Eq['eq']($dictEq)($v))($k)($m2);
                    };
                };
                return $__foreign['all']($f)($m1);
            };
        };
    };
    $isEmpty = $__foreign['all'](function ($v) {
        return function ($v1) {
            return false;
        };
    });
    $insert = function ($k)  use (&$mutate, &$PS__Foreign_Object_ST) {
        return function ($v)  use (&$mutate, &$PS__Foreign_Object_ST, &$k) {
            return $mutate($PS__Foreign_Object_ST['poke']($k)($v));
        };
    };
    $functorObject = $PS__Data_Functor['Functor'](function ($f)  use (&$PS__Data_Function_Uncurried, &$__foreign) {
        return function ($m)  use (&$PS__Data_Function_Uncurried, &$__foreign, &$f) {
            return $PS__Data_Function_Uncurried['runFn2']($__foreign['_fmapObject'])($m)($f);
        };
    });
    $functorWithIndexObject = $PS__Data_FunctorWithIndex['FunctorWithIndex'](function ()  use (&$functorObject) {
        return $functorObject;
    }, $mapWithKey);
    $fromFoldableWith = function ($dictFoldable)  use (&$__foreign, &$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Foreign_Object_ST, &$PS__Data_Foldable, &$PS__Data_Function_Uncurried, &$PS__Control_Applicative) {
        return function ($f)  use (&$__foreign, &$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Foreign_Object_ST, &$PS__Data_Foldable, &$dictFoldable, &$PS__Data_Function_Uncurried, &$PS__Control_Applicative) {
            return function ($l)  use (&$__foreign, &$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Foreign_Object_ST, &$PS__Data_Foldable, &$dictFoldable, &$PS__Data_Function_Uncurried, &$f, &$PS__Control_Applicative) {
                return $__foreign['runST']($PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Foreign_Object_ST['new'])(function ($v)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Foldable, &$dictFoldable, &$l, &$PS__Data_Function_Uncurried, &$__foreign, &$f, &$PS__Foreign_Object_ST, &$PS__Control_Applicative) {
                    return $PS__Control_Bind['discard']($PS__Control_Bind['discardUnit'])($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Foldable['for_']($PS__Control_Monad_ST_Internal['applicativeST'])($dictFoldable)($l)(function ($v1)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Function_Uncurried, &$__foreign, &$f, &$v, &$PS__Foreign_Object_ST) {
                        return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Function_Uncurried['runFn4']($__foreign['_lookupST'])($v1['value1'])($f($v1['value1']))($v1['value0'])($v))(function ($v__prime)  use (&$PS__Foreign_Object_ST, &$v1, &$v) {
                            return $PS__Foreign_Object_ST['poke']($v1['value0'])($v__prime)($v);
                        });
                    }))(function ()  use (&$PS__Control_Applicative, &$PS__Control_Monad_ST_Internal, &$v) {
                        return $PS__Control_Applicative['pure']($PS__Control_Monad_ST_Internal['applicativeST'])($v);
                    });
                }));
            };
        };
    };
    $fromFoldable = function ($dictFoldable)  use (&$__foreign, &$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Foreign_Object_ST, &$PS__Data_Foldable, &$PS__Data_Array, &$PS__Control_Applicative) {
        return function ($l)  use (&$__foreign, &$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Foreign_Object_ST, &$PS__Data_Foldable, &$PS__Data_Array, &$dictFoldable, &$PS__Control_Applicative) {
            return $__foreign['runST']($PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Foreign_Object_ST['new'])(function ($v)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Foldable, &$PS__Data_Array, &$dictFoldable, &$l, &$PS__Foreign_Object_ST, &$PS__Control_Applicative) {
                return $PS__Control_Bind['discard']($PS__Control_Bind['discardUnit'])($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Foldable['for_']($PS__Control_Monad_ST_Internal['applicativeST'])($PS__Data_Foldable['foldableArray'])($PS__Data_Array['fromFoldable']($dictFoldable)($l))(function ($v1)  use (&$PS__Foreign_Object_ST, &$v) {
                    return $PS__Foreign_Object_ST['poke']($v1['value0'])($v1['value1'])($v);
                }))(function ()  use (&$PS__Control_Applicative, &$PS__Control_Monad_ST_Internal, &$v) {
                    return $PS__Control_Applicative['pure']($PS__Control_Monad_ST_Internal['applicativeST'])($v);
                });
            }));
        };
    };
    $freezeST = $__foreign['_copyST'];
    $foldMaybe = function ($f)  use (&$PS__Data_Function_Uncurried, &$__foreign, &$PS__Data_Maybe) {
        return function ($z)  use (&$PS__Data_Function_Uncurried, &$__foreign, &$f, &$PS__Data_Maybe) {
            return function ($m)  use (&$PS__Data_Function_Uncurried, &$__foreign, &$z, &$f, &$PS__Data_Maybe) {
                return $PS__Data_Function_Uncurried['runFn4']($__foreign['_foldSCObject'])($m)($z)($f)($PS__Data_Maybe['fromMaybe']);
            };
        };
    };
    $foldM = function ($dictMonad)  use (&$__foreign, &$PS__Control_Bind, &$PS__Control_Applicative) {
        return function ($f)  use (&$__foreign, &$PS__Control_Bind, &$dictMonad, &$PS__Control_Applicative) {
            return function ($z)  use (&$__foreign, &$PS__Control_Bind, &$dictMonad, &$f, &$PS__Control_Applicative) {
                return $__foreign['_foldM']($PS__Control_Bind['bind']($dictMonad['Bind1']()))($f)($PS__Control_Applicative['pure']($dictMonad['Applicative0']())($z));
            };
        };
    };
    $semigroupObject = function ($dictSemigroup)  use (&$PS__Data_Semigroup, &$mutate, &$foldM, &$PS__Control_Monad_ST_Internal, &$PS__Foreign_Object_ST, &$PS__Data_Function_Uncurried, &$__foreign) {
        return $PS__Data_Semigroup['Semigroup'](function ($m1)  use (&$mutate, &$foldM, &$PS__Control_Monad_ST_Internal, &$PS__Foreign_Object_ST, &$PS__Data_Function_Uncurried, &$__foreign, &$PS__Data_Semigroup, &$dictSemigroup) {
            return function ($m2)  use (&$mutate, &$foldM, &$PS__Control_Monad_ST_Internal, &$PS__Foreign_Object_ST, &$PS__Data_Function_Uncurried, &$__foreign, &$PS__Data_Semigroup, &$dictSemigroup, &$m1) {
                return $mutate(function ($s1)  use (&$foldM, &$PS__Control_Monad_ST_Internal, &$PS__Foreign_Object_ST, &$PS__Data_Function_Uncurried, &$__foreign, &$PS__Data_Semigroup, &$dictSemigroup, &$m2, &$m1) {
                    return $foldM($PS__Control_Monad_ST_Internal['monadST'])(function ($s2)  use (&$PS__Foreign_Object_ST, &$PS__Data_Function_Uncurried, &$__foreign, &$PS__Data_Semigroup, &$dictSemigroup, &$m2) {
                        return function ($k)  use (&$PS__Foreign_Object_ST, &$PS__Data_Function_Uncurried, &$__foreign, &$PS__Data_Semigroup, &$dictSemigroup, &$m2, &$s2) {
                            return function ($v2)  use (&$PS__Foreign_Object_ST, &$k, &$PS__Data_Function_Uncurried, &$__foreign, &$PS__Data_Semigroup, &$dictSemigroup, &$m2, &$s2) {
                                return $PS__Foreign_Object_ST['poke']($k)($PS__Data_Function_Uncurried['runFn4']($__foreign['_lookup'])($v2)(function ($v1)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$v2) {
                                    return $PS__Data_Semigroup['append']($dictSemigroup)($v1)($v2);
                                })($k)($m2))($s2);
                            };
                        };
                    })($s1)($m1);
                })($m2);
            };
        });
    };
    $monoidObject = function ($dictSemigroup)  use (&$PS__Data_Monoid, &$semigroupObject, &$__foreign) {
        return $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupObject, &$dictSemigroup) {
            return $semigroupObject($dictSemigroup);
        }, $__foreign['empty']);
    };
    $union = function ($m)  use (&$mutate, &$foldM, &$PS__Control_Monad_ST_Internal, &$PS__Foreign_Object_ST) {
        return $mutate(function ($s)  use (&$foldM, &$PS__Control_Monad_ST_Internal, &$PS__Foreign_Object_ST, &$m) {
            return $foldM($PS__Control_Monad_ST_Internal['monadST'])(function ($s__prime)  use (&$PS__Foreign_Object_ST) {
                return function ($k)  use (&$PS__Foreign_Object_ST, &$s__prime) {
                    return function ($v)  use (&$PS__Foreign_Object_ST, &$k, &$s__prime) {
                        return $PS__Foreign_Object_ST['poke']($k)($v)($s__prime);
                    };
                };
            })($s)($m);
        });
    };
    $unions = function ($dictFoldable)  use (&$PS__Data_Foldable, &$union, &$__foreign) {
        return $PS__Data_Foldable['foldl']($dictFoldable)($union)($__foreign['empty']);
    };
    $fold = $__foreign['_foldM']($PS__Data_Function['applyFlipped']);
    $foldMap = function ($dictMonoid)  use (&$fold, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
        return function ($f)  use (&$fold, &$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Monoid) {
            return $fold(function ($acc)  use (&$PS__Data_Semigroup, &$dictMonoid, &$f) {
                return function ($k)  use (&$PS__Data_Semigroup, &$dictMonoid, &$acc, &$f) {
                    return function ($v)  use (&$PS__Data_Semigroup, &$dictMonoid, &$acc, &$f, &$k) {
                        return $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($acc)($f($k)($v));
                    };
                };
            })($PS__Data_Monoid['mempty']($dictMonoid));
        };
    };
    $foldableObject = $PS__Data_Foldable['Foldable'](function ($dictMonoid)  use (&$foldMap, &$PS__Data_Function) {
        return function ($f)  use (&$foldMap, &$dictMonoid, &$PS__Data_Function) {
            return $foldMap($dictMonoid)($PS__Data_Function['const']($f));
        };
    }, function ($f)  use (&$fold) {
        return $fold(function ($z)  use (&$f) {
            return function ($v)  use (&$f, &$z) {
                return $f($z);
            };
        });
    }, function ($f)  use (&$PS__Data_Foldable, &$values) {
        return function ($z)  use (&$PS__Data_Foldable, &$f, &$values) {
            return function ($m)  use (&$PS__Data_Foldable, &$f, &$z, &$values) {
                return $PS__Data_Foldable['foldr']($PS__Data_Foldable['foldableArray'])($f)($z)($values($m));
            };
        };
    });
    $foldableWithIndexObject = $PS__Data_FoldableWithIndex['FoldableWithIndex'](function ()  use (&$foldableObject) {
        return $foldableObject;
    }, function ($dictMonoid)  use (&$foldMap) {
        return $foldMap($dictMonoid);
    }, function ($f)  use (&$fold, &$PS__Data_Function) {
        return $fold($PS__Data_Function['flip']($f));
    }, function ($f)  use (&$PS__Data_Foldable, &$PS__Data_Tuple, &$__foreign) {
        return function ($z)  use (&$PS__Data_Foldable, &$PS__Data_Tuple, &$f, &$__foreign) {
            return function ($m)  use (&$PS__Data_Foldable, &$PS__Data_Tuple, &$f, &$z, &$__foreign) {
                return $PS__Data_Foldable['foldr']($PS__Data_Foldable['foldableArray'])($PS__Data_Tuple['uncurry']($f))($z)($__foreign['toArrayWithKey']($PS__Data_Tuple['Tuple']['create'])($m));
            };
        };
    });
    $traversableWithIndexObject = $PS__Data_TraversableWithIndex['TraversableWithIndex'](function ()  use (&$foldableWithIndexObject) {
        return $foldableWithIndexObject;
    }, function ()  use (&$functorWithIndexObject) {
        return $functorWithIndexObject;
    }, function ()  use (&$traversableObject) {
        return $traversableObject;
    }, function ($dictApplicative)  use (&$fold, &$PS__Control_Apply, &$PS__Data_Functor, &$PS__Data_Function, &$insert, &$PS__Control_Applicative, &$__foreign) {
        return function ($f)  use (&$fold, &$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$PS__Data_Function, &$insert, &$PS__Control_Applicative, &$__foreign) {
            return function ($ms)  use (&$fold, &$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$PS__Data_Function, &$insert, &$f, &$PS__Control_Applicative, &$__foreign) {
                return $fold(function ($acc)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$PS__Data_Function, &$insert, &$f) {
                    return function ($k)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$PS__Data_Function, &$insert, &$acc, &$f) {
                        return function ($v)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$PS__Data_Function, &$insert, &$k, &$acc, &$f) {
                            return $PS__Control_Apply['apply']($dictApplicative['Apply0']())($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Function['flip']($insert($k)))($acc))($f($k)($v));
                        };
                    };
                })($PS__Control_Applicative['pure']($dictApplicative)($__foreign['empty']))($ms);
            };
        };
    });
    $traversableObject = $PS__Data_Traversable['Traversable'](function ()  use (&$foldableObject) {
        return $foldableObject;
    }, function ()  use (&$functorObject) {
        return $functorObject;
    }, function ($dictApplicative)  use (&$PS__Data_Traversable, &$traversableObject, &$PS__Control_Category) {
        return $PS__Data_Traversable['traverse']($traversableObject)($dictApplicative)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    }, function ($dictApplicative)  use (&$PS__Control_Semigroupoid, &$PS__Data_TraversableWithIndex, &$traversableWithIndexObject, &$PS__Data_Function) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_TraversableWithIndex['traverseWithIndex']($traversableWithIndexObject)($dictApplicative))($PS__Data_Function['const']);
    });
    $filterWithKey = function ($predicate)  use (&$PS__Foreign_Object_ST, &$PS__Control_Applicative, &$PS__Control_Monad_ST_Internal, &$PS__Control_Bind, &$foldM, &$__foreign) {
        return function ($m)  use (&$predicate, &$PS__Foreign_Object_ST, &$PS__Control_Applicative, &$PS__Control_Monad_ST_Internal, &$PS__Control_Bind, &$foldM, &$__foreign) {
            $go = (function ()  use (&$predicate, &$PS__Foreign_Object_ST, &$PS__Control_Applicative, &$PS__Control_Monad_ST_Internal, &$PS__Control_Bind, &$foldM, &$m) {
                $step = function ($acc)  use (&$predicate, &$PS__Foreign_Object_ST, &$PS__Control_Applicative, &$PS__Control_Monad_ST_Internal) {
                    return function ($k)  use (&$predicate, &$PS__Foreign_Object_ST, &$acc, &$PS__Control_Applicative, &$PS__Control_Monad_ST_Internal) {
                        return function ($v)  use (&$predicate, &$k, &$PS__Foreign_Object_ST, &$acc, &$PS__Control_Applicative, &$PS__Control_Monad_ST_Internal) {
                            $__local_var__40 = $predicate($k)($v);
                            if ($__local_var__40) {
                                return $PS__Foreign_Object_ST['poke']($k)($v)($acc);
                            };
                            return $PS__Control_Applicative['pure']($PS__Control_Monad_ST_Internal['applicativeST'])($acc);
                        };
                    };
                };
                return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Foreign_Object_ST['new'])(function ($v)  use (&$foldM, &$PS__Control_Monad_ST_Internal, &$step, &$m) {
                    return $foldM($PS__Control_Monad_ST_Internal['monadST'])($step)($v)($m);
                });
            })();
            return $__foreign['runST']($go);
        };
    };
    $filterKeys = function ($predicate)  use (&$PS__Data_Function, &$filterWithKey, &$PS__Control_Semigroupoid) {
        return $PS__Data_Function['apply']($filterWithKey)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Function['const'])($predicate));
    };
    $filter = function ($predicate)  use (&$PS__Data_Function, &$filterWithKey) {
        return $PS__Data_Function['apply']($filterWithKey)($PS__Data_Function['const']($predicate));
    };
    $eqObject = function ($dictEq)  use (&$PS__Data_Eq, &$PS__Data_HeytingAlgebra, &$isSubmap) {
        return $PS__Data_Eq['Eq'](function ($m1)  use (&$PS__Data_HeytingAlgebra, &$isSubmap, &$dictEq) {
            return function ($m2)  use (&$PS__Data_HeytingAlgebra, &$isSubmap, &$dictEq, &$m1) {
                return $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($isSubmap($dictEq)($m1)($m2))($isSubmap($dictEq)($m2)($m1));
            };
        });
    };
    $ordObject = function ($dictOrd)  use (&$PS__Data_Ord, &$eqObject, &$PS__Data_Tuple, &$toAscArray) {
        return $PS__Data_Ord['Ord'](function ()  use (&$eqObject, &$dictOrd) {
            return $eqObject($dictOrd['Eq0']());
        }, function ($m1)  use (&$PS__Data_Ord, &$PS__Data_Tuple, &$dictOrd, &$toAscArray) {
            return function ($m2)  use (&$PS__Data_Ord, &$PS__Data_Tuple, &$dictOrd, &$toAscArray, &$m1) {
                return $PS__Data_Ord['compare']($PS__Data_Ord['ordArray']($PS__Data_Tuple['ordTuple']($PS__Data_Ord['ordString'])($dictOrd)))($toAscArray($m1))($toAscArray($m2));
            };
        });
    };
    $eq1Object = $PS__Data_Eq['Eq1'](function ($dictEq)  use (&$PS__Data_Eq, &$eqObject) {
        return $PS__Data_Eq['eq']($eqObject($dictEq));
    });
    $__delete = function ($k)  use (&$mutate, &$PS__Foreign_Object_ST) {
        return $mutate($PS__Foreign_Object_ST['delete']($k));
    };
    $pop = function ($k)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$lookup, &$PS__Data_Tuple, &$__delete) {
        return function ($m)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$lookup, &$k, &$PS__Data_Tuple, &$__delete) {
            return $PS__Data_Functor['mapFlipped']($PS__Data_Maybe['functorMaybe'])($lookup($k)($m))(function ($a)  use (&$PS__Data_Tuple, &$__delete, &$k, &$m) {
                return $PS__Data_Tuple['Tuple']['constructor']($a, $__delete($k)($m));
            });
        };
    };
    $alter = function ($f)  use (&$lookup, &$__delete, &$insert) {
        return function ($k)  use (&$f, &$lookup, &$__delete, &$insert) {
            return function ($m)  use (&$f, &$lookup, &$k, &$__delete, &$insert) {
                $v = $f($lookup($k)($m));
                if ($v['type'] === 'Nothing') {
                    return $__delete($k)($m);
                };
                if ($v['type'] === 'Just') {
                    return $insert($k)($v['value0'])($m);
                };
                throw new \Exception('Failed pattern match at Foreign.Object line 204, column 15 - line 206, column 25: ' . var_dump([ $v['constructor']['name'] ]));
            };
        };
    };
    $update = function ($f)  use (&$alter, &$PS__Data_Maybe) {
        return function ($k)  use (&$alter, &$PS__Data_Maybe, &$f) {
            return function ($m)  use (&$alter, &$PS__Data_Maybe, &$f, &$k) {
                return $alter($PS__Data_Maybe['maybe']($PS__Data_Maybe['Nothing']())($f))($k)($m);
            };
        };
    };
    return [
        'isEmpty' => $isEmpty,
        'singleton' => $singleton,
        'insert' => $insert,
        'lookup' => $lookup,
        'toUnfoldable' => $toUnfoldable,
        'toAscUnfoldable' => $toAscUnfoldable,
        'fromFoldable' => $fromFoldable,
        'fromFoldableWith' => $fromFoldableWith,
        'delete' => $__delete,
        'pop' => $pop,
        'member' => $member,
        'alter' => $alter,
        'update' => $update,
        'mapWithKey' => $mapWithKey,
        'filterWithKey' => $filterWithKey,
        'filterKeys' => $filterKeys,
        'filter' => $filter,
        'values' => $values,
        'union' => $union,
        'unions' => $unions,
        'isSubmap' => $isSubmap,
        'fold' => $fold,
        'foldMap' => $foldMap,
        'foldM' => $foldM,
        'foldMaybe' => $foldMaybe,
        'thawST' => $thawST,
        'freezeST' => $freezeST,
        'functorObject' => $functorObject,
        'functorWithIndexObject' => $functorWithIndexObject,
        'foldableObject' => $foldableObject,
        'foldableWithIndexObject' => $foldableWithIndexObject,
        'traversableObject' => $traversableObject,
        'traversableWithIndexObject' => $traversableWithIndexObject,
        'eqObject' => $eqObject,
        'eq1Object' => $eq1Object,
        'ordObject' => $ordObject,
        'showObject' => $showObject,
        'semigroupObject' => $semigroupObject,
        'monoidObject' => $monoidObject,
        'empty' => $__foreign['empty'],
        'size' => $__foreign['size'],
        'keys' => $__foreign['keys'],
        'all' => $__foreign['all'],
        'runST' => $__foreign['runST'],
        'toArrayWithKey' => $__foreign['toArrayWithKey']
    ];
})();
