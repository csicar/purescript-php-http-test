<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Monoid_Conj = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Semiring, &$PS__Data_HeytingAlgebra, &$PS__Data_Monoid, &$PS__Data_Functor, &$PS__Data_Eq, &$PS__Data_Ord, &$PS__Control_Apply, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Control_Monad) {
    $Conj = function ($x) {
        return $x;
    };
    $showConj = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Conj ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $semiringConj = function ($dictHeytingAlgebra)  use (&$PS__Data_Semiring, &$PS__Data_HeytingAlgebra) {
        return $PS__Data_Semiring['Semiring'](function ($v)  use (&$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra) {
            return function ($v1)  use (&$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$v) {
                return $PS__Data_HeytingAlgebra['conj']($dictHeytingAlgebra)($v)($v1);
            };
        }, function ($v)  use (&$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra) {
            return function ($v1)  use (&$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$v) {
                return $PS__Data_HeytingAlgebra['disj']($dictHeytingAlgebra)($v)($v1);
            };
        }, $PS__Data_HeytingAlgebra['ff']($dictHeytingAlgebra), $PS__Data_HeytingAlgebra['tt']($dictHeytingAlgebra));
    };
    $semigroupConj = function ($dictHeytingAlgebra)  use (&$PS__Data_Semigroup, &$PS__Data_HeytingAlgebra) {
        return $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra) {
            return function ($v1)  use (&$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$v) {
                return $PS__Data_HeytingAlgebra['conj']($dictHeytingAlgebra)($v)($v1);
            };
        });
    };
    $ordConj = function ($dictOrd) {
        return $dictOrd;
    };
    $monoidConj = function ($dictHeytingAlgebra)  use (&$PS__Data_Monoid, &$semigroupConj, &$PS__Data_HeytingAlgebra) {
        return $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupConj, &$dictHeytingAlgebra) {
            return $semigroupConj($dictHeytingAlgebra);
        }, $PS__Data_HeytingAlgebra['tt']($dictHeytingAlgebra));
    };
    $functorConj = $PS__Data_Functor['Functor'](function ($f) {
        return function ($m)  use (&$f) {
            return $f($m);
        };
    });
    $eqConj = function ($dictEq) {
        return $dictEq;
    };
    $eq1Conj = $PS__Data_Eq['Eq1'](function ($dictEq)  use (&$PS__Data_Eq, &$eqConj) {
        return $PS__Data_Eq['eq']($eqConj($dictEq));
    });
    $ord1Conj = $PS__Data_Ord['Ord1'](function ()  use (&$eq1Conj) {
        return $eq1Conj;
    }, function ($dictOrd)  use (&$PS__Data_Ord, &$ordConj) {
        return $PS__Data_Ord['compare']($ordConj($dictOrd));
    });
    $boundedConj = function ($dictBounded) {
        return $dictBounded;
    };
    $applyConj = $PS__Control_Apply['Apply'](function ()  use (&$functorConj) {
        return $functorConj;
    }, function ($v) {
        return function ($v1)  use (&$v) {
            return $v($v1);
        };
    });
    $bindConj = $PS__Control_Bind['Bind'](function ()  use (&$applyConj) {
        return $applyConj;
    }, function ($v) {
        return function ($f)  use (&$v) {
            return $f($v);
        };
    });
    $applicativeConj = $PS__Control_Applicative['Applicative'](function ()  use (&$applyConj) {
        return $applyConj;
    }, $Conj);
    $monadConj = $PS__Control_Monad['Monad'](function ()  use (&$applicativeConj) {
        return $applicativeConj;
    }, function ()  use (&$bindConj) {
        return $bindConj;
    });
    return [
        'Conj' => $Conj,
        'eqConj' => $eqConj,
        'eq1Conj' => $eq1Conj,
        'ordConj' => $ordConj,
        'ord1Conj' => $ord1Conj,
        'boundedConj' => $boundedConj,
        'showConj' => $showConj,
        'functorConj' => $functorConj,
        'applyConj' => $applyConj,
        'applicativeConj' => $applicativeConj,
        'bindConj' => $bindConj,
        'monadConj' => $monadConj,
        'semigroupConj' => $semigroupConj,
        'monoidConj' => $monoidConj,
        'semiringConj' => $semiringConj
    ];
})();
