<?php
$PS__Data_String_Unsafe = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    return [
        'char' => $__foreign['char'],
        'charAt' => $__foreign['charAt']
    ];
})();
