<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Data.List.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Data.List.Lazy.Types/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_List_Lazy_NonEmpty = (function ()  use (&$PS__Data_Lazy, &$PS__Data_List_Lazy_Types, &$PS__Control_Semigroupoid, &$PS__Data_Unfoldable, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_List_Lazy, &$PS__Control_Applicative, &$PS__Data_Function, &$PS__Data_NonEmpty, &$PS__Data_Semiring, &$PS__Control_Bind, &$PS__Data_Semigroup) {
    $uncons = function ($v)  use (&$PS__Data_Lazy) {
        $v1 = $PS__Data_Lazy['force']($v);
        return [
            'head' => $v1['value0'],
            'tail' => $v1['value1']
        ];
    };
    $toList = function ($v)  use (&$PS__Data_Lazy, &$PS__Data_List_Lazy_Types) {
        $v1 = $PS__Data_Lazy['force']($v);
        return $PS__Data_List_Lazy_Types['cons']($v1['value0'])($v1['value1']);
    };
    $toUnfoldable = function ($dictUnfoldable)  use (&$PS__Control_Semigroupoid, &$PS__Data_Unfoldable, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_List_Lazy, &$toList) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Unfoldable['unfoldr']($dictUnfoldable)(function ($xs)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_List_Lazy) {
            return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($rec)  use (&$PS__Data_Tuple) {
                return $PS__Data_Tuple['Tuple']['constructor']($rec['head'], $rec['tail']);
            })($PS__Data_List_Lazy['uncons']($xs));
        }))($toList);
    };
    $tail = function ($v)  use (&$PS__Data_Lazy) {
        $v1 = $PS__Data_Lazy['force']($v);
        return $v1['value1'];
    };
    $singleton = $PS__Control_Applicative['pure']($PS__Data_List_Lazy_Types['applicativeNonEmptyList']);
    $repeat = function ($x)  use (&$PS__Data_Function, &$PS__Data_List_Lazy_Types, &$PS__Data_Lazy, &$PS__Data_NonEmpty, &$PS__Data_List_Lazy) {
        return $PS__Data_Function['apply']($PS__Data_List_Lazy_Types['NonEmptyList'])($PS__Data_Lazy['defer'](function ($v)  use (&$PS__Data_NonEmpty, &$x, &$PS__Data_List_Lazy) {
            return $PS__Data_NonEmpty['NonEmpty']['constructor']($x, $PS__Data_List_Lazy['repeat']($x));
        }));
    };
    $length = function ($v)  use (&$PS__Data_Lazy, &$PS__Data_Semiring, &$PS__Data_List_Lazy) {
        $v1 = $PS__Data_Lazy['force']($v);
        return $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])(1)($PS__Data_List_Lazy['length']($v1['value1']));
    };
    $last = function ($v)  use (&$PS__Data_Lazy, &$PS__Data_Maybe, &$PS__Data_List_Lazy) {
        $v1 = $PS__Data_Lazy['force']($v);
        return $PS__Data_Maybe['fromMaybe']($v1['value0'])($PS__Data_List_Lazy['last']($v1['value1']));
    };
    $iterate = function ($f)  use (&$PS__Data_Function, &$PS__Data_List_Lazy_Types, &$PS__Data_Lazy, &$PS__Data_NonEmpty, &$PS__Data_List_Lazy) {
        return function ($x)  use (&$PS__Data_Function, &$PS__Data_List_Lazy_Types, &$PS__Data_Lazy, &$PS__Data_NonEmpty, &$PS__Data_List_Lazy, &$f) {
            return $PS__Data_Function['apply']($PS__Data_List_Lazy_Types['NonEmptyList'])($PS__Data_Lazy['defer'](function ($v)  use (&$PS__Data_NonEmpty, &$x, &$PS__Data_List_Lazy, &$f) {
                return $PS__Data_NonEmpty['NonEmpty']['constructor']($x, $PS__Data_List_Lazy['iterate']($f)($f($x)));
            }));
        };
    };
    $init = function ($v)  use (&$PS__Data_Lazy, &$PS__Data_Maybe, &$PS__Data_List_Lazy_Types, &$PS__Data_List_Lazy) {
        $v1 = $PS__Data_Lazy['force']($v);
        return $PS__Data_Maybe['maybe']($PS__Data_List_Lazy_Types['nil'])(function ($v2)  use (&$PS__Data_List_Lazy_Types, &$v1) {
            return $PS__Data_List_Lazy_Types['cons']($v1['value0'])($v2);
        })($PS__Data_List_Lazy['init']($v1['value1']));
    };
    $head = function ($v)  use (&$PS__Data_Lazy) {
        $v1 = $PS__Data_Lazy['force']($v);
        return $v1['value0'];
    };
    $fromList = function ($l)  use (&$PS__Data_List_Lazy_Types, &$PS__Data_Maybe, &$PS__Data_Lazy, &$PS__Data_NonEmpty) {
        $v = $PS__Data_List_Lazy_Types['step']($l);
        if ($v['type'] === 'Nil') {
            return $PS__Data_Maybe['Nothing']();
        };
        if ($v['type'] === 'Cons') {
            return $PS__Data_Maybe['Just']['constructor']($PS__Data_Lazy['defer'](function ($v1)  use (&$PS__Data_NonEmpty, &$v) {
                return $PS__Data_NonEmpty['NonEmpty']['constructor']($v['value0'], $v['value1']);
            }));
        };
        throw new \Exception('Failed pattern match at Data.List.Lazy.NonEmpty line 41, column 3 - line 43, column 61: ' . var_dump([ $v['constructor']['name'] ]));
    };
    $fromFoldable = function ($dictFoldable)  use (&$PS__Control_Semigroupoid, &$fromList, &$PS__Data_List_Lazy) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($fromList)($PS__Data_List_Lazy['fromFoldable']($dictFoldable));
    };
    $concatMap = $PS__Data_Function['flip']($PS__Control_Bind['bind']($PS__Data_List_Lazy_Types['bindNonEmptyList']));
    $appendFoldable = function ($dictFoldable)  use (&$PS__Data_Lazy, &$PS__Data_NonEmpty, &$head, &$PS__Data_Semigroup, &$PS__Data_List_Lazy_Types, &$tail, &$PS__Data_List_Lazy) {
        return function ($nel)  use (&$PS__Data_Lazy, &$PS__Data_NonEmpty, &$head, &$PS__Data_Semigroup, &$PS__Data_List_Lazy_Types, &$tail, &$PS__Data_List_Lazy, &$dictFoldable) {
            return function ($ys)  use (&$PS__Data_Lazy, &$PS__Data_NonEmpty, &$head, &$nel, &$PS__Data_Semigroup, &$PS__Data_List_Lazy_Types, &$tail, &$PS__Data_List_Lazy, &$dictFoldable) {
                return $PS__Data_Lazy['defer'](function ($v)  use (&$PS__Data_NonEmpty, &$head, &$nel, &$PS__Data_Semigroup, &$PS__Data_List_Lazy_Types, &$tail, &$PS__Data_List_Lazy, &$dictFoldable, &$ys) {
                    return $PS__Data_NonEmpty['NonEmpty']['constructor']($head($nel), $PS__Data_Semigroup['append']($PS__Data_List_Lazy_Types['semigroupList'])($tail($nel))($PS__Data_List_Lazy['fromFoldable']($dictFoldable)($ys)));
                });
            };
        };
    };
    return [
        'toUnfoldable' => $toUnfoldable,
        'fromFoldable' => $fromFoldable,
        'fromList' => $fromList,
        'toList' => $toList,
        'singleton' => $singleton,
        'repeat' => $repeat,
        'iterate' => $iterate,
        'head' => $head,
        'last' => $last,
        'tail' => $tail,
        'init' => $init,
        'uncons' => $uncons,
        'length' => $length,
        'concatMap' => $concatMap,
        'appendFoldable' => $appendFoldable
    ];
})();
