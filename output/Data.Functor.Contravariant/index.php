<?php
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Void/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Functor_Contravariant = (function ()  use (&$PS__Data_Functor, &$PS__Data_Void) {
    $Contravariant = function ($cmap) {
        return [
            'cmap' => $cmap
        ];
    };
    $cmap = function ($dict) {
        return $dict['cmap'];
    };
    $cmapFlipped = function ($dictContravariant)  use (&$cmap) {
        return function ($x)  use (&$cmap, &$dictContravariant) {
            return function ($f)  use (&$cmap, &$dictContravariant, &$x) {
                return $cmap($dictContravariant)($f)($x);
            };
        };
    };
    $coerce = function ($dictContravariant)  use (&$PS__Data_Functor, &$PS__Data_Void, &$cmap) {
        return function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Void, &$cmap, &$dictContravariant) {
            return function ($a)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Void, &$cmap, &$dictContravariant) {
                return $PS__Data_Functor['map']($dictFunctor)($PS__Data_Void['absurd'])($cmap($dictContravariant)($PS__Data_Void['absurd'])($a));
            };
        };
    };
    $imapC = function ($dictContravariant)  use (&$cmap) {
        return function ($v)  use (&$cmap, &$dictContravariant) {
            return function ($f)  use (&$cmap, &$dictContravariant) {
                return $cmap($dictContravariant)($f);
            };
        };
    };
    return [
        'cmap' => $cmap,
        'Contravariant' => $Contravariant,
        'cmapFlipped' => $cmapFlipped,
        'coerce' => $coerce,
        'imapC' => $imapC
    ];
})();
