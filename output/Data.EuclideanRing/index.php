<?php
require_once(dirname(__FILE__) . '/../Data.BooleanAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.CommutativeRing/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
$PS__Data_EuclideanRing = (function ()  use (&$PS__Data_Eq, &$PS__Data_Semiring, &$PS__Data_CommutativeRing, &$PS__Data_HeytingAlgebra) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $EuclideanRing = function ($CommutativeRing0, $degree, $div, $mod) {
        return [
            'CommutativeRing0' => $CommutativeRing0,
            'degree' => $degree,
            'div' => $div,
            'mod' => $mod
        ];
    };
    $mod = function ($dict) {
        return $dict['mod'];
    };
    $gcd = function ($_dollar_copy_dictEq)  use (&$PS__Data_Eq, &$PS__Data_Semiring, &$mod) {
        return function ($_dollar_copy_dictEuclideanRing)  use (&$_dollar_copy_dictEq, &$PS__Data_Eq, &$PS__Data_Semiring, &$mod) {
            return function ($_dollar_copy_a)  use (&$_dollar_copy_dictEq, &$_dollar_copy_dictEuclideanRing, &$PS__Data_Eq, &$PS__Data_Semiring, &$mod) {
                return function ($_dollar_copy_b)  use (&$_dollar_copy_dictEq, &$_dollar_copy_dictEuclideanRing, &$_dollar_copy_a, &$PS__Data_Eq, &$PS__Data_Semiring, &$mod) {
                    $_dollar_tco_var_dictEq = $_dollar_copy_dictEq;
                    $_dollar_tco_var_dictEuclideanRing = $_dollar_copy_dictEuclideanRing;
                    $_dollar_tco_var_a = $_dollar_copy_a;
                    $_dollar_tco_done = false;
                    $_dollar_tco_result = NULL;
                    $_dollar_tco_loop = function ($dictEq, $dictEuclideanRing, $a, $b)  use (&$PS__Data_Eq, &$PS__Data_Semiring, &$_dollar_tco_done, &$_dollar_tco_var_dictEq, &$_dollar_tco_var_dictEuclideanRing, &$_dollar_tco_var_a, &$_dollar_copy_b, &$mod) {
                        $__local_var__7 = $PS__Data_Eq['eq']($dictEq)($b)($PS__Data_Semiring['zero']((($dictEuclideanRing['CommutativeRing0']())['Ring0']())['Semiring0']()));
                        if ($__local_var__7) {
                            $_dollar_tco_done = true;
                            return $a;
                        };
                        $_dollar_tco_var_dictEq = $dictEq;
                        $_dollar_tco_var_dictEuclideanRing = $dictEuclideanRing;
                        $_dollar_tco_var_a = $b;
                        $_dollar_copy_b = $mod($dictEuclideanRing)($a)($b);
                        return;
                    };
                    while (!$_dollar_tco_done) {
                        $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_dictEq, $_dollar_tco_var_dictEuclideanRing, $_dollar_tco_var_a, $_dollar_copy_b);
                    };
                    return $_dollar_tco_result;
                };
            };
        };
    };
    $euclideanRingNumber = $EuclideanRing(function ()  use (&$PS__Data_CommutativeRing) {
        return $PS__Data_CommutativeRing['commutativeRingNumber'];
    }, function ($v) {
        return 1;
    }, $__foreign['numDiv'], function ($v) {
        return function ($v1) {
            return 0.0;
        };
    });
    $euclideanRingInt = $EuclideanRing(function ()  use (&$PS__Data_CommutativeRing) {
        return $PS__Data_CommutativeRing['commutativeRingInt'];
    }, $__foreign['intDegree'], $__foreign['intDiv'], $__foreign['intMod']);
    $div = function ($dict) {
        return $dict['div'];
    };
    $lcm = function ($dictEq)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$PS__Data_Semiring, &$div, &$gcd) {
        return function ($dictEuclideanRing)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$dictEq, &$PS__Data_Semiring, &$div, &$gcd) {
            return function ($a)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$dictEq, &$PS__Data_Semiring, &$dictEuclideanRing, &$div, &$gcd) {
                return function ($b)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$dictEq, &$a, &$PS__Data_Semiring, &$dictEuclideanRing, &$div, &$gcd) {
                    $__local_var__8 = $PS__Data_HeytingAlgebra['disj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Eq['eq']($dictEq)($a)($PS__Data_Semiring['zero']((($dictEuclideanRing['CommutativeRing0']())['Ring0']())['Semiring0']())))($PS__Data_Eq['eq']($dictEq)($b)($PS__Data_Semiring['zero']((($dictEuclideanRing['CommutativeRing0']())['Ring0']())['Semiring0']())));
                    if ($__local_var__8) {
                        return $PS__Data_Semiring['zero']((($dictEuclideanRing['CommutativeRing0']())['Ring0']())['Semiring0']());
                    };
                    return $div($dictEuclideanRing)($PS__Data_Semiring['mul']((($dictEuclideanRing['CommutativeRing0']())['Ring0']())['Semiring0']())($a)($b))($gcd($dictEq)($dictEuclideanRing)($a)($b));
                };
            };
        };
    };
    $degree = function ($dict) {
        return $dict['degree'];
    };
    return [
        'EuclideanRing' => $EuclideanRing,
        'degree' => $degree,
        'div' => $div,
        'mod' => $mod,
        'gcd' => $gcd,
        'lcm' => $lcm,
        'euclideanRingInt' => $euclideanRingInt,
        'euclideanRingNumber' => $euclideanRingNumber
    ];
})();
