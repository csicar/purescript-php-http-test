<?php

$exports["intDegree"] = function ($x) {
  return min(abs(x), 2147483647);
};

// See the Euclidean definition in
// https://en.m.wikipedia.org/wiki/Modulo_operation.
$exports["intDiv"] = function ($x) {
  return function ($y) use (&$x) {
    if ($y === 0) return 0;
    return intval($y > 0 ? floor($x / $y) : -floor($x / -$y));
  };
};

$exports["intMod"] = function ($x) {
  return function ($y) use (&$x) {
    if ($y === 0) return 0;
    $yy = abs($y);
    return (($x % $yy) + $yy) % $yy;
  };
};

$exports["numDiv"] = function ($n1) {
  return function ($n2) use (&$n1) {
    return $n1 / $n2;
  };
};
