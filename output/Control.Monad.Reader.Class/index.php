<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Reader_Class = (function ()  use (&$PS__Control_Monad, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$PS__Data_Functor) {
    $MonadAsk = function ($Monad0, $ask) {
        return [
            'Monad0' => $Monad0,
            'ask' => $ask
        ];
    };
    $MonadReader = function ($MonadAsk0, $local) {
        return [
            'MonadAsk0' => $MonadAsk0,
            'local' => $local
        ];
    };
    $monadAskFun = $MonadAsk(function ()  use (&$PS__Control_Monad) {
        return $PS__Control_Monad['monadFn'];
    }, $PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    $monadReaderFun = $MonadReader(function ()  use (&$monadAskFun) {
        return $monadAskFun;
    }, $PS__Control_Semigroupoid['composeFlipped']($PS__Control_Semigroupoid['semigroupoidFn']));
    $local = function ($dict) {
        return $dict['local'];
    };
    $ask = function ($dict) {
        return $dict['ask'];
    };
    $asks = function ($dictMonadAsk)  use (&$PS__Data_Functor, &$ask) {
        return function ($f)  use (&$PS__Data_Functor, &$dictMonadAsk, &$ask) {
            return $PS__Data_Functor['map'](((($dictMonadAsk['Monad0']())['Bind1']())['Apply0']())['Functor0']())($f)($ask($dictMonadAsk));
        };
    };
    return [
        'ask' => $ask,
        'local' => $local,
        'MonadAsk' => $MonadAsk,
        'asks' => $asks,
        'MonadReader' => $MonadReader,
        'monadAskFun' => $monadAskFun,
        'monadReaderFun' => $monadReaderFun
    ];
})();
