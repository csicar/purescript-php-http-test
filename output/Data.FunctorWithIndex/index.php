<?php
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe.First/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe.Last/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Additive/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Conj/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Disj/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Dual/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Multiplicative/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_FunctorWithIndex = (function ()  use (&$PS__Data_Function, &$PS__Data_Monoid_Multiplicative, &$PS__Data_Functor, &$PS__Data_Unit, &$PS__Data_Maybe, &$PS__Data_Maybe_Last, &$PS__Data_Maybe_First, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Disj, &$PS__Data_Monoid_Conj, &$PS__Data_Monoid_Additive) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $FunctorWithIndex = function ($Functor0, $mapWithIndex) {
        return [
            'Functor0' => $Functor0,
            'mapWithIndex' => $mapWithIndex
        ];
    };
    $mapWithIndex = function ($dict) {
        return $dict['mapWithIndex'];
    };
    $mapDefault = function ($dictFunctorWithIndex)  use (&$mapWithIndex, &$PS__Data_Function) {
        return function ($f)  use (&$mapWithIndex, &$dictFunctorWithIndex, &$PS__Data_Function) {
            return $mapWithIndex($dictFunctorWithIndex)($PS__Data_Function['const']($f));
        };
    };
    $functorWithIndexMultiplicative = $FunctorWithIndex(function ()  use (&$PS__Data_Monoid_Multiplicative) {
        return $PS__Data_Monoid_Multiplicative['functorMultiplicative'];
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Monoid_Multiplicative, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Functor['map']($PS__Data_Monoid_Multiplicative['functorMultiplicative']))($f($PS__Data_Unit['unit']));
    });
    $functorWithIndexMaybe = $FunctorWithIndex(function ()  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['functorMaybe'];
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe']))($f($PS__Data_Unit['unit']));
    });
    $functorWithIndexLast = $FunctorWithIndex(function ()  use (&$PS__Data_Maybe_Last) {
        return $PS__Data_Maybe_Last['functorLast'];
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Maybe_Last, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Functor['map']($PS__Data_Maybe_Last['functorLast']))($f($PS__Data_Unit['unit']));
    });
    $functorWithIndexFirst = $FunctorWithIndex(function ()  use (&$PS__Data_Maybe_First) {
        return $PS__Data_Maybe_First['functorFirst'];
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Maybe_First, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Functor['map']($PS__Data_Maybe_First['functorFirst']))($f($PS__Data_Unit['unit']));
    });
    $functorWithIndexDual = $FunctorWithIndex(function ()  use (&$PS__Data_Monoid_Dual) {
        return $PS__Data_Monoid_Dual['functorDual'];
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Monoid_Dual, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Functor['map']($PS__Data_Monoid_Dual['functorDual']))($f($PS__Data_Unit['unit']));
    });
    $functorWithIndexDisj = $FunctorWithIndex(function ()  use (&$PS__Data_Monoid_Disj) {
        return $PS__Data_Monoid_Disj['functorDisj'];
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Monoid_Disj, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Functor['map']($PS__Data_Monoid_Disj['functorDisj']))($f($PS__Data_Unit['unit']));
    });
    $functorWithIndexConj = $FunctorWithIndex(function ()  use (&$PS__Data_Monoid_Conj) {
        return $PS__Data_Monoid_Conj['functorConj'];
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Monoid_Conj, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Functor['map']($PS__Data_Monoid_Conj['functorConj']))($f($PS__Data_Unit['unit']));
    });
    $functorWithIndexArray = $FunctorWithIndex(function ()  use (&$PS__Data_Functor) {
        return $PS__Data_Functor['functorArray'];
    }, $__foreign['mapWithIndexArray']);
    $functorWithIndexAdditive = $FunctorWithIndex(function ()  use (&$PS__Data_Monoid_Additive) {
        return $PS__Data_Monoid_Additive['functorAdditive'];
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Monoid_Additive, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Functor['map']($PS__Data_Monoid_Additive['functorAdditive']))($f($PS__Data_Unit['unit']));
    });
    return [
        'FunctorWithIndex' => $FunctorWithIndex,
        'mapWithIndex' => $mapWithIndex,
        'mapDefault' => $mapDefault,
        'functorWithIndexArray' => $functorWithIndexArray,
        'functorWithIndexMaybe' => $functorWithIndexMaybe,
        'functorWithIndexFirst' => $functorWithIndexFirst,
        'functorWithIndexLast' => $functorWithIndexLast,
        'functorWithIndexAdditive' => $functorWithIndexAdditive,
        'functorWithIndexDual' => $functorWithIndexDual,
        'functorWithIndexConj' => $functorWithIndexConj,
        'functorWithIndexDisj' => $functorWithIndexDisj,
        'functorWithIndexMultiplicative' => $functorWithIndexMultiplicative
    ];
})();
