<?php

$exports["mapWithIndexArray"] = function ($f) {
  return function ($xs) use ($f) {
    $l = count($xs);
    $result = [];
    for ($i = 0; $i < $l; $i++) {
      $result[$i] = $f($i)($xs[$i]);
    }
    return $result;
  };
};
