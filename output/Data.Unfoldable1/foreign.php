<?php

$exports['unfoldr1ArrayImpl'] = function ($isNothing) {
  return function ($fromJust) use (&$isNothing) {
    return function ($fst) use (&$fromJust, &$isNothing) {
      return function ($snd) use (&$fst, &$fromJust, &$isNothing) {
        return function ($f) use (&$snd, &$fst, &$fromJust, &$isNothing) {
          return function ($b) use (&$f, &$snd, &$fst, &$fromJust, &$isNothing) {
            $result = [];
            $value = b;
            while (true) { // eslint-disable-line no-constant-condition
              $tuple = $f($value);
              $result[] = $fst($tuple);
              $maybe = $snd($tuple);
              if ($isNothing($maybe)) return $result;
              $value = $fromJust($maybe);
            }
          };
        };
      };
    };
  };
};
