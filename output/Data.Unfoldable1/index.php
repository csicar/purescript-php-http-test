<?php
require_once(dirname(__FILE__) . '/../Data.Boolean/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Partial.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Unfoldable1 = (function ()  use (&$PS__Data_Maybe, &$PS__Partial_Unsafe, &$PS__Data_Tuple, &$PS__Data_Ord, &$PS__Data_Boolean, &$PS__Data_Ring, &$PS__Data_Semigroup_Traversable, &$PS__Data_Semiring, &$PS__Data_Eq) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Unfoldable1 = function ($unfoldr1) {
        return [
            'unfoldr1' => $unfoldr1
        ];
    };
    $unfoldr1 = function ($dict) {
        return $dict['unfoldr1'];
    };
    $unfoldable1Array = $Unfoldable1($__foreign['unfoldr1ArrayImpl']($PS__Data_Maybe['isNothing'])($PS__Partial_Unsafe['unsafePartial'](function ($dictPartial)  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['fromJust']($dictPartial);
    }))($PS__Data_Tuple['fst'])($PS__Data_Tuple['snd']));
    $replicate1 = function ($dictUnfoldable1)  use (&$PS__Data_Ord, &$PS__Data_Tuple, &$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Data_Ring, &$unfoldr1) {
        return function ($n)  use (&$PS__Data_Ord, &$PS__Data_Tuple, &$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Data_Ring, &$unfoldr1, &$dictUnfoldable1) {
            return function ($v)  use (&$PS__Data_Ord, &$PS__Data_Tuple, &$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Data_Ring, &$unfoldr1, &$dictUnfoldable1, &$n) {
                $step = function ($i)  use (&$PS__Data_Ord, &$PS__Data_Tuple, &$v, &$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Data_Ring) {
                    if ($PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])($i)(0)) {
                        return $PS__Data_Tuple['Tuple']['constructor']($v, $PS__Data_Maybe['Nothing']());
                    };
                    if ($PS__Data_Boolean['otherwise']) {
                        return $PS__Data_Tuple['Tuple']['constructor']($v, $PS__Data_Maybe['Just']['constructor']($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($i)(1)));
                    };
                    throw new \Exception('Failed pattern match at Data.Unfoldable1 line 47, column 5 - line 47, column 39: ' . var_dump([ $i['constructor']['name'] ]));
                };
                return $unfoldr1($dictUnfoldable1)($step)($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($n)(1));
            };
        };
    };
    $replicate1A = function ($dictApply)  use (&$PS__Data_Semigroup_Traversable, &$replicate1) {
        return function ($dictUnfoldable1)  use (&$PS__Data_Semigroup_Traversable, &$dictApply, &$replicate1) {
            return function ($dictTraversable1)  use (&$PS__Data_Semigroup_Traversable, &$dictApply, &$replicate1, &$dictUnfoldable1) {
                return function ($n)  use (&$PS__Data_Semigroup_Traversable, &$dictTraversable1, &$dictApply, &$replicate1, &$dictUnfoldable1) {
                    return function ($m)  use (&$PS__Data_Semigroup_Traversable, &$dictTraversable1, &$dictApply, &$replicate1, &$dictUnfoldable1, &$n) {
                        return $PS__Data_Semigroup_Traversable['sequence1']($dictTraversable1)($dictApply)($replicate1($dictUnfoldable1)($n)($m));
                    };
                };
            };
        };
    };
    $singleton = function ($dictUnfoldable1)  use (&$replicate1) {
        return $replicate1($dictUnfoldable1)(1);
    };
    $range = function ($dictUnfoldable1)  use (&$PS__Data_Semiring, &$PS__Data_Tuple, &$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_Ord, &$PS__Data_Ring, &$unfoldr1) {
        return function ($start)  use (&$PS__Data_Semiring, &$PS__Data_Tuple, &$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_Ord, &$PS__Data_Ring, &$unfoldr1, &$dictUnfoldable1) {
            return function ($end)  use (&$PS__Data_Semiring, &$PS__Data_Tuple, &$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_Ord, &$start, &$PS__Data_Ring, &$unfoldr1, &$dictUnfoldable1) {
                $go = function ($delta)  use (&$PS__Data_Semiring, &$PS__Data_Tuple, &$PS__Data_Eq, &$end, &$PS__Data_Maybe) {
                    return function ($i)  use (&$PS__Data_Semiring, &$delta, &$PS__Data_Tuple, &$PS__Data_Eq, &$end, &$PS__Data_Maybe) {
                        $i__prime = $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($i)($delta);
                        return $PS__Data_Tuple['Tuple']['constructor']($i, (function ()  use (&$PS__Data_Eq, &$i, &$end, &$PS__Data_Maybe, &$i__prime) {
                            $__local_var__8 = $PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($i)($end);
                            if ($__local_var__8) {
                                return $PS__Data_Maybe['Nothing']();
                            };
                            return $PS__Data_Maybe['Just']['constructor']($i__prime);
                        })());
                    };
                };
                $delta = (function ()  use (&$PS__Data_Ord, &$end, &$start, &$PS__Data_Ring) {
                    $__local_var__9 = $PS__Data_Ord['greaterThanOrEq']($PS__Data_Ord['ordInt'])($end)($start);
                    if ($__local_var__9) {
                        return 1;
                    };
                    return $PS__Data_Ring['negate']($PS__Data_Ring['ringInt'])(1);
                })();
                return $unfoldr1($dictUnfoldable1)($go($delta))($start);
            };
        };
    };
    return [
        'Unfoldable1' => $Unfoldable1,
        'unfoldr1' => $unfoldr1,
        'replicate1' => $replicate1,
        'replicate1A' => $replicate1A,
        'singleton' => $singleton,
        'range' => $range,
        'unfoldable1Array' => $unfoldable1Array
    ];
})();
