<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad/index.php');
require_once(dirname(__FILE__) . '/../Control.Extend/index.php');
require_once(dirname(__FILE__) . '/../Control.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Data.BooleanAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.CommutativeRing/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.EuclideanRing/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor.Invariant/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Identity = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Functor_Invariant, &$PS__Data_Foldable, &$PS__Data_Traversable, &$PS__Control_Extend, &$PS__Data_Eq, &$PS__Data_Ord, &$PS__Control_Comonad, &$PS__Control_Apply, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Control_Monad, &$PS__Control_Alt) {
    $Identity = function ($x) {
        return $x;
    };
    $showIdentity = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Identity ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $semiringIdentity = function ($dictSemiring) {
        return $dictSemiring;
    };
    $semigroupIdenity = function ($dictSemigroup) {
        return $dictSemigroup;
    };
    $ringIdentity = function ($dictRing) {
        return $dictRing;
    };
    $ordIdentity = function ($dictOrd) {
        return $dictOrd;
    };
    $newtypeIdentity = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Identity);
    $monoidIdentity = function ($dictMonoid) {
        return $dictMonoid;
    };
    $lazyIdentity = function ($dictLazy) {
        return $dictLazy;
    };
    $heytingAlgebraIdentity = function ($dictHeytingAlgebra) {
        return $dictHeytingAlgebra;
    };
    $functorIdentity = $PS__Data_Functor['Functor'](function ($f) {
        return function ($m)  use (&$f) {
            return $f($m);
        };
    });
    $invariantIdentity = $PS__Data_Functor_Invariant['Invariant']($PS__Data_Functor_Invariant['imapF']($functorIdentity));
    $foldableIdentity = $PS__Data_Foldable['Foldable'](function ($dictMonoid) {
        return function ($f) {
            return function ($v)  use (&$f) {
                return $f($v);
            };
        };
    }, function ($f) {
        return function ($z)  use (&$f) {
            return function ($v)  use (&$f, &$z) {
                return $f($z)($v);
            };
        };
    }, function ($f) {
        return function ($z)  use (&$f) {
            return function ($v)  use (&$f, &$z) {
                return $f($v)($z);
            };
        };
    });
    $traversableIdentity = $PS__Data_Traversable['Traversable'](function ()  use (&$foldableIdentity) {
        return $foldableIdentity;
    }, function ()  use (&$functorIdentity) {
        return $functorIdentity;
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$Identity) {
        return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$Identity) {
            return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($Identity)($v);
        };
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$Identity) {
        return function ($f)  use (&$PS__Data_Functor, &$dictApplicative, &$Identity) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$Identity, &$f) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($Identity)($f($v));
            };
        };
    });
    $extendIdentity = $PS__Control_Extend['Extend'](function ()  use (&$functorIdentity) {
        return $functorIdentity;
    }, function ($f) {
        return function ($m)  use (&$f) {
            return $f($m);
        };
    });
    $euclideanRingIdentity = function ($dictEuclideanRing) {
        return $dictEuclideanRing;
    };
    $eqIdentity = function ($dictEq) {
        return $dictEq;
    };
    $eq1Identity = $PS__Data_Eq['Eq1'](function ($dictEq)  use (&$PS__Data_Eq, &$eqIdentity) {
        return $PS__Data_Eq['eq']($eqIdentity($dictEq));
    });
    $ord1Identity = $PS__Data_Ord['Ord1'](function ()  use (&$eq1Identity) {
        return $eq1Identity;
    }, function ($dictOrd)  use (&$PS__Data_Ord, &$ordIdentity) {
        return $PS__Data_Ord['compare']($ordIdentity($dictOrd));
    });
    $comonadIdentity = $PS__Control_Comonad['Comonad'](function ()  use (&$extendIdentity) {
        return $extendIdentity;
    }, function ($v) {
        return $v;
    });
    $commutativeRingIdentity = function ($dictCommutativeRing) {
        return $dictCommutativeRing;
    };
    $boundedIdentity = function ($dictBounded) {
        return $dictBounded;
    };
    $booleanAlgebraIdentity = function ($dictBooleanAlgebra) {
        return $dictBooleanAlgebra;
    };
    $applyIdentity = $PS__Control_Apply['Apply'](function ()  use (&$functorIdentity) {
        return $functorIdentity;
    }, function ($v) {
        return function ($v1)  use (&$v) {
            return $v($v1);
        };
    });
    $bindIdentity = $PS__Control_Bind['Bind'](function ()  use (&$applyIdentity) {
        return $applyIdentity;
    }, function ($v) {
        return function ($f)  use (&$v) {
            return $f($v);
        };
    });
    $applicativeIdentity = $PS__Control_Applicative['Applicative'](function ()  use (&$applyIdentity) {
        return $applyIdentity;
    }, $Identity);
    $monadIdentity = $PS__Control_Monad['Monad'](function ()  use (&$applicativeIdentity) {
        return $applicativeIdentity;
    }, function ()  use (&$bindIdentity) {
        return $bindIdentity;
    });
    $altIdentity = $PS__Control_Alt['Alt'](function ()  use (&$functorIdentity) {
        return $functorIdentity;
    }, function ($x) {
        return function ($v)  use (&$x) {
            return $x;
        };
    });
    return [
        'Identity' => $Identity,
        'newtypeIdentity' => $newtypeIdentity,
        'eqIdentity' => $eqIdentity,
        'ordIdentity' => $ordIdentity,
        'boundedIdentity' => $boundedIdentity,
        'heytingAlgebraIdentity' => $heytingAlgebraIdentity,
        'booleanAlgebraIdentity' => $booleanAlgebraIdentity,
        'semigroupIdenity' => $semigroupIdenity,
        'monoidIdentity' => $monoidIdentity,
        'semiringIdentity' => $semiringIdentity,
        'euclideanRingIdentity' => $euclideanRingIdentity,
        'ringIdentity' => $ringIdentity,
        'commutativeRingIdentity' => $commutativeRingIdentity,
        'lazyIdentity' => $lazyIdentity,
        'showIdentity' => $showIdentity,
        'eq1Identity' => $eq1Identity,
        'ord1Identity' => $ord1Identity,
        'functorIdentity' => $functorIdentity,
        'invariantIdentity' => $invariantIdentity,
        'altIdentity' => $altIdentity,
        'applyIdentity' => $applyIdentity,
        'applicativeIdentity' => $applicativeIdentity,
        'bindIdentity' => $bindIdentity,
        'monadIdentity' => $monadIdentity,
        'extendIdentity' => $extendIdentity,
        'comonadIdentity' => $comonadIdentity,
        'foldableIdentity' => $foldableIdentity,
        'traversableIdentity' => $traversableIdentity
    ];
})();
