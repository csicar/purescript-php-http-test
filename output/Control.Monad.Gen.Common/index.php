<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Gen/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Gen.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Data.Either/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Identity/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Gen_Common = (function ()  use (&$PS__Control_Apply, &$PS__Data_Tuple, &$PS__Data_Functor, &$PS__Data_NonEmpty, &$PS__Control_Monad_Gen_Class, &$PS__Data_Ring, &$PS__Control_Monad_Gen, &$PS__Control_Bind, &$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Control_Applicative, &$PS__Data_Identity, &$PS__Data_Either) {
    $genTuple = function ($dictApply)  use (&$PS__Control_Apply, &$PS__Data_Tuple) {
        return $PS__Control_Apply['lift2']($dictApply)($PS__Data_Tuple['Tuple']['create']);
    };
    $genNonEmpty = function ($dictMonadRec)  use (&$PS__Control_Apply, &$PS__Data_Functor, &$PS__Data_NonEmpty, &$PS__Control_Monad_Gen_Class, &$PS__Data_Ring, &$PS__Control_Monad_Gen) {
        return function ($dictMonadGen)  use (&$PS__Control_Apply, &$PS__Data_Functor, &$PS__Data_NonEmpty, &$PS__Control_Monad_Gen_Class, &$PS__Data_Ring, &$PS__Control_Monad_Gen, &$dictMonadRec) {
            return function ($dictUnfoldable)  use (&$PS__Control_Apply, &$dictMonadGen, &$PS__Data_Functor, &$PS__Data_NonEmpty, &$PS__Control_Monad_Gen_Class, &$PS__Data_Ring, &$PS__Control_Monad_Gen, &$dictMonadRec) {
                return function ($gen)  use (&$PS__Control_Apply, &$dictMonadGen, &$PS__Data_Functor, &$PS__Data_NonEmpty, &$PS__Control_Monad_Gen_Class, &$PS__Data_Ring, &$PS__Control_Monad_Gen, &$dictMonadRec, &$dictUnfoldable) {
                    return $PS__Control_Apply['apply']((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())($PS__Data_Functor['map'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($PS__Data_NonEmpty['NonEmpty']['create'])($gen))($PS__Control_Monad_Gen_Class['resize']($dictMonadGen)(function ($v)  use (&$PS__Data_Ring) {
                        return $PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(1);
                    })($PS__Control_Monad_Gen['unfoldable']($dictMonadRec)($dictMonadGen)($dictUnfoldable)($gen)));
                };
            };
        };
    };
    $genMaybe = function ($dictMonadGen)  use (&$PS__Control_Bind, &$PS__Control_Monad_Gen_Class, &$PS__Data_Ord, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Control_Applicative) {
        return function ($gen)  use (&$PS__Control_Bind, &$dictMonadGen, &$PS__Control_Monad_Gen_Class, &$PS__Data_Ord, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Control_Applicative) {
            return $PS__Control_Bind['bind'](($dictMonadGen['Monad0']())['Bind1']())($PS__Control_Monad_Gen_Class['chooseFloat']($dictMonadGen)(0.0)(1.0))(function ($v)  use (&$PS__Data_Ord, &$PS__Data_Functor, &$dictMonadGen, &$PS__Data_Maybe, &$gen, &$PS__Control_Applicative) {
                $__local_var__10 = $PS__Data_Ord['lessThan']($PS__Data_Ord['ordNumber'])($v)(0.75);
                if ($__local_var__10) {
                    return $PS__Data_Functor['map'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($PS__Data_Maybe['Just']['create'])($gen);
                };
                return $PS__Control_Applicative['pure'](($dictMonadGen['Monad0']())['Applicative0']())($PS__Data_Maybe['Nothing']());
            });
        };
    };
    $genIdentity = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Identity) {
        return $PS__Data_Functor['map']($dictFunctor)($PS__Data_Identity['Identity']);
    };
    $genEither = function ($dictMonadGen)  use (&$PS__Control_Monad_Gen, &$PS__Data_Functor, &$PS__Data_Either) {
        return function ($genA)  use (&$PS__Control_Monad_Gen, &$dictMonadGen, &$PS__Data_Functor, &$PS__Data_Either) {
            return function ($genB)  use (&$PS__Control_Monad_Gen, &$dictMonadGen, &$PS__Data_Functor, &$PS__Data_Either, &$genA) {
                return $PS__Control_Monad_Gen['choose']($dictMonadGen)($PS__Data_Functor['map'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($PS__Data_Either['Left']['create'])($genA))($PS__Data_Functor['map'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($PS__Data_Either['Right']['create'])($genB));
            };
        };
    };
    return [
        'genEither' => $genEither,
        'genIdentity' => $genIdentity,
        'genMaybe' => $genMaybe,
        'genTuple' => $genTuple,
        'genNonEmpty' => $genNonEmpty
    ];
})();
