<?php
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.String/index.php');
require_once(dirname(__FILE__) . '/../Data.String.CodeUnits/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Common/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Pattern/index.php');
require_once(dirname(__FILE__) . '/../Data.Symbol/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
require_once(dirname(__FILE__) . '/../Unsafe.Coerce/index.php');
$PS__Data_String_NonEmpty_Internal = (function ()  use (&$PS__Data_String_Common, &$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Ord, &$PS__Data_Symbol, &$PS__Control_Semigroupoid, &$PS__Data_Foldable, &$PS__Data_Monoid, &$PS__Unsafe_Coerce, &$PS__Data_Maybe, &$PS__Control_Bind, &$PS__Data_String_CodeUnits, &$PS__Data_Eq) {
    $NonEmptyString = function ($x) {
        return $x;
    };
    $NonEmptyReplacement = function ($x) {
        return $x;
    };
    $MakeNonEmpty = function ($nes) {
        return [
            'nes' => $nes
        ];
    };
    $toUpper = function ($v)  use (&$PS__Data_String_Common) {
        return $PS__Data_String_Common['toUpper']($v);
    };
    $toString = function ($v) {
        return $v;
    };
    $toLower = function ($v)  use (&$PS__Data_String_Common) {
        return $PS__Data_String_Common['toLower']($v);
    };
    $showNonEmptyString = $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show) {
        return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(NonEmptyString.unsafeFromString ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Show['showString'])($v))(')'));
    });
    $showNonEmptyReplacement = $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$showNonEmptyString) {
        return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(NonEmptyReplacement ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($showNonEmptyString)($v))(')'));
    });
    $semigroupNonEmptyString = $PS__Data_Semigroup['semigroupString'];
    $semigroupNonEmptyReplacement = $semigroupNonEmptyString;
    $replaceAll = function ($pat)  use (&$PS__Data_String_Common) {
        return function ($v)  use (&$PS__Data_String_Common, &$pat) {
            return function ($v1)  use (&$PS__Data_String_Common, &$pat, &$v) {
                return $PS__Data_String_Common['replaceAll']($pat)($v)($v1);
            };
        };
    };
    $replace = function ($pat)  use (&$PS__Data_String_Common) {
        return function ($v)  use (&$PS__Data_String_Common, &$pat) {
            return function ($v1)  use (&$PS__Data_String_Common, &$pat, &$v) {
                return $PS__Data_String_Common['replace']($pat)($v)($v1);
            };
        };
    };
    $prependString = function ($s1)  use (&$PS__Data_Semigroup) {
        return function ($v)  use (&$PS__Data_Semigroup, &$s1) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($s1)($v);
        };
    };
    $ordNonEmptyString = $PS__Data_Ord['ordString'];
    $ordNonEmptyReplacement = $ordNonEmptyString;
    $nonEmptyNonEmpty = function ($dictIsSymbol)  use (&$MakeNonEmpty, &$PS__Data_Symbol) {
        return $MakeNonEmpty(function ($p)  use (&$PS__Data_Symbol, &$dictIsSymbol) {
            return $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($p);
        });
    };
    $nes = function ($dict) {
        return $dict['nes'];
    };
    $makeNonEmptyBad = function ($dictFail)  use (&$MakeNonEmpty) {
        return $MakeNonEmpty(function ($v) {
            return '';
        });
    };
    $localeCompare = function ($v)  use (&$PS__Data_String_Common) {
        return function ($v1)  use (&$PS__Data_String_Common, &$v) {
            return $PS__Data_String_Common['localeCompare']($v)($v1);
        };
    };
    $liftS = function ($f) {
        return function ($v)  use (&$f) {
            return $f($v);
        };
    };
    $joinWith1 = function ($dictFoldable1)  use (&$PS__Control_Semigroupoid, &$NonEmptyString, &$PS__Data_Foldable, &$PS__Data_Monoid) {
        return function ($v)  use (&$PS__Control_Semigroupoid, &$NonEmptyString, &$PS__Data_Foldable, &$dictFoldable1, &$PS__Data_Monoid) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($NonEmptyString)($PS__Data_Foldable['intercalate']($dictFoldable1['Foldable0']())($PS__Data_Monoid['monoidString'])($v));
        };
    };
    $joinWith = function ($dictFoldable)  use (&$PS__Control_Semigroupoid, &$PS__Data_Foldable, &$PS__Data_Monoid, &$PS__Unsafe_Coerce) {
        return function ($splice)  use (&$PS__Control_Semigroupoid, &$PS__Data_Foldable, &$dictFoldable, &$PS__Data_Monoid, &$PS__Unsafe_Coerce) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Foldable['intercalate']($dictFoldable)($PS__Data_Monoid['monoidString'])($splice))($PS__Unsafe_Coerce['unsafeCoerce']);
        };
    };
    $join1With = function ($dictFoldable1)  use (&$PS__Control_Semigroupoid, &$NonEmptyString, &$joinWith) {
        return function ($splice)  use (&$PS__Control_Semigroupoid, &$NonEmptyString, &$joinWith, &$dictFoldable1) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($NonEmptyString)($joinWith($dictFoldable1['Foldable0']())($splice));
        };
    };
    $fromString = function ($v)  use (&$PS__Data_Maybe) {
        if ($v === '') {
            return $PS__Data_Maybe['Nothing']();
        };
        return $PS__Data_Maybe['Just']['constructor']($v);
    };
    $stripPrefix = function ($pat)  use (&$PS__Control_Bind, &$PS__Data_Maybe, &$fromString, &$liftS, &$PS__Data_String_CodeUnits) {
        return $PS__Control_Bind['composeKleisliFlipped']($PS__Data_Maybe['bindMaybe'])($fromString)($liftS($PS__Data_String_CodeUnits['stripPrefix']($pat)));
    };
    $stripSuffix = function ($pat)  use (&$PS__Control_Bind, &$PS__Data_Maybe, &$fromString, &$liftS, &$PS__Data_String_CodeUnits) {
        return $PS__Control_Bind['composeKleisliFlipped']($PS__Data_Maybe['bindMaybe'])($fromString)($liftS($PS__Data_String_CodeUnits['stripSuffix']($pat)));
    };
    $trim = function ($v)  use (&$fromString, &$PS__Data_String_Common) {
        return $fromString($PS__Data_String_Common['trim']($v));
    };
    $unsafeFromString = function ($dictPartial)  use (&$PS__Control_Semigroupoid, &$PS__Data_Maybe, &$fromString) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['fromJust']($dictPartial))($fromString);
    };
    $eqNonEmptyString = $PS__Data_Eq['eqString'];
    $eqNonEmptyReplacement = $eqNonEmptyString;
    $contains = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($liftS)($PS__Data_String_CodeUnits['contains']);
    $appendString = function ($v)  use (&$PS__Data_Semigroup) {
        return function ($s2)  use (&$PS__Data_Semigroup, &$v) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($v)($s2);
        };
    };
    return [
        'nes' => $nes,
        'NonEmptyString' => $NonEmptyString,
        'MakeNonEmpty' => $MakeNonEmpty,
        'NonEmptyReplacement' => $NonEmptyReplacement,
        'fromString' => $fromString,
        'unsafeFromString' => $unsafeFromString,
        'toString' => $toString,
        'appendString' => $appendString,
        'prependString' => $prependString,
        'stripPrefix' => $stripPrefix,
        'stripSuffix' => $stripSuffix,
        'contains' => $contains,
        'localeCompare' => $localeCompare,
        'replace' => $replace,
        'replaceAll' => $replaceAll,
        'toLower' => $toLower,
        'toUpper' => $toUpper,
        'trim' => $trim,
        'joinWith' => $joinWith,
        'join1With' => $join1With,
        'joinWith1' => $joinWith1,
        'liftS' => $liftS,
        'eqNonEmptyString' => $eqNonEmptyString,
        'ordNonEmptyString' => $ordNonEmptyString,
        'semigroupNonEmptyString' => $semigroupNonEmptyString,
        'showNonEmptyString' => $showNonEmptyString,
        'makeNonEmptyBad' => $makeNonEmptyBad,
        'nonEmptyNonEmpty' => $nonEmptyNonEmpty,
        'eqNonEmptyReplacement' => $eqNonEmptyReplacement,
        'ordNonEmptyReplacement' => $ordNonEmptyReplacement,
        'semigroupNonEmptyReplacement' => $semigroupNonEmptyReplacement,
        'showNonEmptyReplacement' => $showNonEmptyReplacement
    ];
})();
