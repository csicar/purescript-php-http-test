<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe.First/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe.Last/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Additive/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Conj/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Disj/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Dual/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Multiplicative/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable.Accum/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable.Accum.Internal/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Traversable = (function ()  use (&$PS__Data_Foldable, &$PS__Data_Monoid_Multiplicative, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Control_Applicative, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Disj, &$PS__Data_Monoid_Conj, &$PS__Data_Monoid_Additive, &$PS__Control_Category, &$PS__Control_Apply, &$PS__Data_Maybe_First, &$PS__Data_Maybe_Last, &$PS__Data_Traversable_Accum_Internal) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Traversable = function ($Foldable1, $Functor0, $sequence, $traverse) {
        return [
            'Foldable1' => $Foldable1,
            'Functor0' => $Functor0,
            'sequence' => $sequence,
            'traverse' => $traverse
        ];
    };
    $traverse = function ($dict) {
        return $dict['traverse'];
    };
    $traversableMultiplicative = $Traversable(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableMultiplicative'];
    }, function ()  use (&$PS__Data_Monoid_Multiplicative) {
        return $PS__Data_Monoid_Multiplicative['functorMultiplicative'];
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Monoid_Multiplicative) {
        return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Monoid_Multiplicative) {
            return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Monoid_Multiplicative['Multiplicative'])($v);
        };
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Monoid_Multiplicative) {
        return function ($f)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Monoid_Multiplicative) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Monoid_Multiplicative, &$f) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Monoid_Multiplicative['Multiplicative'])($f($v));
            };
        };
    });
    $traversableMaybe = $Traversable(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableMaybe'];
    }, function ()  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['functorMaybe'];
    }, function ($dictApplicative)  use (&$PS__Control_Applicative, &$PS__Data_Maybe, &$PS__Data_Functor) {
        return function ($v)  use (&$PS__Control_Applicative, &$dictApplicative, &$PS__Data_Maybe, &$PS__Data_Functor) {
            if ($v['type'] === 'Nothing') {
                return $PS__Control_Applicative['pure']($dictApplicative)($PS__Data_Maybe['Nothing']());
            };
            if ($v['type'] === 'Just') {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Maybe['Just']['create'])($v['value0']);
            };
            throw new \Exception('Failed pattern match at Data.Traversable line 86, column 1 - line 86, column 47: ' . var_dump([ $v['constructor']['name'] ]));
        };
    }, function ($dictApplicative)  use (&$PS__Control_Applicative, &$PS__Data_Maybe, &$PS__Data_Functor) {
        return function ($v)  use (&$PS__Control_Applicative, &$dictApplicative, &$PS__Data_Maybe, &$PS__Data_Functor) {
            return function ($v1)  use (&$PS__Control_Applicative, &$dictApplicative, &$PS__Data_Maybe, &$PS__Data_Functor, &$v) {
                if ($v1['type'] === 'Nothing') {
                    return $PS__Control_Applicative['pure']($dictApplicative)($PS__Data_Maybe['Nothing']());
                };
                if ($v1['type'] === 'Just') {
                    return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Maybe['Just']['create'])($v($v1['value0']));
                };
                throw new \Exception('Failed pattern match at Data.Traversable line 86, column 1 - line 86, column 47: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
            };
        };
    });
    $traversableDual = $Traversable(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableDual'];
    }, function ()  use (&$PS__Data_Monoid_Dual) {
        return $PS__Data_Monoid_Dual['functorDual'];
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Monoid_Dual) {
        return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Monoid_Dual) {
            return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Monoid_Dual['Dual'])($v);
        };
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Monoid_Dual) {
        return function ($f)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Monoid_Dual) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Monoid_Dual, &$f) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Monoid_Dual['Dual'])($f($v));
            };
        };
    });
    $traversableDisj = $Traversable(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableDisj'];
    }, function ()  use (&$PS__Data_Monoid_Disj) {
        return $PS__Data_Monoid_Disj['functorDisj'];
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Monoid_Disj) {
        return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Monoid_Disj) {
            return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Monoid_Disj['Disj'])($v);
        };
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Monoid_Disj) {
        return function ($f)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Monoid_Disj) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Monoid_Disj, &$f) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Monoid_Disj['Disj'])($f($v));
            };
        };
    });
    $traversableConj = $Traversable(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableConj'];
    }, function ()  use (&$PS__Data_Monoid_Conj) {
        return $PS__Data_Monoid_Conj['functorConj'];
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Monoid_Conj) {
        return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Monoid_Conj) {
            return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Monoid_Conj['Conj'])($v);
        };
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Monoid_Conj) {
        return function ($f)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Monoid_Conj) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Monoid_Conj, &$f) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Monoid_Conj['Conj'])($f($v));
            };
        };
    });
    $traversableAdditive = $Traversable(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableAdditive'];
    }, function ()  use (&$PS__Data_Monoid_Additive) {
        return $PS__Data_Monoid_Additive['functorAdditive'];
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Monoid_Additive) {
        return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Monoid_Additive) {
            return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Monoid_Additive['Additive'])($v);
        };
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Monoid_Additive) {
        return function ($f)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Monoid_Additive) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Monoid_Additive, &$f) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Monoid_Additive['Additive'])($f($v));
            };
        };
    });
    $sequenceDefault = function ($dictTraversable)  use (&$traverse, &$PS__Control_Category) {
        return function ($dictApplicative)  use (&$traverse, &$dictTraversable, &$PS__Control_Category) {
            return $traverse($dictTraversable)($dictApplicative)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        };
    };
    $traversableArray = $Traversable(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableArray'];
    }, function ()  use (&$PS__Data_Functor) {
        return $PS__Data_Functor['functorArray'];
    }, function ($dictApplicative)  use (&$sequenceDefault, &$traversableArray) {
        return $sequenceDefault($traversableArray)($dictApplicative);
    }, function ($dictApplicative)  use (&$__foreign, &$PS__Control_Apply, &$PS__Data_Functor, &$PS__Control_Applicative) {
        return $__foreign['traverseArrayImpl']($PS__Control_Apply['apply']($dictApplicative['Apply0']()))($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']()))($PS__Control_Applicative['pure']($dictApplicative));
    });
    $sequence = function ($dict) {
        return $dict['sequence'];
    };
    $traversableFirst = $Traversable(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableFirst'];
    }, function ()  use (&$PS__Data_Maybe_First) {
        return $PS__Data_Maybe_First['functorFirst'];
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Maybe_First, &$sequence, &$traversableMaybe) {
        return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Maybe_First, &$sequence, &$traversableMaybe) {
            return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Maybe_First['First'])($sequence($traversableMaybe)($dictApplicative)($v));
        };
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Maybe_First, &$traverse, &$traversableMaybe) {
        return function ($f)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Maybe_First, &$traverse, &$traversableMaybe) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Maybe_First, &$traverse, &$traversableMaybe, &$f) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Maybe_First['First'])($traverse($traversableMaybe)($dictApplicative)($f)($v));
            };
        };
    });
    $traversableLast = $Traversable(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableLast'];
    }, function ()  use (&$PS__Data_Maybe_Last) {
        return $PS__Data_Maybe_Last['functorLast'];
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Maybe_Last, &$sequence, &$traversableMaybe) {
        return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Maybe_Last, &$sequence, &$traversableMaybe) {
            return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Maybe_Last['Last'])($sequence($traversableMaybe)($dictApplicative)($v));
        };
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Maybe_Last, &$traverse, &$traversableMaybe) {
        return function ($f)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Maybe_Last, &$traverse, &$traversableMaybe) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Maybe_Last, &$traverse, &$traversableMaybe, &$f) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Maybe_Last['Last'])($traverse($traversableMaybe)($dictApplicative)($f)($v));
            };
        };
    });
    $traverseDefault = function ($dictTraversable)  use (&$sequence, &$PS__Data_Functor) {
        return function ($dictApplicative)  use (&$sequence, &$dictTraversable, &$PS__Data_Functor) {
            return function ($f)  use (&$sequence, &$dictTraversable, &$dictApplicative, &$PS__Data_Functor) {
                return function ($ta)  use (&$sequence, &$dictTraversable, &$dictApplicative, &$PS__Data_Functor, &$f) {
                    return $sequence($dictTraversable)($dictApplicative)($PS__Data_Functor['map']($dictTraversable['Functor0']())($f)($ta));
                };
            };
        };
    };
    $mapAccumR = function ($dictTraversable)  use (&$PS__Data_Traversable_Accum_Internal, &$traverse) {
        return function ($f)  use (&$PS__Data_Traversable_Accum_Internal, &$traverse, &$dictTraversable) {
            return function ($s0)  use (&$PS__Data_Traversable_Accum_Internal, &$traverse, &$dictTraversable, &$f) {
                return function ($xs)  use (&$PS__Data_Traversable_Accum_Internal, &$traverse, &$dictTraversable, &$f, &$s0) {
                    return $PS__Data_Traversable_Accum_Internal['stateR']($traverse($dictTraversable)($PS__Data_Traversable_Accum_Internal['applicativeStateR'])(function ($a)  use (&$f) {
                        return function ($s)  use (&$f, &$a) {
                            return $f($s)($a);
                        };
                    })($xs))($s0);
                };
            };
        };
    };
    $scanr = function ($dictTraversable)  use (&$mapAccumR) {
        return function ($f)  use (&$mapAccumR, &$dictTraversable) {
            return function ($b0)  use (&$mapAccumR, &$dictTraversable, &$f) {
                return function ($xs)  use (&$mapAccumR, &$dictTraversable, &$f, &$b0) {
                    return ($mapAccumR($dictTraversable)(function ($b)  use (&$f) {
                        return function ($a)  use (&$f, &$b) {
                            $b__prime = $f($a)($b);
                            return [
                                'accum' => $b__prime,
                                'value' => $b__prime
                            ];
                        };
                    })($b0)($xs))['value'];
                };
            };
        };
    };
    $mapAccumL = function ($dictTraversable)  use (&$PS__Data_Traversable_Accum_Internal, &$traverse) {
        return function ($f)  use (&$PS__Data_Traversable_Accum_Internal, &$traverse, &$dictTraversable) {
            return function ($s0)  use (&$PS__Data_Traversable_Accum_Internal, &$traverse, &$dictTraversable, &$f) {
                return function ($xs)  use (&$PS__Data_Traversable_Accum_Internal, &$traverse, &$dictTraversable, &$f, &$s0) {
                    return $PS__Data_Traversable_Accum_Internal['stateL']($traverse($dictTraversable)($PS__Data_Traversable_Accum_Internal['applicativeStateL'])(function ($a)  use (&$f) {
                        return function ($s)  use (&$f, &$a) {
                            return $f($s)($a);
                        };
                    })($xs))($s0);
                };
            };
        };
    };
    $scanl = function ($dictTraversable)  use (&$mapAccumL) {
        return function ($f)  use (&$mapAccumL, &$dictTraversable) {
            return function ($b0)  use (&$mapAccumL, &$dictTraversable, &$f) {
                return function ($xs)  use (&$mapAccumL, &$dictTraversable, &$f, &$b0) {
                    return ($mapAccumL($dictTraversable)(function ($b)  use (&$f) {
                        return function ($a)  use (&$f, &$b) {
                            $b__prime = $f($b)($a);
                            return [
                                'accum' => $b__prime,
                                'value' => $b__prime
                            ];
                        };
                    })($b0)($xs))['value'];
                };
            };
        };
    };
    $__for = function ($dictApplicative)  use (&$traverse) {
        return function ($dictTraversable)  use (&$traverse, &$dictApplicative) {
            return function ($x)  use (&$traverse, &$dictTraversable, &$dictApplicative) {
                return function ($f)  use (&$traverse, &$dictTraversable, &$dictApplicative, &$x) {
                    return $traverse($dictTraversable)($dictApplicative)($f)($x);
                };
            };
        };
    };
    return [
        'Traversable' => $Traversable,
        'traverse' => $traverse,
        'sequence' => $sequence,
        'traverseDefault' => $traverseDefault,
        'sequenceDefault' => $sequenceDefault,
        'for' => $__for,
        'scanl' => $scanl,
        'scanr' => $scanr,
        'mapAccumL' => $mapAccumL,
        'mapAccumR' => $mapAccumR,
        'traversableArray' => $traversableArray,
        'traversableMaybe' => $traversableMaybe,
        'traversableFirst' => $traversableFirst,
        'traversableLast' => $traversableLast,
        'traversableAdditive' => $traversableAdditive,
        'traversableDual' => $traversableDual,
        'traversableConj' => $traversableConj,
        'traversableDisj' => $traversableDisj,
        'traversableMultiplicative' => $traversableMultiplicative
    ];
})();
