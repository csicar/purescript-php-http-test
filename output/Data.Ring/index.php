<?php
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Symbol/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Record.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Type.Data.RowList/index.php');
$PS__Data_Ring = (function ()  use (&$PS__Data_Semiring, &$PS__Data_Unit, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Ring = function ($Semiring0, $sub) {
        return [
            'Semiring0' => $Semiring0,
            'sub' => $sub
        ];
    };
    $RingRecord = function ($SemiringRecord0, $subRecord) {
        return [
            'SemiringRecord0' => $SemiringRecord0,
            'subRecord' => $subRecord
        ];
    };
    $subRecord = function ($dict) {
        return $dict['subRecord'];
    };
    $sub = function ($dict) {
        return $dict['sub'];
    };
    $ringUnit = $Ring(function ()  use (&$PS__Data_Semiring) {
        return $PS__Data_Semiring['semiringUnit'];
    }, function ($v)  use (&$PS__Data_Unit) {
        return function ($v1)  use (&$PS__Data_Unit) {
            return $PS__Data_Unit['unit'];
        };
    });
    $ringRecordNil = $RingRecord(function ()  use (&$PS__Data_Semiring) {
        return $PS__Data_Semiring['semiringRecordNil'];
    }, function ($v) {
        return function ($v1) {
            return function ($v2) {
                return [];
            };
        };
    });
    $ringRecordCons = function ($dictIsSymbol)  use (&$RingRecord, &$PS__Data_Semiring, &$subRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$sub) {
        return function ($dictCons)  use (&$RingRecord, &$PS__Data_Semiring, &$dictIsSymbol, &$subRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$sub) {
            return function ($dictRingRecord)  use (&$RingRecord, &$PS__Data_Semiring, &$dictIsSymbol, &$dictCons, &$subRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$sub) {
                return function ($dictRing)  use (&$RingRecord, &$PS__Data_Semiring, &$dictIsSymbol, &$dictCons, &$dictRingRecord, &$subRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$sub) {
                    return $RingRecord(function ()  use (&$PS__Data_Semiring, &$dictIsSymbol, &$dictCons, &$dictRingRecord, &$dictRing) {
                        return $PS__Data_Semiring['semiringRecordCons']($dictIsSymbol)($dictCons)($dictRingRecord['SemiringRecord0']())($dictRing['Semiring0']());
                    }, function ($v)  use (&$subRecord, &$dictRingRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$sub, &$dictRing) {
                        return function ($ra)  use (&$subRecord, &$dictRingRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$sub, &$dictRing) {
                            return function ($rb)  use (&$subRecord, &$dictRingRecord, &$PS__Type_Data_RowList, &$ra, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$sub, &$dictRing) {
                                $tail = $subRecord($dictRingRecord)($PS__Type_Data_RowList['RLProxy']())($ra)($rb);
                                $key = $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($PS__Data_Symbol['SProxy']());
                                $insert = $PS__Record_Unsafe['unsafeSet']($key);
                                $get = $PS__Record_Unsafe['unsafeGet']($key);
                                return $insert($sub($dictRing)($get($ra))($get($rb)))($tail);
                            };
                        };
                    });
                };
            };
        };
    };
    $ringRecord = function ($dictRowToList)  use (&$Ring, &$PS__Data_Semiring, &$subRecord, &$PS__Type_Data_RowList) {
        return function ($dictRingRecord)  use (&$Ring, &$PS__Data_Semiring, &$dictRowToList, &$subRecord, &$PS__Type_Data_RowList) {
            return $Ring(function ()  use (&$PS__Data_Semiring, &$dictRowToList, &$dictRingRecord) {
                return $PS__Data_Semiring['semiringRecord']($dictRowToList)($dictRingRecord['SemiringRecord0']());
            }, $subRecord($dictRingRecord)($PS__Type_Data_RowList['RLProxy']()));
        };
    };
    $ringNumber = $Ring(function ()  use (&$PS__Data_Semiring) {
        return $PS__Data_Semiring['semiringNumber'];
    }, $__foreign['numSub']);
    $ringInt = $Ring(function ()  use (&$PS__Data_Semiring) {
        return $PS__Data_Semiring['semiringInt'];
    }, $__foreign['intSub']);
    $ringFn = function ($dictRing)  use (&$Ring, &$PS__Data_Semiring, &$sub) {
        return $Ring(function ()  use (&$PS__Data_Semiring, &$dictRing) {
            return $PS__Data_Semiring['semiringFn']($dictRing['Semiring0']());
        }, function ($f)  use (&$sub, &$dictRing) {
            return function ($g)  use (&$sub, &$dictRing, &$f) {
                return function ($x)  use (&$sub, &$dictRing, &$f, &$g) {
                    return $sub($dictRing)($f($x))($g($x));
                };
            };
        });
    };
    $negate = function ($dictRing)  use (&$sub, &$PS__Data_Semiring) {
        return function ($a)  use (&$sub, &$dictRing, &$PS__Data_Semiring) {
            return $sub($dictRing)($PS__Data_Semiring['zero']($dictRing['Semiring0']()))($a);
        };
    };
    return [
        'Ring' => $Ring,
        'sub' => $sub,
        'negate' => $negate,
        'RingRecord' => $RingRecord,
        'subRecord' => $subRecord,
        'ringInt' => $ringInt,
        'ringNumber' => $ringNumber,
        'ringUnit' => $ringUnit,
        'ringFn' => $ringFn,
        'ringRecord' => $ringRecord,
        'ringRecordNil' => $ringRecordNil,
        'ringRecordCons' => $ringRecordCons
    ];
})();
