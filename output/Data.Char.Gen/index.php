<?php
require_once(dirname(__FILE__) . '/../Control.Monad.Gen/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Gen.Class/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Enum/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Char_Gen = (function ()  use (&$PS__Data_Functor, &$PS__Data_Enum, &$PS__Data_Bounded, &$PS__Control_Monad_Gen_Class, &$PS__Control_Monad_Gen, &$PS__Data_NonEmpty, &$PS__Data_Foldable) {
    $genUnicodeChar = function ($dictMonadGen)  use (&$PS__Data_Functor, &$PS__Data_Enum, &$PS__Data_Bounded, &$PS__Control_Monad_Gen_Class) {
        return $PS__Data_Functor['map'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($PS__Data_Enum['toEnumWithDefaults']($PS__Data_Enum['boundedEnumChar'])($PS__Data_Bounded['bottom']($PS__Data_Bounded['boundedChar']))($PS__Data_Bounded['top']($PS__Data_Bounded['boundedChar'])))($PS__Control_Monad_Gen_Class['chooseInt']($dictMonadGen)(0)(65536));
    };
    $genDigitChar = function ($dictMonadGen)  use (&$PS__Data_Functor, &$PS__Data_Enum, &$PS__Data_Bounded, &$PS__Control_Monad_Gen_Class) {
        return $PS__Data_Functor['map'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($PS__Data_Enum['toEnumWithDefaults']($PS__Data_Enum['boundedEnumChar'])($PS__Data_Bounded['bottom']($PS__Data_Bounded['boundedChar']))($PS__Data_Bounded['top']($PS__Data_Bounded['boundedChar'])))($PS__Control_Monad_Gen_Class['chooseInt']($dictMonadGen)(48)(57));
    };
    $genAsciiChar__prime = function ($dictMonadGen)  use (&$PS__Data_Functor, &$PS__Data_Enum, &$PS__Data_Bounded, &$PS__Control_Monad_Gen_Class) {
        return $PS__Data_Functor['map'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($PS__Data_Enum['toEnumWithDefaults']($PS__Data_Enum['boundedEnumChar'])($PS__Data_Bounded['bottom']($PS__Data_Bounded['boundedChar']))($PS__Data_Bounded['top']($PS__Data_Bounded['boundedChar'])))($PS__Control_Monad_Gen_Class['chooseInt']($dictMonadGen)(0)(127));
    };
    $genAsciiChar = function ($dictMonadGen)  use (&$PS__Data_Functor, &$PS__Data_Enum, &$PS__Data_Bounded, &$PS__Control_Monad_Gen_Class) {
        return $PS__Data_Functor['map'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($PS__Data_Enum['toEnumWithDefaults']($PS__Data_Enum['boundedEnumChar'])($PS__Data_Bounded['bottom']($PS__Data_Bounded['boundedChar']))($PS__Data_Bounded['top']($PS__Data_Bounded['boundedChar'])))($PS__Control_Monad_Gen_Class['chooseInt']($dictMonadGen)(32)(127));
    };
    $genAlphaUppercase = function ($dictMonadGen)  use (&$PS__Data_Functor, &$PS__Data_Enum, &$PS__Data_Bounded, &$PS__Control_Monad_Gen_Class) {
        return $PS__Data_Functor['map'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($PS__Data_Enum['toEnumWithDefaults']($PS__Data_Enum['boundedEnumChar'])($PS__Data_Bounded['bottom']($PS__Data_Bounded['boundedChar']))($PS__Data_Bounded['top']($PS__Data_Bounded['boundedChar'])))($PS__Control_Monad_Gen_Class['chooseInt']($dictMonadGen)(65)(90));
    };
    $genAlphaLowercase = function ($dictMonadGen)  use (&$PS__Data_Functor, &$PS__Data_Enum, &$PS__Data_Bounded, &$PS__Control_Monad_Gen_Class) {
        return $PS__Data_Functor['map'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($PS__Data_Enum['toEnumWithDefaults']($PS__Data_Enum['boundedEnumChar'])($PS__Data_Bounded['bottom']($PS__Data_Bounded['boundedChar']))($PS__Data_Bounded['top']($PS__Data_Bounded['boundedChar'])))($PS__Control_Monad_Gen_Class['chooseInt']($dictMonadGen)(97)(122));
    };
    $genAlpha = function ($dictMonadGen)  use (&$PS__Control_Monad_Gen, &$PS__Data_NonEmpty, &$PS__Data_Foldable, &$genAlphaLowercase, &$genAlphaUppercase) {
        return $PS__Control_Monad_Gen['oneOf']($dictMonadGen)($PS__Data_NonEmpty['foldable1NonEmpty']($PS__Data_Foldable['foldableArray']))($PS__Data_NonEmpty['NonEmpty']['constructor']($genAlphaLowercase($dictMonadGen), [ $genAlphaUppercase($dictMonadGen) ]));
    };
    return [
        'genUnicodeChar' => $genUnicodeChar,
        'genAsciiChar' => $genAsciiChar,
        'genAsciiChar\'' => $genAsciiChar__prime,
        'genDigitChar' => $genDigitChar,
        'genAlpha' => $genAlpha,
        'genAlphaLowercase' => $genAlphaLowercase,
        'genAlphaUppercase' => $genAlphaUppercase
    ];
})();
