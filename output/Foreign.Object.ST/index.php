<?php
require_once(dirname(__FILE__) . '/../Control.Monad.ST/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
$PS__Foreign_Object_ST = (function ()  use (&$PS__Data_Maybe) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $peek = $__foreign['peekImpl']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    return [
        'peek' => $peek,
        'new' => $__foreign['new'],
        'poke' => $__foreign['poke'],
        'delete' => $__foreign['delete']
    ];
})();
