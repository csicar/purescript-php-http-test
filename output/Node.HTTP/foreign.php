<?php
use Psr\Http\Message\ServerRequestInterface;
use React\EventLoop\Factory;
use React\Http\Response;
use React\Http\Server;
require './vendor/autoload.php';
// var http = require("http");
$exports['createServer'] = function ($handleRequest) {
  return function () use (&$handleRequest) {
    return new Server(function($req) use (&$handleRequest) {
      $res = (object) [
        'statusCode' => 200,
        'header' => ['Content-Type' => 'text/plain'],
        'body' => ''
      ];
      $handleRequest($req, $res);
      echo var_dump($res);
      return new Response($res['statusCode'], $res['header'], $res['body']);
    });
  };
};

$exports['listenImpl'] = function ($server) {
  return function ($port) use (&$server) {
    return function ($hostname) use (&$port, &$server){
      return function ($backlog) use (&$hostname, &$port, &$server) {
        return function ($done) use (&$backlog, &$hostname, &$port, &$server) {
          return function () use (&$done, &$backlog, &$hostname, &$port, &$server) {
            $loop = $GLOBALS['loop'];

            $socket = new \React\Socket\Server($hostname . ":" . $port, $loop);
            $server->listen($socket);
            // TODO backlog stuff
            // if (backlog !== null) {
            //   server.listen(port, hostname, backlog, done);
            // } else {
            //   server.listen(port, hostname, done);
            // }
          };
        };
      };
    };
  };
};

$exports['closeImpl'] = function ($server) {
  return function ($done) {
    return function () use (&$server, &$done) {
      $server->close($done);
    };
  };
};

$exports['listenSocket'] = function ($server) {
  return function ($path) use (&$server) {
    return function ($done) use (&$server, &$path) {
      return function () use (&$server, &$path, &$done) {
        $server->listen($path, $done);
      };
    };
  };
};

$exports['setHeader'] = function (&$res) {
  return function ($key) use (&$res) {
    return function ($value) use (&$res, &$key) {
      return function () use (&$res, &$key, &$value) {
        echo "ads";
        $res['header'][$key] = $value;
      };
    };
  };
};

$exports['setHeaders'] = function (&$res) {
  return function ($key) use (&$res) {
    return function ($values) use (&$res, &$key) {
      return function () use (&$res, &$key, &$values) {
        $res['header'][$key] = $values;
      };
    };
  };
};

$exports['setStatusCode'] = function (&$res) {
  return function ($code) use (&$res) {
    return function () use (&$res, &$code) {
      $res['statusCode'] = $code;
    };
  };
};

$exports['setBody'] = function (&$res) {
  return function ($body) use (&$res) {
    return function () use (&$res, &$body) {
      $res['body'] = $body;
    };
  };
};

$exports['setStatusMessage'] = function (&$res) {
  return function ($message) use (&$res) {
    return function () use (&$res, &$message) {
      $res['statusMessage'] = $message;
    };
  };
};
