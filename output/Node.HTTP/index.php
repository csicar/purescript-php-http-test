<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Nullable/index.php');
require_once(dirname(__FILE__) . '/../Effect/index.php');
require_once(dirname(__FILE__) . '/../Foreign.Object/index.php');
require_once(dirname(__FILE__) . '/../Node.Stream/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
require_once(dirname(__FILE__) . '/../Unsafe.Coerce/index.php');
$PS__Node_HTTP = (function ()  use (&$PS__Unsafe_Coerce, &$PS__Control_Semigroupoid, &$PS__Data_Nullable) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $responseAsStream = $PS__Unsafe_Coerce['unsafeCoerce'];
    $requestURL = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])(function ($v) {
        return $v['url'];
    })($PS__Unsafe_Coerce['unsafeCoerce']);
    $requestMethod = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])(function ($v) {
        return $v['method'];
    })($PS__Unsafe_Coerce['unsafeCoerce']);
    $requestHeaders = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])(function ($v) {
        return $v['headers'];
    })($PS__Unsafe_Coerce['unsafeCoerce']);
    $requestAsStream = $PS__Unsafe_Coerce['unsafeCoerce'];
    $listen = function ($server)  use (&$__foreign, &$PS__Data_Nullable) {
        return function ($opts)  use (&$__foreign, &$server, &$PS__Data_Nullable) {
            return function ($done)  use (&$__foreign, &$server, &$opts, &$PS__Data_Nullable) {
                return $__foreign['listenImpl']($server)($opts['port'])($opts['hostname'])($PS__Data_Nullable['toNullable']($opts['backlog']))($done);
            };
        };
    };
    $httpVersion = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])(function ($v) {
        return $v['httpVersion'];
    })($PS__Unsafe_Coerce['unsafeCoerce']);
    $close = function ($server)  use (&$__foreign) {
        return function ($done)  use (&$__foreign, &$server) {
            return $__foreign['closeImpl']($server)($done);
        };
    };
    return [
        'listen' => $listen,
        'close' => $close,
        'httpVersion' => $httpVersion,
        'requestHeaders' => $requestHeaders,
        'requestMethod' => $requestMethod,
        'requestURL' => $requestURL,
        'requestAsStream' => $requestAsStream,
        'responseAsStream' => $responseAsStream,
        'createServer' => $__foreign['createServer'],
        'listenSocket' => $__foreign['listenSocket'],
        'setHeader' => $__foreign['setHeader'],
        'setHeaders' => $__foreign['setHeaders'],
        'setStatusCode' => $__foreign['setStatusCode'],
        'setStatusMessage' => $__foreign['setStatusMessage'],
        'setBody' => $__foreign['setBody']
    ];
})();
