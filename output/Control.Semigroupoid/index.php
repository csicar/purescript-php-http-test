<?php
$PS__Control_Semigroupoid = (function () {
    $Semigroupoid = function ($compose) {
        return [
            'compose' => $compose
        ];
    };
    $semigroupoidFn = $Semigroupoid(function ($f) {
        return function ($g)  use (&$f) {
            return function ($x)  use (&$f, &$g) {
                return $f($g($x));
            };
        };
    });
    $compose = function ($dict) {
        return $dict['compose'];
    };
    $composeFlipped = function ($dictSemigroupoid)  use (&$compose) {
        return function ($f)  use (&$compose, &$dictSemigroupoid) {
            return function ($g)  use (&$compose, &$dictSemigroupoid, &$f) {
                return $compose($dictSemigroupoid)($g)($f);
            };
        };
    };
    return [
        'compose' => $compose,
        'Semigroupoid' => $Semigroupoid,
        'composeFlipped' => $composeFlipped,
        'semigroupoidFn' => $semigroupoidFn
    ];
})();
