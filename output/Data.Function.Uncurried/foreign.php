<?php

$exports["mkFn0"] = function($fn) {
  return function() use (&$fn) {
    return $fn([]);
  };
};

$exports["mkFn1"] = function ($fn) {
  return function ($x) use(&$fn) {
    return $fn($x);
  };
};

$exports["mkFn2"] = function ($fn) {
  return function ($a, $b) use(&$fn) {
    return $fn($a)($b);
  };
};

$exports["mkFn3"] = function ($fn) {
  return function ($a, $b, $c) use(&$fn) {
    return $fn($a)($b)($c);
  };
};

$exports["mkFn4"] = function ($fn) {
  return function ($a, $b, $c, $d) use(&$fn) {
    return $fn($a)($b)($c)($d);
  };
};

$exports["mkFn5"] = function ($fn) {
  return function ($a, $b, $c, $d, $e) use(&$fn) {
    return $fn($a)($b)($c)($d)($e);
  };
};

$exports["mkFn6"] = function ($fn) {
  return function ($a, $b, $c, $d, $e, $f) use(&$fn) {
    return $fn($a)($b)($c)($d)($e)($f);
  };
};

$exports["mkFn7"] = function ($fn) {
  return function ($a, $b, $c, $d, $e, $f, $g) use(&$fn) {
    return $fn($a)($b)($c)($d)($e)($f)($g);
  };
};

$exports["mkFn8"] = function ($fn) {
  return function ($a, $b, $c, $d, $e, $f, $g, $h) use(&$fn) {
    return $fn($a)($b)($c)($d)($e)($f)($g)($h);
  };
};

$exports["mkFn9"] = function ($fn) {
  return function ($a, $b, $c, $d, $e, $f, $g, $h, $i) use(&$fn) {
    return $fn($a)($b)($c)($d)($e)($f)($g)($h)($i);
  };
};

$exports["mkFn10"] = function ($fn) {
  return function ($a, $b, $c, $d, $e, $f, $g, $h, $i, $j) use(&$fn) {
    return $fn($a)($b)($c)($d)($e)($f)($g)($h)($i)($j);
  };
};

$exports["runFn0"] = function ($fn) {
  return $fn();
};

$exports["runFn1"] = function ($fn) {
  return function ($a) use(&$fn) {
    return $fn($a);
  };
};

$exports["runFn2"] = function ($fn) {
  return function ($a) use(&$fn) {
    return function ($b) use(&$fn, &$a) {
      return $fn($a, $b);
    };
  };
};

$exports["runFn3"] = function ($fn) {
  return function ($a) use(& $fn) {
    return function ($b) use(& $fn, & $a) {
      return function ($c) use(& $fn, & $a, & $b) {
        return $fn($a, $b, $c);
      };
    };
  };
};

$exports["runFn4"] = function ($fn) {
  return function ($a) use(& $fn) {
    return function ($b) use(& $fn, & $a) {
      return function ($c) use(& $fn, & $a, & $b) {
        return function ($d) use(& $fn, & $a, & $b, & $c) {
          return $fn($a, $b, $c, $d);
        };
      };
    };
  };
};

$exports["runFn5"] = function ($fn) {
  return function ($a) use(& $fn) {
    return function ($b) use(& $fn, & $a) {
      return function ($c) use(& $fn, & $a, & $b) {
        return function ($d) use(& $fn, & $a, & $b, & $c) {
          return function ($e) use(& $fn, & $a, & $b, & $c, & $d) {
            return $fn($a, $b, $c, $d, $e);
          };
        };
      };
    };
  };
};

$exports["runFn6"] = function ($fn) {
  return function ($a) use(& $fn) {
    return function ($b) use(& $fn, & $a) {
      return function ($c) use(& $fn, & $a, & $b) {
        return function ($d) use(& $fn, & $a, & $b, & $c) {
          return function ($e) use(& $fn, & $a, & $b, & $c, & $d) {
            return function ($f) use(& $fn, & $a, & $b, & $c, & $d, & $e) {
              return $fn($a, $b, $c, $d, $e, $f);
            };
          };
        };
      };
    };
  };
};

$exports["runFn7"] = function ($fn) {
  return function ($a) use(& $fn) {
    return function ($b) use(& $fn, & $a) {
      return function ($c) use(& $fn, & $a, & $b) {
        return function ($d) use(& $fn, & $a, & $b, & $c) {
          return function ($e) use(& $fn, & $a, & $b, & $c, & $d) {
            return function ($f) use(& $fn, & $a, & $b, & $c, & $d, & $e) {
              return function ($g) use(& $fn, & $a, & $b, & $c, & $d, & $e, & $f) {
                return $fn($a, $b, $c, $d, $e, $f, $g);
              };
            };
          };
        };
      };
    };
  };
};

$exports["runFn8"] = function ($fn) {
  return function ($a) use(& $fn) {
    return function ($b) use(& $fn, & $a) {
      return function ($c) use(& $fn, & $a, & $b) {
        return function ($d) use(& $fn, & $a, & $b, & $c) {
          return function ($e) use(& $fn, & $a, & $b, & $c, & $d) {
            return function ($f) use(& $fn, & $a, & $b, & $c, & $d, & $e) {
              return function ($g) use(& $fn, & $a, & $b, & $c, & $d, & $e, & $f) {
                return function ($h) use(& $fn, & $a, & $b, & $c, & $d, & $e, & $f, & $g) {
                  return $fn($a, $b, $c, $d, $e, $f, $g, $h);
                };
              };
            };
          };
        };
      };
    };
  };
};

$exports["runFn9"] = function ($fn) {
  return function ($a) use(& $fn) {
    return function ($b) use(& $fn, & $a) {
      return function ($c) use(& $fn, & $a, & $b) {
        return function ($d) use(& $fn, & $a, & $b, & $c) {
          return function ($e) use(& $fn, & $a, & $b, & $c, & $d) {
            return function ($f) use(& $fn, & $a, & $b, & $c, & $d, & $e) {
              return function ($g) use(& $fn, & $a, & $b, & $c, & $d, & $e, & $f) {
                return function ($h) use(& $fn, & $a, & $b, & $c, & $d, & $e, & $f, & $g) {
                  return function ($i) use(& $fn, & $a, & $b, & $c, & $d, & $e, & $f, & $g, & $h) {
                    return $fn($a, $b, $c, $d, $e, $f, $g, $h, $i);
                  };
                };
              };
            };
          };
        };
      };
    };
  };
};

$exports["runFn10"] = function ($fn) {
  return function ($a) use(& $fn) {
    return function ($b) use(& $fn, & $a) {
      return function ($c) use(& $fn, & $a, & $b) {
        return function ($d) use(& $fn, & $a, & $b, & $c) {
          return function ($e) use(& $fn, & $a, & $b, & $c, & $d) {
            return function ($f) use(& $fn, & $a, & $b, & $c, & $d, & $e) {
              return function ($g) use(& $fn, & $a, & $b, & $c, & $d, & $e, & $f) {
                return function ($h) use(& $fn, & $a, & $b, & $c, & $d, & $e, & $f, & $g) {
                  return function ($i) use(& $fn, & $a, & $b, & $c, & $d, & $e, & $f, & $g, & $h) {
                    return function ($j) use(& $fn, & $a, & $b, & $c, & $d, & $e, & $f, & $g, & $h, & $i) {
                      return $fn($a, $b, $c, $d, $e, $f, $g, $h, $i, $j);
                    };
                  };
                };
              };
            };
          };
        };
      };
    };
  };
};
