<?php
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
$PS__Data_Function_Uncurried = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $runFn1 = function ($f) {
        return $f;
    };
    $mkFn1 = function ($f) {
        return $f;
    };
    return [
        'mkFn1' => $mkFn1,
        'runFn1' => $runFn1,
        'mkFn0' => $__foreign['mkFn0'],
        'mkFn2' => $__foreign['mkFn2'],
        'mkFn3' => $__foreign['mkFn3'],
        'mkFn4' => $__foreign['mkFn4'],
        'mkFn5' => $__foreign['mkFn5'],
        'mkFn6' => $__foreign['mkFn6'],
        'mkFn7' => $__foreign['mkFn7'],
        'mkFn8' => $__foreign['mkFn8'],
        'mkFn9' => $__foreign['mkFn9'],
        'mkFn10' => $__foreign['mkFn10'],
        'runFn0' => $__foreign['runFn0'],
        'runFn2' => $__foreign['runFn2'],
        'runFn3' => $__foreign['runFn3'],
        'runFn4' => $__foreign['runFn4'],
        'runFn5' => $__foreign['runFn5'],
        'runFn6' => $__foreign['runFn6'],
        'runFn7' => $__foreign['runFn7'],
        'runFn8' => $__foreign['runFn8'],
        'runFn9' => $__foreign['runFn9'],
        'runFn10' => $__foreign['runFn10']
    ];
})();
