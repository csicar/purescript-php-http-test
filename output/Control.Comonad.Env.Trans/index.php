<?php
require_once(dirname(__FILE__) . '/../Control.Comonad/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad.Trans.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Extend/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Comonad_Env_Trans = (function ()  use (&$PS__Data_Function, &$PS__Data_Tuple, &$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Foldable, &$PS__Data_Traversable, &$PS__Control_Extend, &$PS__Control_Semigroupoid, &$PS__Control_Comonad_Trans_Class, &$PS__Control_Comonad) {
    $EnvT = function ($x) {
        return $x;
    };
    $withEnvT = function ($f)  use (&$PS__Data_Function, &$EnvT, &$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Function, &$EnvT, &$PS__Data_Tuple, &$f) {
            return $PS__Data_Function['apply']($EnvT)($PS__Data_Tuple['Tuple']['constructor']($f($v['value0']), $v['value1']));
        };
    };
    $runEnvT = function ($v) {
        return $v;
    };
    $newtypeEnvT = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $EnvT);
    $mapEnvT = function ($f)  use (&$PS__Data_Function, &$EnvT, &$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Function, &$EnvT, &$PS__Data_Tuple, &$f) {
            return $PS__Data_Function['apply']($EnvT)($PS__Data_Tuple['Tuple']['constructor']($v['value0'], $f($v['value1'])));
        };
    };
    $functorEnvT = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Function, &$EnvT, &$PS__Data_Tuple) {
        return $PS__Data_Functor['Functor'](function ($f)  use (&$PS__Data_Function, &$EnvT, &$PS__Data_Tuple, &$PS__Data_Functor, &$dictFunctor) {
            return function ($v)  use (&$PS__Data_Function, &$EnvT, &$PS__Data_Tuple, &$PS__Data_Functor, &$dictFunctor, &$f) {
                return $PS__Data_Function['apply']($EnvT)($PS__Data_Tuple['Tuple']['constructor']($v['value0'], $PS__Data_Functor['map']($dictFunctor)($f)($v['value1'])));
            };
        });
    };
    $foldableEnvT = function ($dictFoldable)  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['Foldable'](function ($dictMonoid)  use (&$PS__Data_Foldable, &$dictFoldable) {
            return function ($fn)  use (&$PS__Data_Foldable, &$dictFoldable, &$dictMonoid) {
                return function ($v)  use (&$PS__Data_Foldable, &$dictFoldable, &$dictMonoid, &$fn) {
                    return $PS__Data_Foldable['foldMap']($dictFoldable)($dictMonoid)($fn)($v['value1']);
                };
            };
        }, function ($fn)  use (&$PS__Data_Foldable, &$dictFoldable) {
            return function ($a)  use (&$PS__Data_Foldable, &$dictFoldable, &$fn) {
                return function ($v)  use (&$PS__Data_Foldable, &$dictFoldable, &$fn, &$a) {
                    return $PS__Data_Foldable['foldl']($dictFoldable)($fn)($a)($v['value1']);
                };
            };
        }, function ($fn)  use (&$PS__Data_Foldable, &$dictFoldable) {
            return function ($a)  use (&$PS__Data_Foldable, &$dictFoldable, &$fn) {
                return function ($v)  use (&$PS__Data_Foldable, &$dictFoldable, &$fn, &$a) {
                    return $PS__Data_Foldable['foldr']($dictFoldable)($fn)($a)($v['value1']);
                };
            };
        });
    };
    $traversableEnvT = function ($dictTraversable)  use (&$PS__Data_Traversable, &$foldableEnvT, &$functorEnvT, &$PS__Data_Functor, &$EnvT, &$PS__Data_Tuple) {
        return $PS__Data_Traversable['Traversable'](function ()  use (&$foldableEnvT, &$dictTraversable) {
            return $foldableEnvT($dictTraversable['Foldable1']());
        }, function ()  use (&$functorEnvT, &$dictTraversable) {
            return $functorEnvT($dictTraversable['Functor0']());
        }, function ($dictApplicative)  use (&$PS__Data_Functor, &$EnvT, &$PS__Data_Tuple, &$PS__Data_Traversable, &$dictTraversable) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$EnvT, &$PS__Data_Tuple, &$PS__Data_Traversable, &$dictTraversable) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Functor['map']($PS__Data_Functor['functorFn'])($EnvT)($PS__Data_Tuple['Tuple']['create']($v['value0'])))($PS__Data_Traversable['sequence']($dictTraversable)($dictApplicative)($v['value1']));
            };
        }, function ($dictApplicative)  use (&$PS__Data_Functor, &$EnvT, &$PS__Data_Tuple, &$PS__Data_Traversable, &$dictTraversable) {
            return function ($f)  use (&$PS__Data_Functor, &$dictApplicative, &$EnvT, &$PS__Data_Tuple, &$PS__Data_Traversable, &$dictTraversable) {
                return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$EnvT, &$PS__Data_Tuple, &$PS__Data_Traversable, &$dictTraversable, &$f) {
                    return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Functor['map']($PS__Data_Functor['functorFn'])($EnvT)($PS__Data_Tuple['Tuple']['create']($v['value0'])))($PS__Data_Traversable['traverse']($dictTraversable)($dictApplicative)($f)($v['value1']));
                };
            };
        });
    };
    $extendEnvT = function ($dictExtend)  use (&$PS__Control_Extend, &$functorEnvT, &$PS__Data_Function, &$EnvT, &$PS__Data_Tuple, &$PS__Data_Functor, &$PS__Control_Semigroupoid) {
        return $PS__Control_Extend['Extend'](function ()  use (&$functorEnvT, &$dictExtend) {
            return $functorEnvT($dictExtend['Functor0']());
        }, function ($f)  use (&$PS__Data_Function, &$EnvT, &$PS__Data_Tuple, &$PS__Data_Functor, &$dictExtend, &$PS__Control_Extend, &$PS__Control_Semigroupoid) {
            return function ($v)  use (&$PS__Data_Function, &$EnvT, &$PS__Data_Tuple, &$PS__Data_Functor, &$dictExtend, &$f, &$PS__Control_Extend, &$PS__Control_Semigroupoid) {
                return $PS__Data_Function['apply']($EnvT)($PS__Data_Tuple['Tuple']['constructor']($v['value0'], $PS__Data_Functor['map']($dictExtend['Functor0']())($f)($PS__Control_Extend['extend']($dictExtend)($PS__Control_Semigroupoid['composeFlipped']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Tuple['Tuple']['create']($v['value0']))($EnvT))($v['value1']))));
            };
        });
    };
    $comonadTransEnvT = $PS__Control_Comonad_Trans_Class['ComonadTrans'](function ($dictComonad) {
        return function ($v) {
            return $v['value1'];
        };
    });
    $comonadEnvT = function ($dictComonad)  use (&$PS__Control_Comonad, &$extendEnvT) {
        return $PS__Control_Comonad['Comonad'](function ()  use (&$extendEnvT, &$dictComonad) {
            return $extendEnvT($dictComonad['Extend0']());
        }, function ($v)  use (&$PS__Control_Comonad, &$dictComonad) {
            return $PS__Control_Comonad['extract']($dictComonad)($v['value1']);
        });
    };
    return [
        'EnvT' => $EnvT,
        'runEnvT' => $runEnvT,
        'withEnvT' => $withEnvT,
        'mapEnvT' => $mapEnvT,
        'newtypeEnvT' => $newtypeEnvT,
        'functorEnvT' => $functorEnvT,
        'extendEnvT' => $extendEnvT,
        'comonadEnvT' => $comonadEnvT,
        'comonadTransEnvT' => $comonadTransEnvT,
        'foldableEnvT' => $foldableEnvT,
        'traversableEnvT' => $traversableEnvT
    ];
})();
