<?php
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Gen/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Gen.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.List/index.php');
require_once(dirname(__FILE__) . '/../Data.List.Types/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Foreign.Object/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Foreign_Object_Gen = (function ()  use (&$PS__Control_Monad_Gen_Class, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Foreign_Object, &$PS__Data_List_Types, &$PS__Control_Monad_Gen, &$PS__Control_Apply, &$PS__Data_Tuple) {
    $genForeignObject = function ($dictMonadRec)  use (&$PS__Control_Monad_Gen_Class, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Foreign_Object, &$PS__Data_List_Types, &$PS__Control_Monad_Gen, &$PS__Control_Apply, &$PS__Data_Tuple) {
        return function ($dictMonadGen)  use (&$PS__Control_Monad_Gen_Class, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Foreign_Object, &$PS__Data_List_Types, &$PS__Control_Monad_Gen, &$dictMonadRec, &$PS__Control_Apply, &$PS__Data_Tuple) {
            return function ($genKey)  use (&$PS__Control_Monad_Gen_Class, &$dictMonadGen, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Foreign_Object, &$PS__Data_List_Types, &$PS__Control_Monad_Gen, &$dictMonadRec, &$PS__Control_Apply, &$PS__Data_Tuple) {
                return function ($genValue)  use (&$PS__Control_Monad_Gen_Class, &$dictMonadGen, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Foreign_Object, &$PS__Data_List_Types, &$PS__Control_Monad_Gen, &$dictMonadRec, &$PS__Control_Apply, &$PS__Data_Tuple, &$genKey) {
                    return $PS__Control_Monad_Gen_Class['sized']($dictMonadGen)(function ($size)  use (&$PS__Control_Bind, &$dictMonadGen, &$PS__Control_Monad_Gen_Class, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Foreign_Object, &$PS__Data_List_Types, &$PS__Control_Monad_Gen, &$dictMonadRec, &$PS__Control_Apply, &$PS__Data_Tuple, &$genKey, &$genValue) {
                        return $PS__Control_Bind['bind'](($dictMonadGen['Monad0']())['Bind1']())($PS__Control_Monad_Gen_Class['chooseInt']($dictMonadGen)(0)($size))(function ($v)  use (&$PS__Data_Function, &$PS__Control_Monad_Gen_Class, &$dictMonadGen, &$PS__Data_Functor, &$PS__Foreign_Object, &$PS__Data_List_Types, &$PS__Control_Monad_Gen, &$dictMonadRec, &$PS__Control_Apply, &$PS__Data_Tuple, &$genKey, &$genValue) {
                            return $PS__Data_Function['apply']($PS__Control_Monad_Gen_Class['resize']($dictMonadGen)($PS__Data_Function['const']($v)))($PS__Data_Functor['map'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($PS__Foreign_Object['fromFoldable']($PS__Data_List_Types['foldableList']))($PS__Control_Monad_Gen['unfoldable']($dictMonadRec)($dictMonadGen)($PS__Data_List_Types['unfoldableList'])($PS__Control_Apply['apply']((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())($PS__Data_Functor['map'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($PS__Data_Tuple['Tuple']['create'])($genKey))($genValue))));
                        });
                    });
                };
            };
        };
    };
    return [
        'genForeignObject' => $genForeignObject
    ];
})();
