<?php


$exports['unsafeStringify'] = function ($x) {
  return json_encode($x);
};

$exports['unsafeToFixed'] = function ($digits) {
  return function ($n) use (&$digits) {
    return number_format($n, $digits);
  };
};

$exports['unsafeToExponential'] = function ($digits) {
  return function ($n) use (&$digits) {
    return number_format($n); //TODO correct solutio
    // return n.toExponential(digits);
  };
};

$exports['unsafeToPrecision']  = function ($digits) {
  return function ($n) use (&$digits) {
    return number_format($n, $digits);
    // return n.toPrecision(digits);
  };
};

$exports['unsafeDecodeURI'] = function($a) {
  return urldecode($a);
};

$exports['unsafeEncodeURI'] = function($a) {
  return urlencode($a);
};

$exports['unsafeDecodeURIComponent'] = function($a) {
  return urldecode($a); //TODO find correct implementation
};

$exports['unsafeEncodeURIComponent'] = function($a) {
  return urlencode($a); //TODO find correct implementation
};

