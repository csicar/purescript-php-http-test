<?php
$PS__Global_Unsafe = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    return [
        'unsafeStringify' => $__foreign['unsafeStringify'],
        'unsafeToFixed' => $__foreign['unsafeToFixed'],
        'unsafeToExponential' => $__foreign['unsafeToExponential'],
        'unsafeToPrecision' => $__foreign['unsafeToPrecision'],
        'unsafeDecodeURI' => $__foreign['unsafeDecodeURI'],
        'unsafeEncodeURI' => $__foreign['unsafeEncodeURI'],
        'unsafeDecodeURIComponent' => $__foreign['unsafeDecodeURIComponent'],
        'unsafeEncodeURIComponent' => $__foreign['unsafeEncodeURIComponent']
    ];
})();
