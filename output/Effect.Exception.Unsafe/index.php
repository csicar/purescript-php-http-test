<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Effect.Exception/index.php');
require_once(dirname(__FILE__) . '/../Effect.Unsafe/index.php');
$PS__Effect_Exception_Unsafe = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Effect_Unsafe, &$PS__Effect_Exception) {
    $unsafeThrowException = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Effect_Unsafe['unsafePerformEffect'])($PS__Effect_Exception['throwException']);
    $unsafeThrow = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unsafeThrowException)($PS__Effect_Exception['error']);
    return [
        'unsafeThrowException' => $unsafeThrowException,
        'unsafeThrow' => $unsafeThrow
    ];
})();
