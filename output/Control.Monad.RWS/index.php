<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.RWS.Trans/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Reader.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.State.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Trans.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Writer.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Identity/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_RWS = (function ()  use (&$PS__Control_Monad_RWS_Trans, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Identity, &$PS__Control_Semigroupoid, &$PS__Data_Newtype) {
    $withRWS = $PS__Control_Monad_RWS_Trans['withRWST'];
    $rws = function ($f)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Identity) {
        return function ($r)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Identity, &$f) {
            return function ($s)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Identity, &$f, &$r) {
                return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($PS__Data_Identity['applicativeIdentity']))($f($r)($s));
            };
        };
    };
    $runRWS = function ($m) {
        return function ($r)  use (&$m) {
            return function ($s)  use (&$m, &$r) {
                $v = $m($r)($s);
                return $v;
            };
        };
    };
    $mapRWS = function ($f)  use (&$PS__Control_Monad_RWS_Trans, &$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$PS__Data_Identity) {
        return $PS__Control_Monad_RWS_Trans['mapRWST']($PS__Control_Semigroupoid['composeFlipped']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity']))($PS__Control_Semigroupoid['composeFlipped']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($PS__Data_Identity['Identity'])));
    };
    $execRWS = function ($m)  use (&$PS__Data_Function, &$PS__Data_Newtype, &$PS__Data_Identity, &$PS__Control_Monad_RWS_Trans) {
        return function ($r)  use (&$PS__Data_Function, &$PS__Data_Newtype, &$PS__Data_Identity, &$PS__Control_Monad_RWS_Trans, &$m) {
            return function ($s)  use (&$PS__Data_Function, &$PS__Data_Newtype, &$PS__Data_Identity, &$PS__Control_Monad_RWS_Trans, &$m, &$r) {
                return $PS__Data_Function['apply']($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity']))($PS__Control_Monad_RWS_Trans['execRWST']($PS__Data_Identity['monadIdentity'])($m)($r)($s));
            };
        };
    };
    $evalRWS = function ($m)  use (&$PS__Data_Function, &$PS__Data_Newtype, &$PS__Data_Identity, &$PS__Control_Monad_RWS_Trans) {
        return function ($r)  use (&$PS__Data_Function, &$PS__Data_Newtype, &$PS__Data_Identity, &$PS__Control_Monad_RWS_Trans, &$m) {
            return function ($s)  use (&$PS__Data_Function, &$PS__Data_Newtype, &$PS__Data_Identity, &$PS__Control_Monad_RWS_Trans, &$m, &$r) {
                return $PS__Data_Function['apply']($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity']))($PS__Control_Monad_RWS_Trans['evalRWST']($PS__Data_Identity['monadIdentity'])($m)($r)($s));
            };
        };
    };
    return [
        'rws' => $rws,
        'runRWS' => $runRWS,
        'evalRWS' => $evalRWS,
        'execRWS' => $execRWS,
        'mapRWS' => $mapRWS,
        'withRWS' => $withRWS
    ];
})();
