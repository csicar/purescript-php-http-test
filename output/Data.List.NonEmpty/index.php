<?php
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Boolean/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.List/index.php');
require_once(dirname(__FILE__) . '/../Data.List.Types/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Partial.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_List_NonEmpty = (function ()  use (&$PS__Data_NonEmpty, &$PS__Data_List, &$PS__Data_Semigroup_Traversable, &$PS__Data_List_Types, &$PS__Data_Tuple, &$PS__Partial_Unsafe, &$PS__Data_Semigroup, &$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$PS__Data_Ring, &$PS__Data_Unfoldable, &$PS__Data_Ord, &$PS__Data_Semiring, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Category) {
    $zipWith = function ($f)  use (&$PS__Data_NonEmpty, &$PS__Data_List) {
        return function ($v)  use (&$PS__Data_NonEmpty, &$f, &$PS__Data_List) {
            return function ($v1)  use (&$PS__Data_NonEmpty, &$f, &$v, &$PS__Data_List) {
                return $PS__Data_NonEmpty['NonEmpty']['constructor']($f($v['value0'])($v1['value0']), $PS__Data_List['zipWith']($f)($v['value1'])($v1['value1']));
            };
        };
    };
    $zipWithA = function ($dictApplicative)  use (&$PS__Data_Semigroup_Traversable, &$PS__Data_List_Types, &$zipWith) {
        return function ($f)  use (&$PS__Data_Semigroup_Traversable, &$PS__Data_List_Types, &$dictApplicative, &$zipWith) {
            return function ($xs)  use (&$PS__Data_Semigroup_Traversable, &$PS__Data_List_Types, &$dictApplicative, &$zipWith, &$f) {
                return function ($ys)  use (&$PS__Data_Semigroup_Traversable, &$PS__Data_List_Types, &$dictApplicative, &$zipWith, &$f, &$xs) {
                    return $PS__Data_Semigroup_Traversable['sequence1']($PS__Data_List_Types['traversable1NonEmptyList'])($dictApplicative['Apply0']())($zipWith($f)($xs)($ys));
                };
            };
        };
    };
    $zip = $zipWith($PS__Data_Tuple['Tuple']['create']);
    $wrappedOperation2 = function ($name)  use (&$PS__Data_List_Types, &$PS__Data_NonEmpty, &$PS__Partial_Unsafe, &$PS__Data_Semigroup) {
        return function ($f)  use (&$PS__Data_List_Types, &$PS__Data_NonEmpty, &$PS__Partial_Unsafe, &$PS__Data_Semigroup, &$name) {
            return function ($v)  use (&$f, &$PS__Data_List_Types, &$PS__Data_NonEmpty, &$PS__Partial_Unsafe, &$PS__Data_Semigroup, &$name) {
                return function ($v1)  use (&$f, &$PS__Data_List_Types, &$v, &$PS__Data_NonEmpty, &$PS__Partial_Unsafe, &$PS__Data_Semigroup, &$name) {
                    $v2 = $f($PS__Data_List_Types['Cons']['constructor']($v['value0'], $v['value1']))($PS__Data_List_Types['Cons']['constructor']($v1['value0'], $v1['value1']));
                    if ($v2['type'] === 'Cons') {
                        return $PS__Data_NonEmpty['NonEmpty']['constructor']($v2['value0'], $v2['value1']);
                    };
                    if ($v2['type'] === 'Nil') {
                        return $PS__Partial_Unsafe['unsafeCrashWith']($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('Impossible: empty list in NonEmptyList ')($name));
                    };
                    throw new \Exception('Failed pattern match at Data.List.NonEmpty line 101, column 3 - line 103, column 81: ' . var_dump([ $v2['constructor']['name'] ]));
                };
            };
        };
    };
    $wrappedOperation = function ($name)  use (&$PS__Data_List_Types, &$PS__Data_NonEmpty, &$PS__Partial_Unsafe, &$PS__Data_Semigroup) {
        return function ($f)  use (&$PS__Data_List_Types, &$PS__Data_NonEmpty, &$PS__Partial_Unsafe, &$PS__Data_Semigroup, &$name) {
            return function ($v)  use (&$f, &$PS__Data_List_Types, &$PS__Data_NonEmpty, &$PS__Partial_Unsafe, &$PS__Data_Semigroup, &$name) {
                $v1 = $f($PS__Data_List_Types['Cons']['constructor']($v['value0'], $v['value1']));
                if ($v1['type'] === 'Cons') {
                    return $PS__Data_NonEmpty['NonEmpty']['constructor']($v1['value0'], $v1['value1']);
                };
                if ($v1['type'] === 'Nil') {
                    return $PS__Partial_Unsafe['unsafeCrashWith']($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('Impossible: empty list in NonEmptyList ')($name));
                };
                throw new \Exception('Failed pattern match at Data.List.NonEmpty line 88, column 3 - line 90, column 81: ' . var_dump([ $v1['constructor']['name'] ]));
            };
        };
    };
    $updateAt = function ($i)  use (&$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_NonEmpty, &$PS__Data_Boolean, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$PS__Data_List_Types, &$PS__Data_List, &$PS__Data_Ring) {
        return function ($a)  use (&$PS__Data_Eq, &$i, &$PS__Data_Maybe, &$PS__Data_NonEmpty, &$PS__Data_Boolean, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$PS__Data_List_Types, &$PS__Data_List, &$PS__Data_Ring) {
            return function ($v)  use (&$PS__Data_Eq, &$i, &$PS__Data_Maybe, &$PS__Data_NonEmpty, &$a, &$PS__Data_Boolean, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$PS__Data_List_Types, &$PS__Data_List, &$PS__Data_Ring) {
                if ($PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($i)(0)) {
                    return $PS__Data_Maybe['Just']['constructor']($PS__Data_NonEmpty['NonEmpty']['constructor']($a, $v['value1']));
                };
                if ($PS__Data_Boolean['otherwise']) {
                    return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_List_Types['NonEmptyList'])(function ($v1)  use (&$PS__Data_NonEmpty, &$v) {
                        return $PS__Data_NonEmpty['NonEmpty']['constructor']($v['value0'], $v1);
                    }))($PS__Data_List['updateAt']($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($i)(1))($a)($v['value1']));
                };
                throw new \Exception('Failed pattern match at Data.List.NonEmpty line 187, column 1 - line 187, column 75: ' . var_dump([ $i['constructor']['name'], $a['constructor']['name'], $v['constructor']['name'] ]));
            };
        };
    };
    $unzip = function ($ts)  use (&$PS__Data_Tuple, &$PS__Data_Functor, &$PS__Data_List_Types) {
        return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_Functor['map']($PS__Data_List_Types['functorNonEmptyList'])($PS__Data_Tuple['fst'])($ts), $PS__Data_Functor['map']($PS__Data_List_Types['functorNonEmptyList'])($PS__Data_Tuple['snd'])($ts));
    };
    $unsnoc = function ($v)  use (&$PS__Data_List, &$PS__Data_List_Types) {
        $v1 = $PS__Data_List['unsnoc']($v['value1']);
        if ($v1['type'] === 'Nothing') {
            return [
                'init' => $PS__Data_List_Types['Nil'](),
                'last' => $v['value0']
            ];
        };
        if ($v1['type'] === 'Just') {
            return [
                'init' => $PS__Data_List_Types['Cons']['constructor']($v['value0'], $v1['value0']['init']),
                'last' => $v1['value0']['last']
            ];
        };
        throw new \Exception('Failed pattern match at Data.List.NonEmpty line 149, column 35 - line 151, column 50: ' . var_dump([ $v1['constructor']['name'] ]));
    };
    $unionBy = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($wrappedOperation2('unionBy'))($PS__Data_List['unionBy']);
    $union = function ($dictEq)  use (&$wrappedOperation2, &$PS__Data_List) {
        return $wrappedOperation2('union')($PS__Data_List['union']($dictEq));
    };
    $uncons = function ($v) {
        return [
            'head' => $v['value0'],
            'tail' => $v['value1']
        ];
    };
    $toList = function ($v)  use (&$PS__Data_List_Types) {
        return $PS__Data_List_Types['Cons']['constructor']($v['value0'], $v['value1']);
    };
    $toUnfoldable = function ($dictUnfoldable)  use (&$PS__Control_Semigroupoid, &$PS__Data_Unfoldable, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_List, &$toList) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Unfoldable['unfoldr']($dictUnfoldable)(function ($xs)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_List) {
            return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($rec)  use (&$PS__Data_Tuple) {
                return $PS__Data_Tuple['Tuple']['constructor']($rec['head'], $rec['tail']);
            })($PS__Data_List['uncons']($xs));
        }))($toList);
    };
    $tail = function ($v) {
        return $v['value1'];
    };
    $sortBy = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($wrappedOperation('sortBy'))($PS__Data_List['sortBy']);
    $sort = function ($dictOrd)  use (&$sortBy, &$PS__Data_Ord) {
        return function ($xs)  use (&$sortBy, &$PS__Data_Ord, &$dictOrd) {
            return $sortBy($PS__Data_Ord['compare']($dictOrd))($xs);
        };
    };
    $snoc = function ($v)  use (&$PS__Data_NonEmpty, &$PS__Data_List) {
        return function ($y)  use (&$PS__Data_NonEmpty, &$v, &$PS__Data_List) {
            return $PS__Data_NonEmpty['NonEmpty']['constructor']($v['value0'], $PS__Data_List['snoc']($v['value1'])($y));
        };
    };
    $singleton = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_List_Types['NonEmptyList'])($PS__Data_NonEmpty['singleton']($PS__Data_List_Types['plusList']));
    $reverse = $wrappedOperation('reverse')($PS__Data_List['reverse']);
    $nubBy = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($wrappedOperation('nubBy'))($PS__Data_List['nubBy']);
    $nub = function ($dictEq)  use (&$wrappedOperation, &$PS__Data_List) {
        return $wrappedOperation('nub')($PS__Data_List['nub']($dictEq));
    };
    $modifyAt = function ($i)  use (&$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_NonEmpty, &$PS__Data_Boolean, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$PS__Data_List_Types, &$PS__Data_List, &$PS__Data_Ring) {
        return function ($f)  use (&$PS__Data_Eq, &$i, &$PS__Data_Maybe, &$PS__Data_NonEmpty, &$PS__Data_Boolean, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$PS__Data_List_Types, &$PS__Data_List, &$PS__Data_Ring) {
            return function ($v)  use (&$PS__Data_Eq, &$i, &$PS__Data_Maybe, &$PS__Data_NonEmpty, &$f, &$PS__Data_Boolean, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$PS__Data_List_Types, &$PS__Data_List, &$PS__Data_Ring) {
                if ($PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($i)(0)) {
                    return $PS__Data_Maybe['Just']['constructor']($PS__Data_NonEmpty['NonEmpty']['constructor']($f($v['value0']), $v['value1']));
                };
                if ($PS__Data_Boolean['otherwise']) {
                    return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_List_Types['NonEmptyList'])(function ($v1)  use (&$PS__Data_NonEmpty, &$v) {
                        return $PS__Data_NonEmpty['NonEmpty']['constructor']($v['value0'], $v1);
                    }))($PS__Data_List['modifyAt']($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($i)(1))($f)($v['value1']));
                };
                throw new \Exception('Failed pattern match at Data.List.NonEmpty line 192, column 1 - line 192, column 82: ' . var_dump([ $i['constructor']['name'], $f['constructor']['name'], $v['constructor']['name'] ]));
            };
        };
    };
    $mapWithIndex = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($wrappedOperation('mapWithIndex'))($PS__Data_List['mapWithIndex']);
    $lift = function ($f)  use (&$PS__Data_List_Types) {
        return function ($v)  use (&$f, &$PS__Data_List_Types) {
            return $f($PS__Data_List_Types['Cons']['constructor']($v['value0'], $v['value1']));
        };
    };
    $mapMaybe = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($lift)($PS__Data_List['mapMaybe']);
    $partition = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($lift)($PS__Data_List['partition']);
    $span = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($lift)($PS__Data_List['span']);
    $take = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($lift)($PS__Data_List['take']);
    $takeWhile = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($lift)($PS__Data_List['takeWhile']);
    $length = function ($v)  use (&$PS__Data_Semiring, &$PS__Data_List) {
        return $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])(1)($PS__Data_List['length']($v['value1']));
    };
    $last = function ($v)  use (&$PS__Data_Maybe, &$PS__Data_List) {
        return $PS__Data_Maybe['fromMaybe']($v['value0'])($PS__Data_List['last']($v['value1']));
    };
    $intersectBy = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($wrappedOperation2('intersectBy'))($PS__Data_List['intersectBy']);
    $intersect = function ($dictEq)  use (&$wrappedOperation2, &$PS__Data_List) {
        return $wrappedOperation2('intersect')($PS__Data_List['intersect']($dictEq));
    };
    $insertAt = function ($i)  use (&$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_NonEmpty, &$PS__Data_List_Types, &$PS__Data_Boolean, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$PS__Data_List, &$PS__Data_Ring) {
        return function ($a)  use (&$PS__Data_Eq, &$i, &$PS__Data_Maybe, &$PS__Data_NonEmpty, &$PS__Data_List_Types, &$PS__Data_Boolean, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$PS__Data_List, &$PS__Data_Ring) {
            return function ($v)  use (&$PS__Data_Eq, &$i, &$PS__Data_Maybe, &$PS__Data_NonEmpty, &$a, &$PS__Data_List_Types, &$PS__Data_Boolean, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$PS__Data_List, &$PS__Data_Ring) {
                if ($PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($i)(0)) {
                    return $PS__Data_Maybe['Just']['constructor']($PS__Data_NonEmpty['NonEmpty']['constructor']($a, $PS__Data_List_Types['Cons']['constructor']($v['value0'], $v['value1'])));
                };
                if ($PS__Data_Boolean['otherwise']) {
                    return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_List_Types['NonEmptyList'])(function ($v1)  use (&$PS__Data_NonEmpty, &$v) {
                        return $PS__Data_NonEmpty['NonEmpty']['constructor']($v['value0'], $v1);
                    }))($PS__Data_List['insertAt']($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($i)(1))($a)($v['value1']));
                };
                throw new \Exception('Failed pattern match at Data.List.NonEmpty line 182, column 1 - line 182, column 75: ' . var_dump([ $i['constructor']['name'], $a['constructor']['name'], $v['constructor']['name'] ]));
            };
        };
    };
    $init = function ($v)  use (&$PS__Data_Maybe, &$PS__Data_List_Types, &$PS__Data_List) {
        return $PS__Data_Maybe['maybe']($PS__Data_List_Types['Nil']())(function ($v1)  use (&$PS__Data_List_Types, &$v) {
            return $PS__Data_List_Types['Cons']['constructor']($v['value0'], $v1);
        })($PS__Data_List['init']($v['value1']));
    };
    $index = function ($v)  use (&$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Data_List, &$PS__Data_Ring) {
        return function ($i)  use (&$PS__Data_Eq, &$PS__Data_Maybe, &$v, &$PS__Data_Boolean, &$PS__Data_List, &$PS__Data_Ring) {
            if ($PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($i)(0)) {
                return $PS__Data_Maybe['Just']['constructor']($v['value0']);
            };
            if ($PS__Data_Boolean['otherwise']) {
                return $PS__Data_List['index']($v['value1'])($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($i)(1));
            };
            throw new \Exception('Failed pattern match at Data.List.NonEmpty line 156, column 1 - line 156, column 52: ' . var_dump([ $v['constructor']['name'], $i['constructor']['name'] ]));
        };
    };
    $head = function ($v) {
        return $v['value0'];
    };
    $groupBy = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($wrappedOperation('groupBy'))($PS__Data_List['groupBy']);
    $group__prime = function ($dictOrd)  use (&$wrappedOperation, &$PS__Data_List) {
        return $wrappedOperation('group\'')($PS__Data_List['group\'']($dictOrd));
    };
    $group = function ($dictEq)  use (&$wrappedOperation, &$PS__Data_List) {
        return $wrappedOperation('group')($PS__Data_List['group']($dictEq));
    };
    $fromList = function ($v)  use (&$PS__Data_Maybe, &$PS__Data_NonEmpty) {
        if ($v['type'] === 'Nil') {
            return $PS__Data_Maybe['Nothing']();
        };
        if ($v['type'] === 'Cons') {
            return $PS__Data_Maybe['Just']['constructor']($PS__Data_NonEmpty['NonEmpty']['constructor']($v['value0'], $v['value1']));
        };
        throw new \Exception('Failed pattern match at Data.List.NonEmpty line 117, column 1 - line 117, column 57: ' . var_dump([ $v['constructor']['name'] ]));
    };
    $fromFoldable = function ($dictFoldable)  use (&$PS__Control_Semigroupoid, &$fromList, &$PS__Data_List) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($fromList)($PS__Data_List['fromFoldable']($dictFoldable));
    };
    $foldM = function ($dictMonad)  use (&$PS__Control_Bind, &$PS__Data_List) {
        return function ($f)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Data_List) {
            return function ($a)  use (&$PS__Control_Bind, &$dictMonad, &$f, &$PS__Data_List) {
                return function ($v)  use (&$PS__Control_Bind, &$dictMonad, &$f, &$a, &$PS__Data_List) {
                    return $PS__Control_Bind['bind']($dictMonad['Bind1']())($f($a)($v['value0']))(function ($a__prime)  use (&$PS__Data_List, &$dictMonad, &$f, &$v) {
                        return $PS__Data_List['foldM']($dictMonad)($f)($a__prime)($v['value1']);
                    });
                };
            };
        };
    };
    $findLastIndex = function ($f)  use (&$PS__Data_List, &$PS__Data_Maybe, &$PS__Data_Semiring, &$PS__Data_Boolean) {
        return function ($v)  use (&$PS__Data_List, &$f, &$PS__Data_Maybe, &$PS__Data_Semiring, &$PS__Data_Boolean) {
            $v1 = $PS__Data_List['findLastIndex']($f)($v['value1']);
            if ($v1['type'] === 'Just') {
                return $PS__Data_Maybe['Just']['constructor']($PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($v1['value0'])(1));
            };
            if ($v1['type'] === 'Nothing') {
                if ($f($v['value0'])) {
                    return $PS__Data_Maybe['Just']['constructor'](0);
                };
                if ($PS__Data_Boolean['otherwise']) {
                    return $PS__Data_Maybe['Nothing']();
                };
            };
            throw new \Exception('Failed pattern match at Data.List.NonEmpty line 176, column 3 - line 180, column 29: ' . var_dump([ $v1['constructor']['name'] ]));
        };
    };
    $findIndex = function ($f)  use (&$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Data_Functor, &$PS__Data_Semiring, &$PS__Data_List) {
        return function ($v)  use (&$f, &$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Data_Functor, &$PS__Data_Semiring, &$PS__Data_List) {
            if ($f($v['value0'])) {
                return $PS__Data_Maybe['Just']['constructor'](0);
            };
            if ($PS__Data_Boolean['otherwise']) {
                return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($v1)  use (&$PS__Data_Semiring) {
                    return $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($v1)(1);
                })($PS__Data_List['findIndex']($f)($v['value1']));
            };
            throw new \Exception('Failed pattern match at Data.List.NonEmpty line 169, column 1 - line 169, column 69: ' . var_dump([ $f['constructor']['name'], $v['constructor']['name'] ]));
        };
    };
    $filterM = function ($dictMonad)  use (&$PS__Control_Semigroupoid, &$lift, &$PS__Data_List) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($lift)($PS__Data_List['filterM']($dictMonad));
    };
    $filter = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($lift)($PS__Data_List['filter']);
    $elemLastIndex = function ($dictEq)  use (&$findLastIndex, &$PS__Data_Eq) {
        return function ($x)  use (&$findLastIndex, &$PS__Data_Eq, &$dictEq) {
            return $findLastIndex(function ($v)  use (&$PS__Data_Eq, &$dictEq, &$x) {
                return $PS__Data_Eq['eq']($dictEq)($v)($x);
            });
        };
    };
    $elemIndex = function ($dictEq)  use (&$findIndex, &$PS__Data_Eq) {
        return function ($x)  use (&$findIndex, &$PS__Data_Eq, &$dictEq) {
            return $findIndex(function ($v)  use (&$PS__Data_Eq, &$dictEq, &$x) {
                return $PS__Data_Eq['eq']($dictEq)($v)($x);
            });
        };
    };
    $dropWhile = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($lift)($PS__Data_List['dropWhile']);
    $drop = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($lift)($PS__Data_List['drop']);
    $cons = function ($y)  use (&$PS__Data_NonEmpty, &$PS__Data_List_Types) {
        return function ($v)  use (&$PS__Data_NonEmpty, &$y, &$PS__Data_List_Types) {
            return $PS__Data_NonEmpty['NonEmpty']['constructor']($y, $PS__Data_List_Types['Cons']['constructor']($v['value0'], $v['value1']));
        };
    };
    $concatMap = $PS__Data_Function['flip']($PS__Control_Bind['bind']($PS__Data_List_Types['bindNonEmptyList']));
    $concat = function ($v)  use (&$PS__Control_Bind, &$PS__Data_List_Types, &$PS__Control_Category) {
        return $PS__Control_Bind['bind']($PS__Data_List_Types['bindNonEmptyList'])($v)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    };
    $catMaybes = $lift($PS__Data_List['catMaybes']);
    $appendFoldable = function ($dictFoldable)  use (&$PS__Data_NonEmpty, &$PS__Data_Semigroup, &$PS__Data_List_Types, &$PS__Data_List) {
        return function ($v)  use (&$PS__Data_NonEmpty, &$PS__Data_Semigroup, &$PS__Data_List_Types, &$PS__Data_List, &$dictFoldable) {
            return function ($ys)  use (&$PS__Data_NonEmpty, &$v, &$PS__Data_Semigroup, &$PS__Data_List_Types, &$PS__Data_List, &$dictFoldable) {
                return $PS__Data_NonEmpty['NonEmpty']['constructor']($v['value0'], $PS__Data_Semigroup['append']($PS__Data_List_Types['semigroupList'])($v['value1'])($PS__Data_List['fromFoldable']($dictFoldable)($ys)));
            };
        };
    };
    return [
        'toUnfoldable' => $toUnfoldable,
        'fromFoldable' => $fromFoldable,
        'fromList' => $fromList,
        'toList' => $toList,
        'singleton' => $singleton,
        'length' => $length,
        'cons' => $cons,
        'snoc' => $snoc,
        'head' => $head,
        'last' => $last,
        'tail' => $tail,
        'init' => $init,
        'uncons' => $uncons,
        'unsnoc' => $unsnoc,
        'index' => $index,
        'elemIndex' => $elemIndex,
        'elemLastIndex' => $elemLastIndex,
        'findIndex' => $findIndex,
        'findLastIndex' => $findLastIndex,
        'insertAt' => $insertAt,
        'updateAt' => $updateAt,
        'modifyAt' => $modifyAt,
        'reverse' => $reverse,
        'concat' => $concat,
        'concatMap' => $concatMap,
        'filter' => $filter,
        'filterM' => $filterM,
        'mapMaybe' => $mapMaybe,
        'catMaybes' => $catMaybes,
        'appendFoldable' => $appendFoldable,
        'mapWithIndex' => $mapWithIndex,
        'sort' => $sort,
        'sortBy' => $sortBy,
        'take' => $take,
        'takeWhile' => $takeWhile,
        'drop' => $drop,
        'dropWhile' => $dropWhile,
        'span' => $span,
        'group' => $group,
        'group\'' => $group__prime,
        'groupBy' => $groupBy,
        'partition' => $partition,
        'nub' => $nub,
        'nubBy' => $nubBy,
        'union' => $union,
        'unionBy' => $unionBy,
        'intersect' => $intersect,
        'intersectBy' => $intersectBy,
        'zipWith' => $zipWith,
        'zipWithA' => $zipWithA,
        'zip' => $zip,
        'unzip' => $unzip,
        'foldM' => $foldM
    ];
})();
