<?php
require_once(dirname(__FILE__) . '/../Control.Comonad.Store.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad.Store.Trans/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Identity/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Comonad_Store = (function ()  use (&$PS__Data_Function, &$PS__Control_Comonad_Store_Trans, &$PS__Data_Tuple, &$PS__Data_Functor, &$PS__Data_Newtype, &$PS__Data_Identity) {
    $store = function ($f)  use (&$PS__Data_Function, &$PS__Control_Comonad_Store_Trans, &$PS__Data_Tuple) {
        return function ($x)  use (&$PS__Data_Function, &$PS__Control_Comonad_Store_Trans, &$PS__Data_Tuple, &$f) {
            return $PS__Data_Function['apply']($PS__Control_Comonad_Store_Trans['StoreT'])($PS__Data_Tuple['Tuple']['constructor']($f, $x));
        };
    };
    $runStore = function ($v)  use (&$PS__Data_Tuple, &$PS__Data_Functor, &$PS__Data_Newtype, &$PS__Data_Identity) {
        return $PS__Data_Tuple['swap']($PS__Data_Functor['map']($PS__Data_Tuple['functorTuple'])($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity']))($PS__Data_Tuple['swap']($v)));
    };
    return [
        'runStore' => $runStore,
        'store' => $store
    ];
})();
