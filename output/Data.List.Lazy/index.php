<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Boolean/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Data.List.Lazy.Types/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_List_Lazy = (function ()  use (&$PS__Data_List_Lazy_Types, &$PS__Control_Apply, &$PS__Data_Lazy, &$PS__Data_Functor, &$PS__Data_Newtype, &$PS__Data_Traversable, &$PS__Data_Tuple, &$PS__Data_Ring, &$PS__Data_Foldable, &$PS__Data_Maybe, &$PS__Data_Unfoldable, &$PS__Control_Semigroupoid, &$PS__Data_Ord, &$PS__Data_Function, &$PS__Control_Monad_Rec_Class, &$PS__Data_Eq, &$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Control_Lazy, &$PS__Data_Semiring, &$PS__Control_Applicative, &$PS__Data_Boolean, &$PS__Control_Bind, &$PS__Control_Alt, &$PS__Data_NonEmpty, &$PS__Data_HeytingAlgebra, &$PS__Control_Category) {
    $Pattern = function ($x) {
        return $x;
    };
    $zipWith = function ($f)  use (&$PS__Data_List_Lazy_Types, &$zipWith, &$PS__Control_Apply, &$PS__Data_Lazy, &$PS__Data_Functor, &$PS__Data_Newtype) {
        return function ($xs)  use (&$PS__Data_List_Lazy_Types, &$f, &$zipWith, &$PS__Control_Apply, &$PS__Data_Lazy, &$PS__Data_Functor, &$PS__Data_Newtype) {
            return function ($ys)  use (&$PS__Data_List_Lazy_Types, &$f, &$zipWith, &$PS__Control_Apply, &$PS__Data_Lazy, &$PS__Data_Functor, &$PS__Data_Newtype, &$xs) {
                $go = function ($v)  use (&$PS__Data_List_Lazy_Types, &$f, &$zipWith) {
                    return function ($v1)  use (&$v, &$PS__Data_List_Lazy_Types, &$f, &$zipWith) {
                        if ($v['type'] === 'Nil') {
                            return $PS__Data_List_Lazy_Types['Nil']();
                        };
                        if ($v1['type'] === 'Nil') {
                            return $PS__Data_List_Lazy_Types['Nil']();
                        };
                        if ($v['type'] === 'Cons' && $v1['type'] === 'Cons') {
                            return $PS__Data_List_Lazy_Types['Cons']['constructor']($f($v['value0'])($v1['value0']), $zipWith($f)($v['value1'])($v1['value1']));
                        };
                        throw new \Exception('Failed pattern match at Data.List.Lazy line 692, column 3 - line 692, column 35: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
                    };
                };
                return $PS__Control_Apply['apply']($PS__Data_Lazy['applyLazy'])($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go)($PS__Data_Newtype['unwrap']($PS__Data_List_Lazy_Types['newtypeList'])($xs)))($PS__Data_Newtype['unwrap']($PS__Data_List_Lazy_Types['newtypeList'])($ys));
            };
        };
    };
    $zipWithA = function ($dictApplicative)  use (&$PS__Data_Traversable, &$PS__Data_List_Lazy_Types, &$zipWith) {
        return function ($f)  use (&$PS__Data_Traversable, &$PS__Data_List_Lazy_Types, &$dictApplicative, &$zipWith) {
            return function ($xs)  use (&$PS__Data_Traversable, &$PS__Data_List_Lazy_Types, &$dictApplicative, &$zipWith, &$f) {
                return function ($ys)  use (&$PS__Data_Traversable, &$PS__Data_List_Lazy_Types, &$dictApplicative, &$zipWith, &$f, &$xs) {
                    return $PS__Data_Traversable['sequence']($PS__Data_List_Lazy_Types['traversableList'])($dictApplicative)($zipWith($f)($xs)($ys));
                };
            };
        };
    };
    $zip = $zipWith($PS__Data_Tuple['Tuple']['create']);
    $updateAt = function ($n)  use (&$PS__Data_List_Lazy_Types, &$updateAt, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
        return function ($x)  use (&$PS__Data_List_Lazy_Types, &$updateAt, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Lazy, &$n, &$PS__Data_Newtype) {
            return function ($xs)  use (&$PS__Data_List_Lazy_Types, &$x, &$updateAt, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Lazy, &$n, &$PS__Data_Newtype) {
                $go = function ($v)  use (&$PS__Data_List_Lazy_Types, &$x, &$updateAt, &$PS__Data_Ring) {
                    return function ($v1)  use (&$PS__Data_List_Lazy_Types, &$v, &$x, &$updateAt, &$PS__Data_Ring) {
                        if ($v1['type'] === 'Nil') {
                            return $PS__Data_List_Lazy_Types['Nil']();
                        };
                        if ($v === 0 && $v1['type'] === 'Cons') {
                            return $PS__Data_List_Lazy_Types['Cons']['constructor']($x, $v1['value1']);
                        };
                        if ($v1['type'] === 'Cons') {
                            return $PS__Data_List_Lazy_Types['Cons']['constructor']($v1['value0'], $updateAt($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(1))($x)($v1['value1']));
                        };
                        throw new \Exception('Failed pattern match at Data.List.Lazy line 366, column 3 - line 366, column 17: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
                    };
                };
                return $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go($n))($PS__Data_Newtype['unwrap']($PS__Data_List_Lazy_Types['newtypeList'])($xs));
            };
        };
    };
    $unzip = $PS__Data_Foldable['foldr']($PS__Data_List_Lazy_Types['foldableList'])(function ($v)  use (&$PS__Data_Tuple, &$PS__Data_List_Lazy_Types) {
        return function ($v1)  use (&$PS__Data_Tuple, &$PS__Data_List_Lazy_Types, &$v) {
            return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_List_Lazy_Types['cons']($v['value0'])($v1['value0']), $PS__Data_List_Lazy_Types['cons']($v['value1'])($v1['value1']));
        };
    })($PS__Data_Tuple['Tuple']['constructor']($PS__Data_List_Lazy_Types['nil'], $PS__Data_List_Lazy_Types['nil']));
    $uncons = function ($xs)  use (&$PS__Data_List_Lazy_Types, &$PS__Data_Maybe) {
        $v = $PS__Data_List_Lazy_Types['step']($xs);
        if ($v['type'] === 'Nil') {
            return $PS__Data_Maybe['Nothing']();
        };
        if ($v['type'] === 'Cons') {
            return $PS__Data_Maybe['Just']['constructor']([
                'head' => $v['value0'],
                'tail' => $v['value1']
            ]);
        };
        throw new \Exception('Failed pattern match at Data.List.Lazy line 284, column 13 - line 286, column 44: ' . var_dump([ $v['constructor']['name'] ]));
    };
    $toUnfoldable = function ($dictUnfoldable)  use (&$PS__Data_Unfoldable, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Tuple, &$uncons) {
        return $PS__Data_Unfoldable['unfoldr']($dictUnfoldable)(function ($xs)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Tuple, &$uncons) {
            return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($rec)  use (&$PS__Data_Tuple) {
                return $PS__Data_Tuple['Tuple']['constructor']($rec['head'], $rec['tail']);
            })($uncons($xs));
        });
    };
    $takeWhile = function ($p)  use (&$PS__Data_List_Lazy_Types, &$takeWhile, &$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
        $go = function ($v)  use (&$p, &$PS__Data_List_Lazy_Types, &$takeWhile) {
            if ($v['type'] === 'Cons' && $p($v['value0'])) {
                return $PS__Data_List_Lazy_Types['Cons']['constructor']($v['value0'], $takeWhile($p)($v['value1']));
            };
            return $PS__Data_List_Lazy_Types['Nil']();
        };
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_List_Lazy_Types['List'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go))($PS__Data_Newtype['unwrap']($PS__Data_List_Lazy_Types['newtypeList'])));
    };
    $take = function ($n)  use (&$PS__Data_List_Lazy_Types, &$take, &$PS__Data_Ring, &$PS__Data_Ord, &$PS__Data_Function, &$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
        $go = function ($v)  use (&$PS__Data_List_Lazy_Types, &$take, &$PS__Data_Ring) {
            return function ($v1)  use (&$PS__Data_List_Lazy_Types, &$take, &$PS__Data_Ring, &$v) {
                if ($v1['type'] === 'Nil') {
                    return $PS__Data_List_Lazy_Types['Nil']();
                };
                if ($v1['type'] === 'Cons') {
                    return $PS__Data_List_Lazy_Types['Cons']['constructor']($v1['value0'], $take($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(1))($v1['value1']));
                };
                throw new \Exception('Failed pattern match at Data.List.Lazy line 516, column 3 - line 516, column 32: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
            };
        };
        $__local_var__123 = $PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])($n)(0);
        if ($__local_var__123) {
            return $PS__Data_Function['const']($PS__Data_List_Lazy_Types['nil']);
        };
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_List_Lazy_Types['List'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go($n)))($PS__Data_Newtype['unwrap']($PS__Data_List_Lazy_Types['newtypeList'])));
    };
    $tail = function ($xs)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$uncons) {
        return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($v) {
            return $v['tail'];
        })($uncons($xs));
    };
    $stripPrefix = function ($dictEq)  use (&$PS__Data_List_Lazy_Types, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Control_Monad_Rec_Class, &$PS__Data_Eq) {
        return function ($v)  use (&$PS__Data_List_Lazy_Types, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Control_Monad_Rec_Class, &$PS__Data_Eq, &$dictEq) {
            return function ($s)  use (&$PS__Data_List_Lazy_Types, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Control_Monad_Rec_Class, &$PS__Data_Eq, &$dictEq, &$v) {
                $go = function ($prefix)  use (&$PS__Data_List_Lazy_Types, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Control_Monad_Rec_Class, &$PS__Data_Eq, &$dictEq) {
                    return function ($input)  use (&$PS__Data_List_Lazy_Types, &$prefix, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Control_Monad_Rec_Class, &$PS__Data_Eq, &$dictEq) {
                        $v1 = $PS__Data_List_Lazy_Types['step']($prefix);
                        if ($v1['type'] === 'Nil') {
                            return $PS__Data_Function['apply']($PS__Data_Maybe['Just']['create'])($PS__Control_Monad_Rec_Class['Done']['constructor']($input));
                        };
                        if ($v1['type'] === 'Cons') {
                            $v2 = $PS__Data_List_Lazy_Types['step']($input);
                            if ($v2['type'] === 'Cons' && $PS__Data_Eq['eq']($dictEq)($v1['value0'])($v2['value0'])) {
                                return $PS__Data_Function['apply']($PS__Data_Maybe['Just']['create'])($PS__Control_Monad_Rec_Class['Loop']['constructor']([
                                    'a' => $v1['value1'],
                                    'b' => $v2['value1']
                                ]));
                            };
                            return $PS__Data_Maybe['Nothing']();
                        };
                        throw new \Exception('Failed pattern match at Data.List.Lazy line 498, column 21 - line 502, column 19: ' . var_dump([ $v1['constructor']['name'] ]));
                    };
                };
                return $PS__Control_Monad_Rec_Class['tailRecM2']($PS__Control_Monad_Rec_Class['monadRecMaybe'])($go)($v)($s);
            };
        };
    };
    $span = function ($p)  use (&$uncons, &$span, &$PS__Data_List_Lazy_Types) {
        return function ($xs)  use (&$uncons, &$p, &$span, &$PS__Data_List_Lazy_Types) {
            $v = $uncons($xs);
            if ($v['type'] === 'Just' && $p($v['value0']['head'])) {
                $v1 = $span($p)($v['value0']['tail']);
                return [
                    'init' => $PS__Data_List_Lazy_Types['cons']($v['value0']['head'])($v1['init']),
                    'rest' => $v1['rest']
                ];
            };
            return [
                'init' => $PS__Data_List_Lazy_Types['nil'],
                'rest' => $xs
            ];
        };
    };
    $snoc = function ($xs)  use (&$PS__Data_Foldable, &$PS__Data_List_Lazy_Types) {
        return function ($x)  use (&$PS__Data_Foldable, &$PS__Data_List_Lazy_Types, &$xs) {
            return $PS__Data_Foldable['foldr']($PS__Data_List_Lazy_Types['foldableList'])($PS__Data_List_Lazy_Types['cons'])($PS__Data_List_Lazy_Types['cons']($x)($PS__Data_List_Lazy_Types['nil']))($xs);
        };
    };
    $singleton = function ($a)  use (&$PS__Data_List_Lazy_Types) {
        return $PS__Data_List_Lazy_Types['cons']($a)($PS__Data_List_Lazy_Types['nil']);
    };
    $showPattern = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_List_Lazy_Types) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$PS__Data_List_Lazy_Types, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Pattern ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_List_Lazy_Types['showList']($dictShow))($v))(')'));
        });
    };
    $reverse = function ($xs)  use (&$PS__Control_Lazy, &$PS__Data_List_Lazy_Types, &$PS__Data_Foldable, &$PS__Data_Function) {
        return $PS__Control_Lazy['defer']($PS__Data_List_Lazy_Types['lazyList'])(function ($v)  use (&$PS__Data_Foldable, &$PS__Data_List_Lazy_Types, &$PS__Data_Function, &$xs) {
            return $PS__Data_Foldable['foldl']($PS__Data_List_Lazy_Types['foldableList'])($PS__Data_Function['flip']($PS__Data_List_Lazy_Types['cons']))($PS__Data_List_Lazy_Types['nil'])($xs);
        });
    };
    $replicateM = function ($dictMonad)  use (&$PS__Data_Ord, &$PS__Data_Semiring, &$PS__Control_Applicative, &$PS__Data_List_Lazy_Types, &$PS__Data_Boolean, &$PS__Control_Bind, &$replicateM, &$PS__Data_Ring) {
        return function ($n)  use (&$PS__Data_Ord, &$PS__Data_Semiring, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_List_Lazy_Types, &$PS__Data_Boolean, &$PS__Control_Bind, &$replicateM, &$PS__Data_Ring) {
            return function ($m)  use (&$PS__Data_Ord, &$n, &$PS__Data_Semiring, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_List_Lazy_Types, &$PS__Data_Boolean, &$PS__Control_Bind, &$replicateM, &$PS__Data_Ring) {
                if ($PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($n)($PS__Data_Semiring['one']($PS__Data_Semiring['semiringInt']))) {
                    return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($PS__Data_List_Lazy_Types['nil']);
                };
                if ($PS__Data_Boolean['otherwise']) {
                    return $PS__Control_Bind['bind']($dictMonad['Bind1']())($m)(function ($v)  use (&$PS__Control_Bind, &$dictMonad, &$replicateM, &$PS__Data_Ring, &$n, &$PS__Data_Semiring, &$m, &$PS__Control_Applicative, &$PS__Data_List_Lazy_Types) {
                        return $PS__Control_Bind['bind']($dictMonad['Bind1']())($replicateM($dictMonad)($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($n)($PS__Data_Semiring['one']($PS__Data_Semiring['semiringInt'])))($m))(function ($v1)  use (&$PS__Control_Applicative, &$dictMonad, &$PS__Data_List_Lazy_Types, &$v) {
                            return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($PS__Data_List_Lazy_Types['cons']($v)($v1));
                        });
                    });
                };
                throw new \Exception('Failed pattern match at Data.List.Lazy line 160, column 1 - line 160, column 62: ' . var_dump([ $n['constructor']['name'], $m['constructor']['name'] ]));
            };
        };
    };
    $repeat = function ($x)  use (&$PS__Control_Lazy, &$PS__Data_List_Lazy_Types) {
        return $PS__Control_Lazy['fix']($PS__Data_List_Lazy_Types['lazyList'])(function ($xs)  use (&$PS__Data_List_Lazy_Types, &$x) {
            return $PS__Data_List_Lazy_Types['cons']($x)($xs);
        });
    };
    $replicate = function ($i)  use (&$take, &$repeat) {
        return function ($xs)  use (&$take, &$i, &$repeat) {
            return $take($i)($repeat($xs));
        };
    };
    $range = function ($start)  use (&$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Ring, &$PS__Data_Boolean, &$PS__Data_Unfoldable, &$PS__Data_List_Lazy_Types, &$PS__Data_Semiring) {
        return function ($end)  use (&$PS__Data_Ord, &$start, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Ring, &$PS__Data_Boolean, &$PS__Data_Unfoldable, &$PS__Data_List_Lazy_Types, &$PS__Data_Semiring) {
            if ($PS__Data_Ord['greaterThan']($PS__Data_Ord['ordInt'])($start)($end)) {
                $g = function ($x)  use (&$PS__Data_Ord, &$end, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Ring, &$PS__Data_Boolean) {
                    if ($PS__Data_Ord['greaterThanOrEq']($PS__Data_Ord['ordInt'])($x)($end)) {
                        return $PS__Data_Maybe['Just']['constructor']($PS__Data_Tuple['Tuple']['constructor']($x, $PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($x)(1)));
                    };
                    if ($PS__Data_Boolean['otherwise']) {
                        return $PS__Data_Maybe['Nothing']();
                    };
                    throw new \Exception('Failed pattern match at Data.List.Lazy line 147, column 13 - line 148, column 38: ' . var_dump([ $x['constructor']['name'] ]));
                };
                return $PS__Data_Unfoldable['unfoldr']($PS__Data_List_Lazy_Types['unfoldableList'])($g)($start);
            };
            if ($PS__Data_Boolean['otherwise']) {
                $f = function ($x)  use (&$PS__Data_Ord, &$end, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Semiring, &$PS__Data_Boolean) {
                    if ($PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])($x)($end)) {
                        return $PS__Data_Maybe['Just']['constructor']($PS__Data_Tuple['Tuple']['constructor']($x, $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($x)(1)));
                    };
                    if ($PS__Data_Boolean['otherwise']) {
                        return $PS__Data_Maybe['Nothing']();
                    };
                    throw new \Exception('Failed pattern match at Data.List.Lazy line 152, column 5 - line 153, column 30: ' . var_dump([ $x['constructor']['name'] ]));
                };
                return $PS__Data_Unfoldable['unfoldr']($PS__Data_List_Lazy_Types['unfoldableList'])($f)($start);
            };
            throw new \Exception('Failed pattern match at Data.List.Lazy line 144, column 1 - line 144, column 32: ' . var_dump([ $start['constructor']['name'], $end['constructor']['name'] ]));
        };
    };
    $partition = function ($f)  use (&$PS__Data_List_Lazy_Types, &$PS__Data_Foldable) {
        $go = function ($x)  use (&$f, &$PS__Data_List_Lazy_Types) {
            return function ($v)  use (&$f, &$x, &$PS__Data_List_Lazy_Types) {
                $__local_var__150 = $f($x);
                if ($__local_var__150) {
                    return [
                        'yes' => $PS__Data_List_Lazy_Types['cons']($x)($v['yes']),
                        'no' => $v['no']
                    ];
                };
                return [
                    'yes' => $v['yes'],
                    'no' => $PS__Data_List_Lazy_Types['cons']($x)($v['no'])
                ];
            };
        };
        return $PS__Data_Foldable['foldr']($PS__Data_List_Lazy_Types['foldableList'])($go)([
            'yes' => $PS__Data_List_Lazy_Types['nil'],
            'no' => $PS__Data_List_Lazy_Types['nil']
        ]);
    };
    $__null = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['isNothing'])($uncons);
    $newtypePattern = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Pattern);
    $mapMaybe = function ($f)  use (&$PS__Data_List_Lazy_Types, &$mapMaybe, &$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
        $go = function ($_dollar_copy_v)  use (&$PS__Data_List_Lazy_Types, &$f, &$mapMaybe) {
            $_dollar_tco_done = false;
            $_dollar_tco_result = NULL;
            $_dollar_tco_loop = function ($v)  use (&$_dollar_tco_done, &$PS__Data_List_Lazy_Types, &$f, &$_dollar_copy_v, &$mapMaybe) {
                if ($v['type'] === 'Nil') {
                    $_dollar_tco_done = true;
                    return $PS__Data_List_Lazy_Types['Nil']();
                };
                if ($v['type'] === 'Cons') {
                    $v1 = $f($v['value0']);
                    if ($v1['type'] === 'Nothing') {
                        $_dollar_copy_v = $PS__Data_List_Lazy_Types['step']($v['value1']);
                        return;
                    };
                    if ($v1['type'] === 'Just') {
                        $_dollar_tco_done = true;
                        return $PS__Data_List_Lazy_Types['Cons']['constructor']($v1['value0'], $mapMaybe($f)($v['value1']));
                    };
                    throw new \Exception('Failed pattern match at Data.List.Lazy line 459, column 5 - line 461, column 39: ' . var_dump([ $v1['constructor']['name'] ]));
                };
                throw new \Exception('Failed pattern match at Data.List.Lazy line 457, column 3 - line 457, column 15: ' . var_dump([ $v['constructor']['name'] ]));
            };
            while (!$_dollar_tco_done) {
                $_dollar_tco_result = $_dollar_tco_loop($_dollar_copy_v);
            };
            return $_dollar_tco_result;
        };
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_List_Lazy_Types['List'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go))($PS__Data_Newtype['unwrap']($PS__Data_List_Lazy_Types['newtypeList'])));
    };
    $some = function ($dictAlternative)  use (&$PS__Control_Apply, &$PS__Data_Functor, &$PS__Data_List_Lazy_Types, &$PS__Control_Lazy, &$many) {
        return function ($dictLazy)  use (&$PS__Control_Apply, &$dictAlternative, &$PS__Data_Functor, &$PS__Data_List_Lazy_Types, &$PS__Control_Lazy, &$many) {
            return function ($v)  use (&$PS__Control_Apply, &$dictAlternative, &$PS__Data_Functor, &$PS__Data_List_Lazy_Types, &$PS__Control_Lazy, &$dictLazy, &$many) {
                return $PS__Control_Apply['apply'](($dictAlternative['Applicative0']())['Apply0']())($PS__Data_Functor['map']((($dictAlternative['Plus1']())['Alt0']())['Functor0']())($PS__Data_List_Lazy_Types['cons'])($v))($PS__Control_Lazy['defer']($dictLazy)(function ($v1)  use (&$many, &$dictAlternative, &$dictLazy, &$v) {
                    return $many($dictAlternative)($dictLazy)($v);
                }));
            };
        };
    };
    $many = function ($dictAlternative)  use (&$PS__Control_Alt, &$some, &$PS__Control_Applicative, &$PS__Data_List_Lazy_Types) {
        return function ($dictLazy)  use (&$PS__Control_Alt, &$dictAlternative, &$some, &$PS__Control_Applicative, &$PS__Data_List_Lazy_Types) {
            return function ($v)  use (&$PS__Control_Alt, &$dictAlternative, &$some, &$dictLazy, &$PS__Control_Applicative, &$PS__Data_List_Lazy_Types) {
                return $PS__Control_Alt['alt'](($dictAlternative['Plus1']())['Alt0']())($some($dictAlternative)($dictLazy)($v))($PS__Control_Applicative['pure']($dictAlternative['Applicative0']())($PS__Data_List_Lazy_Types['nil']));
            };
        };
    };
    $length = $PS__Data_Foldable['foldl']($PS__Data_List_Lazy_Types['foldableList'])(function ($l)  use (&$PS__Data_Semiring) {
        return function ($v)  use (&$PS__Data_Semiring, &$l) {
            return $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($l)(1);
        };
    })(0);
    $last = (function ()  use (&$__null, &$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Data_List_Lazy_Types, &$PS__Control_Semigroupoid) {
        $go = function ($_dollar_copy_v)  use (&$__null, &$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Data_List_Lazy_Types) {
            $_dollar_tco_done = false;
            $_dollar_tco_result = NULL;
            $_dollar_tco_loop = function ($v)  use (&$__null, &$_dollar_tco_done, &$PS__Data_Maybe, &$PS__Data_Boolean, &$_dollar_copy_v, &$PS__Data_List_Lazy_Types) {
                if ($v['type'] === 'Cons') {
                    if ($__null($v['value1'])) {
                        $_dollar_tco_done = true;
                        return $PS__Data_Maybe['Just']['constructor']($v['value0']);
                    };
                    if ($PS__Data_Boolean['otherwise']) {
                        $_dollar_copy_v = $PS__Data_List_Lazy_Types['step']($v['value1']);
                        return;
                    };
                };
                $_dollar_tco_done = true;
                return $PS__Data_Maybe['Nothing']();
            };
            while (!$_dollar_tco_done) {
                $_dollar_tco_result = $_dollar_tco_loop($_dollar_copy_v);
            };
            return $_dollar_tco_result;
        };
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($go)($PS__Data_List_Lazy_Types['step']);
    })();
    $iterate = function ($f)  use (&$PS__Control_Lazy, &$PS__Data_List_Lazy_Types, &$PS__Data_Functor) {
        return function ($x)  use (&$PS__Control_Lazy, &$PS__Data_List_Lazy_Types, &$PS__Data_Functor, &$f) {
            return $PS__Control_Lazy['fix']($PS__Data_List_Lazy_Types['lazyList'])(function ($xs)  use (&$PS__Data_List_Lazy_Types, &$x, &$PS__Data_Functor, &$f) {
                return $PS__Data_List_Lazy_Types['cons']($x)($PS__Data_Functor['map']($PS__Data_List_Lazy_Types['functorList'])($f)($xs));
            });
        };
    };
    $insertAt = function ($v)  use (&$PS__Data_List_Lazy_Types, &$insertAt, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
        return function ($x)  use (&$v, &$PS__Data_List_Lazy_Types, &$insertAt, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
            return function ($xs)  use (&$v, &$PS__Data_List_Lazy_Types, &$x, &$insertAt, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
                if ($v === 0) {
                    return $PS__Data_List_Lazy_Types['cons']($x)($xs);
                };
                $go = function ($v1)  use (&$PS__Data_List_Lazy_Types, &$x, &$insertAt, &$PS__Data_Ring, &$v) {
                    if ($v1['type'] === 'Nil') {
                        return $PS__Data_List_Lazy_Types['Cons']['constructor']($x, $PS__Data_List_Lazy_Types['nil']);
                    };
                    if ($v1['type'] === 'Cons') {
                        return $PS__Data_List_Lazy_Types['Cons']['constructor']($v1['value0'], $insertAt($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(1))($x)($v1['value1']));
                    };
                    throw new \Exception('Failed pattern match at Data.List.Lazy line 339, column 3 - line 339, column 22: ' . var_dump([ $v1['constructor']['name'] ]));
                };
                return $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go)($PS__Data_Newtype['unwrap']($PS__Data_List_Lazy_Types['newtypeList'])($xs));
            };
        };
    };
    $init = (function ()  use (&$__null, &$PS__Data_Maybe, &$PS__Data_List_Lazy_Types, &$PS__Data_Boolean, &$PS__Data_Functor, &$PS__Control_Semigroupoid) {
        $go = function ($v)  use (&$__null, &$PS__Data_Maybe, &$PS__Data_List_Lazy_Types, &$PS__Data_Boolean, &$PS__Data_Functor, &$go) {
            if ($v['type'] === 'Cons') {
                if ($__null($v['value1'])) {
                    return $PS__Data_Maybe['Just']['constructor']($PS__Data_List_Lazy_Types['nil']);
                };
                if ($PS__Data_Boolean['otherwise']) {
                    return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])($PS__Data_List_Lazy_Types['cons']($v['value0']))($go($PS__Data_List_Lazy_Types['step']($v['value1'])));
                };
            };
            return $PS__Data_Maybe['Nothing']();
        };
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($go)($PS__Data_List_Lazy_Types['step']);
    })();
    $index = function ($xs)  use (&$PS__Data_Maybe, &$PS__Data_List_Lazy_Types, &$PS__Data_Ring) {
        $go = function ($_dollar_copy_v)  use (&$PS__Data_Maybe, &$PS__Data_List_Lazy_Types, &$PS__Data_Ring) {
            return function ($_dollar_copy_v1)  use (&$_dollar_copy_v, &$PS__Data_Maybe, &$PS__Data_List_Lazy_Types, &$PS__Data_Ring) {
                $_dollar_tco_var_v = $_dollar_copy_v;
                $_dollar_tco_done = false;
                $_dollar_tco_result = NULL;
                $_dollar_tco_loop = function ($v, $v1)  use (&$_dollar_tco_done, &$PS__Data_Maybe, &$_dollar_tco_var_v, &$PS__Data_List_Lazy_Types, &$_dollar_copy_v1, &$PS__Data_Ring) {
                    if ($v['type'] === 'Nil') {
                        $_dollar_tco_done = true;
                        return $PS__Data_Maybe['Nothing']();
                    };
                    if ($v['type'] === 'Cons' && $v1 === 0) {
                        $_dollar_tco_done = true;
                        return $PS__Data_Maybe['Just']['constructor']($v['value0']);
                    };
                    if ($v['type'] === 'Cons') {
                        $_dollar_tco_var_v = $PS__Data_List_Lazy_Types['step']($v['value1']);
                        $_dollar_copy_v1 = $PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v1)(1);
                        return;
                    };
                    throw new \Exception('Failed pattern match at Data.List.Lazy line 298, column 3 - line 298, column 21: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
                };
                while (!$_dollar_tco_done) {
                    $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_v, $_dollar_copy_v1);
                };
                return $_dollar_tco_result;
            };
        };
        return $go($PS__Data_List_Lazy_Types['step']($xs));
    };
    $head = function ($xs)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$uncons) {
        return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($v) {
            return $v['head'];
        })($uncons($xs));
    };
    $transpose = function ($xs)  use (&$uncons, &$transpose, &$PS__Data_List_Lazy_Types, &$mapMaybe, &$head, &$tail) {
        $v = $uncons($xs);
        if ($v['type'] === 'Nothing') {
            return $xs;
        };
        if ($v['type'] === 'Just') {
            $v1 = $uncons($v['value0']['head']);
            if ($v1['type'] === 'Nothing') {
                return $transpose($v['value0']['tail']);
            };
            if ($v1['type'] === 'Just') {
                return $PS__Data_List_Lazy_Types['cons']($PS__Data_List_Lazy_Types['cons']($v1['value0']['head'])($mapMaybe($head)($v['value0']['tail'])))($transpose($PS__Data_List_Lazy_Types['cons']($v1['value0']['tail'])($mapMaybe($tail)($v['value0']['tail']))));
            };
            throw new \Exception('Failed pattern match at Data.List.Lazy line 733, column 7 - line 737, column 72: ' . var_dump([ $v1['constructor']['name'] ]));
        };
        throw new \Exception('Failed pattern match at Data.List.Lazy line 729, column 3 - line 737, column 72: ' . var_dump([ $v['constructor']['name'] ]));
    };
    $groupBy = function ($eq)  use (&$PS__Data_List_Lazy_Types, &$span, &$PS__Data_Lazy, &$PS__Data_NonEmpty, &$groupBy, &$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_Newtype) {
        $go = function ($v)  use (&$PS__Data_List_Lazy_Types, &$span, &$eq, &$PS__Data_Lazy, &$PS__Data_NonEmpty, &$groupBy) {
            if ($v['type'] === 'Nil') {
                return $PS__Data_List_Lazy_Types['Nil']();
            };
            if ($v['type'] === 'Cons') {
                $v1 = $span($eq($v['value0']))($v['value1']);
                return $PS__Data_List_Lazy_Types['Cons']['constructor']($PS__Data_Lazy['defer'](function ($v2)  use (&$PS__Data_NonEmpty, &$v, &$v1) {
                    return $PS__Data_NonEmpty['NonEmpty']['constructor']($v['value0'], $v1['init']);
                }), $groupBy($eq)($v1['rest']));
            };
            throw new \Exception('Failed pattern match at Data.List.Lazy line 587, column 3 - line 587, column 15: ' . var_dump([ $v['constructor']['name'] ]));
        };
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_List_Lazy_Types['List'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go))($PS__Data_Newtype['unwrap']($PS__Data_List_Lazy_Types['newtypeList'])));
    };
    $group = function ($dictEq)  use (&$groupBy, &$PS__Data_Eq) {
        return $groupBy($PS__Data_Eq['eq']($dictEq));
    };
    $fromStep = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_List_Lazy_Types['List'])($PS__Control_Applicative['pure']($PS__Data_Lazy['applicativeLazy']));
    $insertBy = function ($cmp)  use (&$PS__Data_List_Lazy_Types, &$insertBy, &$fromStep, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
        return function ($x)  use (&$PS__Data_List_Lazy_Types, &$cmp, &$insertBy, &$fromStep, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
            return function ($xs)  use (&$PS__Data_List_Lazy_Types, &$x, &$cmp, &$insertBy, &$fromStep, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
                $go = function ($v)  use (&$PS__Data_List_Lazy_Types, &$x, &$cmp, &$insertBy, &$fromStep) {
                    if ($v['type'] === 'Nil') {
                        return $PS__Data_List_Lazy_Types['Cons']['constructor']($x, $PS__Data_List_Lazy_Types['nil']);
                    };
                    if ($v['type'] === 'Cons') {
                        $v1 = $cmp($x)($v['value0']);
                        if ($v1['type'] === 'GT') {
                            return $PS__Data_List_Lazy_Types['Cons']['constructor']($v['value0'], $insertBy($cmp)($x)($v['value1']));
                        };
                        return $PS__Data_List_Lazy_Types['Cons']['constructor']($x, $fromStep($v));
                    };
                    throw new \Exception('Failed pattern match at Data.List.Lazy line 234, column 3 - line 234, column 22: ' . var_dump([ $v['constructor']['name'] ]));
                };
                return $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go)($PS__Data_Newtype['unwrap']($PS__Data_List_Lazy_Types['newtypeList'])($xs));
            };
        };
    };
    $insert = function ($dictOrd)  use (&$insertBy, &$PS__Data_Ord) {
        return $insertBy($PS__Data_Ord['compare']($dictOrd));
    };
    $fromFoldable = function ($dictFoldable)  use (&$PS__Data_Foldable, &$PS__Data_List_Lazy_Types) {
        return $PS__Data_Foldable['foldr']($dictFoldable)($PS__Data_List_Lazy_Types['cons'])($PS__Data_List_Lazy_Types['nil']);
    };
    $foldrLazy = function ($dictLazy)  use (&$PS__Data_List_Lazy_Types, &$PS__Control_Lazy) {
        return function ($op)  use (&$PS__Data_List_Lazy_Types, &$PS__Control_Lazy, &$dictLazy) {
            return function ($z)  use (&$PS__Data_List_Lazy_Types, &$PS__Control_Lazy, &$dictLazy, &$op) {
                $go = function ($xs)  use (&$PS__Data_List_Lazy_Types, &$PS__Control_Lazy, &$dictLazy, &$op, &$go, &$z) {
                    $v = $PS__Data_List_Lazy_Types['step']($xs);
                    if ($v['type'] === 'Cons') {
                        return $PS__Control_Lazy['defer']($dictLazy)(function ($v1)  use (&$op, &$v, &$go) {
                            return $op($v['value0'])($go($v['value1']));
                        });
                    };
                    if ($v['type'] === 'Nil') {
                        return $z;
                    };
                    throw new \Exception('Failed pattern match at Data.List.Lazy line 755, column 13 - line 757, column 14: ' . var_dump([ $v['constructor']['name'] ]));
                };
                return $go;
            };
        };
    };
    $foldM = function ($dictMonad)  use (&$uncons, &$PS__Control_Applicative, &$PS__Control_Bind, &$foldM) {
        return function ($f)  use (&$uncons, &$PS__Control_Applicative, &$dictMonad, &$PS__Control_Bind, &$foldM) {
            return function ($a)  use (&$uncons, &$PS__Control_Applicative, &$dictMonad, &$PS__Control_Bind, &$f, &$foldM) {
                return function ($xs)  use (&$uncons, &$PS__Control_Applicative, &$dictMonad, &$a, &$PS__Control_Bind, &$f, &$foldM) {
                    $v = $uncons($xs);
                    if ($v['type'] === 'Nothing') {
                        return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($a);
                    };
                    if ($v['type'] === 'Just') {
                        return $PS__Control_Bind['bind']($dictMonad['Bind1']())($f($a)($v['value0']['head']))(function ($a__prime)  use (&$foldM, &$dictMonad, &$f, &$v) {
                            return $foldM($dictMonad)($f)($a__prime)($v['value0']['tail']);
                        });
                    };
                    throw new \Exception('Failed pattern match at Data.List.Lazy line 746, column 5 - line 749, column 54: ' . var_dump([ $v['constructor']['name'] ]));
                };
            };
        };
    };
    $findIndex = function ($fn)  use (&$PS__Control_Bind, &$PS__Data_Maybe, &$uncons, &$PS__Control_Applicative, &$PS__Data_Semiring) {
        $go = function ($n)  use (&$PS__Control_Bind, &$PS__Data_Maybe, &$uncons, &$fn, &$PS__Control_Applicative, &$go, &$PS__Data_Semiring) {
            return function ($list)  use (&$PS__Control_Bind, &$PS__Data_Maybe, &$uncons, &$fn, &$PS__Control_Applicative, &$n, &$go, &$PS__Data_Semiring) {
                return $PS__Control_Bind['bind']($PS__Data_Maybe['bindMaybe'])($uncons($list))(function ($v)  use (&$fn, &$PS__Control_Applicative, &$PS__Data_Maybe, &$n, &$go, &$PS__Data_Semiring) {
                    $__local_var__203 = $fn($v['head']);
                    if ($__local_var__203) {
                        return $PS__Control_Applicative['pure']($PS__Data_Maybe['applicativeMaybe'])($n);
                    };
                    return $go($PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($n)(1))($v['tail']);
                });
            };
        };
        return $go(0);
    };
    $findLastIndex = function ($fn)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Ring, &$length, &$findIndex, &$reverse) {
        return function ($xs)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Ring, &$length, &$findIndex, &$fn, &$reverse) {
            return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($v)  use (&$PS__Data_Ring, &$length, &$xs) {
                return $PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($length($xs))(1))($v);
            })($findIndex($fn)($reverse($xs)));
        };
    };
    $filterM = function ($dictMonad)  use (&$uncons, &$PS__Control_Applicative, &$PS__Data_List_Lazy_Types, &$PS__Control_Bind, &$filterM) {
        return function ($p)  use (&$uncons, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_List_Lazy_Types, &$PS__Control_Bind, &$filterM) {
            return function ($list)  use (&$uncons, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_List_Lazy_Types, &$PS__Control_Bind, &$p, &$filterM) {
                $v = $uncons($list);
                if ($v['type'] === 'Nothing') {
                    return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($PS__Data_List_Lazy_Types['nil']);
                };
                if ($v['type'] === 'Just') {
                    return $PS__Control_Bind['bind']($dictMonad['Bind1']())($p($v['value0']['head']))(function ($v1)  use (&$PS__Control_Bind, &$dictMonad, &$filterM, &$p, &$v, &$PS__Control_Applicative, &$PS__Data_List_Lazy_Types) {
                        return $PS__Control_Bind['bind']($dictMonad['Bind1']())($filterM($dictMonad)($p)($v['value0']['tail']))(function ($v2)  use (&$PS__Control_Applicative, &$dictMonad, &$v1, &$PS__Data_List_Lazy_Types, &$v) {
                            return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())((function ()  use (&$v1, &$PS__Data_List_Lazy_Types, &$v, &$v2) {
                                if ($v1) {
                                    return $PS__Data_List_Lazy_Types['cons']($v['value0']['head'])($v2);
                                };
                                return $v2;
                            })());
                        });
                    });
                };
                throw new \Exception('Failed pattern match at Data.List.Lazy line 442, column 5 - line 447, column 48: ' . var_dump([ $v['constructor']['name'] ]));
            };
        };
    };
    $filter = function ($p)  use (&$PS__Data_List_Lazy_Types, &$filter, &$PS__Data_Boolean, &$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
        $go = function ($_dollar_copy_v)  use (&$PS__Data_List_Lazy_Types, &$p, &$filter, &$PS__Data_Boolean) {
            $_dollar_tco_done = false;
            $_dollar_tco_result = NULL;
            $_dollar_tco_loop = function ($v)  use (&$_dollar_tco_done, &$PS__Data_List_Lazy_Types, &$p, &$filter, &$PS__Data_Boolean, &$_dollar_copy_v) {
                if ($v['type'] === 'Nil') {
                    $_dollar_tco_done = true;
                    return $PS__Data_List_Lazy_Types['Nil']();
                };
                if ($v['type'] === 'Cons') {
                    if ($p($v['value0'])) {
                        $_dollar_tco_done = true;
                        return $PS__Data_List_Lazy_Types['Cons']['constructor']($v['value0'], $filter($p)($v['value1']));
                    };
                    if ($PS__Data_Boolean['otherwise']) {
                        $_dollar_copy_v = $PS__Data_List_Lazy_Types['step']($v['value1']);
                        return;
                    };
                };
                throw new \Exception('Failed pattern match at Data.List.Lazy line 427, column 3 - line 427, column 15: ' . var_dump([ $v['constructor']['name'] ]));
            };
            while (!$_dollar_tco_done) {
                $_dollar_tco_result = $_dollar_tco_loop($_dollar_copy_v);
            };
            return $_dollar_tco_result;
        };
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_List_Lazy_Types['List'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go))($PS__Data_Newtype['unwrap']($PS__Data_List_Lazy_Types['newtypeList'])));
    };
    $intersectBy = function ($eq)  use (&$filter, &$PS__Data_Foldable, &$PS__Data_List_Lazy_Types, &$PS__Data_HeytingAlgebra) {
        return function ($xs)  use (&$filter, &$PS__Data_Foldable, &$PS__Data_List_Lazy_Types, &$PS__Data_HeytingAlgebra, &$eq) {
            return function ($ys)  use (&$filter, &$PS__Data_Foldable, &$PS__Data_List_Lazy_Types, &$PS__Data_HeytingAlgebra, &$eq, &$xs) {
                return $filter(function ($x)  use (&$PS__Data_Foldable, &$PS__Data_List_Lazy_Types, &$PS__Data_HeytingAlgebra, &$eq, &$ys) {
                    return $PS__Data_Foldable['any']($PS__Data_List_Lazy_Types['foldableList'])($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($eq($x))($ys);
                })($xs);
            };
        };
    };
    $intersect = function ($dictEq)  use (&$intersectBy, &$PS__Data_Eq) {
        return $intersectBy($PS__Data_Eq['eq']($dictEq));
    };
    $nubBy = function ($eq)  use (&$PS__Data_List_Lazy_Types, &$nubBy, &$filter, &$PS__Data_HeytingAlgebra, &$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
        $go = function ($v)  use (&$PS__Data_List_Lazy_Types, &$nubBy, &$eq, &$filter, &$PS__Data_HeytingAlgebra) {
            if ($v['type'] === 'Nil') {
                return $PS__Data_List_Lazy_Types['Nil']();
            };
            if ($v['type'] === 'Cons') {
                return $PS__Data_List_Lazy_Types['Cons']['constructor']($v['value0'], $nubBy($eq)($filter(function ($y)  use (&$PS__Data_HeytingAlgebra, &$eq, &$v) {
                    return $PS__Data_HeytingAlgebra['not']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($eq($v['value0'])($y));
                })($v['value1'])));
            };
            throw new \Exception('Failed pattern match at Data.List.Lazy line 620, column 3 - line 620, column 15: ' . var_dump([ $v['constructor']['name'] ]));
        };
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_List_Lazy_Types['List'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go))($PS__Data_Newtype['unwrap']($PS__Data_List_Lazy_Types['newtypeList'])));
    };
    $nub = function ($dictEq)  use (&$nubBy, &$PS__Data_Eq) {
        return $nubBy($PS__Data_Eq['eq']($dictEq));
    };
    $eqPattern = function ($dictEq)  use (&$PS__Data_Eq, &$PS__Data_List_Lazy_Types) {
        return $PS__Data_Eq['Eq'](function ($x)  use (&$PS__Data_Eq, &$PS__Data_List_Lazy_Types, &$dictEq) {
            return function ($y)  use (&$PS__Data_Eq, &$PS__Data_List_Lazy_Types, &$dictEq, &$x) {
                return $PS__Data_Eq['eq']($PS__Data_List_Lazy_Types['eqList']($dictEq))($x)($y);
            };
        });
    };
    $ordPattern = function ($dictOrd)  use (&$PS__Data_Ord, &$eqPattern, &$PS__Data_List_Lazy_Types) {
        return $PS__Data_Ord['Ord'](function ()  use (&$eqPattern, &$dictOrd) {
            return $eqPattern($dictOrd['Eq0']());
        }, function ($x)  use (&$PS__Data_Ord, &$PS__Data_List_Lazy_Types, &$dictOrd) {
            return function ($y)  use (&$PS__Data_Ord, &$PS__Data_List_Lazy_Types, &$dictOrd, &$x) {
                return $PS__Data_Ord['compare']($PS__Data_List_Lazy_Types['ordList']($dictOrd))($x)($y);
            };
        });
    };
    $elemLastIndex = function ($dictEq)  use (&$findLastIndex, &$PS__Data_Eq) {
        return function ($x)  use (&$findLastIndex, &$PS__Data_Eq, &$dictEq) {
            return $findLastIndex(function ($v)  use (&$PS__Data_Eq, &$dictEq, &$x) {
                return $PS__Data_Eq['eq']($dictEq)($v)($x);
            });
        };
    };
    $elemIndex = function ($dictEq)  use (&$findIndex, &$PS__Data_Eq) {
        return function ($x)  use (&$findIndex, &$PS__Data_Eq, &$dictEq) {
            return $findIndex(function ($v)  use (&$PS__Data_Eq, &$dictEq, &$x) {
                return $PS__Data_Eq['eq']($dictEq)($v)($x);
            });
        };
    };
    $dropWhile = function ($p)  use (&$PS__Data_List_Lazy_Types, &$fromStep, &$PS__Control_Semigroupoid) {
        $go = function ($_dollar_copy_v)  use (&$p, &$PS__Data_List_Lazy_Types, &$fromStep) {
            $_dollar_tco_done = false;
            $_dollar_tco_result = NULL;
            $_dollar_tco_loop = function ($v)  use (&$p, &$_dollar_copy_v, &$PS__Data_List_Lazy_Types, &$_dollar_tco_done, &$fromStep) {
                if ($v['type'] === 'Cons' && $p($v['value0'])) {
                    $_dollar_copy_v = $PS__Data_List_Lazy_Types['step']($v['value1']);
                    return;
                };
                $_dollar_tco_done = true;
                return $fromStep($v);
            };
            while (!$_dollar_tco_done) {
                $_dollar_tco_result = $_dollar_tco_loop($_dollar_copy_v);
            };
            return $_dollar_tco_result;
        };
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($go)($PS__Data_List_Lazy_Types['step']);
    };
    $drop = function ($n)  use (&$PS__Data_List_Lazy_Types, &$PS__Data_Ring, &$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
        $go = function ($_dollar_copy_v)  use (&$PS__Data_List_Lazy_Types, &$PS__Data_Ring) {
            return function ($_dollar_copy_v1)  use (&$_dollar_copy_v, &$PS__Data_List_Lazy_Types, &$PS__Data_Ring) {
                $_dollar_tco_var_v = $_dollar_copy_v;
                $_dollar_tco_done = false;
                $_dollar_tco_result = NULL;
                $_dollar_tco_loop = function ($v, $v1)  use (&$_dollar_tco_done, &$PS__Data_List_Lazy_Types, &$_dollar_tco_var_v, &$PS__Data_Ring, &$_dollar_copy_v1) {
                    if ($v === 0) {
                        $_dollar_tco_done = true;
                        return $v1;
                    };
                    if ($v1['type'] === 'Nil') {
                        $_dollar_tco_done = true;
                        return $PS__Data_List_Lazy_Types['Nil']();
                    };
                    if ($v1['type'] === 'Cons') {
                        $_dollar_tco_var_v = $PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(1);
                        $_dollar_copy_v1 = $PS__Data_List_Lazy_Types['step']($v1['value1']);
                        return;
                    };
                    throw new \Exception('Failed pattern match at Data.List.Lazy line 535, column 3 - line 535, column 15: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
                };
                while (!$_dollar_tco_done) {
                    $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_v, $_dollar_copy_v1);
                };
                return $_dollar_tco_result;
            };
        };
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_List_Lazy_Types['List'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go($n)))($PS__Data_Newtype['unwrap']($PS__Data_List_Lazy_Types['newtypeList'])));
    };
    $slice = function ($start)  use (&$take, &$PS__Data_Ring, &$drop) {
        return function ($end)  use (&$take, &$PS__Data_Ring, &$start, &$drop) {
            return function ($xs)  use (&$take, &$PS__Data_Ring, &$end, &$start, &$drop) {
                return $take($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($end)($start))($drop($start)($xs));
            };
        };
    };
    $deleteBy = function ($eq)  use (&$PS__Data_List_Lazy_Types, &$PS__Data_Boolean, &$deleteBy, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
        return function ($x)  use (&$PS__Data_List_Lazy_Types, &$eq, &$PS__Data_Boolean, &$deleteBy, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
            return function ($xs)  use (&$PS__Data_List_Lazy_Types, &$eq, &$x, &$PS__Data_Boolean, &$deleteBy, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
                $go = function ($v)  use (&$PS__Data_List_Lazy_Types, &$eq, &$x, &$PS__Data_Boolean, &$deleteBy) {
                    if ($v['type'] === 'Nil') {
                        return $PS__Data_List_Lazy_Types['Nil']();
                    };
                    if ($v['type'] === 'Cons') {
                        if ($eq($x)($v['value0'])) {
                            return $PS__Data_List_Lazy_Types['step']($v['value1']);
                        };
                        if ($PS__Data_Boolean['otherwise']) {
                            return $PS__Data_List_Lazy_Types['Cons']['constructor']($v['value0'], $deleteBy($eq)($x)($v['value1']));
                        };
                    };
                    throw new \Exception('Failed pattern match at Data.List.Lazy line 649, column 3 - line 649, column 15: ' . var_dump([ $v['constructor']['name'] ]));
                };
                return $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go)($PS__Data_Newtype['unwrap']($PS__Data_List_Lazy_Types['newtypeList'])($xs));
            };
        };
    };
    $unionBy = function ($eq)  use (&$PS__Data_Semigroup, &$PS__Data_List_Lazy_Types, &$PS__Data_Foldable, &$PS__Data_Function, &$deleteBy, &$nubBy) {
        return function ($xs)  use (&$PS__Data_Semigroup, &$PS__Data_List_Lazy_Types, &$PS__Data_Foldable, &$PS__Data_Function, &$deleteBy, &$eq, &$nubBy) {
            return function ($ys)  use (&$PS__Data_Semigroup, &$PS__Data_List_Lazy_Types, &$xs, &$PS__Data_Foldable, &$PS__Data_Function, &$deleteBy, &$eq, &$nubBy) {
                return $PS__Data_Semigroup['append']($PS__Data_List_Lazy_Types['semigroupList'])($xs)($PS__Data_Foldable['foldl']($PS__Data_List_Lazy_Types['foldableList'])($PS__Data_Function['flip']($deleteBy($eq)))($nubBy($eq)($ys))($xs));
            };
        };
    };
    $union = function ($dictEq)  use (&$unionBy, &$PS__Data_Eq) {
        return $unionBy($PS__Data_Eq['eq']($dictEq));
    };
    $deleteAt = function ($n)  use (&$PS__Data_List_Lazy_Types, &$deleteAt, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
        return function ($xs)  use (&$PS__Data_List_Lazy_Types, &$deleteAt, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Lazy, &$n, &$PS__Data_Newtype) {
            $go = function ($v)  use (&$PS__Data_List_Lazy_Types, &$deleteAt, &$PS__Data_Ring) {
                return function ($v1)  use (&$PS__Data_List_Lazy_Types, &$v, &$deleteAt, &$PS__Data_Ring) {
                    if ($v1['type'] === 'Nil') {
                        return $PS__Data_List_Lazy_Types['Nil']();
                    };
                    if ($v === 0 && $v1['type'] === 'Cons') {
                        return $PS__Data_List_Lazy_Types['step']($v1['value1']);
                    };
                    if ($v1['type'] === 'Cons') {
                        return $PS__Data_List_Lazy_Types['Cons']['constructor']($v1['value0'], $deleteAt($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(1))($v1['value1']));
                    };
                    throw new \Exception('Failed pattern match at Data.List.Lazy line 352, column 3 - line 352, column 17: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
                };
            };
            return $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go($n))($PS__Data_Newtype['unwrap']($PS__Data_List_Lazy_Types['newtypeList'])($xs));
        };
    };
    $__delete = function ($dictEq)  use (&$deleteBy, &$PS__Data_Eq) {
        return $deleteBy($PS__Data_Eq['eq']($dictEq));
    };
    $difference = function ($dictEq)  use (&$PS__Data_Foldable, &$PS__Data_List_Lazy_Types, &$PS__Data_Function, &$__delete) {
        return $PS__Data_Foldable['foldl']($PS__Data_List_Lazy_Types['foldableList'])($PS__Data_Function['flip']($__delete($dictEq)));
    };
    $cycle = function ($xs)  use (&$PS__Control_Lazy, &$PS__Data_List_Lazy_Types, &$PS__Data_Semigroup) {
        return $PS__Control_Lazy['fix']($PS__Data_List_Lazy_Types['lazyList'])(function ($ys)  use (&$PS__Data_Semigroup, &$PS__Data_List_Lazy_Types, &$xs) {
            return $PS__Data_Semigroup['append']($PS__Data_List_Lazy_Types['semigroupList'])($xs)($ys);
        });
    };
    $concatMap = $PS__Data_Function['flip']($PS__Control_Bind['bind']($PS__Data_List_Lazy_Types['bindList']));
    $concat = function ($v)  use (&$PS__Control_Bind, &$PS__Data_List_Lazy_Types, &$PS__Control_Category) {
        return $PS__Control_Bind['bind']($PS__Data_List_Lazy_Types['bindList'])($v)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    };
    $catMaybes = $mapMaybe($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    $alterAt = function ($n)  use (&$PS__Data_List_Lazy_Types, &$alterAt, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype) {
        return function ($f)  use (&$PS__Data_List_Lazy_Types, &$alterAt, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Lazy, &$n, &$PS__Data_Newtype) {
            return function ($xs)  use (&$PS__Data_List_Lazy_Types, &$f, &$alterAt, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Lazy, &$n, &$PS__Data_Newtype) {
                $go = function ($v)  use (&$PS__Data_List_Lazy_Types, &$f, &$alterAt, &$PS__Data_Ring) {
                    return function ($v1)  use (&$PS__Data_List_Lazy_Types, &$v, &$f, &$alterAt, &$PS__Data_Ring) {
                        if ($v1['type'] === 'Nil') {
                            return $PS__Data_List_Lazy_Types['Nil']();
                        };
                        if ($v === 0 && $v1['type'] === 'Cons') {
                            $v2 = $f($v1['value0']);
                            if ($v2['type'] === 'Nothing') {
                                return $PS__Data_List_Lazy_Types['step']($v1['value1']);
                            };
                            if ($v2['type'] === 'Just') {
                                return $PS__Data_List_Lazy_Types['Cons']['constructor']($v2['value0'], $v1['value1']);
                            };
                            throw new \Exception('Failed pattern match at Data.List.Lazy line 393, column 22 - line 395, column 26: ' . var_dump([ $v2['constructor']['name'] ]));
                        };
                        if ($v1['type'] === 'Cons') {
                            return $PS__Data_List_Lazy_Types['Cons']['constructor']($v1['value0'], $alterAt($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(1))($f)($v1['value1']));
                        };
                        throw new \Exception('Failed pattern match at Data.List.Lazy line 392, column 3 - line 392, column 17: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
                    };
                };
                return $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go($n))($PS__Data_Newtype['unwrap']($PS__Data_List_Lazy_Types['newtypeList'])($xs));
            };
        };
    };
    $modifyAt = function ($n)  use (&$alterAt, &$PS__Control_Semigroupoid, &$PS__Data_Maybe) {
        return function ($f)  use (&$alterAt, &$n, &$PS__Control_Semigroupoid, &$PS__Data_Maybe) {
            return $alterAt($n)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['Just']['create'])($f));
        };
    };
    return [
        'toUnfoldable' => $toUnfoldable,
        'fromFoldable' => $fromFoldable,
        'singleton' => $singleton,
        'range' => $range,
        'replicate' => $replicate,
        'replicateM' => $replicateM,
        'some' => $some,
        'many' => $many,
        'repeat' => $repeat,
        'iterate' => $iterate,
        'cycle' => $cycle,
        'null' => $__null,
        'length' => $length,
        'snoc' => $snoc,
        'insert' => $insert,
        'insertBy' => $insertBy,
        'head' => $head,
        'last' => $last,
        'tail' => $tail,
        'init' => $init,
        'uncons' => $uncons,
        'index' => $index,
        'elemIndex' => $elemIndex,
        'elemLastIndex' => $elemLastIndex,
        'findIndex' => $findIndex,
        'findLastIndex' => $findLastIndex,
        'insertAt' => $insertAt,
        'deleteAt' => $deleteAt,
        'updateAt' => $updateAt,
        'modifyAt' => $modifyAt,
        'alterAt' => $alterAt,
        'reverse' => $reverse,
        'concat' => $concat,
        'concatMap' => $concatMap,
        'filter' => $filter,
        'filterM' => $filterM,
        'mapMaybe' => $mapMaybe,
        'catMaybes' => $catMaybes,
        'Pattern' => $Pattern,
        'stripPrefix' => $stripPrefix,
        'slice' => $slice,
        'take' => $take,
        'takeWhile' => $takeWhile,
        'drop' => $drop,
        'dropWhile' => $dropWhile,
        'span' => $span,
        'group' => $group,
        'groupBy' => $groupBy,
        'partition' => $partition,
        'nub' => $nub,
        'nubBy' => $nubBy,
        'union' => $union,
        'unionBy' => $unionBy,
        'delete' => $__delete,
        'deleteBy' => $deleteBy,
        'difference' => $difference,
        'intersect' => $intersect,
        'intersectBy' => $intersectBy,
        'zipWith' => $zipWith,
        'zipWithA' => $zipWithA,
        'zip' => $zip,
        'unzip' => $unzip,
        'transpose' => $transpose,
        'foldM' => $foldM,
        'foldrLazy' => $foldrLazy,
        'eqPattern' => $eqPattern,
        'ordPattern' => $ordPattern,
        'newtypePattern' => $newtypePattern,
        'showPattern' => $showPattern
    ];
})();
