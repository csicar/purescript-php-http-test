<?php
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Data.Void/index.php');
$PS__Data_Ord = (function ()  use (&$PS__Data_Eq, &$PS__Data_Ordering, &$PS__Data_Ord_Unsafe, &$PS__Data_Semiring, &$PS__Data_Ring) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Ord = function ($Eq0, $compare) {
        return [
            'Eq0' => $Eq0,
            'compare' => $compare
        ];
    };
    $Ord1 = function ($Eq10, $compare1) {
        return [
            'Eq10' => $Eq10,
            'compare1' => $compare1
        ];
    };
    $ordVoid = $Ord(function ()  use (&$PS__Data_Eq) {
        return $PS__Data_Eq['eqVoid'];
    }, function ($v)  use (&$PS__Data_Ordering) {
        return function ($v1)  use (&$PS__Data_Ordering) {
            return $PS__Data_Ordering['EQ']();
        };
    });
    $ordUnit = $Ord(function ()  use (&$PS__Data_Eq) {
        return $PS__Data_Eq['eqUnit'];
    }, function ($v)  use (&$PS__Data_Ordering) {
        return function ($v1)  use (&$PS__Data_Ordering) {
            return $PS__Data_Ordering['EQ']();
        };
    });
    $ordString = $Ord(function ()  use (&$PS__Data_Eq) {
        return $PS__Data_Eq['eqString'];
    }, $PS__Data_Ord_Unsafe['unsafeCompare']);
    $ordOrdering = $Ord(function ()  use (&$PS__Data_Ordering) {
        return $PS__Data_Ordering['eqOrdering'];
    }, function ($v)  use (&$PS__Data_Ordering) {
        return function ($v1)  use (&$v, &$PS__Data_Ordering) {
            if ($v['type'] === 'LT' && $v1['type'] === 'LT') {
                return $PS__Data_Ordering['EQ']();
            };
            if ($v['type'] === 'EQ' && $v1['type'] === 'EQ') {
                return $PS__Data_Ordering['EQ']();
            };
            if ($v['type'] === 'GT' && $v1['type'] === 'GT') {
                return $PS__Data_Ordering['EQ']();
            };
            if ($v['type'] === 'LT') {
                return $PS__Data_Ordering['LT']();
            };
            if ($v['type'] === 'EQ' && $v1['type'] === 'LT') {
                return $PS__Data_Ordering['GT']();
            };
            if ($v['type'] === 'EQ' && $v1['type'] === 'GT') {
                return $PS__Data_Ordering['LT']();
            };
            if ($v['type'] === 'GT') {
                return $PS__Data_Ordering['GT']();
            };
            throw new \Exception('Failed pattern match at Data.Ord line 67, column 1 - line 67, column 37: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
        };
    });
    $ordNumber = $Ord(function ()  use (&$PS__Data_Eq) {
        return $PS__Data_Eq['eqNumber'];
    }, $PS__Data_Ord_Unsafe['unsafeCompare']);
    $ordInt = $Ord(function ()  use (&$PS__Data_Eq) {
        return $PS__Data_Eq['eqInt'];
    }, $PS__Data_Ord_Unsafe['unsafeCompare']);
    $ordChar = $Ord(function ()  use (&$PS__Data_Eq) {
        return $PS__Data_Eq['eqChar'];
    }, $PS__Data_Ord_Unsafe['unsafeCompare']);
    $ordBoolean = $Ord(function ()  use (&$PS__Data_Eq) {
        return $PS__Data_Eq['eqBoolean'];
    }, $PS__Data_Ord_Unsafe['unsafeCompare']);
    $compare1 = function ($dict) {
        return $dict['compare1'];
    };
    $compare = function ($dict) {
        return $dict['compare'];
    };
    $comparing = function ($dictOrd)  use (&$compare) {
        return function ($f)  use (&$compare, &$dictOrd) {
            return function ($x)  use (&$compare, &$dictOrd, &$f) {
                return function ($y)  use (&$compare, &$dictOrd, &$f, &$x) {
                    return $compare($dictOrd)($f($x))($f($y));
                };
            };
        };
    };
    $greaterThan = function ($dictOrd)  use (&$compare) {
        return function ($a1)  use (&$compare, &$dictOrd) {
            return function ($a2)  use (&$compare, &$dictOrd, &$a1) {
                $v = $compare($dictOrd)($a1)($a2);
                if ($v['type'] === 'GT') {
                    return true;
                };
                return false;
            };
        };
    };
    $greaterThanOrEq = function ($dictOrd)  use (&$compare) {
        return function ($a1)  use (&$compare, &$dictOrd) {
            return function ($a2)  use (&$compare, &$dictOrd, &$a1) {
                $v = $compare($dictOrd)($a1)($a2);
                if ($v['type'] === 'LT') {
                    return false;
                };
                return true;
            };
        };
    };
    $signum = function ($dictOrd)  use (&$greaterThanOrEq, &$PS__Data_Semiring, &$PS__Data_Ring) {
        return function ($dictRing)  use (&$greaterThanOrEq, &$dictOrd, &$PS__Data_Semiring, &$PS__Data_Ring) {
            return function ($x)  use (&$greaterThanOrEq, &$dictOrd, &$PS__Data_Semiring, &$dictRing, &$PS__Data_Ring) {
                $__local_var__33 = $greaterThanOrEq($dictOrd)($x)($PS__Data_Semiring['zero']($dictRing['Semiring0']()));
                if ($__local_var__33) {
                    return $PS__Data_Semiring['one']($dictRing['Semiring0']());
                };
                return $PS__Data_Ring['negate']($dictRing)($PS__Data_Semiring['one']($dictRing['Semiring0']()));
            };
        };
    };
    $lessThan = function ($dictOrd)  use (&$compare) {
        return function ($a1)  use (&$compare, &$dictOrd) {
            return function ($a2)  use (&$compare, &$dictOrd, &$a1) {
                $v = $compare($dictOrd)($a1)($a2);
                if ($v['type'] === 'LT') {
                    return true;
                };
                return false;
            };
        };
    };
    $lessThanOrEq = function ($dictOrd)  use (&$compare) {
        return function ($a1)  use (&$compare, &$dictOrd) {
            return function ($a2)  use (&$compare, &$dictOrd, &$a1) {
                $v = $compare($dictOrd)($a1)($a2);
                if ($v['type'] === 'GT') {
                    return false;
                };
                return true;
            };
        };
    };
    $max = function ($dictOrd)  use (&$compare) {
        return function ($x)  use (&$compare, &$dictOrd) {
            return function ($y)  use (&$compare, &$dictOrd, &$x) {
                $v = $compare($dictOrd)($x)($y);
                if ($v['type'] === 'LT') {
                    return $y;
                };
                if ($v['type'] === 'EQ') {
                    return $x;
                };
                if ($v['type'] === 'GT') {
                    return $x;
                };
                throw new \Exception('Failed pattern match at Data.Ord line 122, column 3 - line 125, column 12: ' . var_dump([ $v['constructor']['name'] ]));
            };
        };
    };
    $min = function ($dictOrd)  use (&$compare) {
        return function ($x)  use (&$compare, &$dictOrd) {
            return function ($y)  use (&$compare, &$dictOrd, &$x) {
                $v = $compare($dictOrd)($x)($y);
                if ($v['type'] === 'LT') {
                    return $x;
                };
                if ($v['type'] === 'EQ') {
                    return $x;
                };
                if ($v['type'] === 'GT') {
                    return $y;
                };
                throw new \Exception('Failed pattern match at Data.Ord line 113, column 3 - line 116, column 12: ' . var_dump([ $v['constructor']['name'] ]));
            };
        };
    };
    $ordArray = function ($dictOrd)  use (&$Ord, &$PS__Data_Eq, &$compare, &$PS__Data_Ring, &$ordInt, &$__foreign) {
        return $Ord(function ()  use (&$PS__Data_Eq, &$dictOrd) {
            return $PS__Data_Eq['eqArray']($dictOrd['Eq0']());
        }, (function ()  use (&$compare, &$dictOrd, &$PS__Data_Ring, &$ordInt, &$__foreign) {
            $toDelta = function ($x)  use (&$compare, &$dictOrd, &$PS__Data_Ring) {
                return function ($y)  use (&$compare, &$dictOrd, &$x, &$PS__Data_Ring) {
                    $v = $compare($dictOrd)($x)($y);
                    if ($v['type'] === 'EQ') {
                        return 0;
                    };
                    if ($v['type'] === 'LT') {
                        return 1;
                    };
                    if ($v['type'] === 'GT') {
                        return $PS__Data_Ring['negate']($PS__Data_Ring['ringInt'])(1);
                    };
                    throw new \Exception('Failed pattern match at Data.Ord line 60, column 7 - line 65, column 1: ' . var_dump([ $v['constructor']['name'] ]));
                };
            };
            return function ($xs)  use (&$compare, &$ordInt, &$__foreign, &$toDelta) {
                return function ($ys)  use (&$compare, &$ordInt, &$__foreign, &$toDelta, &$xs) {
                    return $compare($ordInt)(0)($__foreign['ordArrayImpl']($toDelta)($xs)($ys));
                };
            };
        })());
    };
    $ord1Array = $Ord1(function ()  use (&$PS__Data_Eq) {
        return $PS__Data_Eq['eq1Array'];
    }, function ($dictOrd)  use (&$compare, &$ordArray) {
        return $compare($ordArray($dictOrd));
    });
    $clamp = function ($dictOrd)  use (&$min, &$max) {
        return function ($low)  use (&$min, &$dictOrd, &$max) {
            return function ($hi)  use (&$min, &$dictOrd, &$max, &$low) {
                return function ($x)  use (&$min, &$dictOrd, &$hi, &$max, &$low) {
                    return $min($dictOrd)($hi)($max($dictOrd)($low)($x));
                };
            };
        };
    };
    $between = function ($dictOrd)  use (&$lessThan, &$greaterThan) {
        return function ($low)  use (&$lessThan, &$dictOrd, &$greaterThan) {
            return function ($hi)  use (&$lessThan, &$dictOrd, &$low, &$greaterThan) {
                return function ($x)  use (&$lessThan, &$dictOrd, &$low, &$greaterThan, &$hi) {
                    if ($lessThan($dictOrd)($x)($low)) {
                        return false;
                    };
                    if ($greaterThan($dictOrd)($x)($hi)) {
                        return false;
                    };
                    return true;
                };
            };
        };
    };
    $abs = function ($dictOrd)  use (&$greaterThanOrEq, &$PS__Data_Semiring, &$PS__Data_Ring) {
        return function ($dictRing)  use (&$greaterThanOrEq, &$dictOrd, &$PS__Data_Semiring, &$PS__Data_Ring) {
            return function ($x)  use (&$greaterThanOrEq, &$dictOrd, &$PS__Data_Semiring, &$dictRing, &$PS__Data_Ring) {
                $__local_var__42 = $greaterThanOrEq($dictOrd)($x)($PS__Data_Semiring['zero']($dictRing['Semiring0']()));
                if ($__local_var__42) {
                    return $x;
                };
                return $PS__Data_Ring['negate']($dictRing)($x);
            };
        };
    };
    return [
        'Ord' => $Ord,
        'compare' => $compare,
        'Ord1' => $Ord1,
        'compare1' => $compare1,
        'lessThan' => $lessThan,
        'lessThanOrEq' => $lessThanOrEq,
        'greaterThan' => $greaterThan,
        'greaterThanOrEq' => $greaterThanOrEq,
        'comparing' => $comparing,
        'min' => $min,
        'max' => $max,
        'clamp' => $clamp,
        'between' => $between,
        'abs' => $abs,
        'signum' => $signum,
        'ordBoolean' => $ordBoolean,
        'ordInt' => $ordInt,
        'ordNumber' => $ordNumber,
        'ordString' => $ordString,
        'ordChar' => $ordChar,
        'ordUnit' => $ordUnit,
        'ordVoid' => $ordVoid,
        'ordArray' => $ordArray,
        'ordOrdering' => $ordOrdering,
        'ord1Array' => $ord1Array
    ];
})();
