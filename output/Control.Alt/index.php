<?php
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
$PS__Control_Alt = (function ()  use (&$PS__Data_Functor, &$PS__Data_Semigroup) {
    $Alt = function ($Functor0, $alt) {
        return [
            'Functor0' => $Functor0,
            'alt' => $alt
        ];
    };
    $altArray = $Alt(function ()  use (&$PS__Data_Functor) {
        return $PS__Data_Functor['functorArray'];
    }, $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupArray']));
    $alt = function ($dict) {
        return $dict['alt'];
    };
    return [
        'Alt' => $Alt,
        'alt' => $alt,
        'altArray' => $altArray
    ];
})();
