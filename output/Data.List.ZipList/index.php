<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.List.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Data.List.Lazy.Types/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Partial.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_List_ZipList = (function ()  use (&$PS__Data_List_Lazy_Types, &$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Newtype, &$PS__Control_Apply, &$PS__Data_List_Lazy, &$PS__Data_Function, &$PS__Control_Bind, &$PS__Partial_Unsafe, &$PS__Control_Applicative, &$PS__Control_Semigroupoid, &$PS__Control_Alt, &$PS__Control_Plus, &$PS__Data_Monoid, &$PS__Control_Alternative) {
    $ZipList = function ($x) {
        return $x;
    };
    $traversableZipList = $PS__Data_List_Lazy_Types['traversableList'];
    $showZipList = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_List_Lazy_Types) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$PS__Data_List_Lazy_Types, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(ZipList ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_List_Lazy_Types['showList']($dictShow))($v))(')'));
        });
    };
    $semigroupZipList = $PS__Data_List_Lazy_Types['semigroupList'];
    $ordZipList = function ($dictOrd)  use (&$PS__Data_List_Lazy_Types) {
        return $PS__Data_List_Lazy_Types['ordList']($dictOrd);
    };
    $newtypeZipList = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $ZipList);
    $monoidZipList = $PS__Data_List_Lazy_Types['monoidList'];
    $functorZipList = $PS__Data_List_Lazy_Types['functorList'];
    $foldableZipList = $PS__Data_List_Lazy_Types['foldableList'];
    $eqZipList = function ($dictEq)  use (&$PS__Data_List_Lazy_Types) {
        return $PS__Data_List_Lazy_Types['eqList']($dictEq);
    };
    $applyZipList = $PS__Control_Apply['Apply'](function ()  use (&$functorZipList) {
        return $functorZipList;
    }, function ($v)  use (&$PS__Data_List_Lazy, &$PS__Data_Function) {
        return function ($v1)  use (&$PS__Data_List_Lazy, &$PS__Data_Function, &$v) {
            return $PS__Data_List_Lazy['zipWith']($PS__Data_Function['apply'])($v)($v1);
        };
    });
    $zipListIsNotBind = function ($dictFail)  use (&$PS__Control_Bind, &$applyZipList, &$PS__Partial_Unsafe) {
        return $PS__Control_Bind['Bind'](function ()  use (&$applyZipList) {
            return $applyZipList;
        }, $PS__Partial_Unsafe['unsafeCrashWith']('bind: unreachable'));
    };
    $applicativeZipList = $PS__Control_Applicative['Applicative'](function ()  use (&$applyZipList) {
        return $applyZipList;
    }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($ZipList)($PS__Data_List_Lazy['repeat']));
    $altZipList = $PS__Control_Alt['Alt'](function ()  use (&$functorZipList) {
        return $functorZipList;
    }, $PS__Data_Semigroup['append']($semigroupZipList));
    $plusZipList = $PS__Control_Plus['Plus'](function ()  use (&$altZipList) {
        return $altZipList;
    }, $PS__Data_Monoid['mempty']($monoidZipList));
    $alternativeZipList = $PS__Control_Alternative['Alternative'](function ()  use (&$applicativeZipList) {
        return $applicativeZipList;
    }, function ()  use (&$plusZipList) {
        return $plusZipList;
    });
    return [
        'ZipList' => $ZipList,
        'showZipList' => $showZipList,
        'newtypeZipList' => $newtypeZipList,
        'eqZipList' => $eqZipList,
        'ordZipList' => $ordZipList,
        'semigroupZipList' => $semigroupZipList,
        'monoidZipList' => $monoidZipList,
        'foldableZipList' => $foldableZipList,
        'traversableZipList' => $traversableZipList,
        'functorZipList' => $functorZipList,
        'applyZipList' => $applyZipList,
        'applicativeZipList' => $applicativeZipList,
        'altZipList' => $altZipList,
        'plusZipList' => $plusZipList,
        'alternativeZipList' => $alternativeZipList,
        'zipListIsNotBind' => $zipListIsNotBind
    ];
})();
