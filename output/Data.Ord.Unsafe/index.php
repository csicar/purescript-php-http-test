<?php
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
$PS__Data_Ord_Unsafe = (function ()  use (&$PS__Data_Ordering) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $unsafeCompare = $__foreign['unsafeCompareImpl']($PS__Data_Ordering['LT']())($PS__Data_Ordering['EQ']())($PS__Data_Ordering['GT']());
    return [
        'unsafeCompare' => $unsafeCompare
    ];
})();
