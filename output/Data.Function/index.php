<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Data.Boolean/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
$PS__Data_Function = (function ()  use (&$PS__Data_Ord, &$PS__Data_Boolean, &$PS__Data_Ring) {
    $on = function ($f) {
        return function ($g)  use (&$f) {
            return function ($x)  use (&$f, &$g) {
                return function ($y)  use (&$f, &$g, &$x) {
                    return $f($g($x))($g($y));
                };
            };
        };
    };
    $flip = function ($f) {
        return function ($b)  use (&$f) {
            return function ($a)  use (&$f, &$b) {
                return $f($a)($b);
            };
        };
    };
    $__const = function ($a) {
        return function ($v)  use (&$a) {
            return $a;
        };
    };
    $applyN = function ($f)  use (&$PS__Data_Ord, &$PS__Data_Boolean, &$PS__Data_Ring) {
        $go = function ($_dollar_copy_n)  use (&$PS__Data_Ord, &$PS__Data_Boolean, &$PS__Data_Ring, &$f) {
            return function ($_dollar_copy_acc)  use (&$_dollar_copy_n, &$PS__Data_Ord, &$PS__Data_Boolean, &$PS__Data_Ring, &$f) {
                $_dollar_tco_var_n = $_dollar_copy_n;
                $_dollar_tco_done = false;
                $_dollar_tco_result = NULL;
                $_dollar_tco_loop = function ($n, $acc)  use (&$PS__Data_Ord, &$_dollar_tco_done, &$PS__Data_Boolean, &$_dollar_tco_var_n, &$PS__Data_Ring, &$_dollar_copy_acc, &$f) {
                    if ($PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])($n)(0)) {
                        $_dollar_tco_done = true;
                        return $acc;
                    };
                    if ($PS__Data_Boolean['otherwise']) {
                        $_dollar_tco_var_n = $PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($n)(1);
                        $_dollar_copy_acc = $f($acc);
                        return;
                    };
                    throw new \Exception('Failed pattern match at Data.Function line 94, column 3 - line 96, column 37: ' . var_dump([ $n['constructor']['name'], $acc['constructor']['name'] ]));
                };
                while (!$_dollar_tco_done) {
                    $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_n, $_dollar_copy_acc);
                };
                return $_dollar_tco_result;
            };
        };
        return $go;
    };
    $applyFlipped = function ($x) {
        return function ($f)  use (&$x) {
            return $f($x);
        };
    };
    $apply = function ($f) {
        return function ($x)  use (&$f) {
            return $f($x);
        };
    };
    return [
        'flip' => $flip,
        'const' => $__const,
        'apply' => $apply,
        'applyFlipped' => $applyFlipped,
        'applyN' => $applyN,
        'on' => $on
    ];
})();
