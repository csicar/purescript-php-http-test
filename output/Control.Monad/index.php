<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
$PS__Control_Monad = (function ()  use (&$PS__Control_Bind, &$PS__Control_Applicative) {
    $Monad = function ($Applicative0, $Bind1) {
        return [
            'Applicative0' => $Applicative0,
            'Bind1' => $Bind1
        ];
    };
    $whenM = function ($dictMonad)  use (&$PS__Control_Bind, &$PS__Control_Applicative) {
        return function ($mb)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Control_Applicative) {
            return function ($m)  use (&$PS__Control_Bind, &$dictMonad, &$mb, &$PS__Control_Applicative) {
                return $PS__Control_Bind['bind']($dictMonad['Bind1']())($mb)(function ($v)  use (&$PS__Control_Applicative, &$dictMonad, &$m) {
                    return $PS__Control_Applicative['when']($dictMonad['Applicative0']())($v)($m);
                });
            };
        };
    };
    $unlessM = function ($dictMonad)  use (&$PS__Control_Bind, &$PS__Control_Applicative) {
        return function ($mb)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Control_Applicative) {
            return function ($m)  use (&$PS__Control_Bind, &$dictMonad, &$mb, &$PS__Control_Applicative) {
                return $PS__Control_Bind['bind']($dictMonad['Bind1']())($mb)(function ($v)  use (&$PS__Control_Applicative, &$dictMonad, &$m) {
                    return $PS__Control_Applicative['unless']($dictMonad['Applicative0']())($v)($m);
                });
            };
        };
    };
    $monadFn = $Monad(function ()  use (&$PS__Control_Applicative) {
        return $PS__Control_Applicative['applicativeFn'];
    }, function ()  use (&$PS__Control_Bind) {
        return $PS__Control_Bind['bindFn'];
    });
    $monadArray = $Monad(function ()  use (&$PS__Control_Applicative) {
        return $PS__Control_Applicative['applicativeArray'];
    }, function ()  use (&$PS__Control_Bind) {
        return $PS__Control_Bind['bindArray'];
    });
    $liftM1 = function ($dictMonad)  use (&$PS__Control_Bind, &$PS__Control_Applicative) {
        return function ($f)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Control_Applicative) {
            return function ($a)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Control_Applicative, &$f) {
                return $PS__Control_Bind['bind']($dictMonad['Bind1']())($a)(function ($v)  use (&$PS__Control_Applicative, &$dictMonad, &$f) {
                    return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($f($v));
                });
            };
        };
    };
    $ap = function ($dictMonad)  use (&$PS__Control_Bind, &$PS__Control_Applicative) {
        return function ($f)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Control_Applicative) {
            return function ($a)  use (&$PS__Control_Bind, &$dictMonad, &$f, &$PS__Control_Applicative) {
                return $PS__Control_Bind['bind']($dictMonad['Bind1']())($f)(function ($v)  use (&$PS__Control_Bind, &$dictMonad, &$a, &$PS__Control_Applicative) {
                    return $PS__Control_Bind['bind']($dictMonad['Bind1']())($a)(function ($v1)  use (&$PS__Control_Applicative, &$dictMonad, &$v) {
                        return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($v($v1));
                    });
                });
            };
        };
    };
    return [
        'Monad' => $Monad,
        'liftM1' => $liftM1,
        'ap' => $ap,
        'whenM' => $whenM,
        'unlessM' => $unlessM,
        'monadFn' => $monadFn,
        'monadArray' => $monadArray
    ];
})();
