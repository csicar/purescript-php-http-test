<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Biapplicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Biapply/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Bifunctor_Join = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Bifunctor, &$PS__Control_Apply, &$PS__Control_Biapply, &$PS__Control_Applicative, &$PS__Control_Biapplicative) {
    $Join = function ($x) {
        return $x;
    };
    $showJoin = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Join ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $ordJoin = function ($dictOrd) {
        return $dictOrd;
    };
    $newtypeJoin = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Join);
    $eqJoin = function ($dictEq) {
        return $dictEq;
    };
    $bifunctorJoin = function ($dictBifunctor)  use (&$PS__Data_Functor, &$PS__Data_Bifunctor) {
        return $PS__Data_Functor['Functor'](function ($f)  use (&$PS__Data_Bifunctor, &$dictBifunctor) {
            return function ($v)  use (&$PS__Data_Bifunctor, &$dictBifunctor, &$f) {
                return $PS__Data_Bifunctor['bimap']($dictBifunctor)($f)($f)($v);
            };
        });
    };
    $biapplyJoin = function ($dictBiapply)  use (&$PS__Control_Apply, &$bifunctorJoin, &$PS__Control_Biapply) {
        return $PS__Control_Apply['Apply'](function ()  use (&$bifunctorJoin, &$dictBiapply) {
            return $bifunctorJoin($dictBiapply['Bifunctor0']());
        }, function ($v)  use (&$PS__Control_Biapply, &$dictBiapply) {
            return function ($v1)  use (&$PS__Control_Biapply, &$dictBiapply, &$v) {
                return $PS__Control_Biapply['biapply']($dictBiapply)($v)($v1);
            };
        });
    };
    $biapplicativeJoin = function ($dictBiapplicative)  use (&$PS__Control_Applicative, &$biapplyJoin, &$PS__Control_Biapplicative) {
        return $PS__Control_Applicative['Applicative'](function ()  use (&$biapplyJoin, &$dictBiapplicative) {
            return $biapplyJoin($dictBiapplicative['Biapply0']());
        }, function ($a)  use (&$PS__Control_Biapplicative, &$dictBiapplicative) {
            return $PS__Control_Biapplicative['bipure']($dictBiapplicative)($a)($a);
        });
    };
    return [
        'Join' => $Join,
        'newtypeJoin' => $newtypeJoin,
        'eqJoin' => $eqJoin,
        'ordJoin' => $ordJoin,
        'showJoin' => $showJoin,
        'bifunctorJoin' => $bifunctorJoin,
        'biapplyJoin' => $biapplyJoin,
        'biapplicativeJoin' => $biapplicativeJoin
    ];
})();
