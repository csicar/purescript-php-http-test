<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Error.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Reader.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.State.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Trans.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Writer.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Effect.Class/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_RWS_Trans = (function ()  use (&$PS__Data_Tuple, &$PS__Data_Newtype, &$PS__Control_Monad_Trans_Class, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Monoid, &$PS__Control_Lazy, &$PS__Data_Unit, &$PS__Data_Functor, &$PS__Control_Apply, &$PS__Data_Semigroup, &$PS__Control_Monad, &$PS__Control_Monad_Reader_Class, &$PS__Effect_Class, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Rec_Class, &$PS__Control_Monad_State_Class, &$PS__Control_Monad_Writer_Class, &$PS__Control_Monad_Error_Class, &$PS__Control_Alt, &$PS__Control_Plus, &$PS__Control_Alternative) {
    $RWSResult = [
        'constructor' => function ($value0, $value1, $value2) {
            return [
                'type' => 'RWSResult',
                'value0' => $value0,
                'value1' => $value1,
                'value2' => $value2
            ];
        },
        'create' => function ($value0)  use (&$RWSResult) {
            return function ($value1)  use (&$RWSResult, &$value0) {
                return function ($value2)  use (&$RWSResult, &$value0, &$value1) {
                    return $RWSResult['constructor']($value0, $value1, $value2);
                };
            };
        }
    ];
    $RWST = function ($x) {
        return $x;
    };
    $withRWST = function ($f)  use (&$PS__Data_Tuple) {
        return function ($m)  use (&$PS__Data_Tuple, &$f) {
            return function ($r)  use (&$PS__Data_Tuple, &$m, &$f) {
                return function ($s)  use (&$PS__Data_Tuple, &$m, &$f, &$r) {
                    return $PS__Data_Tuple['uncurry']($m)($f($r)($s));
                };
            };
        };
    };
    $runRWST = function ($v) {
        return $v;
    };
    $newtypeRWST = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $RWST);
    $monadTransRWST = function ($dictMonoid)  use (&$PS__Control_Monad_Trans_Class, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Monoid) {
        return $PS__Control_Monad_Trans_Class['MonadTrans'](function ($dictMonad)  use (&$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Monoid, &$dictMonoid) {
            return function ($m)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Monoid, &$dictMonoid) {
                return function ($v)  use (&$PS__Control_Bind, &$dictMonad, &$m, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Monoid, &$dictMonoid) {
                    return function ($s)  use (&$PS__Control_Bind, &$dictMonad, &$m, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Monoid, &$dictMonoid) {
                        return $PS__Control_Bind['bind']($dictMonad['Bind1']())($m)(function ($a)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$RWSResult, &$s, &$PS__Data_Monoid, &$dictMonoid) {
                            return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($RWSResult['constructor']($s, $a, $PS__Data_Monoid['mempty']($dictMonoid)));
                        });
                    };
                };
            };
        });
    };
    $mapRWST = function ($f) {
        return function ($v)  use (&$f) {
            return function ($r)  use (&$f, &$v) {
                return function ($s)  use (&$f, &$v, &$r) {
                    return $f($v($r)($s));
                };
            };
        };
    };
    $lazyRWST = $PS__Control_Lazy['Lazy'](function ($f)  use (&$PS__Data_Unit) {
        return function ($r)  use (&$f, &$PS__Data_Unit) {
            return function ($s)  use (&$f, &$PS__Data_Unit, &$r) {
                $v = $f($PS__Data_Unit['unit']);
                return $v($r)($s);
            };
        };
    });
    $functorRWST = function ($dictFunctor)  use (&$PS__Data_Functor, &$RWSResult) {
        return $PS__Data_Functor['Functor'](function ($f)  use (&$PS__Data_Functor, &$dictFunctor, &$RWSResult) {
            return function ($v)  use (&$PS__Data_Functor, &$dictFunctor, &$RWSResult, &$f) {
                return function ($r)  use (&$PS__Data_Functor, &$dictFunctor, &$RWSResult, &$f, &$v) {
                    return function ($s)  use (&$PS__Data_Functor, &$dictFunctor, &$RWSResult, &$f, &$v, &$r) {
                        return $PS__Data_Functor['map']($dictFunctor)(function ($v1)  use (&$RWSResult, &$f) {
                            return $RWSResult['constructor']($v1['value0'], $f($v1['value1']), $v1['value2']);
                        })($v($r)($s));
                    };
                };
            };
        });
    };
    $execRWST = function ($dictMonad)  use (&$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Control_Applicative, &$PS__Data_Tuple) {
            return function ($r)  use (&$PS__Control_Bind, &$dictMonad, &$v, &$PS__Control_Applicative, &$PS__Data_Tuple) {
                return function ($s)  use (&$PS__Control_Bind, &$dictMonad, &$v, &$r, &$PS__Control_Applicative, &$PS__Data_Tuple) {
                    return $PS__Control_Bind['bind']($dictMonad['Bind1']())($v($r)($s))(function ($v1)  use (&$PS__Control_Applicative, &$dictMonad, &$PS__Data_Tuple) {
                        return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($PS__Data_Tuple['Tuple']['constructor']($v1['value0'], $v1['value2']));
                    });
                };
            };
        };
    };
    $evalRWST = function ($dictMonad)  use (&$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Control_Applicative, &$PS__Data_Tuple) {
            return function ($r)  use (&$PS__Control_Bind, &$dictMonad, &$v, &$PS__Control_Applicative, &$PS__Data_Tuple) {
                return function ($s)  use (&$PS__Control_Bind, &$dictMonad, &$v, &$r, &$PS__Control_Applicative, &$PS__Data_Tuple) {
                    return $PS__Control_Bind['bind']($dictMonad['Bind1']())($v($r)($s))(function ($v1)  use (&$PS__Control_Applicative, &$dictMonad, &$PS__Data_Tuple) {
                        return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($PS__Data_Tuple['Tuple']['constructor']($v1['value1'], $v1['value2']));
                    });
                };
            };
        };
    };
    $applyRWST = function ($dictBind)  use (&$PS__Control_Apply, &$functorRWST, &$PS__Control_Bind, &$PS__Data_Functor, &$RWSResult, &$PS__Data_Semigroup) {
        return function ($dictMonoid)  use (&$PS__Control_Apply, &$functorRWST, &$dictBind, &$PS__Control_Bind, &$PS__Data_Functor, &$RWSResult, &$PS__Data_Semigroup) {
            return $PS__Control_Apply['Apply'](function ()  use (&$functorRWST, &$dictBind) {
                return $functorRWST(($dictBind['Apply0']())['Functor0']());
            }, function ($v)  use (&$PS__Control_Bind, &$dictBind, &$PS__Data_Functor, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid) {
                return function ($v1)  use (&$PS__Control_Bind, &$dictBind, &$v, &$PS__Data_Functor, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid) {
                    return function ($r)  use (&$PS__Control_Bind, &$dictBind, &$v, &$PS__Data_Functor, &$v1, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid) {
                        return function ($s)  use (&$PS__Control_Bind, &$dictBind, &$v, &$r, &$PS__Data_Functor, &$v1, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid) {
                            return $PS__Control_Bind['bind']($dictBind)($v($r)($s))(function ($v2)  use (&$PS__Data_Functor, &$dictBind, &$v1, &$r, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid) {
                                return $PS__Data_Functor['mapFlipped'](($dictBind['Apply0']())['Functor0']())($v1($r)($v2['value0']))(function ($v3)  use (&$RWSResult, &$v2, &$PS__Data_Semigroup, &$dictMonoid) {
                                    return $RWSResult['constructor']($v3['value0'], $v2['value1']($v3['value1']), $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($v2['value2'])($v3['value2']));
                                });
                            });
                        };
                    };
                };
            });
        };
    };
    $bindRWST = function ($dictBind)  use (&$PS__Control_Bind, &$applyRWST, &$PS__Data_Functor, &$RWSResult, &$PS__Data_Semigroup) {
        return function ($dictMonoid)  use (&$PS__Control_Bind, &$applyRWST, &$dictBind, &$PS__Data_Functor, &$RWSResult, &$PS__Data_Semigroup) {
            return $PS__Control_Bind['Bind'](function ()  use (&$applyRWST, &$dictBind, &$dictMonoid) {
                return $applyRWST($dictBind)($dictMonoid);
            }, function ($v)  use (&$PS__Control_Bind, &$dictBind, &$PS__Data_Functor, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid) {
                return function ($f)  use (&$PS__Control_Bind, &$dictBind, &$v, &$PS__Data_Functor, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid) {
                    return function ($r)  use (&$PS__Control_Bind, &$dictBind, &$v, &$f, &$PS__Data_Functor, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid) {
                        return function ($s)  use (&$PS__Control_Bind, &$dictBind, &$v, &$r, &$f, &$PS__Data_Functor, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid) {
                            return $PS__Control_Bind['bind']($dictBind)($v($r)($s))(function ($v1)  use (&$f, &$PS__Data_Functor, &$dictBind, &$r, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid) {
                                $v2 = $f($v1['value1']);
                                return $PS__Data_Functor['mapFlipped'](($dictBind['Apply0']())['Functor0']())($v2($r)($v1['value0']))(function ($v3)  use (&$RWSResult, &$PS__Data_Semigroup, &$dictMonoid, &$v1) {
                                    return $RWSResult['constructor']($v3['value0'], $v3['value1'], $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($v1['value2'])($v3['value2']));
                                });
                            });
                        };
                    };
                };
            });
        };
    };
    $applicativeRWST = function ($dictMonad)  use (&$PS__Control_Applicative, &$applyRWST, &$PS__Data_Function, &$RWSResult, &$PS__Data_Monoid) {
        return function ($dictMonoid)  use (&$PS__Control_Applicative, &$applyRWST, &$dictMonad, &$PS__Data_Function, &$RWSResult, &$PS__Data_Monoid) {
            return $PS__Control_Applicative['Applicative'](function ()  use (&$applyRWST, &$dictMonad, &$dictMonoid) {
                return $applyRWST($dictMonad['Bind1']())($dictMonoid);
            }, function ($a)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$RWSResult, &$PS__Data_Monoid, &$dictMonoid) {
                return function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$RWSResult, &$a, &$PS__Data_Monoid, &$dictMonoid) {
                    return function ($s)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$RWSResult, &$a, &$PS__Data_Monoid, &$dictMonoid) {
                        return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($RWSResult['constructor']($s, $a, $PS__Data_Monoid['mempty']($dictMonoid)));
                    };
                };
            });
        };
    };
    $monadRWST = function ($dictMonad)  use (&$PS__Control_Monad, &$applicativeRWST, &$bindRWST) {
        return function ($dictMonoid)  use (&$PS__Control_Monad, &$applicativeRWST, &$dictMonad, &$bindRWST) {
            return $PS__Control_Monad['Monad'](function ()  use (&$applicativeRWST, &$dictMonad, &$dictMonoid) {
                return $applicativeRWST($dictMonad)($dictMonoid);
            }, function ()  use (&$bindRWST, &$dictMonad, &$dictMonoid) {
                return $bindRWST($dictMonad['Bind1']())($dictMonoid);
            });
        };
    };
    $monadAskRWST = function ($dictMonad)  use (&$PS__Control_Monad_Reader_Class, &$monadRWST, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Monoid) {
        return function ($dictMonoid)  use (&$PS__Control_Monad_Reader_Class, &$monadRWST, &$dictMonad, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Monoid) {
            return $PS__Control_Monad_Reader_Class['MonadAsk'](function ()  use (&$monadRWST, &$dictMonad, &$dictMonoid) {
                return $monadRWST($dictMonad)($dictMonoid);
            }, function ($r)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$RWSResult, &$PS__Data_Monoid, &$dictMonoid) {
                return function ($s)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$RWSResult, &$r, &$PS__Data_Monoid, &$dictMonoid) {
                    return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($RWSResult['constructor']($s, $r, $PS__Data_Monoid['mempty']($dictMonoid)));
                };
            });
        };
    };
    $monadReaderRWST = function ($dictMonad)  use (&$PS__Control_Monad_Reader_Class, &$monadAskRWST) {
        return function ($dictMonoid)  use (&$PS__Control_Monad_Reader_Class, &$monadAskRWST, &$dictMonad) {
            return $PS__Control_Monad_Reader_Class['MonadReader'](function ()  use (&$monadAskRWST, &$dictMonad, &$dictMonoid) {
                return $monadAskRWST($dictMonad)($dictMonoid);
            }, function ($f) {
                return function ($m)  use (&$f) {
                    return function ($r)  use (&$m, &$f) {
                        return function ($s)  use (&$m, &$f, &$r) {
                            return $m($f($r))($s);
                        };
                    };
                };
            });
        };
    };
    $monadEffectRWS = function ($dictMonoid)  use (&$PS__Effect_Class, &$monadRWST, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransRWST) {
        return function ($dictMonadEffect)  use (&$PS__Effect_Class, &$monadRWST, &$dictMonoid, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransRWST) {
            return $PS__Effect_Class['MonadEffect'](function ()  use (&$monadRWST, &$dictMonadEffect, &$dictMonoid) {
                return $monadRWST($dictMonadEffect['Monad0']())($dictMonoid);
            }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Trans_Class['lift']($monadTransRWST($dictMonoid))($dictMonadEffect['Monad0']()))($PS__Effect_Class['liftEffect']($dictMonadEffect)));
        };
    };
    $monadRecRWST = function ($dictMonadRec)  use (&$PS__Control_Monad_Rec_Class, &$monadRWST, &$PS__Control_Bind, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
        return function ($dictMonoid)  use (&$PS__Control_Monad_Rec_Class, &$monadRWST, &$dictMonadRec, &$PS__Control_Bind, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
            return $PS__Control_Monad_Rec_Class['MonadRec'](function ()  use (&$monadRWST, &$dictMonadRec, &$dictMonoid) {
                return $monadRWST($dictMonadRec['Monad0']())($dictMonoid);
            }, function ($k)  use (&$PS__Control_Bind, &$dictMonadRec, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Monoid) {
                return function ($a)  use (&$k, &$PS__Control_Bind, &$dictMonadRec, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Monoid) {
                    $k__prime = function ($r)  use (&$k, &$PS__Control_Bind, &$dictMonadRec, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid) {
                        return function ($v)  use (&$k, &$PS__Control_Bind, &$dictMonadRec, &$r, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid) {
                            $v1 = $k($v['value1']);
                            return $PS__Control_Bind['bind'](($dictMonadRec['Monad0']())['Bind1']())($v1($r)($v['value0']))(function ($v2)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid, &$v) {
                                return $PS__Control_Applicative['pure'](($dictMonadRec['Monad0']())['Applicative0']())((function ()  use (&$v2, &$PS__Control_Monad_Rec_Class, &$RWSResult, &$PS__Data_Semigroup, &$dictMonoid, &$v) {
                                    if ($v2['value1']['type'] === 'Loop') {
                                        return $PS__Control_Monad_Rec_Class['Loop']['constructor']($RWSResult['constructor']($v2['value0'], $v2['value1']['value0'], $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($v['value2'])($v2['value2'])));
                                    };
                                    if ($v2['value1']['type'] === 'Done') {
                                        return $PS__Control_Monad_Rec_Class['Done']['constructor']($RWSResult['constructor']($v2['value0'], $v2['value1']['value0'], $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($v['value2'])($v2['value2'])));
                                    };
                                    throw new \Exception('Failed pattern match at Control.Monad.RWS.Trans line 127, column 16 - line 129, column 68: ' . var_dump([ $v2['value1']['constructor']['name'] ]));
                                })());
                            });
                        };
                    };
                    return function ($r)  use (&$PS__Control_Monad_Rec_Class, &$dictMonadRec, &$k__prime, &$RWSResult, &$a, &$PS__Data_Monoid, &$dictMonoid) {
                        return function ($s)  use (&$PS__Control_Monad_Rec_Class, &$dictMonadRec, &$k__prime, &$r, &$RWSResult, &$a, &$PS__Data_Monoid, &$dictMonoid) {
                            return $PS__Control_Monad_Rec_Class['tailRecM']($dictMonadRec)($k__prime($r))($RWSResult['constructor']($s, $a, $PS__Data_Monoid['mempty']($dictMonoid)));
                        };
                    };
                };
            });
        };
    };
    $monadStateRWST = function ($dictMonad)  use (&$PS__Control_Monad_State_Class, &$monadRWST, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Monoid) {
        return function ($dictMonoid)  use (&$PS__Control_Monad_State_Class, &$monadRWST, &$dictMonad, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Monoid) {
            return $PS__Control_Monad_State_Class['MonadState'](function ()  use (&$monadRWST, &$dictMonad, &$dictMonoid) {
                return $monadRWST($dictMonad)($dictMonoid);
            }, function ($f)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$RWSResult, &$PS__Data_Monoid, &$dictMonoid) {
                return function ($v)  use (&$f, &$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$RWSResult, &$PS__Data_Monoid, &$dictMonoid) {
                    return function ($s)  use (&$f, &$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$RWSResult, &$PS__Data_Monoid, &$dictMonoid) {
                        $v1 = $f($s);
                        return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($RWSResult['constructor']($v1['value1'], $v1['value0'], $PS__Data_Monoid['mempty']($dictMonoid)));
                    };
                };
            });
        };
    };
    $monadTellRWST = function ($dictMonad)  use (&$PS__Control_Monad_Writer_Class, &$monadRWST, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Unit) {
        return function ($dictMonoid)  use (&$PS__Control_Monad_Writer_Class, &$monadRWST, &$dictMonad, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Unit) {
            return $PS__Control_Monad_Writer_Class['MonadTell'](function ()  use (&$monadRWST, &$dictMonad, &$dictMonoid) {
                return $monadRWST($dictMonad)($dictMonoid);
            }, function ($w)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$RWSResult, &$PS__Data_Unit) {
                return function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$RWSResult, &$PS__Data_Unit, &$w) {
                    return function ($s)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$RWSResult, &$PS__Data_Unit, &$w) {
                        return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($RWSResult['constructor']($s, $PS__Data_Unit['unit'], $w));
                    };
                };
            });
        };
    };
    $monadWriterRWST = function ($dictMonad)  use (&$PS__Control_Monad_Writer_Class, &$monadTellRWST, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Tuple) {
        return function ($dictMonoid)  use (&$PS__Control_Monad_Writer_Class, &$monadTellRWST, &$dictMonad, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Tuple) {
            return $PS__Control_Monad_Writer_Class['MonadWriter'](function ()  use (&$monadTellRWST, &$dictMonad, &$dictMonoid) {
                return $monadTellRWST($dictMonad)($dictMonoid);
            }, function ($m)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Tuple) {
                return function ($r)  use (&$PS__Control_Bind, &$dictMonad, &$m, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Tuple) {
                    return function ($s)  use (&$PS__Control_Bind, &$dictMonad, &$m, &$r, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult, &$PS__Data_Tuple) {
                        return $PS__Control_Bind['bind']($dictMonad['Bind1']())($m($r)($s))(function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$RWSResult, &$PS__Data_Tuple) {
                            return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($RWSResult['constructor']($v['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1'], $v['value2']), $v['value2']));
                        });
                    };
                };
            }, function ($m)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult) {
                return function ($r)  use (&$PS__Control_Bind, &$dictMonad, &$m, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult) {
                    return function ($s)  use (&$PS__Control_Bind, &$dictMonad, &$m, &$r, &$PS__Data_Function, &$PS__Control_Applicative, &$RWSResult) {
                        return $PS__Control_Bind['bind']($dictMonad['Bind1']())($m($r)($s))(function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$RWSResult) {
                            return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($RWSResult['constructor']($v['value0'], $v['value1']['value0'], $v['value1']['value1']($v['value2'])));
                        });
                    };
                };
            });
        };
    };
    $monadThrowRWST = function ($dictMonadThrow)  use (&$PS__Control_Monad_Error_Class, &$monadRWST, &$PS__Control_Monad_Trans_Class, &$monadTransRWST) {
        return function ($dictMonoid)  use (&$PS__Control_Monad_Error_Class, &$monadRWST, &$dictMonadThrow, &$PS__Control_Monad_Trans_Class, &$monadTransRWST) {
            return $PS__Control_Monad_Error_Class['MonadThrow'](function ()  use (&$monadRWST, &$dictMonadThrow, &$dictMonoid) {
                return $monadRWST($dictMonadThrow['Monad0']())($dictMonoid);
            }, function ($e)  use (&$PS__Control_Monad_Trans_Class, &$monadTransRWST, &$dictMonoid, &$dictMonadThrow, &$PS__Control_Monad_Error_Class) {
                return $PS__Control_Monad_Trans_Class['lift']($monadTransRWST($dictMonoid))($dictMonadThrow['Monad0']())($PS__Control_Monad_Error_Class['throwError']($dictMonadThrow)($e));
            });
        };
    };
    $monadErrorRWST = function ($dictMonadError)  use (&$PS__Control_Monad_Error_Class, &$monadThrowRWST, &$PS__Data_Function, &$RWST) {
        return function ($dictMonoid)  use (&$PS__Control_Monad_Error_Class, &$monadThrowRWST, &$dictMonadError, &$PS__Data_Function, &$RWST) {
            return $PS__Control_Monad_Error_Class['MonadError'](function ()  use (&$monadThrowRWST, &$dictMonadError, &$dictMonoid) {
                return $monadThrowRWST($dictMonadError['MonadThrow0']())($dictMonoid);
            }, function ($m)  use (&$PS__Data_Function, &$RWST, &$PS__Control_Monad_Error_Class, &$dictMonadError) {
                return function ($h)  use (&$PS__Data_Function, &$RWST, &$PS__Control_Monad_Error_Class, &$dictMonadError, &$m) {
                    return $PS__Data_Function['apply']($RWST)(function ($r)  use (&$PS__Control_Monad_Error_Class, &$dictMonadError, &$m, &$h) {
                        return function ($s)  use (&$PS__Control_Monad_Error_Class, &$dictMonadError, &$m, &$r, &$h) {
                            return $PS__Control_Monad_Error_Class['catchError']($dictMonadError)($m($r)($s))(function ($e)  use (&$h, &$r, &$s) {
                                $v = $h($e);
                                return $v($r)($s);
                            });
                        };
                    });
                };
            });
        };
    };
    $altRWST = function ($dictAlt)  use (&$PS__Control_Alt, &$functorRWST, &$PS__Data_Function, &$RWST) {
        return $PS__Control_Alt['Alt'](function ()  use (&$functorRWST, &$dictAlt) {
            return $functorRWST($dictAlt['Functor0']());
        }, function ($v)  use (&$PS__Data_Function, &$RWST, &$PS__Control_Alt, &$dictAlt) {
            return function ($v1)  use (&$PS__Data_Function, &$RWST, &$PS__Control_Alt, &$dictAlt, &$v) {
                return $PS__Data_Function['apply']($RWST)(function ($r)  use (&$PS__Control_Alt, &$dictAlt, &$v, &$v1) {
                    return function ($s)  use (&$PS__Control_Alt, &$dictAlt, &$v, &$r, &$v1) {
                        return $PS__Control_Alt['alt']($dictAlt)($v($r)($s))($v1($r)($s));
                    };
                });
            };
        });
    };
    $plusRWST = function ($dictPlus)  use (&$PS__Control_Plus, &$altRWST) {
        return $PS__Control_Plus['Plus'](function ()  use (&$altRWST, &$dictPlus) {
            return $altRWST($dictPlus['Alt0']());
        }, function ($v)  use (&$PS__Control_Plus, &$dictPlus) {
            return function ($v1)  use (&$PS__Control_Plus, &$dictPlus) {
                return $PS__Control_Plus['empty']($dictPlus);
            };
        });
    };
    $alternativeRWST = function ($dictMonoid)  use (&$PS__Control_Alternative, &$applicativeRWST, &$plusRWST) {
        return function ($dictAlternative)  use (&$PS__Control_Alternative, &$applicativeRWST, &$dictMonoid, &$plusRWST) {
            return function ($dictMonad)  use (&$PS__Control_Alternative, &$applicativeRWST, &$dictMonoid, &$plusRWST, &$dictAlternative) {
                return $PS__Control_Alternative['Alternative'](function ()  use (&$applicativeRWST, &$dictMonad, &$dictMonoid) {
                    return $applicativeRWST($dictMonad)($dictMonoid);
                }, function ()  use (&$plusRWST, &$dictAlternative) {
                    return $plusRWST($dictAlternative['Plus1']());
                });
            };
        };
    };
    return [
        'RWSResult' => $RWSResult,
        'RWST' => $RWST,
        'runRWST' => $runRWST,
        'evalRWST' => $evalRWST,
        'execRWST' => $execRWST,
        'mapRWST' => $mapRWST,
        'withRWST' => $withRWST,
        'newtypeRWST' => $newtypeRWST,
        'functorRWST' => $functorRWST,
        'applyRWST' => $applyRWST,
        'altRWST' => $altRWST,
        'alternativeRWST' => $alternativeRWST,
        'bindRWST' => $bindRWST,
        'applicativeRWST' => $applicativeRWST,
        'monadRWST' => $monadRWST,
        'monadTransRWST' => $monadTransRWST,
        'lazyRWST' => $lazyRWST,
        'monadEffectRWS' => $monadEffectRWS,
        'monadAskRWST' => $monadAskRWST,
        'monadReaderRWST' => $monadReaderRWST,
        'monadStateRWST' => $monadStateRWST,
        'monadTellRWST' => $monadTellRWST,
        'monadWriterRWST' => $monadWriterRWST,
        'monadThrowRWST' => $monadThrowRWST,
        'monadErrorRWST' => $monadErrorRWST,
        'monadRecRWST' => $monadRecRWST,
        'plusRWST' => $plusRWST
    ];
})();
