<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifoldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor.Clown/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor.Flip/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor.Joker/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor.Product/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor.Wrap/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Bitraversable = (function ()  use (&$PS__Control_Applicative, &$PS__Data_Bifoldable, &$PS__Data_Bifunctor_Joker, &$PS__Data_Functor, &$PS__Data_Traversable, &$PS__Data_Bifunctor_Clown, &$PS__Control_Category, &$PS__Data_Bifunctor_Flip, &$PS__Data_Bifunctor_Product, &$PS__Control_Apply, &$PS__Data_Bifunctor_Wrap, &$PS__Data_Bifunctor) {
    $Bitraversable = function ($Bifoldable1, $Bifunctor0, $bisequence, $bitraverse) {
        return [
            'Bifoldable1' => $Bifoldable1,
            'Bifunctor0' => $Bifunctor0,
            'bisequence' => $bisequence,
            'bitraverse' => $bitraverse
        ];
    };
    $bitraverse = function ($dict) {
        return $dict['bitraverse'];
    };
    $lfor = function ($dictBitraversable)  use (&$bitraverse, &$PS__Control_Applicative) {
        return function ($dictApplicative)  use (&$bitraverse, &$dictBitraversable, &$PS__Control_Applicative) {
            return function ($t)  use (&$bitraverse, &$dictBitraversable, &$dictApplicative, &$PS__Control_Applicative) {
                return function ($f)  use (&$bitraverse, &$dictBitraversable, &$dictApplicative, &$PS__Control_Applicative, &$t) {
                    return $bitraverse($dictBitraversable)($dictApplicative)($f)($PS__Control_Applicative['pure']($dictApplicative))($t);
                };
            };
        };
    };
    $ltraverse = function ($dictBitraversable)  use (&$bitraverse, &$PS__Control_Applicative) {
        return function ($dictApplicative)  use (&$bitraverse, &$dictBitraversable, &$PS__Control_Applicative) {
            return function ($f)  use (&$bitraverse, &$dictBitraversable, &$dictApplicative, &$PS__Control_Applicative) {
                return $bitraverse($dictBitraversable)($dictApplicative)($f)($PS__Control_Applicative['pure']($dictApplicative));
            };
        };
    };
    $rfor = function ($dictBitraversable)  use (&$bitraverse, &$PS__Control_Applicative) {
        return function ($dictApplicative)  use (&$bitraverse, &$dictBitraversable, &$PS__Control_Applicative) {
            return function ($t)  use (&$bitraverse, &$dictBitraversable, &$dictApplicative, &$PS__Control_Applicative) {
                return function ($f)  use (&$bitraverse, &$dictBitraversable, &$dictApplicative, &$PS__Control_Applicative, &$t) {
                    return $bitraverse($dictBitraversable)($dictApplicative)($PS__Control_Applicative['pure']($dictApplicative))($f)($t);
                };
            };
        };
    };
    $rtraverse = function ($dictBitraversable)  use (&$bitraverse, &$PS__Control_Applicative) {
        return function ($dictApplicative)  use (&$bitraverse, &$dictBitraversable, &$PS__Control_Applicative) {
            return $bitraverse($dictBitraversable)($dictApplicative)($PS__Control_Applicative['pure']($dictApplicative));
        };
    };
    $bitraversableJoker = function ($dictTraversable)  use (&$Bitraversable, &$PS__Data_Bifoldable, &$PS__Data_Bifunctor_Joker, &$PS__Data_Functor, &$PS__Data_Traversable) {
        return $Bitraversable(function ()  use (&$PS__Data_Bifoldable, &$dictTraversable) {
            return $PS__Data_Bifoldable['bifoldableJoker']($dictTraversable['Foldable1']());
        }, function ()  use (&$PS__Data_Bifunctor_Joker, &$dictTraversable) {
            return $PS__Data_Bifunctor_Joker['bifunctorJoker']($dictTraversable['Functor0']());
        }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Bifunctor_Joker, &$PS__Data_Traversable, &$dictTraversable) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Joker, &$PS__Data_Traversable, &$dictTraversable) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Bifunctor_Joker['Joker'])($PS__Data_Traversable['sequence']($dictTraversable)($dictApplicative)($v));
            };
        }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Bifunctor_Joker, &$PS__Data_Traversable, &$dictTraversable) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Joker, &$PS__Data_Traversable, &$dictTraversable) {
                return function ($r)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Joker, &$PS__Data_Traversable, &$dictTraversable) {
                    return function ($v1)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Joker, &$PS__Data_Traversable, &$dictTraversable, &$r) {
                        return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Bifunctor_Joker['Joker'])($PS__Data_Traversable['traverse']($dictTraversable)($dictApplicative)($r)($v1));
                    };
                };
            };
        });
    };
    $bitraversableClown = function ($dictTraversable)  use (&$Bitraversable, &$PS__Data_Bifoldable, &$PS__Data_Bifunctor_Clown, &$PS__Data_Functor, &$PS__Data_Traversable) {
        return $Bitraversable(function ()  use (&$PS__Data_Bifoldable, &$dictTraversable) {
            return $PS__Data_Bifoldable['bifoldableClown']($dictTraversable['Foldable1']());
        }, function ()  use (&$PS__Data_Bifunctor_Clown, &$dictTraversable) {
            return $PS__Data_Bifunctor_Clown['bifunctorClown']($dictTraversable['Functor0']());
        }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Bifunctor_Clown, &$PS__Data_Traversable, &$dictTraversable) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Clown, &$PS__Data_Traversable, &$dictTraversable) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Bifunctor_Clown['Clown'])($PS__Data_Traversable['sequence']($dictTraversable)($dictApplicative)($v));
            };
        }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Bifunctor_Clown, &$PS__Data_Traversable, &$dictTraversable) {
            return function ($l)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Clown, &$PS__Data_Traversable, &$dictTraversable) {
                return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Clown, &$PS__Data_Traversable, &$dictTraversable, &$l) {
                    return function ($v1)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Clown, &$PS__Data_Traversable, &$dictTraversable, &$l) {
                        return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Bifunctor_Clown['Clown'])($PS__Data_Traversable['traverse']($dictTraversable)($dictApplicative)($l)($v1));
                    };
                };
            };
        });
    };
    $bisequenceDefault = function ($dictBitraversable)  use (&$bitraverse, &$PS__Control_Category) {
        return function ($dictApplicative)  use (&$bitraverse, &$dictBitraversable, &$PS__Control_Category) {
            return $bitraverse($dictBitraversable)($dictApplicative)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']))($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        };
    };
    $bisequence = function ($dict) {
        return $dict['bisequence'];
    };
    $bitraversableFlip = function ($dictBitraversable)  use (&$Bitraversable, &$PS__Data_Bifoldable, &$PS__Data_Bifunctor_Flip, &$PS__Data_Functor, &$bisequence, &$bitraverse) {
        return $Bitraversable(function ()  use (&$PS__Data_Bifoldable, &$dictBitraversable) {
            return $PS__Data_Bifoldable['bifoldableFlip']($dictBitraversable['Bifoldable1']());
        }, function ()  use (&$PS__Data_Bifunctor_Flip, &$dictBitraversable) {
            return $PS__Data_Bifunctor_Flip['bifunctorFlip']($dictBitraversable['Bifunctor0']());
        }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Bifunctor_Flip, &$bisequence, &$dictBitraversable) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Flip, &$bisequence, &$dictBitraversable) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Bifunctor_Flip['Flip'])($bisequence($dictBitraversable)($dictApplicative)($v));
            };
        }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Bifunctor_Flip, &$bitraverse, &$dictBitraversable) {
            return function ($r)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Flip, &$bitraverse, &$dictBitraversable) {
                return function ($l)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Flip, &$bitraverse, &$dictBitraversable, &$r) {
                    return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Flip, &$bitraverse, &$dictBitraversable, &$l, &$r) {
                        return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Bifunctor_Flip['Flip'])($bitraverse($dictBitraversable)($dictApplicative)($l)($r)($v));
                    };
                };
            };
        });
    };
    $bitraversableProduct = function ($dictBitraversable)  use (&$Bitraversable, &$PS__Data_Bifoldable, &$PS__Data_Bifunctor_Product, &$PS__Control_Apply, &$PS__Data_Functor, &$bisequence, &$bitraverse) {
        return function ($dictBitraversable1)  use (&$Bitraversable, &$PS__Data_Bifoldable, &$dictBitraversable, &$PS__Data_Bifunctor_Product, &$PS__Control_Apply, &$PS__Data_Functor, &$bisequence, &$bitraverse) {
            return $Bitraversable(function ()  use (&$PS__Data_Bifoldable, &$dictBitraversable, &$dictBitraversable1) {
                return $PS__Data_Bifoldable['bifoldableProduct']($dictBitraversable['Bifoldable1']())($dictBitraversable1['Bifoldable1']());
            }, function ()  use (&$PS__Data_Bifunctor_Product, &$dictBitraversable, &$dictBitraversable1) {
                return $PS__Data_Bifunctor_Product['bifunctorProduct']($dictBitraversable['Bifunctor0']())($dictBitraversable1['Bifunctor0']());
            }, function ($dictApplicative)  use (&$PS__Control_Apply, &$PS__Data_Functor, &$PS__Data_Bifunctor_Product, &$bisequence, &$dictBitraversable, &$dictBitraversable1) {
                return function ($v)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$PS__Data_Bifunctor_Product, &$bisequence, &$dictBitraversable, &$dictBitraversable1) {
                    return $PS__Control_Apply['apply']($dictApplicative['Apply0']())($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Bifunctor_Product['Product']['create'])($bisequence($dictBitraversable)($dictApplicative)($v['value0'])))($bisequence($dictBitraversable1)($dictApplicative)($v['value1']));
                };
            }, function ($dictApplicative)  use (&$PS__Control_Apply, &$PS__Data_Functor, &$PS__Data_Bifunctor_Product, &$bitraverse, &$dictBitraversable, &$dictBitraversable1) {
                return function ($l)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$PS__Data_Bifunctor_Product, &$bitraverse, &$dictBitraversable, &$dictBitraversable1) {
                    return function ($r)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$PS__Data_Bifunctor_Product, &$bitraverse, &$dictBitraversable, &$l, &$dictBitraversable1) {
                        return function ($v)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$PS__Data_Bifunctor_Product, &$bitraverse, &$dictBitraversable, &$l, &$r, &$dictBitraversable1) {
                            return $PS__Control_Apply['apply']($dictApplicative['Apply0']())($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Bifunctor_Product['Product']['create'])($bitraverse($dictBitraversable)($dictApplicative)($l)($r)($v['value0'])))($bitraverse($dictBitraversable1)($dictApplicative)($l)($r)($v['value1']));
                        };
                    };
                };
            });
        };
    };
    $bitraversableWrap = function ($dictBitraversable)  use (&$Bitraversable, &$PS__Data_Bifoldable, &$PS__Data_Bifunctor_Wrap, &$PS__Data_Functor, &$bisequence, &$bitraverse) {
        return $Bitraversable(function ()  use (&$PS__Data_Bifoldable, &$dictBitraversable) {
            return $PS__Data_Bifoldable['bifoldableWrap']($dictBitraversable['Bifoldable1']());
        }, function ()  use (&$PS__Data_Bifunctor_Wrap, &$dictBitraversable) {
            return $PS__Data_Bifunctor_Wrap['bifunctorWrap']($dictBitraversable['Bifunctor0']());
        }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Bifunctor_Wrap, &$bisequence, &$dictBitraversable) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Wrap, &$bisequence, &$dictBitraversable) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Bifunctor_Wrap['Wrap'])($bisequence($dictBitraversable)($dictApplicative)($v));
            };
        }, function ($dictApplicative)  use (&$PS__Data_Functor, &$PS__Data_Bifunctor_Wrap, &$bitraverse, &$dictBitraversable) {
            return function ($l)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Wrap, &$bitraverse, &$dictBitraversable) {
                return function ($r)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Wrap, &$bitraverse, &$dictBitraversable, &$l) {
                    return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Bifunctor_Wrap, &$bitraverse, &$dictBitraversable, &$l, &$r) {
                        return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Bifunctor_Wrap['Wrap'])($bitraverse($dictBitraversable)($dictApplicative)($l)($r)($v));
                    };
                };
            };
        });
    };
    $bitraverseDefault = function ($dictBitraversable)  use (&$bisequence, &$PS__Data_Bifunctor) {
        return function ($dictApplicative)  use (&$bisequence, &$dictBitraversable, &$PS__Data_Bifunctor) {
            return function ($f)  use (&$bisequence, &$dictBitraversable, &$dictApplicative, &$PS__Data_Bifunctor) {
                return function ($g)  use (&$bisequence, &$dictBitraversable, &$dictApplicative, &$PS__Data_Bifunctor, &$f) {
                    return function ($t)  use (&$bisequence, &$dictBitraversable, &$dictApplicative, &$PS__Data_Bifunctor, &$f, &$g) {
                        return $bisequence($dictBitraversable)($dictApplicative)($PS__Data_Bifunctor['bimap']($dictBitraversable['Bifunctor0']())($f)($g)($t));
                    };
                };
            };
        };
    };
    $bifor = function ($dictBitraversable)  use (&$bitraverse) {
        return function ($dictApplicative)  use (&$bitraverse, &$dictBitraversable) {
            return function ($t)  use (&$bitraverse, &$dictBitraversable, &$dictApplicative) {
                return function ($f)  use (&$bitraverse, &$dictBitraversable, &$dictApplicative, &$t) {
                    return function ($g)  use (&$bitraverse, &$dictBitraversable, &$dictApplicative, &$f, &$t) {
                        return $bitraverse($dictBitraversable)($dictApplicative)($f)($g)($t);
                    };
                };
            };
        };
    };
    return [
        'Bitraversable' => $Bitraversable,
        'bitraverse' => $bitraverse,
        'bisequence' => $bisequence,
        'bitraverseDefault' => $bitraverseDefault,
        'bisequenceDefault' => $bisequenceDefault,
        'ltraverse' => $ltraverse,
        'rtraverse' => $rtraverse,
        'bifor' => $bifor,
        'lfor' => $lfor,
        'rfor' => $rfor,
        'bitraversableClown' => $bitraversableClown,
        'bitraversableJoker' => $bitraversableJoker,
        'bitraversableFlip' => $bitraversableFlip,
        'bitraversableProduct' => $bitraversableProduct,
        'bitraversableWrap' => $bitraversableWrap
    ];
})();
