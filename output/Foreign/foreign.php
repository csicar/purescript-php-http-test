<?php

$exports['unsafeToForeign'] = function ($value) {
  return $value;
};

$exports['unsafeFromForeign'] = function ($value) {
  return $value;
};

$exports['typeOf'] = function ($value) {
  $type = gettype($value);
  $mapping = [
    "boolean" => "boolean",
    "integer" => "number",
    "double" => "number",
    "string" => "string",
    "array" => "object",
    "object" => "object",
    "resource" => "object",
    "resource (closed)" => "object",
    "NULL" => "undefined",
    "unknown type" => "undefined"
  ];
  return $mapping[$type];
};

$exports['tagOf'] = function ($value) use (&$exports) {
  if (is_array($value) && array_keys($value) !== range(0, count($value) - 1)) return "Array";
  return ucfirst($exports['typeOf']($value));
  // return Object.prototype.toString.call(value).slice(8, -1);
};

$exports['isNull'] = function ($value) {
  return is_null($value);
};

$exports['isUndefined'] = function ($value) {
  return is_null($value);
};

$exports['isArray'] = function($value) {
  return array_keys($value) !== range(0, count($value) - 1);
};
