<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Error.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Except/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Except.Trans/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Boolean/index.php');
require_once(dirname(__FILE__) . '/../Data.Either/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Identity/index.php');
require_once(dirname(__FILE__) . '/../Data.Int/index.php');
require_once(dirname(__FILE__) . '/../Data.List.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.String.CodeUnits/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Foreign = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Control_Applicative, &$PS__Control_Monad_Except_Trans, &$PS__Data_Identity, &$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Data_HeytingAlgebra, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Error_Class, &$PS__Data_List_NonEmpty, &$PS__Data_Function, &$PS__Data_Eq, &$PS__Data_Either, &$PS__Data_Int, &$PS__Control_Monad_Except, &$PS__Data_String_CodeUnits, &$PS__Data_Ord, &$PS__Data_Ordering) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $ForeignError = [
        'constructor' => function ($value0) {
            return [
                'type' => 'ForeignError',
                'value0' => $value0
            ];
        },
        'create' => function ($value0)  use (&$ForeignError) {
            return $ForeignError['constructor']($value0);
        }
    ];
    $TypeMismatch = [
        'constructor' => function ($value0, $value1) {
            return [
                'type' => 'TypeMismatch',
                'value0' => $value0,
                'value1' => $value1
            ];
        },
        'create' => function ($value0)  use (&$TypeMismatch) {
            return function ($value1)  use (&$TypeMismatch, &$value0) {
                return $TypeMismatch['constructor']($value0, $value1);
            };
        }
    ];
    $ErrorAtIndex = [
        'constructor' => function ($value0, $value1) {
            return [
                'type' => 'ErrorAtIndex',
                'value0' => $value0,
                'value1' => $value1
            ];
        },
        'create' => function ($value0)  use (&$ErrorAtIndex) {
            return function ($value1)  use (&$ErrorAtIndex, &$value0) {
                return $ErrorAtIndex['constructor']($value0, $value1);
            };
        }
    ];
    $ErrorAtProperty = [
        'constructor' => function ($value0, $value1) {
            return [
                'type' => 'ErrorAtProperty',
                'value0' => $value0,
                'value1' => $value1
            ];
        },
        'create' => function ($value0)  use (&$ErrorAtProperty) {
            return function ($value1)  use (&$ErrorAtProperty, &$value0) {
                return $ErrorAtProperty['constructor']($value0, $value1);
            };
        }
    ];
    $showForeignError = $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$showForeignError) {
        if ($v['type'] === 'ForeignError') {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(ForeignError ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Show['showString'])($v['value0']))(')'));
        };
        if ($v['type'] === 'ErrorAtIndex') {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(ErrorAtIndex ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Show['showInt'])($v['value0']))($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])(' ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($showForeignError)($v['value1']))(')'))));
        };
        if ($v['type'] === 'ErrorAtProperty') {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(ErrorAtProperty ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Show['showString'])($v['value0']))($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])(' ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($showForeignError)($v['value1']))(')'))));
        };
        if ($v['type'] === 'TypeMismatch') {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(TypeMismatch ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Show['showString'])($v['value0']))($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])(' ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Show['showString'])($v['value1']))(')'))));
        };
        throw new \Exception('Failed pattern match at Foreign line 63, column 1 - line 63, column 47: ' . var_dump([ $v['constructor']['name'] ]));
    });
    $renderForeignError = function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$renderForeignError) {
        if ($v['type'] === 'ForeignError') {
            return $v['value0'];
        };
        if ($v['type'] === 'ErrorAtIndex') {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('Error at array index ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Show['showInt'])($v['value0']))($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])(': ')($renderForeignError($v['value1']))));
        };
        if ($v['type'] === 'ErrorAtProperty') {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('Error at property ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Show['showString'])($v['value0']))($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])(': ')($renderForeignError($v['value1']))));
        };
        if ($v['type'] === 'TypeMismatch') {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('Type mismatch: expected ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($v['value0'])($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])(', found ')($v['value1'])));
        };
        throw new \Exception('Failed pattern match at Foreign line 72, column 1 - line 72, column 45: ' . var_dump([ $v['constructor']['name'] ]));
    };
    $readUndefined = function ($value)  use (&$__foreign, &$PS__Control_Applicative, &$PS__Control_Monad_Except_Trans, &$PS__Data_Identity, &$PS__Data_Maybe, &$PS__Data_Boolean) {
        if ($__foreign['isUndefined']($value)) {
            return $PS__Control_Applicative['pure']($PS__Control_Monad_Except_Trans['applicativeExceptT']($PS__Data_Identity['monadIdentity']))($PS__Data_Maybe['Nothing']());
        };
        if ($PS__Data_Boolean['otherwise']) {
            return $PS__Control_Applicative['pure']($PS__Control_Monad_Except_Trans['applicativeExceptT']($PS__Data_Identity['monadIdentity']))($PS__Data_Maybe['Just']['constructor']($value));
        };
        throw new \Exception('Failed pattern match at Foreign line 157, column 1 - line 157, column 46: ' . var_dump([ $value['constructor']['name'] ]));
    };
    $readNullOrUndefined = function ($value)  use (&$PS__Data_HeytingAlgebra, &$__foreign, &$PS__Control_Applicative, &$PS__Control_Monad_Except_Trans, &$PS__Data_Identity, &$PS__Data_Maybe, &$PS__Data_Boolean) {
        if ($PS__Data_HeytingAlgebra['disj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($__foreign['isNull']($value))($__foreign['isUndefined']($value))) {
            return $PS__Control_Applicative['pure']($PS__Control_Monad_Except_Trans['applicativeExceptT']($PS__Data_Identity['monadIdentity']))($PS__Data_Maybe['Nothing']());
        };
        if ($PS__Data_Boolean['otherwise']) {
            return $PS__Control_Applicative['pure']($PS__Control_Monad_Except_Trans['applicativeExceptT']($PS__Data_Identity['monadIdentity']))($PS__Data_Maybe['Just']['constructor']($value));
        };
        throw new \Exception('Failed pattern match at Foreign line 162, column 1 - line 162, column 52: ' . var_dump([ $value['constructor']['name'] ]));
    };
    $readNull = function ($value)  use (&$__foreign, &$PS__Control_Applicative, &$PS__Control_Monad_Except_Trans, &$PS__Data_Identity, &$PS__Data_Maybe, &$PS__Data_Boolean) {
        if ($__foreign['isNull']($value)) {
            return $PS__Control_Applicative['pure']($PS__Control_Monad_Except_Trans['applicativeExceptT']($PS__Data_Identity['monadIdentity']))($PS__Data_Maybe['Nothing']());
        };
        if ($PS__Data_Boolean['otherwise']) {
            return $PS__Control_Applicative['pure']($PS__Control_Monad_Except_Trans['applicativeExceptT']($PS__Data_Identity['monadIdentity']))($PS__Data_Maybe['Just']['constructor']($value));
        };
        throw new \Exception('Failed pattern match at Foreign line 152, column 1 - line 152, column 41: ' . var_dump([ $value['constructor']['name'] ]));
    };
    $fail = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Error_Class['throwError']($PS__Control_Monad_Except_Trans['monadThrowExceptT']($PS__Data_Identity['monadIdentity'])))($PS__Data_List_NonEmpty['singleton']);
    $readArray = function ($value)  use (&$__foreign, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Control_Monad_Except_Trans, &$PS__Data_Identity, &$PS__Data_Boolean, &$fail, &$TypeMismatch) {
        if ($__foreign['isArray']($value)) {
            return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($PS__Control_Monad_Except_Trans['applicativeExceptT']($PS__Data_Identity['monadIdentity'])))($__foreign['unsafeFromForeign']($value));
        };
        if ($PS__Data_Boolean['otherwise']) {
            return $PS__Data_Function['apply']($fail)($TypeMismatch['constructor']('array', $__foreign['tagOf']($value)));
        };
        throw new \Exception('Failed pattern match at Foreign line 147, column 1 - line 147, column 42: ' . var_dump([ $value['constructor']['name'] ]));
    };
    $unsafeReadTagged = function ($tag)  use (&$PS__Data_Eq, &$__foreign, &$PS__Control_Applicative, &$PS__Control_Monad_Except_Trans, &$PS__Data_Identity, &$PS__Data_Boolean, &$PS__Data_Function, &$fail, &$TypeMismatch) {
        return function ($value)  use (&$PS__Data_Eq, &$__foreign, &$tag, &$PS__Control_Applicative, &$PS__Control_Monad_Except_Trans, &$PS__Data_Identity, &$PS__Data_Boolean, &$PS__Data_Function, &$fail, &$TypeMismatch) {
            if ($PS__Data_Eq['eq']($PS__Data_Eq['eqString'])($__foreign['tagOf']($value))($tag)) {
                return $PS__Control_Applicative['pure']($PS__Control_Monad_Except_Trans['applicativeExceptT']($PS__Data_Identity['monadIdentity']))($__foreign['unsafeFromForeign']($value));
            };
            if ($PS__Data_Boolean['otherwise']) {
                return $PS__Data_Function['apply']($fail)($TypeMismatch['constructor']($tag, $__foreign['tagOf']($value)));
            };
            throw new \Exception('Failed pattern match at Foreign line 106, column 1 - line 106, column 55: ' . var_dump([ $tag['constructor']['name'], $value['constructor']['name'] ]));
        };
    };
    $readBoolean = $unsafeReadTagged('Boolean');
    $readNumber = $unsafeReadTagged('Number');
    $readInt = function ($value)  use (&$PS__Data_Function, &$PS__Data_Either, &$PS__Data_List_NonEmpty, &$TypeMismatch, &$__foreign, &$PS__Control_Semigroupoid, &$PS__Data_Maybe, &$PS__Control_Applicative, &$PS__Data_Int, &$PS__Control_Monad_Except, &$readNumber) {
        $error = $PS__Data_Function['apply']($PS__Data_Either['Left']['create'])($PS__Data_Function['apply']($PS__Data_List_NonEmpty['singleton'])($TypeMismatch['constructor']('Int', $__foreign['tagOf']($value))));
        $fromNumber = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['maybe']($error)($PS__Control_Applicative['pure']($PS__Data_Either['applicativeEither'])))($PS__Data_Int['fromNumber']);
        return $PS__Control_Monad_Except['mapExcept']($PS__Data_Either['either']($PS__Data_Function['const']($error))($fromNumber))($readNumber($value));
    };
    $readString = $unsafeReadTagged('String');
    $readChar = function ($value)  use (&$PS__Data_Function, &$PS__Data_Either, &$PS__Data_List_NonEmpty, &$TypeMismatch, &$__foreign, &$PS__Control_Semigroupoid, &$PS__Data_Maybe, &$PS__Control_Applicative, &$PS__Data_String_CodeUnits, &$PS__Control_Monad_Except, &$readString) {
        $error = $PS__Data_Function['apply']($PS__Data_Either['Left']['create'])($PS__Data_Function['apply']($PS__Data_List_NonEmpty['singleton'])($TypeMismatch['constructor']('Char', $__foreign['tagOf']($value))));
        $fromString = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['maybe']($error)($PS__Control_Applicative['pure']($PS__Data_Either['applicativeEither'])))($PS__Data_String_CodeUnits['toChar']);
        return $PS__Control_Monad_Except['mapExcept']($PS__Data_Either['either']($PS__Data_Function['const']($error))($fromString))($readString($value));
    };
    $eqForeignError = $PS__Data_Eq['Eq'](function ($x)  use (&$PS__Data_Eq, &$PS__Data_HeytingAlgebra, &$eqForeignError) {
        return function ($y)  use (&$x, &$PS__Data_Eq, &$PS__Data_HeytingAlgebra, &$eqForeignError) {
            if ($x['type'] === 'ForeignError' && $y['type'] === 'ForeignError') {
                return $PS__Data_Eq['eq']($PS__Data_Eq['eqString'])($x['value0'])($y['value0']);
            };
            if ($x['type'] === 'TypeMismatch' && $y['type'] === 'TypeMismatch') {
                return $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Eq['eq']($PS__Data_Eq['eqString'])($x['value0'])($y['value0']))($PS__Data_Eq['eq']($PS__Data_Eq['eqString'])($x['value1'])($y['value1']));
            };
            if ($x['type'] === 'ErrorAtIndex' && $y['type'] === 'ErrorAtIndex') {
                return $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($x['value0'])($y['value0']))($PS__Data_Eq['eq']($eqForeignError)($x['value1'])($y['value1']));
            };
            if ($x['type'] === 'ErrorAtProperty' && $y['type'] === 'ErrorAtProperty') {
                return $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Eq['eq']($PS__Data_Eq['eqString'])($x['value0'])($y['value0']))($PS__Data_Eq['eq']($eqForeignError)($x['value1'])($y['value1']));
            };
            return false;
        };
    });
    $ordForeignError = $PS__Data_Ord['Ord'](function ()  use (&$eqForeignError) {
        return $eqForeignError;
    }, function ($x)  use (&$PS__Data_Ord, &$PS__Data_Ordering, &$ordForeignError) {
        return function ($y)  use (&$x, &$PS__Data_Ord, &$PS__Data_Ordering, &$ordForeignError) {
            if ($x['type'] === 'ForeignError' && $y['type'] === 'ForeignError') {
                return $PS__Data_Ord['compare']($PS__Data_Ord['ordString'])($x['value0'])($y['value0']);
            };
            if ($x['type'] === 'ForeignError') {
                return $PS__Data_Ordering['LT']();
            };
            if ($y['type'] === 'ForeignError') {
                return $PS__Data_Ordering['GT']();
            };
            if ($x['type'] === 'TypeMismatch' && $y['type'] === 'TypeMismatch') {
                $v = $PS__Data_Ord['compare']($PS__Data_Ord['ordString'])($x['value0'])($y['value0']);
                if ($v['type'] === 'LT') {
                    return $PS__Data_Ordering['LT']();
                };
                if ($v['type'] === 'GT') {
                    return $PS__Data_Ordering['GT']();
                };
                return $PS__Data_Ord['compare']($PS__Data_Ord['ordString'])($x['value1'])($y['value1']);
            };
            if ($x['type'] === 'TypeMismatch') {
                return $PS__Data_Ordering['LT']();
            };
            if ($y['type'] === 'TypeMismatch') {
                return $PS__Data_Ordering['GT']();
            };
            if ($x['type'] === 'ErrorAtIndex' && $y['type'] === 'ErrorAtIndex') {
                $v = $PS__Data_Ord['compare']($PS__Data_Ord['ordInt'])($x['value0'])($y['value0']);
                if ($v['type'] === 'LT') {
                    return $PS__Data_Ordering['LT']();
                };
                if ($v['type'] === 'GT') {
                    return $PS__Data_Ordering['GT']();
                };
                return $PS__Data_Ord['compare']($ordForeignError)($x['value1'])($y['value1']);
            };
            if ($x['type'] === 'ErrorAtIndex') {
                return $PS__Data_Ordering['LT']();
            };
            if ($y['type'] === 'ErrorAtIndex') {
                return $PS__Data_Ordering['GT']();
            };
            if ($x['type'] === 'ErrorAtProperty' && $y['type'] === 'ErrorAtProperty') {
                $v = $PS__Data_Ord['compare']($PS__Data_Ord['ordString'])($x['value0'])($y['value0']);
                if ($v['type'] === 'LT') {
                    return $PS__Data_Ordering['LT']();
                };
                if ($v['type'] === 'GT') {
                    return $PS__Data_Ordering['GT']();
                };
                return $PS__Data_Ord['compare']($ordForeignError)($x['value1'])($y['value1']);
            };
            throw new \Exception('Failed pattern match at Foreign line 61, column 8 - line 61, column 52: ' . var_dump([ $x['constructor']['name'], $y['constructor']['name'] ]));
        };
    });
    return [
        'ForeignError' => $ForeignError,
        'TypeMismatch' => $TypeMismatch,
        'ErrorAtIndex' => $ErrorAtIndex,
        'ErrorAtProperty' => $ErrorAtProperty,
        'renderForeignError' => $renderForeignError,
        'unsafeReadTagged' => $unsafeReadTagged,
        'readString' => $readString,
        'readChar' => $readChar,
        'readBoolean' => $readBoolean,
        'readNumber' => $readNumber,
        'readInt' => $readInt,
        'readArray' => $readArray,
        'readNull' => $readNull,
        'readUndefined' => $readUndefined,
        'readNullOrUndefined' => $readNullOrUndefined,
        'fail' => $fail,
        'eqForeignError' => $eqForeignError,
        'ordForeignError' => $ordForeignError,
        'showForeignError' => $showForeignError,
        'unsafeToForeign' => $__foreign['unsafeToForeign'],
        'unsafeFromForeign' => $__foreign['unsafeFromForeign'],
        'typeOf' => $__foreign['typeOf'],
        'tagOf' => $__foreign['tagOf'],
        'isNull' => $__foreign['isNull'],
        'isUndefined' => $__foreign['isUndefined'],
        'isArray' => $__foreign['isArray']
    ];
})();
