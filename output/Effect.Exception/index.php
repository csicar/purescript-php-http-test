<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Either/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Effect/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Effect_Exception = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Control_Applicative, &$PS__Effect, &$PS__Data_Either, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Show) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $__try = function ($action)  use (&$__foreign, &$PS__Control_Semigroupoid, &$PS__Control_Applicative, &$PS__Effect, &$PS__Data_Either, &$PS__Data_Functor) {
        return $__foreign['catchException']($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Applicative['pure']($PS__Effect['applicativeEffect']))($PS__Data_Either['Left']['create']))($PS__Data_Functor['map']($PS__Effect['functorEffect'])($PS__Data_Either['Right']['create'])($action));
    };
    $__throw = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($__foreign['throwException'])($__foreign['error']);
    $stack = $__foreign['stackImpl']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $showError = $PS__Data_Show['Show']($__foreign['showErrorImpl']);
    return [
        'stack' => $stack,
        'throw' => $__throw,
        'try' => $__try,
        'showError' => $showError,
        'error' => $__foreign['error'],
        'message' => $__foreign['message'],
        'name' => $__foreign['name'],
        'throwException' => $__foreign['throwException'],
        'catchException' => $__foreign['catchException']
    ];
})();
