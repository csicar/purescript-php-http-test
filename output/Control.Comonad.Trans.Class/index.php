<?php
require_once(dirname(__FILE__) . '/../Control.Comonad/index.php');
$PS__Control_Comonad_Trans_Class = (function () {
    $ComonadTrans = function ($lower) {
        return [
            'lower' => $lower
        ];
    };
    $lower = function ($dict) {
        return $dict['lower'];
    };
    return [
        'lower' => $lower,
        'ComonadTrans' => $ComonadTrans
    ];
})();
