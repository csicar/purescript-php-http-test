<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
$PS__Control_Bind = (function ()  use (&$PS__Control_Apply, &$PS__Data_Function, &$PS__Control_Category) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Bind = function ($Apply0, $bind) {
        return [
            'Apply0' => $Apply0,
            'bind' => $bind
        ];
    };
    $Discard = function ($discard) {
        return [
            'discard' => $discard
        ];
    };
    $discard = function ($dict) {
        return $dict['discard'];
    };
    $bindFn = $Bind(function ()  use (&$PS__Control_Apply) {
        return $PS__Control_Apply['applyFn'];
    }, function ($m) {
        return function ($f)  use (&$m) {
            return function ($x)  use (&$f, &$m) {
                return $f($m($x))($x);
            };
        };
    });
    $bindArray = $Bind(function ()  use (&$PS__Control_Apply) {
        return $PS__Control_Apply['applyArray'];
    }, $__foreign['arrayBind']);
    $bind = function ($dict) {
        return $dict['bind'];
    };
    $bindFlipped = function ($dictBind)  use (&$PS__Data_Function, &$bind) {
        return $PS__Data_Function['flip']($bind($dictBind));
    };
    $composeKleisliFlipped = function ($dictBind)  use (&$bindFlipped) {
        return function ($f)  use (&$bindFlipped, &$dictBind) {
            return function ($g)  use (&$bindFlipped, &$dictBind, &$f) {
                return function ($a)  use (&$bindFlipped, &$dictBind, &$f, &$g) {
                    return $bindFlipped($dictBind)($f)($g($a));
                };
            };
        };
    };
    $composeKleisli = function ($dictBind)  use (&$bind) {
        return function ($f)  use (&$bind, &$dictBind) {
            return function ($g)  use (&$bind, &$dictBind, &$f) {
                return function ($a)  use (&$bind, &$dictBind, &$f, &$g) {
                    return $bind($dictBind)($f($a))($g);
                };
            };
        };
    };
    $discardUnit = $Discard(function ($dictBind)  use (&$bind) {
        return $bind($dictBind);
    });
    $ifM = function ($dictBind)  use (&$bind) {
        return function ($cond)  use (&$bind, &$dictBind) {
            return function ($t)  use (&$bind, &$dictBind, &$cond) {
                return function ($f)  use (&$bind, &$dictBind, &$cond, &$t) {
                    return $bind($dictBind)($cond)(function ($cond__prime)  use (&$t, &$f) {
                        if ($cond__prime) {
                            return $t;
                        };
                        return $f;
                    });
                };
            };
        };
    };
    $join = function ($dictBind)  use (&$bind, &$PS__Control_Category) {
        return function ($m)  use (&$bind, &$dictBind, &$PS__Control_Category) {
            return $bind($dictBind)($m)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        };
    };
    return [
        'Bind' => $Bind,
        'bind' => $bind,
        'bindFlipped' => $bindFlipped,
        'Discard' => $Discard,
        'discard' => $discard,
        'join' => $join,
        'composeKleisli' => $composeKleisli,
        'composeKleisliFlipped' => $composeKleisliFlipped,
        'ifM' => $ifM,
        'bindFn' => $bindFn,
        'bindArray' => $bindArray,
        'discardUnit' => $discardUnit
    ];
})();
