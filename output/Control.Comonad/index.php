<?php
require_once(dirname(__FILE__) . '/../Control.Extend/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
$PS__Control_Comonad = (function () {
    $Comonad = function ($Extend0, $extract) {
        return [
            'Extend0' => $Extend0,
            'extract' => $extract
        ];
    };
    $extract = function ($dict) {
        return $dict['extract'];
    };
    return [
        'Comonad' => $Comonad,
        'extract' => $extract
    ];
})();
