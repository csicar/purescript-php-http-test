<?php
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Symbol/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
$PS__Data_BooleanAlgebra = (function ()  use (&$PS__Data_HeytingAlgebra) {
    $BooleanAlgebra = function ($HeytingAlgebra0) {
        return [
            'HeytingAlgebra0' => $HeytingAlgebra0
        ];
    };
    $BooleanAlgebraRecord = function ($HeytingAlgebraRecord0) {
        return [
            'HeytingAlgebraRecord0' => $HeytingAlgebraRecord0
        ];
    };
    $booleanAlgebraUnit = $BooleanAlgebra(function ()  use (&$PS__Data_HeytingAlgebra) {
        return $PS__Data_HeytingAlgebra['heytingAlgebraUnit'];
    });
    $booleanAlgebraRecordNil = $BooleanAlgebraRecord(function ()  use (&$PS__Data_HeytingAlgebra) {
        return $PS__Data_HeytingAlgebra['heytingAlgebraRecordNil'];
    });
    $booleanAlgebraRecordCons = function ($dictIsSymbol)  use (&$BooleanAlgebraRecord, &$PS__Data_HeytingAlgebra) {
        return function ($dictCons)  use (&$BooleanAlgebraRecord, &$PS__Data_HeytingAlgebra, &$dictIsSymbol) {
            return function ($dictBooleanAlgebraRecord)  use (&$BooleanAlgebraRecord, &$PS__Data_HeytingAlgebra, &$dictIsSymbol, &$dictCons) {
                return function ($dictBooleanAlgebra)  use (&$BooleanAlgebraRecord, &$PS__Data_HeytingAlgebra, &$dictIsSymbol, &$dictCons, &$dictBooleanAlgebraRecord) {
                    return $BooleanAlgebraRecord(function ()  use (&$PS__Data_HeytingAlgebra, &$dictIsSymbol, &$dictCons, &$dictBooleanAlgebraRecord, &$dictBooleanAlgebra) {
                        return $PS__Data_HeytingAlgebra['heytingAlgebraRecordCons']($dictIsSymbol)($dictCons)($dictBooleanAlgebraRecord['HeytingAlgebraRecord0']())($dictBooleanAlgebra['HeytingAlgebra0']());
                    });
                };
            };
        };
    };
    $booleanAlgebraRecord = function ($dictRowToList)  use (&$BooleanAlgebra, &$PS__Data_HeytingAlgebra) {
        return function ($dictBooleanAlgebraRecord)  use (&$BooleanAlgebra, &$PS__Data_HeytingAlgebra, &$dictRowToList) {
            return $BooleanAlgebra(function ()  use (&$PS__Data_HeytingAlgebra, &$dictRowToList, &$dictBooleanAlgebraRecord) {
                return $PS__Data_HeytingAlgebra['heytingAlgebraRecord']($dictRowToList)($dictBooleanAlgebraRecord['HeytingAlgebraRecord0']());
            });
        };
    };
    $booleanAlgebraFn = function ($dictBooleanAlgebra)  use (&$BooleanAlgebra, &$PS__Data_HeytingAlgebra) {
        return $BooleanAlgebra(function ()  use (&$PS__Data_HeytingAlgebra, &$dictBooleanAlgebra) {
            return $PS__Data_HeytingAlgebra['heytingAlgebraFunction']($dictBooleanAlgebra['HeytingAlgebra0']());
        });
    };
    $booleanAlgebraBoolean = $BooleanAlgebra(function ()  use (&$PS__Data_HeytingAlgebra) {
        return $PS__Data_HeytingAlgebra['heytingAlgebraBoolean'];
    });
    return [
        'BooleanAlgebra' => $BooleanAlgebra,
        'BooleanAlgebraRecord' => $BooleanAlgebraRecord,
        'booleanAlgebraBoolean' => $booleanAlgebraBoolean,
        'booleanAlgebraUnit' => $booleanAlgebraUnit,
        'booleanAlgebraFn' => $booleanAlgebraFn,
        'booleanAlgebraRecord' => $booleanAlgebraRecord,
        'booleanAlgebraRecordNil' => $booleanAlgebraRecordNil,
        'booleanAlgebraRecordCons' => $booleanAlgebraRecordCons
    ];
})();
