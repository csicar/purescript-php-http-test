<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
$PS__Control_Extend = (function ()  use (&$PS__Data_Functor, &$PS__Data_Semigroup, &$PS__Control_Category) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Extend = function ($Functor0, $extend) {
        return [
            'Functor0' => $Functor0,
            'extend' => $extend
        ];
    };
    $extendFn = function ($dictSemigroup)  use (&$Extend, &$PS__Data_Functor, &$PS__Data_Semigroup) {
        return $Extend(function ()  use (&$PS__Data_Functor) {
            return $PS__Data_Functor['functorFn'];
        }, function ($f)  use (&$PS__Data_Semigroup, &$dictSemigroup) {
            return function ($g)  use (&$f, &$PS__Data_Semigroup, &$dictSemigroup) {
                return function ($w)  use (&$f, &$g, &$PS__Data_Semigroup, &$dictSemigroup) {
                    return $f(function ($w__prime)  use (&$g, &$PS__Data_Semigroup, &$dictSemigroup, &$w) {
                        return $g($PS__Data_Semigroup['append']($dictSemigroup)($w)($w__prime));
                    });
                };
            };
        });
    };
    $extendArray = $Extend(function ()  use (&$PS__Data_Functor) {
        return $PS__Data_Functor['functorArray'];
    }, $__foreign['arrayExtend']);
    $extend = function ($dict) {
        return $dict['extend'];
    };
    $extendFlipped = function ($dictExtend)  use (&$extend) {
        return function ($w)  use (&$extend, &$dictExtend) {
            return function ($f)  use (&$extend, &$dictExtend, &$w) {
                return $extend($dictExtend)($f)($w);
            };
        };
    };
    $duplicate = function ($dictExtend)  use (&$extend, &$PS__Control_Category) {
        return $extend($dictExtend)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    };
    $composeCoKleisliFlipped = function ($dictExtend)  use (&$extend) {
        return function ($f)  use (&$extend, &$dictExtend) {
            return function ($g)  use (&$f, &$extend, &$dictExtend) {
                return function ($w)  use (&$f, &$extend, &$dictExtend, &$g) {
                    return $f($extend($dictExtend)($g)($w));
                };
            };
        };
    };
    $composeCoKleisli = function ($dictExtend)  use (&$extend) {
        return function ($f)  use (&$extend, &$dictExtend) {
            return function ($g)  use (&$extend, &$dictExtend, &$f) {
                return function ($w)  use (&$g, &$extend, &$dictExtend, &$f) {
                    return $g($extend($dictExtend)($f)($w));
                };
            };
        };
    };
    return [
        'Extend' => $Extend,
        'extend' => $extend,
        'extendFlipped' => $extendFlipped,
        'composeCoKleisli' => $composeCoKleisli,
        'composeCoKleisliFlipped' => $composeCoKleisliFlipped,
        'duplicate' => $duplicate,
        'extendFn' => $extendFn,
        'extendArray' => $extendArray
    ];
})();
