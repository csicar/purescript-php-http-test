<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Cont.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Error.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Reader.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.State.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Trans.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Writer.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadPlus/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadZero/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Effect.Class/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_State_Trans = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$PS__Control_Monad_Trans_Class, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple, &$PS__Control_Lazy, &$PS__Data_Unit, &$PS__Data_Functor, &$PS__Control_Monad, &$PS__Control_Apply, &$PS__Control_Monad_Reader_Class, &$PS__Control_Monad_Cont_Class, &$PS__Effect_Class, &$PS__Control_Monad_Rec_Class, &$PS__Control_Monad_State_Class, &$PS__Control_Monad_Writer_Class, &$PS__Control_Monad_Error_Class, &$PS__Control_Alt, &$PS__Control_Plus, &$PS__Control_Alternative, &$PS__Control_MonadZero, &$PS__Control_MonadPlus) {
    $StateT = function ($x) {
        return $x;
    };
    $withStateT = function ($f)  use (&$PS__Control_Semigroupoid) {
        return function ($v)  use (&$PS__Control_Semigroupoid, &$f) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($v)($f);
        };
    };
    $runStateT = function ($v) {
        return $v;
    };
    $newtypeStateT = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $StateT);
    $monadTransStateT = $PS__Control_Monad_Trans_Class['MonadTrans'](function ($dictMonad)  use (&$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
        return function ($m)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
            return function ($s)  use (&$PS__Control_Bind, &$dictMonad, &$m, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
                return $PS__Control_Bind['bind']($dictMonad['Bind1']())($m)(function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_Tuple, &$s) {
                    return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($PS__Data_Tuple['Tuple']['constructor']($v, $s));
                });
            };
        };
    });
    $mapStateT = function ($f)  use (&$PS__Control_Semigroupoid) {
        return function ($v)  use (&$PS__Control_Semigroupoid, &$f) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($v);
        };
    };
    $lazyStateT = $PS__Control_Lazy['Lazy'](function ($f)  use (&$PS__Data_Unit) {
        return function ($s)  use (&$f, &$PS__Data_Unit) {
            $v = $f($PS__Data_Unit['unit']);
            return $v($s);
        };
    });
    $functorStateT = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Tuple) {
        return $PS__Data_Functor['Functor'](function ($f)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Tuple) {
            return function ($v)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Tuple, &$f) {
                return function ($s)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Tuple, &$f, &$v) {
                    return $PS__Data_Functor['map']($dictFunctor)(function ($v1)  use (&$PS__Data_Tuple, &$f) {
                        return $PS__Data_Tuple['Tuple']['constructor']($f($v1['value0']), $v1['value1']);
                    })($v($s));
                };
            };
        });
    };
    $execStateT = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Tuple) {
            return function ($s)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Tuple, &$v) {
                return $PS__Data_Functor['map']($dictFunctor)($PS__Data_Tuple['snd'])($v($s));
            };
        };
    };
    $evalStateT = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Tuple) {
            return function ($s)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Tuple, &$v) {
                return $PS__Data_Functor['map']($dictFunctor)($PS__Data_Tuple['fst'])($v($s));
            };
        };
    };
    $monadStateT = function ($dictMonad)  use (&$PS__Control_Monad, &$applicativeStateT, &$bindStateT) {
        return $PS__Control_Monad['Monad'](function ()  use (&$applicativeStateT, &$dictMonad) {
            return $applicativeStateT($dictMonad);
        }, function ()  use (&$bindStateT, &$dictMonad) {
            return $bindStateT($dictMonad);
        });
    };
    $bindStateT = function ($dictMonad)  use (&$PS__Control_Bind, &$applyStateT) {
        return $PS__Control_Bind['Bind'](function ()  use (&$applyStateT, &$dictMonad) {
            return $applyStateT($dictMonad);
        }, function ($v)  use (&$PS__Control_Bind, &$dictMonad) {
            return function ($f)  use (&$PS__Control_Bind, &$dictMonad, &$v) {
                return function ($s)  use (&$PS__Control_Bind, &$dictMonad, &$v, &$f) {
                    return $PS__Control_Bind['bind']($dictMonad['Bind1']())($v($s))(function ($v1)  use (&$f) {
                        $v3 = $f($v1['value0']);
                        return $v3($v1['value1']);
                    });
                };
            };
        });
    };
    $applyStateT = function ($dictMonad)  use (&$PS__Control_Apply, &$functorStateT, &$PS__Control_Monad, &$monadStateT) {
        return $PS__Control_Apply['Apply'](function ()  use (&$functorStateT, &$dictMonad) {
            return $functorStateT((($dictMonad['Bind1']())['Apply0']())['Functor0']());
        }, $PS__Control_Monad['ap']($monadStateT($dictMonad)));
    };
    $applicativeStateT = function ($dictMonad)  use (&$PS__Control_Applicative, &$applyStateT, &$PS__Data_Function, &$PS__Data_Tuple) {
        return $PS__Control_Applicative['Applicative'](function ()  use (&$applyStateT, &$dictMonad) {
            return $applyStateT($dictMonad);
        }, function ($a)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_Tuple) {
            return function ($s)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_Tuple, &$a) {
                return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($PS__Data_Tuple['Tuple']['constructor']($a, $s));
            };
        });
    };
    $monadAskStateT = function ($dictMonadAsk)  use (&$PS__Control_Monad_Reader_Class, &$monadStateT, &$PS__Control_Monad_Trans_Class, &$monadTransStateT) {
        return $PS__Control_Monad_Reader_Class['MonadAsk'](function ()  use (&$monadStateT, &$dictMonadAsk) {
            return $monadStateT($dictMonadAsk['Monad0']());
        }, $PS__Control_Monad_Trans_Class['lift']($monadTransStateT)($dictMonadAsk['Monad0']())($PS__Control_Monad_Reader_Class['ask']($dictMonadAsk)));
    };
    $monadReaderStateT = function ($dictMonadReader)  use (&$PS__Control_Monad_Reader_Class, &$monadAskStateT, &$PS__Control_Semigroupoid, &$mapStateT) {
        return $PS__Control_Monad_Reader_Class['MonadReader'](function ()  use (&$monadAskStateT, &$dictMonadReader) {
            return $monadAskStateT($dictMonadReader['MonadAsk0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($mapStateT)($PS__Control_Monad_Reader_Class['local']($dictMonadReader)));
    };
    $monadContStateT = function ($dictMonadCont)  use (&$PS__Control_Monad_Cont_Class, &$monadStateT, &$PS__Data_Tuple) {
        return $PS__Control_Monad_Cont_Class['MonadCont'](function ()  use (&$monadStateT, &$dictMonadCont) {
            return $monadStateT($dictMonadCont['Monad0']());
        }, function ($f)  use (&$PS__Control_Monad_Cont_Class, &$dictMonadCont, &$PS__Data_Tuple) {
            return function ($s)  use (&$PS__Control_Monad_Cont_Class, &$dictMonadCont, &$f, &$PS__Data_Tuple) {
                return $PS__Control_Monad_Cont_Class['callCC']($dictMonadCont)(function ($c)  use (&$f, &$PS__Data_Tuple, &$s) {
                    $v = $f(function ($a)  use (&$c, &$PS__Data_Tuple) {
                        return function ($s__prime)  use (&$c, &$PS__Data_Tuple, &$a) {
                            return $c($PS__Data_Tuple['Tuple']['constructor']($a, $s__prime));
                        };
                    });
                    return $v($s);
                });
            };
        });
    };
    $monadEffectState = function ($dictMonadEffect)  use (&$PS__Effect_Class, &$monadStateT, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransStateT) {
        return $PS__Effect_Class['MonadEffect'](function ()  use (&$monadStateT, &$dictMonadEffect) {
            return $monadStateT($dictMonadEffect['Monad0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Trans_Class['lift']($monadTransStateT)($dictMonadEffect['Monad0']()))($PS__Effect_Class['liftEffect']($dictMonadEffect)));
    };
    $monadRecStateT = function ($dictMonadRec)  use (&$PS__Control_Monad_Rec_Class, &$monadStateT, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Data_Tuple) {
        return $PS__Control_Monad_Rec_Class['MonadRec'](function ()  use (&$monadStateT, &$dictMonadRec) {
            return $monadStateT($dictMonadRec['Monad0']());
        }, function ($f)  use (&$PS__Control_Bind, &$dictMonadRec, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$PS__Data_Tuple) {
            return function ($a)  use (&$PS__Control_Bind, &$dictMonadRec, &$f, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$PS__Data_Tuple) {
                $f__prime = function ($v)  use (&$PS__Control_Bind, &$dictMonadRec, &$f, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$PS__Data_Tuple) {
                    return $PS__Control_Bind['bind'](($dictMonadRec['Monad0']())['Bind1']())((function ()  use (&$f, &$v) {
                        $v1 = $f($v['value0']);
                        return $v1;
                    })()($v['value1']))(function ($v1)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$PS__Data_Tuple) {
                        return $PS__Control_Applicative['pure'](($dictMonadRec['Monad0']())['Applicative0']())((function ()  use (&$v1, &$PS__Control_Monad_Rec_Class, &$PS__Data_Tuple) {
                            if ($v1['value0']['type'] === 'Loop') {
                                return $PS__Control_Monad_Rec_Class['Loop']['constructor']($PS__Data_Tuple['Tuple']['constructor']($v1['value0']['value0'], $v1['value1']));
                            };
                            if ($v1['value0']['type'] === 'Done') {
                                return $PS__Control_Monad_Rec_Class['Done']['constructor']($PS__Data_Tuple['Tuple']['constructor']($v1['value0']['value0'], $v1['value1']));
                            };
                            throw new \Exception('Failed pattern match at Control.Monad.State.Trans line 87, column 16 - line 89, column 40: ' . var_dump([ $v1['value0']['constructor']['name'] ]));
                        })());
                    });
                };
                return function ($s)  use (&$PS__Control_Monad_Rec_Class, &$dictMonadRec, &$f__prime, &$PS__Data_Tuple, &$a) {
                    return $PS__Control_Monad_Rec_Class['tailRecM']($dictMonadRec)($f__prime)($PS__Data_Tuple['Tuple']['constructor']($a, $s));
                };
            };
        });
    };
    $monadStateStateT = function ($dictMonad)  use (&$PS__Control_Monad_State_Class, &$monadStateT, &$PS__Data_Function, &$StateT, &$PS__Control_Semigroupoid, &$PS__Control_Applicative) {
        return $PS__Control_Monad_State_Class['MonadState'](function ()  use (&$monadStateT, &$dictMonad) {
            return $monadStateT($dictMonad);
        }, function ($f)  use (&$PS__Data_Function, &$StateT, &$PS__Control_Semigroupoid, &$PS__Control_Applicative, &$dictMonad) {
            return $PS__Data_Function['apply']($StateT)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($f));
        });
    };
    $monadTellStateT = function ($dictMonadTell)  use (&$PS__Control_Monad_Writer_Class, &$monadStateT, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransStateT) {
        return $PS__Control_Monad_Writer_Class['MonadTell'](function ()  use (&$monadStateT, &$dictMonadTell) {
            return $monadStateT($dictMonadTell['Monad0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Trans_Class['lift']($monadTransStateT)($dictMonadTell['Monad0']()))($PS__Control_Monad_Writer_Class['tell']($dictMonadTell)));
    };
    $monadWriterStateT = function ($dictMonadWriter)  use (&$PS__Control_Monad_Writer_Class, &$monadTellStateT, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
        return $PS__Control_Monad_Writer_Class['MonadWriter'](function ()  use (&$monadTellStateT, &$dictMonadWriter) {
            return $monadTellStateT($dictMonadWriter['MonadTell0']());
        }, function ($m)  use (&$PS__Control_Bind, &$dictMonadWriter, &$PS__Control_Monad_Writer_Class, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
            return function ($s)  use (&$PS__Control_Bind, &$dictMonadWriter, &$PS__Control_Monad_Writer_Class, &$m, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
                return $PS__Control_Bind['bind']((($dictMonadWriter['MonadTell0']())['Monad0']())['Bind1']())($PS__Control_Monad_Writer_Class['listen']($dictMonadWriter)($m($s)))(function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonadWriter, &$PS__Data_Tuple) {
                    return $PS__Data_Function['apply']($PS__Control_Applicative['pure']((($dictMonadWriter['MonadTell0']())['Monad0']())['Applicative0']()))($PS__Data_Tuple['Tuple']['constructor']($PS__Data_Tuple['Tuple']['constructor']($v['value0']['value0'], $v['value1']), $v['value0']['value1']));
                });
            };
        }, function ($m)  use (&$PS__Control_Monad_Writer_Class, &$dictMonadWriter, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
            return function ($s)  use (&$PS__Control_Monad_Writer_Class, &$dictMonadWriter, &$PS__Control_Bind, &$m, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
                return $PS__Control_Monad_Writer_Class['pass']($dictMonadWriter)($PS__Control_Bind['bind']((($dictMonadWriter['MonadTell0']())['Monad0']())['Bind1']())($m($s))(function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonadWriter, &$PS__Data_Tuple) {
                    return $PS__Data_Function['apply']($PS__Control_Applicative['pure']((($dictMonadWriter['MonadTell0']())['Monad0']())['Applicative0']()))($PS__Data_Tuple['Tuple']['constructor']($PS__Data_Tuple['Tuple']['constructor']($v['value0']['value0'], $v['value1']), $v['value0']['value1']));
                }));
            };
        });
    };
    $monadThrowStateT = function ($dictMonadThrow)  use (&$PS__Control_Monad_Error_Class, &$monadStateT, &$PS__Control_Monad_Trans_Class, &$monadTransStateT) {
        return $PS__Control_Monad_Error_Class['MonadThrow'](function ()  use (&$monadStateT, &$dictMonadThrow) {
            return $monadStateT($dictMonadThrow['Monad0']());
        }, function ($e)  use (&$PS__Control_Monad_Trans_Class, &$monadTransStateT, &$dictMonadThrow, &$PS__Control_Monad_Error_Class) {
            return $PS__Control_Monad_Trans_Class['lift']($monadTransStateT)($dictMonadThrow['Monad0']())($PS__Control_Monad_Error_Class['throwError']($dictMonadThrow)($e));
        });
    };
    $monadErrorStateT = function ($dictMonadError)  use (&$PS__Control_Monad_Error_Class, &$monadThrowStateT) {
        return $PS__Control_Monad_Error_Class['MonadError'](function ()  use (&$monadThrowStateT, &$dictMonadError) {
            return $monadThrowStateT($dictMonadError['MonadThrow0']());
        }, function ($v)  use (&$PS__Control_Monad_Error_Class, &$dictMonadError) {
            return function ($h)  use (&$PS__Control_Monad_Error_Class, &$dictMonadError, &$v) {
                return function ($s)  use (&$PS__Control_Monad_Error_Class, &$dictMonadError, &$v, &$h) {
                    return $PS__Control_Monad_Error_Class['catchError']($dictMonadError)($v($s))(function ($e)  use (&$h, &$s) {
                        $v1 = $h($e);
                        return $v1($s);
                    });
                };
            };
        });
    };
    $altStateT = function ($dictMonad)  use (&$PS__Control_Alt, &$functorStateT) {
        return function ($dictAlt)  use (&$PS__Control_Alt, &$functorStateT) {
            return $PS__Control_Alt['Alt'](function ()  use (&$functorStateT, &$dictAlt) {
                return $functorStateT($dictAlt['Functor0']());
            }, function ($v)  use (&$PS__Control_Alt, &$dictAlt) {
                return function ($v1)  use (&$PS__Control_Alt, &$dictAlt, &$v) {
                    return function ($s)  use (&$PS__Control_Alt, &$dictAlt, &$v, &$v1) {
                        return $PS__Control_Alt['alt']($dictAlt)($v($s))($v1($s));
                    };
                };
            });
        };
    };
    $plusStateT = function ($dictMonad)  use (&$PS__Control_Plus, &$altStateT) {
        return function ($dictPlus)  use (&$PS__Control_Plus, &$altStateT, &$dictMonad) {
            return $PS__Control_Plus['Plus'](function ()  use (&$altStateT, &$dictMonad, &$dictPlus) {
                return $altStateT($dictMonad)($dictPlus['Alt0']());
            }, function ($v)  use (&$PS__Control_Plus, &$dictPlus) {
                return $PS__Control_Plus['empty']($dictPlus);
            });
        };
    };
    $alternativeStateT = function ($dictMonad)  use (&$PS__Control_Alternative, &$applicativeStateT, &$plusStateT) {
        return function ($dictAlternative)  use (&$PS__Control_Alternative, &$applicativeStateT, &$dictMonad, &$plusStateT) {
            return $PS__Control_Alternative['Alternative'](function ()  use (&$applicativeStateT, &$dictMonad) {
                return $applicativeStateT($dictMonad);
            }, function ()  use (&$plusStateT, &$dictMonad, &$dictAlternative) {
                return $plusStateT($dictMonad)($dictAlternative['Plus1']());
            });
        };
    };
    $monadZeroStateT = function ($dictMonadZero)  use (&$PS__Control_MonadZero, &$alternativeStateT, &$monadStateT) {
        return $PS__Control_MonadZero['MonadZero'](function ()  use (&$alternativeStateT, &$dictMonadZero) {
            return $alternativeStateT($dictMonadZero['Monad0']())($dictMonadZero['Alternative1']());
        }, function ()  use (&$monadStateT, &$dictMonadZero) {
            return $monadStateT($dictMonadZero['Monad0']());
        });
    };
    $monadPlusStateT = function ($dictMonadPlus)  use (&$PS__Control_MonadPlus, &$monadZeroStateT) {
        return $PS__Control_MonadPlus['MonadPlus'](function ()  use (&$monadZeroStateT, &$dictMonadPlus) {
            return $monadZeroStateT($dictMonadPlus['MonadZero0']());
        });
    };
    return [
        'StateT' => $StateT,
        'runStateT' => $runStateT,
        'evalStateT' => $evalStateT,
        'execStateT' => $execStateT,
        'mapStateT' => $mapStateT,
        'withStateT' => $withStateT,
        'newtypeStateT' => $newtypeStateT,
        'functorStateT' => $functorStateT,
        'applyStateT' => $applyStateT,
        'applicativeStateT' => $applicativeStateT,
        'altStateT' => $altStateT,
        'plusStateT' => $plusStateT,
        'alternativeStateT' => $alternativeStateT,
        'bindStateT' => $bindStateT,
        'monadStateT' => $monadStateT,
        'monadRecStateT' => $monadRecStateT,
        'monadZeroStateT' => $monadZeroStateT,
        'monadPlusStateT' => $monadPlusStateT,
        'monadTransStateT' => $monadTransStateT,
        'lazyStateT' => $lazyStateT,
        'monadEffectState' => $monadEffectState,
        'monadContStateT' => $monadContStateT,
        'monadThrowStateT' => $monadThrowStateT,
        'monadErrorStateT' => $monadErrorStateT,
        'monadAskStateT' => $monadAskStateT,
        'monadReaderStateT' => $monadReaderStateT,
        'monadStateStateT' => $monadStateStateT,
        'monadTellStateT' => $monadTellStateT,
        'monadWriterStateT' => $monadWriterStateT
    ];
})();
