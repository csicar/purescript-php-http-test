<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Biapplicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Biapply/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Bifunctor_Clown = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Bifunctor, &$PS__Control_Biapply, &$PS__Control_Apply, &$PS__Control_Biapplicative, &$PS__Control_Applicative) {
    $Clown = function ($x) {
        return $x;
    };
    $showClown = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Clown ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $ordClown = function ($dictOrd) {
        return $dictOrd;
    };
    $newtypeClown = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Clown);
    $functorClown = $PS__Data_Functor['Functor'](function ($v) {
        return function ($v1) {
            return $v1;
        };
    });
    $eqClown = function ($dictEq) {
        return $dictEq;
    };
    $bifunctorClown = function ($dictFunctor)  use (&$PS__Data_Bifunctor, &$PS__Data_Functor) {
        return $PS__Data_Bifunctor['Bifunctor'](function ($f)  use (&$PS__Data_Functor, &$dictFunctor) {
            return function ($v)  use (&$PS__Data_Functor, &$dictFunctor, &$f) {
                return function ($v1)  use (&$PS__Data_Functor, &$dictFunctor, &$f) {
                    return $PS__Data_Functor['map']($dictFunctor)($f)($v1);
                };
            };
        });
    };
    $biapplyClown = function ($dictApply)  use (&$PS__Control_Biapply, &$bifunctorClown, &$PS__Control_Apply) {
        return $PS__Control_Biapply['Biapply'](function ()  use (&$bifunctorClown, &$dictApply) {
            return $bifunctorClown($dictApply['Functor0']());
        }, function ($v)  use (&$PS__Control_Apply, &$dictApply) {
            return function ($v1)  use (&$PS__Control_Apply, &$dictApply, &$v) {
                return $PS__Control_Apply['apply']($dictApply)($v)($v1);
            };
        });
    };
    $biapplicativeClown = function ($dictApplicative)  use (&$PS__Control_Biapplicative, &$biapplyClown, &$PS__Control_Applicative) {
        return $PS__Control_Biapplicative['Biapplicative'](function ()  use (&$biapplyClown, &$dictApplicative) {
            return $biapplyClown($dictApplicative['Apply0']());
        }, function ($a)  use (&$PS__Control_Applicative, &$dictApplicative) {
            return function ($v)  use (&$PS__Control_Applicative, &$dictApplicative, &$a) {
                return $PS__Control_Applicative['pure']($dictApplicative)($a);
            };
        });
    };
    return [
        'Clown' => $Clown,
        'newtypeClown' => $newtypeClown,
        'eqClown' => $eqClown,
        'ordClown' => $ordClown,
        'showClown' => $showClown,
        'functorClown' => $functorClown,
        'bifunctorClown' => $bifunctorClown,
        'biapplyClown' => $biapplyClown,
        'biapplicativeClown' => $biapplicativeClown
    ];
})();
