<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Writer_Class = (function ()  use (&$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
    $MonadTell = function ($Monad0, $tell) {
        return [
            'Monad0' => $Monad0,
            'tell' => $tell
        ];
    };
    $MonadWriter = function ($MonadTell0, $listen, $pass) {
        return [
            'MonadTell0' => $MonadTell0,
            'listen' => $listen,
            'pass' => $pass
        ];
    };
    $tell = function ($dict) {
        return $dict['tell'];
    };
    $pass = function ($dict) {
        return $dict['pass'];
    };
    $listen = function ($dict) {
        return $dict['listen'];
    };
    $listens = function ($dictMonadWriter)  use (&$PS__Control_Bind, &$listen, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
        return function ($f)  use (&$PS__Control_Bind, &$dictMonadWriter, &$listen, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
            return function ($m)  use (&$PS__Control_Bind, &$dictMonadWriter, &$listen, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple, &$f) {
                return $PS__Control_Bind['bind']((($dictMonadWriter['MonadTell0']())['Monad0']())['Bind1']())($listen($dictMonadWriter)($m))(function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonadWriter, &$PS__Data_Tuple, &$f) {
                    return $PS__Data_Function['apply']($PS__Control_Applicative['pure']((($dictMonadWriter['MonadTell0']())['Monad0']())['Applicative0']()))($PS__Data_Tuple['Tuple']['constructor']($v['value0'], $f($v['value1'])));
                });
            };
        };
    };
    $censor = function ($dictMonadWriter)  use (&$pass, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
        return function ($f)  use (&$pass, &$dictMonadWriter, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
            return function ($m)  use (&$pass, &$dictMonadWriter, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple, &$f) {
                return $pass($dictMonadWriter)($PS__Control_Bind['bind']((($dictMonadWriter['MonadTell0']())['Monad0']())['Bind1']())($m)(function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonadWriter, &$PS__Data_Tuple, &$f) {
                    return $PS__Data_Function['apply']($PS__Control_Applicative['pure']((($dictMonadWriter['MonadTell0']())['Monad0']())['Applicative0']()))($PS__Data_Tuple['Tuple']['constructor']($v, $f));
                }));
            };
        };
    };
    return [
        'listen' => $listen,
        'pass' => $pass,
        'tell' => $tell,
        'MonadTell' => $MonadTell,
        'MonadWriter' => $MonadWriter,
        'listens' => $listens,
        'censor' => $censor
    ];
})();
