<?php
require_once(dirname(__FILE__) . '/../Data.Comparison/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor.Contravariant/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Equivalence = (function ()  use (&$PS__Data_Semigroup, &$PS__Data_HeytingAlgebra, &$PS__Data_Newtype, &$PS__Data_Monoid, &$PS__Data_Eq, &$PS__Data_Functor_Contravariant, &$PS__Data_Function, &$PS__Data_Ordering) {
    $Equivalence = function ($x) {
        return $x;
    };
    $semigroupEquivalence = $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$PS__Data_HeytingAlgebra) {
        return function ($v1)  use (&$PS__Data_HeytingAlgebra, &$v) {
            return function ($a)  use (&$PS__Data_HeytingAlgebra, &$v, &$v1) {
                return function ($b)  use (&$PS__Data_HeytingAlgebra, &$v, &$a, &$v1) {
                    return $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($v($a)($b))($v1($a)($b));
                };
            };
        };
    });
    $newtypeEquivalence = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Equivalence);
    $monoidEquivalence = $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupEquivalence) {
        return $semigroupEquivalence;
    }, function ($v) {
        return function ($v1) {
            return true;
        };
    });
    $defaultEquivalence = function ($dictEq)  use (&$PS__Data_Eq) {
        return $PS__Data_Eq['eq']($dictEq);
    };
    $contravariantEquivalence = $PS__Data_Functor_Contravariant['Contravariant'](function ($f)  use (&$PS__Data_Function) {
        return function ($v)  use (&$PS__Data_Function, &$f) {
            return $PS__Data_Function['on']($v)($f);
        };
    });
    $comparisonEquivalence = function ($v)  use (&$PS__Data_Eq, &$PS__Data_Ordering) {
        return function ($a)  use (&$PS__Data_Eq, &$PS__Data_Ordering, &$v) {
            return function ($b)  use (&$PS__Data_Eq, &$PS__Data_Ordering, &$v, &$a) {
                return $PS__Data_Eq['eq']($PS__Data_Ordering['eqOrdering'])($v($a)($b))($PS__Data_Ordering['EQ']());
            };
        };
    };
    return [
        'Equivalence' => $Equivalence,
        'defaultEquivalence' => $defaultEquivalence,
        'comparisonEquivalence' => $comparisonEquivalence,
        'newtypeEquivalence' => $newtypeEquivalence,
        'contravariantEquivalence' => $contravariantEquivalence,
        'semigroupEquivalence' => $semigroupEquivalence,
        'monoidEquivalence' => $monoidEquivalence
    ];
})();
