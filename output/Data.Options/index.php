<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Op/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Foreign/index.php');
require_once(dirname(__FILE__) . '/../Foreign.Object/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Options = (function ()  use (&$PS__Data_Semigroup, &$PS__Foreign, &$PS__Foreign_Object, &$PS__Data_Foldable, &$PS__Data_Newtype, &$PS__Data_Monoid, &$PS__Data_Tuple, &$PS__Control_Semigroupoid, &$PS__Data_Op, &$PS__Data_Function, &$PS__Data_Maybe) {
    $Options = function ($x) {
        return $x;
    };
    $semigroupOptions = $PS__Data_Semigroup['semigroupArray'];
    $options = function ($v)  use (&$PS__Foreign, &$PS__Foreign_Object, &$PS__Data_Foldable) {
        return $PS__Foreign['unsafeToForeign']($PS__Foreign_Object['fromFoldable']($PS__Data_Foldable['foldableArray'])($v));
    };
    $newtypeOptions = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Options);
    $monoidOptions = $PS__Data_Monoid['monoidArray'];
    $defaultToOptions = function ($k)  use (&$PS__Data_Tuple, &$PS__Foreign) {
        return function ($v)  use (&$PS__Data_Tuple, &$k, &$PS__Foreign) {
            return [ $PS__Data_Tuple['Tuple']['constructor']($k, $PS__Foreign['unsafeToForeign']($v)) ];
        };
    };
    $opt = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Op['Op'])($defaultToOptions);
    $assoc = $PS__Data_Newtype['unwrap']($PS__Data_Op['newtypeOp']);
    $optional = function ($option)  use (&$PS__Data_Function, &$PS__Data_Op, &$PS__Data_Maybe, &$PS__Data_Monoid, &$monoidOptions, &$assoc) {
        return $PS__Data_Function['apply']($PS__Data_Op['Op'])($PS__Data_Maybe['maybe']($PS__Data_Monoid['mempty']($monoidOptions))(function ($v)  use (&$assoc, &$option) {
            return $assoc($option)($v);
        }));
    };
    $tag = function ($o)  use (&$assoc) {
        return function ($value)  use (&$assoc, &$o) {
            return function ($v)  use (&$assoc, &$o, &$value) {
                return $assoc($o)($value);
            };
        };
    };
    return [
        'Options' => $Options,
        'options' => $options,
        'assoc' => $assoc,
        'optional' => $optional,
        'opt' => $opt,
        'tag' => $tag,
        'defaultToOptions' => $defaultToOptions,
        'newtypeOptions' => $newtypeOptions,
        'semigroupOptions' => $semigroupOptions,
        'monoidOptions' => $monoidOptions
    ];
})();
