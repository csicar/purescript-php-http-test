<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadZero/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
$PS__Control_MonadPlus = (function ()  use (&$PS__Control_MonadZero) {
    $MonadPlus = function ($MonadZero0) {
        return [
            'MonadZero0' => $MonadZero0
        ];
    };
    $monadPlusArray = $MonadPlus(function ()  use (&$PS__Control_MonadZero) {
        return $PS__Control_MonadZero['monadZeroArray'];
    });
    return [
        'MonadPlus' => $MonadPlus,
        'monadPlusArray' => $monadPlusArray
    ];
})();
