<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor.Clown/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor.Flip/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor.Joker/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor.Product/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor.Wrap/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Conj/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Disj/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Dual/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Endo/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Bifoldable = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Control_Apply, &$PS__Control_Applicative, &$PS__Data_Unit, &$PS__Control_Category, &$PS__Data_Foldable, &$PS__Data_Semigroup, &$PS__Data_Monoid, &$PS__Data_Newtype, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Data_Function, &$PS__Data_Monoid_Disj, &$PS__Data_Monoid_Conj) {
    $Bifoldable = function ($bifoldMap, $bifoldl, $bifoldr) {
        return [
            'bifoldMap' => $bifoldMap,
            'bifoldl' => $bifoldl,
            'bifoldr' => $bifoldr
        ];
    };
    $bifoldr = function ($dict) {
        return $dict['bifoldr'];
    };
    $bitraverse_ = function ($dictBifoldable)  use (&$bifoldr, &$PS__Control_Semigroupoid, &$PS__Control_Apply, &$PS__Control_Applicative, &$PS__Data_Unit) {
        return function ($dictApplicative)  use (&$bifoldr, &$dictBifoldable, &$PS__Control_Semigroupoid, &$PS__Control_Apply, &$PS__Control_Applicative, &$PS__Data_Unit) {
            return function ($f)  use (&$bifoldr, &$dictBifoldable, &$PS__Control_Semigroupoid, &$PS__Control_Apply, &$dictApplicative, &$PS__Control_Applicative, &$PS__Data_Unit) {
                return function ($g)  use (&$bifoldr, &$dictBifoldable, &$PS__Control_Semigroupoid, &$PS__Control_Apply, &$dictApplicative, &$f, &$PS__Control_Applicative, &$PS__Data_Unit) {
                    return $bifoldr($dictBifoldable)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Apply['applySecond']($dictApplicative['Apply0']()))($f))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Apply['applySecond']($dictApplicative['Apply0']()))($g))($PS__Control_Applicative['pure']($dictApplicative)($PS__Data_Unit['unit']));
                };
            };
        };
    };
    $bifor_ = function ($dictBifoldable)  use (&$bitraverse_) {
        return function ($dictApplicative)  use (&$bitraverse_, &$dictBifoldable) {
            return function ($t)  use (&$bitraverse_, &$dictBifoldable, &$dictApplicative) {
                return function ($f)  use (&$bitraverse_, &$dictBifoldable, &$dictApplicative, &$t) {
                    return function ($g)  use (&$bitraverse_, &$dictBifoldable, &$dictApplicative, &$f, &$t) {
                        return $bitraverse_($dictBifoldable)($dictApplicative)($f)($g)($t);
                    };
                };
            };
        };
    };
    $bisequence_ = function ($dictBifoldable)  use (&$bitraverse_, &$PS__Control_Category) {
        return function ($dictApplicative)  use (&$bitraverse_, &$dictBifoldable, &$PS__Control_Category) {
            return $bitraverse_($dictBifoldable)($dictApplicative)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']))($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        };
    };
    $bifoldl = function ($dict) {
        return $dict['bifoldl'];
    };
    $bifoldableJoker = function ($dictFoldable)  use (&$Bifoldable, &$PS__Data_Foldable) {
        return $Bifoldable(function ($dictMonoid)  use (&$PS__Data_Foldable, &$dictFoldable) {
            return function ($v)  use (&$PS__Data_Foldable, &$dictFoldable, &$dictMonoid) {
                return function ($r)  use (&$PS__Data_Foldable, &$dictFoldable, &$dictMonoid) {
                    return function ($v1)  use (&$PS__Data_Foldable, &$dictFoldable, &$dictMonoid, &$r) {
                        return $PS__Data_Foldable['foldMap']($dictFoldable)($dictMonoid)($r)($v1);
                    };
                };
            };
        }, function ($v)  use (&$PS__Data_Foldable, &$dictFoldable) {
            return function ($r)  use (&$PS__Data_Foldable, &$dictFoldable) {
                return function ($u)  use (&$PS__Data_Foldable, &$dictFoldable, &$r) {
                    return function ($v1)  use (&$PS__Data_Foldable, &$dictFoldable, &$r, &$u) {
                        return $PS__Data_Foldable['foldl']($dictFoldable)($r)($u)($v1);
                    };
                };
            };
        }, function ($v)  use (&$PS__Data_Foldable, &$dictFoldable) {
            return function ($r)  use (&$PS__Data_Foldable, &$dictFoldable) {
                return function ($u)  use (&$PS__Data_Foldable, &$dictFoldable, &$r) {
                    return function ($v1)  use (&$PS__Data_Foldable, &$dictFoldable, &$r, &$u) {
                        return $PS__Data_Foldable['foldr']($dictFoldable)($r)($u)($v1);
                    };
                };
            };
        });
    };
    $bifoldableClown = function ($dictFoldable)  use (&$Bifoldable, &$PS__Data_Foldable) {
        return $Bifoldable(function ($dictMonoid)  use (&$PS__Data_Foldable, &$dictFoldable) {
            return function ($l)  use (&$PS__Data_Foldable, &$dictFoldable, &$dictMonoid) {
                return function ($v)  use (&$PS__Data_Foldable, &$dictFoldable, &$dictMonoid, &$l) {
                    return function ($v1)  use (&$PS__Data_Foldable, &$dictFoldable, &$dictMonoid, &$l) {
                        return $PS__Data_Foldable['foldMap']($dictFoldable)($dictMonoid)($l)($v1);
                    };
                };
            };
        }, function ($l)  use (&$PS__Data_Foldable, &$dictFoldable) {
            return function ($v)  use (&$PS__Data_Foldable, &$dictFoldable, &$l) {
                return function ($u)  use (&$PS__Data_Foldable, &$dictFoldable, &$l) {
                    return function ($v1)  use (&$PS__Data_Foldable, &$dictFoldable, &$l, &$u) {
                        return $PS__Data_Foldable['foldl']($dictFoldable)($l)($u)($v1);
                    };
                };
            };
        }, function ($l)  use (&$PS__Data_Foldable, &$dictFoldable) {
            return function ($v)  use (&$PS__Data_Foldable, &$dictFoldable, &$l) {
                return function ($u)  use (&$PS__Data_Foldable, &$dictFoldable, &$l) {
                    return function ($v1)  use (&$PS__Data_Foldable, &$dictFoldable, &$l, &$u) {
                        return $PS__Data_Foldable['foldr']($dictFoldable)($l)($u)($v1);
                    };
                };
            };
        });
    };
    $bifoldMapDefaultR = function ($dictBifoldable)  use (&$bifoldr, &$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
        return function ($dictMonoid)  use (&$bifoldr, &$dictBifoldable, &$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
            return function ($f)  use (&$bifoldr, &$dictBifoldable, &$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Monoid) {
                return function ($g)  use (&$bifoldr, &$dictBifoldable, &$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$dictMonoid, &$f, &$PS__Data_Monoid) {
                    return $bifoldr($dictBifoldable)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Semigroup['append']($dictMonoid['Semigroup0']()))($f))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Semigroup['append']($dictMonoid['Semigroup0']()))($g))($PS__Data_Monoid['mempty']($dictMonoid));
                };
            };
        };
    };
    $bifoldMapDefaultL = function ($dictBifoldable)  use (&$bifoldl, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
        return function ($dictMonoid)  use (&$bifoldl, &$dictBifoldable, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
            return function ($f)  use (&$bifoldl, &$dictBifoldable, &$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Monoid) {
                return function ($g)  use (&$bifoldl, &$dictBifoldable, &$PS__Data_Semigroup, &$dictMonoid, &$f, &$PS__Data_Monoid) {
                    return $bifoldl($dictBifoldable)(function ($m)  use (&$PS__Data_Semigroup, &$dictMonoid, &$f) {
                        return function ($a)  use (&$PS__Data_Semigroup, &$dictMonoid, &$m, &$f) {
                            return $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($m)($f($a));
                        };
                    })(function ($m)  use (&$PS__Data_Semigroup, &$dictMonoid, &$g) {
                        return function ($b)  use (&$PS__Data_Semigroup, &$dictMonoid, &$m, &$g) {
                            return $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($m)($g($b));
                        };
                    })($PS__Data_Monoid['mempty']($dictMonoid));
                };
            };
        };
    };
    $bifoldMap = function ($dict) {
        return $dict['bifoldMap'];
    };
    $bifoldableFlip = function ($dictBifoldable)  use (&$Bifoldable, &$bifoldMap, &$bifoldl, &$bifoldr) {
        return $Bifoldable(function ($dictMonoid)  use (&$bifoldMap, &$dictBifoldable) {
            return function ($r)  use (&$bifoldMap, &$dictBifoldable, &$dictMonoid) {
                return function ($l)  use (&$bifoldMap, &$dictBifoldable, &$dictMonoid, &$r) {
                    return function ($v)  use (&$bifoldMap, &$dictBifoldable, &$dictMonoid, &$l, &$r) {
                        return $bifoldMap($dictBifoldable)($dictMonoid)($l)($r)($v);
                    };
                };
            };
        }, function ($r)  use (&$bifoldl, &$dictBifoldable) {
            return function ($l)  use (&$bifoldl, &$dictBifoldable, &$r) {
                return function ($u)  use (&$bifoldl, &$dictBifoldable, &$l, &$r) {
                    return function ($v)  use (&$bifoldl, &$dictBifoldable, &$l, &$r, &$u) {
                        return $bifoldl($dictBifoldable)($l)($r)($u)($v);
                    };
                };
            };
        }, function ($r)  use (&$bifoldr, &$dictBifoldable) {
            return function ($l)  use (&$bifoldr, &$dictBifoldable, &$r) {
                return function ($u)  use (&$bifoldr, &$dictBifoldable, &$l, &$r) {
                    return function ($v)  use (&$bifoldr, &$dictBifoldable, &$l, &$r, &$u) {
                        return $bifoldr($dictBifoldable)($l)($r)($u)($v);
                    };
                };
            };
        });
    };
    $bifoldableWrap = function ($dictBifoldable)  use (&$Bifoldable, &$bifoldMap, &$bifoldl, &$bifoldr) {
        return $Bifoldable(function ($dictMonoid)  use (&$bifoldMap, &$dictBifoldable) {
            return function ($l)  use (&$bifoldMap, &$dictBifoldable, &$dictMonoid) {
                return function ($r)  use (&$bifoldMap, &$dictBifoldable, &$dictMonoid, &$l) {
                    return function ($v)  use (&$bifoldMap, &$dictBifoldable, &$dictMonoid, &$l, &$r) {
                        return $bifoldMap($dictBifoldable)($dictMonoid)($l)($r)($v);
                    };
                };
            };
        }, function ($l)  use (&$bifoldl, &$dictBifoldable) {
            return function ($r)  use (&$bifoldl, &$dictBifoldable, &$l) {
                return function ($u)  use (&$bifoldl, &$dictBifoldable, &$l, &$r) {
                    return function ($v)  use (&$bifoldl, &$dictBifoldable, &$l, &$r, &$u) {
                        return $bifoldl($dictBifoldable)($l)($r)($u)($v);
                    };
                };
            };
        }, function ($l)  use (&$bifoldr, &$dictBifoldable) {
            return function ($r)  use (&$bifoldr, &$dictBifoldable, &$l) {
                return function ($u)  use (&$bifoldr, &$dictBifoldable, &$l, &$r) {
                    return function ($v)  use (&$bifoldr, &$dictBifoldable, &$l, &$r, &$u) {
                        return $bifoldr($dictBifoldable)($l)($r)($u)($v);
                    };
                };
            };
        });
    };
    $bifoldlDefault = function ($dictBifoldable)  use (&$PS__Data_Newtype, &$bifoldMap, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$PS__Data_Function) {
        return function ($f)  use (&$PS__Data_Newtype, &$bifoldMap, &$dictBifoldable, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$PS__Data_Function) {
            return function ($g)  use (&$PS__Data_Newtype, &$bifoldMap, &$dictBifoldable, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$PS__Data_Function, &$f) {
                return function ($z)  use (&$PS__Data_Newtype, &$bifoldMap, &$dictBifoldable, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$PS__Data_Function, &$f, &$g) {
                    return function ($p)  use (&$PS__Data_Newtype, &$bifoldMap, &$dictBifoldable, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$PS__Data_Function, &$f, &$g, &$z) {
                        return $PS__Data_Newtype['unwrap']($PS__Data_Newtype['newtypeEndo'])($PS__Data_Newtype['unwrap']($PS__Data_Newtype['newtypeDual'])($bifoldMap($dictBifoldable)($PS__Data_Monoid_Dual['monoidDual']($PS__Data_Monoid_Endo['monoidEndo']($PS__Control_Category['categoryFn'])))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Dual['Dual'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Endo['Endo'])($PS__Data_Function['flip']($f))))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Dual['Dual'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Endo['Endo'])($PS__Data_Function['flip']($g))))($p)))($z);
                    };
                };
            };
        };
    };
    $bifoldrDefault = function ($dictBifoldable)  use (&$PS__Data_Newtype, &$bifoldMap, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid) {
        return function ($f)  use (&$PS__Data_Newtype, &$bifoldMap, &$dictBifoldable, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid) {
            return function ($g)  use (&$PS__Data_Newtype, &$bifoldMap, &$dictBifoldable, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$f) {
                return function ($z)  use (&$PS__Data_Newtype, &$bifoldMap, &$dictBifoldable, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$f, &$g) {
                    return function ($p)  use (&$PS__Data_Newtype, &$bifoldMap, &$dictBifoldable, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$f, &$g, &$z) {
                        return $PS__Data_Newtype['unwrap']($PS__Data_Newtype['newtypeEndo'])($bifoldMap($dictBifoldable)($PS__Data_Monoid_Endo['monoidEndo']($PS__Control_Category['categoryFn']))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Endo['Endo'])($f))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Endo['Endo'])($g))($p))($z);
                    };
                };
            };
        };
    };
    $bifoldableProduct = function ($dictBifoldable)  use (&$Bifoldable, &$PS__Data_Semigroup, &$bifoldMap, &$bifoldlDefault, &$bifoldableProduct, &$bifoldrDefault) {
        return function ($dictBifoldable1)  use (&$Bifoldable, &$PS__Data_Semigroup, &$bifoldMap, &$dictBifoldable, &$bifoldlDefault, &$bifoldableProduct, &$bifoldrDefault) {
            return $Bifoldable(function ($dictMonoid)  use (&$PS__Data_Semigroup, &$bifoldMap, &$dictBifoldable, &$dictBifoldable1) {
                return function ($l)  use (&$PS__Data_Semigroup, &$dictMonoid, &$bifoldMap, &$dictBifoldable, &$dictBifoldable1) {
                    return function ($r)  use (&$PS__Data_Semigroup, &$dictMonoid, &$bifoldMap, &$dictBifoldable, &$l, &$dictBifoldable1) {
                        return function ($v)  use (&$PS__Data_Semigroup, &$dictMonoid, &$bifoldMap, &$dictBifoldable, &$l, &$r, &$dictBifoldable1) {
                            return $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($bifoldMap($dictBifoldable)($dictMonoid)($l)($r)($v['value0']))($bifoldMap($dictBifoldable1)($dictMonoid)($l)($r)($v['value1']));
                        };
                    };
                };
            }, function ($l)  use (&$bifoldlDefault, &$bifoldableProduct, &$dictBifoldable, &$dictBifoldable1) {
                return function ($r)  use (&$bifoldlDefault, &$bifoldableProduct, &$dictBifoldable, &$dictBifoldable1, &$l) {
                    return function ($u)  use (&$bifoldlDefault, &$bifoldableProduct, &$dictBifoldable, &$dictBifoldable1, &$l, &$r) {
                        return function ($m)  use (&$bifoldlDefault, &$bifoldableProduct, &$dictBifoldable, &$dictBifoldable1, &$l, &$r, &$u) {
                            return $bifoldlDefault($bifoldableProduct($dictBifoldable)($dictBifoldable1))($l)($r)($u)($m);
                        };
                    };
                };
            }, function ($l)  use (&$bifoldrDefault, &$bifoldableProduct, &$dictBifoldable, &$dictBifoldable1) {
                return function ($r)  use (&$bifoldrDefault, &$bifoldableProduct, &$dictBifoldable, &$dictBifoldable1, &$l) {
                    return function ($u)  use (&$bifoldrDefault, &$bifoldableProduct, &$dictBifoldable, &$dictBifoldable1, &$l, &$r) {
                        return function ($m)  use (&$bifoldrDefault, &$bifoldableProduct, &$dictBifoldable, &$dictBifoldable1, &$l, &$r, &$u) {
                            return $bifoldrDefault($bifoldableProduct($dictBifoldable)($dictBifoldable1))($l)($r)($u)($m);
                        };
                    };
                };
            });
        };
    };
    $bifold = function ($dictBifoldable)  use (&$bifoldMap, &$PS__Control_Category) {
        return function ($dictMonoid)  use (&$bifoldMap, &$dictBifoldable, &$PS__Control_Category) {
            return $bifoldMap($dictBifoldable)($dictMonoid)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']))($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        };
    };
    $biany = function ($dictBifoldable)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$bifoldMap, &$PS__Data_Monoid_Disj) {
        return function ($dictBooleanAlgebra)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$bifoldMap, &$dictBifoldable, &$PS__Data_Monoid_Disj) {
            return function ($p)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$bifoldMap, &$dictBifoldable, &$PS__Data_Monoid_Disj, &$dictBooleanAlgebra) {
                return function ($q)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$bifoldMap, &$dictBifoldable, &$PS__Data_Monoid_Disj, &$dictBooleanAlgebra, &$p) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Newtype['unwrap']($PS__Data_Newtype['newtypeDisj']))($bifoldMap($dictBifoldable)($PS__Data_Monoid_Disj['monoidDisj']($dictBooleanAlgebra['HeytingAlgebra0']()))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Disj['Disj'])($p))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Disj['Disj'])($q)));
                };
            };
        };
    };
    $biall = function ($dictBifoldable)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$bifoldMap, &$PS__Data_Monoid_Conj) {
        return function ($dictBooleanAlgebra)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$bifoldMap, &$dictBifoldable, &$PS__Data_Monoid_Conj) {
            return function ($p)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$bifoldMap, &$dictBifoldable, &$PS__Data_Monoid_Conj, &$dictBooleanAlgebra) {
                return function ($q)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$bifoldMap, &$dictBifoldable, &$PS__Data_Monoid_Conj, &$dictBooleanAlgebra, &$p) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Newtype['unwrap']($PS__Data_Newtype['newtypeConj']))($bifoldMap($dictBifoldable)($PS__Data_Monoid_Conj['monoidConj']($dictBooleanAlgebra['HeytingAlgebra0']()))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Conj['Conj'])($p))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Conj['Conj'])($q)));
                };
            };
        };
    };
    return [
        'bifoldMap' => $bifoldMap,
        'bifoldl' => $bifoldl,
        'bifoldr' => $bifoldr,
        'Bifoldable' => $Bifoldable,
        'bifoldrDefault' => $bifoldrDefault,
        'bifoldlDefault' => $bifoldlDefault,
        'bifoldMapDefaultR' => $bifoldMapDefaultR,
        'bifoldMapDefaultL' => $bifoldMapDefaultL,
        'bifold' => $bifold,
        'bitraverse_' => $bitraverse_,
        'bifor_' => $bifor_,
        'bisequence_' => $bisequence_,
        'biany' => $biany,
        'biall' => $biall,
        'bifoldableClown' => $bifoldableClown,
        'bifoldableJoker' => $bifoldableJoker,
        'bifoldableFlip' => $bifoldableFlip,
        'bifoldableProduct' => $bifoldableProduct,
        'bifoldableWrap' => $bifoldableWrap
    ];
})();
