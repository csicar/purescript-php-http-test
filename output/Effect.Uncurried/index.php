<?php
require_once(dirname(__FILE__) . '/../Effect/index.php');
$PS__Effect_Uncurried = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    return [
        'mkEffectFn1' => $__foreign['mkEffectFn1'],
        'mkEffectFn2' => $__foreign['mkEffectFn2'],
        'mkEffectFn3' => $__foreign['mkEffectFn3'],
        'mkEffectFn4' => $__foreign['mkEffectFn4'],
        'mkEffectFn5' => $__foreign['mkEffectFn5'],
        'mkEffectFn6' => $__foreign['mkEffectFn6'],
        'mkEffectFn7' => $__foreign['mkEffectFn7'],
        'mkEffectFn8' => $__foreign['mkEffectFn8'],
        'mkEffectFn9' => $__foreign['mkEffectFn9'],
        'mkEffectFn10' => $__foreign['mkEffectFn10'],
        'runEffectFn1' => $__foreign['runEffectFn1'],
        'runEffectFn2' => $__foreign['runEffectFn2'],
        'runEffectFn3' => $__foreign['runEffectFn3'],
        'runEffectFn4' => $__foreign['runEffectFn4'],
        'runEffectFn5' => $__foreign['runEffectFn5'],
        'runEffectFn6' => $__foreign['runEffectFn6'],
        'runEffectFn7' => $__foreign['runEffectFn7'],
        'runEffectFn8' => $__foreign['runEffectFn8'],
        'runEffectFn9' => $__foreign['runEffectFn9'],
        'runEffectFn10' => $__foreign['runEffectFn10']
    ];
})();
