<?php
require_once(dirname(__FILE__) . '/../Data.CommutativeRing/index.php');
require_once(dirname(__FILE__) . '/../Data.DivisionRing/index.php');
require_once(dirname(__FILE__) . '/../Data.EuclideanRing/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
$PS__Data_Field = (function () {
    $Field = function ($DivisionRing1, $EuclideanRing0) {
        return [
            'DivisionRing1' => $DivisionRing1,
            'EuclideanRing0' => $EuclideanRing0
        ];
    };
    $field = function ($dictEuclideanRing)  use (&$Field) {
        return function ($dictDivisionRing)  use (&$Field, &$dictEuclideanRing) {
            return $Field(function ()  use (&$dictDivisionRing) {
                return $dictDivisionRing;
            }, function ()  use (&$dictEuclideanRing) {
                return $dictEuclideanRing;
            });
        };
    };
    return [
        'Field' => $Field,
        'field' => $field
    ];
})();
