<?php

$exports["boolConj"] = function ($b1) {
  return function ($b2) use (&$b1) {
    return $b1 && $b2;
  };
};

$exports["boolDisj"] = function ($b1) {
  return function ($b2) use (&$b1) {
    return $b1 || $b2;
  };
};

$exports["boolNot"] = function ($b) {
  return !$b;
};
