<?php
require_once(dirname(__FILE__) . '/../Data.Symbol/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Record.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Type.Data.Row/index.php');
require_once(dirname(__FILE__) . '/../Type.Data.RowList/index.php');
$PS__Data_HeytingAlgebra = (function ()  use (&$PS__Data_Unit, &$PS__Type_Data_RowList, &$PS__Type_Data_Row, &$PS__Data_Symbol, &$PS__Record_Unsafe) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $HeytingAlgebra = function ($conj, $disj, $ff, $implies, $not, $tt) {
        return [
            'conj' => $conj,
            'disj' => $disj,
            'ff' => $ff,
            'implies' => $implies,
            'not' => $not,
            'tt' => $tt
        ];
    };
    $HeytingAlgebraRecord = function ($conjRecord, $disjRecord, $ffRecord, $impliesRecord, $notRecord, $ttRecord) {
        return [
            'conjRecord' => $conjRecord,
            'disjRecord' => $disjRecord,
            'ffRecord' => $ffRecord,
            'impliesRecord' => $impliesRecord,
            'notRecord' => $notRecord,
            'ttRecord' => $ttRecord
        ];
    };
    $ttRecord = function ($dict) {
        return $dict['ttRecord'];
    };
    $tt = function ($dict) {
        return $dict['tt'];
    };
    $notRecord = function ($dict) {
        return $dict['notRecord'];
    };
    $not = function ($dict) {
        return $dict['not'];
    };
    $impliesRecord = function ($dict) {
        return $dict['impliesRecord'];
    };
    $implies = function ($dict) {
        return $dict['implies'];
    };
    $heytingAlgebraUnit = $HeytingAlgebra(function ($v)  use (&$PS__Data_Unit) {
        return function ($v1)  use (&$PS__Data_Unit) {
            return $PS__Data_Unit['unit'];
        };
    }, function ($v)  use (&$PS__Data_Unit) {
        return function ($v1)  use (&$PS__Data_Unit) {
            return $PS__Data_Unit['unit'];
        };
    }, $PS__Data_Unit['unit'], function ($v)  use (&$PS__Data_Unit) {
        return function ($v1)  use (&$PS__Data_Unit) {
            return $PS__Data_Unit['unit'];
        };
    }, function ($v)  use (&$PS__Data_Unit) {
        return $PS__Data_Unit['unit'];
    }, $PS__Data_Unit['unit']);
    $heytingAlgebraRecordNil = $HeytingAlgebraRecord(function ($v) {
        return function ($v1) {
            return function ($v2) {
                return [];
            };
        };
    }, function ($v) {
        return function ($v1) {
            return function ($v2) {
                return [];
            };
        };
    }, function ($v) {
        return function ($v1) {
            return [];
        };
    }, function ($v) {
        return function ($v1) {
            return function ($v2) {
                return [];
            };
        };
    }, function ($v) {
        return function ($v1) {
            return [];
        };
    }, function ($v) {
        return function ($v1) {
            return [];
        };
    });
    $ffRecord = function ($dict) {
        return $dict['ffRecord'];
    };
    $ff = function ($dict) {
        return $dict['ff'];
    };
    $disjRecord = function ($dict) {
        return $dict['disjRecord'];
    };
    $disj = function ($dict) {
        return $dict['disj'];
    };
    $heytingAlgebraBoolean = $HeytingAlgebra($__foreign['boolConj'], $__foreign['boolDisj'], false, function ($a)  use (&$disj, &$heytingAlgebraBoolean, &$not) {
        return function ($b)  use (&$disj, &$heytingAlgebraBoolean, &$not, &$a) {
            return $disj($heytingAlgebraBoolean)($not($heytingAlgebraBoolean)($a))($b);
        };
    }, $__foreign['boolNot'], true);
    $conjRecord = function ($dict) {
        return $dict['conjRecord'];
    };
    $heytingAlgebraRecord = function ($dictRowToList)  use (&$HeytingAlgebra, &$conjRecord, &$PS__Type_Data_RowList, &$disjRecord, &$ffRecord, &$PS__Type_Data_Row, &$impliesRecord, &$notRecord, &$ttRecord) {
        return function ($dictHeytingAlgebraRecord)  use (&$HeytingAlgebra, &$conjRecord, &$PS__Type_Data_RowList, &$disjRecord, &$ffRecord, &$PS__Type_Data_Row, &$impliesRecord, &$notRecord, &$ttRecord) {
            return $HeytingAlgebra($conjRecord($dictHeytingAlgebraRecord)($PS__Type_Data_RowList['RLProxy']()), $disjRecord($dictHeytingAlgebraRecord)($PS__Type_Data_RowList['RLProxy']()), $ffRecord($dictHeytingAlgebraRecord)($PS__Type_Data_RowList['RLProxy']())($PS__Type_Data_Row['RProxy']()), $impliesRecord($dictHeytingAlgebraRecord)($PS__Type_Data_RowList['RLProxy']()), $notRecord($dictHeytingAlgebraRecord)($PS__Type_Data_RowList['RLProxy']()), $ttRecord($dictHeytingAlgebraRecord)($PS__Type_Data_RowList['RLProxy']())($PS__Type_Data_Row['RProxy']()));
        };
    };
    $conj = function ($dict) {
        return $dict['conj'];
    };
    $heytingAlgebraFunction = function ($dictHeytingAlgebra)  use (&$HeytingAlgebra, &$conj, &$disj, &$ff, &$implies, &$not, &$tt) {
        return $HeytingAlgebra(function ($f)  use (&$conj, &$dictHeytingAlgebra) {
            return function ($g)  use (&$conj, &$dictHeytingAlgebra, &$f) {
                return function ($a)  use (&$conj, &$dictHeytingAlgebra, &$f, &$g) {
                    return $conj($dictHeytingAlgebra)($f($a))($g($a));
                };
            };
        }, function ($f)  use (&$disj, &$dictHeytingAlgebra) {
            return function ($g)  use (&$disj, &$dictHeytingAlgebra, &$f) {
                return function ($a)  use (&$disj, &$dictHeytingAlgebra, &$f, &$g) {
                    return $disj($dictHeytingAlgebra)($f($a))($g($a));
                };
            };
        }, function ($v)  use (&$ff, &$dictHeytingAlgebra) {
            return $ff($dictHeytingAlgebra);
        }, function ($f)  use (&$implies, &$dictHeytingAlgebra) {
            return function ($g)  use (&$implies, &$dictHeytingAlgebra, &$f) {
                return function ($a)  use (&$implies, &$dictHeytingAlgebra, &$f, &$g) {
                    return $implies($dictHeytingAlgebra)($f($a))($g($a));
                };
            };
        }, function ($f)  use (&$not, &$dictHeytingAlgebra) {
            return function ($a)  use (&$not, &$dictHeytingAlgebra, &$f) {
                return $not($dictHeytingAlgebra)($f($a));
            };
        }, function ($v)  use (&$tt, &$dictHeytingAlgebra) {
            return $tt($dictHeytingAlgebra);
        });
    };
    $heytingAlgebraRecordCons = function ($dictIsSymbol)  use (&$HeytingAlgebraRecord, &$conjRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$conj, &$disjRecord, &$disj, &$ffRecord, &$ff, &$impliesRecord, &$implies, &$notRecord, &$not, &$ttRecord, &$tt) {
        return function ($dictCons)  use (&$HeytingAlgebraRecord, &$conjRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$conj, &$disjRecord, &$disj, &$ffRecord, &$ff, &$impliesRecord, &$implies, &$notRecord, &$not, &$ttRecord, &$tt) {
            return function ($dictHeytingAlgebraRecord)  use (&$HeytingAlgebraRecord, &$conjRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$conj, &$disjRecord, &$disj, &$ffRecord, &$ff, &$impliesRecord, &$implies, &$notRecord, &$not, &$ttRecord, &$tt) {
                return function ($dictHeytingAlgebra)  use (&$HeytingAlgebraRecord, &$conjRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$conj, &$disjRecord, &$disj, &$ffRecord, &$ff, &$impliesRecord, &$implies, &$notRecord, &$not, &$ttRecord, &$tt) {
                    return $HeytingAlgebraRecord(function ($v)  use (&$conjRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$conj, &$dictHeytingAlgebra) {
                        return function ($ra)  use (&$conjRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$conj, &$dictHeytingAlgebra) {
                            return function ($rb)  use (&$conjRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$ra, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$conj, &$dictHeytingAlgebra) {
                                $tail = $conjRecord($dictHeytingAlgebraRecord)($PS__Type_Data_RowList['RLProxy']())($ra)($rb);
                                $key = $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($PS__Data_Symbol['SProxy']());
                                $insert = $PS__Record_Unsafe['unsafeSet']($key);
                                $get = $PS__Record_Unsafe['unsafeGet']($key);
                                return $insert($conj($dictHeytingAlgebra)($get($ra))($get($rb)))($tail);
                            };
                        };
                    }, function ($v)  use (&$disjRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$disj, &$dictHeytingAlgebra) {
                        return function ($ra)  use (&$disjRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$disj, &$dictHeytingAlgebra) {
                            return function ($rb)  use (&$disjRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$ra, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$disj, &$dictHeytingAlgebra) {
                                $tail = $disjRecord($dictHeytingAlgebraRecord)($PS__Type_Data_RowList['RLProxy']())($ra)($rb);
                                $key = $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($PS__Data_Symbol['SProxy']());
                                $insert = $PS__Record_Unsafe['unsafeSet']($key);
                                $get = $PS__Record_Unsafe['unsafeGet']($key);
                                return $insert($disj($dictHeytingAlgebra)($get($ra))($get($rb)))($tail);
                            };
                        };
                    }, function ($v)  use (&$ffRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$ff, &$dictHeytingAlgebra) {
                        return function ($row)  use (&$ffRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$ff, &$dictHeytingAlgebra) {
                            $tail = $ffRecord($dictHeytingAlgebraRecord)($PS__Type_Data_RowList['RLProxy']())($row);
                            $key = $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($PS__Data_Symbol['SProxy']());
                            $insert = $PS__Record_Unsafe['unsafeSet']($key);
                            return $insert($ff($dictHeytingAlgebra))($tail);
                        };
                    }, function ($v)  use (&$impliesRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$implies, &$dictHeytingAlgebra) {
                        return function ($ra)  use (&$impliesRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$implies, &$dictHeytingAlgebra) {
                            return function ($rb)  use (&$impliesRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$ra, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$implies, &$dictHeytingAlgebra) {
                                $tail = $impliesRecord($dictHeytingAlgebraRecord)($PS__Type_Data_RowList['RLProxy']())($ra)($rb);
                                $key = $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($PS__Data_Symbol['SProxy']());
                                $insert = $PS__Record_Unsafe['unsafeSet']($key);
                                $get = $PS__Record_Unsafe['unsafeGet']($key);
                                return $insert($implies($dictHeytingAlgebra)($get($ra))($get($rb)))($tail);
                            };
                        };
                    }, function ($v)  use (&$notRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$not, &$dictHeytingAlgebra) {
                        return function ($row)  use (&$notRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$not, &$dictHeytingAlgebra) {
                            $tail = $notRecord($dictHeytingAlgebraRecord)($PS__Type_Data_RowList['RLProxy']())($row);
                            $key = $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($PS__Data_Symbol['SProxy']());
                            $insert = $PS__Record_Unsafe['unsafeSet']($key);
                            $get = $PS__Record_Unsafe['unsafeGet']($key);
                            return $insert($not($dictHeytingAlgebra)($get($row)))($tail);
                        };
                    }, function ($v)  use (&$ttRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$tt, &$dictHeytingAlgebra) {
                        return function ($row)  use (&$ttRecord, &$dictHeytingAlgebraRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$tt, &$dictHeytingAlgebra) {
                            $tail = $ttRecord($dictHeytingAlgebraRecord)($PS__Type_Data_RowList['RLProxy']())($row);
                            $key = $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($PS__Data_Symbol['SProxy']());
                            $insert = $PS__Record_Unsafe['unsafeSet']($key);
                            return $insert($tt($dictHeytingAlgebra))($tail);
                        };
                    });
                };
            };
        };
    };
    return [
        'HeytingAlgebra' => $HeytingAlgebra,
        'tt' => $tt,
        'ff' => $ff,
        'implies' => $implies,
        'conj' => $conj,
        'disj' => $disj,
        'not' => $not,
        'HeytingAlgebraRecord' => $HeytingAlgebraRecord,
        'ffRecord' => $ffRecord,
        'ttRecord' => $ttRecord,
        'impliesRecord' => $impliesRecord,
        'conjRecord' => $conjRecord,
        'disjRecord' => $disjRecord,
        'notRecord' => $notRecord,
        'heytingAlgebraBoolean' => $heytingAlgebraBoolean,
        'heytingAlgebraUnit' => $heytingAlgebraUnit,
        'heytingAlgebraFunction' => $heytingAlgebraFunction,
        'heytingAlgebraRecord' => $heytingAlgebraRecord,
        'heytingAlgebraRecordNil' => $heytingAlgebraRecordNil,
        'heytingAlgebraRecordCons' => $heytingAlgebraRecordCons
    ];
})();
