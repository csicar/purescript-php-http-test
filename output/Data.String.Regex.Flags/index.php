<?php
require_once(dirname(__FILE__) . '/../Control.MonadPlus/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadZero/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.String/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Common/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_String_Regex_Flags = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Functor, &$PS__Control_MonadZero, &$PS__Data_Eq, &$PS__Data_String_Common, &$PS__Data_HeytingAlgebra, &$PS__Data_Monoid) {
    $RegexFlags = [
        'constructor' => function ($value0) {
            return [
                'type' => 'RegexFlags',
                'value0' => $value0
            ];
        },
        'create' => function ($value0)  use (&$RegexFlags) {
            return $RegexFlags['constructor']($value0);
        }
    ];
    $unicode = $RegexFlags['constructor']([
        'global' => false,
        'ignoreCase' => false,
        'multiline' => false,
        'sticky' => false,
        'unicode' => true
    ]);
    $sticky = $RegexFlags['constructor']([
        'global' => false,
        'ignoreCase' => false,
        'multiline' => false,
        'sticky' => true,
        'unicode' => false
    ]);
    $showRegexFlags = $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Functor, &$PS__Control_MonadZero, &$PS__Data_Eq, &$PS__Data_String_Common) {
        $usedFlags = $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupArray'])([  ])($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupArray'])($PS__Data_Functor['voidLeft']($PS__Data_Functor['functorArray'])($PS__Control_MonadZero['guard']($PS__Control_MonadZero['monadZeroArray'])($v['value0']['global']))('global'))($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupArray'])($PS__Data_Functor['voidLeft']($PS__Data_Functor['functorArray'])($PS__Control_MonadZero['guard']($PS__Control_MonadZero['monadZeroArray'])($v['value0']['ignoreCase']))('ignoreCase'))($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupArray'])($PS__Data_Functor['voidLeft']($PS__Data_Functor['functorArray'])($PS__Control_MonadZero['guard']($PS__Control_MonadZero['monadZeroArray'])($v['value0']['multiline']))('multiline'))($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupArray'])($PS__Data_Functor['voidLeft']($PS__Data_Functor['functorArray'])($PS__Control_MonadZero['guard']($PS__Control_MonadZero['monadZeroArray'])($v['value0']['sticky']))('sticky'))($PS__Data_Functor['voidLeft']($PS__Data_Functor['functorArray'])($PS__Control_MonadZero['guard']($PS__Control_MonadZero['monadZeroArray'])($v['value0']['unicode']))('unicode'))))));
        $__local_var__6 = $PS__Data_Eq['eq']($PS__Data_Eq['eqArray']($PS__Data_Eq['eqString']))($usedFlags)([  ]);
        if ($__local_var__6) {
            return 'noFlags';
        };
        return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_String_Common['joinWith'](' <> ')($usedFlags))(')'));
    });
    $semigroupRegexFlags = $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$RegexFlags, &$PS__Data_HeytingAlgebra) {
        return function ($v1)  use (&$RegexFlags, &$PS__Data_HeytingAlgebra, &$v) {
            return $RegexFlags['constructor']([
                'global' => $PS__Data_HeytingAlgebra['disj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($v['value0']['global'])($v1['value0']['global']),
                'ignoreCase' => $PS__Data_HeytingAlgebra['disj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($v['value0']['ignoreCase'])($v1['value0']['ignoreCase']),
                'multiline' => $PS__Data_HeytingAlgebra['disj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($v['value0']['multiline'])($v1['value0']['multiline']),
                'sticky' => $PS__Data_HeytingAlgebra['disj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($v['value0']['sticky'])($v1['value0']['sticky']),
                'unicode' => $PS__Data_HeytingAlgebra['disj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($v['value0']['unicode'])($v1['value0']['unicode'])
            ]);
        };
    });
    $noFlags = $RegexFlags['constructor']([
        'global' => false,
        'ignoreCase' => false,
        'multiline' => false,
        'sticky' => false,
        'unicode' => false
    ]);
    $multiline = $RegexFlags['constructor']([
        'global' => false,
        'ignoreCase' => false,
        'multiline' => true,
        'sticky' => false,
        'unicode' => false
    ]);
    $monoidRegexFlags = $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupRegexFlags) {
        return $semigroupRegexFlags;
    }, $noFlags);
    $ignoreCase = $RegexFlags['constructor']([
        'global' => false,
        'ignoreCase' => true,
        'multiline' => false,
        'sticky' => false,
        'unicode' => false
    ]);
    $global = $RegexFlags['constructor']([
        'global' => true,
        'ignoreCase' => false,
        'multiline' => false,
        'sticky' => false,
        'unicode' => false
    ]);
    $eqRegexFlags = $PS__Data_Eq['Eq'](function ($v)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Eq) {
        return function ($v1)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$v) {
            return $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Eq['eq']($PS__Data_Eq['eqBoolean'])($v['value0']['global'])($v1['value0']['global']))($PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Eq['eq']($PS__Data_Eq['eqBoolean'])($v['value0']['ignoreCase'])($v1['value0']['ignoreCase']))($PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Eq['eq']($PS__Data_Eq['eqBoolean'])($v['value0']['multiline'])($v1['value0']['multiline']))($PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Eq['eq']($PS__Data_Eq['eqBoolean'])($v['value0']['sticky'])($v1['value0']['sticky']))($PS__Data_Eq['eq']($PS__Data_Eq['eqBoolean'])($v['value0']['unicode'])($v1['value0']['unicode'])))));
        };
    });
    return [
        'RegexFlags' => $RegexFlags,
        'noFlags' => $noFlags,
        'global' => $global,
        'ignoreCase' => $ignoreCase,
        'multiline' => $multiline,
        'sticky' => $sticky,
        'unicode' => $unicode,
        'semigroupRegexFlags' => $semigroupRegexFlags,
        'monoidRegexFlags' => $monoidRegexFlags,
        'eqRegexFlags' => $eqRegexFlags,
        'showRegexFlags' => $showRegexFlags
    ];
})();
