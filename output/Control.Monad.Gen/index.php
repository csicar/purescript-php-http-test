<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Gen.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Additive/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Gen = (function ()  use (&$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$PS__Control_Bind, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Unfoldable, &$PS__Control_Monad_Gen_Class, &$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$PS__Data_Ord, &$PS__Data_Newtype, &$PS__Data_Monoid_Additive, &$PS__Data_Foldable, &$PS__Data_Semiring, &$PS__Data_Semigroup_Foldable, &$PS__Data_Unit) {
    $Cons = [
        'constructor' => function ($value0, $value1) {
            return [
                'type' => 'Cons',
                'value0' => $value0,
                'value1' => $value1
            ];
        },
        'create' => function ($value0)  use (&$Cons) {
            return function ($value1)  use (&$Cons, &$value0) {
                return $Cons['constructor']($value0, $value1);
            };
        }
    ];
    $Nil = function () {
        return [
            'type' => 'Nil'
        ];
    };
    $FreqSemigroup = function ($x) {
        return $x;
    };
    $AtIndex = function ($x) {
        return $x;
    };
    $unfoldable = function ($dictMonadRec)  use (&$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$PS__Control_Bind, &$Cons, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Unfoldable, &$PS__Control_Monad_Gen_Class, &$PS__Control_Semigroupoid, &$Nil) {
        return function ($dictMonadGen)  use (&$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$PS__Control_Bind, &$Cons, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Unfoldable, &$PS__Control_Monad_Gen_Class, &$PS__Control_Semigroupoid, &$dictMonadRec, &$Nil) {
            return function ($dictUnfoldable)  use (&$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Function, &$PS__Control_Applicative, &$dictMonadGen, &$PS__Control_Monad_Rec_Class, &$PS__Control_Bind, &$Cons, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Unfoldable, &$PS__Control_Monad_Gen_Class, &$PS__Control_Semigroupoid, &$dictMonadRec, &$Nil) {
                return function ($gen)  use (&$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Function, &$PS__Control_Applicative, &$dictMonadGen, &$PS__Control_Monad_Rec_Class, &$PS__Control_Bind, &$Cons, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_Unfoldable, &$dictUnfoldable, &$PS__Control_Monad_Gen_Class, &$PS__Control_Semigroupoid, &$dictMonadRec, &$Nil) {
                    $unfold = function ($v)  use (&$PS__Data_Maybe, &$PS__Data_Tuple) {
                        if ($v['type'] === 'Nil') {
                            return $PS__Data_Maybe['Nothing']();
                        };
                        if ($v['type'] === 'Cons') {
                            return $PS__Data_Maybe['Just']['constructor']($PS__Data_Tuple['Tuple']['constructor']($v['value0'], $v['value1']));
                        };
                        throw new \Exception('Failed pattern match at Control.Monad.Gen line 101, column 12 - line 103, column 35: ' . var_dump([ $v['constructor']['name'] ]));
                    };
                    $loopGen = function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonadGen, &$PS__Control_Monad_Rec_Class, &$PS__Control_Bind, &$gen, &$PS__Data_Tuple, &$Cons, &$PS__Data_Ring) {
                        if ($v['value1'] === 0) {
                            return $PS__Data_Function['apply']($PS__Control_Applicative['pure'](($dictMonadGen['Monad0']())['Applicative0']()))($PS__Control_Monad_Rec_Class['Done']['constructor']($v['value0']));
                        };
                        return $PS__Control_Bind['bind'](($dictMonadGen['Monad0']())['Bind1']())($gen)(function ($v1)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonadGen, &$PS__Control_Monad_Rec_Class, &$PS__Data_Tuple, &$Cons, &$v, &$PS__Data_Ring) {
                            return $PS__Data_Function['apply']($PS__Control_Applicative['pure'](($dictMonadGen['Monad0']())['Applicative0']()))($PS__Control_Monad_Rec_Class['Loop']['constructor']($PS__Data_Tuple['Tuple']['constructor']($Cons['constructor']($v1, $v['value0']), $PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v['value1'])(1))));
                        });
                    };
                    return $PS__Data_Functor['map'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($PS__Data_Unfoldable['unfoldr']($dictUnfoldable)($unfold))($PS__Control_Monad_Gen_Class['sized']($dictMonadGen)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Rec_Class['tailRecM']($dictMonadRec)($loopGen))($PS__Data_Tuple['Tuple']['create']($Nil()))));
                };
            };
        };
    };
    $semigroupFreqSemigroup = $PS__Data_Semigroup['Semigroup'](function ($v) {
        return function ($v1)  use (&$v) {
            return function ($pos)  use (&$v, &$v1) {
                $v2 = $v($pos);
                if ($v2['value0']['type'] === 'Just') {
                    return $v1($v2['value0']['value0']);
                };
                return $v2;
            };
        };
    });
    $semigroupAtIndex = $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$PS__Data_Ord, &$PS__Data_Ring) {
        return function ($v1)  use (&$PS__Data_Ord, &$v, &$PS__Data_Ring) {
            return function ($i)  use (&$PS__Data_Ord, &$v, &$v1, &$PS__Data_Ring) {
                $__local_var__48 = $PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])($i)(0);
                if ($__local_var__48) {
                    return $v($i);
                };
                return $v1($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($i)(1));
            };
        };
    });
    $getFreqVal = function ($v)  use (&$PS__Control_Semigroupoid, &$PS__Data_Tuple) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Tuple['snd'])($v);
    };
    $getAtIndex = function ($v) {
        return $v;
    };
    $freqSemigroup = function ($v)  use (&$PS__Data_Ord, &$PS__Data_Tuple, &$PS__Data_Maybe, &$PS__Data_Ring) {
        return function ($pos)  use (&$PS__Data_Ord, &$v, &$PS__Data_Tuple, &$PS__Data_Maybe, &$PS__Data_Ring) {
            $__local_var__52 = $PS__Data_Ord['greaterThanOrEq']($PS__Data_Ord['ordNumber'])($pos)($v['value0']);
            if ($__local_var__52) {
                return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_Maybe['Just']['constructor']($PS__Data_Ring['sub']($PS__Data_Ring['ringNumber'])($pos)($v['value0'])), $v['value1']);
            };
            return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_Maybe['Nothing'](), $v['value1']);
        };
    };
    $frequency = function ($dictMonadGen)  use (&$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Monoid_Additive, &$PS__Data_Foldable, &$PS__Data_Semiring, &$PS__Data_Tuple, &$PS__Control_Bind, &$PS__Control_Monad_Gen_Class, &$getFreqVal, &$PS__Data_Semigroup_Foldable, &$semigroupFreqSemigroup, &$freqSemigroup) {
        return function ($dictFoldable1)  use (&$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Monoid_Additive, &$PS__Data_Foldable, &$PS__Data_Semiring, &$PS__Data_Tuple, &$PS__Control_Bind, &$dictMonadGen, &$PS__Control_Monad_Gen_Class, &$getFreqVal, &$PS__Data_Semigroup_Foldable, &$semigroupFreqSemigroup, &$freqSemigroup) {
            return function ($xs)  use (&$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Monoid_Additive, &$PS__Data_Foldable, &$dictFoldable1, &$PS__Data_Semiring, &$PS__Data_Tuple, &$PS__Control_Bind, &$dictMonadGen, &$PS__Control_Monad_Gen_Class, &$getFreqVal, &$PS__Data_Semigroup_Foldable, &$semigroupFreqSemigroup, &$freqSemigroup) {
                $total = $PS__Data_Newtype['alaF']($PS__Data_Functor['functorFn'])($PS__Data_Functor['functorFn'])($PS__Data_Newtype['newtypeAdditive'])($PS__Data_Newtype['newtypeAdditive'])($PS__Data_Monoid_Additive['Additive'])($PS__Data_Foldable['foldMap']($dictFoldable1['Foldable0']())($PS__Data_Monoid_Additive['monoidAdditive']($PS__Data_Semiring['semiringNumber'])))($PS__Data_Tuple['fst'])($xs);
                return $PS__Control_Bind['bind'](($dictMonadGen['Monad0']())['Bind1']())($PS__Control_Monad_Gen_Class['chooseFloat']($dictMonadGen)(0.0)($total))($getFreqVal($PS__Data_Semigroup_Foldable['foldMap1']($dictFoldable1)($semigroupFreqSemigroup)($freqSemigroup)($xs)));
            };
        };
    };
    $filtered = function ($dictMonadRec)  use (&$PS__Data_Functor, &$PS__Control_Monad_Rec_Class, &$PS__Data_Unit) {
        return function ($dictMonadGen)  use (&$PS__Data_Functor, &$PS__Control_Monad_Rec_Class, &$PS__Data_Unit, &$dictMonadRec) {
            return function ($gen)  use (&$PS__Data_Functor, &$dictMonadGen, &$PS__Control_Monad_Rec_Class, &$PS__Data_Unit, &$dictMonadRec) {
                $go = function ($v)  use (&$PS__Data_Functor, &$dictMonadGen, &$gen, &$PS__Control_Monad_Rec_Class, &$PS__Data_Unit) {
                    return $PS__Data_Functor['mapFlipped'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($gen)(function ($a)  use (&$PS__Control_Monad_Rec_Class, &$PS__Data_Unit) {
                        if ($a['type'] === 'Nothing') {
                            return $PS__Control_Monad_Rec_Class['Loop']['constructor']($PS__Data_Unit['unit']);
                        };
                        if ($a['type'] === 'Just') {
                            return $PS__Control_Monad_Rec_Class['Done']['constructor']($a['value0']);
                        };
                        throw new \Exception('Failed pattern match at Control.Monad.Gen line 117, column 24 - line 119, column 23: ' . var_dump([ $a['constructor']['name'] ]));
                    });
                };
                return $PS__Control_Monad_Rec_Class['tailRecM']($dictMonadRec)($go)($PS__Data_Unit['unit']);
            };
        };
    };
    $suchThat = function ($dictMonadRec)  use (&$PS__Data_Function, &$filtered, &$PS__Data_Functor, &$PS__Data_Maybe) {
        return function ($dictMonadGen)  use (&$PS__Data_Function, &$filtered, &$dictMonadRec, &$PS__Data_Functor, &$PS__Data_Maybe) {
            return function ($gen)  use (&$PS__Data_Function, &$filtered, &$dictMonadRec, &$dictMonadGen, &$PS__Data_Functor, &$PS__Data_Maybe) {
                return function ($pred)  use (&$PS__Data_Function, &$filtered, &$dictMonadRec, &$dictMonadGen, &$PS__Data_Functor, &$gen, &$PS__Data_Maybe) {
                    return $PS__Data_Function['apply']($filtered($dictMonadRec)($dictMonadGen))($PS__Data_Functor['mapFlipped'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($gen)(function ($a)  use (&$pred, &$PS__Data_Maybe) {
                        $__local_var__57 = $pred($a);
                        if ($__local_var__57) {
                            return $PS__Data_Maybe['Just']['constructor']($a);
                        };
                        return $PS__Data_Maybe['Nothing']();
                    }));
                };
            };
        };
    };
    $choose = function ($dictMonadGen)  use (&$PS__Control_Bind, &$PS__Control_Monad_Gen_Class) {
        return function ($genA)  use (&$PS__Control_Bind, &$dictMonadGen, &$PS__Control_Monad_Gen_Class) {
            return function ($genB)  use (&$PS__Control_Bind, &$dictMonadGen, &$PS__Control_Monad_Gen_Class, &$genA) {
                return $PS__Control_Bind['bind'](($dictMonadGen['Monad0']())['Bind1']())($PS__Control_Monad_Gen_Class['chooseBool']($dictMonadGen))(function ($v)  use (&$genA, &$genB) {
                    if ($v) {
                        return $genA;
                    };
                    return $genB;
                });
            };
        };
    };
    $atIndex = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($AtIndex)($PS__Data_Function['const']);
    $fromIndex = function ($dictFoldable1)  use (&$getAtIndex, &$PS__Data_Semigroup_Foldable, &$semigroupAtIndex, &$atIndex) {
        return function ($i)  use (&$getAtIndex, &$PS__Data_Semigroup_Foldable, &$dictFoldable1, &$semigroupAtIndex, &$atIndex) {
            return function ($xs)  use (&$getAtIndex, &$PS__Data_Semigroup_Foldable, &$dictFoldable1, &$semigroupAtIndex, &$atIndex, &$i) {
                return $getAtIndex($PS__Data_Semigroup_Foldable['foldMap1']($dictFoldable1)($semigroupAtIndex)($atIndex)($xs))($i);
            };
        };
    };
    $elements = function ($dictMonadGen)  use (&$PS__Control_Bind, &$PS__Control_Monad_Gen_Class, &$PS__Data_Ring, &$PS__Data_Foldable, &$PS__Data_Semiring, &$PS__Data_Function, &$PS__Control_Applicative, &$fromIndex) {
        return function ($dictFoldable1)  use (&$PS__Control_Bind, &$dictMonadGen, &$PS__Control_Monad_Gen_Class, &$PS__Data_Ring, &$PS__Data_Foldable, &$PS__Data_Semiring, &$PS__Data_Function, &$PS__Control_Applicative, &$fromIndex) {
            return function ($xs)  use (&$PS__Control_Bind, &$dictMonadGen, &$PS__Control_Monad_Gen_Class, &$PS__Data_Ring, &$PS__Data_Foldable, &$dictFoldable1, &$PS__Data_Semiring, &$PS__Data_Function, &$PS__Control_Applicative, &$fromIndex) {
                return $PS__Control_Bind['bind'](($dictMonadGen['Monad0']())['Bind1']())($PS__Control_Monad_Gen_Class['chooseInt']($dictMonadGen)(0)($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($PS__Data_Foldable['length']($dictFoldable1['Foldable0']())($PS__Data_Semiring['semiringInt'])($xs))(1)))(function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonadGen, &$fromIndex, &$dictFoldable1, &$xs) {
                    return $PS__Data_Function['apply']($PS__Control_Applicative['pure'](($dictMonadGen['Monad0']())['Applicative0']()))($fromIndex($dictFoldable1)($v)($xs));
                });
            };
        };
    };
    $oneOf = function ($dictMonadGen)  use (&$PS__Control_Bind, &$PS__Control_Monad_Gen_Class, &$PS__Data_Ring, &$PS__Data_Foldable, &$PS__Data_Semiring, &$fromIndex) {
        return function ($dictFoldable1)  use (&$PS__Control_Bind, &$dictMonadGen, &$PS__Control_Monad_Gen_Class, &$PS__Data_Ring, &$PS__Data_Foldable, &$PS__Data_Semiring, &$fromIndex) {
            return function ($xs)  use (&$PS__Control_Bind, &$dictMonadGen, &$PS__Control_Monad_Gen_Class, &$PS__Data_Ring, &$PS__Data_Foldable, &$dictFoldable1, &$PS__Data_Semiring, &$fromIndex) {
                return $PS__Control_Bind['bind'](($dictMonadGen['Monad0']())['Bind1']())($PS__Control_Monad_Gen_Class['chooseInt']($dictMonadGen)(0)($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($PS__Data_Foldable['length']($dictFoldable1['Foldable0']())($PS__Data_Semiring['semiringInt'])($xs))(1)))(function ($v)  use (&$fromIndex, &$dictFoldable1, &$xs) {
                    return $fromIndex($dictFoldable1)($v)($xs);
                });
            };
        };
    };
    return [
        'choose' => $choose,
        'oneOf' => $oneOf,
        'frequency' => $frequency,
        'elements' => $elements,
        'unfoldable' => $unfoldable,
        'suchThat' => $suchThat,
        'filtered' => $filtered
    ];
})();
