<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
$PS__Control_Biapply = (function ()  use (&$PS__Control_Category, &$PS__Data_Bifunctor, &$PS__Data_Function) {
    $Biapply = function ($Bifunctor0, $biapply) {
        return [
            'Bifunctor0' => $Bifunctor0,
            'biapply' => $biapply
        ];
    };
    $biapply = function ($dict) {
        return $dict['biapply'];
    };
    $biapplyFirst = function ($dictBiapply)  use (&$biapply, &$PS__Control_Category, &$PS__Data_Bifunctor, &$PS__Data_Function) {
        return function ($a)  use (&$biapply, &$dictBiapply, &$PS__Control_Category, &$PS__Data_Bifunctor, &$PS__Data_Function) {
            return function ($b)  use (&$biapply, &$dictBiapply, &$PS__Control_Category, &$PS__Data_Bifunctor, &$PS__Data_Function, &$a) {
                return $biapply($dictBiapply)($PS__Control_Category['identity']($PS__Control_Category['categoryFn'])($PS__Data_Bifunctor['bimap']($dictBiapply['Bifunctor0']())($PS__Data_Function['const']($PS__Control_Category['identity']($PS__Control_Category['categoryFn'])))($PS__Data_Function['const']($PS__Control_Category['identity']($PS__Control_Category['categoryFn']))))($a))($b);
            };
        };
    };
    $biapplySecond = function ($dictBiapply)  use (&$biapply, &$PS__Control_Category, &$PS__Data_Bifunctor, &$PS__Data_Function) {
        return function ($a)  use (&$biapply, &$dictBiapply, &$PS__Control_Category, &$PS__Data_Bifunctor, &$PS__Data_Function) {
            return function ($b)  use (&$biapply, &$dictBiapply, &$PS__Control_Category, &$PS__Data_Bifunctor, &$PS__Data_Function, &$a) {
                return $biapply($dictBiapply)($PS__Control_Category['identity']($PS__Control_Category['categoryFn'])($PS__Data_Bifunctor['bimap']($dictBiapply['Bifunctor0']())($PS__Data_Function['const'])($PS__Data_Function['const']))($a))($b);
            };
        };
    };
    $bilift2 = function ($dictBiapply)  use (&$biapply, &$PS__Control_Category, &$PS__Data_Bifunctor) {
        return function ($f)  use (&$biapply, &$dictBiapply, &$PS__Control_Category, &$PS__Data_Bifunctor) {
            return function ($g)  use (&$biapply, &$dictBiapply, &$PS__Control_Category, &$PS__Data_Bifunctor, &$f) {
                return function ($a)  use (&$biapply, &$dictBiapply, &$PS__Control_Category, &$PS__Data_Bifunctor, &$f, &$g) {
                    return function ($b)  use (&$biapply, &$dictBiapply, &$PS__Control_Category, &$PS__Data_Bifunctor, &$f, &$g, &$a) {
                        return $biapply($dictBiapply)($PS__Control_Category['identity']($PS__Control_Category['categoryFn'])($PS__Data_Bifunctor['bimap']($dictBiapply['Bifunctor0']())($f)($g))($a))($b);
                    };
                };
            };
        };
    };
    $bilift3 = function ($dictBiapply)  use (&$biapply, &$PS__Control_Category, &$PS__Data_Bifunctor) {
        return function ($f)  use (&$biapply, &$dictBiapply, &$PS__Control_Category, &$PS__Data_Bifunctor) {
            return function ($g)  use (&$biapply, &$dictBiapply, &$PS__Control_Category, &$PS__Data_Bifunctor, &$f) {
                return function ($a)  use (&$biapply, &$dictBiapply, &$PS__Control_Category, &$PS__Data_Bifunctor, &$f, &$g) {
                    return function ($b)  use (&$biapply, &$dictBiapply, &$PS__Control_Category, &$PS__Data_Bifunctor, &$f, &$g, &$a) {
                        return function ($c)  use (&$biapply, &$dictBiapply, &$PS__Control_Category, &$PS__Data_Bifunctor, &$f, &$g, &$a, &$b) {
                            return $biapply($dictBiapply)($biapply($dictBiapply)($PS__Control_Category['identity']($PS__Control_Category['categoryFn'])($PS__Data_Bifunctor['bimap']($dictBiapply['Bifunctor0']())($f)($g))($a))($b))($c);
                        };
                    };
                };
            };
        };
    };
    return [
        'biapply' => $biapply,
        'Biapply' => $Biapply,
        'biapplyFirst' => $biapplyFirst,
        'biapplySecond' => $biapplySecond,
        'bilift2' => $bilift2,
        'bilift3' => $bilift3
    ];
})();
