<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Trans.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadPlus/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadZero/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable1/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Effect.Class/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_List_Trans = (function ()  use (&$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$PS__Data_Lazy, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Control_Bind, &$PS__Data_Ring, &$PS__Data_Newtype, &$PS__Control_Category, &$PS__Control_Monad_Trans_Class, &$PS__Control_Monad_Rec_Class, &$PS__Data_Unit, &$PS__Data_Unfoldable1, &$PS__Data_Unfoldable, &$PS__Data_Semigroup, &$PS__Data_Monoid, &$PS__Control_Monad, &$PS__Control_Apply, &$PS__Effect_Class, &$PS__Control_Alt, &$PS__Control_Plus, &$PS__Control_Alternative, &$PS__Control_MonadZero, &$PS__Control_MonadPlus) {
    $Yield = [
        'constructor' => function ($value0, $value1) {
            return [
                'type' => 'Yield',
                'value0' => $value0,
                'value1' => $value1
            ];
        },
        'create' => function ($value0)  use (&$Yield) {
            return function ($value1)  use (&$Yield, &$value0) {
                return $Yield['constructor']($value0, $value1);
            };
        }
    ];
    $Skip = [
        'constructor' => function ($value0) {
            return [
                'type' => 'Skip',
                'value0' => $value0
            ];
        },
        'create' => function ($value0)  use (&$Skip) {
            return $Skip['constructor']($value0);
        }
    ];
    $Done = function () {
        return [
            'type' => 'Done'
        ];
    };
    $ListT = function ($x) {
        return $x;
    };
    $wrapLazy = function ($dictApplicative)  use (&$PS__Data_Function, &$ListT, &$PS__Control_Applicative, &$Skip) {
        return function ($v)  use (&$PS__Data_Function, &$ListT, &$PS__Control_Applicative, &$dictApplicative, &$Skip) {
            return $PS__Data_Function['apply']($ListT)($PS__Control_Applicative['pure']($dictApplicative)($Skip['constructor']($v)));
        };
    };
    $wrapEffect = function ($dictFunctor)  use (&$PS__Data_Function, &$ListT, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$Skip, &$PS__Data_Lazy) {
        return function ($v)  use (&$PS__Data_Function, &$ListT, &$PS__Data_Functor, &$dictFunctor, &$PS__Control_Semigroupoid, &$Skip, &$PS__Data_Lazy) {
            return $PS__Data_Function['apply']($ListT)($PS__Data_Functor['map']($dictFunctor)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($Skip['create'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Lazy['defer'])($PS__Data_Function['const'])))($v));
        };
    };
    $unfold = function ($dictMonad)  use (&$Yield, &$PS__Data_Lazy, &$unfold, &$Done, &$PS__Data_Function, &$ListT, &$PS__Data_Functor) {
        return function ($f)  use (&$Yield, &$PS__Data_Lazy, &$unfold, &$dictMonad, &$Done, &$PS__Data_Function, &$ListT, &$PS__Data_Functor) {
            return function ($z)  use (&$Yield, &$PS__Data_Lazy, &$unfold, &$dictMonad, &$f, &$Done, &$PS__Data_Function, &$ListT, &$PS__Data_Functor) {
                $g = function ($v)  use (&$Yield, &$PS__Data_Lazy, &$unfold, &$dictMonad, &$f, &$Done) {
                    if ($v['type'] === 'Just') {
                        return $Yield['constructor']($v['value0']['value1'], $PS__Data_Lazy['defer'](function ($v1)  use (&$unfold, &$dictMonad, &$f, &$v) {
                            return $unfold($dictMonad)($f)($v['value0']['value0']);
                        }));
                    };
                    if ($v['type'] === 'Nothing') {
                        return $Done();
                    };
                    throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 130, column 3 - line 130, column 60: ' . var_dump([ $v['constructor']['name'] ]));
                };
                return $PS__Data_Function['apply']($ListT)($PS__Data_Functor['map']((($dictMonad['Bind1']())['Apply0']())['Functor0']())($g)($f($z)));
            };
        };
    };
    $uncons = function ($dictMonad)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Lazy, &$uncons, &$PS__Control_Bind) {
        return function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Lazy, &$uncons, &$PS__Control_Bind) {
            $g = function ($v1)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Lazy, &$uncons) {
                if ($v1['type'] === 'Yield') {
                    return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($PS__Data_Function['apply']($PS__Data_Maybe['Just']['create'])($PS__Data_Tuple['Tuple']['constructor']($v1['value0'], $PS__Data_Lazy['force']($v1['value1']))));
                };
                if ($v1['type'] === 'Skip') {
                    return $uncons($dictMonad)($PS__Data_Lazy['force']($v1['value0']));
                };
                if ($v1['type'] === 'Done') {
                    return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($PS__Data_Maybe['Nothing']());
                };
                throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 195, column 3 - line 195, column 50: ' . var_dump([ $v1['constructor']['name'] ]));
            };
            return $PS__Control_Bind['bind']($dictMonad['Bind1']())($v)($g);
        };
    };
    $tail = function ($dictMonad)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Tuple, &$uncons) {
        return function ($l)  use (&$PS__Data_Functor, &$dictMonad, &$PS__Data_Maybe, &$PS__Data_Tuple, &$uncons) {
            return $PS__Data_Functor['map']((($dictMonad['Bind1']())['Apply0']())['Functor0']())($PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])($PS__Data_Tuple['snd']))($uncons($dictMonad)($l));
        };
    };
    $stepMap = function ($dictFunctor)  use (&$PS__Data_Function, &$ListT, &$PS__Data_Functor) {
        return function ($f)  use (&$PS__Data_Function, &$ListT, &$PS__Data_Functor, &$dictFunctor) {
            return function ($v)  use (&$PS__Data_Function, &$ListT, &$PS__Data_Functor, &$dictFunctor, &$f) {
                return $PS__Data_Function['apply']($ListT)($PS__Data_Functor['map']($dictFunctor)($f)($v));
            };
        };
    };
    $takeWhile = function ($dictApplicative)  use (&$Yield, &$PS__Data_Functor, &$PS__Data_Lazy, &$takeWhile, &$Done, &$PS__Data_Function, &$Skip, &$stepMap) {
        return function ($f)  use (&$Yield, &$PS__Data_Functor, &$PS__Data_Lazy, &$takeWhile, &$dictApplicative, &$Done, &$PS__Data_Function, &$Skip, &$stepMap) {
            $g = function ($v)  use (&$f, &$Yield, &$PS__Data_Functor, &$PS__Data_Lazy, &$takeWhile, &$dictApplicative, &$Done, &$PS__Data_Function, &$Skip) {
                if ($v['type'] === 'Yield') {
                    $__local_var__101 = $f($v['value0']);
                    if ($__local_var__101) {
                        return $Yield['constructor']($v['value0'], $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($takeWhile($dictApplicative)($f))($v['value1']));
                    };
                    return $Done();
                };
                if ($v['type'] === 'Skip') {
                    return $PS__Data_Function['apply']($Skip['create'])($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($takeWhile($dictApplicative)($f))($v['value0']));
                };
                if ($v['type'] === 'Done') {
                    return $Done();
                };
                throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 154, column 3 - line 154, column 68: ' . var_dump([ $v['constructor']['name'] ]));
            };
            return $stepMap(($dictApplicative['Apply0']())['Functor0']())($g);
        };
    };
    $scanl = function ($dictMonad)  use (&$PS__Data_Function, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Lazy, &$PS__Data_Functor, &$unfold) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Lazy, &$PS__Data_Functor, &$dictMonad, &$unfold) {
            return function ($b)  use (&$f, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Lazy, &$PS__Data_Functor, &$dictMonad, &$unfold) {
                return function ($l)  use (&$f, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Lazy, &$PS__Data_Functor, &$dictMonad, &$unfold, &$b) {
                    $g = function ($v)  use (&$f, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Lazy, &$PS__Data_Functor, &$dictMonad) {
                        $h = function ($v1)  use (&$f, &$v, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Lazy) {
                            if ($v1['type'] === 'Yield') {
                                $b__prime__prime = $f($v['value0'])($v1['value0']);
                                return $PS__Data_Function['apply']($PS__Data_Maybe['Just']['create'])($PS__Data_Tuple['Tuple']['constructor']($PS__Data_Tuple['Tuple']['constructor']($b__prime__prime, $PS__Data_Lazy['force']($v1['value1'])), $v['value0']));
                            };
                            if ($v1['type'] === 'Skip') {
                                return $PS__Data_Function['apply']($PS__Data_Maybe['Just']['create'])($PS__Data_Tuple['Tuple']['constructor']($PS__Data_Tuple['Tuple']['constructor']($v['value0'], $PS__Data_Lazy['force']($v1['value0'])), $v['value0']));
                            };
                            if ($v1['type'] === 'Done') {
                                return $PS__Data_Maybe['Nothing']();
                            };
                            throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 248, column 5 - line 248, column 78: ' . var_dump([ $v1['constructor']['name'] ]));
                        };
                        return $PS__Data_Functor['map']((($dictMonad['Bind1']())['Apply0']())['Functor0']())($h)($v['value1']);
                    };
                    return $unfold($dictMonad)($g)($PS__Data_Tuple['Tuple']['constructor']($b, $l));
                };
            };
        };
    };
    $prepend__prime = function ($dictApplicative)  use (&$PS__Data_Function, &$ListT, &$PS__Control_Applicative, &$Yield) {
        return function ($h)  use (&$PS__Data_Function, &$ListT, &$PS__Control_Applicative, &$dictApplicative, &$Yield) {
            return function ($t)  use (&$PS__Data_Function, &$ListT, &$PS__Control_Applicative, &$dictApplicative, &$Yield, &$h) {
                return $PS__Data_Function['apply']($ListT)($PS__Control_Applicative['pure']($dictApplicative)($Yield['constructor']($h, $t)));
            };
        };
    };
    $prepend = function ($dictApplicative)  use (&$prepend__prime, &$PS__Data_Function, &$PS__Data_Lazy) {
        return function ($h)  use (&$prepend__prime, &$dictApplicative, &$PS__Data_Function, &$PS__Data_Lazy) {
            return function ($t)  use (&$prepend__prime, &$dictApplicative, &$h, &$PS__Data_Function, &$PS__Data_Lazy) {
                return $prepend__prime($dictApplicative)($h)($PS__Data_Function['apply']($PS__Data_Lazy['defer'])($PS__Data_Function['const']($t)));
            };
        };
    };
    $nil = function ($dictApplicative)  use (&$PS__Data_Function, &$ListT, &$PS__Control_Applicative, &$Done) {
        return $PS__Data_Function['apply']($ListT)($PS__Control_Applicative['pure']($dictApplicative)($Done()));
    };
    $singleton = function ($dictApplicative)  use (&$prepend, &$nil) {
        return function ($a)  use (&$prepend, &$dictApplicative, &$nil) {
            return $prepend($dictApplicative)($a)($nil($dictApplicative));
        };
    };
    $take = function ($dictApplicative)  use (&$nil, &$Yield, &$PS__Data_Functor, &$PS__Data_Lazy, &$take, &$PS__Data_Ring, &$Skip, &$Done, &$stepMap) {
        return function ($v)  use (&$nil, &$dictApplicative, &$Yield, &$PS__Data_Functor, &$PS__Data_Lazy, &$take, &$PS__Data_Ring, &$Skip, &$Done, &$stepMap) {
            return function ($fa)  use (&$v, &$nil, &$dictApplicative, &$Yield, &$PS__Data_Functor, &$PS__Data_Lazy, &$take, &$PS__Data_Ring, &$Skip, &$Done, &$stepMap) {
                if ($v === 0) {
                    return $nil($dictApplicative);
                };
                $f = function ($v1)  use (&$Yield, &$PS__Data_Functor, &$PS__Data_Lazy, &$take, &$dictApplicative, &$PS__Data_Ring, &$v, &$Skip, &$Done) {
                    if ($v1['type'] === 'Yield') {
                        return $Yield['constructor']($v1['value0'], $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($take($dictApplicative)($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(1)))($v1['value1']));
                    };
                    if ($v1['type'] === 'Skip') {
                        return $Skip['constructor']($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($take($dictApplicative)($v))($v1['value0']));
                    };
                    if ($v1['type'] === 'Done') {
                        return $Done();
                    };
                    throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 147, column 3 - line 147, column 47: ' . var_dump([ $v1['constructor']['name'] ]));
                };
                return $stepMap(($dictApplicative['Apply0']())['Functor0']())($f)($fa);
            };
        };
    };
    $zipWith__prime = function ($dictMonad)  use (&$PS__Control_Applicative, &$nil, &$PS__Data_Functor, &$PS__Data_Function, &$prepend__prime, &$PS__Data_Lazy, &$zipWith__prime, &$wrapEffect, &$PS__Control_Bind, &$uncons) {
        return function ($f)  use (&$PS__Control_Applicative, &$dictMonad, &$nil, &$PS__Data_Functor, &$PS__Data_Function, &$prepend__prime, &$PS__Data_Lazy, &$zipWith__prime, &$wrapEffect, &$PS__Control_Bind, &$uncons) {
            $g = function ($v)  use (&$PS__Control_Applicative, &$dictMonad, &$nil, &$PS__Data_Functor, &$PS__Data_Function, &$prepend__prime, &$PS__Data_Lazy, &$zipWith__prime, &$f) {
                return function ($v1)  use (&$PS__Control_Applicative, &$dictMonad, &$nil, &$v, &$PS__Data_Functor, &$PS__Data_Function, &$prepend__prime, &$PS__Data_Lazy, &$zipWith__prime, &$f) {
                    if ($v1['type'] === 'Nothing') {
                        return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($nil($dictMonad['Applicative0']()));
                    };
                    if ($v['type'] === 'Nothing') {
                        return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($nil($dictMonad['Applicative0']()));
                    };
                    if ($v['type'] === 'Just' && $v1['type'] === 'Just') {
                        return $PS__Data_Functor['map']((($dictMonad['Bind1']())['Apply0']())['Functor0']())($PS__Data_Function['flip']($prepend__prime($dictMonad['Applicative0']()))($PS__Data_Lazy['defer'](function ($v2)  use (&$zipWith__prime, &$dictMonad, &$f, &$v, &$v1) {
                            return $zipWith__prime($dictMonad)($f)($v['value0']['value1'])($v1['value0']['value1']);
                        })))($f($v['value0']['value0'])($v1['value0']['value0']));
                    };
                    throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 260, column 3 - line 260, column 25: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
                };
            };
            $loop = function ($fa)  use (&$wrapEffect, &$dictMonad, &$PS__Control_Bind, &$uncons, &$g) {
                return function ($fb)  use (&$wrapEffect, &$dictMonad, &$PS__Control_Bind, &$uncons, &$fa, &$g) {
                    return $wrapEffect((($dictMonad['Bind1']())['Apply0']())['Functor0']())($PS__Control_Bind['bind']($dictMonad['Bind1']())($uncons($dictMonad)($fa))(function ($v)  use (&$PS__Control_Bind, &$dictMonad, &$uncons, &$fb, &$g) {
                        return $PS__Control_Bind['bind']($dictMonad['Bind1']())($uncons($dictMonad)($fb))(function ($v1)  use (&$g, &$v) {
                            return $g($v)($v1);
                        });
                    }));
                };
            };
            return $loop;
        };
    };
    $zipWith = function ($dictMonad)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$zipWith__prime) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$zipWith__prime) {
            $g = function ($a)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$f) {
                return function ($b)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$f, &$a) {
                    return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($f($a)($b));
                };
            };
            return $zipWith__prime($dictMonad)($g);
        };
    };
    $newtypeListT = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $ListT);
    $mapMaybe = function ($dictFunctor)  use (&$PS__Data_Maybe, &$Skip, &$PS__Data_Functor, &$Yield, &$PS__Data_Lazy, &$mapMaybe, &$PS__Data_Function, &$Done, &$stepMap) {
        return function ($f)  use (&$PS__Data_Maybe, &$Skip, &$PS__Data_Functor, &$Yield, &$PS__Data_Lazy, &$mapMaybe, &$dictFunctor, &$PS__Data_Function, &$Done, &$stepMap) {
            $g = function ($v)  use (&$PS__Data_Maybe, &$Skip, &$PS__Data_Functor, &$Yield, &$f, &$PS__Data_Lazy, &$mapMaybe, &$dictFunctor, &$PS__Data_Function, &$Done) {
                if ($v['type'] === 'Yield') {
                    return $PS__Data_Maybe['fromMaybe']($Skip['create'])($PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])($Yield['create'])($f($v['value0'])))($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($mapMaybe($dictFunctor)($f))($v['value1']));
                };
                if ($v['type'] === 'Skip') {
                    return $PS__Data_Function['apply']($Skip['create'])($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($mapMaybe($dictFunctor)($f))($v['value0']));
                };
                if ($v['type'] === 'Done') {
                    return $Done();
                };
                throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 183, column 3 - line 183, column 72: ' . var_dump([ $v['constructor']['name'] ]));
            };
            return $stepMap($dictFunctor)($g);
        };
    };
    $iterate = function ($dictMonad)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Maybe, &$PS__Data_Tuple, &$unfold) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_Maybe, &$PS__Data_Tuple, &$unfold) {
            return function ($a)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_Maybe, &$PS__Data_Tuple, &$f, &$unfold) {
                $g = function ($x)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_Maybe, &$PS__Data_Tuple, &$f) {
                    return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($PS__Data_Maybe['Just']['constructor']($PS__Data_Tuple['Tuple']['constructor']($f($x), $x)));
                };
                return $unfold($dictMonad)($g)($a);
            };
        };
    };
    $repeat = function ($dictMonad)  use (&$iterate, &$PS__Control_Category) {
        return $iterate($dictMonad)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    };
    $head = function ($dictMonad)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Tuple, &$uncons) {
        return function ($l)  use (&$PS__Data_Functor, &$dictMonad, &$PS__Data_Maybe, &$PS__Data_Tuple, &$uncons) {
            return $PS__Data_Functor['map']((($dictMonad['Bind1']())['Apply0']())['Functor0']())($PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])($PS__Data_Tuple['fst']))($uncons($dictMonad)($l));
        };
    };
    $functorListT = function ($dictFunctor)  use (&$PS__Data_Functor, &$Yield, &$PS__Data_Lazy, &$functorListT, &$Skip, &$Done, &$stepMap) {
        return $PS__Data_Functor['Functor'](function ($f)  use (&$Yield, &$PS__Data_Functor, &$PS__Data_Lazy, &$functorListT, &$dictFunctor, &$Skip, &$Done, &$stepMap) {
            $g = function ($v)  use (&$Yield, &$f, &$PS__Data_Functor, &$PS__Data_Lazy, &$functorListT, &$dictFunctor, &$Skip, &$Done) {
                if ($v['type'] === 'Yield') {
                    return $Yield['constructor']($f($v['value0']), $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($PS__Data_Functor['map']($functorListT($dictFunctor))($f))($v['value1']));
                };
                if ($v['type'] === 'Skip') {
                    return $Skip['constructor']($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($PS__Data_Functor['map']($functorListT($dictFunctor))($f))($v['value0']));
                };
                if ($v['type'] === 'Done') {
                    return $Done();
                };
                throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 280, column 5 - line 280, column 48: ' . var_dump([ $v['constructor']['name'] ]));
            };
            return $stepMap($dictFunctor)($g);
        });
    };
    $fromEffect = function ($dictApplicative)  use (&$PS__Data_Function, &$ListT, &$PS__Data_Functor, &$Yield, &$PS__Data_Lazy, &$nil) {
        return function ($fa)  use (&$PS__Data_Function, &$ListT, &$PS__Data_Functor, &$dictApplicative, &$Yield, &$PS__Data_Lazy, &$nil) {
            return $PS__Data_Function['apply']($ListT)($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Function['flip']($Yield['create'])($PS__Data_Function['apply']($PS__Data_Lazy['defer'])(function ($v)  use (&$nil, &$dictApplicative) {
                return $nil($dictApplicative);
            })))($fa));
        };
    };
    $monadTransListT = $PS__Control_Monad_Trans_Class['MonadTrans'](function ($dictMonad)  use (&$fromEffect) {
        return $fromEffect($dictMonad['Applicative0']());
    });
    $foldlRec__prime = function ($dictMonadRec)  use (&$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$PS__Control_Bind, &$uncons) {
        return function ($f)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$PS__Control_Bind, &$uncons) {
            $loop = function ($b)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$PS__Control_Bind, &$f, &$uncons) {
                return function ($l)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$b, &$PS__Control_Bind, &$f, &$uncons) {
                    $g = function ($v)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$b, &$PS__Control_Bind, &$f) {
                        if ($v['type'] === 'Nothing') {
                            return $PS__Control_Applicative['pure'](($dictMonadRec['Monad0']())['Applicative0']())($PS__Control_Monad_Rec_Class['Done']['constructor']($b));
                        };
                        if ($v['type'] === 'Just') {
                            return $PS__Control_Bind['bind'](($dictMonadRec['Monad0']())['Bind1']())($f($b)($v['value0']['value0']))(function ($b__prime)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$v) {
                                return $PS__Control_Applicative['pure'](($dictMonadRec['Monad0']())['Applicative0']())($PS__Control_Monad_Rec_Class['Loop']['constructor']([
                                    'a' => $b__prime,
                                    'b' => $v['value0']['value1']
                                ]));
                            });
                        };
                        throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 221, column 5 - line 221, column 45: ' . var_dump([ $v['constructor']['name'] ]));
                    };
                    return $PS__Control_Bind['bind'](($dictMonadRec['Monad0']())['Bind1']())($uncons($dictMonadRec['Monad0']())($l))($g);
                };
            };
            return $PS__Control_Monad_Rec_Class['tailRecM2']($dictMonadRec)($loop);
        };
    };
    $runListTRec = function ($dictMonadRec)  use (&$foldlRec__prime, &$PS__Control_Applicative, &$PS__Data_Unit) {
        return $foldlRec__prime($dictMonadRec)(function ($v)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Data_Unit) {
            return function ($v1)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Data_Unit) {
                return $PS__Control_Applicative['pure'](($dictMonadRec['Monad0']())['Applicative0']())($PS__Data_Unit['unit']);
            };
        })($PS__Data_Unit['unit']);
    };
    $foldlRec = function ($dictMonadRec)  use (&$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$PS__Control_Bind, &$uncons) {
        return function ($f)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$PS__Control_Bind, &$uncons) {
            $loop = function ($b)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$f, &$PS__Control_Bind, &$uncons) {
                return function ($l)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$b, &$f, &$PS__Control_Bind, &$uncons) {
                    $g = function ($v)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$b, &$f) {
                        if ($v['type'] === 'Nothing') {
                            return $PS__Control_Applicative['pure'](($dictMonadRec['Monad0']())['Applicative0']())($PS__Control_Monad_Rec_Class['Done']['constructor']($b));
                        };
                        if ($v['type'] === 'Just') {
                            return $PS__Control_Applicative['pure'](($dictMonadRec['Monad0']())['Applicative0']())($PS__Control_Monad_Rec_Class['Loop']['constructor']([
                                'a' => $f($b)($v['value0']['value0']),
                                'b' => $v['value0']['value1']
                            ]));
                        };
                        throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 239, column 7 - line 239, column 47: ' . var_dump([ $v['constructor']['name'] ]));
                    };
                    return $PS__Control_Bind['bind'](($dictMonadRec['Monad0']())['Bind1']())($uncons($dictMonadRec['Monad0']())($l))($g);
                };
            };
            return $PS__Control_Monad_Rec_Class['tailRecM2']($dictMonadRec)($loop);
        };
    };
    $foldl__prime = function ($dictMonad)  use (&$PS__Control_Applicative, &$PS__Control_Bind, &$PS__Data_Function, &$uncons) {
        return function ($f)  use (&$PS__Control_Applicative, &$dictMonad, &$PS__Control_Bind, &$PS__Data_Function, &$uncons) {
            $loop = function ($b)  use (&$PS__Control_Applicative, &$dictMonad, &$PS__Control_Bind, &$f, &$PS__Data_Function, &$loop, &$uncons) {
                return function ($l)  use (&$PS__Control_Applicative, &$dictMonad, &$b, &$PS__Control_Bind, &$f, &$PS__Data_Function, &$loop, &$uncons) {
                    $g = function ($v)  use (&$PS__Control_Applicative, &$dictMonad, &$b, &$PS__Control_Bind, &$f, &$PS__Data_Function, &$loop) {
                        if ($v['type'] === 'Nothing') {
                            return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($b);
                        };
                        if ($v['type'] === 'Just') {
                            return $PS__Control_Bind['bind']($dictMonad['Bind1']())($f($b)($v['value0']['value0']))($PS__Data_Function['flip']($loop)($v['value0']['value1']));
                        };
                        throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 212, column 5 - line 212, column 35: ' . var_dump([ $v['constructor']['name'] ]));
                    };
                    return $PS__Control_Bind['bind']($dictMonad['Bind1']())($uncons($dictMonad)($l))($g);
                };
            };
            return $loop;
        };
    };
    $runListT = function ($dictMonad)  use (&$foldl__prime, &$PS__Control_Applicative, &$PS__Data_Unit) {
        return $foldl__prime($dictMonad)(function ($v)  use (&$PS__Control_Applicative, &$dictMonad, &$PS__Data_Unit) {
            return function ($v1)  use (&$PS__Control_Applicative, &$dictMonad, &$PS__Data_Unit) {
                return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($PS__Data_Unit['unit']);
            };
        })($PS__Data_Unit['unit']);
    };
    $foldl = function ($dictMonad)  use (&$PS__Control_Applicative, &$PS__Control_Bind, &$uncons) {
        return function ($f)  use (&$PS__Control_Applicative, &$dictMonad, &$PS__Control_Bind, &$uncons) {
            $loop = function ($b)  use (&$PS__Control_Applicative, &$dictMonad, &$loop, &$f, &$PS__Control_Bind, &$uncons) {
                return function ($l)  use (&$PS__Control_Applicative, &$dictMonad, &$b, &$loop, &$f, &$PS__Control_Bind, &$uncons) {
                    $g = function ($v)  use (&$PS__Control_Applicative, &$dictMonad, &$b, &$loop, &$f) {
                        if ($v['type'] === 'Nothing') {
                            return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($b);
                        };
                        if ($v['type'] === 'Just') {
                            return $loop($f($b)($v['value0']['value0']))($v['value0']['value1']);
                        };
                        throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 229, column 5 - line 229, column 35: ' . var_dump([ $v['constructor']['name'] ]));
                    };
                    return $PS__Control_Bind['bind']($dictMonad['Bind1']())($uncons($dictMonad)($l))($g);
                };
            };
            return $loop;
        };
    };
    $filter = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Lazy, &$filter, &$Yield, &$Skip, &$Done, &$stepMap) {
        return function ($f)  use (&$PS__Data_Functor, &$PS__Data_Lazy, &$filter, &$dictFunctor, &$Yield, &$Skip, &$Done, &$stepMap) {
            $g = function ($v)  use (&$PS__Data_Functor, &$PS__Data_Lazy, &$filter, &$dictFunctor, &$f, &$Yield, &$Skip, &$Done) {
                if ($v['type'] === 'Yield') {
                    $s__prime = $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($filter($dictFunctor)($f))($v['value1']);
                    $__local_var__154 = $f($v['value0']);
                    if ($__local_var__154) {
                        return $Yield['constructor']($v['value0'], $s__prime);
                    };
                    return $Skip['constructor']($s__prime);
                };
                if ($v['type'] === 'Skip') {
                    $s__prime = $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($filter($dictFunctor)($f))($v['value0']);
                    return $Skip['constructor']($s__prime);
                };
                if ($v['type'] === 'Done') {
                    return $Done();
                };
                throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 176, column 3 - line 176, column 80: ' . var_dump([ $v['constructor']['name'] ]));
            };
            return $stepMap($dictFunctor)($g);
        };
    };
    $dropWhile = function ($dictApplicative)  use (&$Skip, &$PS__Data_Functor, &$PS__Data_Lazy, &$dropWhile, &$Yield, &$PS__Data_Function, &$Done, &$stepMap) {
        return function ($f)  use (&$Skip, &$PS__Data_Functor, &$PS__Data_Lazy, &$dropWhile, &$dictApplicative, &$Yield, &$PS__Data_Function, &$Done, &$stepMap) {
            $g = function ($v)  use (&$f, &$Skip, &$PS__Data_Functor, &$PS__Data_Lazy, &$dropWhile, &$dictApplicative, &$Yield, &$PS__Data_Function, &$Done) {
                if ($v['type'] === 'Yield') {
                    $__local_var__159 = $f($v['value0']);
                    if ($__local_var__159) {
                        return $Skip['constructor']($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($dropWhile($dictApplicative)($f))($v['value1']));
                    };
                    return $Yield['constructor']($v['value0'], $v['value1']);
                };
                if ($v['type'] === 'Skip') {
                    return $PS__Data_Function['apply']($Skip['create'])($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($dropWhile($dictApplicative)($f))($v['value0']));
                };
                if ($v['type'] === 'Done') {
                    return $Done();
                };
                throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 169, column 3 - line 169, column 70: ' . var_dump([ $v['constructor']['name'] ]));
            };
            return $stepMap(($dictApplicative['Apply0']())['Functor0']())($g);
        };
    };
    $drop = function ($dictApplicative)  use (&$Skip, &$PS__Data_Functor, &$PS__Data_Lazy, &$drop, &$PS__Data_Ring, &$Done, &$stepMap) {
        return function ($v)  use (&$Skip, &$PS__Data_Functor, &$PS__Data_Lazy, &$drop, &$dictApplicative, &$PS__Data_Ring, &$Done, &$stepMap) {
            return function ($fa)  use (&$v, &$Skip, &$PS__Data_Functor, &$PS__Data_Lazy, &$drop, &$dictApplicative, &$PS__Data_Ring, &$Done, &$stepMap) {
                if ($v === 0) {
                    return $fa;
                };
                $f = function ($v1)  use (&$Skip, &$PS__Data_Functor, &$PS__Data_Lazy, &$drop, &$dictApplicative, &$PS__Data_Ring, &$v, &$Done) {
                    if ($v1['type'] === 'Yield') {
                        return $Skip['constructor']($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($drop($dictApplicative)($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(1)))($v1['value1']));
                    };
                    if ($v1['type'] === 'Skip') {
                        return $Skip['constructor']($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($drop($dictApplicative)($v))($v1['value0']));
                    };
                    if ($v1['type'] === 'Done') {
                        return $Done();
                    };
                    throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 162, column 3 - line 162, column 44: ' . var_dump([ $v1['constructor']['name'] ]));
                };
                return $stepMap(($dictApplicative['Apply0']())['Functor0']())($f)($fa);
            };
        };
    };
    $cons = function ($dictApplicative)  use (&$PS__Data_Function, &$ListT, &$PS__Control_Applicative, &$Yield, &$PS__Data_Lazy) {
        return function ($lh)  use (&$PS__Data_Function, &$ListT, &$PS__Control_Applicative, &$dictApplicative, &$Yield, &$PS__Data_Lazy) {
            return function ($t)  use (&$PS__Data_Function, &$ListT, &$PS__Control_Applicative, &$dictApplicative, &$Yield, &$PS__Data_Lazy, &$lh) {
                return $PS__Data_Function['apply']($ListT)($PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictApplicative))($Yield['constructor']($PS__Data_Lazy['force']($lh), $t)));
            };
        };
    };
    $unfoldable1ListT = function ($dictMonad)  use (&$PS__Data_Unfoldable1, &$singleton, &$cons, &$PS__Control_Applicative, &$PS__Data_Lazy) {
        return $PS__Data_Unfoldable1['Unfoldable1'](function ($f)  use (&$singleton, &$dictMonad, &$cons, &$PS__Control_Applicative, &$PS__Data_Lazy) {
            return function ($b)  use (&$singleton, &$dictMonad, &$cons, &$PS__Control_Applicative, &$PS__Data_Lazy, &$f) {
                $go = function ($v)  use (&$singleton, &$dictMonad, &$cons, &$PS__Control_Applicative, &$PS__Data_Lazy, &$go, &$f) {
                    if ($v['value1']['type'] === 'Nothing') {
                        return $singleton($dictMonad['Applicative0']())($v['value0']);
                    };
                    if ($v['value1']['type'] === 'Just') {
                        return $cons($dictMonad['Applicative0']())($PS__Control_Applicative['pure']($PS__Data_Lazy['applicativeLazy'])($v['value0']))($PS__Data_Lazy['defer'](function ($v1)  use (&$go, &$f, &$v) {
                            return $go($f($v['value1']['value0']));
                        }));
                    };
                    throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 294, column 12 - line 296, column 67: ' . var_dump([ $v['constructor']['name'] ]));
                };
                return $go($f($b));
            };
        });
    };
    $unfoldableListT = function ($dictMonad)  use (&$PS__Data_Unfoldable, &$unfoldable1ListT, &$nil, &$cons, &$PS__Control_Applicative, &$PS__Data_Lazy) {
        return $PS__Data_Unfoldable['Unfoldable'](function ()  use (&$unfoldable1ListT, &$dictMonad) {
            return $unfoldable1ListT($dictMonad);
        }, function ($f)  use (&$nil, &$dictMonad, &$cons, &$PS__Control_Applicative, &$PS__Data_Lazy) {
            return function ($b)  use (&$nil, &$dictMonad, &$cons, &$PS__Control_Applicative, &$PS__Data_Lazy, &$f) {
                $go = function ($v)  use (&$nil, &$dictMonad, &$cons, &$PS__Control_Applicative, &$PS__Data_Lazy, &$go, &$f) {
                    if ($v['type'] === 'Nothing') {
                        return $nil($dictMonad['Applicative0']());
                    };
                    if ($v['type'] === 'Just') {
                        return $cons($dictMonad['Applicative0']())($PS__Control_Applicative['pure']($PS__Data_Lazy['applicativeLazy'])($v['value0']['value0']))($PS__Data_Lazy['defer'](function ($v1)  use (&$go, &$f, &$v) {
                            return $go($f($v['value0']['value1']));
                        }));
                    };
                    throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 287, column 12 - line 289, column 67: ' . var_dump([ $v['constructor']['name'] ]));
                };
                return $go($f($b));
            };
        });
    };
    $semigroupListT = function ($dictApplicative)  use (&$PS__Data_Semigroup, &$concat) {
        return $PS__Data_Semigroup['Semigroup']($concat($dictApplicative));
    };
    $concat = function ($dictApplicative)  use (&$Yield, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Semigroup, &$semigroupListT, &$Skip, &$PS__Data_Function, &$stepMap) {
        return function ($x)  use (&$Yield, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Semigroup, &$semigroupListT, &$dictApplicative, &$Skip, &$PS__Data_Function, &$stepMap) {
            return function ($y)  use (&$Yield, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Semigroup, &$semigroupListT, &$dictApplicative, &$Skip, &$PS__Data_Function, &$stepMap, &$x) {
                $f = function ($v)  use (&$Yield, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Semigroup, &$semigroupListT, &$dictApplicative, &$y, &$Skip, &$PS__Data_Function) {
                    if ($v['type'] === 'Yield') {
                        return $Yield['constructor']($v['value0'], $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])(function ($v1)  use (&$PS__Data_Semigroup, &$semigroupListT, &$dictApplicative, &$y) {
                            return $PS__Data_Semigroup['append']($semigroupListT($dictApplicative))($v1)($y);
                        })($v['value1']));
                    };
                    if ($v['type'] === 'Skip') {
                        return $Skip['constructor']($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])(function ($v1)  use (&$PS__Data_Semigroup, &$semigroupListT, &$dictApplicative, &$y) {
                            return $PS__Data_Semigroup['append']($semigroupListT($dictApplicative))($v1)($y);
                        })($v['value0']));
                    };
                    if ($v['type'] === 'Done') {
                        return $Skip['constructor']($PS__Data_Function['apply']($PS__Data_Lazy['defer'])($PS__Data_Function['const']($y)));
                    };
                    throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 106, column 3 - line 106, column 43: ' . var_dump([ $v['constructor']['name'] ]));
                };
                return $stepMap(($dictApplicative['Apply0']())['Functor0']())($f)($x);
            };
        };
    };
    $monoidListT = function ($dictApplicative)  use (&$PS__Data_Monoid, &$semigroupListT, &$nil) {
        return $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupListT, &$dictApplicative) {
            return $semigroupListT($dictApplicative);
        }, $nil($dictApplicative));
    };
    $catMaybes = function ($dictFunctor)  use (&$mapMaybe, &$PS__Control_Category) {
        return $mapMaybe($dictFunctor)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    };
    $monadListT = function ($dictMonad)  use (&$PS__Control_Monad, &$applicativeListT, &$bindListT) {
        return $PS__Control_Monad['Monad'](function ()  use (&$applicativeListT, &$dictMonad) {
            return $applicativeListT($dictMonad);
        }, function ()  use (&$bindListT, &$dictMonad) {
            return $bindListT($dictMonad);
        });
    };
    $bindListT = function ($dictMonad)  use (&$PS__Control_Bind, &$applyListT, &$PS__Data_Semigroup, &$semigroupListT, &$bindListT, &$Skip, &$PS__Data_Functor, &$PS__Data_Lazy, &$Done, &$stepMap) {
        return $PS__Control_Bind['Bind'](function ()  use (&$applyListT, &$dictMonad) {
            return $applyListT($dictMonad);
        }, function ($fa)  use (&$PS__Data_Semigroup, &$semigroupListT, &$dictMonad, &$PS__Control_Bind, &$bindListT, &$Skip, &$PS__Data_Functor, &$PS__Data_Lazy, &$Done, &$stepMap) {
            return function ($f)  use (&$PS__Data_Semigroup, &$semigroupListT, &$dictMonad, &$PS__Control_Bind, &$bindListT, &$Skip, &$PS__Data_Functor, &$PS__Data_Lazy, &$Done, &$stepMap, &$fa) {
                $g = function ($v)  use (&$PS__Data_Semigroup, &$semigroupListT, &$dictMonad, &$f, &$PS__Control_Bind, &$bindListT, &$Skip, &$PS__Data_Functor, &$PS__Data_Lazy, &$Done) {
                    if ($v['type'] === 'Yield') {
                        $h = function ($s__prime)  use (&$PS__Data_Semigroup, &$semigroupListT, &$dictMonad, &$f, &$v, &$PS__Control_Bind, &$bindListT) {
                            return $PS__Data_Semigroup['append']($semigroupListT($dictMonad['Applicative0']()))($f($v['value0']))($PS__Control_Bind['bind']($bindListT($dictMonad))($s__prime)($f));
                        };
                        return $Skip['constructor']($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($h)($v['value1']));
                    };
                    if ($v['type'] === 'Skip') {
                        return $Skip['constructor']($PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])(function ($v1)  use (&$PS__Control_Bind, &$bindListT, &$dictMonad, &$f) {
                            return $PS__Control_Bind['bind']($bindListT($dictMonad))($v1)($f);
                        })($v['value0']));
                    };
                    if ($v['type'] === 'Done') {
                        return $Done();
                    };
                    throw new \Exception('Failed pattern match at Control.Monad.List.Trans line 306, column 5 - line 308, column 31: ' . var_dump([ $v['constructor']['name'] ]));
                };
                return $stepMap((($dictMonad['Bind1']())['Apply0']())['Functor0']())($g)($fa);
            };
        });
    };
    $applyListT = function ($dictMonad)  use (&$PS__Control_Apply, &$functorListT, &$PS__Control_Monad, &$monadListT) {
        return $PS__Control_Apply['Apply'](function ()  use (&$functorListT, &$dictMonad) {
            return $functorListT((($dictMonad['Bind1']())['Apply0']())['Functor0']());
        }, $PS__Control_Monad['ap']($monadListT($dictMonad)));
    };
    $applicativeListT = function ($dictMonad)  use (&$PS__Control_Applicative, &$applyListT, &$singleton) {
        return $PS__Control_Applicative['Applicative'](function ()  use (&$applyListT, &$dictMonad) {
            return $applyListT($dictMonad);
        }, $singleton($dictMonad['Applicative0']()));
    };
    $monadEffectListT = function ($dictMonadEffect)  use (&$PS__Effect_Class, &$monadListT, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransListT) {
        return $PS__Effect_Class['MonadEffect'](function ()  use (&$monadListT, &$dictMonadEffect) {
            return $monadListT($dictMonadEffect['Monad0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Trans_Class['lift']($monadTransListT)($dictMonadEffect['Monad0']()))($PS__Effect_Class['liftEffect']($dictMonadEffect)));
    };
    $altListT = function ($dictApplicative)  use (&$PS__Control_Alt, &$functorListT, &$concat) {
        return $PS__Control_Alt['Alt'](function ()  use (&$functorListT, &$dictApplicative) {
            return $functorListT(($dictApplicative['Apply0']())['Functor0']());
        }, $concat($dictApplicative));
    };
    $plusListT = function ($dictMonad)  use (&$PS__Control_Plus, &$altListT, &$nil) {
        return $PS__Control_Plus['Plus'](function ()  use (&$altListT, &$dictMonad) {
            return $altListT($dictMonad['Applicative0']());
        }, $nil($dictMonad['Applicative0']()));
    };
    $alternativeListT = function ($dictMonad)  use (&$PS__Control_Alternative, &$applicativeListT, &$plusListT) {
        return $PS__Control_Alternative['Alternative'](function ()  use (&$applicativeListT, &$dictMonad) {
            return $applicativeListT($dictMonad);
        }, function ()  use (&$plusListT, &$dictMonad) {
            return $plusListT($dictMonad);
        });
    };
    $monadZeroListT = function ($dictMonad)  use (&$PS__Control_MonadZero, &$alternativeListT, &$monadListT) {
        return $PS__Control_MonadZero['MonadZero'](function ()  use (&$alternativeListT, &$dictMonad) {
            return $alternativeListT($dictMonad);
        }, function ()  use (&$monadListT, &$dictMonad) {
            return $monadListT($dictMonad);
        });
    };
    $monadPlusListT = function ($dictMonad)  use (&$PS__Control_MonadPlus, &$monadZeroListT) {
        return $PS__Control_MonadPlus['MonadPlus'](function ()  use (&$monadZeroListT, &$dictMonad) {
            return $monadZeroListT($dictMonad);
        });
    };
    return [
        'ListT' => $ListT,
        'Yield' => $Yield,
        'Skip' => $Skip,
        'Done' => $Done,
        'catMaybes' => $catMaybes,
        'cons' => $cons,
        'drop' => $drop,
        'dropWhile' => $dropWhile,
        'filter' => $filter,
        'foldl' => $foldl,
        'foldlRec' => $foldlRec,
        'foldl\'' => $foldl__prime,
        'foldlRec\'' => $foldlRec__prime,
        'fromEffect' => $fromEffect,
        'head' => $head,
        'iterate' => $iterate,
        'mapMaybe' => $mapMaybe,
        'nil' => $nil,
        'prepend' => $prepend,
        'prepend\'' => $prepend__prime,
        'repeat' => $repeat,
        'runListT' => $runListT,
        'runListTRec' => $runListTRec,
        'scanl' => $scanl,
        'singleton' => $singleton,
        'tail' => $tail,
        'take' => $take,
        'takeWhile' => $takeWhile,
        'uncons' => $uncons,
        'unfold' => $unfold,
        'wrapEffect' => $wrapEffect,
        'wrapLazy' => $wrapLazy,
        'zipWith' => $zipWith,
        'zipWith\'' => $zipWith__prime,
        'newtypeListT' => $newtypeListT,
        'semigroupListT' => $semigroupListT,
        'monoidListT' => $monoidListT,
        'functorListT' => $functorListT,
        'unfoldableListT' => $unfoldableListT,
        'unfoldable1ListT' => $unfoldable1ListT,
        'applyListT' => $applyListT,
        'applicativeListT' => $applicativeListT,
        'bindListT' => $bindListT,
        'monadListT' => $monadListT,
        'monadTransListT' => $monadTransListT,
        'altListT' => $altListT,
        'plusListT' => $plusListT,
        'alternativeListT' => $alternativeListT,
        'monadZeroListT' => $monadZeroListT,
        'monadPlusListT' => $monadPlusListT,
        'monadEffectListT' => $monadEffectListT
    ];
})();
