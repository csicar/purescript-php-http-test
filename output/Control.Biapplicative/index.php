<?php
require_once(dirname(__FILE__) . '/../Control.Biapply/index.php');
$PS__Control_Biapplicative = (function () {
    $Biapplicative = function ($Biapply0, $bipure) {
        return [
            'Biapply0' => $Biapply0,
            'bipure' => $bipure
        ];
    };
    $bipure = function ($dict) {
        return $dict['bipure'];
    };
    return [
        'bipure' => $bipure,
        'Biapplicative' => $Biapplicative
    ];
})();
