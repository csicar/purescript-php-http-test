<?php

$exports['empty'] = function () {
  return [];
};

$exports['peekImpl'] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($i) use (&$just, &$nothing) {
      return function ($xs) use (&$just, &$nothing, &$i) {
        return function () use (&$just, &$nothing, &$i, &$xs) {
          return $i >= 0 && $i < count($xs) ? $just($xs[$i]) : $nothing;
        };
      };
    };
  };
};

$exports['poke'] = function ($i) {
  return function ($a) use (&$i) {
    return function ($xs) use (&$i, &$a) {
      return function () use (&$i, &$a, &$xs) {
        $ret = $i >= 0 && $i < count($xs);
        if ($ret) $xs[$i] = $a;
        return $ret;
      };
    };
  };
};

$exports['pushAll'] = function ($as) {
  return function ($xs) use (&$as) {
    return function () use (&$as, &$xs) {
      return array_push($xs, ... $as);
    };
  };
};

$exports['splice'] = function ($i) {
  return function ($howMany) use (&$i) {
    return function ($bs) use (&$howMany, &$i) {
      return function ($xs) use (&$howMany, &$i, &$bs) {
        return function () use (&$howMany, &$i, &$bs, &$xs) {
          return array_splice($xs, $i, $howMany, ... $bs);
        };
      };
    };
  };
};

$exports['copyImpl'] = function ($xs) {
  return function () use (&$xs) {
    return array_merge($xs);
  };
};

$exports['sortByImpl'] = function ($comp) {
  return function ($xs) use (&$comp) {
    return function () use (&$comp, &$xs) {
      return usort($xs, function($x, $y) use (&$comp) {
        return $comp($x)($y);
      });
    };
  };
};

$exports['toAssocArray'] = function ($xs) {
  return function () use (&$xs) {
    $n = count($xs);
    $as = [];
    for ($i = 0; $i < $n; $i++) $as[$i] = [ 'value'=> $xs[$i], 'index' => $i ];
    return $as;
  };
};
