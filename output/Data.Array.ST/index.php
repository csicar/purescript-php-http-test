<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.ST/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.ST.Internal/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
require_once(dirname(__FILE__) . '/../Unsafe.Coerce/index.php');
$PS__Data_Array_ST = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Control_Applicative, &$PS__Control_Monad_ST_Internal, &$PS__Unsafe_Coerce, &$PS__Control_Bind, &$PS__Data_Ring, &$PS__Data_Ord, &$PS__Data_Maybe) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $unsafeThaw = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Applicative['pure']($PS__Control_Monad_ST_Internal['applicativeST']))($PS__Unsafe_Coerce['unsafeCoerce']);
    $unsafeFreeze = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Applicative['pure']($PS__Control_Monad_ST_Internal['applicativeST']))($PS__Unsafe_Coerce['unsafeCoerce']);
    $thaw = $__foreign['copyImpl'];
    $withArray = function ($f)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$thaw, &$unsafeFreeze) {
        return function ($xs)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$thaw, &$f, &$unsafeFreeze) {
            return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($thaw($xs))(function ($v)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$f, &$unsafeFreeze) {
                return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($f($v))(function ($v1)  use (&$unsafeFreeze, &$v) {
                    return $unsafeFreeze($v);
                });
            });
        };
    };
    $sortBy = function ($comp)  use (&$PS__Data_Ring, &$__foreign) {
        $comp__prime = function ($x)  use (&$comp, &$PS__Data_Ring) {
            return function ($y)  use (&$comp, &$x, &$PS__Data_Ring) {
                $v = $comp($x)($y);
                if ($v['type'] === 'GT') {
                    return 1;
                };
                if ($v['type'] === 'EQ') {
                    return 0;
                };
                if ($v['type'] === 'LT') {
                    return $PS__Data_Ring['negate']($PS__Data_Ring['ringInt'])(1);
                };
                throw new \Exception('Failed pattern match at Data.Array.ST line 85, column 15 - line 90, column 1: ' . var_dump([ $v['constructor']['name'] ]));
            };
        };
        return $__foreign['sortByImpl']($comp__prime);
    };
    $sortWith = function ($dictOrd)  use (&$sortBy, &$PS__Data_Ord) {
        return function ($f)  use (&$sortBy, &$PS__Data_Ord, &$dictOrd) {
            return $sortBy($PS__Data_Ord['comparing']($dictOrd)($f));
        };
    };
    $sort = function ($dictOrd)  use (&$sortBy, &$PS__Data_Ord) {
        return $sortBy($PS__Data_Ord['compare']($dictOrd));
    };
    $push = function ($a)  use (&$__foreign) {
        return $__foreign['pushAll']([ $a ]);
    };
    $peek = $__foreign['peekImpl']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $modify = function ($i)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$peek, &$__foreign, &$PS__Control_Applicative) {
        return function ($f)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$peek, &$i, &$__foreign, &$PS__Control_Applicative) {
            return function ($xs)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$peek, &$i, &$__foreign, &$f, &$PS__Control_Applicative) {
                return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($peek($i)($xs))(function ($v)  use (&$__foreign, &$i, &$f, &$xs, &$PS__Control_Applicative, &$PS__Control_Monad_ST_Internal) {
                    if ($v['type'] === 'Just') {
                        return $__foreign['poke']($i)($f($v['value0']))($xs);
                    };
                    if ($v['type'] === 'Nothing') {
                        return $PS__Control_Applicative['pure']($PS__Control_Monad_ST_Internal['applicativeST'])(false);
                    };
                    throw new \Exception('Failed pattern match at Data.Array.ST line 147, column 3 - line 149, column 26: ' . var_dump([ $v['constructor']['name'] ]));
                });
            };
        };
    };
    $freeze = $__foreign['copyImpl'];
    return [
        'withArray' => $withArray,
        'peek' => $peek,
        'push' => $push,
        'modify' => $modify,
        'sort' => $sort,
        'sortBy' => $sortBy,
        'sortWith' => $sortWith,
        'freeze' => $freeze,
        'thaw' => $thaw,
        'unsafeFreeze' => $unsafeFreeze,
        'unsafeThaw' => $unsafeThaw,
        'empty' => $__foreign['empty'],
        'poke' => $__foreign['poke'],
        'pushAll' => $__foreign['pushAll'],
        'splice' => $__foreign['splice'],
        'toAssocArray' => $__foreign['toAssocArray']
    ];
})();
