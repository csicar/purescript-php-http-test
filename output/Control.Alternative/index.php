<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
$PS__Control_Alternative = (function ()  use (&$PS__Control_Applicative, &$PS__Control_Plus) {
    $Alternative = function ($Applicative0, $Plus1) {
        return [
            'Applicative0' => $Applicative0,
            'Plus1' => $Plus1
        ];
    };
    $alternativeArray = $Alternative(function ()  use (&$PS__Control_Applicative) {
        return $PS__Control_Applicative['applicativeArray'];
    }, function ()  use (&$PS__Control_Plus) {
        return $PS__Control_Plus['plusArray'];
    });
    return [
        'Alternative' => $Alternative,
        'alternativeArray' => $alternativeArray
    ];
})();
