<?php
require_once(dirname(__FILE__) . '/../Data.List/index.php');
require_once(dirname(__FILE__) . '/../Data.List.Types/index.php');
$PS__Data_List_Partial = (function ()  use (&$PS__Data_List_Types) {
    $tail = function ($dictPartial) {
        return function ($v)  use (&$dictPartial) {
            $___unused = function ($dictPartial1) {
                return function ($__local_var__5) {
                    return $__local_var__5;
                };
            };
            return $___unused($dictPartial)((function ()  use (&$v) {
                if ($v['type'] === 'Cons') {
                    return $v['value1'];
                };
                throw new \Exception('Failed pattern match at Data.List.Partial line 15, column 1 - line 15, column 46: ' . var_dump([ $v['constructor']['name'] ]));
            })());
        };
    };
    $last = function ($dictPartial)  use (&$last) {
        return function ($v)  use (&$dictPartial, &$last) {
            $___unused = function ($dictPartial1) {
                return function ($__local_var__9) {
                    return $__local_var__9;
                };
            };
            return $___unused($dictPartial)((function ()  use (&$v, &$last, &$dictPartial) {
                if ($v['type'] === 'Cons' && $v['value1']['type'] === 'Nil') {
                    return $v['value0'];
                };
                if ($v['type'] === 'Cons') {
                    return $last($dictPartial)($v['value1']);
                };
                throw new \Exception('Failed pattern match at Data.List.Partial line 21, column 1 - line 21, column 41: ' . var_dump([ $v['constructor']['name'] ]));
            })());
        };
    };
    $init = function ($dictPartial)  use (&$PS__Data_List_Types, &$init) {
        return function ($v)  use (&$dictPartial, &$PS__Data_List_Types, &$init) {
            $___unused = function ($dictPartial1) {
                return function ($__local_var__13) {
                    return $__local_var__13;
                };
            };
            return $___unused($dictPartial)((function ()  use (&$v, &$PS__Data_List_Types, &$init, &$dictPartial) {
                if ($v['type'] === 'Cons' && $v['value1']['type'] === 'Nil') {
                    return $PS__Data_List_Types['Nil']();
                };
                if ($v['type'] === 'Cons') {
                    return $PS__Data_List_Types['Cons']['constructor']($v['value0'], $init($dictPartial)($v['value1']));
                };
                throw new \Exception('Failed pattern match at Data.List.Partial line 28, column 1 - line 28, column 46: ' . var_dump([ $v['constructor']['name'] ]));
            })());
        };
    };
    $head = function ($dictPartial) {
        return function ($v)  use (&$dictPartial) {
            $___unused = function ($dictPartial1) {
                return function ($__local_var__17) {
                    return $__local_var__17;
                };
            };
            return $___unused($dictPartial)((function ()  use (&$v) {
                if ($v['type'] === 'Cons') {
                    return $v['value0'];
                };
                throw new \Exception('Failed pattern match at Data.List.Partial line 9, column 1 - line 9, column 41: ' . var_dump([ $v['constructor']['name'] ]));
            })());
        };
    };
    return [
        'head' => $head,
        'tail' => $tail,
        'last' => $last,
        'init' => $init
    ];
})();
