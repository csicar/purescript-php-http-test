<?php
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Gen/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Gen.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Data.Char.Gen/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.String.CodeUnits/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_String_Gen = (function ()  use (&$PS__Control_Monad_Gen_Class, &$PS__Control_Bind, &$PS__Data_Ord, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_String_CodeUnits, &$PS__Control_Monad_Gen, &$PS__Data_Unfoldable, &$PS__Data_Char_Gen) {
    $genString = function ($dictMonadRec)  use (&$PS__Control_Monad_Gen_Class, &$PS__Control_Bind, &$PS__Data_Ord, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_String_CodeUnits, &$PS__Control_Monad_Gen, &$PS__Data_Unfoldable) {
        return function ($dictMonadGen)  use (&$PS__Control_Monad_Gen_Class, &$PS__Control_Bind, &$PS__Data_Ord, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_String_CodeUnits, &$PS__Control_Monad_Gen, &$dictMonadRec, &$PS__Data_Unfoldable) {
            return function ($genChar)  use (&$PS__Control_Monad_Gen_Class, &$dictMonadGen, &$PS__Control_Bind, &$PS__Data_Ord, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_String_CodeUnits, &$PS__Control_Monad_Gen, &$dictMonadRec, &$PS__Data_Unfoldable) {
                return $PS__Control_Monad_Gen_Class['sized']($dictMonadGen)(function ($size)  use (&$PS__Control_Bind, &$dictMonadGen, &$PS__Control_Monad_Gen_Class, &$PS__Data_Ord, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_String_CodeUnits, &$PS__Control_Monad_Gen, &$dictMonadRec, &$PS__Data_Unfoldable, &$genChar) {
                    return $PS__Control_Bind['bind'](($dictMonadGen['Monad0']())['Bind1']())($PS__Control_Monad_Gen_Class['chooseInt']($dictMonadGen)(1)($PS__Data_Ord['max']($PS__Data_Ord['ordInt'])(1)($size)))(function ($v)  use (&$PS__Data_Function, &$PS__Control_Monad_Gen_Class, &$dictMonadGen, &$PS__Data_Functor, &$PS__Data_String_CodeUnits, &$PS__Control_Monad_Gen, &$dictMonadRec, &$PS__Data_Unfoldable, &$genChar) {
                        return $PS__Data_Function['apply']($PS__Control_Monad_Gen_Class['resize']($dictMonadGen)($PS__Data_Function['const']($v)))($PS__Data_Functor['map'](((($dictMonadGen['Monad0']())['Bind1']())['Apply0']())['Functor0']())($PS__Data_String_CodeUnits['fromCharArray'])($PS__Control_Monad_Gen['unfoldable']($dictMonadRec)($dictMonadGen)($PS__Data_Unfoldable['unfoldableArray'])($genChar)));
                    });
                });
            };
        };
    };
    $genUnicodeString = function ($dictMonadRec)  use (&$genString, &$PS__Data_Char_Gen) {
        return function ($dictMonadGen)  use (&$genString, &$dictMonadRec, &$PS__Data_Char_Gen) {
            return $genString($dictMonadRec)($dictMonadGen)($PS__Data_Char_Gen['genUnicodeChar']($dictMonadGen));
        };
    };
    $genDigitString = function ($dictMonadRec)  use (&$genString, &$PS__Data_Char_Gen) {
        return function ($dictMonadGen)  use (&$genString, &$dictMonadRec, &$PS__Data_Char_Gen) {
            return $genString($dictMonadRec)($dictMonadGen)($PS__Data_Char_Gen['genDigitChar']($dictMonadGen));
        };
    };
    $genAsciiString__prime = function ($dictMonadRec)  use (&$genString, &$PS__Data_Char_Gen) {
        return function ($dictMonadGen)  use (&$genString, &$dictMonadRec, &$PS__Data_Char_Gen) {
            return $genString($dictMonadRec)($dictMonadGen)($PS__Data_Char_Gen['genAsciiChar\'']($dictMonadGen));
        };
    };
    $genAsciiString = function ($dictMonadRec)  use (&$genString, &$PS__Data_Char_Gen) {
        return function ($dictMonadGen)  use (&$genString, &$dictMonadRec, &$PS__Data_Char_Gen) {
            return $genString($dictMonadRec)($dictMonadGen)($PS__Data_Char_Gen['genAsciiChar']($dictMonadGen));
        };
    };
    $genAlphaUppercaseString = function ($dictMonadRec)  use (&$genString, &$PS__Data_Char_Gen) {
        return function ($dictMonadGen)  use (&$genString, &$dictMonadRec, &$PS__Data_Char_Gen) {
            return $genString($dictMonadRec)($dictMonadGen)($PS__Data_Char_Gen['genAlphaUppercase']($dictMonadGen));
        };
    };
    $genAlphaString = function ($dictMonadRec)  use (&$genString, &$PS__Data_Char_Gen) {
        return function ($dictMonadGen)  use (&$genString, &$dictMonadRec, &$PS__Data_Char_Gen) {
            return $genString($dictMonadRec)($dictMonadGen)($PS__Data_Char_Gen['genAlpha']($dictMonadGen));
        };
    };
    $genAlphaLowercaseString = function ($dictMonadRec)  use (&$genString, &$PS__Data_Char_Gen) {
        return function ($dictMonadGen)  use (&$genString, &$dictMonadRec, &$PS__Data_Char_Gen) {
            return $genString($dictMonadRec)($dictMonadGen)($PS__Data_Char_Gen['genAlphaLowercase']($dictMonadGen));
        };
    };
    return [
        'genString' => $genString,
        'genUnicodeString' => $genUnicodeString,
        'genAsciiString' => $genAsciiString,
        'genAsciiString\'' => $genAsciiString__prime,
        'genDigitString' => $genDigitString,
        'genAlphaString' => $genAlphaString,
        'genAlphaLowercaseString' => $genAlphaLowercaseString,
        'genAlphaUppercaseString' => $genAlphaUppercaseString
    ];
})();
