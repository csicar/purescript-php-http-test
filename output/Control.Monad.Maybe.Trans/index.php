<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Cont.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Error.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Reader.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.State.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Trans.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Writer.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadPlus/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadZero/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Effect.Class/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Maybe_Trans = (function ()  use (&$PS__Data_Newtype, &$PS__Control_Monad_Trans_Class, &$PS__Control_Semigroupoid, &$PS__Control_Monad, &$PS__Data_Maybe, &$PS__Data_Functor, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Control_Apply, &$PS__Control_Monad_Reader_Class, &$PS__Control_Monad_Cont_Class, &$PS__Data_Function, &$PS__Effect_Class, &$PS__Control_Monad_Rec_Class, &$PS__Control_Monad_State_Class, &$PS__Control_Monad_Writer_Class, &$PS__Data_Tuple, &$PS__Control_Category, &$PS__Control_Monad_Error_Class, &$PS__Control_Alt, &$PS__Control_Plus, &$PS__Control_Alternative, &$PS__Control_MonadZero, &$PS__Control_MonadPlus) {
    $MaybeT = function ($x) {
        return $x;
    };
    $runMaybeT = function ($v) {
        return $v;
    };
    $newtypeMaybeT = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $MaybeT);
    $monadTransMaybeT = $PS__Control_Monad_Trans_Class['MonadTrans'](function ($dictMonad)  use (&$PS__Control_Semigroupoid, &$MaybeT, &$PS__Control_Monad, &$PS__Data_Maybe) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($MaybeT)($PS__Control_Monad['liftM1']($dictMonad)($PS__Data_Maybe['Just']['create']));
    });
    $mapMaybeT = function ($f) {
        return function ($v)  use (&$f) {
            return $f($v);
        };
    };
    $functorMaybeT = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Maybe) {
        return $PS__Data_Functor['Functor'](function ($f)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Maybe) {
            return function ($v)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Maybe, &$f) {
                return $PS__Data_Functor['map']($dictFunctor)($PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])($f))($v);
            };
        });
    };
    $monadMaybeT = function ($dictMonad)  use (&$PS__Control_Monad, &$applicativeMaybeT, &$bindMaybeT) {
        return $PS__Control_Monad['Monad'](function ()  use (&$applicativeMaybeT, &$dictMonad) {
            return $applicativeMaybeT($dictMonad);
        }, function ()  use (&$bindMaybeT, &$dictMonad) {
            return $bindMaybeT($dictMonad);
        });
    };
    $bindMaybeT = function ($dictMonad)  use (&$PS__Control_Bind, &$applyMaybeT, &$PS__Control_Applicative, &$PS__Data_Maybe) {
        return $PS__Control_Bind['Bind'](function ()  use (&$applyMaybeT, &$dictMonad) {
            return $applyMaybeT($dictMonad);
        }, function ($v)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Control_Applicative, &$PS__Data_Maybe) {
            return function ($f)  use (&$PS__Control_Bind, &$dictMonad, &$v, &$PS__Control_Applicative, &$PS__Data_Maybe) {
                return $PS__Control_Bind['bind']($dictMonad['Bind1']())($v)(function ($v1)  use (&$PS__Control_Applicative, &$dictMonad, &$PS__Data_Maybe, &$f) {
                    if ($v1['type'] === 'Nothing') {
                        return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($PS__Data_Maybe['Nothing']());
                    };
                    if ($v1['type'] === 'Just') {
                        $v2 = $f($v1['value0']);
                        return $v2;
                    };
                    throw new \Exception('Failed pattern match at Control.Monad.Maybe.Trans line 54, column 11 - line 56, column 42: ' . var_dump([ $v1['constructor']['name'] ]));
                });
            };
        });
    };
    $applyMaybeT = function ($dictMonad)  use (&$PS__Control_Apply, &$functorMaybeT, &$PS__Control_Monad, &$monadMaybeT) {
        return $PS__Control_Apply['Apply'](function ()  use (&$functorMaybeT, &$dictMonad) {
            return $functorMaybeT((($dictMonad['Bind1']())['Apply0']())['Functor0']());
        }, $PS__Control_Monad['ap']($monadMaybeT($dictMonad)));
    };
    $applicativeMaybeT = function ($dictMonad)  use (&$PS__Control_Applicative, &$applyMaybeT, &$PS__Control_Semigroupoid, &$MaybeT, &$PS__Data_Maybe) {
        return $PS__Control_Applicative['Applicative'](function ()  use (&$applyMaybeT, &$dictMonad) {
            return $applyMaybeT($dictMonad);
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($MaybeT)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($PS__Data_Maybe['Just']['create'])));
    };
    $monadAskMaybeT = function ($dictMonadAsk)  use (&$PS__Control_Monad_Reader_Class, &$monadMaybeT, &$PS__Control_Monad_Trans_Class, &$monadTransMaybeT) {
        return $PS__Control_Monad_Reader_Class['MonadAsk'](function ()  use (&$monadMaybeT, &$dictMonadAsk) {
            return $monadMaybeT($dictMonadAsk['Monad0']());
        }, $PS__Control_Monad_Trans_Class['lift']($monadTransMaybeT)($dictMonadAsk['Monad0']())($PS__Control_Monad_Reader_Class['ask']($dictMonadAsk)));
    };
    $monadReaderMaybeT = function ($dictMonadReader)  use (&$PS__Control_Monad_Reader_Class, &$monadAskMaybeT, &$mapMaybeT) {
        return $PS__Control_Monad_Reader_Class['MonadReader'](function ()  use (&$monadAskMaybeT, &$dictMonadReader) {
            return $monadAskMaybeT($dictMonadReader['MonadAsk0']());
        }, function ($f)  use (&$mapMaybeT, &$PS__Control_Monad_Reader_Class, &$dictMonadReader) {
            return $mapMaybeT($PS__Control_Monad_Reader_Class['local']($dictMonadReader)($f));
        });
    };
    $monadContMaybeT = function ($dictMonadCont)  use (&$PS__Control_Monad_Cont_Class, &$monadMaybeT, &$PS__Data_Function, &$MaybeT, &$PS__Data_Maybe) {
        return $PS__Control_Monad_Cont_Class['MonadCont'](function ()  use (&$monadMaybeT, &$dictMonadCont) {
            return $monadMaybeT($dictMonadCont['Monad0']());
        }, function ($f)  use (&$PS__Data_Function, &$MaybeT, &$PS__Control_Monad_Cont_Class, &$dictMonadCont, &$PS__Data_Maybe) {
            return $PS__Data_Function['apply']($MaybeT)($PS__Control_Monad_Cont_Class['callCC']($dictMonadCont)(function ($c)  use (&$f, &$PS__Data_Function, &$MaybeT, &$PS__Data_Maybe) {
                $v = $f(function ($a)  use (&$PS__Data_Function, &$MaybeT, &$c, &$PS__Data_Maybe) {
                    return $PS__Data_Function['apply']($MaybeT)($PS__Data_Function['apply']($c)($PS__Data_Maybe['Just']['constructor']($a)));
                });
                return $v;
            }));
        });
    };
    $monadEffectMaybe = function ($dictMonadEffect)  use (&$PS__Effect_Class, &$monadMaybeT, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransMaybeT) {
        return $PS__Effect_Class['MonadEffect'](function ()  use (&$monadMaybeT, &$dictMonadEffect) {
            return $monadMaybeT($dictMonadEffect['Monad0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Trans_Class['lift']($monadTransMaybeT)($dictMonadEffect['Monad0']()))($PS__Effect_Class['liftEffect']($dictMonadEffect)));
    };
    $monadRecMaybeT = function ($dictMonadRec)  use (&$PS__Control_Monad_Rec_Class, &$monadMaybeT, &$PS__Control_Semigroupoid, &$MaybeT, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Data_Maybe) {
        return $PS__Control_Monad_Rec_Class['MonadRec'](function ()  use (&$monadMaybeT, &$dictMonadRec) {
            return $monadMaybeT($dictMonadRec['Monad0']());
        }, function ($f)  use (&$PS__Control_Semigroupoid, &$MaybeT, &$PS__Control_Monad_Rec_Class, &$dictMonadRec, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Data_Maybe) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($MaybeT)($PS__Control_Monad_Rec_Class['tailRecM']($dictMonadRec)(function ($a)  use (&$PS__Control_Bind, &$dictMonadRec, &$f, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$PS__Data_Maybe) {
                return $PS__Control_Bind['bind'](($dictMonadRec['Monad0']())['Bind1']())((function ()  use (&$f, &$a) {
                    $v = $f($a);
                    return $v;
                })())(function ($m__prime)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$PS__Data_Maybe) {
                    return $PS__Control_Applicative['pure'](($dictMonadRec['Monad0']())['Applicative0']())((function ()  use (&$m__prime, &$PS__Control_Monad_Rec_Class, &$PS__Data_Maybe) {
                        if ($m__prime['type'] === 'Nothing') {
                            return $PS__Control_Monad_Rec_Class['Done']['constructor']($PS__Data_Maybe['Nothing']());
                        };
                        if ($m__prime['type'] === 'Just' && $m__prime['value0']['type'] === 'Loop') {
                            return $PS__Control_Monad_Rec_Class['Loop']['constructor']($m__prime['value0']['value0']);
                        };
                        if ($m__prime['type'] === 'Just' && $m__prime['value0']['type'] === 'Done') {
                            return $PS__Control_Monad_Rec_Class['Done']['constructor']($PS__Data_Maybe['Just']['constructor']($m__prime['value0']['value0']));
                        };
                        throw new \Exception('Failed pattern match at Control.Monad.Maybe.Trans line 84, column 16 - line 87, column 43: ' . var_dump([ $m__prime['constructor']['name'] ]));
                    })());
                });
            }));
        });
    };
    $monadStateMaybeT = function ($dictMonadState)  use (&$PS__Control_Monad_State_Class, &$monadMaybeT, &$PS__Control_Monad_Trans_Class, &$monadTransMaybeT) {
        return $PS__Control_Monad_State_Class['MonadState'](function ()  use (&$monadMaybeT, &$dictMonadState) {
            return $monadMaybeT($dictMonadState['Monad0']());
        }, function ($f)  use (&$PS__Control_Monad_Trans_Class, &$monadTransMaybeT, &$dictMonadState, &$PS__Control_Monad_State_Class) {
            return $PS__Control_Monad_Trans_Class['lift']($monadTransMaybeT)($dictMonadState['Monad0']())($PS__Control_Monad_State_Class['state']($dictMonadState)($f));
        });
    };
    $monadTellMaybeT = function ($dictMonadTell)  use (&$PS__Control_Monad_Writer_Class, &$monadMaybeT, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransMaybeT) {
        return $PS__Control_Monad_Writer_Class['MonadTell'](function ()  use (&$monadMaybeT, &$dictMonadTell) {
            return $monadMaybeT($dictMonadTell['Monad0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Trans_Class['lift']($monadTransMaybeT)($dictMonadTell['Monad0']()))($PS__Control_Monad_Writer_Class['tell']($dictMonadTell)));
    };
    $monadWriterMaybeT = function ($dictMonadWriter)  use (&$PS__Control_Monad_Writer_Class, &$monadTellMaybeT, &$mapMaybeT, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Control_Category) {
        return $PS__Control_Monad_Writer_Class['MonadWriter'](function ()  use (&$monadTellMaybeT, &$dictMonadWriter) {
            return $monadTellMaybeT($dictMonadWriter['MonadTell0']());
        }, $mapMaybeT(function ($m)  use (&$PS__Control_Bind, &$dictMonadWriter, &$PS__Control_Monad_Writer_Class, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Tuple) {
            return $PS__Control_Bind['bind']((($dictMonadWriter['MonadTell0']())['Monad0']())['Bind1']())($PS__Control_Monad_Writer_Class['listen']($dictMonadWriter)($m))(function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonadWriter, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Tuple) {
                return $PS__Data_Function['apply']($PS__Control_Applicative['pure']((($dictMonadWriter['MonadTell0']())['Monad0']())['Applicative0']()))($PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($r)  use (&$PS__Data_Tuple, &$v) {
                    return $PS__Data_Tuple['Tuple']['constructor']($r, $v['value1']);
                })($v['value0']));
            });
        }), $mapMaybeT(function ($m)  use (&$PS__Control_Monad_Writer_Class, &$dictMonadWriter, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Data_Tuple, &$PS__Data_Maybe, &$PS__Control_Category) {
            return $PS__Control_Monad_Writer_Class['pass']($dictMonadWriter)($PS__Control_Bind['bind']((($dictMonadWriter['MonadTell0']())['Monad0']())['Bind1']())($m)(function ($v)  use (&$PS__Control_Applicative, &$dictMonadWriter, &$PS__Data_Tuple, &$PS__Data_Maybe, &$PS__Control_Category) {
                return $PS__Control_Applicative['pure']((($dictMonadWriter['MonadTell0']())['Monad0']())['Applicative0']())((function ()  use (&$v, &$PS__Data_Tuple, &$PS__Data_Maybe, &$PS__Control_Category) {
                    if ($v['type'] === 'Nothing') {
                        return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_Maybe['Nothing'](), $PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
                    };
                    if ($v['type'] === 'Just') {
                        return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_Maybe['Just']['constructor']($v['value0']['value0']), $v['value0']['value1']);
                    };
                    throw new \Exception('Failed pattern match at Control.Monad.Maybe.Trans line 121, column 10 - line 123, column 42: ' . var_dump([ $v['constructor']['name'] ]));
                })());
            }));
        }));
    };
    $monadThrowMaybeT = function ($dictMonadThrow)  use (&$PS__Control_Monad_Error_Class, &$monadMaybeT, &$PS__Control_Monad_Trans_Class, &$monadTransMaybeT) {
        return $PS__Control_Monad_Error_Class['MonadThrow'](function ()  use (&$monadMaybeT, &$dictMonadThrow) {
            return $monadMaybeT($dictMonadThrow['Monad0']());
        }, function ($e)  use (&$PS__Control_Monad_Trans_Class, &$monadTransMaybeT, &$dictMonadThrow, &$PS__Control_Monad_Error_Class) {
            return $PS__Control_Monad_Trans_Class['lift']($monadTransMaybeT)($dictMonadThrow['Monad0']())($PS__Control_Monad_Error_Class['throwError']($dictMonadThrow)($e));
        });
    };
    $monadErrorMaybeT = function ($dictMonadError)  use (&$PS__Control_Monad_Error_Class, &$monadThrowMaybeT, &$PS__Data_Function, &$MaybeT) {
        return $PS__Control_Monad_Error_Class['MonadError'](function ()  use (&$monadThrowMaybeT, &$dictMonadError) {
            return $monadThrowMaybeT($dictMonadError['MonadThrow0']());
        }, function ($v)  use (&$PS__Data_Function, &$MaybeT, &$PS__Control_Monad_Error_Class, &$dictMonadError) {
            return function ($h)  use (&$PS__Data_Function, &$MaybeT, &$PS__Control_Monad_Error_Class, &$dictMonadError, &$v) {
                return $PS__Data_Function['apply']($MaybeT)($PS__Control_Monad_Error_Class['catchError']($dictMonadError)($v)(function ($a)  use (&$h) {
                    $v1 = $h($a);
                    return $v1;
                }));
            };
        });
    };
    $altMaybeT = function ($dictMonad)  use (&$PS__Control_Alt, &$functorMaybeT, &$PS__Control_Bind, &$PS__Control_Applicative) {
        return $PS__Control_Alt['Alt'](function ()  use (&$functorMaybeT, &$dictMonad) {
            return $functorMaybeT((($dictMonad['Bind1']())['Apply0']())['Functor0']());
        }, function ($v)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Control_Applicative) {
            return function ($v1)  use (&$PS__Control_Bind, &$dictMonad, &$v, &$PS__Control_Applicative) {
                return $PS__Control_Bind['bind']($dictMonad['Bind1']())($v)(function ($v2)  use (&$v1, &$PS__Control_Applicative, &$dictMonad) {
                    if ($v2['type'] === 'Nothing') {
                        return $v1;
                    };
                    return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($v2);
                });
            };
        });
    };
    $plusMaybeT = function ($dictMonad)  use (&$PS__Control_Plus, &$altMaybeT, &$PS__Control_Applicative, &$PS__Data_Maybe) {
        return $PS__Control_Plus['Plus'](function ()  use (&$altMaybeT, &$dictMonad) {
            return $altMaybeT($dictMonad);
        }, $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($PS__Data_Maybe['Nothing']()));
    };
    $alternativeMaybeT = function ($dictMonad)  use (&$PS__Control_Alternative, &$applicativeMaybeT, &$plusMaybeT) {
        return $PS__Control_Alternative['Alternative'](function ()  use (&$applicativeMaybeT, &$dictMonad) {
            return $applicativeMaybeT($dictMonad);
        }, function ()  use (&$plusMaybeT, &$dictMonad) {
            return $plusMaybeT($dictMonad);
        });
    };
    $monadZeroMaybeT = function ($dictMonad)  use (&$PS__Control_MonadZero, &$alternativeMaybeT, &$monadMaybeT) {
        return $PS__Control_MonadZero['MonadZero'](function ()  use (&$alternativeMaybeT, &$dictMonad) {
            return $alternativeMaybeT($dictMonad);
        }, function ()  use (&$monadMaybeT, &$dictMonad) {
            return $monadMaybeT($dictMonad);
        });
    };
    $monadPlusMaybeT = function ($dictMonad)  use (&$PS__Control_MonadPlus, &$monadZeroMaybeT) {
        return $PS__Control_MonadPlus['MonadPlus'](function ()  use (&$monadZeroMaybeT, &$dictMonad) {
            return $monadZeroMaybeT($dictMonad);
        });
    };
    return [
        'MaybeT' => $MaybeT,
        'runMaybeT' => $runMaybeT,
        'mapMaybeT' => $mapMaybeT,
        'newtypeMaybeT' => $newtypeMaybeT,
        'functorMaybeT' => $functorMaybeT,
        'applyMaybeT' => $applyMaybeT,
        'applicativeMaybeT' => $applicativeMaybeT,
        'bindMaybeT' => $bindMaybeT,
        'monadMaybeT' => $monadMaybeT,
        'monadTransMaybeT' => $monadTransMaybeT,
        'altMaybeT' => $altMaybeT,
        'plusMaybeT' => $plusMaybeT,
        'alternativeMaybeT' => $alternativeMaybeT,
        'monadPlusMaybeT' => $monadPlusMaybeT,
        'monadZeroMaybeT' => $monadZeroMaybeT,
        'monadRecMaybeT' => $monadRecMaybeT,
        'monadEffectMaybe' => $monadEffectMaybe,
        'monadContMaybeT' => $monadContMaybeT,
        'monadThrowMaybeT' => $monadThrowMaybeT,
        'monadErrorMaybeT' => $monadErrorMaybeT,
        'monadAskMaybeT' => $monadAskMaybeT,
        'monadReaderMaybeT' => $monadReaderMaybeT,
        'monadStateMaybeT' => $monadStateMaybeT,
        'monadTellMaybeT' => $monadTellMaybeT,
        'monadWriterMaybeT' => $monadWriterMaybeT
    ];
})();
