<?php
require_once(dirname(__FILE__) . '/../Control.Comonad/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad.Trans.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Extend/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Comonad_Traced_Trans = (function ()  use (&$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Function, &$PS__Control_Extend, &$PS__Data_Semigroup, &$PS__Control_Comonad_Trans_Class, &$PS__Data_Monoid, &$PS__Control_Comonad) {
    $TracedT = function ($x) {
        return $x;
    };
    $runTracedT = function ($v) {
        return $v;
    };
    $newtypeTracedT = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $TracedT);
    $functorTracedT = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Function) {
        return $PS__Data_Functor['Functor'](function ($f)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Function) {
            return function ($v)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Function, &$f) {
                return $PS__Data_Functor['map']($dictFunctor)(function ($g)  use (&$PS__Data_Function, &$f) {
                    return function ($t)  use (&$PS__Data_Function, &$f, &$g) {
                        return $PS__Data_Function['apply']($f)($g($t));
                    };
                })($v);
            };
        });
    };
    $extendTracedT = function ($dictExtend)  use (&$PS__Control_Extend, &$functorTracedT, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Semigroup) {
        return function ($dictSemigroup)  use (&$PS__Control_Extend, &$functorTracedT, &$dictExtend, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Semigroup) {
            return $PS__Control_Extend['Extend'](function ()  use (&$functorTracedT, &$dictExtend) {
                return $functorTracedT($dictExtend['Functor0']());
            }, function ($f)  use (&$PS__Control_Extend, &$dictExtend, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Semigroup, &$dictSemigroup) {
                return function ($v)  use (&$PS__Control_Extend, &$dictExtend, &$PS__Data_Function, &$f, &$PS__Data_Functor, &$PS__Data_Semigroup, &$dictSemigroup) {
                    return $PS__Control_Extend['extend']($dictExtend)(function ($w__prime)  use (&$PS__Data_Function, &$f, &$PS__Data_Functor, &$dictExtend, &$PS__Data_Semigroup, &$dictSemigroup) {
                        return function ($t)  use (&$PS__Data_Function, &$f, &$PS__Data_Functor, &$dictExtend, &$PS__Data_Semigroup, &$dictSemigroup, &$w__prime) {
                            return $PS__Data_Function['apply']($f)($PS__Data_Functor['map']($dictExtend['Functor0']())(function ($h)  use (&$PS__Data_Function, &$PS__Data_Semigroup, &$dictSemigroup, &$t) {
                                return function ($t__prime)  use (&$PS__Data_Function, &$h, &$PS__Data_Semigroup, &$dictSemigroup, &$t) {
                                    return $PS__Data_Function['apply']($h)($PS__Data_Semigroup['append']($dictSemigroup)($t)($t__prime));
                                };
                            })($w__prime));
                        };
                    })($v);
                };
            });
        };
    };
    $comonadTransTracedT = function ($dictMonoid)  use (&$PS__Control_Comonad_Trans_Class, &$PS__Data_Functor, &$PS__Data_Monoid) {
        return $PS__Control_Comonad_Trans_Class['ComonadTrans'](function ($dictComonad)  use (&$PS__Data_Functor, &$PS__Data_Monoid, &$dictMonoid) {
            return function ($v)  use (&$PS__Data_Functor, &$dictComonad, &$PS__Data_Monoid, &$dictMonoid) {
                return $PS__Data_Functor['map'](($dictComonad['Extend0']())['Functor0']())(function ($f)  use (&$PS__Data_Monoid, &$dictMonoid) {
                    return $f($PS__Data_Monoid['mempty']($dictMonoid));
                })($v);
            };
        });
    };
    $comonadTracedT = function ($dictComonad)  use (&$PS__Control_Comonad, &$extendTracedT, &$PS__Data_Monoid) {
        return function ($dictMonoid)  use (&$PS__Control_Comonad, &$extendTracedT, &$dictComonad, &$PS__Data_Monoid) {
            return $PS__Control_Comonad['Comonad'](function ()  use (&$extendTracedT, &$dictComonad, &$dictMonoid) {
                return $extendTracedT($dictComonad['Extend0']())($dictMonoid['Semigroup0']());
            }, function ($v)  use (&$PS__Control_Comonad, &$dictComonad, &$PS__Data_Monoid, &$dictMonoid) {
                return $PS__Control_Comonad['extract']($dictComonad)($v)($PS__Data_Monoid['mempty']($dictMonoid));
            });
        };
    };
    return [
        'TracedT' => $TracedT,
        'runTracedT' => $runTracedT,
        'newtypeTracedT' => $newtypeTracedT,
        'functorTracedT' => $functorTracedT,
        'extendTracedT' => $extendTracedT,
        'comonadTracedT' => $comonadTracedT,
        'comonadTransTracedT' => $comonadTransTracedT
    ];
})();
