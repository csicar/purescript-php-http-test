<?php
require_once(dirname(__FILE__) . '/../Control.Comonad/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad.Trans.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Extend/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Comonad_Store_Trans = (function ()  use (&$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Function, &$PS__Data_Tuple, &$PS__Control_Semigroupoid, &$PS__Control_Extend, &$PS__Control_Comonad_Trans_Class, &$PS__Control_Comonad) {
    $StoreT = function ($x) {
        return $x;
    };
    $runStoreT = function ($v) {
        return $v;
    };
    $newtypeStoreT = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $StoreT);
    $functorStoreT = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Function, &$StoreT, &$PS__Data_Tuple, &$PS__Control_Semigroupoid) {
        return $PS__Data_Functor['Functor'](function ($f)  use (&$PS__Data_Function, &$StoreT, &$PS__Data_Tuple, &$PS__Data_Functor, &$dictFunctor, &$PS__Control_Semigroupoid) {
            return function ($v)  use (&$PS__Data_Function, &$StoreT, &$PS__Data_Tuple, &$PS__Data_Functor, &$dictFunctor, &$PS__Control_Semigroupoid, &$f) {
                return $PS__Data_Function['apply']($StoreT)($PS__Data_Tuple['Tuple']['constructor']($PS__Data_Functor['map']($dictFunctor)(function ($h)  use (&$PS__Control_Semigroupoid, &$f) {
                    return $PS__Control_Semigroupoid['composeFlipped']($PS__Control_Semigroupoid['semigroupoidFn'])($h)($f);
                })($v['value0']), $v['value1']));
            };
        });
    };
    $extendStoreT = function ($dictExtend)  use (&$PS__Control_Extend, &$functorStoreT, &$PS__Data_Function, &$StoreT, &$PS__Data_Tuple) {
        return $PS__Control_Extend['Extend'](function ()  use (&$functorStoreT, &$dictExtend) {
            return $functorStoreT($dictExtend['Functor0']());
        }, function ($f)  use (&$PS__Data_Function, &$StoreT, &$PS__Data_Tuple, &$PS__Control_Extend, &$dictExtend) {
            return function ($v)  use (&$PS__Data_Function, &$StoreT, &$PS__Data_Tuple, &$PS__Control_Extend, &$dictExtend, &$f) {
                return $PS__Data_Function['apply']($StoreT)($PS__Data_Tuple['Tuple']['constructor']($PS__Control_Extend['extend']($dictExtend)(function ($w__prime)  use (&$PS__Data_Function, &$f, &$StoreT, &$PS__Data_Tuple) {
                    return function ($s__prime)  use (&$PS__Data_Function, &$f, &$StoreT, &$PS__Data_Tuple, &$w__prime) {
                        return $PS__Data_Function['apply']($f)($PS__Data_Function['apply']($StoreT)($PS__Data_Tuple['Tuple']['constructor']($w__prime, $s__prime)));
                    };
                })($v['value0']), $v['value1']));
            };
        });
    };
    $comonadTransStoreT = $PS__Control_Comonad_Trans_Class['ComonadTrans'](function ($dictComonad)  use (&$PS__Data_Functor, &$PS__Data_Function) {
        return function ($v)  use (&$PS__Data_Functor, &$dictComonad, &$PS__Data_Function) {
            return $PS__Data_Functor['map'](($dictComonad['Extend0']())['Functor0']())(function ($v1)  use (&$PS__Data_Function, &$v) {
                return $PS__Data_Function['apply']($v1)($v['value1']);
            })($v['value0']);
        };
    });
    $comonadStoreT = function ($dictComonad)  use (&$PS__Control_Comonad, &$extendStoreT) {
        return $PS__Control_Comonad['Comonad'](function ()  use (&$extendStoreT, &$dictComonad) {
            return $extendStoreT($dictComonad['Extend0']());
        }, function ($v)  use (&$PS__Control_Comonad, &$dictComonad) {
            return $PS__Control_Comonad['extract']($dictComonad)($v['value0'])($v['value1']);
        });
    };
    return [
        'StoreT' => $StoreT,
        'runStoreT' => $runStoreT,
        'newtypeStoreT' => $newtypeStoreT,
        'functorStoreT' => $functorStoreT,
        'extendStoreT' => $extendStoreT,
        'comonadStoreT' => $comonadStoreT,
        'comonadTransStoreT' => $comonadTransStoreT
    ];
})();
