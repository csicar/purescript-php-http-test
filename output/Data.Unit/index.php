<?php
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
$PS__Data_Unit = (function ()  use (&$PS__Data_Show) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $showUnit = $PS__Data_Show['Show'](function ($v) {
        return 'unit';
    });
    return [
        'showUnit' => $showUnit,
        'unit' => $__foreign['unit']
    ];
})();
