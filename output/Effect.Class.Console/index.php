<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Effect.Class/index.php');
require_once(dirname(__FILE__) . '/../Effect.Console/index.php');
$PS__Effect_Class_Console = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Effect_Class, &$PS__Effect_Console) {
    $warnShow = function ($dictMonadEffect)  use (&$PS__Control_Semigroupoid, &$PS__Effect_Class, &$PS__Effect_Console) {
        return function ($dictShow)  use (&$PS__Control_Semigroupoid, &$PS__Effect_Class, &$dictMonadEffect, &$PS__Effect_Console) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Effect_Class['liftEffect']($dictMonadEffect))($PS__Effect_Console['warnShow']($dictShow));
        };
    };
    $warn = function ($dictMonadEffect)  use (&$PS__Control_Semigroupoid, &$PS__Effect_Class, &$PS__Effect_Console) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Effect_Class['liftEffect']($dictMonadEffect))($PS__Effect_Console['warn']);
    };
    $logShow = function ($dictMonadEffect)  use (&$PS__Control_Semigroupoid, &$PS__Effect_Class, &$PS__Effect_Console) {
        return function ($dictShow)  use (&$PS__Control_Semigroupoid, &$PS__Effect_Class, &$dictMonadEffect, &$PS__Effect_Console) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Effect_Class['liftEffect']($dictMonadEffect))($PS__Effect_Console['logShow']($dictShow));
        };
    };
    $log = function ($dictMonadEffect)  use (&$PS__Control_Semigroupoid, &$PS__Effect_Class, &$PS__Effect_Console) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Effect_Class['liftEffect']($dictMonadEffect))($PS__Effect_Console['log']);
    };
    $infoShow = function ($dictMonadEffect)  use (&$PS__Control_Semigroupoid, &$PS__Effect_Class, &$PS__Effect_Console) {
        return function ($dictShow)  use (&$PS__Control_Semigroupoid, &$PS__Effect_Class, &$dictMonadEffect, &$PS__Effect_Console) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Effect_Class['liftEffect']($dictMonadEffect))($PS__Effect_Console['infoShow']($dictShow));
        };
    };
    $info = function ($dictMonadEffect)  use (&$PS__Control_Semigroupoid, &$PS__Effect_Class, &$PS__Effect_Console) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Effect_Class['liftEffect']($dictMonadEffect))($PS__Effect_Console['info']);
    };
    $errorShow = function ($dictMonadEffect)  use (&$PS__Control_Semigroupoid, &$PS__Effect_Class, &$PS__Effect_Console) {
        return function ($dictShow)  use (&$PS__Control_Semigroupoid, &$PS__Effect_Class, &$dictMonadEffect, &$PS__Effect_Console) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Effect_Class['liftEffect']($dictMonadEffect))($PS__Effect_Console['errorShow']($dictShow));
        };
    };
    $error = function ($dictMonadEffect)  use (&$PS__Control_Semigroupoid, &$PS__Effect_Class, &$PS__Effect_Console) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Effect_Class['liftEffect']($dictMonadEffect))($PS__Effect_Console['error']);
    };
    return [
        'log' => $log,
        'logShow' => $logShow,
        'warn' => $warn,
        'warnShow' => $warnShow,
        'error' => $error,
        'errorShow' => $errorShow,
        'info' => $info,
        'infoShow' => $infoShow
    ];
})();
