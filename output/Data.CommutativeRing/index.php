<?php
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Symbol/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
$PS__Data_CommutativeRing = (function ()  use (&$PS__Data_Ring) {
    $CommutativeRing = function ($Ring0) {
        return [
            'Ring0' => $Ring0
        ];
    };
    $CommutativeRingRecord = function ($RingRecord0) {
        return [
            'RingRecord0' => $RingRecord0
        ];
    };
    $commutativeRingUnit = $CommutativeRing(function ()  use (&$PS__Data_Ring) {
        return $PS__Data_Ring['ringUnit'];
    });
    $commutativeRingRecordNil = $CommutativeRingRecord(function ()  use (&$PS__Data_Ring) {
        return $PS__Data_Ring['ringRecordNil'];
    });
    $commutativeRingRecordCons = function ($dictIsSymbol)  use (&$CommutativeRingRecord, &$PS__Data_Ring) {
        return function ($dictCons)  use (&$CommutativeRingRecord, &$PS__Data_Ring, &$dictIsSymbol) {
            return function ($dictCommutativeRingRecord)  use (&$CommutativeRingRecord, &$PS__Data_Ring, &$dictIsSymbol, &$dictCons) {
                return function ($dictCommutativeRing)  use (&$CommutativeRingRecord, &$PS__Data_Ring, &$dictIsSymbol, &$dictCons, &$dictCommutativeRingRecord) {
                    return $CommutativeRingRecord(function ()  use (&$PS__Data_Ring, &$dictIsSymbol, &$dictCons, &$dictCommutativeRingRecord, &$dictCommutativeRing) {
                        return $PS__Data_Ring['ringRecordCons']($dictIsSymbol)($dictCons)($dictCommutativeRingRecord['RingRecord0']())($dictCommutativeRing['Ring0']());
                    });
                };
            };
        };
    };
    $commutativeRingRecord = function ($dictRowToList)  use (&$CommutativeRing, &$PS__Data_Ring) {
        return function ($dictCommutativeRingRecord)  use (&$CommutativeRing, &$PS__Data_Ring, &$dictRowToList) {
            return $CommutativeRing(function ()  use (&$PS__Data_Ring, &$dictRowToList, &$dictCommutativeRingRecord) {
                return $PS__Data_Ring['ringRecord']($dictRowToList)($dictCommutativeRingRecord['RingRecord0']());
            });
        };
    };
    $commutativeRingNumber = $CommutativeRing(function ()  use (&$PS__Data_Ring) {
        return $PS__Data_Ring['ringNumber'];
    });
    $commutativeRingInt = $CommutativeRing(function ()  use (&$PS__Data_Ring) {
        return $PS__Data_Ring['ringInt'];
    });
    $commutativeRingFn = function ($dictCommutativeRing)  use (&$CommutativeRing, &$PS__Data_Ring) {
        return $CommutativeRing(function ()  use (&$PS__Data_Ring, &$dictCommutativeRing) {
            return $PS__Data_Ring['ringFn']($dictCommutativeRing['Ring0']());
        });
    };
    return [
        'CommutativeRing' => $CommutativeRing,
        'CommutativeRingRecord' => $CommutativeRingRecord,
        'commutativeRingInt' => $commutativeRingInt,
        'commutativeRingNumber' => $commutativeRingNumber,
        'commutativeRingUnit' => $commutativeRingUnit,
        'commutativeRingFn' => $commutativeRingFn,
        'commutativeRingRecord' => $commutativeRingRecord,
        'commutativeRingRecordNil' => $commutativeRingRecordNil,
        'commutativeRingRecordCons' => $commutativeRingRecordCons
    ];
})();
