<?php
require_once(dirname(__FILE__) . '/../Control.Biapplicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Biapply/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Bifunctor_Flip = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Bifunctor, &$PS__Control_Biapply, &$PS__Control_Biapplicative) {
    $Flip = function ($x) {
        return $x;
    };
    $showFlip = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Flip ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $ordFlip = function ($dictOrd) {
        return $dictOrd;
    };
    $newtypeFlip = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Flip);
    $functorFlip = function ($dictBifunctor)  use (&$PS__Data_Functor, &$PS__Data_Bifunctor) {
        return $PS__Data_Functor['Functor'](function ($f)  use (&$PS__Data_Bifunctor, &$dictBifunctor) {
            return function ($v)  use (&$PS__Data_Bifunctor, &$dictBifunctor, &$f) {
                return $PS__Data_Bifunctor['lmap']($dictBifunctor)($f)($v);
            };
        });
    };
    $eqFlip = function ($dictEq) {
        return $dictEq;
    };
    $bifunctorFlip = function ($dictBifunctor)  use (&$PS__Data_Bifunctor) {
        return $PS__Data_Bifunctor['Bifunctor'](function ($f)  use (&$PS__Data_Bifunctor, &$dictBifunctor) {
            return function ($g)  use (&$PS__Data_Bifunctor, &$dictBifunctor, &$f) {
                return function ($v)  use (&$PS__Data_Bifunctor, &$dictBifunctor, &$g, &$f) {
                    return $PS__Data_Bifunctor['bimap']($dictBifunctor)($g)($f)($v);
                };
            };
        });
    };
    $biapplyFlip = function ($dictBiapply)  use (&$PS__Control_Biapply, &$bifunctorFlip) {
        return $PS__Control_Biapply['Biapply'](function ()  use (&$bifunctorFlip, &$dictBiapply) {
            return $bifunctorFlip($dictBiapply['Bifunctor0']());
        }, function ($v)  use (&$PS__Control_Biapply, &$dictBiapply) {
            return function ($v1)  use (&$PS__Control_Biapply, &$dictBiapply, &$v) {
                return $PS__Control_Biapply['biapply']($dictBiapply)($v)($v1);
            };
        });
    };
    $biapplicativeFlip = function ($dictBiapplicative)  use (&$PS__Control_Biapplicative, &$biapplyFlip) {
        return $PS__Control_Biapplicative['Biapplicative'](function ()  use (&$biapplyFlip, &$dictBiapplicative) {
            return $biapplyFlip($dictBiapplicative['Biapply0']());
        }, function ($a)  use (&$PS__Control_Biapplicative, &$dictBiapplicative) {
            return function ($b)  use (&$PS__Control_Biapplicative, &$dictBiapplicative, &$a) {
                return $PS__Control_Biapplicative['bipure']($dictBiapplicative)($b)($a);
            };
        });
    };
    return [
        'Flip' => $Flip,
        'newtypeFlip' => $newtypeFlip,
        'eqFlip' => $eqFlip,
        'ordFlip' => $ordFlip,
        'showFlip' => $showFlip,
        'functorFlip' => $functorFlip,
        'bifunctorFlip' => $bifunctorFlip,
        'biapplyFlip' => $biapplyFlip,
        'biapplicativeFlip' => $biapplicativeFlip
    ];
})();
