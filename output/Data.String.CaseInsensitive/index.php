<?php
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.String/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Common/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_String_CaseInsensitive = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Newtype, &$PS__Data_Eq, &$PS__Data_String_Common, &$PS__Data_Ord) {
    $CaseInsensitiveString = function ($x) {
        return $x;
    };
    $showCaseInsensitiveString = $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show) {
        return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(CaseInsensitiveString ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Show['showString'])($v))(')'));
    });
    $newtypeCaseInsensitiveString = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $CaseInsensitiveString);
    $eqCaseInsensitiveString = $PS__Data_Eq['Eq'](function ($v)  use (&$PS__Data_Eq, &$PS__Data_String_Common) {
        return function ($v1)  use (&$PS__Data_Eq, &$PS__Data_String_Common, &$v) {
            return $PS__Data_Eq['eq']($PS__Data_Eq['eqString'])($PS__Data_String_Common['toLower']($v))($PS__Data_String_Common['toLower']($v1));
        };
    });
    $ordCaseInsensitiveString = $PS__Data_Ord['Ord'](function ()  use (&$eqCaseInsensitiveString) {
        return $eqCaseInsensitiveString;
    }, function ($v)  use (&$PS__Data_Ord, &$PS__Data_String_Common) {
        return function ($v1)  use (&$PS__Data_Ord, &$PS__Data_String_Common, &$v) {
            return $PS__Data_Ord['compare']($PS__Data_Ord['ordString'])($PS__Data_String_Common['toLower']($v))($PS__Data_String_Common['toLower']($v1));
        };
    });
    return [
        'CaseInsensitiveString' => $CaseInsensitiveString,
        'eqCaseInsensitiveString' => $eqCaseInsensitiveString,
        'ordCaseInsensitiveString' => $ordCaseInsensitiveString,
        'showCaseInsensitiveString' => $showCaseInsensitiveString,
        'newtypeCaseInsensitiveString' => $newtypeCaseInsensitiveString
    ];
})();
