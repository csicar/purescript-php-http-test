<?php

$exports["intAdd"] = function ($x) {
  return function ($y) use ($x) {
    return $x + $y | 0;
  };
}; 

$exports["intMul"] = function ($x) {
  return function ($y) use ($x) {
    return $x * $y | 0;
  };
};

$exports["numAdd"] = function ($n1) {
  return function ($n2) use ($n1) {
    return $n1 + $n2;
  };
};

$exports["numMul"] = function ($n1) {
  return function ($n2) use ($n1) {
    return $n1 * $n2;
  };
};
