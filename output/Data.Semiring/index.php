<?php
require_once(dirname(__FILE__) . '/../Data.Symbol/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Record.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Type.Data.Row/index.php');
require_once(dirname(__FILE__) . '/../Type.Data.RowList/index.php');
$PS__Data_Semiring = (function ()  use (&$PS__Data_Unit, &$PS__Type_Data_RowList, &$PS__Type_Data_Row, &$PS__Data_Symbol, &$PS__Record_Unsafe) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Semiring = function ($add, $mul, $one, $zero) {
        return [
            'add' => $add,
            'mul' => $mul,
            'one' => $one,
            'zero' => $zero
        ];
    };
    $SemiringRecord = function ($addRecord, $mulRecord, $oneRecord, $zeroRecord) {
        return [
            'addRecord' => $addRecord,
            'mulRecord' => $mulRecord,
            'oneRecord' => $oneRecord,
            'zeroRecord' => $zeroRecord
        ];
    };
    $zeroRecord = function ($dict) {
        return $dict['zeroRecord'];
    };
    $zero = function ($dict) {
        return $dict['zero'];
    };
    $semiringUnit = $Semiring(function ($v)  use (&$PS__Data_Unit) {
        return function ($v1)  use (&$PS__Data_Unit) {
            return $PS__Data_Unit['unit'];
        };
    }, function ($v)  use (&$PS__Data_Unit) {
        return function ($v1)  use (&$PS__Data_Unit) {
            return $PS__Data_Unit['unit'];
        };
    }, $PS__Data_Unit['unit'], $PS__Data_Unit['unit']);
    $semiringRecordNil = $SemiringRecord(function ($v) {
        return function ($v1) {
            return function ($v2) {
                return [];
            };
        };
    }, function ($v) {
        return function ($v1) {
            return function ($v2) {
                return [];
            };
        };
    }, function ($v) {
        return function ($v1) {
            return [];
        };
    }, function ($v) {
        return function ($v1) {
            return [];
        };
    });
    $semiringNumber = $Semiring($__foreign['numAdd'], $__foreign['numMul'], 1.0, 0.0);
    $semiringInt = $Semiring($__foreign['intAdd'], $__foreign['intMul'], 1, 0);
    $oneRecord = function ($dict) {
        return $dict['oneRecord'];
    };
    $one = function ($dict) {
        return $dict['one'];
    };
    $mulRecord = function ($dict) {
        return $dict['mulRecord'];
    };
    $mul = function ($dict) {
        return $dict['mul'];
    };
    $addRecord = function ($dict) {
        return $dict['addRecord'];
    };
    $semiringRecord = function ($dictRowToList)  use (&$Semiring, &$addRecord, &$PS__Type_Data_RowList, &$mulRecord, &$oneRecord, &$PS__Type_Data_Row, &$zeroRecord) {
        return function ($dictSemiringRecord)  use (&$Semiring, &$addRecord, &$PS__Type_Data_RowList, &$mulRecord, &$oneRecord, &$PS__Type_Data_Row, &$zeroRecord) {
            return $Semiring($addRecord($dictSemiringRecord)($PS__Type_Data_RowList['RLProxy']()), $mulRecord($dictSemiringRecord)($PS__Type_Data_RowList['RLProxy']()), $oneRecord($dictSemiringRecord)($PS__Type_Data_RowList['RLProxy']())($PS__Type_Data_Row['RProxy']()), $zeroRecord($dictSemiringRecord)($PS__Type_Data_RowList['RLProxy']())($PS__Type_Data_Row['RProxy']()));
        };
    };
    $add = function ($dict) {
        return $dict['add'];
    };
    $semiringFn = function ($dictSemiring)  use (&$Semiring, &$add, &$mul, &$one, &$zero) {
        return $Semiring(function ($f)  use (&$add, &$dictSemiring) {
            return function ($g)  use (&$add, &$dictSemiring, &$f) {
                return function ($x)  use (&$add, &$dictSemiring, &$f, &$g) {
                    return $add($dictSemiring)($f($x))($g($x));
                };
            };
        }, function ($f)  use (&$mul, &$dictSemiring) {
            return function ($g)  use (&$mul, &$dictSemiring, &$f) {
                return function ($x)  use (&$mul, &$dictSemiring, &$f, &$g) {
                    return $mul($dictSemiring)($f($x))($g($x));
                };
            };
        }, function ($v)  use (&$one, &$dictSemiring) {
            return $one($dictSemiring);
        }, function ($v)  use (&$zero, &$dictSemiring) {
            return $zero($dictSemiring);
        });
    };
    $semiringRecordCons = function ($dictIsSymbol)  use (&$SemiringRecord, &$addRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$add, &$mulRecord, &$mul, &$oneRecord, &$PS__Type_Data_Row, &$one, &$zeroRecord, &$zero) {
        return function ($dictCons)  use (&$SemiringRecord, &$addRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$add, &$mulRecord, &$mul, &$oneRecord, &$PS__Type_Data_Row, &$one, &$zeroRecord, &$zero) {
            return function ($dictSemiringRecord)  use (&$SemiringRecord, &$addRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$add, &$mulRecord, &$mul, &$oneRecord, &$PS__Type_Data_Row, &$one, &$zeroRecord, &$zero) {
                return function ($dictSemiring)  use (&$SemiringRecord, &$addRecord, &$dictSemiringRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$add, &$mulRecord, &$mul, &$oneRecord, &$PS__Type_Data_Row, &$one, &$zeroRecord, &$zero) {
                    return $SemiringRecord(function ($v)  use (&$addRecord, &$dictSemiringRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$add, &$dictSemiring) {
                        return function ($ra)  use (&$addRecord, &$dictSemiringRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$add, &$dictSemiring) {
                            return function ($rb)  use (&$addRecord, &$dictSemiringRecord, &$PS__Type_Data_RowList, &$ra, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$add, &$dictSemiring) {
                                $tail = $addRecord($dictSemiringRecord)($PS__Type_Data_RowList['RLProxy']())($ra)($rb);
                                $key = $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($PS__Data_Symbol['SProxy']());
                                $insert = $PS__Record_Unsafe['unsafeSet']($key);
                                $get = $PS__Record_Unsafe['unsafeGet']($key);
                                return $insert($add($dictSemiring)($get($ra))($get($rb)))($tail);
                            };
                        };
                    }, function ($v)  use (&$mulRecord, &$dictSemiringRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$mul, &$dictSemiring) {
                        return function ($ra)  use (&$mulRecord, &$dictSemiringRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$mul, &$dictSemiring) {
                            return function ($rb)  use (&$mulRecord, &$dictSemiringRecord, &$PS__Type_Data_RowList, &$ra, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$mul, &$dictSemiring) {
                                $tail = $mulRecord($dictSemiringRecord)($PS__Type_Data_RowList['RLProxy']())($ra)($rb);
                                $key = $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($PS__Data_Symbol['SProxy']());
                                $insert = $PS__Record_Unsafe['unsafeSet']($key);
                                $get = $PS__Record_Unsafe['unsafeGet']($key);
                                return $insert($mul($dictSemiring)($get($ra))($get($rb)))($tail);
                            };
                        };
                    }, function ($v)  use (&$oneRecord, &$dictSemiringRecord, &$PS__Type_Data_RowList, &$PS__Type_Data_Row, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$one, &$dictSemiring) {
                        return function ($v1)  use (&$oneRecord, &$dictSemiringRecord, &$PS__Type_Data_RowList, &$PS__Type_Data_Row, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$one, &$dictSemiring) {
                            $tail = $oneRecord($dictSemiringRecord)($PS__Type_Data_RowList['RLProxy']())($PS__Type_Data_Row['RProxy']());
                            $key = $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($PS__Data_Symbol['SProxy']());
                            $insert = $PS__Record_Unsafe['unsafeSet']($key);
                            return $insert($one($dictSemiring))($tail);
                        };
                    }, function ($v)  use (&$zeroRecord, &$dictSemiringRecord, &$PS__Type_Data_RowList, &$PS__Type_Data_Row, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$zero, &$dictSemiring) {
                        return function ($v1)  use (&$zeroRecord, &$dictSemiringRecord, &$PS__Type_Data_RowList, &$PS__Type_Data_Row, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$zero, &$dictSemiring) {
                            $tail = $zeroRecord($dictSemiringRecord)($PS__Type_Data_RowList['RLProxy']())($PS__Type_Data_Row['RProxy']());
                            $key = $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($PS__Data_Symbol['SProxy']());
                            $insert = $PS__Record_Unsafe['unsafeSet']($key);
                            return $insert($zero($dictSemiring))($tail);
                        };
                    });
                };
            };
        };
    };
    return [
        'Semiring' => $Semiring,
        'add' => $add,
        'zero' => $zero,
        'mul' => $mul,
        'one' => $one,
        'SemiringRecord' => $SemiringRecord,
        'addRecord' => $addRecord,
        'mulRecord' => $mulRecord,
        'oneRecord' => $oneRecord,
        'zeroRecord' => $zeroRecord,
        'semiringInt' => $semiringInt,
        'semiringNumber' => $semiringNumber,
        'semiringFn' => $semiringFn,
        'semiringUnit' => $semiringUnit,
        'semiringRecord' => $semiringRecord,
        'semiringRecordNil' => $semiringRecordNil,
        'semiringRecordCons' => $semiringRecordCons
    ];
})();
