<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
$PS__Data_Functor = (function ()  use (&$PS__Data_Function, &$PS__Data_Unit, &$PS__Control_Semigroupoid) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Functor = function ($map) {
        return [
            'map' => $map
        ];
    };
    $map = function ($dict) {
        return $dict['map'];
    };
    $mapFlipped = function ($dictFunctor)  use (&$map) {
        return function ($fa)  use (&$map, &$dictFunctor) {
            return function ($f)  use (&$map, &$dictFunctor, &$fa) {
                return $map($dictFunctor)($f)($fa);
            };
        };
    };
    $__void = function ($dictFunctor)  use (&$map, &$PS__Data_Function, &$PS__Data_Unit) {
        return $map($dictFunctor)($PS__Data_Function['const']($PS__Data_Unit['unit']));
    };
    $voidLeft = function ($dictFunctor)  use (&$map, &$PS__Data_Function) {
        return function ($f)  use (&$map, &$dictFunctor, &$PS__Data_Function) {
            return function ($x)  use (&$map, &$dictFunctor, &$PS__Data_Function, &$f) {
                return $map($dictFunctor)($PS__Data_Function['const']($x))($f);
            };
        };
    };
    $voidRight = function ($dictFunctor)  use (&$map, &$PS__Data_Function) {
        return function ($x)  use (&$map, &$dictFunctor, &$PS__Data_Function) {
            return $map($dictFunctor)($PS__Data_Function['const']($x));
        };
    };
    $functorFn = $Functor($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn']));
    $functorArray = $Functor($__foreign['arrayMap']);
    $flap = function ($dictFunctor)  use (&$map) {
        return function ($ff)  use (&$map, &$dictFunctor) {
            return function ($x)  use (&$map, &$dictFunctor, &$ff) {
                return $map($dictFunctor)(function ($f)  use (&$x) {
                    return $f($x);
                })($ff);
            };
        };
    };
    return [
        'Functor' => $Functor,
        'map' => $map,
        'mapFlipped' => $mapFlipped,
        'void' => $__void,
        'voidRight' => $voidRight,
        'voidLeft' => $voidLeft,
        'flap' => $flap,
        'functorFn' => $functorFn,
        'functorArray' => $functorArray
    ];
})();
