<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
$PS__Control_MonadZero = (function ()  use (&$PS__Control_Alternative, &$PS__Control_Monad, &$PS__Control_Applicative, &$PS__Data_Unit, &$PS__Control_Plus) {
    $MonadZero = function ($Alternative1, $Monad0) {
        return [
            'Alternative1' => $Alternative1,
            'Monad0' => $Monad0
        ];
    };
    $monadZeroArray = $MonadZero(function ()  use (&$PS__Control_Alternative) {
        return $PS__Control_Alternative['alternativeArray'];
    }, function ()  use (&$PS__Control_Monad) {
        return $PS__Control_Monad['monadArray'];
    });
    $guard = function ($dictMonadZero)  use (&$PS__Control_Applicative, &$PS__Data_Unit, &$PS__Control_Plus) {
        return function ($v)  use (&$PS__Control_Applicative, &$dictMonadZero, &$PS__Data_Unit, &$PS__Control_Plus) {
            if ($v) {
                return $PS__Control_Applicative['pure'](($dictMonadZero['Alternative1']())['Applicative0']())($PS__Data_Unit['unit']);
            };
            if (!$v) {
                return $PS__Control_Plus['empty'](($dictMonadZero['Alternative1']())['Plus1']());
            };
            throw new \Exception('Failed pattern match at Control.MonadZero line 54, column 1 - line 54, column 52: ' . var_dump([ $v['constructor']['name'] ]));
        };
    };
    return [
        'MonadZero' => $MonadZero,
        'guard' => $guard,
        'monadZeroArray' => $monadZeroArray
    ];
})();
