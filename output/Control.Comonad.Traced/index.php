<?php
require_once(dirname(__FILE__) . '/../Control.Comonad.Traced.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad.Traced.Trans/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Identity/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Comonad_Traced = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Data_Identity, &$PS__Control_Comonad_Traced_Trans, &$PS__Data_Newtype) {
    $traced = $PS__Control_Semigroupoid['composeFlipped']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Identity['Identity'])($PS__Control_Comonad_Traced_Trans['TracedT']);
    $runTraced = function ($v)  use (&$PS__Data_Newtype, &$PS__Data_Identity) {
        return $PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity'])($v);
    };
    return [
        'runTraced' => $runTraced,
        'traced' => $traced
    ];
})();
