<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Cont.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Error.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Reader.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.State.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Trans.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Writer.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadPlus/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadZero/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Effect.Class/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Writer_Trans = (function ()  use (&$PS__Data_Newtype, &$PS__Control_Monad_Trans_Class, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple, &$PS__Data_Monoid, &$PS__Data_Functor, &$PS__Control_Apply, &$PS__Data_Semigroup, &$PS__Control_Monad, &$PS__Control_Monad_Reader_Class, &$PS__Control_Monad_Cont_Class, &$PS__Effect_Class, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Rec_Class, &$PS__Control_Monad_State_Class, &$PS__Control_Monad_Writer_Class, &$PS__Data_Unit, &$PS__Control_Monad_Error_Class, &$PS__Control_Alt, &$PS__Control_Plus, &$PS__Control_Alternative, &$PS__Control_MonadZero, &$PS__Control_MonadPlus) {
    $WriterT = function ($x) {
        return $x;
    };
    $runWriterT = function ($v) {
        return $v;
    };
    $newtypeWriterT = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $WriterT);
    $monadTransWriterT = function ($dictMonoid)  use (&$PS__Control_Monad_Trans_Class, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple, &$PS__Data_Monoid) {
        return $PS__Control_Monad_Trans_Class['MonadTrans'](function ($dictMonad)  use (&$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple, &$PS__Data_Monoid, &$dictMonoid) {
            return function ($m)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple, &$PS__Data_Monoid, &$dictMonoid) {
                return $PS__Control_Bind['bind']($dictMonad['Bind1']())($m)(function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_Tuple, &$PS__Data_Monoid, &$dictMonoid) {
                    return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($PS__Data_Tuple['Tuple']['constructor']($v, $PS__Data_Monoid['mempty']($dictMonoid)));
                });
            };
        });
    };
    $mapWriterT = function ($f) {
        return function ($v)  use (&$f) {
            return $f($v);
        };
    };
    $functorWriterT = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Function, &$mapWriterT, &$PS__Data_Tuple) {
        return $PS__Data_Functor['Functor'](function ($f)  use (&$PS__Data_Function, &$mapWriterT, &$PS__Data_Functor, &$dictFunctor, &$PS__Data_Tuple) {
            return $PS__Data_Function['apply']($mapWriterT)($PS__Data_Functor['map']($dictFunctor)(function ($v)  use (&$PS__Data_Tuple, &$f) {
                return $PS__Data_Tuple['Tuple']['constructor']($f($v['value0']), $v['value1']);
            }));
        });
    };
    $execWriterT = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Tuple) {
            return $PS__Data_Functor['map']($dictFunctor)($PS__Data_Tuple['snd'])($v);
        };
    };
    $applyWriterT = function ($dictSemigroup)  use (&$PS__Control_Apply, &$functorWriterT, &$PS__Data_Tuple, &$PS__Data_Semigroup, &$PS__Data_Functor) {
        return function ($dictApply)  use (&$PS__Control_Apply, &$functorWriterT, &$PS__Data_Tuple, &$PS__Data_Semigroup, &$dictSemigroup, &$PS__Data_Functor) {
            return $PS__Control_Apply['Apply'](function ()  use (&$functorWriterT, &$dictApply) {
                return $functorWriterT($dictApply['Functor0']());
            }, function ($v)  use (&$PS__Data_Tuple, &$PS__Data_Semigroup, &$dictSemigroup, &$PS__Control_Apply, &$dictApply, &$PS__Data_Functor) {
                return function ($v1)  use (&$PS__Data_Tuple, &$PS__Data_Semigroup, &$dictSemigroup, &$PS__Control_Apply, &$dictApply, &$PS__Data_Functor, &$v) {
                    $k = function ($v3)  use (&$PS__Data_Tuple, &$PS__Data_Semigroup, &$dictSemigroup) {
                        return function ($v4)  use (&$PS__Data_Tuple, &$v3, &$PS__Data_Semigroup, &$dictSemigroup) {
                            return $PS__Data_Tuple['Tuple']['constructor']($v3['value0']($v4['value0']), $PS__Data_Semigroup['append']($dictSemigroup)($v3['value1'])($v4['value1']));
                        };
                    };
                    return $PS__Control_Apply['apply']($dictApply)($PS__Data_Functor['map']($dictApply['Functor0']())($k)($v))($v1);
                };
            });
        };
    };
    $bindWriterT = function ($dictSemigroup)  use (&$PS__Control_Bind, &$applyWriterT, &$PS__Data_Function, &$WriterT, &$PS__Data_Functor, &$PS__Data_Tuple, &$PS__Data_Semigroup) {
        return function ($dictBind)  use (&$PS__Control_Bind, &$applyWriterT, &$dictSemigroup, &$PS__Data_Function, &$WriterT, &$PS__Data_Functor, &$PS__Data_Tuple, &$PS__Data_Semigroup) {
            return $PS__Control_Bind['Bind'](function ()  use (&$applyWriterT, &$dictSemigroup, &$dictBind) {
                return $applyWriterT($dictSemigroup)($dictBind['Apply0']());
            }, function ($v)  use (&$PS__Data_Function, &$WriterT, &$PS__Control_Bind, &$dictBind, &$PS__Data_Functor, &$PS__Data_Tuple, &$PS__Data_Semigroup, &$dictSemigroup) {
                return function ($k)  use (&$PS__Data_Function, &$WriterT, &$PS__Control_Bind, &$dictBind, &$v, &$PS__Data_Functor, &$PS__Data_Tuple, &$PS__Data_Semigroup, &$dictSemigroup) {
                    return $PS__Data_Function['apply']($WriterT)($PS__Control_Bind['bind']($dictBind)($v)(function ($v1)  use (&$k, &$PS__Data_Functor, &$dictBind, &$PS__Data_Tuple, &$PS__Data_Semigroup, &$dictSemigroup) {
                        $v2 = $k($v1['value0']);
                        return $PS__Data_Functor['map'](($dictBind['Apply0']())['Functor0']())(function ($v3)  use (&$PS__Data_Tuple, &$PS__Data_Semigroup, &$dictSemigroup, &$v1) {
                            return $PS__Data_Tuple['Tuple']['constructor']($v3['value0'], $PS__Data_Semigroup['append']($dictSemigroup)($v1['value1'])($v3['value1']));
                        })($v2);
                    }));
                };
            });
        };
    };
    $applicativeWriterT = function ($dictMonoid)  use (&$PS__Control_Applicative, &$applyWriterT, &$PS__Data_Function, &$WriterT, &$PS__Data_Tuple, &$PS__Data_Monoid) {
        return function ($dictApplicative)  use (&$PS__Control_Applicative, &$applyWriterT, &$dictMonoid, &$PS__Data_Function, &$WriterT, &$PS__Data_Tuple, &$PS__Data_Monoid) {
            return $PS__Control_Applicative['Applicative'](function ()  use (&$applyWriterT, &$dictMonoid, &$dictApplicative) {
                return $applyWriterT($dictMonoid['Semigroup0']())($dictApplicative['Apply0']());
            }, function ($a)  use (&$PS__Data_Function, &$WriterT, &$PS__Control_Applicative, &$dictApplicative, &$PS__Data_Tuple, &$PS__Data_Monoid, &$dictMonoid) {
                return $PS__Data_Function['apply']($WriterT)($PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictApplicative))($PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Monoid['mempty']($dictMonoid))));
            });
        };
    };
    $monadWriterT = function ($dictMonoid)  use (&$PS__Control_Monad, &$applicativeWriterT, &$bindWriterT) {
        return function ($dictMonad)  use (&$PS__Control_Monad, &$applicativeWriterT, &$dictMonoid, &$bindWriterT) {
            return $PS__Control_Monad['Monad'](function ()  use (&$applicativeWriterT, &$dictMonoid, &$dictMonad) {
                return $applicativeWriterT($dictMonoid)($dictMonad['Applicative0']());
            }, function ()  use (&$bindWriterT, &$dictMonoid, &$dictMonad) {
                return $bindWriterT($dictMonoid['Semigroup0']())($dictMonad['Bind1']());
            });
        };
    };
    $monadAskWriterT = function ($dictMonoid)  use (&$PS__Control_Monad_Reader_Class, &$monadWriterT, &$PS__Control_Monad_Trans_Class, &$monadTransWriterT) {
        return function ($dictMonadAsk)  use (&$PS__Control_Monad_Reader_Class, &$monadWriterT, &$dictMonoid, &$PS__Control_Monad_Trans_Class, &$monadTransWriterT) {
            return $PS__Control_Monad_Reader_Class['MonadAsk'](function ()  use (&$monadWriterT, &$dictMonoid, &$dictMonadAsk) {
                return $monadWriterT($dictMonoid)($dictMonadAsk['Monad0']());
            }, $PS__Control_Monad_Trans_Class['lift']($monadTransWriterT($dictMonoid))($dictMonadAsk['Monad0']())($PS__Control_Monad_Reader_Class['ask']($dictMonadAsk)));
        };
    };
    $monadReaderWriterT = function ($dictMonoid)  use (&$PS__Control_Monad_Reader_Class, &$monadAskWriterT, &$mapWriterT) {
        return function ($dictMonadReader)  use (&$PS__Control_Monad_Reader_Class, &$monadAskWriterT, &$dictMonoid, &$mapWriterT) {
            return $PS__Control_Monad_Reader_Class['MonadReader'](function ()  use (&$monadAskWriterT, &$dictMonoid, &$dictMonadReader) {
                return $monadAskWriterT($dictMonoid)($dictMonadReader['MonadAsk0']());
            }, function ($f)  use (&$mapWriterT, &$PS__Control_Monad_Reader_Class, &$dictMonadReader) {
                return $mapWriterT($PS__Control_Monad_Reader_Class['local']($dictMonadReader)($f));
            });
        };
    };
    $monadContWriterT = function ($dictMonoid)  use (&$PS__Control_Monad_Cont_Class, &$monadWriterT, &$PS__Data_Function, &$WriterT, &$PS__Data_Tuple, &$PS__Data_Monoid) {
        return function ($dictMonadCont)  use (&$PS__Control_Monad_Cont_Class, &$monadWriterT, &$dictMonoid, &$PS__Data_Function, &$WriterT, &$PS__Data_Tuple, &$PS__Data_Monoid) {
            return $PS__Control_Monad_Cont_Class['MonadCont'](function ()  use (&$monadWriterT, &$dictMonoid, &$dictMonadCont) {
                return $monadWriterT($dictMonoid)($dictMonadCont['Monad0']());
            }, function ($f)  use (&$PS__Data_Function, &$WriterT, &$PS__Control_Monad_Cont_Class, &$dictMonadCont, &$PS__Data_Tuple, &$PS__Data_Monoid, &$dictMonoid) {
                return $PS__Data_Function['apply']($WriterT)($PS__Control_Monad_Cont_Class['callCC']($dictMonadCont)(function ($c)  use (&$f, &$PS__Data_Function, &$WriterT, &$PS__Data_Tuple, &$PS__Data_Monoid, &$dictMonoid) {
                    $v = $f(function ($a)  use (&$PS__Data_Function, &$WriterT, &$c, &$PS__Data_Tuple, &$PS__Data_Monoid, &$dictMonoid) {
                        return $PS__Data_Function['apply']($WriterT)($c($PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Monoid['mempty']($dictMonoid))));
                    });
                    return $v;
                }));
            });
        };
    };
    $monadEffectWriter = function ($dictMonoid)  use (&$PS__Effect_Class, &$monadWriterT, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransWriterT) {
        return function ($dictMonadEffect)  use (&$PS__Effect_Class, &$monadWriterT, &$dictMonoid, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransWriterT) {
            return $PS__Effect_Class['MonadEffect'](function ()  use (&$monadWriterT, &$dictMonoid, &$dictMonadEffect) {
                return $monadWriterT($dictMonoid)($dictMonadEffect['Monad0']());
            }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Trans_Class['lift']($monadTransWriterT($dictMonoid))($dictMonadEffect['Monad0']()))($PS__Effect_Class['liftEffect']($dictMonadEffect)));
        };
    };
    $monadRecWriterT = function ($dictMonoid)  use (&$PS__Control_Monad_Rec_Class, &$monadWriterT, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Data_Tuple, &$PS__Data_Semigroup, &$PS__Data_Function, &$WriterT, &$PS__Data_Monoid) {
        return function ($dictMonadRec)  use (&$PS__Control_Monad_Rec_Class, &$monadWriterT, &$dictMonoid, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Data_Tuple, &$PS__Data_Semigroup, &$PS__Data_Function, &$WriterT, &$PS__Data_Monoid) {
            return $PS__Control_Monad_Rec_Class['MonadRec'](function ()  use (&$monadWriterT, &$dictMonoid, &$dictMonadRec) {
                return $monadWriterT($dictMonoid)($dictMonadRec['Monad0']());
            }, function ($f)  use (&$PS__Control_Bind, &$dictMonadRec, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$PS__Data_Tuple, &$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Function, &$WriterT, &$PS__Data_Monoid) {
                return function ($a)  use (&$PS__Control_Bind, &$dictMonadRec, &$f, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$PS__Data_Tuple, &$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Function, &$WriterT, &$PS__Data_Monoid) {
                    $f__prime = function ($v)  use (&$PS__Control_Bind, &$dictMonadRec, &$f, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$PS__Data_Tuple, &$PS__Data_Semigroup, &$dictMonoid) {
                        return $PS__Control_Bind['bind'](($dictMonadRec['Monad0']())['Bind1']())((function ()  use (&$f, &$v) {
                            $v1 = $f($v['value0']);
                            return $v1;
                        })())(function ($v1)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$PS__Data_Tuple, &$PS__Data_Semigroup, &$dictMonoid, &$v) {
                            return $PS__Control_Applicative['pure'](($dictMonadRec['Monad0']())['Applicative0']())((function ()  use (&$v1, &$PS__Control_Monad_Rec_Class, &$PS__Data_Tuple, &$PS__Data_Semigroup, &$dictMonoid, &$v) {
                                if ($v1['value0']['type'] === 'Loop') {
                                    return $PS__Control_Monad_Rec_Class['Loop']['constructor']($PS__Data_Tuple['Tuple']['constructor']($v1['value0']['value0'], $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($v['value1'])($v1['value1'])));
                                };
                                if ($v1['value0']['type'] === 'Done') {
                                    return $PS__Control_Monad_Rec_Class['Done']['constructor']($PS__Data_Tuple['Tuple']['constructor']($v1['value0']['value0'], $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($v['value1'])($v1['value1'])));
                                };
                                throw new \Exception('Failed pattern match at Control.Monad.Writer.Trans line 83, column 16 - line 85, column 47: ' . var_dump([ $v1['value0']['constructor']['name'] ]));
                            })());
                        });
                    };
                    return $PS__Data_Function['apply']($WriterT)($PS__Control_Monad_Rec_Class['tailRecM']($dictMonadRec)($f__prime)($PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Monoid['mempty']($dictMonoid))));
                };
            });
        };
    };
    $monadStateWriterT = function ($dictMonoid)  use (&$PS__Control_Monad_State_Class, &$monadWriterT, &$PS__Control_Monad_Trans_Class, &$monadTransWriterT) {
        return function ($dictMonadState)  use (&$PS__Control_Monad_State_Class, &$monadWriterT, &$dictMonoid, &$PS__Control_Monad_Trans_Class, &$monadTransWriterT) {
            return $PS__Control_Monad_State_Class['MonadState'](function ()  use (&$monadWriterT, &$dictMonoid, &$dictMonadState) {
                return $monadWriterT($dictMonoid)($dictMonadState['Monad0']());
            }, function ($f)  use (&$PS__Control_Monad_Trans_Class, &$monadTransWriterT, &$dictMonoid, &$dictMonadState, &$PS__Control_Monad_State_Class) {
                return $PS__Control_Monad_Trans_Class['lift']($monadTransWriterT($dictMonoid))($dictMonadState['Monad0']())($PS__Control_Monad_State_Class['state']($dictMonadState)($f));
            });
        };
    };
    $monadTellWriterT = function ($dictMonoid)  use (&$PS__Control_Monad_Writer_Class, &$monadWriterT, &$PS__Control_Semigroupoid, &$WriterT, &$PS__Control_Applicative, &$PS__Data_Tuple, &$PS__Data_Unit) {
        return function ($dictMonad)  use (&$PS__Control_Monad_Writer_Class, &$monadWriterT, &$dictMonoid, &$PS__Control_Semigroupoid, &$WriterT, &$PS__Control_Applicative, &$PS__Data_Tuple, &$PS__Data_Unit) {
            return $PS__Control_Monad_Writer_Class['MonadTell'](function ()  use (&$monadWriterT, &$dictMonoid, &$dictMonad) {
                return $monadWriterT($dictMonoid)($dictMonad);
            }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($WriterT)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($PS__Data_Tuple['Tuple']['create']($PS__Data_Unit['unit']))));
        };
    };
    $monadWriterWriterT = function ($dictMonoid)  use (&$PS__Control_Monad_Writer_Class, &$monadTellWriterT, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
        return function ($dictMonad)  use (&$PS__Control_Monad_Writer_Class, &$monadTellWriterT, &$dictMonoid, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
            return $PS__Control_Monad_Writer_Class['MonadWriter'](function ()  use (&$monadTellWriterT, &$dictMonoid, &$dictMonad) {
                return $monadTellWriterT($dictMonoid)($dictMonad);
            }, function ($v)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
                return $PS__Control_Bind['bind']($dictMonad['Bind1']())($v)(function ($v1)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_Tuple) {
                    return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($PS__Data_Tuple['Tuple']['constructor']($PS__Data_Tuple['Tuple']['constructor']($v1['value0'], $v1['value1']), $v1['value1']));
                });
            }, function ($v)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
                return $PS__Control_Bind['bind']($dictMonad['Bind1']())($v)(function ($v1)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_Tuple) {
                    return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($PS__Data_Tuple['Tuple']['constructor']($v1['value0']['value0'], $v1['value0']['value1']($v1['value1'])));
                });
            });
        };
    };
    $monadThrowWriterT = function ($dictMonoid)  use (&$PS__Control_Monad_Error_Class, &$monadWriterT, &$PS__Control_Monad_Trans_Class, &$monadTransWriterT) {
        return function ($dictMonadThrow)  use (&$PS__Control_Monad_Error_Class, &$monadWriterT, &$dictMonoid, &$PS__Control_Monad_Trans_Class, &$monadTransWriterT) {
            return $PS__Control_Monad_Error_Class['MonadThrow'](function ()  use (&$monadWriterT, &$dictMonoid, &$dictMonadThrow) {
                return $monadWriterT($dictMonoid)($dictMonadThrow['Monad0']());
            }, function ($e)  use (&$PS__Control_Monad_Trans_Class, &$monadTransWriterT, &$dictMonoid, &$dictMonadThrow, &$PS__Control_Monad_Error_Class) {
                return $PS__Control_Monad_Trans_Class['lift']($monadTransWriterT($dictMonoid))($dictMonadThrow['Monad0']())($PS__Control_Monad_Error_Class['throwError']($dictMonadThrow)($e));
            });
        };
    };
    $monadErrorWriterT = function ($dictMonoid)  use (&$PS__Control_Monad_Error_Class, &$monadThrowWriterT, &$PS__Data_Function, &$WriterT) {
        return function ($dictMonadError)  use (&$PS__Control_Monad_Error_Class, &$monadThrowWriterT, &$dictMonoid, &$PS__Data_Function, &$WriterT) {
            return $PS__Control_Monad_Error_Class['MonadError'](function ()  use (&$monadThrowWriterT, &$dictMonoid, &$dictMonadError) {
                return $monadThrowWriterT($dictMonoid)($dictMonadError['MonadThrow0']());
            }, function ($v)  use (&$PS__Data_Function, &$WriterT, &$PS__Control_Monad_Error_Class, &$dictMonadError) {
                return function ($h)  use (&$PS__Data_Function, &$WriterT, &$PS__Control_Monad_Error_Class, &$dictMonadError, &$v) {
                    return $PS__Data_Function['apply']($WriterT)($PS__Control_Monad_Error_Class['catchError']($dictMonadError)($v)(function ($e)  use (&$h) {
                        $v1 = $h($e);
                        return $v1;
                    }));
                };
            });
        };
    };
    $altWriterT = function ($dictAlt)  use (&$PS__Control_Alt, &$functorWriterT) {
        return $PS__Control_Alt['Alt'](function ()  use (&$functorWriterT, &$dictAlt) {
            return $functorWriterT($dictAlt['Functor0']());
        }, function ($v)  use (&$PS__Control_Alt, &$dictAlt) {
            return function ($v1)  use (&$PS__Control_Alt, &$dictAlt, &$v) {
                return $PS__Control_Alt['alt']($dictAlt)($v)($v1);
            };
        });
    };
    $plusWriterT = function ($dictPlus)  use (&$PS__Control_Plus, &$altWriterT) {
        return $PS__Control_Plus['Plus'](function ()  use (&$altWriterT, &$dictPlus) {
            return $altWriterT($dictPlus['Alt0']());
        }, $PS__Control_Plus['empty']($dictPlus));
    };
    $alternativeWriterT = function ($dictMonoid)  use (&$PS__Control_Alternative, &$applicativeWriterT, &$plusWriterT) {
        return function ($dictAlternative)  use (&$PS__Control_Alternative, &$applicativeWriterT, &$dictMonoid, &$plusWriterT) {
            return $PS__Control_Alternative['Alternative'](function ()  use (&$applicativeWriterT, &$dictMonoid, &$dictAlternative) {
                return $applicativeWriterT($dictMonoid)($dictAlternative['Applicative0']());
            }, function ()  use (&$plusWriterT, &$dictAlternative) {
                return $plusWriterT($dictAlternative['Plus1']());
            });
        };
    };
    $monadZeroWriterT = function ($dictMonoid)  use (&$PS__Control_MonadZero, &$alternativeWriterT, &$monadWriterT) {
        return function ($dictMonadZero)  use (&$PS__Control_MonadZero, &$alternativeWriterT, &$dictMonoid, &$monadWriterT) {
            return $PS__Control_MonadZero['MonadZero'](function ()  use (&$alternativeWriterT, &$dictMonoid, &$dictMonadZero) {
                return $alternativeWriterT($dictMonoid)($dictMonadZero['Alternative1']());
            }, function ()  use (&$monadWriterT, &$dictMonoid, &$dictMonadZero) {
                return $monadWriterT($dictMonoid)($dictMonadZero['Monad0']());
            });
        };
    };
    $monadPlusWriterT = function ($dictMonoid)  use (&$PS__Control_MonadPlus, &$monadZeroWriterT) {
        return function ($dictMonadPlus)  use (&$PS__Control_MonadPlus, &$monadZeroWriterT, &$dictMonoid) {
            return $PS__Control_MonadPlus['MonadPlus'](function ()  use (&$monadZeroWriterT, &$dictMonoid, &$dictMonadPlus) {
                return $monadZeroWriterT($dictMonoid)($dictMonadPlus['MonadZero0']());
            });
        };
    };
    return [
        'WriterT' => $WriterT,
        'runWriterT' => $runWriterT,
        'execWriterT' => $execWriterT,
        'mapWriterT' => $mapWriterT,
        'newtypeWriterT' => $newtypeWriterT,
        'functorWriterT' => $functorWriterT,
        'applyWriterT' => $applyWriterT,
        'applicativeWriterT' => $applicativeWriterT,
        'altWriterT' => $altWriterT,
        'plusWriterT' => $plusWriterT,
        'alternativeWriterT' => $alternativeWriterT,
        'bindWriterT' => $bindWriterT,
        'monadWriterT' => $monadWriterT,
        'monadRecWriterT' => $monadRecWriterT,
        'monadZeroWriterT' => $monadZeroWriterT,
        'monadPlusWriterT' => $monadPlusWriterT,
        'monadTransWriterT' => $monadTransWriterT,
        'monadEffectWriter' => $monadEffectWriter,
        'monadContWriterT' => $monadContWriterT,
        'monadThrowWriterT' => $monadThrowWriterT,
        'monadErrorWriterT' => $monadErrorWriterT,
        'monadAskWriterT' => $monadAskWriterT,
        'monadReaderWriterT' => $monadReaderWriterT,
        'monadStateWriterT' => $monadStateWriterT,
        'monadTellWriterT' => $monadTellWriterT,
        'monadWriterWriterT' => $monadWriterWriterT
    ];
})();
