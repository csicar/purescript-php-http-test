<?php
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
$PS__Data_Functor_Invariant = (function ()  use (&$PS__Data_Functor) {
    $Invariant = function ($imap) {
        return [
            'imap' => $imap
        ];
    };
    $imapF = function ($dictFunctor)  use (&$PS__Data_Functor) {
        return function ($f)  use (&$PS__Data_Functor, &$dictFunctor) {
            return function ($v)  use (&$PS__Data_Functor, &$dictFunctor, &$f) {
                return $PS__Data_Functor['map']($dictFunctor)($f);
            };
        };
    };
    $invariantArray = $Invariant($imapF($PS__Data_Functor['functorArray']));
    $invariantFn = $Invariant($imapF($PS__Data_Functor['functorFn']));
    $imap = function ($dict) {
        return $dict['imap'];
    };
    return [
        'imap' => $imap,
        'Invariant' => $Invariant,
        'imapF' => $imapF,
        'invariantFn' => $invariantFn,
        'invariantArray' => $invariantArray
    ];
})();
