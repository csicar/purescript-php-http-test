<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Additive/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Conj/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Disj/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Dual/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Endo/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Multiplicative/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.First/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Last/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Newtype = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_Function, &$PS__Data_Monoid_Multiplicative, &$PS__Data_Semigroup_Last, &$PS__Data_Semigroup_First, &$PS__Data_Monoid_Endo, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Disj, &$PS__Data_Monoid_Conj, &$PS__Data_Monoid_Additive) {
    $Newtype = function ($unwrap, $wrap) {
        return [
            'unwrap' => $unwrap,
            'wrap' => $wrap
        ];
    };
    $wrap = function ($dict) {
        return $dict['wrap'];
    };
    $unwrap = function ($dict) {
        return $dict['unwrap'];
    };
    $underF2 = function ($dictFunctor)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$unwrap, &$PS__Data_Function, &$wrap) {
        return function ($dictFunctor1)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$unwrap, &$PS__Data_Function, &$dictFunctor, &$wrap) {
            return function ($dictNewtype)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$unwrap, &$PS__Data_Function, &$dictFunctor, &$wrap) {
                return function ($dictNewtype1)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$unwrap, &$PS__Data_Function, &$dictFunctor, &$wrap, &$dictNewtype) {
                    return function ($v)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$unwrap, &$dictNewtype1, &$PS__Data_Function, &$dictFunctor, &$wrap, &$dictNewtype) {
                        return function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$unwrap, &$dictNewtype1, &$PS__Data_Function, &$dictFunctor, &$wrap, &$dictNewtype) {
                            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($dictFunctor1)($unwrap($dictNewtype1))))($PS__Data_Function['on']($f)($PS__Data_Functor['map']($dictFunctor)($wrap($dictNewtype))));
                        };
                    };
                };
            };
        };
    };
    $underF = function ($dictFunctor)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$unwrap, &$wrap) {
        return function ($dictFunctor1)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$unwrap, &$dictFunctor, &$wrap) {
            return function ($dictNewtype)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$unwrap, &$dictFunctor, &$wrap) {
                return function ($dictNewtype1)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$unwrap, &$dictFunctor, &$wrap, &$dictNewtype) {
                    return function ($v)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$unwrap, &$dictNewtype1, &$dictFunctor, &$wrap, &$dictNewtype) {
                        return function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$unwrap, &$dictNewtype1, &$dictFunctor, &$wrap, &$dictNewtype) {
                            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($dictFunctor1)($unwrap($dictNewtype1)))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($PS__Data_Functor['map']($dictFunctor)($wrap($dictNewtype))));
                        };
                    };
                };
            };
        };
    };
    $under2 = function ($dictNewtype)  use (&$PS__Control_Semigroupoid, &$unwrap, &$PS__Data_Function, &$wrap) {
        return function ($dictNewtype1)  use (&$PS__Control_Semigroupoid, &$unwrap, &$PS__Data_Function, &$wrap, &$dictNewtype) {
            return function ($v)  use (&$PS__Control_Semigroupoid, &$unwrap, &$dictNewtype1, &$PS__Data_Function, &$wrap, &$dictNewtype) {
                return function ($f)  use (&$PS__Control_Semigroupoid, &$unwrap, &$dictNewtype1, &$PS__Data_Function, &$wrap, &$dictNewtype) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unwrap($dictNewtype1)))($PS__Data_Function['on']($f)($wrap($dictNewtype)));
                };
            };
        };
    };
    $under = function ($dictNewtype)  use (&$PS__Control_Semigroupoid, &$unwrap, &$wrap) {
        return function ($dictNewtype1)  use (&$PS__Control_Semigroupoid, &$unwrap, &$wrap, &$dictNewtype) {
            return function ($v)  use (&$PS__Control_Semigroupoid, &$unwrap, &$dictNewtype1, &$wrap, &$dictNewtype) {
                return function ($f)  use (&$PS__Control_Semigroupoid, &$unwrap, &$dictNewtype1, &$wrap, &$dictNewtype) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unwrap($dictNewtype1))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($wrap($dictNewtype)));
                };
            };
        };
    };
    $un = function ($dictNewtype)  use (&$unwrap) {
        return function ($v)  use (&$unwrap, &$dictNewtype) {
            return $unwrap($dictNewtype);
        };
    };
    $traverse = function ($dictFunctor)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$wrap, &$unwrap) {
        return function ($dictNewtype)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor, &$wrap, &$unwrap) {
            return function ($v)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor, &$wrap, &$dictNewtype, &$unwrap) {
                return function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor, &$wrap, &$dictNewtype, &$unwrap) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($dictFunctor)($wrap($dictNewtype)))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($unwrap($dictNewtype)));
                };
            };
        };
    };
    $overF2 = function ($dictFunctor)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$wrap, &$PS__Data_Function, &$unwrap) {
        return function ($dictFunctor1)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$wrap, &$PS__Data_Function, &$dictFunctor, &$unwrap) {
            return function ($dictNewtype)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$wrap, &$PS__Data_Function, &$dictFunctor, &$unwrap) {
                return function ($dictNewtype1)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$wrap, &$PS__Data_Function, &$dictFunctor, &$unwrap, &$dictNewtype) {
                    return function ($v)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$wrap, &$dictNewtype1, &$PS__Data_Function, &$dictFunctor, &$unwrap, &$dictNewtype) {
                        return function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$wrap, &$dictNewtype1, &$PS__Data_Function, &$dictFunctor, &$unwrap, &$dictNewtype) {
                            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($dictFunctor1)($wrap($dictNewtype1))))($PS__Data_Function['on']($f)($PS__Data_Functor['map']($dictFunctor)($unwrap($dictNewtype))));
                        };
                    };
                };
            };
        };
    };
    $overF = function ($dictFunctor)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$wrap, &$unwrap) {
        return function ($dictFunctor1)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$wrap, &$dictFunctor, &$unwrap) {
            return function ($dictNewtype)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$wrap, &$dictFunctor, &$unwrap) {
                return function ($dictNewtype1)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$wrap, &$dictFunctor, &$unwrap, &$dictNewtype) {
                    return function ($v)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$wrap, &$dictNewtype1, &$dictFunctor, &$unwrap, &$dictNewtype) {
                        return function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$wrap, &$dictNewtype1, &$dictFunctor, &$unwrap, &$dictNewtype) {
                            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($dictFunctor1)($wrap($dictNewtype1)))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($PS__Data_Functor['map']($dictFunctor)($unwrap($dictNewtype))));
                        };
                    };
                };
            };
        };
    };
    $over2 = function ($dictNewtype)  use (&$PS__Control_Semigroupoid, &$wrap, &$PS__Data_Function, &$unwrap) {
        return function ($dictNewtype1)  use (&$PS__Control_Semigroupoid, &$wrap, &$PS__Data_Function, &$unwrap, &$dictNewtype) {
            return function ($v)  use (&$PS__Control_Semigroupoid, &$wrap, &$dictNewtype1, &$PS__Data_Function, &$unwrap, &$dictNewtype) {
                return function ($f)  use (&$PS__Control_Semigroupoid, &$wrap, &$dictNewtype1, &$PS__Data_Function, &$unwrap, &$dictNewtype) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($wrap($dictNewtype1)))($PS__Data_Function['on']($f)($unwrap($dictNewtype)));
                };
            };
        };
    };
    $over = function ($dictNewtype)  use (&$PS__Control_Semigroupoid, &$wrap, &$unwrap) {
        return function ($dictNewtype1)  use (&$PS__Control_Semigroupoid, &$wrap, &$unwrap, &$dictNewtype) {
            return function ($v)  use (&$PS__Control_Semigroupoid, &$wrap, &$dictNewtype1, &$unwrap, &$dictNewtype) {
                return function ($f)  use (&$PS__Control_Semigroupoid, &$wrap, &$dictNewtype1, &$unwrap, &$dictNewtype) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($wrap($dictNewtype1))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($unwrap($dictNewtype)));
                };
            };
        };
    };
    $op = function ($dictNewtype)  use (&$un) {
        return $un($dictNewtype);
    };
    $newtypeMultiplicative = $Newtype(function ($v) {
        return $v;
    }, $PS__Data_Monoid_Multiplicative['Multiplicative']);
    $newtypeLast = $Newtype(function ($v) {
        return $v;
    }, $PS__Data_Semigroup_Last['Last']);
    $newtypeFirst = $Newtype(function ($v) {
        return $v;
    }, $PS__Data_Semigroup_First['First']);
    $newtypeEndo = $Newtype(function ($v) {
        return $v;
    }, $PS__Data_Monoid_Endo['Endo']);
    $newtypeDual = $Newtype(function ($v) {
        return $v;
    }, $PS__Data_Monoid_Dual['Dual']);
    $newtypeDisj = $Newtype(function ($v) {
        return $v;
    }, $PS__Data_Monoid_Disj['Disj']);
    $newtypeConj = $Newtype(function ($v) {
        return $v;
    }, $PS__Data_Monoid_Conj['Conj']);
    $newtypeAdditive = $Newtype(function ($v) {
        return $v;
    }, $PS__Data_Monoid_Additive['Additive']);
    $collect = function ($dictFunctor)  use (&$PS__Control_Semigroupoid, &$wrap, &$PS__Data_Functor, &$unwrap) {
        return function ($dictNewtype)  use (&$PS__Control_Semigroupoid, &$wrap, &$PS__Data_Functor, &$dictFunctor, &$unwrap) {
            return function ($v)  use (&$PS__Control_Semigroupoid, &$wrap, &$dictNewtype, &$PS__Data_Functor, &$dictFunctor, &$unwrap) {
                return function ($f)  use (&$PS__Control_Semigroupoid, &$wrap, &$dictNewtype, &$PS__Data_Functor, &$dictFunctor, &$unwrap) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($wrap($dictNewtype))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($PS__Data_Functor['map']($dictFunctor)($unwrap($dictNewtype))));
                };
            };
        };
    };
    $alaF = function ($dictFunctor)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$unwrap, &$wrap) {
        return function ($dictFunctor1)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$unwrap, &$dictFunctor, &$wrap) {
            return function ($dictNewtype)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$unwrap, &$dictFunctor, &$wrap) {
                return function ($dictNewtype1)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$unwrap, &$dictFunctor, &$wrap, &$dictNewtype) {
                    return function ($v)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$unwrap, &$dictNewtype1, &$dictFunctor, &$wrap, &$dictNewtype) {
                        return function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor1, &$unwrap, &$dictNewtype1, &$dictFunctor, &$wrap, &$dictNewtype) {
                            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($dictFunctor1)($unwrap($dictNewtype1)))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($PS__Data_Functor['map']($dictFunctor)($wrap($dictNewtype))));
                        };
                    };
                };
            };
        };
    };
    $ala = function ($dictFunctor)  use (&$PS__Data_Functor, &$unwrap, &$wrap) {
        return function ($dictNewtype)  use (&$PS__Data_Functor, &$dictFunctor, &$unwrap, &$wrap) {
            return function ($dictNewtype1)  use (&$PS__Data_Functor, &$dictFunctor, &$unwrap, &$dictNewtype, &$wrap) {
                return function ($v)  use (&$PS__Data_Functor, &$dictFunctor, &$unwrap, &$dictNewtype, &$wrap, &$dictNewtype1) {
                    return function ($f)  use (&$PS__Data_Functor, &$dictFunctor, &$unwrap, &$dictNewtype, &$wrap, &$dictNewtype1) {
                        return $PS__Data_Functor['map']($dictFunctor)($unwrap($dictNewtype))($f($wrap($dictNewtype1)));
                    };
                };
            };
        };
    };
    return [
        'unwrap' => $unwrap,
        'wrap' => $wrap,
        'Newtype' => $Newtype,
        'un' => $un,
        'op' => $op,
        'ala' => $ala,
        'alaF' => $alaF,
        'over' => $over,
        'overF' => $overF,
        'under' => $under,
        'underF' => $underF,
        'over2' => $over2,
        'overF2' => $overF2,
        'under2' => $under2,
        'underF2' => $underF2,
        'traverse' => $traverse,
        'collect' => $collect,
        'newtypeAdditive' => $newtypeAdditive,
        'newtypeMultiplicative' => $newtypeMultiplicative,
        'newtypeConj' => $newtypeConj,
        'newtypeDisj' => $newtypeDisj,
        'newtypeDual' => $newtypeDual,
        'newtypeEndo' => $newtypeEndo,
        'newtypeFirst' => $newtypeFirst,
        'newtypeLast' => $newtypeLast
    ];
})();
