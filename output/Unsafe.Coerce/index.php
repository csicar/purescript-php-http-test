<?php
$PS__Unsafe_Coerce = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    return [
        'unsafeCoerce' => $__foreign['unsafeCoerce']
    ];
})();
