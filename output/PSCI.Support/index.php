<?php
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Effect/index.php');
require_once(dirname(__FILE__) . '/../Effect.Console/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__PSCI_Support = (function ()  use (&$PS__Effect_Console, &$PS__Control_Bind, &$PS__Effect) {
    $Eval = function ($__eval) {
        return [
            'eval' => $__eval
        ];
    };
    $evalShow = function ($dictShow)  use (&$Eval, &$PS__Effect_Console) {
        return $Eval($PS__Effect_Console['logShow']($dictShow));
    };
    $__eval = function ($dict) {
        return $dict['eval'];
    };
    $evalEffect = function ($dictEval)  use (&$Eval, &$PS__Control_Bind, &$PS__Effect, &$__eval) {
        return $Eval(function ($x)  use (&$PS__Control_Bind, &$PS__Effect, &$__eval, &$dictEval) {
            return $PS__Control_Bind['bind']($PS__Effect['bindEffect'])($x)($__eval($dictEval));
        });
    };
    return [
        'eval' => $__eval,
        'Eval' => $Eval,
        'evalEffect' => $evalEffect,
        'evalShow' => $evalShow
    ];
})();
