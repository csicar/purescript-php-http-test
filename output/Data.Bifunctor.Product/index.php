<?php
require_once(dirname(__FILE__) . '/../Control.Biapplicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Biapply/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Bifunctor_Product = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Eq, &$PS__Data_HeytingAlgebra, &$PS__Data_Ord, &$PS__Data_Ordering, &$PS__Data_Bifunctor, &$PS__Control_Biapply, &$PS__Control_Biapplicative) {
    $Product = [
        'constructor' => function ($value0, $value1) {
            return [
                'type' => 'Product',
                'value0' => $value0,
                'value1' => $value1
            ];
        },
        'create' => function ($value0)  use (&$Product) {
            return function ($value1)  use (&$Product, &$value0) {
                return $Product['constructor']($value0, $value1);
            };
        }
    ];
    $showProduct = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return function ($dictShow1)  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$dictShow) {
            return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow, &$dictShow1) {
                return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Product ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v['value0']))($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])(' ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow1)($v['value1']))(')'))));
            });
        };
    };
    $eqProduct = function ($dictEq)  use (&$PS__Data_Eq, &$PS__Data_HeytingAlgebra) {
        return function ($dictEq1)  use (&$PS__Data_Eq, &$PS__Data_HeytingAlgebra, &$dictEq) {
            return $PS__Data_Eq['Eq'](function ($x)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$dictEq, &$dictEq1) {
                return function ($y)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$dictEq, &$x, &$dictEq1) {
                    return $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Eq['eq']($dictEq)($x['value0'])($y['value0']))($PS__Data_Eq['eq']($dictEq1)($x['value1'])($y['value1']));
                };
            });
        };
    };
    $ordProduct = function ($dictOrd)  use (&$PS__Data_Ord, &$eqProduct, &$PS__Data_Ordering) {
        return function ($dictOrd1)  use (&$PS__Data_Ord, &$eqProduct, &$dictOrd, &$PS__Data_Ordering) {
            return $PS__Data_Ord['Ord'](function ()  use (&$eqProduct, &$dictOrd, &$dictOrd1) {
                return $eqProduct($dictOrd['Eq0']())($dictOrd1['Eq0']());
            }, function ($x)  use (&$PS__Data_Ord, &$dictOrd, &$PS__Data_Ordering, &$dictOrd1) {
                return function ($y)  use (&$PS__Data_Ord, &$dictOrd, &$x, &$PS__Data_Ordering, &$dictOrd1) {
                    $v = $PS__Data_Ord['compare']($dictOrd)($x['value0'])($y['value0']);
                    if ($v['type'] === 'LT') {
                        return $PS__Data_Ordering['LT']();
                    };
                    if ($v['type'] === 'GT') {
                        return $PS__Data_Ordering['GT']();
                    };
                    return $PS__Data_Ord['compare']($dictOrd1)($x['value1'])($y['value1']);
                };
            });
        };
    };
    $bifunctorProduct = function ($dictBifunctor)  use (&$PS__Data_Bifunctor, &$Product) {
        return function ($dictBifunctor1)  use (&$PS__Data_Bifunctor, &$Product, &$dictBifunctor) {
            return $PS__Data_Bifunctor['Bifunctor'](function ($f)  use (&$Product, &$PS__Data_Bifunctor, &$dictBifunctor, &$dictBifunctor1) {
                return function ($g)  use (&$Product, &$PS__Data_Bifunctor, &$dictBifunctor, &$f, &$dictBifunctor1) {
                    return function ($v)  use (&$Product, &$PS__Data_Bifunctor, &$dictBifunctor, &$f, &$g, &$dictBifunctor1) {
                        return $Product['constructor']($PS__Data_Bifunctor['bimap']($dictBifunctor)($f)($g)($v['value0']), $PS__Data_Bifunctor['bimap']($dictBifunctor1)($f)($g)($v['value1']));
                    };
                };
            });
        };
    };
    $biapplyProduct = function ($dictBiapply)  use (&$PS__Control_Biapply, &$bifunctorProduct, &$Product) {
        return function ($dictBiapply1)  use (&$PS__Control_Biapply, &$bifunctorProduct, &$dictBiapply, &$Product) {
            return $PS__Control_Biapply['Biapply'](function ()  use (&$bifunctorProduct, &$dictBiapply, &$dictBiapply1) {
                return $bifunctorProduct($dictBiapply['Bifunctor0']())($dictBiapply1['Bifunctor0']());
            }, function ($v)  use (&$Product, &$PS__Control_Biapply, &$dictBiapply, &$dictBiapply1) {
                return function ($v1)  use (&$Product, &$PS__Control_Biapply, &$dictBiapply, &$v, &$dictBiapply1) {
                    return $Product['constructor']($PS__Control_Biapply['biapply']($dictBiapply)($v['value0'])($v1['value0']), $PS__Control_Biapply['biapply']($dictBiapply1)($v['value1'])($v1['value1']));
                };
            });
        };
    };
    $biapplicativeProduct = function ($dictBiapplicative)  use (&$PS__Control_Biapplicative, &$biapplyProduct, &$Product) {
        return function ($dictBiapplicative1)  use (&$PS__Control_Biapplicative, &$biapplyProduct, &$dictBiapplicative, &$Product) {
            return $PS__Control_Biapplicative['Biapplicative'](function ()  use (&$biapplyProduct, &$dictBiapplicative, &$dictBiapplicative1) {
                return $biapplyProduct($dictBiapplicative['Biapply0']())($dictBiapplicative1['Biapply0']());
            }, function ($a)  use (&$Product, &$PS__Control_Biapplicative, &$dictBiapplicative, &$dictBiapplicative1) {
                return function ($b)  use (&$Product, &$PS__Control_Biapplicative, &$dictBiapplicative, &$a, &$dictBiapplicative1) {
                    return $Product['constructor']($PS__Control_Biapplicative['bipure']($dictBiapplicative)($a)($b), $PS__Control_Biapplicative['bipure']($dictBiapplicative1)($a)($b));
                };
            });
        };
    };
    return [
        'Product' => $Product,
        'eqProduct' => $eqProduct,
        'ordProduct' => $ordProduct,
        'showProduct' => $showProduct,
        'bifunctorProduct' => $bifunctorProduct,
        'biapplyProduct' => $biapplyProduct,
        'biapplicativeProduct' => $biapplicativeProduct
    ];
})();
