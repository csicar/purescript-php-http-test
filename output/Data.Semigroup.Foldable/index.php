<?php
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Dual/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Multiplicative/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord.Max/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord.Min/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Semigroup_Foldable = (function ()  use (&$PS__Data_Semigroup, &$PS__Data_Function, &$PS__Control_Apply, &$PS__Control_Semigroupoid, &$PS__Control_Category, &$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Ord_Max, &$PS__Data_Ord_Min, &$PS__Data_Unit, &$PS__Data_Foldable) {
    $JoinWith = function ($x) {
        return $x;
    };
    $Act = function ($x) {
        return $x;
    };
    $Foldable1 = function ($Foldable0, $fold1, $foldMap1) {
        return [
            'Foldable0' => $Foldable0,
            'fold1' => $fold1,
            'foldMap1' => $foldMap1
        ];
    };
    $semigroupJoinWith = function ($dictSemigroup)  use (&$PS__Data_Semigroup, &$PS__Data_Function, &$JoinWith) {
        return $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$PS__Data_Function, &$JoinWith, &$PS__Data_Semigroup, &$dictSemigroup) {
            return function ($v1)  use (&$PS__Data_Function, &$JoinWith, &$PS__Data_Semigroup, &$dictSemigroup, &$v) {
                return $PS__Data_Function['apply']($JoinWith)(function ($j)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$v, &$v1) {
                    return $PS__Data_Semigroup['append']($dictSemigroup)($v($j))($PS__Data_Semigroup['append']($dictSemigroup)($j)($v1($j)));
                });
            };
        });
    };
    $semigroupAct = function ($dictApply)  use (&$PS__Data_Semigroup, &$PS__Control_Apply) {
        return $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$PS__Control_Apply, &$dictApply) {
            return function ($v1)  use (&$PS__Control_Apply, &$dictApply, &$v) {
                return $PS__Control_Apply['applySecond']($dictApply)($v)($v1);
            };
        });
    };
    $joinee = function ($v) {
        return $v;
    };
    $getAct = function ($v) {
        return $v;
    };
    $foldMap1 = function ($dict) {
        return $dict['foldMap1'];
    };
    $intercalateMap = function ($dictFoldable1)  use (&$joinee, &$foldMap1, &$semigroupJoinWith, &$PS__Control_Semigroupoid, &$JoinWith, &$PS__Data_Function) {
        return function ($dictSemigroup)  use (&$joinee, &$foldMap1, &$dictFoldable1, &$semigroupJoinWith, &$PS__Control_Semigroupoid, &$JoinWith, &$PS__Data_Function) {
            return function ($j)  use (&$joinee, &$foldMap1, &$dictFoldable1, &$semigroupJoinWith, &$dictSemigroup, &$PS__Control_Semigroupoid, &$JoinWith, &$PS__Data_Function) {
                return function ($f)  use (&$joinee, &$foldMap1, &$dictFoldable1, &$semigroupJoinWith, &$dictSemigroup, &$PS__Control_Semigroupoid, &$JoinWith, &$PS__Data_Function, &$j) {
                    return function ($foldable)  use (&$joinee, &$foldMap1, &$dictFoldable1, &$semigroupJoinWith, &$dictSemigroup, &$PS__Control_Semigroupoid, &$JoinWith, &$PS__Data_Function, &$f, &$j) {
                        return $joinee($foldMap1($dictFoldable1)($semigroupJoinWith($dictSemigroup))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($JoinWith)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Function['const'])($f)))($foldable))($j);
                    };
                };
            };
        };
    };
    $intercalate = function ($dictFoldable1)  use (&$PS__Data_Function, &$intercalateMap, &$PS__Control_Category) {
        return function ($dictSemigroup)  use (&$PS__Data_Function, &$intercalateMap, &$dictFoldable1, &$PS__Control_Category) {
            return $PS__Data_Function['flip']($intercalateMap($dictFoldable1)($dictSemigroup))($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        };
    };
    $maximum = function ($dictOrd)  use (&$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Ord_Max, &$foldMap1) {
        return function ($dictFoldable1)  use (&$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Ord_Max, &$foldMap1, &$dictOrd) {
            return $PS__Data_Newtype['ala']($PS__Data_Functor['functorFn'])($PS__Data_Ord_Max['newtypeMax'])($PS__Data_Ord_Max['newtypeMax'])($PS__Data_Ord_Max['Max'])($foldMap1($dictFoldable1)($PS__Data_Ord_Max['semigroupMax']($dictOrd)));
        };
    };
    $minimum = function ($dictOrd)  use (&$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Ord_Min, &$foldMap1) {
        return function ($dictFoldable1)  use (&$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Ord_Min, &$foldMap1, &$dictOrd) {
            return $PS__Data_Newtype['ala']($PS__Data_Functor['functorFn'])($PS__Data_Ord_Min['newtypeMin'])($PS__Data_Ord_Min['newtypeMin'])($PS__Data_Ord_Min['Min'])($foldMap1($dictFoldable1)($PS__Data_Ord_Min['semigroupMin']($dictOrd)));
        };
    };
    $traverse1_ = function ($dictFoldable1)  use (&$PS__Data_Functor, &$PS__Data_Unit, &$getAct, &$foldMap1, &$semigroupAct, &$PS__Control_Semigroupoid, &$Act) {
        return function ($dictApply)  use (&$PS__Data_Functor, &$PS__Data_Unit, &$getAct, &$foldMap1, &$dictFoldable1, &$semigroupAct, &$PS__Control_Semigroupoid, &$Act) {
            return function ($f)  use (&$PS__Data_Functor, &$dictApply, &$PS__Data_Unit, &$getAct, &$foldMap1, &$dictFoldable1, &$semigroupAct, &$PS__Control_Semigroupoid, &$Act) {
                return function ($t)  use (&$PS__Data_Functor, &$dictApply, &$PS__Data_Unit, &$getAct, &$foldMap1, &$dictFoldable1, &$semigroupAct, &$PS__Control_Semigroupoid, &$Act, &$f) {
                    return $PS__Data_Functor['voidRight']($dictApply['Functor0']())($PS__Data_Unit['unit'])($getAct($foldMap1($dictFoldable1)($semigroupAct($dictApply))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($Act)($f))($t)));
                };
            };
        };
    };
    $for1_ = function ($dictFoldable1)  use (&$PS__Data_Function, &$traverse1_) {
        return function ($dictApply)  use (&$PS__Data_Function, &$traverse1_, &$dictFoldable1) {
            return $PS__Data_Function['flip']($traverse1_($dictFoldable1)($dictApply));
        };
    };
    $sequence1_ = function ($dictFoldable1)  use (&$traverse1_, &$PS__Control_Category) {
        return function ($dictApply)  use (&$traverse1_, &$dictFoldable1, &$PS__Control_Category) {
            return $traverse1_($dictFoldable1)($dictApply)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        };
    };
    $fold1Default = function ($dictFoldable1)  use (&$foldMap1, &$PS__Control_Category) {
        return function ($dictSemigroup)  use (&$foldMap1, &$dictFoldable1, &$PS__Control_Category) {
            return $foldMap1($dictFoldable1)($dictSemigroup)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        };
    };
    $foldableDual = $Foldable1(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableDual'];
    }, function ($dictSemigroup)  use (&$fold1Default, &$foldableDual) {
        return $fold1Default($foldableDual)($dictSemigroup);
    }, function ($dictSemigroup) {
        return function ($f) {
            return function ($v)  use (&$f) {
                return $f($v);
            };
        };
    });
    $foldableMultiplicative = $Foldable1(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableMultiplicative'];
    }, function ($dictSemigroup)  use (&$fold1Default, &$foldableMultiplicative) {
        return $fold1Default($foldableMultiplicative)($dictSemigroup);
    }, function ($dictSemigroup) {
        return function ($f) {
            return function ($v)  use (&$f) {
                return $f($v);
            };
        };
    });
    $fold1 = function ($dict) {
        return $dict['fold1'];
    };
    $foldMap1Default = function ($dictFoldable1)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$fold1) {
        return function ($dictFunctor)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$fold1, &$dictFoldable1) {
            return function ($dictSemigroup)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor, &$fold1, &$dictFoldable1) {
                return function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictFunctor, &$fold1, &$dictFoldable1, &$dictSemigroup) {
                    return $PS__Control_Semigroupoid['composeFlipped']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($dictFunctor)($f))($fold1($dictFoldable1)($dictSemigroup));
                };
            };
        };
    };
    return [
        'Foldable1' => $Foldable1,
        'foldMap1' => $foldMap1,
        'fold1' => $fold1,
        'traverse1_' => $traverse1_,
        'for1_' => $for1_,
        'sequence1_' => $sequence1_,
        'foldMap1Default' => $foldMap1Default,
        'fold1Default' => $fold1Default,
        'intercalate' => $intercalate,
        'intercalateMap' => $intercalateMap,
        'foldableDual' => $foldableDual,
        'foldableMultiplicative' => $foldableMultiplicative
    ];
})();
