<?php
require_once(dirname(__FILE__) . '/../Control.Monad.ST/index.php');
require_once(dirname(__FILE__) . '/../Data.Array.ST/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
$PS__Data_Array_ST_Partial = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $poke = function ($dictPartial)  use (&$__foreign) {
        return $__foreign['pokeImpl'];
    };
    $peek = function ($dictPartial)  use (&$__foreign) {
        return $__foreign['peekImpl'];
    };
    return [
        'peek' => $peek,
        'poke' => $poke
    ];
})();
