<?php

$exports['peekImpl'] = function ($i) {
  return function ($xs) use (&$i) {
    return function () use (&$i, &$xs) {
      return $xs[$i];
    };
  };
};

$exports['pokeImpl'] = function ($i) {
  return function ($a) use (&$i) {
    return function ($xs) use (&$i, &$a) {
      return function () use (&$i, &$a, &$xs) {
        $xs[$i] = $a;
        return [];
      };
    };
  };
};
