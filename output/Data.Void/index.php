<?php
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
$PS__Data_Void = (function ()  use (&$PS__Data_Show) {
    $Void = function ($x) {
        return $x;
    };
    $absurd = function ($a) {
        $spin = function ($_dollar_copy_v) {
            $_dollar_tco_result = NULL;
            $_dollar_tco_loop = function ($v)  use (&$_dollar_copy_v) {
                $_dollar_copy_v = $v;
                return;
            };
            while (!false) {
                $_dollar_tco_result = $_dollar_tco_loop($_dollar_copy_v);
            };
            return $_dollar_tco_result;
        };
        return $spin($a);
    };
    $showVoid = $PS__Data_Show['Show']($absurd);
    return [
        'absurd' => $absurd,
        'showVoid' => $showVoid
    ];
})();
