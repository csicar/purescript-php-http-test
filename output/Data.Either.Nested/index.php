<?php
require_once(dirname(__FILE__) . '/../Data.Either/index.php');
require_once(dirname(__FILE__) . '/../Data.Void/index.php');
$PS__Data_Either_Nested = (function ()  use (&$PS__Data_Either, &$PS__Data_Void) {
    $in9 = function ($v)  use (&$PS__Data_Either) {
        return $PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Left']['constructor']($v)))))))));
    };
    $in8 = function ($v)  use (&$PS__Data_Either) {
        return $PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Left']['constructor']($v))))))));
    };
    $in7 = function ($v)  use (&$PS__Data_Either) {
        return $PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Left']['constructor']($v)))))));
    };
    $in6 = function ($v)  use (&$PS__Data_Either) {
        return $PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Left']['constructor']($v))))));
    };
    $in5 = function ($v)  use (&$PS__Data_Either) {
        return $PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Left']['constructor']($v)))));
    };
    $in4 = function ($v)  use (&$PS__Data_Either) {
        return $PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Left']['constructor']($v))));
    };
    $in3 = function ($v)  use (&$PS__Data_Either) {
        return $PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Left']['constructor']($v)));
    };
    $in2 = function ($v)  use (&$PS__Data_Either) {
        return $PS__Data_Either['Right']['constructor']($PS__Data_Either['Left']['constructor']($v));
    };
    $in10 = function ($v)  use (&$PS__Data_Either) {
        return $PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Right']['constructor']($PS__Data_Either['Left']['constructor']($v))))))))));
    };
    $in1 = $PS__Data_Either['Left']['create'];
    $either9 = function ($a)  use (&$PS__Data_Void) {
        return function ($b)  use (&$a, &$PS__Data_Void) {
            return function ($c)  use (&$a, &$b, &$PS__Data_Void) {
                return function ($d)  use (&$a, &$b, &$c, &$PS__Data_Void) {
                    return function ($e)  use (&$a, &$b, &$c, &$d, &$PS__Data_Void) {
                        return function ($f)  use (&$a, &$b, &$c, &$d, &$e, &$PS__Data_Void) {
                            return function ($g)  use (&$a, &$b, &$c, &$d, &$e, &$f, &$PS__Data_Void) {
                                return function ($h)  use (&$a, &$b, &$c, &$d, &$e, &$f, &$g, &$PS__Data_Void) {
                                    return function ($i)  use (&$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h, &$PS__Data_Void) {
                                        return function ($y)  use (&$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h, &$i, &$PS__Data_Void) {
                                            if ($y['type'] === 'Left') {
                                                return $a($y['value0']);
                                            };
                                            if ($y['type'] === 'Right') {
                                                if ($y['value0']['type'] === 'Left') {
                                                    return $b($y['value0']['value0']);
                                                };
                                                if ($y['value0']['type'] === 'Right') {
                                                    if ($y['value0']['value0']['type'] === 'Left') {
                                                        return $c($y['value0']['value0']['value0']);
                                                    };
                                                    if ($y['value0']['value0']['type'] === 'Right') {
                                                        if ($y['value0']['value0']['value0']['type'] === 'Left') {
                                                            return $d($y['value0']['value0']['value0']['value0']);
                                                        };
                                                        if ($y['value0']['value0']['value0']['type'] === 'Right') {
                                                            if ($y['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                                return $e($y['value0']['value0']['value0']['value0']['value0']);
                                                            };
                                                            if ($y['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                                if ($y['value0']['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                                    return $f($y['value0']['value0']['value0']['value0']['value0']['value0']);
                                                                };
                                                                if ($y['value0']['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                                    if ($y['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                                        return $g($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                                                                    };
                                                                    if ($y['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                                        if ($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                                            return $h($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                                                                        };
                                                                        if ($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                                            if ($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                                                return $i($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                                                                            };
                                                                            if ($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                                                return $PS__Data_Void['absurd']($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                                                                            };
                                                                            throw new \Exception('Failed pattern match at Data.Either.Nested line 236, column 29 - line 238, column 40: ' . var_dump([ $y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                                        };
                                                                        throw new \Exception('Failed pattern match at Data.Either.Nested line 234, column 27 - line 238, column 40: ' . var_dump([ $y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                                    };
                                                                    throw new \Exception('Failed pattern match at Data.Either.Nested line 232, column 25 - line 238, column 40: ' . var_dump([ $y['value0']['value0']['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                                };
                                                                throw new \Exception('Failed pattern match at Data.Either.Nested line 230, column 23 - line 238, column 40: ' . var_dump([ $y['value0']['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                            };
                                                            throw new \Exception('Failed pattern match at Data.Either.Nested line 228, column 21 - line 238, column 40: ' . var_dump([ $y['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                        };
                                                        throw new \Exception('Failed pattern match at Data.Either.Nested line 226, column 19 - line 238, column 40: ' . var_dump([ $y['value0']['value0']['value0']['constructor']['name'] ]));
                                                    };
                                                    throw new \Exception('Failed pattern match at Data.Either.Nested line 224, column 17 - line 238, column 40: ' . var_dump([ $y['value0']['value0']['constructor']['name'] ]));
                                                };
                                                throw new \Exception('Failed pattern match at Data.Either.Nested line 222, column 15 - line 238, column 40: ' . var_dump([ $y['value0']['constructor']['name'] ]));
                                            };
                                            throw new \Exception('Failed pattern match at Data.Either.Nested line 220, column 31 - line 238, column 40: ' . var_dump([ $y['constructor']['name'] ]));
                                        };
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    $either8 = function ($a)  use (&$PS__Data_Void) {
        return function ($b)  use (&$a, &$PS__Data_Void) {
            return function ($c)  use (&$a, &$b, &$PS__Data_Void) {
                return function ($d)  use (&$a, &$b, &$c, &$PS__Data_Void) {
                    return function ($e)  use (&$a, &$b, &$c, &$d, &$PS__Data_Void) {
                        return function ($f)  use (&$a, &$b, &$c, &$d, &$e, &$PS__Data_Void) {
                            return function ($g)  use (&$a, &$b, &$c, &$d, &$e, &$f, &$PS__Data_Void) {
                                return function ($h)  use (&$a, &$b, &$c, &$d, &$e, &$f, &$g, &$PS__Data_Void) {
                                    return function ($y)  use (&$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h, &$PS__Data_Void) {
                                        if ($y['type'] === 'Left') {
                                            return $a($y['value0']);
                                        };
                                        if ($y['type'] === 'Right') {
                                            if ($y['value0']['type'] === 'Left') {
                                                return $b($y['value0']['value0']);
                                            };
                                            if ($y['value0']['type'] === 'Right') {
                                                if ($y['value0']['value0']['type'] === 'Left') {
                                                    return $c($y['value0']['value0']['value0']);
                                                };
                                                if ($y['value0']['value0']['type'] === 'Right') {
                                                    if ($y['value0']['value0']['value0']['type'] === 'Left') {
                                                        return $d($y['value0']['value0']['value0']['value0']);
                                                    };
                                                    if ($y['value0']['value0']['value0']['type'] === 'Right') {
                                                        if ($y['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                            return $e($y['value0']['value0']['value0']['value0']['value0']);
                                                        };
                                                        if ($y['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                            if ($y['value0']['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                                return $f($y['value0']['value0']['value0']['value0']['value0']['value0']);
                                                            };
                                                            if ($y['value0']['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                                if ($y['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                                    return $g($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                                                                };
                                                                if ($y['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                                    if ($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                                        return $h($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                                                                    };
                                                                    if ($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                                        return $PS__Data_Void['absurd']($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                                                                    };
                                                                    throw new \Exception('Failed pattern match at Data.Either.Nested line 215, column 27 - line 217, column 38: ' . var_dump([ $y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                                };
                                                                throw new \Exception('Failed pattern match at Data.Either.Nested line 213, column 25 - line 217, column 38: ' . var_dump([ $y['value0']['value0']['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                            };
                                                            throw new \Exception('Failed pattern match at Data.Either.Nested line 211, column 23 - line 217, column 38: ' . var_dump([ $y['value0']['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                        };
                                                        throw new \Exception('Failed pattern match at Data.Either.Nested line 209, column 21 - line 217, column 38: ' . var_dump([ $y['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                    };
                                                    throw new \Exception('Failed pattern match at Data.Either.Nested line 207, column 19 - line 217, column 38: ' . var_dump([ $y['value0']['value0']['value0']['constructor']['name'] ]));
                                                };
                                                throw new \Exception('Failed pattern match at Data.Either.Nested line 205, column 17 - line 217, column 38: ' . var_dump([ $y['value0']['value0']['constructor']['name'] ]));
                                            };
                                            throw new \Exception('Failed pattern match at Data.Either.Nested line 203, column 15 - line 217, column 38: ' . var_dump([ $y['value0']['constructor']['name'] ]));
                                        };
                                        throw new \Exception('Failed pattern match at Data.Either.Nested line 201, column 29 - line 217, column 38: ' . var_dump([ $y['constructor']['name'] ]));
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    $either7 = function ($a)  use (&$PS__Data_Void) {
        return function ($b)  use (&$a, &$PS__Data_Void) {
            return function ($c)  use (&$a, &$b, &$PS__Data_Void) {
                return function ($d)  use (&$a, &$b, &$c, &$PS__Data_Void) {
                    return function ($e)  use (&$a, &$b, &$c, &$d, &$PS__Data_Void) {
                        return function ($f)  use (&$a, &$b, &$c, &$d, &$e, &$PS__Data_Void) {
                            return function ($g)  use (&$a, &$b, &$c, &$d, &$e, &$f, &$PS__Data_Void) {
                                return function ($y)  use (&$a, &$b, &$c, &$d, &$e, &$f, &$g, &$PS__Data_Void) {
                                    if ($y['type'] === 'Left') {
                                        return $a($y['value0']);
                                    };
                                    if ($y['type'] === 'Right') {
                                        if ($y['value0']['type'] === 'Left') {
                                            return $b($y['value0']['value0']);
                                        };
                                        if ($y['value0']['type'] === 'Right') {
                                            if ($y['value0']['value0']['type'] === 'Left') {
                                                return $c($y['value0']['value0']['value0']);
                                            };
                                            if ($y['value0']['value0']['type'] === 'Right') {
                                                if ($y['value0']['value0']['value0']['type'] === 'Left') {
                                                    return $d($y['value0']['value0']['value0']['value0']);
                                                };
                                                if ($y['value0']['value0']['value0']['type'] === 'Right') {
                                                    if ($y['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                        return $e($y['value0']['value0']['value0']['value0']['value0']);
                                                    };
                                                    if ($y['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                        if ($y['value0']['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                            return $f($y['value0']['value0']['value0']['value0']['value0']['value0']);
                                                        };
                                                        if ($y['value0']['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                            if ($y['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                                return $g($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                                                            };
                                                            if ($y['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                                return $PS__Data_Void['absurd']($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                                                            };
                                                            throw new \Exception('Failed pattern match at Data.Either.Nested line 196, column 25 - line 198, column 36: ' . var_dump([ $y['value0']['value0']['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                        };
                                                        throw new \Exception('Failed pattern match at Data.Either.Nested line 194, column 23 - line 198, column 36: ' . var_dump([ $y['value0']['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                    };
                                                    throw new \Exception('Failed pattern match at Data.Either.Nested line 192, column 21 - line 198, column 36: ' . var_dump([ $y['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                };
                                                throw new \Exception('Failed pattern match at Data.Either.Nested line 190, column 19 - line 198, column 36: ' . var_dump([ $y['value0']['value0']['value0']['constructor']['name'] ]));
                                            };
                                            throw new \Exception('Failed pattern match at Data.Either.Nested line 188, column 17 - line 198, column 36: ' . var_dump([ $y['value0']['value0']['constructor']['name'] ]));
                                        };
                                        throw new \Exception('Failed pattern match at Data.Either.Nested line 186, column 15 - line 198, column 36: ' . var_dump([ $y['value0']['constructor']['name'] ]));
                                    };
                                    throw new \Exception('Failed pattern match at Data.Either.Nested line 184, column 27 - line 198, column 36: ' . var_dump([ $y['constructor']['name'] ]));
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    $either6 = function ($a)  use (&$PS__Data_Void) {
        return function ($b)  use (&$a, &$PS__Data_Void) {
            return function ($c)  use (&$a, &$b, &$PS__Data_Void) {
                return function ($d)  use (&$a, &$b, &$c, &$PS__Data_Void) {
                    return function ($e)  use (&$a, &$b, &$c, &$d, &$PS__Data_Void) {
                        return function ($f)  use (&$a, &$b, &$c, &$d, &$e, &$PS__Data_Void) {
                            return function ($y)  use (&$a, &$b, &$c, &$d, &$e, &$f, &$PS__Data_Void) {
                                if ($y['type'] === 'Left') {
                                    return $a($y['value0']);
                                };
                                if ($y['type'] === 'Right') {
                                    if ($y['value0']['type'] === 'Left') {
                                        return $b($y['value0']['value0']);
                                    };
                                    if ($y['value0']['type'] === 'Right') {
                                        if ($y['value0']['value0']['type'] === 'Left') {
                                            return $c($y['value0']['value0']['value0']);
                                        };
                                        if ($y['value0']['value0']['type'] === 'Right') {
                                            if ($y['value0']['value0']['value0']['type'] === 'Left') {
                                                return $d($y['value0']['value0']['value0']['value0']);
                                            };
                                            if ($y['value0']['value0']['value0']['type'] === 'Right') {
                                                if ($y['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                    return $e($y['value0']['value0']['value0']['value0']['value0']);
                                                };
                                                if ($y['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                    if ($y['value0']['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                        return $f($y['value0']['value0']['value0']['value0']['value0']['value0']);
                                                    };
                                                    if ($y['value0']['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                        return $PS__Data_Void['absurd']($y['value0']['value0']['value0']['value0']['value0']['value0']);
                                                    };
                                                    throw new \Exception('Failed pattern match at Data.Either.Nested line 179, column 23 - line 181, column 34: ' . var_dump([ $y['value0']['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                };
                                                throw new \Exception('Failed pattern match at Data.Either.Nested line 177, column 21 - line 181, column 34: ' . var_dump([ $y['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                            };
                                            throw new \Exception('Failed pattern match at Data.Either.Nested line 175, column 19 - line 181, column 34: ' . var_dump([ $y['value0']['value0']['value0']['constructor']['name'] ]));
                                        };
                                        throw new \Exception('Failed pattern match at Data.Either.Nested line 173, column 17 - line 181, column 34: ' . var_dump([ $y['value0']['value0']['constructor']['name'] ]));
                                    };
                                    throw new \Exception('Failed pattern match at Data.Either.Nested line 171, column 15 - line 181, column 34: ' . var_dump([ $y['value0']['constructor']['name'] ]));
                                };
                                throw new \Exception('Failed pattern match at Data.Either.Nested line 169, column 25 - line 181, column 34: ' . var_dump([ $y['constructor']['name'] ]));
                            };
                        };
                    };
                };
            };
        };
    };
    $either5 = function ($a)  use (&$PS__Data_Void) {
        return function ($b)  use (&$a, &$PS__Data_Void) {
            return function ($c)  use (&$a, &$b, &$PS__Data_Void) {
                return function ($d)  use (&$a, &$b, &$c, &$PS__Data_Void) {
                    return function ($e)  use (&$a, &$b, &$c, &$d, &$PS__Data_Void) {
                        return function ($y)  use (&$a, &$b, &$c, &$d, &$e, &$PS__Data_Void) {
                            if ($y['type'] === 'Left') {
                                return $a($y['value0']);
                            };
                            if ($y['type'] === 'Right') {
                                if ($y['value0']['type'] === 'Left') {
                                    return $b($y['value0']['value0']);
                                };
                                if ($y['value0']['type'] === 'Right') {
                                    if ($y['value0']['value0']['type'] === 'Left') {
                                        return $c($y['value0']['value0']['value0']);
                                    };
                                    if ($y['value0']['value0']['type'] === 'Right') {
                                        if ($y['value0']['value0']['value0']['type'] === 'Left') {
                                            return $d($y['value0']['value0']['value0']['value0']);
                                        };
                                        if ($y['value0']['value0']['value0']['type'] === 'Right') {
                                            if ($y['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                return $e($y['value0']['value0']['value0']['value0']['value0']);
                                            };
                                            if ($y['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                return $PS__Data_Void['absurd']($y['value0']['value0']['value0']['value0']['value0']);
                                            };
                                            throw new \Exception('Failed pattern match at Data.Either.Nested line 164, column 21 - line 166, column 32: ' . var_dump([ $y['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                        };
                                        throw new \Exception('Failed pattern match at Data.Either.Nested line 162, column 19 - line 166, column 32: ' . var_dump([ $y['value0']['value0']['value0']['constructor']['name'] ]));
                                    };
                                    throw new \Exception('Failed pattern match at Data.Either.Nested line 160, column 17 - line 166, column 32: ' . var_dump([ $y['value0']['value0']['constructor']['name'] ]));
                                };
                                throw new \Exception('Failed pattern match at Data.Either.Nested line 158, column 15 - line 166, column 32: ' . var_dump([ $y['value0']['constructor']['name'] ]));
                            };
                            throw new \Exception('Failed pattern match at Data.Either.Nested line 156, column 23 - line 166, column 32: ' . var_dump([ $y['constructor']['name'] ]));
                        };
                    };
                };
            };
        };
    };
    $either4 = function ($a)  use (&$PS__Data_Void) {
        return function ($b)  use (&$a, &$PS__Data_Void) {
            return function ($c)  use (&$a, &$b, &$PS__Data_Void) {
                return function ($d)  use (&$a, &$b, &$c, &$PS__Data_Void) {
                    return function ($y)  use (&$a, &$b, &$c, &$d, &$PS__Data_Void) {
                        if ($y['type'] === 'Left') {
                            return $a($y['value0']);
                        };
                        if ($y['type'] === 'Right') {
                            if ($y['value0']['type'] === 'Left') {
                                return $b($y['value0']['value0']);
                            };
                            if ($y['value0']['type'] === 'Right') {
                                if ($y['value0']['value0']['type'] === 'Left') {
                                    return $c($y['value0']['value0']['value0']);
                                };
                                if ($y['value0']['value0']['type'] === 'Right') {
                                    if ($y['value0']['value0']['value0']['type'] === 'Left') {
                                        return $d($y['value0']['value0']['value0']['value0']);
                                    };
                                    if ($y['value0']['value0']['value0']['type'] === 'Right') {
                                        return $PS__Data_Void['absurd']($y['value0']['value0']['value0']['value0']);
                                    };
                                    throw new \Exception('Failed pattern match at Data.Either.Nested line 151, column 19 - line 153, column 30: ' . var_dump([ $y['value0']['value0']['value0']['constructor']['name'] ]));
                                };
                                throw new \Exception('Failed pattern match at Data.Either.Nested line 149, column 17 - line 153, column 30: ' . var_dump([ $y['value0']['value0']['constructor']['name'] ]));
                            };
                            throw new \Exception('Failed pattern match at Data.Either.Nested line 147, column 15 - line 153, column 30: ' . var_dump([ $y['value0']['constructor']['name'] ]));
                        };
                        throw new \Exception('Failed pattern match at Data.Either.Nested line 145, column 21 - line 153, column 30: ' . var_dump([ $y['constructor']['name'] ]));
                    };
                };
            };
        };
    };
    $either3 = function ($a)  use (&$PS__Data_Void) {
        return function ($b)  use (&$a, &$PS__Data_Void) {
            return function ($c)  use (&$a, &$b, &$PS__Data_Void) {
                return function ($y)  use (&$a, &$b, &$c, &$PS__Data_Void) {
                    if ($y['type'] === 'Left') {
                        return $a($y['value0']);
                    };
                    if ($y['type'] === 'Right') {
                        if ($y['value0']['type'] === 'Left') {
                            return $b($y['value0']['value0']);
                        };
                        if ($y['value0']['type'] === 'Right') {
                            if ($y['value0']['value0']['type'] === 'Left') {
                                return $c($y['value0']['value0']['value0']);
                            };
                            if ($y['value0']['value0']['type'] === 'Right') {
                                return $PS__Data_Void['absurd']($y['value0']['value0']['value0']);
                            };
                            throw new \Exception('Failed pattern match at Data.Either.Nested line 140, column 17 - line 142, column 28: ' . var_dump([ $y['value0']['value0']['constructor']['name'] ]));
                        };
                        throw new \Exception('Failed pattern match at Data.Either.Nested line 138, column 15 - line 142, column 28: ' . var_dump([ $y['value0']['constructor']['name'] ]));
                    };
                    throw new \Exception('Failed pattern match at Data.Either.Nested line 136, column 19 - line 142, column 28: ' . var_dump([ $y['constructor']['name'] ]));
                };
            };
        };
    };
    $either2 = function ($a)  use (&$PS__Data_Void) {
        return function ($b)  use (&$a, &$PS__Data_Void) {
            return function ($y)  use (&$a, &$b, &$PS__Data_Void) {
                if ($y['type'] === 'Left') {
                    return $a($y['value0']);
                };
                if ($y['type'] === 'Right') {
                    if ($y['value0']['type'] === 'Left') {
                        return $b($y['value0']['value0']);
                    };
                    if ($y['value0']['type'] === 'Right') {
                        return $PS__Data_Void['absurd']($y['value0']['value0']);
                    };
                    throw new \Exception('Failed pattern match at Data.Either.Nested line 131, column 15 - line 133, column 26: ' . var_dump([ $y['value0']['constructor']['name'] ]));
                };
                throw new \Exception('Failed pattern match at Data.Either.Nested line 129, column 17 - line 133, column 26: ' . var_dump([ $y['constructor']['name'] ]));
            };
        };
    };
    $either10 = function ($a)  use (&$PS__Data_Void) {
        return function ($b)  use (&$a, &$PS__Data_Void) {
            return function ($c)  use (&$a, &$b, &$PS__Data_Void) {
                return function ($d)  use (&$a, &$b, &$c, &$PS__Data_Void) {
                    return function ($e)  use (&$a, &$b, &$c, &$d, &$PS__Data_Void) {
                        return function ($f)  use (&$a, &$b, &$c, &$d, &$e, &$PS__Data_Void) {
                            return function ($g)  use (&$a, &$b, &$c, &$d, &$e, &$f, &$PS__Data_Void) {
                                return function ($h)  use (&$a, &$b, &$c, &$d, &$e, &$f, &$g, &$PS__Data_Void) {
                                    return function ($i)  use (&$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h, &$PS__Data_Void) {
                                        return function ($j)  use (&$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h, &$i, &$PS__Data_Void) {
                                            return function ($y)  use (&$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h, &$i, &$j, &$PS__Data_Void) {
                                                if ($y['type'] === 'Left') {
                                                    return $a($y['value0']);
                                                };
                                                if ($y['type'] === 'Right') {
                                                    if ($y['value0']['type'] === 'Left') {
                                                        return $b($y['value0']['value0']);
                                                    };
                                                    if ($y['value0']['type'] === 'Right') {
                                                        if ($y['value0']['value0']['type'] === 'Left') {
                                                            return $c($y['value0']['value0']['value0']);
                                                        };
                                                        if ($y['value0']['value0']['type'] === 'Right') {
                                                            if ($y['value0']['value0']['value0']['type'] === 'Left') {
                                                                return $d($y['value0']['value0']['value0']['value0']);
                                                            };
                                                            if ($y['value0']['value0']['value0']['type'] === 'Right') {
                                                                if ($y['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                                    return $e($y['value0']['value0']['value0']['value0']['value0']);
                                                                };
                                                                if ($y['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                                    if ($y['value0']['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                                        return $f($y['value0']['value0']['value0']['value0']['value0']['value0']);
                                                                    };
                                                                    if ($y['value0']['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                                        if ($y['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                                            return $g($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                                                                        };
                                                                        if ($y['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                                            if ($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                                                return $h($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                                                                            };
                                                                            if ($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                                                if ($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                                                    return $i($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                                                                                };
                                                                                if ($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                                                    if ($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Left') {
                                                                                        return $j($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                                                                                    };
                                                                                    if ($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right') {
                                                                                        return $PS__Data_Void['absurd']($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                                                                                    };
                                                                                    throw new \Exception('Failed pattern match at Data.Either.Nested line 259, column 31 - line 261, column 41: ' . var_dump([ $y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                                                };
                                                                                throw new \Exception('Failed pattern match at Data.Either.Nested line 257, column 29 - line 261, column 41: ' . var_dump([ $y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                                            };
                                                                            throw new \Exception('Failed pattern match at Data.Either.Nested line 255, column 27 - line 261, column 41: ' . var_dump([ $y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                                        };
                                                                        throw new \Exception('Failed pattern match at Data.Either.Nested line 253, column 25 - line 261, column 41: ' . var_dump([ $y['value0']['value0']['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                                    };
                                                                    throw new \Exception('Failed pattern match at Data.Either.Nested line 251, column 23 - line 261, column 41: ' . var_dump([ $y['value0']['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                                };
                                                                throw new \Exception('Failed pattern match at Data.Either.Nested line 249, column 21 - line 261, column 41: ' . var_dump([ $y['value0']['value0']['value0']['value0']['constructor']['name'] ]));
                                                            };
                                                            throw new \Exception('Failed pattern match at Data.Either.Nested line 247, column 19 - line 261, column 41: ' . var_dump([ $y['value0']['value0']['value0']['constructor']['name'] ]));
                                                        };
                                                        throw new \Exception('Failed pattern match at Data.Either.Nested line 245, column 17 - line 261, column 41: ' . var_dump([ $y['value0']['value0']['constructor']['name'] ]));
                                                    };
                                                    throw new \Exception('Failed pattern match at Data.Either.Nested line 243, column 15 - line 261, column 41: ' . var_dump([ $y['value0']['constructor']['name'] ]));
                                                };
                                                throw new \Exception('Failed pattern match at Data.Either.Nested line 241, column 34 - line 261, column 41: ' . var_dump([ $y['constructor']['name'] ]));
                                            };
                                        };
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    $either1 = function ($y)  use (&$PS__Data_Void) {
        if ($y['type'] === 'Left') {
            return $y['value0'];
        };
        if ($y['type'] === 'Right') {
            return $PS__Data_Void['absurd']($y['value0']);
        };
        throw new \Exception('Failed pattern match at Data.Either.Nested line 124, column 13 - line 126, column 24: ' . var_dump([ $y['constructor']['name'] ]));
    };
    $at9 = function ($b) {
        return function ($f)  use (&$b) {
            return function ($y)  use (&$f, &$b) {
                if ($y['type'] === 'Right' && ($y['value0']['type'] === 'Right' && ($y['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right' && $y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Left')))))))) {
                    return $f($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                };
                return $b;
            };
        };
    };
    $at8 = function ($b) {
        return function ($f)  use (&$b) {
            return function ($y)  use (&$f, &$b) {
                if ($y['type'] === 'Right' && ($y['value0']['type'] === 'Right' && ($y['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right' && $y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Left'))))))) {
                    return $f($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                };
                return $b;
            };
        };
    };
    $at7 = function ($b) {
        return function ($f)  use (&$b) {
            return function ($y)  use (&$f, &$b) {
                if ($y['type'] === 'Right' && ($y['value0']['type'] === 'Right' && ($y['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['value0']['value0']['type'] === 'Right' && $y['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Left')))))) {
                    return $f($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                };
                return $b;
            };
        };
    };
    $at6 = function ($b) {
        return function ($f)  use (&$b) {
            return function ($y)  use (&$f, &$b) {
                if ($y['type'] === 'Right' && ($y['value0']['type'] === 'Right' && ($y['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['value0']['type'] === 'Right' && $y['value0']['value0']['value0']['value0']['value0']['type'] === 'Left'))))) {
                    return $f($y['value0']['value0']['value0']['value0']['value0']['value0']);
                };
                return $b;
            };
        };
    };
    $at5 = function ($b) {
        return function ($f)  use (&$b) {
            return function ($y)  use (&$f, &$b) {
                if ($y['type'] === 'Right' && ($y['value0']['type'] === 'Right' && ($y['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['type'] === 'Right' && $y['value0']['value0']['value0']['value0']['type'] === 'Left')))) {
                    return $f($y['value0']['value0']['value0']['value0']['value0']);
                };
                return $b;
            };
        };
    };
    $at4 = function ($b) {
        return function ($f)  use (&$b) {
            return function ($y)  use (&$f, &$b) {
                if ($y['type'] === 'Right' && ($y['value0']['type'] === 'Right' && ($y['value0']['value0']['type'] === 'Right' && $y['value0']['value0']['value0']['type'] === 'Left'))) {
                    return $f($y['value0']['value0']['value0']['value0']);
                };
                return $b;
            };
        };
    };
    $at3 = function ($b) {
        return function ($f)  use (&$b) {
            return function ($y)  use (&$f, &$b) {
                if ($y['type'] === 'Right' && ($y['value0']['type'] === 'Right' && $y['value0']['value0']['type'] === 'Left')) {
                    return $f($y['value0']['value0']['value0']);
                };
                return $b;
            };
        };
    };
    $at2 = function ($b) {
        return function ($f)  use (&$b) {
            return function ($y)  use (&$f, &$b) {
                if ($y['type'] === 'Right' && $y['value0']['type'] === 'Left') {
                    return $f($y['value0']['value0']);
                };
                return $b;
            };
        };
    };
    $at10 = function ($b) {
        return function ($f)  use (&$b) {
            return function ($y)  use (&$f, &$b) {
                if ($y['type'] === 'Right' && ($y['value0']['type'] === 'Right' && ($y['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right' && ($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Right' && $y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['type'] === 'Left'))))))))) {
                    return $f($y['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']['value0']);
                };
                return $b;
            };
        };
    };
    $at1 = function ($b) {
        return function ($f)  use (&$b) {
            return function ($y)  use (&$f, &$b) {
                if ($y['type'] === 'Left') {
                    return $f($y['value0']);
                };
                return $b;
            };
        };
    };
    return [
        'in1' => $in1,
        'in2' => $in2,
        'in3' => $in3,
        'in4' => $in4,
        'in5' => $in5,
        'in6' => $in6,
        'in7' => $in7,
        'in8' => $in8,
        'in9' => $in9,
        'in10' => $in10,
        'at1' => $at1,
        'at2' => $at2,
        'at3' => $at3,
        'at4' => $at4,
        'at5' => $at5,
        'at6' => $at6,
        'at7' => $at7,
        'at8' => $at8,
        'at9' => $at9,
        'at10' => $at10,
        'either1' => $either1,
        'either2' => $either2,
        'either3' => $either3,
        'either4' => $either4,
        'either5' => $either5,
        'either6' => $either6,
        'either7' => $either7,
        'either8' => $either8,
        'either9' => $either9,
        'either10' => $either10
    ];
})();
