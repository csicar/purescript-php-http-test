<?php
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Ord_Max = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Ord, &$PS__Data_Newtype, &$PS__Data_Monoid, &$PS__Data_Bounded) {
    $Max = function ($x) {
        return $x;
    };
    $showMax = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Max ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $semigroupMax = function ($dictOrd)  use (&$PS__Data_Semigroup, &$PS__Data_Ord) {
        return $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$PS__Data_Ord, &$dictOrd) {
            return function ($v1)  use (&$PS__Data_Ord, &$dictOrd, &$v) {
                return $PS__Data_Ord['max']($dictOrd)($v)($v1);
            };
        });
    };
    $newtypeMax = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Max);
    $monoidMax = function ($dictBounded)  use (&$PS__Data_Monoid, &$semigroupMax, &$PS__Data_Bounded) {
        return $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupMax, &$dictBounded) {
            return $semigroupMax($dictBounded['Ord0']());
        }, $PS__Data_Bounded['bottom']($dictBounded));
    };
    $eqMax = function ($dictEq) {
        return $dictEq;
    };
    $ordMax = function ($dictOrd)  use (&$PS__Data_Ord, &$eqMax) {
        return $PS__Data_Ord['Ord'](function ()  use (&$eqMax, &$dictOrd) {
            return $eqMax($dictOrd['Eq0']());
        }, function ($v)  use (&$PS__Data_Ord, &$dictOrd) {
            return function ($v1)  use (&$PS__Data_Ord, &$dictOrd, &$v) {
                return $PS__Data_Ord['compare']($dictOrd)($v)($v1);
            };
        });
    };
    return [
        'Max' => $Max,
        'newtypeMax' => $newtypeMax,
        'eqMax' => $eqMax,
        'ordMax' => $ordMax,
        'semigroupMax' => $semigroupMax,
        'monoidMax' => $monoidMax,
        'showMax' => $showMax
    ];
})();
