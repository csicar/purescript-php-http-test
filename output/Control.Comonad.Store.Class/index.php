<?php
require_once(dirname(__FILE__) . '/../Control.Comonad/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad.Env.Trans/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad.Store.Trans/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad.Traced.Trans/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad.Trans.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Extend/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Comonad_Store_Class = (function ()  use (&$PS__Data_Function, &$PS__Control_Semigroupoid, &$PS__Control_Extend, &$PS__Data_Functor, &$PS__Control_Comonad_Traced_Trans, &$PS__Control_Comonad_Trans_Class, &$PS__Control_Comonad_Store_Trans, &$PS__Control_Comonad, &$PS__Control_Comonad_Env_Trans) {
    $ComonadStore = function ($Comonad0, $peek, $pos) {
        return [
            'Comonad0' => $Comonad0,
            'peek' => $peek,
            'pos' => $pos
        ];
    };
    $pos = function ($dict) {
        return $dict['pos'];
    };
    $peek = function ($dict) {
        return $dict['peek'];
    };
    $peeks = function ($dictComonadStore)  use (&$peek, &$PS__Data_Function, &$pos) {
        return function ($f)  use (&$peek, &$dictComonadStore, &$PS__Data_Function, &$pos) {
            return function ($x)  use (&$peek, &$dictComonadStore, &$PS__Data_Function, &$f, &$pos) {
                return $peek($dictComonadStore)($PS__Data_Function['apply']($f)($pos($dictComonadStore)($x)))($x);
            };
        };
    };
    $seeks = function ($dictComonadStore)  use (&$PS__Control_Semigroupoid, &$peeks, &$PS__Control_Extend) {
        return function ($f)  use (&$PS__Control_Semigroupoid, &$peeks, &$dictComonadStore, &$PS__Control_Extend) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($peeks($dictComonadStore)($f))($PS__Control_Extend['duplicate'](($dictComonadStore['Comonad0']())['Extend0']()));
        };
    };
    $seek = function ($dictComonadStore)  use (&$PS__Control_Semigroupoid, &$peek, &$PS__Control_Extend) {
        return function ($s)  use (&$PS__Control_Semigroupoid, &$peek, &$dictComonadStore, &$PS__Control_Extend) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($peek($dictComonadStore)($s))($PS__Control_Extend['duplicate'](($dictComonadStore['Comonad0']())['Extend0']()));
        };
    };
    $experiment = function ($dictComonadStore)  use (&$PS__Data_Functor, &$PS__Data_Function, &$peek, &$pos) {
        return function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Function, &$peek, &$dictComonadStore, &$pos) {
            return function ($f)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Function, &$peek, &$dictComonadStore, &$pos) {
                return function ($x)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Function, &$peek, &$dictComonadStore, &$f, &$pos) {
                    return $PS__Data_Functor['map']($dictFunctor)($PS__Data_Function['flip']($peek($dictComonadStore))($x))($f($pos($dictComonadStore)($x)));
                };
            };
        };
    };
    $comonadStoreTracedT = function ($dictComonadStore)  use (&$ComonadStore, &$PS__Control_Comonad_Traced_Trans, &$PS__Control_Semigroupoid, &$peek, &$PS__Control_Comonad_Trans_Class, &$pos) {
        return function ($dictMonoid)  use (&$ComonadStore, &$PS__Control_Comonad_Traced_Trans, &$dictComonadStore, &$PS__Control_Semigroupoid, &$peek, &$PS__Control_Comonad_Trans_Class, &$pos) {
            return $ComonadStore(function ()  use (&$PS__Control_Comonad_Traced_Trans, &$dictComonadStore, &$dictMonoid) {
                return $PS__Control_Comonad_Traced_Trans['comonadTracedT']($dictComonadStore['Comonad0']())($dictMonoid);
            }, function ($s)  use (&$PS__Control_Semigroupoid, &$peek, &$dictComonadStore, &$PS__Control_Comonad_Trans_Class, &$PS__Control_Comonad_Traced_Trans, &$dictMonoid) {
                return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($peek($dictComonadStore)($s))($PS__Control_Comonad_Trans_Class['lower']($PS__Control_Comonad_Traced_Trans['comonadTransTracedT']($dictMonoid))($dictComonadStore['Comonad0']()));
            }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($pos($dictComonadStore))($PS__Control_Comonad_Trans_Class['lower']($PS__Control_Comonad_Traced_Trans['comonadTransTracedT']($dictMonoid))($dictComonadStore['Comonad0']())));
        };
    };
    $comonadStoreStoreT = function ($dictComonad)  use (&$ComonadStore, &$PS__Control_Comonad_Store_Trans, &$PS__Control_Comonad) {
        return $ComonadStore(function ()  use (&$PS__Control_Comonad_Store_Trans, &$dictComonad) {
            return $PS__Control_Comonad_Store_Trans['comonadStoreT']($dictComonad);
        }, function ($s)  use (&$PS__Control_Comonad, &$dictComonad) {
            return function ($v)  use (&$PS__Control_Comonad, &$dictComonad, &$s) {
                return $PS__Control_Comonad['extract']($dictComonad)($v['value0'])($s);
            };
        }, function ($v) {
            return $v['value1'];
        });
    };
    $comonadStoreEnvT = function ($dictComonadStore)  use (&$ComonadStore, &$PS__Control_Comonad_Env_Trans, &$PS__Control_Semigroupoid, &$peek, &$PS__Control_Comonad_Trans_Class, &$pos) {
        return $ComonadStore(function ()  use (&$PS__Control_Comonad_Env_Trans, &$dictComonadStore) {
            return $PS__Control_Comonad_Env_Trans['comonadEnvT']($dictComonadStore['Comonad0']());
        }, function ($s)  use (&$PS__Control_Semigroupoid, &$peek, &$dictComonadStore, &$PS__Control_Comonad_Trans_Class, &$PS__Control_Comonad_Env_Trans) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($peek($dictComonadStore)($s))($PS__Control_Comonad_Trans_Class['lower']($PS__Control_Comonad_Env_Trans['comonadTransEnvT'])($dictComonadStore['Comonad0']()));
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($pos($dictComonadStore))($PS__Control_Comonad_Trans_Class['lower']($PS__Control_Comonad_Env_Trans['comonadTransEnvT'])($dictComonadStore['Comonad0']())));
    };
    return [
        'peek' => $peek,
        'pos' => $pos,
        'ComonadStore' => $ComonadStore,
        'experiment' => $experiment,
        'peeks' => $peeks,
        'seek' => $seek,
        'seeks' => $seeks,
        'comonadStoreStoreT' => $comonadStoreStoreT,
        'comonadStoreEnvT' => $comonadStoreEnvT,
        'comonadStoreTracedT' => $comonadStoreTracedT
    ];
})();
