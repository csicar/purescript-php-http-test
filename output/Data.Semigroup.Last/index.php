<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Semigroup_Last = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Functor, &$PS__Data_Eq, &$PS__Data_Ord, &$PS__Control_Apply, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Control_Monad) {
    $Last = function ($x) {
        return $x;
    };
    $showLast = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Last ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $semigroupLast = $PS__Data_Semigroup['Semigroup'](function ($v) {
        return function ($x) {
            return $x;
        };
    });
    $ordLast = function ($dictOrd) {
        return $dictOrd;
    };
    $functorLast = $PS__Data_Functor['Functor'](function ($f) {
        return function ($m)  use (&$f) {
            return $f($m);
        };
    });
    $eqLast = function ($dictEq) {
        return $dictEq;
    };
    $eq1Last = $PS__Data_Eq['Eq1'](function ($dictEq)  use (&$PS__Data_Eq, &$eqLast) {
        return $PS__Data_Eq['eq']($eqLast($dictEq));
    });
    $ord1Last = $PS__Data_Ord['Ord1'](function ()  use (&$eq1Last) {
        return $eq1Last;
    }, function ($dictOrd)  use (&$PS__Data_Ord, &$ordLast) {
        return $PS__Data_Ord['compare']($ordLast($dictOrd));
    });
    $boundedLast = function ($dictBounded) {
        return $dictBounded;
    };
    $applyLast = $PS__Control_Apply['Apply'](function ()  use (&$functorLast) {
        return $functorLast;
    }, function ($v) {
        return function ($v1)  use (&$v) {
            return $v($v1);
        };
    });
    $bindLast = $PS__Control_Bind['Bind'](function ()  use (&$applyLast) {
        return $applyLast;
    }, function ($v) {
        return function ($f)  use (&$v) {
            return $f($v);
        };
    });
    $applicativeLast = $PS__Control_Applicative['Applicative'](function ()  use (&$applyLast) {
        return $applyLast;
    }, $Last);
    $monadLast = $PS__Control_Monad['Monad'](function ()  use (&$applicativeLast) {
        return $applicativeLast;
    }, function ()  use (&$bindLast) {
        return $bindLast;
    });
    return [
        'Last' => $Last,
        'eqLast' => $eqLast,
        'eq1Last' => $eq1Last,
        'ordLast' => $ordLast,
        'ord1Last' => $ord1Last,
        'boundedLast' => $boundedLast,
        'showLast' => $showLast,
        'functorLast' => $functorLast,
        'applyLast' => $applyLast,
        'applicativeLast' => $applicativeLast,
        'bindLast' => $bindLast,
        'monadLast' => $monadLast,
        'semigroupLast' => $semigroupLast
    ];
})();
