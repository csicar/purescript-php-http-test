<?php
require_once(dirname(__FILE__) . '/../Control.Monad.Reader.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Reader.Trans/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Identity/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Reader = (function ()  use (&$PS__Control_Monad_Reader_Trans, &$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$PS__Data_Identity, &$PS__Data_Function) {
    $withReader = $PS__Control_Monad_Reader_Trans['withReaderT'];
    $runReader = function ($v)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$PS__Data_Identity) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity']))($v);
    };
    $mapReader = function ($f)  use (&$PS__Data_Function, &$PS__Control_Monad_Reader_Trans, &$PS__Control_Semigroupoid, &$PS__Data_Identity, &$PS__Data_Newtype) {
        return $PS__Data_Function['apply']($PS__Control_Monad_Reader_Trans['mapReaderT'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Identity['Identity'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity']))));
    };
    return [
        'runReader' => $runReader,
        'mapReader' => $mapReader,
        'withReader' => $withReader
    ];
})();
