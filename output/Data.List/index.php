<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor/index.php');
require_once(dirname(__FILE__) . '/../Data.Boolean/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.FunctorWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.List.Types/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_List = (function ()  use (&$PS__Data_Maybe, &$PS__Data_List_Types, &$PS__Data_Functor, &$PS__Data_Ring, &$PS__Data_Foldable, &$PS__Data_Tuple, &$PS__Data_Unfoldable, &$PS__Data_Eq, &$PS__Data_Function, &$PS__Control_Monad_Rec_Class, &$PS__Data_Ordering, &$PS__Data_Boolean, &$PS__Control_Semigroupoid, &$PS__Data_Ord, &$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Traversable, &$PS__Data_Semiring, &$PS__Data_Newtype, &$PS__Data_FunctorWithIndex, &$PS__Control_Bind, &$PS__Control_Alt, &$PS__Control_Applicative, &$PS__Data_Unit, &$PS__Data_Bifunctor, &$PS__Control_Apply, &$PS__Control_Lazy, &$PS__Data_NonEmpty, &$PS__Data_HeytingAlgebra, &$PS__Control_Category) {
    $Pattern = function ($x) {
        return $x;
    };
    $updateAt = function ($v)  use (&$PS__Data_Maybe, &$PS__Data_List_Types, &$PS__Data_Functor, &$updateAt, &$PS__Data_Ring) {
        return function ($v1)  use (&$v, &$PS__Data_Maybe, &$PS__Data_List_Types, &$PS__Data_Functor, &$updateAt, &$PS__Data_Ring) {
            return function ($v2)  use (&$v, &$PS__Data_Maybe, &$PS__Data_List_Types, &$v1, &$PS__Data_Functor, &$updateAt, &$PS__Data_Ring) {
                if ($v === 0 && $v2['type'] === 'Cons') {
                    return $PS__Data_Maybe['Just']['constructor']($PS__Data_List_Types['Cons']['constructor']($v1, $v2['value1']));
                };
                if ($v2['type'] === 'Cons') {
                    return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($v3)  use (&$PS__Data_List_Types, &$v2) {
                        return $PS__Data_List_Types['Cons']['constructor']($v2['value0'], $v3);
                    })($updateAt($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(1))($v1)($v2['value1']));
                };
                return $PS__Data_Maybe['Nothing']();
            };
        };
    };
    $unzip = $PS__Data_Foldable['foldr']($PS__Data_List_Types['foldableList'])(function ($v)  use (&$PS__Data_Tuple, &$PS__Data_List_Types) {
        return function ($v1)  use (&$PS__Data_Tuple, &$PS__Data_List_Types, &$v) {
            return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_List_Types['Cons']['constructor']($v['value0'], $v1['value0']), $PS__Data_List_Types['Cons']['constructor']($v['value1'], $v1['value1']));
        };
    })($PS__Data_Tuple['Tuple']['constructor']($PS__Data_List_Types['Nil'](), $PS__Data_List_Types['Nil']()));
    $uncons = function ($v)  use (&$PS__Data_Maybe) {
        if ($v['type'] === 'Nil') {
            return $PS__Data_Maybe['Nothing']();
        };
        if ($v['type'] === 'Cons') {
            return $PS__Data_Maybe['Just']['constructor']([
                'head' => $v['value0'],
                'tail' => $v['value1']
            ]);
        };
        throw new \Exception('Failed pattern match at Data.List line 259, column 1 - line 259, column 66: ' . var_dump([ $v['constructor']['name'] ]));
    };
    $toUnfoldable = function ($dictUnfoldable)  use (&$PS__Data_Unfoldable, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Tuple, &$uncons) {
        return $PS__Data_Unfoldable['unfoldr']($dictUnfoldable)(function ($xs)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Tuple, &$uncons) {
            return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($rec)  use (&$PS__Data_Tuple) {
                return $PS__Data_Tuple['Tuple']['constructor']($rec['head'], $rec['tail']);
            })($uncons($xs));
        });
    };
    $tail = function ($v)  use (&$PS__Data_Maybe) {
        if ($v['type'] === 'Nil') {
            return $PS__Data_Maybe['Nothing']();
        };
        if ($v['type'] === 'Cons') {
            return $PS__Data_Maybe['Just']['constructor']($v['value1']);
        };
        throw new \Exception('Failed pattern match at Data.List line 245, column 1 - line 245, column 43: ' . var_dump([ $v['constructor']['name'] ]));
    };
    $stripPrefix = function ($dictEq)  use (&$PS__Data_Eq, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Control_Monad_Rec_Class) {
        return function ($v)  use (&$PS__Data_Eq, &$dictEq, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Control_Monad_Rec_Class) {
            return function ($s)  use (&$PS__Data_Eq, &$dictEq, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Control_Monad_Rec_Class, &$v) {
                $go = function ($prefix)  use (&$PS__Data_Eq, &$dictEq, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Control_Monad_Rec_Class) {
                    return function ($input)  use (&$prefix, &$PS__Data_Eq, &$dictEq, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Control_Monad_Rec_Class) {
                        if ($prefix['type'] === 'Cons' && ($input['type'] === 'Cons' && $PS__Data_Eq['eq']($dictEq)($prefix['value0'])($input['value0']))) {
                            return $PS__Data_Function['apply']($PS__Data_Maybe['Just']['create'])($PS__Control_Monad_Rec_Class['Loop']['constructor']([
                                'a' => $prefix['value1'],
                                'b' => $input['value1']
                            ]));
                        };
                        if ($prefix['type'] === 'Nil') {
                            return $PS__Data_Function['apply']($PS__Data_Maybe['Just']['create'])($PS__Control_Monad_Rec_Class['Done']['constructor']($input));
                        };
                        return $PS__Data_Maybe['Nothing']();
                    };
                };
                return $PS__Control_Monad_Rec_Class['tailRecM2']($PS__Control_Monad_Rec_Class['monadRecMaybe'])($go)($v)($s);
            };
        };
    };
    $span = function ($v)  use (&$span, &$PS__Data_List_Types) {
        return function ($v1)  use (&$v, &$span, &$PS__Data_List_Types) {
            if ($v1['type'] === 'Cons' && $v($v1['value0'])) {
                $v2 = $span($v)($v1['value1']);
                return [
                    'init' => $PS__Data_List_Types['Cons']['constructor']($v1['value0'], $v2['init']),
                    'rest' => $v2['rest']
                ];
            };
            return [
                'init' => $PS__Data_List_Types['Nil'](),
                'rest' => $v1
            ];
        };
    };
    $snoc = function ($xs)  use (&$PS__Data_Foldable, &$PS__Data_List_Types) {
        return function ($x)  use (&$PS__Data_Foldable, &$PS__Data_List_Types, &$xs) {
            return $PS__Data_Foldable['foldr']($PS__Data_List_Types['foldableList'])($PS__Data_List_Types['Cons']['create'])($PS__Data_List_Types['Cons']['constructor']($x, $PS__Data_List_Types['Nil']()))($xs);
        };
    };
    $singleton = function ($a)  use (&$PS__Data_List_Types) {
        return $PS__Data_List_Types['Cons']['constructor']($a, $PS__Data_List_Types['Nil']());
    };
    $sortBy = function ($cmp)  use (&$PS__Data_Eq, &$PS__Data_Ordering, &$PS__Data_List_Types, &$PS__Data_Boolean, &$singleton, &$PS__Data_Function, &$PS__Control_Semigroupoid) {
        $merge = function ($v)  use (&$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$PS__Data_List_Types, &$merge, &$PS__Data_Boolean) {
            return function ($v1)  use (&$v, &$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$PS__Data_List_Types, &$merge, &$PS__Data_Boolean) {
                if ($v['type'] === 'Cons' && $v1['type'] === 'Cons') {
                    if ($PS__Data_Eq['eq']($PS__Data_Ordering['eqOrdering'])($cmp($v['value0'])($v1['value0']))($PS__Data_Ordering['GT']())) {
                        return $PS__Data_List_Types['Cons']['constructor']($v1['value0'], $merge($v)($v1['value1']));
                    };
                    if ($PS__Data_Boolean['otherwise']) {
                        return $PS__Data_List_Types['Cons']['constructor']($v['value0'], $merge($v['value1'])($v1));
                    };
                };
                if ($v['type'] === 'Nil') {
                    return $v1;
                };
                if ($v1['type'] === 'Nil') {
                    return $v;
                };
                throw new \Exception('Failed pattern match at Data.List line 473, column 3 - line 473, column 38: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
            };
        };
        $mergePairs = function ($v)  use (&$PS__Data_List_Types, &$merge, &$mergePairs) {
            if ($v['type'] === 'Cons' && $v['value1']['type'] === 'Cons') {
                return $PS__Data_List_Types['Cons']['constructor']($merge($v['value0'])($v['value1']['value0']), $mergePairs($v['value1']['value1']));
            };
            return $v;
        };
        $mergeAll = function ($_dollar_copy_v)  use (&$mergePairs) {
            $_dollar_tco_done = false;
            $_dollar_tco_result = NULL;
            $_dollar_tco_loop = function ($v)  use (&$_dollar_tco_done, &$_dollar_copy_v, &$mergePairs) {
                if ($v['type'] === 'Cons' && $v['value1']['type'] === 'Nil') {
                    $_dollar_tco_done = true;
                    return $v['value0'];
                };
                $_dollar_copy_v = $mergePairs($v);
                return;
            };
            while (!$_dollar_tco_done) {
                $_dollar_tco_result = $_dollar_tco_loop($_dollar_copy_v);
            };
            return $_dollar_tco_result;
        };
        $sequences = function ($v)  use (&$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$descending, &$singleton, &$PS__Data_Boolean, &$ascending, &$PS__Data_List_Types) {
            if ($v['type'] === 'Cons' && $v['value1']['type'] === 'Cons') {
                if ($PS__Data_Eq['eq']($PS__Data_Ordering['eqOrdering'])($cmp($v['value0'])($v['value1']['value0']))($PS__Data_Ordering['GT']())) {
                    return $descending($v['value1']['value0'])($singleton($v['value0']))($v['value1']['value1']);
                };
                if ($PS__Data_Boolean['otherwise']) {
                    return $ascending($v['value1']['value0'])(function ($v1)  use (&$PS__Data_List_Types, &$v) {
                        return $PS__Data_List_Types['Cons']['constructor']($v['value0'], $v1);
                    })($v['value1']['value1']);
                };
            };
            return $singleton($v);
        };
        $descending = function ($_dollar_copy_a)  use (&$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$PS__Data_List_Types, &$sequences) {
            return function ($_dollar_copy_as)  use (&$_dollar_copy_a, &$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$PS__Data_List_Types, &$sequences) {
                return function ($_dollar_copy_v)  use (&$_dollar_copy_a, &$_dollar_copy_as, &$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$PS__Data_List_Types, &$sequences) {
                    $_dollar_tco_var_a = $_dollar_copy_a;
                    $_dollar_tco_var_as = $_dollar_copy_as;
                    $_dollar_tco_done = false;
                    $_dollar_tco_result = NULL;
                    $_dollar_tco_loop = function ($a, $as, $v)  use (&$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$_dollar_tco_var_a, &$_dollar_tco_var_as, &$PS__Data_List_Types, &$_dollar_copy_v, &$_dollar_tco_done, &$sequences) {
                        if ($v['type'] === 'Cons' && $PS__Data_Eq['eq']($PS__Data_Ordering['eqOrdering'])($cmp($a)($v['value0']))($PS__Data_Ordering['GT']())) {
                            $_dollar_tco_var_a = $v['value0'];
                            $_dollar_tco_var_as = $PS__Data_List_Types['Cons']['constructor']($a, $as);
                            $_dollar_copy_v = $v['value1'];
                            return;
                        };
                        $_dollar_tco_done = true;
                        return $PS__Data_List_Types['Cons']['constructor']($PS__Data_List_Types['Cons']['constructor']($a, $as), $sequences($v));
                    };
                    while (!$_dollar_tco_done) {
                        $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_a, $_dollar_tco_var_as, $_dollar_copy_v);
                    };
                    return $_dollar_tco_result;
                };
            };
        };
        $ascending = function ($_dollar_copy_a)  use (&$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$PS__Data_List_Types, &$PS__Data_Function, &$singleton, &$sequences) {
            return function ($_dollar_copy_as)  use (&$_dollar_copy_a, &$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$PS__Data_List_Types, &$PS__Data_Function, &$singleton, &$sequences) {
                return function ($_dollar_copy_v)  use (&$_dollar_copy_a, &$_dollar_copy_as, &$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$PS__Data_List_Types, &$PS__Data_Function, &$singleton, &$sequences) {
                    $_dollar_tco_var_a = $_dollar_copy_a;
                    $_dollar_tco_var_as = $_dollar_copy_as;
                    $_dollar_tco_done = false;
                    $_dollar_tco_result = NULL;
                    $_dollar_tco_loop = function ($a, $as, $v)  use (&$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$_dollar_tco_var_a, &$_dollar_tco_var_as, &$PS__Data_List_Types, &$_dollar_copy_v, &$_dollar_tco_done, &$PS__Data_Function, &$singleton, &$sequences) {
                        if ($v['type'] === 'Cons' && $PS__Data_Eq['notEq']($PS__Data_Ordering['eqOrdering'])($cmp($a)($v['value0']))($PS__Data_Ordering['GT']())) {
                            $_dollar_tco_var_a = $v['value0'];
                            $_dollar_tco_var_as = function ($ys)  use (&$as, &$PS__Data_List_Types, &$a) {
                                return $as($PS__Data_List_Types['Cons']['constructor']($a, $ys));
                            };
                            $_dollar_copy_v = $v['value1'];
                            return;
                        };
                        $_dollar_tco_done = true;
                        return $PS__Data_List_Types['Cons']['constructor']($PS__Data_Function['apply']($as)($singleton($a)), $sequences($v));
                    };
                    while (!$_dollar_tco_done) {
                        $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_a, $_dollar_tco_var_as, $_dollar_copy_v);
                    };
                    return $_dollar_tco_result;
                };
            };
        };
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($mergeAll)($sequences);
    };
    $sort = function ($dictOrd)  use (&$sortBy, &$PS__Data_Ord) {
        return function ($xs)  use (&$sortBy, &$PS__Data_Ord, &$dictOrd) {
            return $sortBy($PS__Data_Ord['compare']($dictOrd))($xs);
        };
    };
    $tails = function ($v)  use (&$singleton, &$PS__Data_List_Types, &$tails) {
        if ($v['type'] === 'Nil') {
            return $singleton($PS__Data_List_Types['Nil']());
        };
        if ($v['type'] === 'Cons') {
            return $PS__Data_List_Types['Cons']['constructor']($v, $tails($v['value1']));
        };
        throw new \Exception('Failed pattern match at Data.List line 626, column 1 - line 626, column 43: ' . var_dump([ $v['constructor']['name'] ]));
    };
    $showPattern = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_List_Types) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$PS__Data_List_Types, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Pattern ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_List_Types['showList']($dictShow))($v))(')'));
        });
    };
    $reverse = (function ()  use (&$PS__Data_List_Types) {
        $go = function ($_dollar_copy_acc)  use (&$PS__Data_List_Types) {
            return function ($_dollar_copy_v)  use (&$_dollar_copy_acc, &$PS__Data_List_Types) {
                $_dollar_tco_var_acc = $_dollar_copy_acc;
                $_dollar_tco_done = false;
                $_dollar_tco_result = NULL;
                $_dollar_tco_loop = function ($acc, $v)  use (&$_dollar_tco_done, &$_dollar_tco_var_acc, &$PS__Data_List_Types, &$_dollar_copy_v) {
                    if ($v['type'] === 'Nil') {
                        $_dollar_tco_done = true;
                        return $acc;
                    };
                    if ($v['type'] === 'Cons') {
                        $_dollar_tco_var_acc = $PS__Data_List_Types['Cons']['constructor']($v['value0'], $acc);
                        $_dollar_copy_v = $v['value1'];
                        return;
                    };
                    throw new \Exception('Failed pattern match at Data.List line 368, column 3 - line 368, column 19: ' . var_dump([ $acc['constructor']['name'], $v['constructor']['name'] ]));
                };
                while (!$_dollar_tco_done) {
                    $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_acc, $_dollar_copy_v);
                };
                return $_dollar_tco_result;
            };
        };
        return $go($PS__Data_List_Types['Nil']());
    })();
    $take = (function ()  use (&$PS__Data_Ord, &$reverse, &$PS__Data_List_Types, &$PS__Data_Ring) {
        $go = function ($_dollar_copy_acc)  use (&$PS__Data_Ord, &$reverse, &$PS__Data_List_Types, &$PS__Data_Ring) {
            return function ($_dollar_copy_v)  use (&$_dollar_copy_acc, &$PS__Data_Ord, &$reverse, &$PS__Data_List_Types, &$PS__Data_Ring) {
                return function ($_dollar_copy_v1)  use (&$_dollar_copy_acc, &$_dollar_copy_v, &$PS__Data_Ord, &$reverse, &$PS__Data_List_Types, &$PS__Data_Ring) {
                    $_dollar_tco_var_acc = $_dollar_copy_acc;
                    $_dollar_tco_var_v = $_dollar_copy_v;
                    $_dollar_tco_done = false;
                    $_dollar_tco_result = NULL;
                    $_dollar_tco_loop = function ($acc, $v, $v1)  use (&$PS__Data_Ord, &$_dollar_tco_done, &$reverse, &$_dollar_tco_var_acc, &$PS__Data_List_Types, &$_dollar_tco_var_v, &$PS__Data_Ring, &$_dollar_copy_v1) {
                        if ($PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($v)(1)) {
                            $_dollar_tco_done = true;
                            return $reverse($acc);
                        };
                        if ($v1['type'] === 'Nil') {
                            $_dollar_tco_done = true;
                            return $reverse($acc);
                        };
                        if ($v1['type'] === 'Cons') {
                            $_dollar_tco_var_acc = $PS__Data_List_Types['Cons']['constructor']($v1['value0'], $acc);
                            $_dollar_tco_var_v = $PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(1);
                            $_dollar_copy_v1 = $v1['value1'];
                            return;
                        };
                        throw new \Exception('Failed pattern match at Data.List line 520, column 3 - line 520, column 35: ' . var_dump([ $acc['constructor']['name'], $v['constructor']['name'], $v1['constructor']['name'] ]));
                    };
                    while (!$_dollar_tco_done) {
                        $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_acc, $_dollar_tco_var_v, $_dollar_copy_v1);
                    };
                    return $_dollar_tco_result;
                };
            };
        };
        return $go($PS__Data_List_Types['Nil']());
    })();
    $takeWhile = function ($p)  use (&$PS__Data_List_Types, &$reverse) {
        $go = function ($_dollar_copy_acc)  use (&$p, &$PS__Data_List_Types, &$reverse) {
            return function ($_dollar_copy_v)  use (&$_dollar_copy_acc, &$p, &$PS__Data_List_Types, &$reverse) {
                $_dollar_tco_var_acc = $_dollar_copy_acc;
                $_dollar_tco_done = false;
                $_dollar_tco_result = NULL;
                $_dollar_tco_loop = function ($acc, $v)  use (&$p, &$_dollar_tco_var_acc, &$PS__Data_List_Types, &$_dollar_copy_v, &$_dollar_tco_done, &$reverse) {
                    if ($v['type'] === 'Cons' && $p($v['value0'])) {
                        $_dollar_tco_var_acc = $PS__Data_List_Types['Cons']['constructor']($v['value0'], $acc);
                        $_dollar_copy_v = $v['value1'];
                        return;
                    };
                    $_dollar_tco_done = true;
                    return $reverse($acc);
                };
                while (!$_dollar_tco_done) {
                    $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_acc, $_dollar_copy_v);
                };
                return $_dollar_tco_result;
            };
        };
        return $go($PS__Data_List_Types['Nil']());
    };
    $unsnoc = function ($lst)  use (&$PS__Data_Maybe, &$PS__Data_List_Types, &$PS__Data_Functor, &$reverse) {
        $go = function ($_dollar_copy_v)  use (&$PS__Data_Maybe, &$PS__Data_List_Types) {
            return function ($_dollar_copy_acc)  use (&$_dollar_copy_v, &$PS__Data_Maybe, &$PS__Data_List_Types) {
                $_dollar_tco_var_v = $_dollar_copy_v;
                $_dollar_tco_done = false;
                $_dollar_tco_result = NULL;
                $_dollar_tco_loop = function ($v, $acc)  use (&$_dollar_tco_done, &$PS__Data_Maybe, &$_dollar_tco_var_v, &$_dollar_copy_acc, &$PS__Data_List_Types) {
                    if ($v['type'] === 'Nil') {
                        $_dollar_tco_done = true;
                        return $PS__Data_Maybe['Nothing']();
                    };
                    if ($v['type'] === 'Cons' && $v['value1']['type'] === 'Nil') {
                        $_dollar_tco_done = true;
                        return $PS__Data_Maybe['Just']['constructor']([
                            'revInit' => $acc,
                            'last' => $v['value0']
                        ]);
                    };
                    if ($v['type'] === 'Cons') {
                        $_dollar_tco_var_v = $v['value1'];
                        $_dollar_copy_acc = $PS__Data_List_Types['Cons']['constructor']($v['value0'], $acc);
                        return;
                    };
                    throw new \Exception('Failed pattern match at Data.List line 270, column 3 - line 270, column 23: ' . var_dump([ $v['constructor']['name'], $acc['constructor']['name'] ]));
                };
                while (!$_dollar_tco_done) {
                    $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_v, $_dollar_copy_acc);
                };
                return $_dollar_tco_result;
            };
        };
        return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($h)  use (&$reverse) {
            return [
                'init' => $reverse($h['revInit']),
                'last' => $h['last']
            ];
        })($go($lst)($PS__Data_List_Types['Nil']()));
    };
    $zipWith = function ($f)  use (&$PS__Data_Function, &$PS__Data_List_Types, &$reverse) {
        return function ($xs)  use (&$PS__Data_Function, &$PS__Data_List_Types, &$f, &$reverse) {
            return function ($ys)  use (&$PS__Data_Function, &$PS__Data_List_Types, &$f, &$reverse, &$xs) {
                $go = function ($v)  use (&$PS__Data_Function, &$go, &$PS__Data_List_Types, &$f) {
                    return function ($v1)  use (&$v, &$PS__Data_Function, &$go, &$PS__Data_List_Types, &$f) {
                        return function ($acc)  use (&$v, &$v1, &$PS__Data_Function, &$go, &$PS__Data_List_Types, &$f) {
                            if ($v['type'] === 'Nil') {
                                return $acc;
                            };
                            if ($v1['type'] === 'Nil') {
                                return $acc;
                            };
                            if ($v['type'] === 'Cons' && $v1['type'] === 'Cons') {
                                return $PS__Data_Function['apply']($go($v['value1'])($v1['value1']))($PS__Data_List_Types['Cons']['constructor']($f($v['value0'])($v1['value0']), $acc));
                            };
                            throw new \Exception('Failed pattern match at Data.List line 718, column 3 - line 718, column 21: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'], $acc['constructor']['name'] ]));
                        };
                    };
                };
                return $PS__Data_Function['apply']($reverse)($go($xs)($ys)($PS__Data_List_Types['Nil']()));
            };
        };
    };
    $zip = $zipWith($PS__Data_Tuple['Tuple']['create']);
    $zipWithA = function ($dictApplicative)  use (&$PS__Data_Traversable, &$PS__Data_List_Types, &$zipWith) {
        return function ($f)  use (&$PS__Data_Traversable, &$PS__Data_List_Types, &$dictApplicative, &$zipWith) {
            return function ($xs)  use (&$PS__Data_Traversable, &$PS__Data_List_Types, &$dictApplicative, &$zipWith, &$f) {
                return function ($ys)  use (&$PS__Data_Traversable, &$PS__Data_List_Types, &$dictApplicative, &$zipWith, &$f, &$xs) {
                    return $PS__Data_Traversable['sequence']($PS__Data_List_Types['traversableList'])($dictApplicative)($zipWith($f)($xs)($ys));
                };
            };
        };
    };
    $range = function ($start)  use (&$PS__Data_Eq, &$singleton, &$PS__Data_Boolean, &$PS__Data_List_Types, &$PS__Data_Semiring, &$PS__Data_Ord, &$PS__Data_Ring) {
        return function ($end)  use (&$PS__Data_Eq, &$start, &$singleton, &$PS__Data_Boolean, &$PS__Data_List_Types, &$PS__Data_Semiring, &$PS__Data_Ord, &$PS__Data_Ring) {
            if ($PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($start)($end)) {
                return $singleton($start);
            };
            if ($PS__Data_Boolean['otherwise']) {
                $go = function ($_dollar_copy_s)  use (&$PS__Data_Eq, &$PS__Data_List_Types, &$PS__Data_Boolean, &$PS__Data_Semiring) {
                    return function ($_dollar_copy_e)  use (&$_dollar_copy_s, &$PS__Data_Eq, &$PS__Data_List_Types, &$PS__Data_Boolean, &$PS__Data_Semiring) {
                        return function ($_dollar_copy_step)  use (&$_dollar_copy_s, &$_dollar_copy_e, &$PS__Data_Eq, &$PS__Data_List_Types, &$PS__Data_Boolean, &$PS__Data_Semiring) {
                            return function ($_dollar_copy_rest)  use (&$_dollar_copy_s, &$_dollar_copy_e, &$_dollar_copy_step, &$PS__Data_Eq, &$PS__Data_List_Types, &$PS__Data_Boolean, &$PS__Data_Semiring) {
                                $_dollar_tco_var_s = $_dollar_copy_s;
                                $_dollar_tco_var_e = $_dollar_copy_e;
                                $_dollar_tco_var_step = $_dollar_copy_step;
                                $_dollar_tco_done = false;
                                $_dollar_tco_result = NULL;
                                $_dollar_tco_loop = function ($s, $e, $step, $rest)  use (&$PS__Data_Eq, &$_dollar_tco_done, &$PS__Data_List_Types, &$PS__Data_Boolean, &$_dollar_tco_var_s, &$PS__Data_Semiring, &$_dollar_tco_var_e, &$_dollar_tco_var_step, &$_dollar_copy_rest) {
                                    if ($PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($s)($e)) {
                                        $_dollar_tco_done = true;
                                        return $PS__Data_List_Types['Cons']['constructor']($s, $rest);
                                    };
                                    if ($PS__Data_Boolean['otherwise']) {
                                        $_dollar_tco_var_s = $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($s)($step);
                                        $_dollar_tco_var_e = $e;
                                        $_dollar_tco_var_step = $step;
                                        $_dollar_copy_rest = $PS__Data_List_Types['Cons']['constructor']($s, $rest);
                                        return;
                                    };
                                    throw new \Exception('Failed pattern match at Data.List line 148, column 3 - line 149, column 65: ' . var_dump([ $s['constructor']['name'], $e['constructor']['name'], $step['constructor']['name'], $rest['constructor']['name'] ]));
                                };
                                while (!$_dollar_tco_done) {
                                    $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_s, $_dollar_tco_var_e, $_dollar_tco_var_step, $_dollar_copy_rest);
                                };
                                return $_dollar_tco_result;
                            };
                        };
                    };
                };
                return $go($end)($start)((function ()  use (&$PS__Data_Ord, &$start, &$end, &$PS__Data_Ring) {
                    $__local_var__223 = $PS__Data_Ord['greaterThan']($PS__Data_Ord['ordInt'])($start)($end);
                    if ($__local_var__223) {
                        return 1;
                    };
                    return $PS__Data_Ring['negate']($PS__Data_Ring['ringInt'])(1);
                })())($PS__Data_List_Types['Nil']());
            };
            throw new \Exception('Failed pattern match at Data.List line 144, column 1 - line 144, column 32: ' . var_dump([ $start['constructor']['name'], $end['constructor']['name'] ]));
        };
    };
    $partition = function ($p)  use (&$PS__Data_List_Types, &$PS__Data_Foldable) {
        return function ($xs)  use (&$p, &$PS__Data_List_Types, &$PS__Data_Foldable) {
            $select = function ($x)  use (&$p, &$PS__Data_List_Types) {
                return function ($v)  use (&$p, &$x, &$PS__Data_List_Types) {
                    $__local_var__226 = $p($x);
                    if ($__local_var__226) {
                        return [
                            'no' => $v['no'],
                            'yes' => $PS__Data_List_Types['Cons']['constructor']($x, $v['yes'])
                        ];
                    };
                    return [
                        'no' => $PS__Data_List_Types['Cons']['constructor']($x, $v['no']),
                        'yes' => $v['yes']
                    ];
                };
            };
            return $PS__Data_Foldable['foldr']($PS__Data_List_Types['foldableList'])($select)([
                'no' => $PS__Data_List_Types['Nil'](),
                'yes' => $PS__Data_List_Types['Nil']()
            ])($xs);
        };
    };
    $__null = function ($v) {
        if ($v['type'] === 'Nil') {
            return true;
        };
        return false;
    };
    $newtypePattern = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Pattern);
    $mapWithIndex = $PS__Data_FunctorWithIndex['mapWithIndex']($PS__Data_List_Types['functorWithIndexList']);
    $mapMaybe = function ($f)  use (&$reverse, &$PS__Data_List_Types) {
        $go = function ($_dollar_copy_acc)  use (&$reverse, &$f, &$PS__Data_List_Types) {
            return function ($_dollar_copy_v)  use (&$_dollar_copy_acc, &$reverse, &$f, &$PS__Data_List_Types) {
                $_dollar_tco_var_acc = $_dollar_copy_acc;
                $_dollar_tco_done = false;
                $_dollar_tco_result = NULL;
                $_dollar_tco_loop = function ($acc, $v)  use (&$_dollar_tco_done, &$reverse, &$f, &$_dollar_tco_var_acc, &$_dollar_copy_v, &$PS__Data_List_Types) {
                    if ($v['type'] === 'Nil') {
                        $_dollar_tco_done = true;
                        return $reverse($acc);
                    };
                    if ($v['type'] === 'Cons') {
                        $v1 = $f($v['value0']);
                        if ($v1['type'] === 'Nothing') {
                            $_dollar_tco_var_acc = $acc;
                            $_dollar_copy_v = $v['value1'];
                            return;
                        };
                        if ($v1['type'] === 'Just') {
                            $_dollar_tco_var_acc = $PS__Data_List_Types['Cons']['constructor']($v1['value0'], $acc);
                            $_dollar_copy_v = $v['value1'];
                            return;
                        };
                        throw new \Exception('Failed pattern match at Data.List line 419, column 5 - line 421, column 32: ' . var_dump([ $v1['constructor']['name'] ]));
                    };
                    throw new \Exception('Failed pattern match at Data.List line 417, column 3 - line 417, column 27: ' . var_dump([ $acc['constructor']['name'], $v['constructor']['name'] ]));
                };
                while (!$_dollar_tco_done) {
                    $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_acc, $_dollar_copy_v);
                };
                return $_dollar_tco_result;
            };
        };
        return $go($PS__Data_List_Types['Nil']());
    };
    $manyRec = function ($dictMonadRec)  use (&$PS__Control_Bind, &$PS__Control_Alt, &$PS__Data_Functor, &$PS__Control_Monad_Rec_Class, &$PS__Control_Applicative, &$PS__Data_Unit, &$PS__Data_Function, &$PS__Data_Bifunctor, &$PS__Data_List_Types, &$reverse) {
        return function ($dictAlternative)  use (&$PS__Control_Bind, &$dictMonadRec, &$PS__Control_Alt, &$PS__Data_Functor, &$PS__Control_Monad_Rec_Class, &$PS__Control_Applicative, &$PS__Data_Unit, &$PS__Data_Function, &$PS__Data_Bifunctor, &$PS__Data_List_Types, &$reverse) {
            return function ($p)  use (&$PS__Control_Bind, &$dictMonadRec, &$PS__Control_Alt, &$dictAlternative, &$PS__Data_Functor, &$PS__Control_Monad_Rec_Class, &$PS__Control_Applicative, &$PS__Data_Unit, &$PS__Data_Function, &$PS__Data_Bifunctor, &$PS__Data_List_Types, &$reverse) {
                $go = function ($acc)  use (&$PS__Control_Bind, &$dictMonadRec, &$PS__Control_Alt, &$dictAlternative, &$PS__Data_Functor, &$PS__Control_Monad_Rec_Class, &$p, &$PS__Control_Applicative, &$PS__Data_Unit, &$PS__Data_Function, &$PS__Data_Bifunctor, &$PS__Data_List_Types, &$reverse) {
                    return $PS__Control_Bind['bind'](($dictMonadRec['Monad0']())['Bind1']())($PS__Control_Alt['alt'](($dictAlternative['Plus1']())['Alt0']())($PS__Data_Functor['map']((($dictAlternative['Plus1']())['Alt0']())['Functor0']())($PS__Control_Monad_Rec_Class['Loop']['create'])($p))($PS__Control_Applicative['pure']($dictAlternative['Applicative0']())($PS__Control_Monad_Rec_Class['Done']['constructor']($PS__Data_Unit['unit']))))(function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictAlternative, &$PS__Data_Bifunctor, &$PS__Control_Monad_Rec_Class, &$PS__Data_List_Types, &$acc, &$reverse) {
                        return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictAlternative['Applicative0']()))($PS__Data_Bifunctor['bimap']($PS__Control_Monad_Rec_Class['bifunctorStep'])(function ($v1)  use (&$PS__Data_List_Types, &$acc) {
                            return $PS__Data_List_Types['Cons']['constructor']($v1, $acc);
                        })(function ($v1)  use (&$reverse, &$acc) {
                            return $reverse($acc);
                        })($v));
                    });
                };
                return $PS__Control_Monad_Rec_Class['tailRecM']($dictMonadRec)($go)($PS__Data_List_Types['Nil']());
            };
        };
    };
    $someRec = function ($dictMonadRec)  use (&$PS__Control_Apply, &$PS__Data_Functor, &$PS__Data_List_Types, &$manyRec) {
        return function ($dictAlternative)  use (&$PS__Control_Apply, &$PS__Data_Functor, &$PS__Data_List_Types, &$manyRec, &$dictMonadRec) {
            return function ($v)  use (&$PS__Control_Apply, &$dictAlternative, &$PS__Data_Functor, &$PS__Data_List_Types, &$manyRec, &$dictMonadRec) {
                return $PS__Control_Apply['apply'](($dictAlternative['Applicative0']())['Apply0']())($PS__Data_Functor['map']((($dictAlternative['Plus1']())['Alt0']())['Functor0']())($PS__Data_List_Types['Cons']['create'])($v))($manyRec($dictMonadRec)($dictAlternative)($v));
            };
        };
    };
    $some = function ($dictAlternative)  use (&$PS__Control_Apply, &$PS__Data_Functor, &$PS__Data_List_Types, &$PS__Control_Lazy, &$many) {
        return function ($dictLazy)  use (&$PS__Control_Apply, &$dictAlternative, &$PS__Data_Functor, &$PS__Data_List_Types, &$PS__Control_Lazy, &$many) {
            return function ($v)  use (&$PS__Control_Apply, &$dictAlternative, &$PS__Data_Functor, &$PS__Data_List_Types, &$PS__Control_Lazy, &$dictLazy, &$many) {
                return $PS__Control_Apply['apply'](($dictAlternative['Applicative0']())['Apply0']())($PS__Data_Functor['map']((($dictAlternative['Plus1']())['Alt0']())['Functor0']())($PS__Data_List_Types['Cons']['create'])($v))($PS__Control_Lazy['defer']($dictLazy)(function ($v1)  use (&$many, &$dictAlternative, &$dictLazy, &$v) {
                    return $many($dictAlternative)($dictLazy)($v);
                }));
            };
        };
    };
    $many = function ($dictAlternative)  use (&$PS__Control_Alt, &$some, &$PS__Control_Applicative, &$PS__Data_List_Types) {
        return function ($dictLazy)  use (&$PS__Control_Alt, &$dictAlternative, &$some, &$PS__Control_Applicative, &$PS__Data_List_Types) {
            return function ($v)  use (&$PS__Control_Alt, &$dictAlternative, &$some, &$dictLazy, &$PS__Control_Applicative, &$PS__Data_List_Types) {
                return $PS__Control_Alt['alt'](($dictAlternative['Plus1']())['Alt0']())($some($dictAlternative)($dictLazy)($v))($PS__Control_Applicative['pure']($dictAlternative['Applicative0']())($PS__Data_List_Types['Nil']()));
            };
        };
    };
    $length = $PS__Data_Foldable['foldl']($PS__Data_List_Types['foldableList'])(function ($acc)  use (&$PS__Data_Semiring) {
        return function ($v)  use (&$PS__Data_Semiring, &$acc) {
            return $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($acc)(1);
        };
    })(0);
    $last = function ($_dollar_copy_v)  use (&$PS__Data_Maybe) {
        $_dollar_tco_done = false;
        $_dollar_tco_result = NULL;
        $_dollar_tco_loop = function ($v)  use (&$_dollar_tco_done, &$PS__Data_Maybe, &$_dollar_copy_v) {
            if ($v['type'] === 'Cons' && $v['value1']['type'] === 'Nil') {
                $_dollar_tco_done = true;
                return $PS__Data_Maybe['Just']['constructor']($v['value0']);
            };
            if ($v['type'] === 'Cons') {
                $_dollar_copy_v = $v['value1'];
                return;
            };
            $_dollar_tco_done = true;
            return $PS__Data_Maybe['Nothing']();
        };
        while (!$_dollar_tco_done) {
            $_dollar_tco_result = $_dollar_tco_loop($_dollar_copy_v);
        };
        return $_dollar_tco_result;
    };
    $insertBy = function ($v)  use (&$singleton, &$PS__Data_List_Types, &$insertBy) {
        return function ($x)  use (&$singleton, &$v, &$PS__Data_List_Types, &$insertBy) {
            return function ($v1)  use (&$singleton, &$x, &$v, &$PS__Data_List_Types, &$insertBy) {
                if ($v1['type'] === 'Nil') {
                    return $singleton($x);
                };
                if ($v1['type'] === 'Cons') {
                    $v2 = $v($x)($v1['value0']);
                    if ($v2['type'] === 'GT') {
                        return $PS__Data_List_Types['Cons']['constructor']($v1['value0'], $insertBy($v)($x)($v1['value1']));
                    };
                    return $PS__Data_List_Types['Cons']['constructor']($x, $v1);
                };
                throw new \Exception('Failed pattern match at Data.List line 216, column 1 - line 216, column 68: ' . var_dump([ $v['constructor']['name'], $x['constructor']['name'], $v1['constructor']['name'] ]));
            };
        };
    };
    $insertAt = function ($v)  use (&$PS__Data_Maybe, &$PS__Data_List_Types, &$PS__Data_Functor, &$insertAt, &$PS__Data_Ring) {
        return function ($v1)  use (&$v, &$PS__Data_Maybe, &$PS__Data_List_Types, &$PS__Data_Functor, &$insertAt, &$PS__Data_Ring) {
            return function ($v2)  use (&$v, &$PS__Data_Maybe, &$PS__Data_List_Types, &$v1, &$PS__Data_Functor, &$insertAt, &$PS__Data_Ring) {
                if ($v === 0) {
                    return $PS__Data_Maybe['Just']['constructor']($PS__Data_List_Types['Cons']['constructor']($v1, $v2));
                };
                if ($v2['type'] === 'Cons') {
                    return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($v3)  use (&$PS__Data_List_Types, &$v2) {
                        return $PS__Data_List_Types['Cons']['constructor']($v2['value0'], $v3);
                    })($insertAt($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(1))($v1)($v2['value1']));
                };
                return $PS__Data_Maybe['Nothing']();
            };
        };
    };
    $insert = function ($dictOrd)  use (&$insertBy, &$PS__Data_Ord) {
        return $insertBy($PS__Data_Ord['compare']($dictOrd));
    };
    $init = function ($lst)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$unsnoc) {
        return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($v) {
            return $v['init'];
        })($unsnoc($lst));
    };
    $index = function ($_dollar_copy_v)  use (&$PS__Data_Maybe, &$PS__Data_Ring) {
        return function ($_dollar_copy_v1)  use (&$_dollar_copy_v, &$PS__Data_Maybe, &$PS__Data_Ring) {
            $_dollar_tco_var_v = $_dollar_copy_v;
            $_dollar_tco_done = false;
            $_dollar_tco_result = NULL;
            $_dollar_tco_loop = function ($v, $v1)  use (&$_dollar_tco_done, &$PS__Data_Maybe, &$_dollar_tco_var_v, &$_dollar_copy_v1, &$PS__Data_Ring) {
                if ($v['type'] === 'Nil') {
                    $_dollar_tco_done = true;
                    return $PS__Data_Maybe['Nothing']();
                };
                if ($v['type'] === 'Cons' && $v1 === 0) {
                    $_dollar_tco_done = true;
                    return $PS__Data_Maybe['Just']['constructor']($v['value0']);
                };
                if ($v['type'] === 'Cons') {
                    $_dollar_tco_var_v = $v['value1'];
                    $_dollar_copy_v1 = $PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v1)(1);
                    return;
                };
                throw new \Exception('Failed pattern match at Data.List line 281, column 1 - line 281, column 44: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
            };
            while (!$_dollar_tco_done) {
                $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_v, $_dollar_copy_v1);
            };
            return $_dollar_tco_result;
        };
    };
    $head = function ($v)  use (&$PS__Data_Maybe) {
        if ($v['type'] === 'Nil') {
            return $PS__Data_Maybe['Nothing']();
        };
        if ($v['type'] === 'Cons') {
            return $PS__Data_Maybe['Just']['constructor']($v['value0']);
        };
        throw new \Exception('Failed pattern match at Data.List line 230, column 1 - line 230, column 22: ' . var_dump([ $v['constructor']['name'] ]));
    };
    $transpose = function ($v)  use (&$PS__Data_List_Types, &$transpose, &$mapMaybe, &$head, &$tail) {
        if ($v['type'] === 'Nil') {
            return $PS__Data_List_Types['Nil']();
        };
        if ($v['type'] === 'Cons' && $v['value0']['type'] === 'Nil') {
            return $transpose($v['value1']);
        };
        if ($v['type'] === 'Cons' && $v['value0']['type'] === 'Cons') {
            return $PS__Data_List_Types['Cons']['constructor']($PS__Data_List_Types['Cons']['constructor']($v['value0']['value0'], $mapMaybe($head)($v['value1'])), $transpose($PS__Data_List_Types['Cons']['constructor']($v['value0']['value1'], $mapMaybe($tail)($v['value1']))));
        };
        throw new \Exception('Failed pattern match at Data.List line 752, column 1 - line 752, column 54: ' . var_dump([ $v['constructor']['name'] ]));
    };
    $groupBy = function ($v)  use (&$PS__Data_List_Types, &$span, &$PS__Data_NonEmpty, &$groupBy) {
        return function ($v1)  use (&$PS__Data_List_Types, &$span, &$v, &$PS__Data_NonEmpty, &$groupBy) {
            if ($v1['type'] === 'Nil') {
                return $PS__Data_List_Types['Nil']();
            };
            if ($v1['type'] === 'Cons') {
                $v2 = $span($v($v1['value0']))($v1['value1']);
                return $PS__Data_List_Types['Cons']['constructor']($PS__Data_NonEmpty['NonEmpty']['constructor']($v1['value0'], $v2['init']), $groupBy($v)($v2['rest']));
            };
            throw new \Exception('Failed pattern match at Data.List line 605, column 1 - line 605, column 80: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
        };
    };
    $group = function ($dictEq)  use (&$groupBy, &$PS__Data_Eq) {
        return $groupBy($PS__Data_Eq['eq']($dictEq));
    };
    $group__prime = function ($dictOrd)  use (&$PS__Control_Semigroupoid, &$group, &$sort) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($group($dictOrd['Eq0']()))($sort($dictOrd));
    };
    $fromFoldable = function ($dictFoldable)  use (&$PS__Data_Foldable, &$PS__Data_List_Types) {
        return $PS__Data_Foldable['foldr']($dictFoldable)($PS__Data_List_Types['Cons']['create'])($PS__Data_List_Types['Nil']());
    };
    $foldM = function ($dictMonad)  use (&$PS__Control_Applicative, &$PS__Control_Bind, &$foldM) {
        return function ($v)  use (&$PS__Control_Applicative, &$dictMonad, &$PS__Control_Bind, &$foldM) {
            return function ($a)  use (&$PS__Control_Applicative, &$dictMonad, &$PS__Control_Bind, &$v, &$foldM) {
                return function ($v1)  use (&$PS__Control_Applicative, &$dictMonad, &$a, &$PS__Control_Bind, &$v, &$foldM) {
                    if ($v1['type'] === 'Nil') {
                        return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($a);
                    };
                    if ($v1['type'] === 'Cons') {
                        return $PS__Control_Bind['bind']($dictMonad['Bind1']())($v($a)($v1['value0']))(function ($a__prime)  use (&$foldM, &$dictMonad, &$v, &$v1) {
                            return $foldM($dictMonad)($v)($a__prime)($v1['value1']);
                        });
                    };
                    throw new \Exception('Failed pattern match at Data.List line 763, column 1 - line 763, column 72: ' . var_dump([ $v['constructor']['name'], $a['constructor']['name'], $v1['constructor']['name'] ]));
                };
            };
        };
    };
    $findIndex = function ($fn)  use (&$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Data_Semiring) {
        $go = function ($_dollar_copy_v)  use (&$fn, &$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Data_Semiring) {
            return function ($_dollar_copy_v1)  use (&$_dollar_copy_v, &$fn, &$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Data_Semiring) {
                $_dollar_tco_var_v = $_dollar_copy_v;
                $_dollar_tco_done = false;
                $_dollar_tco_result = NULL;
                $_dollar_tco_loop = function ($v, $v1)  use (&$fn, &$_dollar_tco_done, &$PS__Data_Maybe, &$PS__Data_Boolean, &$_dollar_tco_var_v, &$PS__Data_Semiring, &$_dollar_copy_v1) {
                    if ($v1['type'] === 'Cons') {
                        if ($fn($v1['value0'])) {
                            $_dollar_tco_done = true;
                            return $PS__Data_Maybe['Just']['constructor']($v);
                        };
                        if ($PS__Data_Boolean['otherwise']) {
                            $_dollar_tco_var_v = $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($v)(1);
                            $_dollar_copy_v1 = $v1['value1'];
                            return;
                        };
                    };
                    if ($v1['type'] === 'Nil') {
                        $_dollar_tco_done = true;
                        return $PS__Data_Maybe['Nothing']();
                    };
                    throw new \Exception('Failed pattern match at Data.List line 301, column 3 - line 301, column 35: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
                };
                while (!$_dollar_tco_done) {
                    $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_v, $_dollar_copy_v1);
                };
                return $_dollar_tco_result;
            };
        };
        return $go(0);
    };
    $findLastIndex = function ($fn)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Ring, &$length, &$findIndex, &$reverse) {
        return function ($xs)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Ring, &$length, &$findIndex, &$fn, &$reverse) {
            return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($v)  use (&$PS__Data_Ring, &$length, &$xs) {
                return $PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($length($xs))(1))($v);
            })($findIndex($fn)($reverse($xs)));
        };
    };
    $filterM = function ($dictMonad)  use (&$PS__Control_Applicative, &$PS__Data_List_Types, &$PS__Control_Bind, &$filterM) {
        return function ($v)  use (&$PS__Control_Applicative, &$dictMonad, &$PS__Data_List_Types, &$PS__Control_Bind, &$filterM) {
            return function ($v1)  use (&$PS__Control_Applicative, &$dictMonad, &$PS__Data_List_Types, &$PS__Control_Bind, &$v, &$filterM) {
                if ($v1['type'] === 'Nil') {
                    return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($PS__Data_List_Types['Nil']());
                };
                if ($v1['type'] === 'Cons') {
                    return $PS__Control_Bind['bind']($dictMonad['Bind1']())($v($v1['value0']))(function ($v2)  use (&$PS__Control_Bind, &$dictMonad, &$filterM, &$v, &$v1, &$PS__Control_Applicative, &$PS__Data_List_Types) {
                        return $PS__Control_Bind['bind']($dictMonad['Bind1']())($filterM($dictMonad)($v)($v1['value1']))(function ($v3)  use (&$PS__Control_Applicative, &$dictMonad, &$v2, &$PS__Data_List_Types, &$v1) {
                            return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())((function ()  use (&$v2, &$PS__Data_List_Types, &$v1, &$v3) {
                                if ($v2) {
                                    return $PS__Data_List_Types['Cons']['constructor']($v1['value0'], $v3);
                                };
                                return $v3;
                            })());
                        });
                    });
                };
                throw new \Exception('Failed pattern match at Data.List line 403, column 1 - line 403, column 75: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
            };
        };
    };
    $filter = function ($p)  use (&$reverse, &$PS__Data_List_Types, &$PS__Data_Boolean) {
        $go = function ($_dollar_copy_acc)  use (&$reverse, &$p, &$PS__Data_List_Types, &$PS__Data_Boolean) {
            return function ($_dollar_copy_v)  use (&$_dollar_copy_acc, &$reverse, &$p, &$PS__Data_List_Types, &$PS__Data_Boolean) {
                $_dollar_tco_var_acc = $_dollar_copy_acc;
                $_dollar_tco_done = false;
                $_dollar_tco_result = NULL;
                $_dollar_tco_loop = function ($acc, $v)  use (&$_dollar_tco_done, &$reverse, &$p, &$_dollar_tco_var_acc, &$PS__Data_List_Types, &$_dollar_copy_v, &$PS__Data_Boolean) {
                    if ($v['type'] === 'Nil') {
                        $_dollar_tco_done = true;
                        return $reverse($acc);
                    };
                    if ($v['type'] === 'Cons') {
                        if ($p($v['value0'])) {
                            $_dollar_tco_var_acc = $PS__Data_List_Types['Cons']['constructor']($v['value0'], $acc);
                            $_dollar_copy_v = $v['value1'];
                            return;
                        };
                        if ($PS__Data_Boolean['otherwise']) {
                            $_dollar_tco_var_acc = $acc;
                            $_dollar_copy_v = $v['value1'];
                            return;
                        };
                    };
                    throw new \Exception('Failed pattern match at Data.List line 390, column 3 - line 390, column 27: ' . var_dump([ $acc['constructor']['name'], $v['constructor']['name'] ]));
                };
                while (!$_dollar_tco_done) {
                    $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_acc, $_dollar_copy_v);
                };
                return $_dollar_tco_result;
            };
        };
        return $go($PS__Data_List_Types['Nil']());
    };
    $intersectBy = function ($v)  use (&$PS__Data_List_Types, &$filter, &$PS__Data_Foldable, &$PS__Data_HeytingAlgebra) {
        return function ($v1)  use (&$PS__Data_List_Types, &$filter, &$PS__Data_Foldable, &$PS__Data_HeytingAlgebra, &$v) {
            return function ($v2)  use (&$v1, &$PS__Data_List_Types, &$filter, &$PS__Data_Foldable, &$PS__Data_HeytingAlgebra, &$v) {
                if ($v1['type'] === 'Nil') {
                    return $PS__Data_List_Types['Nil']();
                };
                if ($v2['type'] === 'Nil') {
                    return $PS__Data_List_Types['Nil']();
                };
                return $filter(function ($x)  use (&$PS__Data_Foldable, &$PS__Data_List_Types, &$PS__Data_HeytingAlgebra, &$v, &$v2) {
                    return $PS__Data_Foldable['any']($PS__Data_List_Types['foldableList'])($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($v($x))($v2);
                })($v1);
            };
        };
    };
    $intersect = function ($dictEq)  use (&$intersectBy, &$PS__Data_Eq) {
        return $intersectBy($PS__Data_Eq['eq']($dictEq));
    };
    $nubBy = function ($v)  use (&$PS__Data_List_Types, &$nubBy, &$filter, &$PS__Data_HeytingAlgebra) {
        return function ($v1)  use (&$PS__Data_List_Types, &$nubBy, &$v, &$filter, &$PS__Data_HeytingAlgebra) {
            if ($v1['type'] === 'Nil') {
                return $PS__Data_List_Types['Nil']();
            };
            if ($v1['type'] === 'Cons') {
                return $PS__Data_List_Types['Cons']['constructor']($v1['value0'], $nubBy($v)($filter(function ($y)  use (&$PS__Data_HeytingAlgebra, &$v, &$v1) {
                    return $PS__Data_HeytingAlgebra['not']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($v($v1['value0'])($y));
                })($v1['value1'])));
            };
            throw new \Exception('Failed pattern match at Data.List line 644, column 1 - line 644, column 59: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
        };
    };
    $nub = function ($dictEq)  use (&$nubBy, &$PS__Data_Eq) {
        return $nubBy($PS__Data_Eq['eq']($dictEq));
    };
    $eqPattern = function ($dictEq)  use (&$PS__Data_Eq, &$PS__Data_List_Types) {
        return $PS__Data_Eq['Eq'](function ($x)  use (&$PS__Data_Eq, &$PS__Data_List_Types, &$dictEq) {
            return function ($y)  use (&$PS__Data_Eq, &$PS__Data_List_Types, &$dictEq, &$x) {
                return $PS__Data_Eq['eq']($PS__Data_List_Types['eqList']($dictEq))($x)($y);
            };
        });
    };
    $ordPattern = function ($dictOrd)  use (&$PS__Data_Ord, &$eqPattern, &$PS__Data_List_Types) {
        return $PS__Data_Ord['Ord'](function ()  use (&$eqPattern, &$dictOrd) {
            return $eqPattern($dictOrd['Eq0']());
        }, function ($x)  use (&$PS__Data_Ord, &$PS__Data_List_Types, &$dictOrd) {
            return function ($y)  use (&$PS__Data_Ord, &$PS__Data_List_Types, &$dictOrd, &$x) {
                return $PS__Data_Ord['compare']($PS__Data_List_Types['ordList']($dictOrd))($x)($y);
            };
        });
    };
    $elemLastIndex = function ($dictEq)  use (&$findLastIndex, &$PS__Data_Eq) {
        return function ($x)  use (&$findLastIndex, &$PS__Data_Eq, &$dictEq) {
            return $findLastIndex(function ($v)  use (&$PS__Data_Eq, &$dictEq, &$x) {
                return $PS__Data_Eq['eq']($dictEq)($v)($x);
            });
        };
    };
    $elemIndex = function ($dictEq)  use (&$findIndex, &$PS__Data_Eq) {
        return function ($x)  use (&$findIndex, &$PS__Data_Eq, &$dictEq) {
            return $findIndex(function ($v)  use (&$PS__Data_Eq, &$dictEq, &$x) {
                return $PS__Data_Eq['eq']($dictEq)($v)($x);
            });
        };
    };
    $dropWhile = function ($p) {
        $go = function ($_dollar_copy_v)  use (&$p) {
            $_dollar_tco_done = false;
            $_dollar_tco_result = NULL;
            $_dollar_tco_loop = function ($v)  use (&$p, &$_dollar_copy_v, &$_dollar_tco_done) {
                if ($v['type'] === 'Cons' && $p($v['value0'])) {
                    $_dollar_copy_v = $v['value1'];
                    return;
                };
                $_dollar_tco_done = true;
                return $v;
            };
            while (!$_dollar_tco_done) {
                $_dollar_tco_result = $_dollar_tco_loop($_dollar_copy_v);
            };
            return $_dollar_tco_result;
        };
        return $go;
    };
    $dropEnd = function ($n)  use (&$take, &$PS__Data_Ring, &$length) {
        return function ($xs)  use (&$take, &$PS__Data_Ring, &$length, &$n) {
            return $take($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($length($xs))($n))($xs);
        };
    };
    $drop = function ($_dollar_copy_v)  use (&$PS__Data_Ord, &$PS__Data_List_Types, &$PS__Data_Ring) {
        return function ($_dollar_copy_v1)  use (&$_dollar_copy_v, &$PS__Data_Ord, &$PS__Data_List_Types, &$PS__Data_Ring) {
            $_dollar_tco_var_v = $_dollar_copy_v;
            $_dollar_tco_done = false;
            $_dollar_tco_result = NULL;
            $_dollar_tco_loop = function ($v, $v1)  use (&$PS__Data_Ord, &$_dollar_tco_done, &$PS__Data_List_Types, &$_dollar_tco_var_v, &$PS__Data_Ring, &$_dollar_copy_v1) {
                if ($PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($v)(1)) {
                    $_dollar_tco_done = true;
                    return $v1;
                };
                if ($v1['type'] === 'Nil') {
                    $_dollar_tco_done = true;
                    return $PS__Data_List_Types['Nil']();
                };
                if ($v1['type'] === 'Cons') {
                    $_dollar_tco_var_v = $PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(1);
                    $_dollar_copy_v1 = $v1['value1'];
                    return;
                };
                throw new \Exception('Failed pattern match at Data.List line 543, column 1 - line 543, column 42: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
            };
            while (!$_dollar_tco_done) {
                $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_v, $_dollar_copy_v1);
            };
            return $_dollar_tco_result;
        };
    };
    $slice = function ($start)  use (&$take, &$PS__Data_Ring, &$drop) {
        return function ($end)  use (&$take, &$PS__Data_Ring, &$start, &$drop) {
            return function ($xs)  use (&$take, &$PS__Data_Ring, &$end, &$start, &$drop) {
                return $take($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($end)($start))($drop($start)($xs));
            };
        };
    };
    $takeEnd = function ($n)  use (&$drop, &$PS__Data_Ring, &$length) {
        return function ($xs)  use (&$drop, &$PS__Data_Ring, &$length, &$n) {
            return $drop($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($length($xs))($n))($xs);
        };
    };
    $deleteBy = function ($v)  use (&$PS__Data_List_Types, &$deleteBy) {
        return function ($v1)  use (&$PS__Data_List_Types, &$v, &$deleteBy) {
            return function ($v2)  use (&$PS__Data_List_Types, &$v, &$v1, &$deleteBy) {
                if ($v2['type'] === 'Nil') {
                    return $PS__Data_List_Types['Nil']();
                };
                if ($v2['type'] === 'Cons' && $v($v1)($v2['value0'])) {
                    return $v2['value1'];
                };
                if ($v2['type'] === 'Cons') {
                    return $PS__Data_List_Types['Cons']['constructor']($v2['value0'], $deleteBy($v)($v1)($v2['value1']));
                };
                throw new \Exception('Failed pattern match at Data.List line 671, column 1 - line 671, column 67: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'], $v2['constructor']['name'] ]));
            };
        };
    };
    $unionBy = function ($eq)  use (&$PS__Data_Semigroup, &$PS__Data_List_Types, &$PS__Data_Foldable, &$PS__Data_Function, &$deleteBy, &$nubBy) {
        return function ($xs)  use (&$PS__Data_Semigroup, &$PS__Data_List_Types, &$PS__Data_Foldable, &$PS__Data_Function, &$deleteBy, &$eq, &$nubBy) {
            return function ($ys)  use (&$PS__Data_Semigroup, &$PS__Data_List_Types, &$xs, &$PS__Data_Foldable, &$PS__Data_Function, &$deleteBy, &$eq, &$nubBy) {
                return $PS__Data_Semigroup['append']($PS__Data_List_Types['semigroupList'])($xs)($PS__Data_Foldable['foldl']($PS__Data_List_Types['foldableList'])($PS__Data_Function['flip']($deleteBy($eq)))($nubBy($eq)($ys))($xs));
            };
        };
    };
    $union = function ($dictEq)  use (&$unionBy, &$PS__Data_Eq) {
        return $unionBy($PS__Data_Eq['eq']($dictEq));
    };
    $deleteAt = function ($v)  use (&$PS__Data_Maybe, &$PS__Data_Functor, &$PS__Data_List_Types, &$deleteAt, &$PS__Data_Ring) {
        return function ($v1)  use (&$v, &$PS__Data_Maybe, &$PS__Data_Functor, &$PS__Data_List_Types, &$deleteAt, &$PS__Data_Ring) {
            if ($v === 0 && $v1['type'] === 'Cons') {
                return $PS__Data_Maybe['Just']['constructor']($v1['value1']);
            };
            if ($v1['type'] === 'Cons') {
                return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($v2)  use (&$PS__Data_List_Types, &$v1) {
                    return $PS__Data_List_Types['Cons']['constructor']($v1['value0'], $v2);
                })($deleteAt($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(1))($v1['value1']));
            };
            return $PS__Data_Maybe['Nothing']();
        };
    };
    $__delete = function ($dictEq)  use (&$deleteBy, &$PS__Data_Eq) {
        return $deleteBy($PS__Data_Eq['eq']($dictEq));
    };
    $difference = function ($dictEq)  use (&$PS__Data_Foldable, &$PS__Data_List_Types, &$PS__Data_Function, &$__delete) {
        return $PS__Data_Foldable['foldl']($PS__Data_List_Types['foldableList'])($PS__Data_Function['flip']($__delete($dictEq)));
    };
    $concatMap = $PS__Data_Function['flip']($PS__Control_Bind['bind']($PS__Data_List_Types['bindList']));
    $concat = function ($v)  use (&$PS__Control_Bind, &$PS__Data_List_Types, &$PS__Control_Category) {
        return $PS__Control_Bind['bind']($PS__Data_List_Types['bindList'])($v)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    };
    $catMaybes = $mapMaybe($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    $alterAt = function ($v)  use (&$PS__Data_Function, &$PS__Data_Maybe, &$PS__Data_List_Types, &$PS__Data_Functor, &$alterAt, &$PS__Data_Ring) {
        return function ($v1)  use (&$v, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Data_List_Types, &$PS__Data_Functor, &$alterAt, &$PS__Data_Ring) {
            return function ($v2)  use (&$v, &$PS__Data_Function, &$PS__Data_Maybe, &$v1, &$PS__Data_List_Types, &$PS__Data_Functor, &$alterAt, &$PS__Data_Ring) {
                if ($v === 0 && $v2['type'] === 'Cons') {
                    return $PS__Data_Function['apply']($PS__Data_Maybe['Just']['create'])((function ()  use (&$v1, &$v2, &$PS__Data_List_Types) {
                        $v3 = $v1($v2['value0']);
                        if ($v3['type'] === 'Nothing') {
                            return $v2['value1'];
                        };
                        if ($v3['type'] === 'Just') {
                            return $PS__Data_List_Types['Cons']['constructor']($v3['value0'], $v2['value1']);
                        };
                        throw new \Exception('Failed pattern match at Data.List line 352, column 3 - line 354, column 23: ' . var_dump([ $v3['constructor']['name'] ]));
                    })());
                };
                if ($v2['type'] === 'Cons') {
                    return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($v3)  use (&$PS__Data_List_Types, &$v2) {
                        return $PS__Data_List_Types['Cons']['constructor']($v2['value0'], $v3);
                    })($alterAt($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(1))($v1)($v2['value1']));
                };
                return $PS__Data_Maybe['Nothing']();
            };
        };
    };
    $modifyAt = function ($n)  use (&$alterAt, &$PS__Control_Semigroupoid, &$PS__Data_Maybe) {
        return function ($f)  use (&$alterAt, &$n, &$PS__Control_Semigroupoid, &$PS__Data_Maybe) {
            return $alterAt($n)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['Just']['create'])($f));
        };
    };
    return [
        'toUnfoldable' => $toUnfoldable,
        'fromFoldable' => $fromFoldable,
        'singleton' => $singleton,
        'range' => $range,
        'some' => $some,
        'someRec' => $someRec,
        'many' => $many,
        'manyRec' => $manyRec,
        'null' => $__null,
        'length' => $length,
        'snoc' => $snoc,
        'insert' => $insert,
        'insertBy' => $insertBy,
        'head' => $head,
        'last' => $last,
        'tail' => $tail,
        'init' => $init,
        'uncons' => $uncons,
        'unsnoc' => $unsnoc,
        'index' => $index,
        'elemIndex' => $elemIndex,
        'elemLastIndex' => $elemLastIndex,
        'findIndex' => $findIndex,
        'findLastIndex' => $findLastIndex,
        'insertAt' => $insertAt,
        'deleteAt' => $deleteAt,
        'updateAt' => $updateAt,
        'modifyAt' => $modifyAt,
        'alterAt' => $alterAt,
        'reverse' => $reverse,
        'concat' => $concat,
        'concatMap' => $concatMap,
        'filter' => $filter,
        'filterM' => $filterM,
        'mapMaybe' => $mapMaybe,
        'catMaybes' => $catMaybes,
        'mapWithIndex' => $mapWithIndex,
        'sort' => $sort,
        'sortBy' => $sortBy,
        'Pattern' => $Pattern,
        'stripPrefix' => $stripPrefix,
        'slice' => $slice,
        'take' => $take,
        'takeEnd' => $takeEnd,
        'takeWhile' => $takeWhile,
        'drop' => $drop,
        'dropEnd' => $dropEnd,
        'dropWhile' => $dropWhile,
        'span' => $span,
        'group' => $group,
        'group\'' => $group__prime,
        'groupBy' => $groupBy,
        'partition' => $partition,
        'nub' => $nub,
        'nubBy' => $nubBy,
        'union' => $union,
        'unionBy' => $unionBy,
        'delete' => $__delete,
        'deleteBy' => $deleteBy,
        'difference' => $difference,
        'intersect' => $intersect,
        'intersectBy' => $intersectBy,
        'zipWith' => $zipWith,
        'zipWithA' => $zipWithA,
        'zip' => $zip,
        'unzip' => $unzip,
        'transpose' => $transpose,
        'foldM' => $foldM,
        'eqPattern' => $eqPattern,
        'ordPattern' => $ordPattern,
        'newtypePattern' => $newtypePattern,
        'showPattern' => $showPattern
    ];
})();
