<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Extend/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadZero/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor.Invariant/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Maybe_Last = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Maybe, &$PS__Data_Newtype, &$PS__Data_Monoid, &$PS__Control_Alt, &$PS__Control_Plus, &$PS__Control_Alternative, &$PS__Control_MonadZero) {
    $Last = function ($x) {
        return $x;
    };
    $showLast = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Maybe) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$PS__Data_Maybe, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Last ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Maybe['showMaybe']($dictShow))($v))(')'));
        });
    };
    $semigroupLast = $PS__Data_Semigroup['Semigroup'](function ($v) {
        return function ($v1)  use (&$v) {
            if ($v1['type'] === 'Just') {
                return $v1;
            };
            if ($v1['type'] === 'Nothing') {
                return $v;
            };
            throw new \Exception('Failed pattern match at Data.Maybe.Last line 52, column 1 - line 52, column 45: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
        };
    });
    $ordLast = function ($dictOrd)  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['ordMaybe']($dictOrd);
    };
    $ord1Last = $PS__Data_Maybe['ord1Maybe'];
    $newtypeLast = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Last);
    $monoidLast = $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupLast) {
        return $semigroupLast;
    }, $PS__Data_Maybe['Nothing']());
    $monadLast = $PS__Data_Maybe['monadMaybe'];
    $invariantLast = $PS__Data_Maybe['invariantMaybe'];
    $functorLast = $PS__Data_Maybe['functorMaybe'];
    $extendLast = $PS__Data_Maybe['extendMaybe'];
    $eqLast = function ($dictEq)  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['eqMaybe']($dictEq);
    };
    $eq1Last = $PS__Data_Maybe['eq1Maybe'];
    $boundedLast = function ($dictBounded)  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['boundedMaybe']($dictBounded);
    };
    $bindLast = $PS__Data_Maybe['bindMaybe'];
    $applyLast = $PS__Data_Maybe['applyMaybe'];
    $applicativeLast = $PS__Data_Maybe['applicativeMaybe'];
    $altLast = $PS__Control_Alt['Alt'](function ()  use (&$functorLast) {
        return $functorLast;
    }, $PS__Data_Semigroup['append']($semigroupLast));
    $plusLast = $PS__Control_Plus['Plus'](function ()  use (&$altLast) {
        return $altLast;
    }, $PS__Data_Monoid['mempty']($monoidLast));
    $alternativeLast = $PS__Control_Alternative['Alternative'](function ()  use (&$applicativeLast) {
        return $applicativeLast;
    }, function ()  use (&$plusLast) {
        return $plusLast;
    });
    $monadZeroLast = $PS__Control_MonadZero['MonadZero'](function ()  use (&$alternativeLast) {
        return $alternativeLast;
    }, function ()  use (&$monadLast) {
        return $monadLast;
    });
    return [
        'Last' => $Last,
        'newtypeLast' => $newtypeLast,
        'eqLast' => $eqLast,
        'eq1Last' => $eq1Last,
        'ordLast' => $ordLast,
        'ord1Last' => $ord1Last,
        'boundedLast' => $boundedLast,
        'functorLast' => $functorLast,
        'invariantLast' => $invariantLast,
        'applyLast' => $applyLast,
        'applicativeLast' => $applicativeLast,
        'bindLast' => $bindLast,
        'monadLast' => $monadLast,
        'extendLast' => $extendLast,
        'showLast' => $showLast,
        'semigroupLast' => $semigroupLast,
        'monoidLast' => $monoidLast,
        'altLast' => $altLast,
        'plusLast' => $plusLast,
        'alternativeLast' => $alternativeLast,
        'monadZeroLast' => $monadZeroLast
    ];
})();
