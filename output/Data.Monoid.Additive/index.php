<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Monoid_Additive = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Semiring, &$PS__Data_Monoid, &$PS__Data_Functor, &$PS__Data_Eq, &$PS__Data_Ord, &$PS__Control_Apply, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Control_Monad) {
    $Additive = function ($x) {
        return $x;
    };
    $showAdditive = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Additive ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $semigroupAdditive = function ($dictSemiring)  use (&$PS__Data_Semigroup, &$PS__Data_Semiring) {
        return $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$PS__Data_Semiring, &$dictSemiring) {
            return function ($v1)  use (&$PS__Data_Semiring, &$dictSemiring, &$v) {
                return $PS__Data_Semiring['add']($dictSemiring)($v)($v1);
            };
        });
    };
    $ordAdditive = function ($dictOrd) {
        return $dictOrd;
    };
    $monoidAdditive = function ($dictSemiring)  use (&$PS__Data_Monoid, &$semigroupAdditive, &$PS__Data_Semiring) {
        return $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupAdditive, &$dictSemiring) {
            return $semigroupAdditive($dictSemiring);
        }, $PS__Data_Semiring['zero']($dictSemiring));
    };
    $functorAdditive = $PS__Data_Functor['Functor'](function ($f) {
        return function ($m)  use (&$f) {
            return $f($m);
        };
    });
    $eqAdditive = function ($dictEq) {
        return $dictEq;
    };
    $eq1Additive = $PS__Data_Eq['Eq1'](function ($dictEq)  use (&$PS__Data_Eq, &$eqAdditive) {
        return $PS__Data_Eq['eq']($eqAdditive($dictEq));
    });
    $ord1Additive = $PS__Data_Ord['Ord1'](function ()  use (&$eq1Additive) {
        return $eq1Additive;
    }, function ($dictOrd)  use (&$PS__Data_Ord, &$ordAdditive) {
        return $PS__Data_Ord['compare']($ordAdditive($dictOrd));
    });
    $boundedAdditive = function ($dictBounded) {
        return $dictBounded;
    };
    $applyAdditive = $PS__Control_Apply['Apply'](function ()  use (&$functorAdditive) {
        return $functorAdditive;
    }, function ($v) {
        return function ($v1)  use (&$v) {
            return $v($v1);
        };
    });
    $bindAdditive = $PS__Control_Bind['Bind'](function ()  use (&$applyAdditive) {
        return $applyAdditive;
    }, function ($v) {
        return function ($f)  use (&$v) {
            return $f($v);
        };
    });
    $applicativeAdditive = $PS__Control_Applicative['Applicative'](function ()  use (&$applyAdditive) {
        return $applyAdditive;
    }, $Additive);
    $monadAdditive = $PS__Control_Monad['Monad'](function ()  use (&$applicativeAdditive) {
        return $applicativeAdditive;
    }, function ()  use (&$bindAdditive) {
        return $bindAdditive;
    });
    return [
        'Additive' => $Additive,
        'eqAdditive' => $eqAdditive,
        'eq1Additive' => $eq1Additive,
        'ordAdditive' => $ordAdditive,
        'ord1Additive' => $ord1Additive,
        'boundedAdditive' => $boundedAdditive,
        'showAdditive' => $showAdditive,
        'functorAdditive' => $functorAdditive,
        'applyAdditive' => $applyAdditive,
        'applicativeAdditive' => $applicativeAdditive,
        'bindAdditive' => $bindAdditive,
        'monadAdditive' => $monadAdditive,
        'semigroupAdditive' => $semigroupAdditive,
        'monoidAdditive' => $monoidAdditive
    ];
})();
