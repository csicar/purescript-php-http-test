<?php
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
$PS__Control_Applicative = (function ()  use (&$PS__Data_Unit, &$PS__Control_Apply) {
    $Applicative = function ($Apply0, $pure) {
        return [
            'Apply0' => $Apply0,
            'pure' => $pure
        ];
    };
    $pure = function ($dict) {
        return $dict['pure'];
    };
    $unless = function ($dictApplicative)  use (&$pure, &$PS__Data_Unit) {
        return function ($v)  use (&$pure, &$dictApplicative, &$PS__Data_Unit) {
            return function ($v1)  use (&$v, &$pure, &$dictApplicative, &$PS__Data_Unit) {
                if (!$v) {
                    return $v1;
                };
                if ($v) {
                    return $pure($dictApplicative)($PS__Data_Unit['unit']);
                };
                throw new \Exception('Failed pattern match at Control.Applicative line 62, column 1 - line 62, column 65: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
            };
        };
    };
    $when = function ($dictApplicative)  use (&$pure, &$PS__Data_Unit) {
        return function ($v)  use (&$pure, &$dictApplicative, &$PS__Data_Unit) {
            return function ($v1)  use (&$v, &$pure, &$dictApplicative, &$PS__Data_Unit) {
                if ($v) {
                    return $v1;
                };
                if (!$v) {
                    return $pure($dictApplicative)($PS__Data_Unit['unit']);
                };
                throw new \Exception('Failed pattern match at Control.Applicative line 57, column 1 - line 57, column 63: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
            };
        };
    };
    $liftA1 = function ($dictApplicative)  use (&$PS__Control_Apply, &$pure) {
        return function ($f)  use (&$PS__Control_Apply, &$dictApplicative, &$pure) {
            return function ($a)  use (&$PS__Control_Apply, &$dictApplicative, &$pure, &$f) {
                return $PS__Control_Apply['apply']($dictApplicative['Apply0']())($pure($dictApplicative)($f))($a);
            };
        };
    };
    $applicativeFn = $Applicative(function ()  use (&$PS__Control_Apply) {
        return $PS__Control_Apply['applyFn'];
    }, function ($x) {
        return function ($v)  use (&$x) {
            return $x;
        };
    });
    $applicativeArray = $Applicative(function ()  use (&$PS__Control_Apply) {
        return $PS__Control_Apply['applyArray'];
    }, function ($x) {
        return [ $x ];
    });
    return [
        'Applicative' => $Applicative,
        'pure' => $pure,
        'liftA1' => $liftA1,
        'unless' => $unless,
        'when' => $when,
        'applicativeFn' => $applicativeFn,
        'applicativeArray' => $applicativeArray
    ];
})();
