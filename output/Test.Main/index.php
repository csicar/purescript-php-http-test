<?php
require_once(dirname(__FILE__) . '/../Effect/index.php');
require_once(dirname(__FILE__) . '/../Effect.Console/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Test_Main = (function ()  use (&$PS__Effect_Console) {
    $main = $PS__Effect_Console['log']('You should add some tests.');
    return [
        'main' => $main
    ];
})();
