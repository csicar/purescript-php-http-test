<?php
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Node_Encoding = (function ()  use (&$PS__Data_Show) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $ASCII = function () {
        return [
            'type' => 'ASCII'
        ];
    };
    $UTF8 = function () {
        return [
            'type' => 'UTF8'
        ];
    };
    $UTF16LE = function () {
        return [
            'type' => 'UTF16LE'
        ];
    };
    $UCS2 = function () {
        return [
            'type' => 'UCS2'
        ];
    };
    $Base64 = function () {
        return [
            'type' => 'Base64'
        ];
    };
    $Latin1 = function () {
        return [
            'type' => 'Latin1'
        ];
    };
    $Binary = function () {
        return [
            'type' => 'Binary'
        ];
    };
    $Hex = function () {
        return [
            'type' => 'Hex'
        ];
    };
    $showEncoding = $PS__Data_Show['Show'](function ($v) {
        if ($v['type'] === 'ASCII') {
            return 'ASCII';
        };
        if ($v['type'] === 'UTF8') {
            return 'UTF8';
        };
        if ($v['type'] === 'UTF16LE') {
            return 'UTF16LE';
        };
        if ($v['type'] === 'UCS2') {
            return 'UCS2';
        };
        if ($v['type'] === 'Base64') {
            return 'Base64';
        };
        if ($v['type'] === 'Latin1') {
            return 'Latin1';
        };
        if ($v['type'] === 'Binary') {
            return 'Binary';
        };
        if ($v['type'] === 'Hex') {
            return 'Hex';
        };
        throw new \Exception('Failed pattern match at Node.Encoding line 19, column 1 - line 19, column 39: ' . var_dump([ $v['constructor']['name'] ]));
    });
    $encodingToNode = function ($v) {
        if ($v['type'] === 'ASCII') {
            return 'ascii';
        };
        if ($v['type'] === 'UTF8') {
            return 'utf8';
        };
        if ($v['type'] === 'UTF16LE') {
            return 'utf16le';
        };
        if ($v['type'] === 'UCS2') {
            return 'ucs2';
        };
        if ($v['type'] === 'Base64') {
            return 'base64';
        };
        if ($v['type'] === 'Latin1') {
            return 'latin1';
        };
        if ($v['type'] === 'Binary') {
            return 'binary';
        };
        if ($v['type'] === 'Hex') {
            return 'hex';
        };
        throw new \Exception('Failed pattern match at Node.Encoding line 31, column 1 - line 31, column 37: ' . var_dump([ $v['constructor']['name'] ]));
    };
    $byteLength = function ($str)  use (&$__foreign, &$encodingToNode) {
        return function ($enc)  use (&$__foreign, &$str, &$encodingToNode) {
            return $__foreign['byteLengthImpl']($str)($encodingToNode($enc));
        };
    };
    return [
        'ASCII' => $ASCII,
        'UTF8' => $UTF8,
        'UTF16LE' => $UTF16LE,
        'UCS2' => $UCS2,
        'Base64' => $Base64,
        'Latin1' => $Latin1,
        'Binary' => $Binary,
        'Hex' => $Hex,
        'encodingToNode' => $encodingToNode,
        'byteLength' => $byteLength,
        'showEncoding' => $showEncoding
    ];
})();
