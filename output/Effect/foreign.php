<?php

$exports["pureE"] = function ($a) {
  return function () use (&$a) {
    return $a;
  };
};

$exports["bindE"] = function ($a) {
  return function ($f) use (&$a) {
    return function () use (&$f, &$a) {
      return $f($a())();
    };
  };
};

$exports["untilE"] = function ($f) {
  return function () use (&$f) {
    while (!$f());
    return [];
  };
};

$exports["whileE"] = function ($f) {
  return function ($a) use (&$f) {
    return function () use (&$f, &$a) {
      while ($f()) {
        $a();
      }
      return [];
    };
  };
};

$exports["forE"] = function ($lo) {
  return function ($hi) use (&$lo) {
    return function ($f) use (&$hi, &$lo) {
      return function () use (&$hi, &$lo, &$f) {
        for ($i = $lo; $i < $hi; $i++) {
          $f($i)();
        }
      };
    };
  };
};

$exports["foreachE"] = function ($as) {
  return function ($f) use (&$as) {
    return function () use (&$as, &$f) {
      for ($i = 0, $l = count($as); $i < $l; $i++) {
        $f($as[$i])();
      }
    };
  };
};
