<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Effect = (function ()  use (&$PS__Control_Monad, &$PS__Control_Bind, &$PS__Control_Apply, &$PS__Control_Applicative, &$PS__Data_Functor, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $monadEffect = $PS__Control_Monad['Monad'](function ()  use (&$applicativeEffect) {
        return $applicativeEffect;
    }, function ()  use (&$bindEffect) {
        return $bindEffect;
    });
    $bindEffect = $PS__Control_Bind['Bind'](function ()  use (&$applyEffect) {
        return $applyEffect;
    }, $__foreign['bindE']);
    $applyEffect = $PS__Control_Apply['Apply'](function ()  use (&$functorEffect) {
        return $functorEffect;
    }, $PS__Control_Monad['ap']($monadEffect));
    $applicativeEffect = $PS__Control_Applicative['Applicative'](function ()  use (&$applyEffect) {
        return $applyEffect;
    }, $__foreign['pureE']);
    $functorEffect = $PS__Data_Functor['Functor']($PS__Control_Applicative['liftA1']($applicativeEffect));
    $semigroupEffect = function ($dictSemigroup)  use (&$PS__Data_Semigroup, &$PS__Control_Apply, &$applyEffect) {
        return $PS__Data_Semigroup['Semigroup']($PS__Control_Apply['lift2']($applyEffect)($PS__Data_Semigroup['append']($dictSemigroup)));
    };
    $monoidEffect = function ($dictMonoid)  use (&$PS__Data_Monoid, &$semigroupEffect, &$__foreign) {
        return $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupEffect, &$dictMonoid) {
            return $semigroupEffect($dictMonoid['Semigroup0']());
        }, $__foreign['pureE']($PS__Data_Monoid['mempty']($dictMonoid)));
    };
    return [
        'functorEffect' => $functorEffect,
        'applyEffect' => $applyEffect,
        'applicativeEffect' => $applicativeEffect,
        'bindEffect' => $bindEffect,
        'monadEffect' => $monadEffect,
        'semigroupEffect' => $semigroupEffect,
        'monoidEffect' => $monoidEffect,
        'untilE' => $__foreign['untilE'],
        'whileE' => $__foreign['whileE'],
        'forE' => $__foreign['forE'],
        'foreachE' => $__foreign['foreachE']
    ];
})();
