<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor/index.php');
require_once(dirname(__FILE__) . '/../Data.Either/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Identity/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Effect/index.php');
require_once(dirname(__FILE__) . '/../Effect.Ref/index.php');
require_once(dirname(__FILE__) . '/../Partial.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Rec_Class = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Data_Maybe, &$PS__Data_Identity, &$PS__Control_Monad, &$PS__Data_Either, &$PS__Effect, &$PS__Partial_Unsafe, &$PS__Control_Bind, &$PS__Effect_Ref, &$PS__Control_Applicative, &$PS__Data_Functor, &$PS__Data_Unit, &$PS__Data_Bifunctor) {
    $Loop = [
        'constructor' => function ($value0) {
            return [
                'type' => 'Loop',
                'value0' => $value0
            ];
        },
        'create' => function ($value0)  use (&$Loop) {
            return $Loop['constructor']($value0);
        }
    ];
    $Done = [
        'constructor' => function ($value0) {
            return [
                'type' => 'Done',
                'value0' => $value0
            ];
        },
        'create' => function ($value0)  use (&$Done) {
            return $Done['constructor']($value0);
        }
    ];
    $MonadRec = function ($Monad0, $tailRecM) {
        return [
            'Monad0' => $Monad0,
            'tailRecM' => $tailRecM
        ];
    };
    $tailRecM = function ($dict) {
        return $dict['tailRecM'];
    };
    $tailRecM2 = function ($dictMonadRec)  use (&$tailRecM) {
        return function ($f)  use (&$tailRecM, &$dictMonadRec) {
            return function ($a)  use (&$tailRecM, &$dictMonadRec, &$f) {
                return function ($b)  use (&$tailRecM, &$dictMonadRec, &$f, &$a) {
                    return $tailRecM($dictMonadRec)(function ($o)  use (&$f) {
                        return $f($o['a'])($o['b']);
                    })([
                        'a' => $a,
                        'b' => $b
                    ]);
                };
            };
        };
    };
    $tailRecM3 = function ($dictMonadRec)  use (&$tailRecM) {
        return function ($f)  use (&$tailRecM, &$dictMonadRec) {
            return function ($a)  use (&$tailRecM, &$dictMonadRec, &$f) {
                return function ($b)  use (&$tailRecM, &$dictMonadRec, &$f, &$a) {
                    return function ($c)  use (&$tailRecM, &$dictMonadRec, &$f, &$a, &$b) {
                        return $tailRecM($dictMonadRec)(function ($o)  use (&$f) {
                            return $f($o['a'])($o['b'])($o['c']);
                        })([
                            'a' => $a,
                            'b' => $b,
                            'c' => $c
                        ]);
                    };
                };
            };
        };
    };
    $tailRec = function ($f)  use (&$PS__Control_Semigroupoid) {
        $go = function ($_dollar_copy_v)  use (&$f) {
            $_dollar_tco_done = false;
            $_dollar_tco_result = NULL;
            $_dollar_tco_loop = function ($v)  use (&$_dollar_copy_v, &$f, &$_dollar_tco_done) {
                if ($v['type'] === 'Loop') {
                    $_dollar_copy_v = $f($v['value0']);
                    return;
                };
                if ($v['type'] === 'Done') {
                    $_dollar_tco_done = true;
                    return $v['value0'];
                };
                throw new \Exception('Failed pattern match at Control.Monad.Rec.Class line 91, column 3 - line 91, column 25: ' . var_dump([ $v['constructor']['name'] ]));
            };
            while (!$_dollar_tco_done) {
                $_dollar_tco_result = $_dollar_tco_loop($_dollar_copy_v);
            };
            return $_dollar_tco_result;
        };
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($go)($f);
    };
    $monadRecMaybe = $MonadRec(function ()  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['monadMaybe'];
    }, function ($f)  use (&$Done, &$PS__Data_Maybe, &$Loop, &$tailRec) {
        return function ($a0)  use (&$Done, &$PS__Data_Maybe, &$Loop, &$f, &$tailRec) {
            $g = function ($v)  use (&$Done, &$PS__Data_Maybe, &$Loop, &$f) {
                if ($v['type'] === 'Nothing') {
                    return $Done['constructor']($PS__Data_Maybe['Nothing']());
                };
                if ($v['type'] === 'Just' && $v['value0']['type'] === 'Loop') {
                    return $Loop['constructor']($f($v['value0']['value0']));
                };
                if ($v['type'] === 'Just' && $v['value0']['type'] === 'Done') {
                    return $Done['constructor']($PS__Data_Maybe['Just']['constructor']($v['value0']['value0']));
                };
                throw new \Exception('Failed pattern match at Control.Monad.Rec.Class line 127, column 7 - line 127, column 31: ' . var_dump([ $v['constructor']['name'] ]));
            };
            return $tailRec($g)($f($a0));
        };
    });
    $monadRecIdentity = $MonadRec(function ()  use (&$PS__Data_Identity) {
        return $PS__Data_Identity['monadIdentity'];
    }, function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Identity, &$tailRec) {
        $runIdentity = function ($v) {
            return $v;
        };
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Identity['Identity'])($tailRec($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($runIdentity)($f)));
    });
    $monadRecFunction = $MonadRec(function ()  use (&$PS__Control_Monad) {
        return $PS__Control_Monad['monadFn'];
    }, function ($f)  use (&$tailRec) {
        return function ($a0)  use (&$tailRec, &$f) {
            return function ($e)  use (&$tailRec, &$f, &$a0) {
                return $tailRec(function ($a)  use (&$f, &$e) {
                    return $f($a)($e);
                })($a0);
            };
        };
    });
    $monadRecEither = $MonadRec(function ()  use (&$PS__Data_Either) {
        return $PS__Data_Either['monadEither'];
    }, function ($f)  use (&$Done, &$PS__Data_Either, &$Loop, &$tailRec) {
        return function ($a0)  use (&$Done, &$PS__Data_Either, &$Loop, &$f, &$tailRec) {
            $g = function ($v)  use (&$Done, &$PS__Data_Either, &$Loop, &$f) {
                if ($v['type'] === 'Left') {
                    return $Done['constructor']($PS__Data_Either['Left']['constructor']($v['value0']));
                };
                if ($v['type'] === 'Right' && $v['value0']['type'] === 'Loop') {
                    return $Loop['constructor']($f($v['value0']['value0']));
                };
                if ($v['type'] === 'Right' && $v['value0']['type'] === 'Done') {
                    return $Done['constructor']($PS__Data_Either['Right']['constructor']($v['value0']['value0']));
                };
                throw new \Exception('Failed pattern match at Control.Monad.Rec.Class line 119, column 7 - line 119, column 33: ' . var_dump([ $v['constructor']['name'] ]));
            };
            return $tailRec($g)($f($a0));
        };
    });
    $monadRecEffect = $MonadRec(function ()  use (&$PS__Effect) {
        return $PS__Effect['monadEffect'];
    }, function ($f)  use (&$PS__Partial_Unsafe, &$PS__Control_Bind, &$PS__Effect, &$PS__Effect_Ref, &$PS__Control_Applicative, &$PS__Data_Functor) {
        return function ($a)  use (&$PS__Partial_Unsafe, &$PS__Control_Bind, &$PS__Effect, &$PS__Effect_Ref, &$f, &$PS__Control_Applicative, &$PS__Data_Functor) {
            $fromDone = $PS__Partial_Unsafe['unsafePartial'](function ($dictPartial) {
                return function ($v)  use (&$dictPartial) {
                    $___unused = function ($dictPartial1) {
                        return function ($__local_var__19) {
                            return $__local_var__19;
                        };
                    };
                    return $___unused($dictPartial)((function ()  use (&$v) {
                        if ($v['type'] === 'Done') {
                            return $v['value0'];
                        };
                        throw new \Exception('Failed pattern match at Control.Monad.Rec.Class line 111, column 30 - line 111, column 44: ' . var_dump([ $v['constructor']['name'] ]));
                    })());
                };
            });
            return $PS__Control_Bind['bind']($PS__Effect['bindEffect'])($PS__Control_Bind['bindFlipped']($PS__Effect['bindEffect'])($PS__Effect_Ref['new'])($f($a)))(function ($v)  use (&$PS__Control_Bind, &$PS__Effect, &$PS__Effect_Ref, &$f, &$PS__Control_Applicative, &$PS__Data_Functor, &$fromDone) {
                return $PS__Control_Bind['discard']($PS__Control_Bind['discardUnit'])($PS__Effect['bindEffect'])($PS__Effect['untilE']($PS__Control_Bind['bind']($PS__Effect['bindEffect'])($PS__Effect_Ref['read']($v))(function ($v1)  use (&$PS__Control_Bind, &$PS__Effect, &$f, &$PS__Effect_Ref, &$v, &$PS__Control_Applicative) {
                    if ($v1['type'] === 'Loop') {
                        return $PS__Control_Bind['bind']($PS__Effect['bindEffect'])($f($v1['value0']))(function ($v2)  use (&$PS__Control_Bind, &$PS__Effect, &$PS__Effect_Ref, &$v, &$PS__Control_Applicative) {
                            return $PS__Control_Bind['bind']($PS__Effect['bindEffect'])($PS__Effect_Ref['write']($v2)($v))(function ($v3)  use (&$PS__Control_Applicative, &$PS__Effect) {
                                return $PS__Control_Applicative['pure']($PS__Effect['applicativeEffect'])(false);
                            });
                        });
                    };
                    if ($v1['type'] === 'Done') {
                        return $PS__Control_Applicative['pure']($PS__Effect['applicativeEffect'])(true);
                    };
                    throw new \Exception('Failed pattern match at Control.Monad.Rec.Class line 102, column 22 - line 107, column 28: ' . var_dump([ $v1['constructor']['name'] ]));
                })))(function ()  use (&$PS__Data_Functor, &$PS__Effect, &$fromDone, &$PS__Effect_Ref, &$v) {
                    return $PS__Data_Functor['map']($PS__Effect['functorEffect'])($fromDone)($PS__Effect_Ref['read']($v));
                });
            });
        };
    });
    $functorStep = $PS__Data_Functor['Functor'](function ($f)  use (&$Loop, &$Done) {
        return function ($m)  use (&$Loop, &$Done, &$f) {
            if ($m['type'] === 'Loop') {
                return $Loop['constructor']($m['value0']);
            };
            if ($m['type'] === 'Done') {
                return $Done['constructor']($f($m['value0']));
            };
            throw new \Exception('Failed pattern match at Control.Monad.Rec.Class line 25, column 8 - line 25, column 48: ' . var_dump([ $m['constructor']['name'] ]));
        };
    });
    $forever = function ($dictMonadRec)  use (&$tailRecM, &$PS__Data_Functor, &$Loop, &$PS__Data_Unit) {
        return function ($ma)  use (&$tailRecM, &$dictMonadRec, &$PS__Data_Functor, &$Loop, &$PS__Data_Unit) {
            return $tailRecM($dictMonadRec)(function ($u)  use (&$PS__Data_Functor, &$dictMonadRec, &$Loop, &$ma) {
                return $PS__Data_Functor['voidRight'](((($dictMonadRec['Monad0']())['Bind1']())['Apply0']())['Functor0']())($Loop['constructor']($u))($ma);
            })($PS__Data_Unit['unit']);
        };
    };
    $bifunctorStep = $PS__Data_Bifunctor['Bifunctor'](function ($v)  use (&$Loop, &$Done) {
        return function ($v1)  use (&$Loop, &$v, &$Done) {
            return function ($v2)  use (&$Loop, &$v, &$Done, &$v1) {
                if ($v2['type'] === 'Loop') {
                    return $Loop['constructor']($v($v2['value0']));
                };
                if ($v2['type'] === 'Done') {
                    return $Done['constructor']($v1($v2['value0']));
                };
                throw new \Exception('Failed pattern match at Control.Monad.Rec.Class line 27, column 1 - line 27, column 41: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'], $v2['constructor']['name'] ]));
            };
        };
    });
    return [
        'Loop' => $Loop,
        'Done' => $Done,
        'MonadRec' => $MonadRec,
        'tailRec' => $tailRec,
        'tailRecM' => $tailRecM,
        'tailRecM2' => $tailRecM2,
        'tailRecM3' => $tailRecM3,
        'forever' => $forever,
        'functorStep' => $functorStep,
        'bifunctorStep' => $bifunctorStep,
        'monadRecIdentity' => $monadRecIdentity,
        'monadRecEffect' => $monadRecEffect,
        'monadRecFunction' => $monadRecFunction,
        'monadRecEither' => $monadRecEither,
        'monadRecMaybe' => $monadRecMaybe
    ];
})();
