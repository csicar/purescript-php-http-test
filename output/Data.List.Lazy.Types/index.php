<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad/index.php');
require_once(dirname(__FILE__) . '/../Control.Extend/index.php');
require_once(dirname(__FILE__) . '/../Control.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadPlus/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadZero/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.FoldableWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.FunctorWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.TraversableWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable1/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_List_Lazy_Types = (function ()  use (&$PS__Data_Function, &$PS__Data_Lazy, &$PS__Data_Newtype, &$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$PS__Data_Functor, &$PS__Data_Show, &$PS__Data_NonEmpty, &$PS__Data_Monoid, &$PS__Control_Lazy, &$PS__Data_Eq, &$PS__Data_Ord, &$PS__Data_Ordering, &$PS__Data_Foldable, &$PS__Control_Extend, &$PS__Data_FoldableWithIndex, &$PS__Data_Tuple, &$PS__Data_Semiring, &$PS__Data_Ring, &$PS__Data_FunctorWithIndex, &$PS__Data_Traversable, &$PS__Control_Category, &$PS__Control_Apply, &$PS__Control_Applicative, &$PS__Data_TraversableWithIndex, &$PS__Data_Unfoldable1, &$PS__Data_Unfoldable, &$PS__Control_Comonad, &$PS__Control_Monad, &$PS__Control_Bind, &$PS__Control_Alt, &$PS__Control_Plus, &$PS__Control_Alternative, &$PS__Control_MonadZero, &$PS__Control_MonadPlus) {
    $List = function ($x) {
        return $x;
    };
    $Nil = function () {
        return [
            'type' => 'Nil'
        ];
    };
    $Cons = [
        'constructor' => function ($value0, $value1) {
            return [
                'type' => 'Cons',
                'value0' => $value0,
                'value1' => $value1
            ];
        },
        'create' => function ($value0)  use (&$Cons) {
            return function ($value1)  use (&$Cons, &$value0) {
                return $Cons['constructor']($value0, $value1);
            };
        }
    ];
    $NonEmptyList = function ($x) {
        return $x;
    };
    $nil = $PS__Data_Function['apply']($List)($PS__Data_Lazy['defer'](function ($v)  use (&$Nil) {
        return $Nil();
    }));
    $newtypeNonEmptyList = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $NonEmptyList);
    $newtypeList = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $List);
    $step = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Lazy['force'])($PS__Data_Newtype['unwrap']($newtypeList));
    $semigroupList = $PS__Data_Semigroup['Semigroup'](function ($xs)  use (&$step, &$Cons, &$PS__Data_Semigroup, &$semigroupList, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype, &$newtypeList) {
        return function ($ys)  use (&$step, &$Cons, &$PS__Data_Semigroup, &$semigroupList, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype, &$newtypeList, &$xs) {
            $go = function ($v)  use (&$step, &$ys, &$Cons, &$PS__Data_Semigroup, &$semigroupList) {
                if ($v['type'] === 'Nil') {
                    return $step($ys);
                };
                if ($v['type'] === 'Cons') {
                    return $Cons['constructor']($v['value0'], $PS__Data_Semigroup['append']($semigroupList)($v['value1'])($ys));
                };
                throw new \Exception('Failed pattern match at Data.List.Lazy.Types line 98, column 5 - line 98, column 21: ' . var_dump([ $v['constructor']['name'] ]));
            };
            return $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go)($PS__Data_Newtype['unwrap']($newtypeList)($xs));
        };
    });
    $showList = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$step) {
        return $PS__Data_Show['Show'](function ($xs)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow, &$step) {
            $go = function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow, &$go, &$step) {
                if ($v['type'] === 'Nil') {
                    return 'Nil';
                };
                if ($v['type'] === 'Cons') {
                    return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Cons ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v['value0']))($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])(' ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($go($step($v['value1'])))(')'))));
                };
                throw new \Exception('Failed pattern match at Data.List.Lazy.Types line 64, column 5 - line 65, column 5: ' . var_dump([ $v['constructor']['name'] ]));
            };
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('fromStrict (')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($go($step($xs)))(')'));
        });
    };
    $showNonEmptyList = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Lazy, &$PS__Data_NonEmpty, &$showList) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$PS__Data_Lazy, &$PS__Data_NonEmpty, &$dictShow, &$showList) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(NonEmptyList ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Lazy['showLazy']($PS__Data_NonEmpty['showNonEmpty']($dictShow)($showList($dictShow))))($v))(')'));
        });
    };
    $monoidList = $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupList) {
        return $semigroupList;
    }, $nil);
    $lazyList = $PS__Control_Lazy['Lazy'](function ($f)  use (&$PS__Data_Function, &$List, &$PS__Data_Lazy, &$PS__Control_Semigroupoid, &$step) {
        return $PS__Data_Function['apply']($List)($PS__Data_Lazy['defer']($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($step)($f)));
    });
    $functorList = $PS__Data_Functor['Functor'](function ($f)  use (&$Nil, &$Cons, &$PS__Data_Functor, &$functorList, &$PS__Data_Lazy, &$PS__Data_Newtype, &$newtypeList) {
        return function ($xs)  use (&$Nil, &$Cons, &$f, &$PS__Data_Functor, &$functorList, &$PS__Data_Lazy, &$PS__Data_Newtype, &$newtypeList) {
            $go = function ($v)  use (&$Nil, &$Cons, &$f, &$PS__Data_Functor, &$functorList) {
                if ($v['type'] === 'Nil') {
                    return $Nil();
                };
                if ($v['type'] === 'Cons') {
                    return $Cons['constructor']($f($v['value0']), $PS__Data_Functor['map']($functorList)($f)($v['value1']));
                };
                throw new \Exception('Failed pattern match at Data.List.Lazy.Types line 107, column 5 - line 107, column 17: ' . var_dump([ $v['constructor']['name'] ]));
            };
            return $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go)($PS__Data_Newtype['unwrap']($newtypeList)($xs));
        };
    });
    $functorNonEmptyList = $PS__Data_Functor['Functor'](function ($f)  use (&$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_NonEmpty, &$functorList) {
        return function ($v)  use (&$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_NonEmpty, &$functorList, &$f) {
            return $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($PS__Data_Functor['map']($PS__Data_NonEmpty['functorNonEmpty']($functorList))($f))($v);
        };
    });
    $eq1List = $PS__Data_Eq['Eq1'](function ($dictEq)  use (&$PS__Data_Eq, &$step) {
        return function ($xs)  use (&$PS__Data_Eq, &$dictEq, &$step) {
            return function ($ys)  use (&$PS__Data_Eq, &$dictEq, &$step, &$xs) {
                $go = function ($_dollar_copy_v)  use (&$PS__Data_Eq, &$dictEq, &$step) {
                    return function ($_dollar_copy_v1)  use (&$_dollar_copy_v, &$PS__Data_Eq, &$dictEq, &$step) {
                        $_dollar_tco_var_v = $_dollar_copy_v;
                        $_dollar_tco_done = false;
                        $_dollar_tco_result = NULL;
                        $_dollar_tco_loop = function ($v, $v1)  use (&$_dollar_tco_done, &$PS__Data_Eq, &$dictEq, &$_dollar_tco_var_v, &$step, &$_dollar_copy_v1) {
                            if ($v['type'] === 'Nil' && $v1['type'] === 'Nil') {
                                $_dollar_tco_done = true;
                                return true;
                            };
                            if ($v['type'] === 'Cons' && ($v1['type'] === 'Cons' && $PS__Data_Eq['eq']($dictEq)($v['value0'])($v1['value0']))) {
                                $_dollar_tco_var_v = $step($v['value1']);
                                $_dollar_copy_v1 = $step($v1['value1']);
                                return;
                            };
                            $_dollar_tco_done = true;
                            return false;
                        };
                        while (!$_dollar_tco_done) {
                            $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_v, $_dollar_copy_v1);
                        };
                        return $_dollar_tco_result;
                    };
                };
                return $go($step($xs))($step($ys));
            };
        };
    });
    $eqList = function ($dictEq)  use (&$PS__Data_Eq, &$eq1List) {
        return $PS__Data_Eq['Eq']($PS__Data_Eq['eq1']($eq1List)($dictEq));
    };
    $eqNonEmptyList = function ($dictEq)  use (&$PS__Data_Lazy, &$PS__Data_NonEmpty, &$eq1List) {
        return $PS__Data_Lazy['eqLazy']($PS__Data_NonEmpty['eqNonEmpty']($eq1List)($dictEq));
    };
    $ord1List = $PS__Data_Ord['Ord1'](function ()  use (&$eq1List) {
        return $eq1List;
    }, function ($dictOrd)  use (&$PS__Data_Ordering, &$PS__Data_Ord, &$step) {
        return function ($xs)  use (&$PS__Data_Ordering, &$PS__Data_Ord, &$dictOrd, &$step) {
            return function ($ys)  use (&$PS__Data_Ordering, &$PS__Data_Ord, &$dictOrd, &$step, &$xs) {
                $go = function ($_dollar_copy_v)  use (&$PS__Data_Ordering, &$PS__Data_Ord, &$dictOrd, &$step) {
                    return function ($_dollar_copy_v1)  use (&$_dollar_copy_v, &$PS__Data_Ordering, &$PS__Data_Ord, &$dictOrd, &$step) {
                        $_dollar_tco_var_v = $_dollar_copy_v;
                        $_dollar_tco_done = false;
                        $_dollar_tco_result = NULL;
                        $_dollar_tco_loop = function ($v, $v1)  use (&$_dollar_tco_done, &$PS__Data_Ordering, &$PS__Data_Ord, &$dictOrd, &$_dollar_tco_var_v, &$step, &$_dollar_copy_v1) {
                            if ($v['type'] === 'Nil' && $v1['type'] === 'Nil') {
                                $_dollar_tco_done = true;
                                return $PS__Data_Ordering['EQ']();
                            };
                            if ($v['type'] === 'Nil') {
                                $_dollar_tco_done = true;
                                return $PS__Data_Ordering['LT']();
                            };
                            if ($v1['type'] === 'Nil') {
                                $_dollar_tco_done = true;
                                return $PS__Data_Ordering['GT']();
                            };
                            if ($v['type'] === 'Cons' && $v1['type'] === 'Cons') {
                                $v2 = $PS__Data_Ord['compare']($dictOrd)($v['value0'])($v1['value0']);
                                if ($v2['type'] === 'EQ') {
                                    $_dollar_tco_var_v = $step($v['value1']);
                                    $_dollar_copy_v1 = $step($v1['value1']);
                                    return;
                                };
                                $_dollar_tco_done = true;
                                return $v2;
                            };
                            throw new \Exception('Failed pattern match at Data.List.Lazy.Types line 84, column 5 - line 84, column 20: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
                        };
                        while (!$_dollar_tco_done) {
                            $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_v, $_dollar_copy_v1);
                        };
                        return $_dollar_tco_result;
                    };
                };
                return $go($step($xs))($step($ys));
            };
        };
    });
    $ordList = function ($dictOrd)  use (&$PS__Data_Ord, &$eqList, &$ord1List) {
        return $PS__Data_Ord['Ord'](function ()  use (&$eqList, &$dictOrd) {
            return $eqList($dictOrd['Eq0']());
        }, $PS__Data_Ord['compare1']($ord1List)($dictOrd));
    };
    $ordNonEmptyList = function ($dictOrd)  use (&$PS__Data_Lazy, &$PS__Data_NonEmpty, &$ord1List) {
        return $PS__Data_Lazy['ordLazy']($PS__Data_NonEmpty['ordNonEmpty']($ord1List)($dictOrd));
    };
    $cons = function ($x)  use (&$PS__Data_Function, &$List, &$PS__Data_Lazy, &$Cons) {
        return function ($xs)  use (&$PS__Data_Function, &$List, &$PS__Data_Lazy, &$Cons, &$x) {
            return $PS__Data_Function['apply']($List)($PS__Data_Lazy['defer'](function ($v)  use (&$Cons, &$x, &$xs) {
                return $Cons['constructor']($x, $xs);
            }));
        };
    };
    $foldableList = $PS__Data_Foldable['Foldable'](function ($dictMonoid)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
        return function ($f)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Monoid) {
            return $PS__Data_Foldable['foldl']($foldableList)(function ($b)  use (&$PS__Data_Semigroup, &$dictMonoid, &$f) {
                return function ($a)  use (&$PS__Data_Semigroup, &$dictMonoid, &$b, &$f) {
                    return $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($b)($f($a));
                };
            })($PS__Data_Monoid['mempty']($dictMonoid));
        };
    }, function ($op)  use (&$step) {
        $go = function ($_dollar_copy_b)  use (&$step, &$op) {
            return function ($_dollar_copy_xs)  use (&$_dollar_copy_b, &$step, &$op) {
                $_dollar_tco_var_b = $_dollar_copy_b;
                $_dollar_tco_done = false;
                $_dollar_tco_result = NULL;
                $_dollar_tco_loop = function ($b, $xs)  use (&$step, &$_dollar_tco_done, &$_dollar_tco_var_b, &$op, &$_dollar_copy_xs) {
                    $v = $step($xs);
                    if ($v['type'] === 'Nil') {
                        $_dollar_tco_done = true;
                        return $b;
                    };
                    if ($v['type'] === 'Cons') {
                        $_dollar_tco_var_b = $op($b)($v['value0']);
                        $_dollar_copy_xs = $v['value1'];
                        return;
                    };
                    throw new \Exception('Failed pattern match at Data.List.Lazy.Types line 122, column 7 - line 124, column 40: ' . var_dump([ $v['constructor']['name'] ]));
                };
                while (!$_dollar_tco_done) {
                    $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_b, $_dollar_copy_xs);
                };
                return $_dollar_tco_result;
            };
        };
        return $go;
    }, function ($op)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$cons, &$nil) {
        return function ($z)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$cons, &$nil, &$op) {
            return function ($xs)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$cons, &$nil, &$op, &$z) {
                $rev = $PS__Data_Foldable['foldl']($foldableList)($PS__Data_Function['flip']($cons))($nil);
                return $PS__Data_Foldable['foldl']($foldableList)($PS__Data_Function['flip']($op))($z)($rev($xs));
            };
        };
    });
    $extendList = $PS__Control_Extend['Extend'](function ()  use (&$functorList) {
        return $functorList;
    }, function ($f)  use (&$cons, &$step, &$nil, &$PS__Data_Foldable, &$foldableList) {
        return function ($l)  use (&$cons, &$f, &$step, &$nil, &$PS__Data_Foldable, &$foldableList) {
            $go = function ($a)  use (&$cons, &$f) {
                return function ($v)  use (&$cons, &$a, &$f) {
                    $acc__prime = $cons($a)($v['acc']);
                    return [
                        'val' => $cons($f($acc__prime))($v['val']),
                        'acc' => $acc__prime
                    ];
                };
            };
            $v = $step($l);
            if ($v['type'] === 'Nil') {
                return $nil;
            };
            if ($v['type'] === 'Cons') {
                return $cons($f($l))(($PS__Data_Foldable['foldr']($foldableList)($go)([
                    'val' => $nil,
                    'acc' => $nil
                ])($v['value1']))['val']);
            };
            throw new \Exception('Failed pattern match at Data.List.Lazy.Types line 194, column 5 - line 197, column 55: ' . var_dump([ $v['constructor']['name'] ]));
        };
    });
    $extendNonEmptyList = $PS__Control_Extend['Extend'](function ()  use (&$functorNonEmptyList) {
        return $functorNonEmptyList;
    }, function ($f)  use (&$cons, &$PS__Data_Lazy, &$PS__Data_NonEmpty, &$PS__Data_Function, &$NonEmptyList, &$PS__Data_Foldable, &$foldableList, &$nil) {
        return function ($v)  use (&$cons, &$f, &$PS__Data_Lazy, &$PS__Data_NonEmpty, &$PS__Data_Function, &$NonEmptyList, &$PS__Data_Foldable, &$foldableList, &$nil) {
            $go = function ($a)  use (&$cons, &$f, &$PS__Data_Lazy, &$PS__Data_NonEmpty) {
                return function ($v1)  use (&$cons, &$f, &$PS__Data_Lazy, &$PS__Data_NonEmpty, &$a) {
                    return [
                        'val' => $cons($f($PS__Data_Lazy['defer'](function ($v2)  use (&$PS__Data_NonEmpty, &$a, &$v1) {
                            return $PS__Data_NonEmpty['NonEmpty']['constructor']($a, $v1['acc']);
                        })))($v1['val']),
                        'acc' => $cons($a)($v1['acc'])
                    ];
                };
            };
            $v1 = $PS__Data_Lazy['force']($v);
            return $PS__Data_Function['apply']($NonEmptyList)($PS__Data_Lazy['defer'](function ($v2)  use (&$PS__Data_NonEmpty, &$f, &$v, &$PS__Data_Foldable, &$foldableList, &$go, &$nil, &$v1) {
                return $PS__Data_NonEmpty['NonEmpty']['constructor']($f($v), ($PS__Data_Foldable['foldr']($foldableList)($go)([
                    'val' => $nil,
                    'acc' => $nil
                ])($v1['value1']))['val']);
            }));
        };
    });
    $foldableNonEmptyList = $PS__Data_Foldable['Foldable'](function ($dictMonoid)  use (&$PS__Data_Foldable, &$PS__Data_NonEmpty, &$foldableList, &$PS__Data_Lazy) {
        return function ($f)  use (&$PS__Data_Foldable, &$PS__Data_NonEmpty, &$foldableList, &$dictMonoid, &$PS__Data_Lazy) {
            return function ($v)  use (&$PS__Data_Foldable, &$PS__Data_NonEmpty, &$foldableList, &$dictMonoid, &$f, &$PS__Data_Lazy) {
                return $PS__Data_Foldable['foldMap']($PS__Data_NonEmpty['foldableNonEmpty']($foldableList))($dictMonoid)($f)($PS__Data_Lazy['force']($v));
            };
        };
    }, function ($f)  use (&$PS__Data_Foldable, &$PS__Data_NonEmpty, &$foldableList, &$PS__Data_Lazy) {
        return function ($b)  use (&$PS__Data_Foldable, &$PS__Data_NonEmpty, &$foldableList, &$f, &$PS__Data_Lazy) {
            return function ($v)  use (&$PS__Data_Foldable, &$PS__Data_NonEmpty, &$foldableList, &$f, &$b, &$PS__Data_Lazy) {
                return $PS__Data_Foldable['foldl']($PS__Data_NonEmpty['foldableNonEmpty']($foldableList))($f)($b)($PS__Data_Lazy['force']($v));
            };
        };
    }, function ($f)  use (&$PS__Data_Foldable, &$PS__Data_NonEmpty, &$foldableList, &$PS__Data_Lazy) {
        return function ($b)  use (&$PS__Data_Foldable, &$PS__Data_NonEmpty, &$foldableList, &$f, &$PS__Data_Lazy) {
            return function ($v)  use (&$PS__Data_Foldable, &$PS__Data_NonEmpty, &$foldableList, &$f, &$b, &$PS__Data_Lazy) {
                return $PS__Data_Foldable['foldr']($PS__Data_NonEmpty['foldableNonEmpty']($foldableList))($f)($b)($PS__Data_Lazy['force']($v));
            };
        };
    });
    $foldableWithIndexList = $PS__Data_FoldableWithIndex['FoldableWithIndex'](function ()  use (&$foldableList) {
        return $foldableList;
    }, function ($dictMonoid)  use (&$PS__Data_FoldableWithIndex, &$foldableWithIndexList, &$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
        return function ($f)  use (&$PS__Data_FoldableWithIndex, &$foldableWithIndexList, &$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Monoid) {
            return $PS__Data_FoldableWithIndex['foldlWithIndex']($foldableWithIndexList)(function ($i)  use (&$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$dictMonoid, &$f) {
                return function ($acc)  use (&$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$dictMonoid, &$f, &$i) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($acc))($f($i));
                };
            })($PS__Data_Monoid['mempty']($dictMonoid));
        };
    }, function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Tuple, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Semiring) {
        return function ($acc)  use (&$PS__Control_Semigroupoid, &$PS__Data_Tuple, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Semiring, &$f) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Tuple['snd'])($PS__Data_Foldable['foldl']($foldableList)(function ($v)  use (&$PS__Data_Tuple, &$PS__Data_Semiring, &$f) {
                return function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Semiring, &$v, &$f) {
                    return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($v['value0'])(1), $f($v['value0'])($v['value1'])($a));
                };
            })($PS__Data_Tuple['Tuple']['constructor'](0, $acc)));
        };
    }, function ($f)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Tuple, &$PS__Data_Semiring, &$cons, &$nil, &$PS__Data_Function, &$PS__Data_Ring) {
        return function ($b)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Tuple, &$PS__Data_Semiring, &$cons, &$nil, &$PS__Data_Function, &$PS__Data_Ring, &$f) {
            return function ($xs)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Tuple, &$PS__Data_Semiring, &$cons, &$nil, &$PS__Data_Function, &$PS__Data_Ring, &$f, &$b) {
                $v = (function ()  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Tuple, &$PS__Data_Semiring, &$cons, &$nil, &$xs) {
                    $rev = $PS__Data_Foldable['foldl']($foldableList)(function ($v1)  use (&$PS__Data_Tuple, &$PS__Data_Semiring, &$cons) {
                        return function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Semiring, &$v1, &$cons) {
                            return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($v1['value0'])(1), $cons($a)($v1['value1']));
                        };
                    });
                    return $rev($PS__Data_Tuple['Tuple']['constructor'](0, $nil))($xs);
                })();
                return $PS__Data_Function['apply']($PS__Data_Tuple['snd'])($PS__Data_Foldable['foldl']($foldableList)(function ($v1)  use (&$PS__Data_Tuple, &$PS__Data_Ring, &$f) {
                    return function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Ring, &$v1, &$f) {
                        return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v1['value0'])(1), $f($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v1['value0'])(1))($a)($v1['value1']));
                    };
                })($PS__Data_Tuple['Tuple']['constructor']($v['value0'], $b))($v['value1']));
            };
        };
    });
    $functorWithIndexList = $PS__Data_FunctorWithIndex['FunctorWithIndex'](function ()  use (&$functorList) {
        return $functorList;
    }, function ($f)  use (&$PS__Data_FoldableWithIndex, &$foldableWithIndexList, &$cons, &$nil) {
        return $PS__Data_FoldableWithIndex['foldrWithIndex']($foldableWithIndexList)(function ($i)  use (&$cons, &$f) {
            return function ($x)  use (&$cons, &$f, &$i) {
                return function ($acc)  use (&$cons, &$f, &$i, &$x) {
                    return $cons($f($i)($x))($acc);
                };
            };
        })($nil);
    });
    $toList = function ($v)  use (&$PS__Control_Lazy, &$lazyList, &$PS__Data_Lazy, &$cons) {
        return $PS__Control_Lazy['defer']($lazyList)(function ($v1)  use (&$PS__Data_Lazy, &$v, &$cons) {
            $v2 = $PS__Data_Lazy['force']($v);
            return $cons($v2['value0'])($v2['value1']);
        });
    };
    $semigroupNonEmptyList = $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$PS__Data_Lazy, &$PS__Data_NonEmpty, &$PS__Data_Semigroup, &$semigroupList, &$toList) {
        return function ($as__prime)  use (&$PS__Data_Lazy, &$v, &$PS__Data_NonEmpty, &$PS__Data_Semigroup, &$semigroupList, &$toList) {
            $v1 = $PS__Data_Lazy['force']($v);
            return $PS__Data_Lazy['defer'](function ($v2)  use (&$PS__Data_NonEmpty, &$v1, &$PS__Data_Semigroup, &$semigroupList, &$toList, &$as__prime) {
                return $PS__Data_NonEmpty['NonEmpty']['constructor']($v1['value0'], $PS__Data_Semigroup['append']($semigroupList)($v1['value1'])($toList($as__prime)));
            });
        };
    });
    $traversableList = $PS__Data_Traversable['Traversable'](function ()  use (&$foldableList) {
        return $foldableList;
    }, function ()  use (&$functorList) {
        return $functorList;
    }, function ($dictApplicative)  use (&$PS__Data_Traversable, &$traversableList, &$PS__Control_Category) {
        return $PS__Data_Traversable['traverse']($traversableList)($dictApplicative)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    }, function ($dictApplicative)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Control_Apply, &$PS__Data_Functor, &$cons, &$PS__Control_Applicative, &$nil) {
        return function ($f)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$cons, &$PS__Control_Applicative, &$nil) {
            return $PS__Data_Foldable['foldr']($foldableList)(function ($a)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$cons, &$f) {
                return function ($b)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$cons, &$f, &$a) {
                    return $PS__Control_Apply['apply']($dictApplicative['Apply0']())($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($cons)($f($a)))($b);
                };
            })($PS__Control_Applicative['pure']($dictApplicative)($nil));
        };
    });
    $traversableNonEmptyList = $PS__Data_Traversable['Traversable'](function ()  use (&$foldableNonEmptyList) {
        return $foldableNonEmptyList;
    }, function ()  use (&$functorNonEmptyList) {
        return $functorNonEmptyList;
    }, function ($dictApplicative)  use (&$PS__Data_Function, &$PS__Data_Functor, &$NonEmptyList, &$PS__Data_Lazy, &$PS__Data_Traversable, &$PS__Data_NonEmpty, &$traversableList) {
        return function ($v)  use (&$PS__Data_Function, &$PS__Data_Functor, &$dictApplicative, &$NonEmptyList, &$PS__Data_Lazy, &$PS__Data_Traversable, &$PS__Data_NonEmpty, &$traversableList) {
            return $PS__Data_Function['apply']($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())(function ($xxs)  use (&$PS__Data_Function, &$NonEmptyList, &$PS__Data_Lazy) {
                return $PS__Data_Function['apply']($NonEmptyList)($PS__Data_Lazy['defer'](function ($v1)  use (&$xxs) {
                    return $xxs;
                }));
            }))($PS__Data_Traversable['sequence']($PS__Data_NonEmpty['traversableNonEmpty']($traversableList))($dictApplicative)($PS__Data_Lazy['force']($v)));
        };
    }, function ($dictApplicative)  use (&$PS__Data_Function, &$PS__Data_Functor, &$NonEmptyList, &$PS__Data_Lazy, &$PS__Data_Traversable, &$PS__Data_NonEmpty, &$traversableList) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Functor, &$dictApplicative, &$NonEmptyList, &$PS__Data_Lazy, &$PS__Data_Traversable, &$PS__Data_NonEmpty, &$traversableList) {
            return function ($v)  use (&$PS__Data_Function, &$PS__Data_Functor, &$dictApplicative, &$NonEmptyList, &$PS__Data_Lazy, &$PS__Data_Traversable, &$PS__Data_NonEmpty, &$traversableList, &$f) {
                return $PS__Data_Function['apply']($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())(function ($xxs)  use (&$PS__Data_Function, &$NonEmptyList, &$PS__Data_Lazy) {
                    return $PS__Data_Function['apply']($NonEmptyList)($PS__Data_Lazy['defer'](function ($v1)  use (&$xxs) {
                        return $xxs;
                    }));
                }))($PS__Data_Traversable['traverse']($PS__Data_NonEmpty['traversableNonEmpty']($traversableList))($dictApplicative)($f)($PS__Data_Lazy['force']($v)));
            };
        };
    });
    $traversableWithIndexList = $PS__Data_TraversableWithIndex['TraversableWithIndex'](function ()  use (&$foldableWithIndexList) {
        return $foldableWithIndexList;
    }, function ()  use (&$functorWithIndexList) {
        return $functorWithIndexList;
    }, function ()  use (&$traversableList) {
        return $traversableList;
    }, function ($dictApplicative)  use (&$PS__Data_FoldableWithIndex, &$foldableWithIndexList, &$PS__Control_Apply, &$PS__Data_Functor, &$cons, &$PS__Control_Applicative, &$nil) {
        return function ($f)  use (&$PS__Data_FoldableWithIndex, &$foldableWithIndexList, &$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$cons, &$PS__Control_Applicative, &$nil) {
            return $PS__Data_FoldableWithIndex['foldrWithIndex']($foldableWithIndexList)(function ($i)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$cons, &$f) {
                return function ($a)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$cons, &$f, &$i) {
                    return function ($b)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$cons, &$f, &$i, &$a) {
                        return $PS__Control_Apply['apply']($dictApplicative['Apply0']())($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($cons)($f($i)($a)))($b);
                    };
                };
            })($PS__Control_Applicative['pure']($dictApplicative)($nil));
        };
    });
    $unfoldable1List = $PS__Data_Unfoldable1['Unfoldable1']((function ()  use (&$PS__Control_Lazy, &$lazyList, &$cons, &$nil) {
        $go = function ($f)  use (&$PS__Control_Lazy, &$lazyList, &$cons, &$go, &$nil) {
            return function ($b)  use (&$PS__Control_Lazy, &$lazyList, &$f, &$cons, &$go, &$nil) {
                return $PS__Control_Lazy['defer']($lazyList)(function ($v)  use (&$f, &$b, &$cons, &$go, &$nil) {
                    $v1 = $f($b);
                    if ($v1['value1']['type'] === 'Just') {
                        return $cons($v1['value0'])($go($f)($v1['value1']['value0']));
                    };
                    if ($v1['value1']['type'] === 'Nothing') {
                        return $cons($v1['value0'])($nil);
                    };
                    throw new \Exception('Failed pattern match at Data.List.Lazy.Types line 146, column 28 - line 148, column 33: ' . var_dump([ $v1['constructor']['name'] ]));
                });
            };
        };
        return $go;
    })());
    $unfoldableList = $PS__Data_Unfoldable['Unfoldable'](function ()  use (&$unfoldable1List) {
        return $unfoldable1List;
    }, (function ()  use (&$PS__Control_Lazy, &$lazyList, &$nil, &$cons) {
        $go = function ($f)  use (&$PS__Control_Lazy, &$lazyList, &$nil, &$cons, &$go) {
            return function ($b)  use (&$PS__Control_Lazy, &$lazyList, &$f, &$nil, &$cons, &$go) {
                return $PS__Control_Lazy['defer']($lazyList)(function ($v)  use (&$f, &$b, &$nil, &$cons, &$go) {
                    $v1 = $f($b);
                    if ($v1['type'] === 'Nothing') {
                        return $nil;
                    };
                    if ($v1['type'] === 'Just') {
                        return $cons($v1['value0']['value0'])($go($f)($v1['value0']['value1']));
                    };
                    throw new \Exception('Failed pattern match at Data.List.Lazy.Types line 152, column 28 - line 154, column 39: ' . var_dump([ $v1['constructor']['name'] ]));
                });
            };
        };
        return $go;
    })());
    $comonadNonEmptyList = $PS__Control_Comonad['Comonad'](function ()  use (&$extendNonEmptyList) {
        return $extendNonEmptyList;
    }, function ($v)  use (&$PS__Data_Function, &$PS__Data_NonEmpty, &$PS__Data_Lazy) {
        return $PS__Data_Function['apply']($PS__Data_NonEmpty['head'])($PS__Data_Lazy['force']($v));
    });
    $monadList = $PS__Control_Monad['Monad'](function ()  use (&$applicativeList) {
        return $applicativeList;
    }, function ()  use (&$bindList) {
        return $bindList;
    });
    $bindList = $PS__Control_Bind['Bind'](function ()  use (&$applyList) {
        return $applyList;
    }, function ($xs)  use (&$Nil, &$step, &$PS__Data_Semigroup, &$semigroupList, &$PS__Control_Bind, &$bindList, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype, &$newtypeList) {
        return function ($f)  use (&$Nil, &$step, &$PS__Data_Semigroup, &$semigroupList, &$PS__Control_Bind, &$bindList, &$PS__Data_Functor, &$PS__Data_Lazy, &$PS__Data_Newtype, &$newtypeList, &$xs) {
            $go = function ($v)  use (&$Nil, &$step, &$PS__Data_Semigroup, &$semigroupList, &$f, &$PS__Control_Bind, &$bindList) {
                if ($v['type'] === 'Nil') {
                    return $Nil();
                };
                if ($v['type'] === 'Cons') {
                    return $step($PS__Data_Semigroup['append']($semigroupList)($f($v['value0']))($PS__Control_Bind['bind']($bindList)($v['value1'])($f)));
                };
                throw new \Exception('Failed pattern match at Data.List.Lazy.Types line 175, column 5 - line 175, column 17: ' . var_dump([ $v['constructor']['name'] ]));
            };
            return $PS__Data_Functor['map']($PS__Data_Lazy['functorLazy'])($go)($PS__Data_Newtype['unwrap']($newtypeList)($xs));
        };
    });
    $applyList = $PS__Control_Apply['Apply'](function ()  use (&$functorList) {
        return $functorList;
    }, $PS__Control_Monad['ap']($monadList));
    $applicativeList = $PS__Control_Applicative['Applicative'](function ()  use (&$applyList) {
        return $applyList;
    }, function ($a)  use (&$cons, &$nil) {
        return $cons($a)($nil);
    });
    $applyNonEmptyList = $PS__Control_Apply['Apply'](function ()  use (&$functorNonEmptyList) {
        return $functorNonEmptyList;
    }, function ($v)  use (&$PS__Data_Lazy, &$PS__Data_NonEmpty, &$PS__Data_Semigroup, &$semigroupList, &$PS__Control_Apply, &$applyList, &$cons, &$nil) {
        return function ($v1)  use (&$PS__Data_Lazy, &$v, &$PS__Data_NonEmpty, &$PS__Data_Semigroup, &$semigroupList, &$PS__Control_Apply, &$applyList, &$cons, &$nil) {
            $v2 = $PS__Data_Lazy['force']($v1);
            $v3 = $PS__Data_Lazy['force']($v);
            return $PS__Data_Lazy['defer'](function ($v4)  use (&$PS__Data_NonEmpty, &$v3, &$v2, &$PS__Data_Semigroup, &$semigroupList, &$PS__Control_Apply, &$applyList, &$cons, &$nil) {
                return $PS__Data_NonEmpty['NonEmpty']['constructor']($v3['value0']($v2['value0']), $PS__Data_Semigroup['append']($semigroupList)($PS__Control_Apply['apply']($applyList)($v3['value1'])($cons($v2['value0'])($nil)))($PS__Control_Apply['apply']($applyList)($cons($v3['value0'])($v3['value1']))($v2['value1'])));
            });
        };
    });
    $bindNonEmptyList = $PS__Control_Bind['Bind'](function ()  use (&$applyNonEmptyList) {
        return $applyNonEmptyList;
    }, function ($v)  use (&$PS__Data_Lazy, &$PS__Data_Function, &$PS__Data_Newtype, &$newtypeNonEmptyList, &$PS__Data_NonEmpty, &$PS__Data_Semigroup, &$semigroupList, &$PS__Control_Bind, &$bindList, &$PS__Control_Semigroupoid, &$toList) {
        return function ($f)  use (&$PS__Data_Lazy, &$v, &$PS__Data_Function, &$PS__Data_Newtype, &$newtypeNonEmptyList, &$PS__Data_NonEmpty, &$PS__Data_Semigroup, &$semigroupList, &$PS__Control_Bind, &$bindList, &$PS__Control_Semigroupoid, &$toList) {
            $v1 = $PS__Data_Lazy['force']($v);
            $v2 = $PS__Data_Function['apply']($PS__Data_Lazy['force'])($PS__Data_Function['apply']($PS__Data_Newtype['unwrap']($newtypeNonEmptyList))($f($v1['value0'])));
            return $PS__Data_Lazy['defer'](function ($v3)  use (&$PS__Data_NonEmpty, &$v2, &$PS__Data_Semigroup, &$semigroupList, &$PS__Control_Bind, &$bindList, &$v1, &$PS__Control_Semigroupoid, &$toList, &$f) {
                return $PS__Data_NonEmpty['NonEmpty']['constructor']($v2['value0'], $PS__Data_Semigroup['append']($semigroupList)($v2['value1'])($PS__Control_Bind['bind']($bindList)($v1['value1'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($toList)($f))));
            });
        };
    });
    $altNonEmptyList = $PS__Control_Alt['Alt'](function ()  use (&$functorNonEmptyList) {
        return $functorNonEmptyList;
    }, $PS__Data_Semigroup['append']($semigroupNonEmptyList));
    $altList = $PS__Control_Alt['Alt'](function ()  use (&$functorList) {
        return $functorList;
    }, $PS__Data_Semigroup['append']($semigroupList));
    $plusList = $PS__Control_Plus['Plus'](function ()  use (&$altList) {
        return $altList;
    }, $nil);
    $alternativeList = $PS__Control_Alternative['Alternative'](function ()  use (&$applicativeList) {
        return $applicativeList;
    }, function ()  use (&$plusList) {
        return $plusList;
    });
    $monadZeroList = $PS__Control_MonadZero['MonadZero'](function ()  use (&$alternativeList) {
        return $alternativeList;
    }, function ()  use (&$monadList) {
        return $monadList;
    });
    $monadPlusList = $PS__Control_MonadPlus['MonadPlus'](function ()  use (&$monadZeroList) {
        return $monadZeroList;
    });
    $applicativeNonEmptyList = $PS__Control_Applicative['Applicative'](function ()  use (&$applyNonEmptyList) {
        return $applyNonEmptyList;
    }, function ($a)  use (&$PS__Data_Lazy, &$PS__Data_NonEmpty, &$plusList) {
        return $PS__Data_Lazy['defer'](function ($v)  use (&$PS__Data_NonEmpty, &$plusList, &$a) {
            return $PS__Data_NonEmpty['singleton']($plusList)($a);
        });
    });
    $monadNonEmptyList = $PS__Control_Monad['Monad'](function ()  use (&$applicativeNonEmptyList) {
        return $applicativeNonEmptyList;
    }, function ()  use (&$bindNonEmptyList) {
        return $bindNonEmptyList;
    });
    return [
        'List' => $List,
        'Nil' => $Nil,
        'Cons' => $Cons,
        'step' => $step,
        'nil' => $nil,
        'cons' => $cons,
        'NonEmptyList' => $NonEmptyList,
        'toList' => $toList,
        'newtypeList' => $newtypeList,
        'showList' => $showList,
        'eqList' => $eqList,
        'eq1List' => $eq1List,
        'ordList' => $ordList,
        'ord1List' => $ord1List,
        'lazyList' => $lazyList,
        'semigroupList' => $semigroupList,
        'monoidList' => $monoidList,
        'functorList' => $functorList,
        'functorWithIndexList' => $functorWithIndexList,
        'foldableList' => $foldableList,
        'foldableWithIndexList' => $foldableWithIndexList,
        'unfoldable1List' => $unfoldable1List,
        'unfoldableList' => $unfoldableList,
        'traversableList' => $traversableList,
        'traversableWithIndexList' => $traversableWithIndexList,
        'applyList' => $applyList,
        'applicativeList' => $applicativeList,
        'bindList' => $bindList,
        'monadList' => $monadList,
        'altList' => $altList,
        'plusList' => $plusList,
        'alternativeList' => $alternativeList,
        'monadZeroList' => $monadZeroList,
        'monadPlusList' => $monadPlusList,
        'extendList' => $extendList,
        'newtypeNonEmptyList' => $newtypeNonEmptyList,
        'eqNonEmptyList' => $eqNonEmptyList,
        'ordNonEmptyList' => $ordNonEmptyList,
        'showNonEmptyList' => $showNonEmptyList,
        'functorNonEmptyList' => $functorNonEmptyList,
        'applyNonEmptyList' => $applyNonEmptyList,
        'applicativeNonEmptyList' => $applicativeNonEmptyList,
        'bindNonEmptyList' => $bindNonEmptyList,
        'monadNonEmptyList' => $monadNonEmptyList,
        'altNonEmptyList' => $altNonEmptyList,
        'extendNonEmptyList' => $extendNonEmptyList,
        'comonadNonEmptyList' => $comonadNonEmptyList,
        'semigroupNonEmptyList' => $semigroupNonEmptyList,
        'foldableNonEmptyList' => $foldableNonEmptyList,
        'traversableNonEmptyList' => $traversableNonEmptyList
    ];
})();
