<?php
$PS__Data_Symbol = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $SProxy = function () {
        return [
            'type' => 'SProxy'
        ];
    };
    $IsSymbol = function ($reflectSymbol) {
        return [
            'reflectSymbol' => $reflectSymbol
        ];
    };
    $reifySymbol = function ($s)  use (&$__foreign, &$SProxy) {
        return function ($f)  use (&$__foreign, &$s, &$SProxy) {
            return $__foreign['unsafeCoerce'](function ($dictIsSymbol)  use (&$f) {
                return $f($dictIsSymbol);
            })([
                'reflectSymbol' => function ($v)  use (&$s) {
                    return $s;
                }
            ])($SProxy());
        };
    };
    $reflectSymbol = function ($dict) {
        return $dict['reflectSymbol'];
    };
    return [
        'IsSymbol' => $IsSymbol,
        'reflectSymbol' => $reflectSymbol,
        'reifySymbol' => $reifySymbol,
        'SProxy' => $SProxy
    ];
})();
