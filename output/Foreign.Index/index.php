<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Except.Trans/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Function.Uncurried/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Identity/index.php');
require_once(dirname(__FILE__) . '/../Data.List.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Foreign/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Foreign_Index = (function ()  use (&$PS__Data_Function_Uncurried, &$PS__Foreign, &$PS__Control_Applicative, &$PS__Control_Monad_Except_Trans, &$PS__Data_Identity, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Data_HeytingAlgebra, &$PS__Data_Eq) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Index = function ($errorAt, $hasOwnProperty, $hasProperty, $index) {
        return [
            'errorAt' => $errorAt,
            'hasOwnProperty' => $hasOwnProperty,
            'hasProperty' => $hasProperty,
            'index' => $index
        ];
    };
    $Indexable = function ($ix) {
        return [
            'ix' => $ix
        ];
    };
    $unsafeReadProp = function ($k)  use (&$PS__Data_Function_Uncurried, &$__foreign, &$PS__Foreign, &$PS__Control_Applicative, &$PS__Control_Monad_Except_Trans, &$PS__Data_Identity) {
        return function ($value)  use (&$PS__Data_Function_Uncurried, &$__foreign, &$PS__Foreign, &$PS__Control_Applicative, &$PS__Control_Monad_Except_Trans, &$PS__Data_Identity, &$k) {
            return $PS__Data_Function_Uncurried['runFn4']($__foreign['unsafeReadPropImpl'])($PS__Foreign['fail']($PS__Foreign['TypeMismatch']['constructor']('object', $PS__Foreign['typeOf']($value))))($PS__Control_Applicative['pure']($PS__Control_Monad_Except_Trans['applicativeExceptT']($PS__Data_Identity['monadIdentity'])))($k)($value);
        };
    };
    $readProp = $unsafeReadProp;
    $readIndex = $unsafeReadProp;
    $ix = function ($dict) {
        return $dict['ix'];
    };
    $index = function ($dict) {
        return $dict['index'];
    };
    $indexableExceptT = $Indexable(function ($dictIndex)  use (&$PS__Control_Bind, &$PS__Control_Monad_Except_Trans, &$PS__Data_Identity, &$PS__Data_Function, &$index) {
        return function ($f)  use (&$PS__Control_Bind, &$PS__Control_Monad_Except_Trans, &$PS__Data_Identity, &$PS__Data_Function, &$index, &$dictIndex) {
            return function ($i)  use (&$PS__Control_Bind, &$PS__Control_Monad_Except_Trans, &$PS__Data_Identity, &$PS__Data_Function, &$index, &$dictIndex, &$f) {
                return $PS__Control_Bind['bindFlipped']($PS__Control_Monad_Except_Trans['bindExceptT']($PS__Data_Identity['monadIdentity']))($PS__Data_Function['flip']($index($dictIndex))($i))($f);
            };
        };
    });
    $indexableForeign = $Indexable(function ($dictIndex)  use (&$index) {
        return $index($dictIndex);
    });
    $hasPropertyImpl = function ($v)  use (&$PS__Foreign, &$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$PS__Data_Function_Uncurried, &$__foreign) {
        return function ($value)  use (&$PS__Foreign, &$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$PS__Data_Function_Uncurried, &$__foreign, &$v) {
            if ($PS__Foreign['isNull']($value)) {
                return false;
            };
            if ($PS__Foreign['isUndefined']($value)) {
                return false;
            };
            if ($PS__Data_HeytingAlgebra['disj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Eq['eq']($PS__Data_Eq['eqString'])($PS__Foreign['typeOf']($value))('object'))($PS__Data_Eq['eq']($PS__Data_Eq['eqString'])($PS__Foreign['typeOf']($value))('function'))) {
                return $PS__Data_Function_Uncurried['runFn2']($__foreign['unsafeHasProperty'])($v)($value);
            };
            return false;
        };
    };
    $hasProperty = function ($dict) {
        return $dict['hasProperty'];
    };
    $hasOwnPropertyImpl = function ($v)  use (&$PS__Foreign, &$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$PS__Data_Function_Uncurried, &$__foreign) {
        return function ($value)  use (&$PS__Foreign, &$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$PS__Data_Function_Uncurried, &$__foreign, &$v) {
            if ($PS__Foreign['isNull']($value)) {
                return false;
            };
            if ($PS__Foreign['isUndefined']($value)) {
                return false;
            };
            if ($PS__Data_HeytingAlgebra['disj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Eq['eq']($PS__Data_Eq['eqString'])($PS__Foreign['typeOf']($value))('object'))($PS__Data_Eq['eq']($PS__Data_Eq['eqString'])($PS__Foreign['typeOf']($value))('function'))) {
                return $PS__Data_Function_Uncurried['runFn2']($__foreign['unsafeHasOwnProperty'])($v)($value);
            };
            return false;
        };
    };
    $indexInt = $Index($PS__Foreign['ErrorAtIndex']['create'], $hasOwnPropertyImpl, $hasPropertyImpl, $PS__Data_Function['flip']($readIndex));
    $indexString = $Index($PS__Foreign['ErrorAtProperty']['create'], $hasOwnPropertyImpl, $hasPropertyImpl, $PS__Data_Function['flip']($readProp));
    $hasOwnProperty = function ($dict) {
        return $dict['hasOwnProperty'];
    };
    $errorAt = function ($dict) {
        return $dict['errorAt'];
    };
    return [
        'Index' => $Index,
        'Indexable' => $Indexable,
        'readProp' => $readProp,
        'readIndex' => $readIndex,
        'ix' => $ix,
        'index' => $index,
        'hasProperty' => $hasProperty,
        'hasOwnProperty' => $hasOwnProperty,
        'errorAt' => $errorAt,
        'indexString' => $indexString,
        'indexInt' => $indexInt,
        'indexableForeign' => $indexableForeign,
        'indexableExceptT' => $indexableExceptT
    ];
})();
