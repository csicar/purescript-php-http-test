<?php
require_once(dirname(__FILE__) . '/../Control.Monad.ST/index.php');
require_once(dirname(__FILE__) . '/../Foreign.Object/index.php');
require_once(dirname(__FILE__) . '/../Foreign.Object.ST/index.php');
$PS__Foreign_Object_ST_Unsafe = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    return [
        'unsafeFreeze' => $__foreign['unsafeFreeze']
    ];
})();
