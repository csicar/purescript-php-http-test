<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Biapplicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Biapply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad/index.php');
require_once(dirname(__FILE__) . '/../Control.Extend/index.php');
require_once(dirname(__FILE__) . '/../Control.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifoldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor/index.php');
require_once(dirname(__FILE__) . '/../Data.Bitraversable/index.php');
require_once(dirname(__FILE__) . '/../Data.BooleanAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.CommutativeRing/index.php');
require_once(dirname(__FILE__) . '/../Data.Distributive/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor.Invariant/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe.First/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
require_once(dirname(__FILE__) . '/../Type.Equality/index.php');
$PS__Data_Tuple = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Semiring, &$PS__Control_Semigroupoid, &$PS__Data_Ring, &$PS__Data_Monoid, &$PS__Data_Newtype, &$PS__Data_Maybe_First, &$PS__Data_Foldable, &$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_HeytingAlgebra, &$PS__Data_Functor, &$PS__Data_Functor_Invariant, &$PS__Control_Lazy, &$PS__Data_Function, &$PS__Data_Unit, &$PS__Data_Traversable, &$PS__Control_Extend, &$PS__Data_Ord, &$PS__Data_Ordering, &$PS__Data_Distributive, &$PS__Type_Equality, &$PS__Control_Comonad, &$PS__Data_CommutativeRing, &$PS__Data_Bounded, &$PS__Data_BooleanAlgebra, &$PS__Data_Bifunctor, &$PS__Data_Bifoldable, &$PS__Data_Bitraversable, &$PS__Control_Apply, &$PS__Control_Biapply, &$PS__Control_Biapplicative, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Control_Monad) {
    $Tuple = [
        'constructor' => function ($value0, $value1) {
            return [
                'type' => 'Tuple',
                'value0' => $value0,
                'value1' => $value1
            ];
        },
        'create' => function ($value0)  use (&$Tuple) {
            return function ($value1)  use (&$Tuple, &$value0) {
                return $Tuple['constructor']($value0, $value1);
            };
        }
    ];
    $uncurry = function ($f) {
        return function ($v)  use (&$f) {
            return $f($v['value0'])($v['value1']);
        };
    };
    $swap = function ($v)  use (&$Tuple) {
        return $Tuple['constructor']($v['value1'], $v['value0']);
    };
    $snd = function ($v) {
        return $v['value1'];
    };
    $showTuple = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return function ($dictShow1)  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$dictShow) {
            return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow, &$dictShow1) {
                return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Tuple ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v['value0']))($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])(' ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow1)($v['value1']))(')'))));
            });
        };
    };
    $semiringTuple = function ($dictSemiring)  use (&$PS__Data_Semiring, &$Tuple) {
        return function ($dictSemiring1)  use (&$PS__Data_Semiring, &$Tuple, &$dictSemiring) {
            return $PS__Data_Semiring['Semiring'](function ($v)  use (&$Tuple, &$PS__Data_Semiring, &$dictSemiring, &$dictSemiring1) {
                return function ($v1)  use (&$Tuple, &$PS__Data_Semiring, &$dictSemiring, &$v, &$dictSemiring1) {
                    return $Tuple['constructor']($PS__Data_Semiring['add']($dictSemiring)($v['value0'])($v1['value0']), $PS__Data_Semiring['add']($dictSemiring1)($v['value1'])($v1['value1']));
                };
            }, function ($v)  use (&$Tuple, &$PS__Data_Semiring, &$dictSemiring, &$dictSemiring1) {
                return function ($v1)  use (&$Tuple, &$PS__Data_Semiring, &$dictSemiring, &$v, &$dictSemiring1) {
                    return $Tuple['constructor']($PS__Data_Semiring['mul']($dictSemiring)($v['value0'])($v1['value0']), $PS__Data_Semiring['mul']($dictSemiring1)($v['value1'])($v1['value1']));
                };
            }, $Tuple['constructor']($PS__Data_Semiring['one']($dictSemiring), $PS__Data_Semiring['one']($dictSemiring1)), $Tuple['constructor']($PS__Data_Semiring['zero']($dictSemiring), $PS__Data_Semiring['zero']($dictSemiring1)));
        };
    };
    $semigroupoidTuple = $PS__Control_Semigroupoid['Semigroupoid'](function ($v)  use (&$Tuple) {
        return function ($v1)  use (&$Tuple, &$v) {
            return $Tuple['constructor']($v1['value0'], $v['value1']);
        };
    });
    $semigroupTuple = function ($dictSemigroup)  use (&$PS__Data_Semigroup, &$Tuple) {
        return function ($dictSemigroup1)  use (&$PS__Data_Semigroup, &$Tuple, &$dictSemigroup) {
            return $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$Tuple, &$PS__Data_Semigroup, &$dictSemigroup, &$dictSemigroup1) {
                return function ($v1)  use (&$Tuple, &$PS__Data_Semigroup, &$dictSemigroup, &$v, &$dictSemigroup1) {
                    return $Tuple['constructor']($PS__Data_Semigroup['append']($dictSemigroup)($v['value0'])($v1['value0']), $PS__Data_Semigroup['append']($dictSemigroup1)($v['value1'])($v1['value1']));
                };
            });
        };
    };
    $ringTuple = function ($dictRing)  use (&$PS__Data_Ring, &$semiringTuple, &$Tuple) {
        return function ($dictRing1)  use (&$PS__Data_Ring, &$semiringTuple, &$dictRing, &$Tuple) {
            return $PS__Data_Ring['Ring'](function ()  use (&$semiringTuple, &$dictRing, &$dictRing1) {
                return $semiringTuple($dictRing['Semiring0']())($dictRing1['Semiring0']());
            }, function ($v)  use (&$Tuple, &$PS__Data_Ring, &$dictRing, &$dictRing1) {
                return function ($v1)  use (&$Tuple, &$PS__Data_Ring, &$dictRing, &$v, &$dictRing1) {
                    return $Tuple['constructor']($PS__Data_Ring['sub']($dictRing)($v['value0'])($v1['value0']), $PS__Data_Ring['sub']($dictRing1)($v['value1'])($v1['value1']));
                };
            });
        };
    };
    $monoidTuple = function ($dictMonoid)  use (&$PS__Data_Monoid, &$semigroupTuple, &$Tuple) {
        return function ($dictMonoid1)  use (&$PS__Data_Monoid, &$semigroupTuple, &$dictMonoid, &$Tuple) {
            return $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupTuple, &$dictMonoid, &$dictMonoid1) {
                return $semigroupTuple($dictMonoid['Semigroup0']())($dictMonoid1['Semigroup0']());
            }, $Tuple['constructor']($PS__Data_Monoid['mempty']($dictMonoid), $PS__Data_Monoid['mempty']($dictMonoid1)));
        };
    };
    $lookup = function ($dictFoldable)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$PS__Data_Maybe_First, &$PS__Data_Foldable, &$PS__Data_Eq, &$PS__Data_Maybe) {
        return function ($dictEq)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$PS__Data_Maybe_First, &$PS__Data_Foldable, &$dictFoldable, &$PS__Data_Eq, &$PS__Data_Maybe) {
            return function ($a)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$PS__Data_Maybe_First, &$PS__Data_Foldable, &$dictFoldable, &$PS__Data_Eq, &$dictEq, &$PS__Data_Maybe) {
                return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Newtype['unwrap']($PS__Data_Maybe_First['newtypeFirst']))($PS__Data_Foldable['foldMap']($dictFoldable)($PS__Data_Maybe_First['monoidFirst'])(function ($v)  use (&$PS__Data_Eq, &$dictEq, &$a, &$PS__Data_Maybe) {
                    $__local_var__149 = $PS__Data_Eq['eq']($dictEq)($a)($v['value0']);
                    if ($__local_var__149) {
                        return $PS__Data_Maybe['Just']['constructor']($v['value1']);
                    };
                    return $PS__Data_Maybe['Nothing']();
                }));
            };
        };
    };
    $heytingAlgebraTuple = function ($dictHeytingAlgebra)  use (&$PS__Data_HeytingAlgebra, &$Tuple) {
        return function ($dictHeytingAlgebra1)  use (&$PS__Data_HeytingAlgebra, &$Tuple, &$dictHeytingAlgebra) {
            return $PS__Data_HeytingAlgebra['HeytingAlgebra'](function ($v)  use (&$Tuple, &$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$dictHeytingAlgebra1) {
                return function ($v1)  use (&$Tuple, &$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$v, &$dictHeytingAlgebra1) {
                    return $Tuple['constructor']($PS__Data_HeytingAlgebra['conj']($dictHeytingAlgebra)($v['value0'])($v1['value0']), $PS__Data_HeytingAlgebra['conj']($dictHeytingAlgebra1)($v['value1'])($v1['value1']));
                };
            }, function ($v)  use (&$Tuple, &$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$dictHeytingAlgebra1) {
                return function ($v1)  use (&$Tuple, &$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$v, &$dictHeytingAlgebra1) {
                    return $Tuple['constructor']($PS__Data_HeytingAlgebra['disj']($dictHeytingAlgebra)($v['value0'])($v1['value0']), $PS__Data_HeytingAlgebra['disj']($dictHeytingAlgebra1)($v['value1'])($v1['value1']));
                };
            }, $Tuple['constructor']($PS__Data_HeytingAlgebra['ff']($dictHeytingAlgebra), $PS__Data_HeytingAlgebra['ff']($dictHeytingAlgebra1)), function ($v)  use (&$Tuple, &$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$dictHeytingAlgebra1) {
                return function ($v1)  use (&$Tuple, &$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$v, &$dictHeytingAlgebra1) {
                    return $Tuple['constructor']($PS__Data_HeytingAlgebra['implies']($dictHeytingAlgebra)($v['value0'])($v1['value0']), $PS__Data_HeytingAlgebra['implies']($dictHeytingAlgebra1)($v['value1'])($v1['value1']));
                };
            }, function ($v)  use (&$Tuple, &$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$dictHeytingAlgebra1) {
                return $Tuple['constructor']($PS__Data_HeytingAlgebra['not']($dictHeytingAlgebra)($v['value0']), $PS__Data_HeytingAlgebra['not']($dictHeytingAlgebra1)($v['value1']));
            }, $Tuple['constructor']($PS__Data_HeytingAlgebra['tt']($dictHeytingAlgebra), $PS__Data_HeytingAlgebra['tt']($dictHeytingAlgebra1)));
        };
    };
    $functorTuple = $PS__Data_Functor['Functor'](function ($f)  use (&$Tuple) {
        return function ($m)  use (&$Tuple, &$f) {
            return $Tuple['constructor']($m['value0'], $f($m['value1']));
        };
    });
    $invariantTuple = $PS__Data_Functor_Invariant['Invariant']($PS__Data_Functor_Invariant['imapF']($functorTuple));
    $fst = function ($v) {
        return $v['value0'];
    };
    $lazyTuple = function ($dictLazy)  use (&$PS__Control_Lazy, &$Tuple, &$PS__Data_Function, &$fst, &$PS__Data_Unit, &$snd) {
        return function ($dictLazy1)  use (&$PS__Control_Lazy, &$Tuple, &$PS__Data_Function, &$dictLazy, &$fst, &$PS__Data_Unit, &$snd) {
            return $PS__Control_Lazy['Lazy'](function ($f)  use (&$Tuple, &$PS__Data_Function, &$PS__Control_Lazy, &$dictLazy, &$fst, &$PS__Data_Unit, &$dictLazy1, &$snd) {
                return $Tuple['constructor']($PS__Data_Function['apply']($PS__Control_Lazy['defer']($dictLazy))(function ($v)  use (&$fst, &$f, &$PS__Data_Unit) {
                    return $fst($f($PS__Data_Unit['unit']));
                }), $PS__Data_Function['apply']($PS__Control_Lazy['defer']($dictLazy1))(function ($v)  use (&$snd, &$f, &$PS__Data_Unit) {
                    return $snd($f($PS__Data_Unit['unit']));
                }));
            });
        };
    };
    $foldableTuple = $PS__Data_Foldable['Foldable'](function ($dictMonoid) {
        return function ($f) {
            return function ($v)  use (&$f) {
                return $f($v['value1']);
            };
        };
    }, function ($f) {
        return function ($z)  use (&$f) {
            return function ($v)  use (&$f, &$z) {
                return $f($z)($v['value1']);
            };
        };
    }, function ($f) {
        return function ($z)  use (&$f) {
            return function ($v)  use (&$f, &$z) {
                return $f($v['value1'])($z);
            };
        };
    });
    $traversableTuple = $PS__Data_Traversable['Traversable'](function ()  use (&$foldableTuple) {
        return $foldableTuple;
    }, function ()  use (&$functorTuple) {
        return $functorTuple;
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$Tuple) {
        return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$Tuple) {
            return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($Tuple['create']($v['value0']))($v['value1']);
        };
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$Tuple) {
        return function ($f)  use (&$PS__Data_Functor, &$dictApplicative, &$Tuple) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$Tuple, &$f) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($Tuple['create']($v['value0']))($f($v['value1']));
            };
        };
    });
    $extendTuple = $PS__Control_Extend['Extend'](function ()  use (&$functorTuple) {
        return $functorTuple;
    }, function ($f)  use (&$Tuple) {
        return function ($v)  use (&$Tuple, &$f) {
            return $Tuple['constructor']($v['value0'], $f($v));
        };
    });
    $eqTuple = function ($dictEq)  use (&$PS__Data_Eq, &$PS__Data_HeytingAlgebra) {
        return function ($dictEq1)  use (&$PS__Data_Eq, &$PS__Data_HeytingAlgebra, &$dictEq) {
            return $PS__Data_Eq['Eq'](function ($x)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$dictEq, &$dictEq1) {
                return function ($y)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$dictEq, &$x, &$dictEq1) {
                    return $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Eq['eq']($dictEq)($x['value0'])($y['value0']))($PS__Data_Eq['eq']($dictEq1)($x['value1'])($y['value1']));
                };
            });
        };
    };
    $ordTuple = function ($dictOrd)  use (&$PS__Data_Ord, &$eqTuple, &$PS__Data_Ordering) {
        return function ($dictOrd1)  use (&$PS__Data_Ord, &$eqTuple, &$dictOrd, &$PS__Data_Ordering) {
            return $PS__Data_Ord['Ord'](function ()  use (&$eqTuple, &$dictOrd, &$dictOrd1) {
                return $eqTuple($dictOrd['Eq0']())($dictOrd1['Eq0']());
            }, function ($x)  use (&$PS__Data_Ord, &$dictOrd, &$PS__Data_Ordering, &$dictOrd1) {
                return function ($y)  use (&$PS__Data_Ord, &$dictOrd, &$x, &$PS__Data_Ordering, &$dictOrd1) {
                    $v = $PS__Data_Ord['compare']($dictOrd)($x['value0'])($y['value0']);
                    if ($v['type'] === 'LT') {
                        return $PS__Data_Ordering['LT']();
                    };
                    if ($v['type'] === 'GT') {
                        return $PS__Data_Ordering['GT']();
                    };
                    return $PS__Data_Ord['compare']($dictOrd1)($x['value1'])($y['value1']);
                };
            });
        };
    };
    $eq1Tuple = function ($dictEq)  use (&$PS__Data_Eq, &$eqTuple) {
        return $PS__Data_Eq['Eq1'](function ($dictEq1)  use (&$PS__Data_Eq, &$eqTuple, &$dictEq) {
            return $PS__Data_Eq['eq']($eqTuple($dictEq)($dictEq1));
        });
    };
    $ord1Tuple = function ($dictOrd)  use (&$PS__Data_Ord, &$eq1Tuple, &$ordTuple) {
        return $PS__Data_Ord['Ord1'](function ()  use (&$eq1Tuple, &$dictOrd) {
            return $eq1Tuple($dictOrd['Eq0']());
        }, function ($dictOrd1)  use (&$PS__Data_Ord, &$ordTuple, &$dictOrd) {
            return $PS__Data_Ord['compare']($ordTuple($dictOrd)($dictOrd1));
        });
    };
    $distributiveTuple = function ($dictTypeEquals)  use (&$PS__Data_Distributive, &$functorTuple, &$distributiveTuple, &$PS__Control_Semigroupoid, &$Tuple, &$PS__Type_Equality, &$PS__Data_Unit, &$PS__Data_Functor, &$snd) {
        return $PS__Data_Distributive['Distributive'](function ()  use (&$functorTuple) {
            return $functorTuple;
        }, function ($dictFunctor)  use (&$PS__Data_Distributive, &$distributiveTuple, &$dictTypeEquals) {
            return $PS__Data_Distributive['collectDefault']($distributiveTuple($dictTypeEquals))($dictFunctor);
        }, function ($dictFunctor)  use (&$PS__Control_Semigroupoid, &$Tuple, &$PS__Type_Equality, &$dictTypeEquals, &$PS__Data_Unit, &$PS__Data_Functor, &$snd) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($Tuple['create']($PS__Type_Equality['from']($dictTypeEquals)($PS__Data_Unit['unit'])))($PS__Data_Functor['map']($dictFunctor)($snd));
        });
    };
    $curry = function ($f)  use (&$Tuple) {
        return function ($a)  use (&$f, &$Tuple) {
            return function ($b)  use (&$f, &$Tuple, &$a) {
                return $f($Tuple['constructor']($a, $b));
            };
        };
    };
    $comonadTuple = $PS__Control_Comonad['Comonad'](function ()  use (&$extendTuple) {
        return $extendTuple;
    }, $snd);
    $commutativeRingTuple = function ($dictCommutativeRing)  use (&$PS__Data_CommutativeRing, &$ringTuple) {
        return function ($dictCommutativeRing1)  use (&$PS__Data_CommutativeRing, &$ringTuple, &$dictCommutativeRing) {
            return $PS__Data_CommutativeRing['CommutativeRing'](function ()  use (&$ringTuple, &$dictCommutativeRing, &$dictCommutativeRing1) {
                return $ringTuple($dictCommutativeRing['Ring0']())($dictCommutativeRing1['Ring0']());
            });
        };
    };
    $boundedTuple = function ($dictBounded)  use (&$PS__Data_Bounded, &$ordTuple, &$Tuple) {
        return function ($dictBounded1)  use (&$PS__Data_Bounded, &$ordTuple, &$dictBounded, &$Tuple) {
            return $PS__Data_Bounded['Bounded'](function ()  use (&$ordTuple, &$dictBounded, &$dictBounded1) {
                return $ordTuple($dictBounded['Ord0']())($dictBounded1['Ord0']());
            }, $Tuple['constructor']($PS__Data_Bounded['bottom']($dictBounded), $PS__Data_Bounded['bottom']($dictBounded1)), $Tuple['constructor']($PS__Data_Bounded['top']($dictBounded), $PS__Data_Bounded['top']($dictBounded1)));
        };
    };
    $booleanAlgebraTuple = function ($dictBooleanAlgebra)  use (&$PS__Data_BooleanAlgebra, &$heytingAlgebraTuple) {
        return function ($dictBooleanAlgebra1)  use (&$PS__Data_BooleanAlgebra, &$heytingAlgebraTuple, &$dictBooleanAlgebra) {
            return $PS__Data_BooleanAlgebra['BooleanAlgebra'](function ()  use (&$heytingAlgebraTuple, &$dictBooleanAlgebra, &$dictBooleanAlgebra1) {
                return $heytingAlgebraTuple($dictBooleanAlgebra['HeytingAlgebra0']())($dictBooleanAlgebra1['HeytingAlgebra0']());
            });
        };
    };
    $bifunctorTuple = $PS__Data_Bifunctor['Bifunctor'](function ($f)  use (&$Tuple) {
        return function ($g)  use (&$Tuple, &$f) {
            return function ($v)  use (&$Tuple, &$f, &$g) {
                return $Tuple['constructor']($f($v['value0']), $g($v['value1']));
            };
        };
    });
    $bifoldableTuple = $PS__Data_Bifoldable['Bifoldable'](function ($dictMonoid)  use (&$PS__Data_Semigroup) {
        return function ($f)  use (&$PS__Data_Semigroup, &$dictMonoid) {
            return function ($g)  use (&$PS__Data_Semigroup, &$dictMonoid, &$f) {
                return function ($v)  use (&$PS__Data_Semigroup, &$dictMonoid, &$f, &$g) {
                    return $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($f($v['value0']))($g($v['value1']));
                };
            };
        };
    }, function ($f) {
        return function ($g)  use (&$f) {
            return function ($z)  use (&$g, &$f) {
                return function ($v)  use (&$g, &$f, &$z) {
                    return $g($f($z)($v['value0']))($v['value1']);
                };
            };
        };
    }, function ($f) {
        return function ($g)  use (&$f) {
            return function ($z)  use (&$f, &$g) {
                return function ($v)  use (&$f, &$g, &$z) {
                    return $f($v['value0'])($g($v['value1'])($z));
                };
            };
        };
    });
    $bitraversableTuple = $PS__Data_Bitraversable['Bitraversable'](function ()  use (&$bifoldableTuple) {
        return $bifoldableTuple;
    }, function ()  use (&$bifunctorTuple) {
        return $bifunctorTuple;
    }, function ($dictApplicative)  use (&$PS__Control_Apply, &$PS__Data_Functor, &$Tuple) {
        return function ($v)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$Tuple) {
            return $PS__Control_Apply['apply']($dictApplicative['Apply0']())($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($Tuple['create'])($v['value0']))($v['value1']);
        };
    }, function ($dictApplicative)  use (&$PS__Control_Apply, &$PS__Data_Functor, &$Tuple) {
        return function ($f)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$Tuple) {
            return function ($g)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$Tuple, &$f) {
                return function ($v)  use (&$PS__Control_Apply, &$dictApplicative, &$PS__Data_Functor, &$Tuple, &$f, &$g) {
                    return $PS__Control_Apply['apply']($dictApplicative['Apply0']())($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($Tuple['create'])($f($v['value0'])))($g($v['value1']));
                };
            };
        };
    });
    $biapplyTuple = $PS__Control_Biapply['Biapply'](function ()  use (&$bifunctorTuple) {
        return $bifunctorTuple;
    }, function ($v)  use (&$Tuple) {
        return function ($v1)  use (&$Tuple, &$v) {
            return $Tuple['constructor']($v['value0']($v1['value0']), $v['value1']($v1['value1']));
        };
    });
    $biapplicativeTuple = $PS__Control_Biapplicative['Biapplicative'](function ()  use (&$biapplyTuple) {
        return $biapplyTuple;
    }, $Tuple['create']);
    $applyTuple = function ($dictSemigroup)  use (&$PS__Control_Apply, &$functorTuple, &$Tuple, &$PS__Data_Semigroup) {
        return $PS__Control_Apply['Apply'](function ()  use (&$functorTuple) {
            return $functorTuple;
        }, function ($v)  use (&$Tuple, &$PS__Data_Semigroup, &$dictSemigroup) {
            return function ($v1)  use (&$Tuple, &$PS__Data_Semigroup, &$dictSemigroup, &$v) {
                return $Tuple['constructor']($PS__Data_Semigroup['append']($dictSemigroup)($v['value0'])($v1['value0']), $v['value1']($v1['value1']));
            };
        });
    };
    $bindTuple = function ($dictSemigroup)  use (&$PS__Control_Bind, &$applyTuple, &$Tuple, &$PS__Data_Semigroup) {
        return $PS__Control_Bind['Bind'](function ()  use (&$applyTuple, &$dictSemigroup) {
            return $applyTuple($dictSemigroup);
        }, function ($v)  use (&$Tuple, &$PS__Data_Semigroup, &$dictSemigroup) {
            return function ($f)  use (&$v, &$Tuple, &$PS__Data_Semigroup, &$dictSemigroup) {
                $v1 = $f($v['value1']);
                return $Tuple['constructor']($PS__Data_Semigroup['append']($dictSemigroup)($v['value0'])($v1['value0']), $v1['value1']);
            };
        });
    };
    $applicativeTuple = function ($dictMonoid)  use (&$PS__Control_Applicative, &$applyTuple, &$Tuple, &$PS__Data_Monoid) {
        return $PS__Control_Applicative['Applicative'](function ()  use (&$applyTuple, &$dictMonoid) {
            return $applyTuple($dictMonoid['Semigroup0']());
        }, $Tuple['create']($PS__Data_Monoid['mempty']($dictMonoid)));
    };
    $monadTuple = function ($dictMonoid)  use (&$PS__Control_Monad, &$applicativeTuple, &$bindTuple) {
        return $PS__Control_Monad['Monad'](function ()  use (&$applicativeTuple, &$dictMonoid) {
            return $applicativeTuple($dictMonoid);
        }, function ()  use (&$bindTuple, &$dictMonoid) {
            return $bindTuple($dictMonoid['Semigroup0']());
        });
    };
    return [
        'Tuple' => $Tuple,
        'fst' => $fst,
        'snd' => $snd,
        'curry' => $curry,
        'uncurry' => $uncurry,
        'swap' => $swap,
        'lookup' => $lookup,
        'showTuple' => $showTuple,
        'eqTuple' => $eqTuple,
        'eq1Tuple' => $eq1Tuple,
        'ordTuple' => $ordTuple,
        'ord1Tuple' => $ord1Tuple,
        'boundedTuple' => $boundedTuple,
        'semigroupoidTuple' => $semigroupoidTuple,
        'semigroupTuple' => $semigroupTuple,
        'monoidTuple' => $monoidTuple,
        'semiringTuple' => $semiringTuple,
        'ringTuple' => $ringTuple,
        'commutativeRingTuple' => $commutativeRingTuple,
        'heytingAlgebraTuple' => $heytingAlgebraTuple,
        'booleanAlgebraTuple' => $booleanAlgebraTuple,
        'functorTuple' => $functorTuple,
        'invariantTuple' => $invariantTuple,
        'bifunctorTuple' => $bifunctorTuple,
        'applyTuple' => $applyTuple,
        'biapplyTuple' => $biapplyTuple,
        'applicativeTuple' => $applicativeTuple,
        'biapplicativeTuple' => $biapplicativeTuple,
        'bindTuple' => $bindTuple,
        'monadTuple' => $monadTuple,
        'extendTuple' => $extendTuple,
        'comonadTuple' => $comonadTuple,
        'lazyTuple' => $lazyTuple,
        'foldableTuple' => $foldableTuple,
        'bifoldableTuple' => $bifoldableTuple,
        'traversableTuple' => $traversableTuple,
        'bitraversableTuple' => $bitraversableTuple,
        'distributiveTuple' => $distributiveTuple
    ];
})();
