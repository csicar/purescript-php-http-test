<?php

$exports['undefined'] = NULL;

$exports['setEncodingImpl'] = function ($s) {
  return function ($enc) use (&$s) {
    return function () {
      s.setEncoding(enc);
    };
  };
};

$exports['readChunkImpl'] = function ($Left) {
  return function ($Right) use (&$Left) {
    return function ($chunk) use (&$Left, &$Right) {
      if (is_string($chunk)) {
        return $Left($chunk);
      } else if (is_array($chunk)) {
        return $Right($chunk);
      } else {
        throw new Exception(
          "Node.Stream.readChunkImpl: Unrecognised " +
          "chunk type; expected String or Buffer, got: " +
          chunk);
      }
    };
  };
};

$exports['onDataEitherImpl'] = function ($readChunk) {
  return function ($r) use (&$readChunk) {
    return function ($f) use (&$readChunk, &$r) {
      return function () use (&$readChunk, &$r, &$f) {
        $r->on("data", function ($data) use (&$f, &$readChunk) {
          $f($readChunk($data))();
        });
      };
    };
  };
};

$exports['onEnd'] = function ($s) {
  return function ($f) use (&$s) {
    return function ()  use (&$s, &$f) {
      $s->on("end", $f);
    };
  };
};

$exports['onFinish'] = function ($s) {
  return function ($f) {
    return function () {
      $s->on("end", $f);
    };
  };
};

$exports['onReadable'] = function ($s) {
  return function ($f) use (&$s) {
    return function () use (&$s, &$f) {
      //TODO correct! event of "readable"
      $s->on("start", $f);
    };
  };
};

$exports['onError'] = function ($s) {
  return function ($f) use (&$s) {
    return function () use (&$s, &$f) {
      $s->on("error", function ($e) use (&$f) {
        $f($e)();
      });
    };
  };
};

$exports['onClose'] = function ($s) {
  return function ($f) use (&$s) {
    return function () use (&$s, &$f) {
      $s->on("close", $f);
    };
  };
};

$exports['resume'] = function ($s) {
  return function () use (&$s) {
    $s->resume();
  };
};

$exports['pause'] = function ($s) {
  return function () use (&$s) {
    $s->pause();
  };
};

$exports['isPaused'] = function ($s) {
  return function () use (&$s) {
    //TODO find implementation!
    return $s->isPaused();
  };
};

$exports['pipe'] = function ($r) {
  return function ($w) use (&$r) {
    return function () use (&$r, &$w) {
      return $r->pipe($w);
    };
  };
};

$exports['unpipe'] = function ($r) {
  return function ($w) use (&$r) {
    return function () use (&$r, &$w) {
      //TODO find implementation!
      return $r->unpipe($w);
    };
  };
};

$exports['unpipeAll'] = function ($r) {
  return function () use (&$r) {
    return $r->unpipe();
  };
};

$exports['readImpl'] = function ($readChunk) {
  return function ($Nothing) use (&$readChunk) {
    return function ($Just) use (&$readChunk, &$Nothing) {
      return function ($r) use (&$readChunk, &$Nothing, &$Just) {
        return function ($s) use (&$readChunk, &$Nothing, &$Just, &$r) {
          return function () use (&$readChunk, &$Nothing, &$Just, &$r, &$s) {
            //TODO find correct implementation!
            $v = $r->read($s);
            if ($v === NULL) {
              return $Nothing;
            } else {
              return $Just($readChunk($v));
            }
          };
        };
      };
    };
  };
};

$exports['write'] = function ($w) {
  return function ($chunk) use (&$w) {
    return function ($done) use (&$w, &$chunk) {
      return function () use (&$w, &$chunk, &$done) {
        //TODO correct
        $res = $w->write($chunk);
        $done();
        return true;
      };
    };
  };
};

$exports['writeStringImpl'] = function ($w) {
  return function ($enc) use (&$w) {
    return function ($s) use (&$w, &$enc) {
      return function ($done) use (&$w, &$enc, &$s) {
        return function () use (&$w, &$enc, &$s, &$done) {
          //TODO correct
          $res = $w->write($chunk);
          $done();
          return true;
        };
      };
    };
  };
};

$exports['cork'] = function ($w) {
  return function () use (&$w) {
    return $w->cork();
  };
};

$exports['uncork'] = function ($w) {
  return function () use (&$w) {
    return $w->uncork();
  };
};

$exports['setDefaultEncodingImpl'] = function ($w) {
  return function ($enc) use (&$w) {
    return function () use (&$w, &$enc) {
      //TODO
      // w.setDefaultEncoding(enc);
    };
  };
};

$exports['end'] = function ($w) {
  return function ($done) use (&$w) {
    return function () use (&$w, &$done) {
      $w->end();
      $done();
    };
  };
};

$exports['destroy'] = function ($strm) {
  return function () use (&$strm) {
    $strm->end();
  };
};

$exports['destroyWithError'] = function ($strm) {
  return function ($e) use (&$strm) {
    return function () use (&$strm, $e) {
      $strm->destroy();
      throw $e;
    };
  };
};
