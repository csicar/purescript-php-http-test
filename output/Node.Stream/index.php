<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Data.Either/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Effect/index.php');
require_once(dirname(__FILE__) . '/../Effect.Exception/index.php');
require_once(dirname(__FILE__) . '/../Node.Buffer/index.php');
require_once(dirname(__FILE__) . '/../Node.Encoding/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Node_Stream = (function ()  use (&$PS__Data_Show, &$PS__Node_Encoding, &$PS__Data_Either, &$PS__Data_Maybe, &$PS__Control_Bind, &$PS__Effect, &$PS__Control_Applicative, &$PS__Effect_Exception, &$PS__Data_Functor, &$PS__Node_Buffer) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $writeString = function ($w)  use (&$__foreign, &$PS__Data_Show, &$PS__Node_Encoding) {
        return function ($enc)  use (&$__foreign, &$w, &$PS__Data_Show, &$PS__Node_Encoding) {
            return $__foreign['writeStringImpl']($w)($PS__Data_Show['show']($PS__Node_Encoding['showEncoding'])($enc));
        };
    };
    $setEncoding = function ($r)  use (&$__foreign, &$PS__Data_Show, &$PS__Node_Encoding) {
        return function ($enc)  use (&$__foreign, &$r, &$PS__Data_Show, &$PS__Node_Encoding) {
            return $__foreign['setEncodingImpl']($r)($PS__Data_Show['show']($PS__Node_Encoding['showEncoding'])($enc));
        };
    };
    $setDefaultEncoding = function ($r)  use (&$__foreign, &$PS__Data_Show, &$PS__Node_Encoding) {
        return function ($enc)  use (&$__foreign, &$r, &$PS__Data_Show, &$PS__Node_Encoding) {
            return $__foreign['setDefaultEncodingImpl']($r)($PS__Data_Show['show']($PS__Node_Encoding['showEncoding'])($enc));
        };
    };
    $readChunk = $__foreign['readChunkImpl']($PS__Data_Either['Left']['create'])($PS__Data_Either['Right']['create']);
    $readEither = function ($r)  use (&$__foreign, &$readChunk, &$PS__Data_Maybe) {
        return function ($size)  use (&$__foreign, &$readChunk, &$PS__Data_Maybe, &$r) {
            return $__foreign['readImpl']($readChunk)($PS__Data_Maybe['Nothing']())($PS__Data_Maybe['Just']['create'])($r)($PS__Data_Maybe['fromMaybe']($__foreign['undefined'])($size));
        };
    };
    $readString = function ($r)  use (&$PS__Control_Bind, &$PS__Effect, &$readEither, &$PS__Control_Applicative, &$PS__Data_Maybe, &$PS__Effect_Exception, &$PS__Data_Functor, &$PS__Node_Buffer) {
        return function ($size)  use (&$PS__Control_Bind, &$PS__Effect, &$readEither, &$r, &$PS__Control_Applicative, &$PS__Data_Maybe, &$PS__Effect_Exception, &$PS__Data_Functor, &$PS__Node_Buffer) {
            return function ($enc)  use (&$PS__Control_Bind, &$PS__Effect, &$readEither, &$r, &$size, &$PS__Control_Applicative, &$PS__Data_Maybe, &$PS__Effect_Exception, &$PS__Data_Functor, &$PS__Node_Buffer) {
                return $PS__Control_Bind['bind']($PS__Effect['bindEffect'])($readEither($r)($size))(function ($v)  use (&$PS__Control_Applicative, &$PS__Effect, &$PS__Data_Maybe, &$PS__Effect_Exception, &$PS__Data_Functor, &$PS__Node_Buffer, &$enc) {
                    if ($v['type'] === 'Nothing') {
                        return $PS__Control_Applicative['pure']($PS__Effect['applicativeEffect'])($PS__Data_Maybe['Nothing']());
                    };
                    if ($v['type'] === 'Just' && $v['value0']['type'] === 'Left') {
                        return $PS__Effect_Exception['throw']('Stream encoding should not be set');
                    };
                    if ($v['type'] === 'Just' && $v['value0']['type'] === 'Right') {
                        return $PS__Data_Functor['map']($PS__Effect['functorEffect'])($PS__Data_Maybe['Just']['create'])($PS__Node_Buffer['toString']($enc)($v['value0']['value0']));
                    };
                    throw new \Exception('Failed pattern match at Node.Stream line 120, column 3 - line 123, column 60: ' . var_dump([ $v['constructor']['name'] ]));
                });
            };
        };
    };
    $read = function ($r)  use (&$PS__Control_Bind, &$PS__Effect, &$readEither, &$PS__Control_Applicative, &$PS__Data_Maybe, &$PS__Effect_Exception) {
        return function ($size)  use (&$PS__Control_Bind, &$PS__Effect, &$readEither, &$r, &$PS__Control_Applicative, &$PS__Data_Maybe, &$PS__Effect_Exception) {
            return $PS__Control_Bind['bind']($PS__Effect['bindEffect'])($readEither($r)($size))(function ($v)  use (&$PS__Control_Applicative, &$PS__Effect, &$PS__Data_Maybe, &$PS__Effect_Exception) {
                if ($v['type'] === 'Nothing') {
                    return $PS__Control_Applicative['pure']($PS__Effect['applicativeEffect'])($PS__Data_Maybe['Nothing']());
                };
                if ($v['type'] === 'Just' && $v['value0']['type'] === 'Left') {
                    return $PS__Effect_Exception['throw']('Stream encoding should not be set');
                };
                if ($v['type'] === 'Just' && $v['value0']['type'] === 'Right') {
                    return $PS__Control_Applicative['pure']($PS__Effect['applicativeEffect'])($PS__Data_Maybe['Just']['constructor']($v['value0']['value0']));
                };
                throw new \Exception('Failed pattern match at Node.Stream line 107, column 3 - line 110, column 36: ' . var_dump([ $v['constructor']['name'] ]));
            });
        };
    };
    $onDataEither = function ($r)  use (&$__foreign, &$readChunk) {
        return function ($cb)  use (&$__foreign, &$readChunk, &$r) {
            return $__foreign['onDataEitherImpl']($readChunk)($r)($cb);
        };
    };
    $onData = function ($r)  use (&$PS__Effect_Exception, &$PS__Control_Applicative, &$PS__Effect, &$onDataEither, &$PS__Control_Bind) {
        return function ($cb)  use (&$PS__Effect_Exception, &$PS__Control_Applicative, &$PS__Effect, &$onDataEither, &$r, &$PS__Control_Bind) {
            $fromEither = function ($x)  use (&$PS__Effect_Exception, &$PS__Control_Applicative, &$PS__Effect) {
                if ($x['type'] === 'Left') {
                    return $PS__Effect_Exception['throw']('Stream encoding should not be set');
                };
                if ($x['type'] === 'Right') {
                    return $PS__Control_Applicative['pure']($PS__Effect['applicativeEffect'])($x['value0']);
                };
                throw new \Exception('Failed pattern match at Node.Stream line 94, column 5 - line 98, column 17: ' . var_dump([ $x['constructor']['name'] ]));
            };
            return $onDataEither($r)($PS__Control_Bind['composeKleisliFlipped']($PS__Effect['bindEffect'])($cb)($fromEither));
        };
    };
    $onDataString = function ($r)  use (&$onData, &$PS__Control_Bind, &$PS__Effect, &$PS__Node_Buffer) {
        return function ($enc)  use (&$onData, &$r, &$PS__Control_Bind, &$PS__Effect, &$PS__Node_Buffer) {
            return function ($cb)  use (&$onData, &$r, &$PS__Control_Bind, &$PS__Effect, &$PS__Node_Buffer, &$enc) {
                return $onData($r)($PS__Control_Bind['composeKleisliFlipped']($PS__Effect['bindEffect'])($cb)($PS__Node_Buffer['toString']($enc)));
            };
        };
    };
    return [
        'onData' => $onData,
        'onDataString' => $onDataString,
        'onDataEither' => $onDataEither,
        'setEncoding' => $setEncoding,
        'read' => $read,
        'readString' => $readString,
        'readEither' => $readEither,
        'writeString' => $writeString,
        'setDefaultEncoding' => $setDefaultEncoding,
        'onReadable' => $__foreign['onReadable'],
        'onEnd' => $__foreign['onEnd'],
        'onFinish' => $__foreign['onFinish'],
        'onClose' => $__foreign['onClose'],
        'onError' => $__foreign['onError'],
        'resume' => $__foreign['resume'],
        'pause' => $__foreign['pause'],
        'isPaused' => $__foreign['isPaused'],
        'pipe' => $__foreign['pipe'],
        'unpipe' => $__foreign['unpipe'],
        'unpipeAll' => $__foreign['unpipeAll'],
        'write' => $__foreign['write'],
        'cork' => $__foreign['cork'],
        'uncork' => $__foreign['uncork'],
        'end' => $__foreign['end'],
        'destroy' => $__foreign['destroy']
    ];
})();
