<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor.Contravariant/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Op/index.php');
require_once(dirname(__FILE__) . '/../Data.Options/index.php');
require_once(dirname(__FILE__) . '/../Effect/index.php');
require_once(dirname(__FILE__) . '/../Foreign/index.php');
require_once(dirname(__FILE__) . '/../Foreign.Object/index.php');
require_once(dirname(__FILE__) . '/../Node.Stream/index.php');
require_once(dirname(__FILE__) . '/../Node.URL/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
require_once(dirname(__FILE__) . '/../Unsafe.Coerce/index.php');
$PS__Node_HTTP_Client = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Unsafe_Coerce, &$PS__Foreign, &$PS__Node_URL, &$PS__Data_Options, &$PS__Data_Function, &$PS__Foreign_Object, &$PS__Data_Functor_Contravariant, &$PS__Data_Op) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $RequestHeaders = function ($x) {
        return $x;
    };
    $IPV4 = function () {
        return [
            'type' => 'IPV4'
        ];
    };
    $IPV6 = function () {
        return [
            'type' => 'IPV6'
        ];
    };
    $statusMessage = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])(function ($v) {
        return $v['statusMessage'];
    })($PS__Unsafe_Coerce['unsafeCoerce']);
    $statusCode = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])(function ($v) {
        return $v['statusCode'];
    })($PS__Unsafe_Coerce['unsafeCoerce']);
    $responseAsStream = $PS__Unsafe_Coerce['unsafeCoerce'];
    $requestFromURI = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($__foreign['requestImpl'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Foreign['unsafeToForeign'])($PS__Node_URL['parse']));
    $requestAsStream = $PS__Unsafe_Coerce['unsafeCoerce'];
    $request = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($__foreign['requestImpl'])($PS__Data_Options['options']);
    $rejectUnauthorized = $PS__Data_Options['opt']('rejectUnauthorized');
    $protocol = $PS__Data_Options['opt']('protocol');
    $port = $PS__Data_Options['opt']('port');
    $path = $PS__Data_Options['opt']('path');
    $method = $PS__Data_Options['opt']('method');
    $key = $PS__Data_Options['opt']('key');
    $httpVersion = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])(function ($v) {
        return $v['httpVersion'];
    })($PS__Unsafe_Coerce['unsafeCoerce']);
    $hostname = $PS__Data_Options['opt']('hostname');
    $headers__prime = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])(function ($v) {
        return $v['headers'];
    })($PS__Unsafe_Coerce['unsafeCoerce']);
    $responseCookies = function ($res)  use (&$PS__Data_Function, &$PS__Foreign_Object, &$headers__prime) {
        return $PS__Data_Function['apply']($PS__Foreign_Object['lookup']('set-cookie'))($headers__prime($res));
    };
    $responseHeaders = function ($res)  use (&$PS__Data_Function, &$PS__Foreign_Object, &$headers__prime) {
        return $PS__Data_Function['apply']($PS__Foreign_Object['delete']('set-cookie'))($headers__prime($res));
    };
    $headers = $PS__Data_Options['opt']('headers');
    $familyToOption = function ($v) {
        if ($v['type'] === 'IPV4') {
            return 4;
        };
        if ($v['type'] === 'IPV6') {
            return 6;
        };
        throw new \Exception('Failed pattern match at Node.HTTP.Client line 105, column 1 - line 105, column 39: ' . var_dump([ $v['constructor']['name'] ]));
    };
    $family = $PS__Data_Functor_Contravariant['cmap']($PS__Data_Op['contravariantOp'])($familyToOption)($PS__Data_Options['opt']('family'));
    $cert = $PS__Data_Options['opt']('cert');
    $auth = $PS__Data_Options['opt']('auth');
    return [
        'RequestHeaders' => $RequestHeaders,
        'IPV4' => $IPV4,
        'IPV6' => $IPV6,
        'protocol' => $protocol,
        'hostname' => $hostname,
        'port' => $port,
        'method' => $method,
        'path' => $path,
        'headers' => $headers,
        'auth' => $auth,
        'key' => $key,
        'cert' => $cert,
        'rejectUnauthorized' => $rejectUnauthorized,
        'family' => $family,
        'request' => $request,
        'requestFromURI' => $requestFromURI,
        'requestAsStream' => $requestAsStream,
        'responseAsStream' => $responseAsStream,
        'httpVersion' => $httpVersion,
        'responseHeaders' => $responseHeaders,
        'responseCookies' => $responseCookies,
        'statusCode' => $statusCode,
        'statusMessage' => $statusMessage,
        'setTimeout' => $__foreign['setTimeout']
    ];
})();
