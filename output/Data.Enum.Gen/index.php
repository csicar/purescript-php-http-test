<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Gen/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Enum/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable1/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Enum_Gen = (function ()  use (&$PS__Data_Enum, &$PS__Data_Bounded, &$PS__Data_Unfoldable1, &$PS__Control_Monad_Gen, &$PS__Data_NonEmpty, &$PS__Data_Foldable, &$PS__Control_Applicative) {
    $genBoundedEnum = function ($dictMonadGen)  use (&$PS__Data_Enum, &$PS__Data_Bounded, &$PS__Data_Unfoldable1, &$PS__Control_Monad_Gen, &$PS__Data_NonEmpty, &$PS__Data_Foldable, &$PS__Control_Applicative) {
        return function ($dictBoundedEnum)  use (&$PS__Data_Enum, &$PS__Data_Bounded, &$PS__Data_Unfoldable1, &$PS__Control_Monad_Gen, &$dictMonadGen, &$PS__Data_NonEmpty, &$PS__Data_Foldable, &$PS__Control_Applicative) {
            $v = $PS__Data_Enum['succ']($dictBoundedEnum['Enum1']())($PS__Data_Bounded['bottom']($dictBoundedEnum['Bounded0']()));
            if ($v['type'] === 'Just') {
                $possibilities = $PS__Data_Enum['enumFromTo']($dictBoundedEnum['Enum1']())($PS__Data_Unfoldable1['unfoldable1Array'])($v['value0'])($PS__Data_Bounded['top']($dictBoundedEnum['Bounded0']()));
                return $PS__Control_Monad_Gen['elements']($dictMonadGen)($PS__Data_NonEmpty['foldable1NonEmpty']($PS__Data_Foldable['foldableArray']))($PS__Data_NonEmpty['NonEmpty']['constructor']($PS__Data_Bounded['bottom']($dictBoundedEnum['Bounded0']()), $possibilities));
            };
            if ($v['type'] === 'Nothing') {
                return $PS__Control_Applicative['pure'](($dictMonadGen['Monad0']())['Applicative0']())($PS__Data_Bounded['bottom']($dictBoundedEnum['Bounded0']()));
            };
            throw new \Exception('Failed pattern match at Data.Enum.Gen line 13, column 3 - line 18, column 12: ' . var_dump([ $v['constructor']['name'] ]));
        };
    };
    return [
        'genBoundedEnum' => $genBoundedEnum
    ];
})();
