<?php
require_once(dirname(__FILE__) . '/../Effect/index.php');
require_once(dirname(__FILE__) . '/../Foreign.Object/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
require_once(dirname(__FILE__) . '/../Unsafe.Coerce/index.php');
$PS__Server = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    return [
        'response' => $__foreign['response'],
        'createLoop' => $__foreign['createLoop'],
        'createServer' => $__foreign['createServer'],
        'createSocket' => $__foreign['createSocket'],
        'listen' => $__foreign['listen'],
        'runLoop' => $__foreign['runLoop']
    ];
})();
