<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Array.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.String.CodePoints/index.php');
require_once(dirname(__FILE__) . '/../Data.String.NonEmpty.Internal/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Pattern/index.php');
require_once(dirname(__FILE__) . '/../Partial.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
require_once(dirname(__FILE__) . '/../Unsafe.Coerce/index.php');
$PS__Data_String_NonEmpty_CodePoints = (function ()  use (&$PS__Unsafe_Coerce, &$PS__Data_Semigroup, &$PS__Data_String_CodePoints, &$PS__Control_Semigroupoid, &$PS__Data_String_NonEmpty_Internal, &$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Partial_Unsafe, &$PS__Data_Array_NonEmpty, &$PS__Data_Semigroup_Foldable) {
    $toNonEmptyString = $PS__Unsafe_Coerce['unsafeCoerce'];
    $snoc = function ($c)  use (&$toNonEmptyString, &$PS__Data_Semigroup, &$PS__Data_String_CodePoints) {
        return function ($s)  use (&$toNonEmptyString, &$PS__Data_Semigroup, &$PS__Data_String_CodePoints, &$c) {
            return $toNonEmptyString($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($s)($PS__Data_String_CodePoints['singleton']($c)));
        };
    };
    $singleton = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($toNonEmptyString)($PS__Data_String_CodePoints['singleton']);
    $liftS = $PS__Unsafe_Coerce['unsafeCoerce'];
    $takeWhile = function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_String_NonEmpty_Internal, &$liftS, &$PS__Data_String_CodePoints) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_String_NonEmpty_Internal['fromString'])($liftS($PS__Data_String_CodePoints['takeWhile']($f)));
    };
    $lastIndexOf__prime = function ($pat)  use (&$PS__Control_Semigroupoid, &$liftS, &$PS__Data_String_CodePoints) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($liftS)($PS__Data_String_CodePoints['lastIndexOf\'']($pat));
    };
    $lastIndexOf = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($liftS)($PS__Data_String_CodePoints['lastIndexOf']);
    $indexOf__prime = function ($pat)  use (&$PS__Control_Semigroupoid, &$liftS, &$PS__Data_String_CodePoints) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($liftS)($PS__Data_String_CodePoints['indexOf\'']($pat));
    };
    $indexOf = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($liftS)($PS__Data_String_CodePoints['indexOf']);
    $fromNonEmptyString = $PS__Unsafe_Coerce['unsafeCoerce'];
    $length = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_String_CodePoints['length'])($fromNonEmptyString);
    $splitAt = function ($i)  use (&$PS__Data_String_CodePoints, &$fromNonEmptyString, &$PS__Data_String_NonEmpty_Internal) {
        return function ($nes)  use (&$PS__Data_String_CodePoints, &$i, &$fromNonEmptyString, &$PS__Data_String_NonEmpty_Internal) {
            $v = $PS__Data_String_CodePoints['splitAt']($i)($fromNonEmptyString($nes));
            return [
                'before' => $PS__Data_String_NonEmpty_Internal['fromString']($v['before']),
                'after' => $PS__Data_String_NonEmpty_Internal['fromString']($v['after'])
            ];
        };
    };
    $take = function ($i)  use (&$fromNonEmptyString, &$PS__Data_Ord, &$PS__Data_Maybe, &$toNonEmptyString, &$PS__Data_String_CodePoints) {
        return function ($nes)  use (&$fromNonEmptyString, &$PS__Data_Ord, &$i, &$PS__Data_Maybe, &$toNonEmptyString, &$PS__Data_String_CodePoints) {
            $s = $fromNonEmptyString($nes);
            $__local_var__9 = $PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($i)(1);
            if ($__local_var__9) {
                return $PS__Data_Maybe['Nothing']();
            };
            return $PS__Data_Maybe['Just']['constructor']($toNonEmptyString($PS__Data_String_CodePoints['take']($i)($s)));
        };
    };
    $toCodePointArray = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_String_CodePoints['toCodePointArray'])($fromNonEmptyString);
    $toNonEmptyCodePointArray = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Partial_Unsafe['unsafePartial'](function ($dictPartial)  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['fromJust']($dictPartial);
    }))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Array_NonEmpty['fromArray'])($toCodePointArray));
    $uncons = function ($nes)  use (&$fromNonEmptyString, &$PS__Partial_Unsafe, &$PS__Data_Maybe, &$PS__Data_String_CodePoints, &$PS__Data_String_NonEmpty_Internal) {
        $s = $fromNonEmptyString($nes);
        return [
            'head' => $PS__Partial_Unsafe['unsafePartial'](function ($dictPartial)  use (&$PS__Data_Maybe) {
                return $PS__Data_Maybe['fromJust']($dictPartial);
            })($PS__Data_String_CodePoints['codePointAt'](0)($s)),
            'tail' => $PS__Data_String_NonEmpty_Internal['fromString']($PS__Data_String_CodePoints['drop'](1)($s))
        ];
    };
    $fromFoldable1 = function ($dictFoldable1)  use (&$PS__Data_Semigroup_Foldable, &$PS__Data_String_NonEmpty_Internal, &$singleton) {
        return $PS__Data_Semigroup_Foldable['foldMap1']($dictFoldable1)($PS__Data_String_NonEmpty_Internal['semigroupNonEmptyString'])($singleton);
    };
    $fromCodePointArray = function ($v)  use (&$PS__Data_Maybe, &$toNonEmptyString, &$PS__Data_String_CodePoints) {
        if (count($v) === 0) {
            return $PS__Data_Maybe['Nothing']();
        };
        return $PS__Data_Maybe['Just']['constructor']($toNonEmptyString($PS__Data_String_CodePoints['fromCodePointArray']($v)));
    };
    $fromNonEmptyCodePointArray = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Partial_Unsafe['unsafePartial'](function ($dictPartial)  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['fromJust']($dictPartial);
    }))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($fromCodePointArray)($PS__Data_Array_NonEmpty['toArray']));
    $dropWhile = function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_String_NonEmpty_Internal, &$liftS, &$PS__Data_String_CodePoints) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_String_NonEmpty_Internal['fromString'])($liftS($PS__Data_String_CodePoints['dropWhile']($f)));
    };
    $drop = function ($i)  use (&$fromNonEmptyString, &$PS__Data_Ord, &$PS__Data_String_CodePoints, &$PS__Data_Maybe, &$toNonEmptyString) {
        return function ($nes)  use (&$fromNonEmptyString, &$PS__Data_Ord, &$i, &$PS__Data_String_CodePoints, &$PS__Data_Maybe, &$toNonEmptyString) {
            $s = $fromNonEmptyString($nes);
            $__local_var__11 = $PS__Data_Ord['greaterThanOrEq']($PS__Data_Ord['ordInt'])($i)($PS__Data_String_CodePoints['length']($s));
            if ($__local_var__11) {
                return $PS__Data_Maybe['Nothing']();
            };
            return $PS__Data_Maybe['Just']['constructor']($toNonEmptyString($PS__Data_String_CodePoints['drop']($i)($s)));
        };
    };
    $countPrefix = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($liftS)($PS__Data_String_CodePoints['countPrefix']);
    $cons = function ($c)  use (&$toNonEmptyString, &$PS__Data_Semigroup, &$PS__Data_String_CodePoints) {
        return function ($s)  use (&$toNonEmptyString, &$PS__Data_Semigroup, &$PS__Data_String_CodePoints, &$c) {
            return $toNonEmptyString($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_String_CodePoints['singleton']($c))($s));
        };
    };
    $codePointAt = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($liftS)($PS__Data_String_CodePoints['codePointAt']);
    return [
        'fromCodePointArray' => $fromCodePointArray,
        'fromNonEmptyCodePointArray' => $fromNonEmptyCodePointArray,
        'singleton' => $singleton,
        'cons' => $cons,
        'snoc' => $snoc,
        'fromFoldable1' => $fromFoldable1,
        'toCodePointArray' => $toCodePointArray,
        'toNonEmptyCodePointArray' => $toNonEmptyCodePointArray,
        'codePointAt' => $codePointAt,
        'indexOf' => $indexOf,
        'indexOf\'' => $indexOf__prime,
        'lastIndexOf' => $lastIndexOf,
        'lastIndexOf\'' => $lastIndexOf__prime,
        'uncons' => $uncons,
        'length' => $length,
        'take' => $take,
        'takeWhile' => $takeWhile,
        'drop' => $drop,
        'dropWhile' => $dropWhile,
        'countPrefix' => $countPrefix,
        'splitAt' => $splitAt
    ];
})();
