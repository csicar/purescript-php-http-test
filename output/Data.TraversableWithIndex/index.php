<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.FoldableWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.FunctorWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe.First/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe.Last/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Additive/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Conj/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Disj/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Dual/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Multiplicative/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable.Accum/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable.Accum.Internal/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_TraversableWithIndex = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Data_Traversable, &$PS__Data_FunctorWithIndex, &$PS__Data_Function, &$PS__Data_FoldableWithIndex, &$PS__Data_Unit, &$PS__Data_Traversable_Accum_Internal) {
    $TraversableWithIndex = function ($FoldableWithIndex1, $FunctorWithIndex0, $Traversable2, $traverseWithIndex) {
        return [
            'FoldableWithIndex1' => $FoldableWithIndex1,
            'FunctorWithIndex0' => $FunctorWithIndex0,
            'Traversable2' => $Traversable2,
            'traverseWithIndex' => $traverseWithIndex
        ];
    };
    $traverseWithIndexDefault = function ($dictTraversableWithIndex)  use (&$PS__Control_Semigroupoid, &$PS__Data_Traversable, &$PS__Data_FunctorWithIndex) {
        return function ($dictApplicative)  use (&$PS__Control_Semigroupoid, &$PS__Data_Traversable, &$dictTraversableWithIndex, &$PS__Data_FunctorWithIndex) {
            return function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Traversable, &$dictTraversableWithIndex, &$dictApplicative, &$PS__Data_FunctorWithIndex) {
                return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Traversable['sequence']($dictTraversableWithIndex['Traversable2']())($dictApplicative))($PS__Data_FunctorWithIndex['mapWithIndex']($dictTraversableWithIndex['FunctorWithIndex0']())($f));
            };
        };
    };
    $traverseWithIndex = function ($dict) {
        return $dict['traverseWithIndex'];
    };
    $traverseDefault = function ($dictTraversableWithIndex)  use (&$traverseWithIndex, &$PS__Data_Function) {
        return function ($dictApplicative)  use (&$traverseWithIndex, &$dictTraversableWithIndex, &$PS__Data_Function) {
            return function ($f)  use (&$traverseWithIndex, &$dictTraversableWithIndex, &$dictApplicative, &$PS__Data_Function) {
                return $traverseWithIndex($dictTraversableWithIndex)($dictApplicative)($PS__Data_Function['const']($f));
            };
        };
    };
    $traversableWithIndexMultiplicative = $TraversableWithIndex(function ()  use (&$PS__Data_FoldableWithIndex) {
        return $PS__Data_FoldableWithIndex['foldableWithIndexMultiplicative'];
    }, function ()  use (&$PS__Data_FunctorWithIndex) {
        return $PS__Data_FunctorWithIndex['functorWithIndexMultiplicative'];
    }, function ()  use (&$PS__Data_Traversable) {
        return $PS__Data_Traversable['traversableMultiplicative'];
    }, function ($dictApplicative)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$dictApplicative, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Traversable['traverse']($PS__Data_Traversable['traversableMultiplicative'])($dictApplicative))($f($PS__Data_Unit['unit']));
        };
    });
    $traversableWithIndexMaybe = $TraversableWithIndex(function ()  use (&$PS__Data_FoldableWithIndex) {
        return $PS__Data_FoldableWithIndex['foldableWithIndexMaybe'];
    }, function ()  use (&$PS__Data_FunctorWithIndex) {
        return $PS__Data_FunctorWithIndex['functorWithIndexMaybe'];
    }, function ()  use (&$PS__Data_Traversable) {
        return $PS__Data_Traversable['traversableMaybe'];
    }, function ($dictApplicative)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$dictApplicative, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Traversable['traverse']($PS__Data_Traversable['traversableMaybe'])($dictApplicative))($f($PS__Data_Unit['unit']));
        };
    });
    $traversableWithIndexLast = $TraversableWithIndex(function ()  use (&$PS__Data_FoldableWithIndex) {
        return $PS__Data_FoldableWithIndex['foldableWithIndexLast'];
    }, function ()  use (&$PS__Data_FunctorWithIndex) {
        return $PS__Data_FunctorWithIndex['functorWithIndexLast'];
    }, function ()  use (&$PS__Data_Traversable) {
        return $PS__Data_Traversable['traversableLast'];
    }, function ($dictApplicative)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$dictApplicative, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Traversable['traverse']($PS__Data_Traversable['traversableLast'])($dictApplicative))($f($PS__Data_Unit['unit']));
        };
    });
    $traversableWithIndexFirst = $TraversableWithIndex(function ()  use (&$PS__Data_FoldableWithIndex) {
        return $PS__Data_FoldableWithIndex['foldableWithIndexFirst'];
    }, function ()  use (&$PS__Data_FunctorWithIndex) {
        return $PS__Data_FunctorWithIndex['functorWithIndexFirst'];
    }, function ()  use (&$PS__Data_Traversable) {
        return $PS__Data_Traversable['traversableFirst'];
    }, function ($dictApplicative)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$dictApplicative, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Traversable['traverse']($PS__Data_Traversable['traversableFirst'])($dictApplicative))($f($PS__Data_Unit['unit']));
        };
    });
    $traversableWithIndexDual = $TraversableWithIndex(function ()  use (&$PS__Data_FoldableWithIndex) {
        return $PS__Data_FoldableWithIndex['foldableWithIndexDual'];
    }, function ()  use (&$PS__Data_FunctorWithIndex) {
        return $PS__Data_FunctorWithIndex['functorWithIndexDual'];
    }, function ()  use (&$PS__Data_Traversable) {
        return $PS__Data_Traversable['traversableDual'];
    }, function ($dictApplicative)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$dictApplicative, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Traversable['traverse']($PS__Data_Traversable['traversableDual'])($dictApplicative))($f($PS__Data_Unit['unit']));
        };
    });
    $traversableWithIndexDisj = $TraversableWithIndex(function ()  use (&$PS__Data_FoldableWithIndex) {
        return $PS__Data_FoldableWithIndex['foldableWithIndexDisj'];
    }, function ()  use (&$PS__Data_FunctorWithIndex) {
        return $PS__Data_FunctorWithIndex['functorWithIndexDisj'];
    }, function ()  use (&$PS__Data_Traversable) {
        return $PS__Data_Traversable['traversableDisj'];
    }, function ($dictApplicative)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$dictApplicative, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Traversable['traverse']($PS__Data_Traversable['traversableDisj'])($dictApplicative))($f($PS__Data_Unit['unit']));
        };
    });
    $traversableWithIndexConj = $TraversableWithIndex(function ()  use (&$PS__Data_FoldableWithIndex) {
        return $PS__Data_FoldableWithIndex['foldableWithIndexConj'];
    }, function ()  use (&$PS__Data_FunctorWithIndex) {
        return $PS__Data_FunctorWithIndex['functorWithIndexConj'];
    }, function ()  use (&$PS__Data_Traversable) {
        return $PS__Data_Traversable['traversableConj'];
    }, function ($dictApplicative)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$dictApplicative, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Traversable['traverse']($PS__Data_Traversable['traversableConj'])($dictApplicative))($f($PS__Data_Unit['unit']));
        };
    });
    $traversableWithIndexArray = $TraversableWithIndex(function ()  use (&$PS__Data_FoldableWithIndex) {
        return $PS__Data_FoldableWithIndex['foldableWithIndexArray'];
    }, function ()  use (&$PS__Data_FunctorWithIndex) {
        return $PS__Data_FunctorWithIndex['functorWithIndexArray'];
    }, function ()  use (&$PS__Data_Traversable) {
        return $PS__Data_Traversable['traversableArray'];
    }, function ($dictApplicative)  use (&$traverseWithIndexDefault, &$traversableWithIndexArray) {
        return $traverseWithIndexDefault($traversableWithIndexArray)($dictApplicative);
    });
    $traversableWithIndexAdditive = $TraversableWithIndex(function ()  use (&$PS__Data_FoldableWithIndex) {
        return $PS__Data_FoldableWithIndex['foldableWithIndexAdditive'];
    }, function ()  use (&$PS__Data_FunctorWithIndex) {
        return $PS__Data_FunctorWithIndex['functorWithIndexAdditive'];
    }, function ()  use (&$PS__Data_Traversable) {
        return $PS__Data_Traversable['traversableAdditive'];
    }, function ($dictApplicative)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Traversable, &$dictApplicative, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Traversable['traverse']($PS__Data_Traversable['traversableAdditive'])($dictApplicative))($f($PS__Data_Unit['unit']));
        };
    });
    $mapAccumRWithIndex = function ($dictTraversableWithIndex)  use (&$PS__Data_Traversable_Accum_Internal, &$traverseWithIndex) {
        return function ($f)  use (&$PS__Data_Traversable_Accum_Internal, &$traverseWithIndex, &$dictTraversableWithIndex) {
            return function ($s0)  use (&$PS__Data_Traversable_Accum_Internal, &$traverseWithIndex, &$dictTraversableWithIndex, &$f) {
                return function ($xs)  use (&$PS__Data_Traversable_Accum_Internal, &$traverseWithIndex, &$dictTraversableWithIndex, &$f, &$s0) {
                    return $PS__Data_Traversable_Accum_Internal['stateR']($traverseWithIndex($dictTraversableWithIndex)($PS__Data_Traversable_Accum_Internal['applicativeStateR'])(function ($i)  use (&$f) {
                        return function ($a)  use (&$f, &$i) {
                            return function ($s)  use (&$f, &$i, &$a) {
                                return $f($i)($s)($a);
                            };
                        };
                    })($xs))($s0);
                };
            };
        };
    };
    $scanrWithIndex = function ($dictTraversableWithIndex)  use (&$mapAccumRWithIndex) {
        return function ($f)  use (&$mapAccumRWithIndex, &$dictTraversableWithIndex) {
            return function ($b0)  use (&$mapAccumRWithIndex, &$dictTraversableWithIndex, &$f) {
                return function ($xs)  use (&$mapAccumRWithIndex, &$dictTraversableWithIndex, &$f, &$b0) {
                    return ($mapAccumRWithIndex($dictTraversableWithIndex)(function ($i)  use (&$f) {
                        return function ($b)  use (&$f, &$i) {
                            return function ($a)  use (&$f, &$i, &$b) {
                                $b__prime = $f($i)($a)($b);
                                return [
                                    'accum' => $b__prime,
                                    'value' => $b__prime
                                ];
                            };
                        };
                    })($b0)($xs))['value'];
                };
            };
        };
    };
    $mapAccumLWithIndex = function ($dictTraversableWithIndex)  use (&$PS__Data_Traversable_Accum_Internal, &$traverseWithIndex) {
        return function ($f)  use (&$PS__Data_Traversable_Accum_Internal, &$traverseWithIndex, &$dictTraversableWithIndex) {
            return function ($s0)  use (&$PS__Data_Traversable_Accum_Internal, &$traverseWithIndex, &$dictTraversableWithIndex, &$f) {
                return function ($xs)  use (&$PS__Data_Traversable_Accum_Internal, &$traverseWithIndex, &$dictTraversableWithIndex, &$f, &$s0) {
                    return $PS__Data_Traversable_Accum_Internal['stateL']($traverseWithIndex($dictTraversableWithIndex)($PS__Data_Traversable_Accum_Internal['applicativeStateL'])(function ($i)  use (&$f) {
                        return function ($a)  use (&$f, &$i) {
                            return function ($s)  use (&$f, &$i, &$a) {
                                return $f($i)($s)($a);
                            };
                        };
                    })($xs))($s0);
                };
            };
        };
    };
    $scanlWithIndex = function ($dictTraversableWithIndex)  use (&$mapAccumLWithIndex) {
        return function ($f)  use (&$mapAccumLWithIndex, &$dictTraversableWithIndex) {
            return function ($b0)  use (&$mapAccumLWithIndex, &$dictTraversableWithIndex, &$f) {
                return function ($xs)  use (&$mapAccumLWithIndex, &$dictTraversableWithIndex, &$f, &$b0) {
                    return ($mapAccumLWithIndex($dictTraversableWithIndex)(function ($i)  use (&$f) {
                        return function ($b)  use (&$f, &$i) {
                            return function ($a)  use (&$f, &$i, &$b) {
                                $b__prime = $f($i)($b)($a);
                                return [
                                    'accum' => $b__prime,
                                    'value' => $b__prime
                                ];
                            };
                        };
                    })($b0)($xs))['value'];
                };
            };
        };
    };
    $forWithIndex = function ($dictApplicative)  use (&$PS__Data_Function, &$traverseWithIndex) {
        return function ($dictTraversableWithIndex)  use (&$PS__Data_Function, &$traverseWithIndex, &$dictApplicative) {
            return $PS__Data_Function['flip']($traverseWithIndex($dictTraversableWithIndex)($dictApplicative));
        };
    };
    return [
        'TraversableWithIndex' => $TraversableWithIndex,
        'traverseWithIndex' => $traverseWithIndex,
        'traverseWithIndexDefault' => $traverseWithIndexDefault,
        'forWithIndex' => $forWithIndex,
        'scanlWithIndex' => $scanlWithIndex,
        'mapAccumLWithIndex' => $mapAccumLWithIndex,
        'scanrWithIndex' => $scanrWithIndex,
        'mapAccumRWithIndex' => $mapAccumRWithIndex,
        'traverseDefault' => $traverseDefault,
        'traversableWithIndexArray' => $traversableWithIndexArray,
        'traversableWithIndexMaybe' => $traversableWithIndexMaybe,
        'traversableWithIndexFirst' => $traversableWithIndexFirst,
        'traversableWithIndexLast' => $traversableWithIndexLast,
        'traversableWithIndexAdditive' => $traversableWithIndexAdditive,
        'traversableWithIndexDual' => $traversableWithIndexDual,
        'traversableWithIndexConj' => $traversableWithIndexConj,
        'traversableWithIndexDisj' => $traversableWithIndexDisj,
        'traversableWithIndexMultiplicative' => $traversableWithIndexMultiplicative
    ];
})();
