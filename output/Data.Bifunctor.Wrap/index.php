<?php
require_once(dirname(__FILE__) . '/../Control.Biapplicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Biapply/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Bifunctor_Wrap = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Bifunctor, &$PS__Control_Biapply, &$PS__Control_Biapplicative) {
    $Wrap = function ($x) {
        return $x;
    };
    $showWrap = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Wrap ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $ordWrap = function ($dictOrd) {
        return $dictOrd;
    };
    $newtypeWrap = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Wrap);
    $functorWrap = function ($dictBifunctor)  use (&$PS__Data_Functor, &$PS__Data_Bifunctor) {
        return $PS__Data_Functor['Functor'](function ($f)  use (&$PS__Data_Bifunctor, &$dictBifunctor) {
            return function ($v)  use (&$PS__Data_Bifunctor, &$dictBifunctor, &$f) {
                return $PS__Data_Bifunctor['rmap']($dictBifunctor)($f)($v);
            };
        });
    };
    $eqWrap = function ($dictEq) {
        return $dictEq;
    };
    $bifunctorWrap = function ($dictBifunctor)  use (&$PS__Data_Bifunctor) {
        return $PS__Data_Bifunctor['Bifunctor'](function ($f)  use (&$PS__Data_Bifunctor, &$dictBifunctor) {
            return function ($g)  use (&$PS__Data_Bifunctor, &$dictBifunctor, &$f) {
                return function ($v)  use (&$PS__Data_Bifunctor, &$dictBifunctor, &$f, &$g) {
                    return $PS__Data_Bifunctor['bimap']($dictBifunctor)($f)($g)($v);
                };
            };
        });
    };
    $biapplyWrap = function ($dictBiapply)  use (&$PS__Control_Biapply, &$bifunctorWrap) {
        return $PS__Control_Biapply['Biapply'](function ()  use (&$bifunctorWrap, &$dictBiapply) {
            return $bifunctorWrap($dictBiapply['Bifunctor0']());
        }, function ($v)  use (&$PS__Control_Biapply, &$dictBiapply) {
            return function ($v1)  use (&$PS__Control_Biapply, &$dictBiapply, &$v) {
                return $PS__Control_Biapply['biapply']($dictBiapply)($v)($v1);
            };
        });
    };
    $biapplicativeWrap = function ($dictBiapplicative)  use (&$PS__Control_Biapplicative, &$biapplyWrap) {
        return $PS__Control_Biapplicative['Biapplicative'](function ()  use (&$biapplyWrap, &$dictBiapplicative) {
            return $biapplyWrap($dictBiapplicative['Biapply0']());
        }, function ($a)  use (&$PS__Control_Biapplicative, &$dictBiapplicative) {
            return function ($b)  use (&$PS__Control_Biapplicative, &$dictBiapplicative, &$a) {
                return $PS__Control_Biapplicative['bipure']($dictBiapplicative)($a)($b);
            };
        });
    };
    return [
        'Wrap' => $Wrap,
        'newtypeWrap' => $newtypeWrap,
        'eqWrap' => $eqWrap,
        'ordWrap' => $ordWrap,
        'showWrap' => $showWrap,
        'functorWrap' => $functorWrap,
        'bifunctorWrap' => $bifunctorWrap,
        'biapplyWrap' => $biapplyWrap,
        'biapplicativeWrap' => $biapplicativeWrap
    ];
})();
