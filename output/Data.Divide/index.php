<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Data.Comparison/index.php');
require_once(dirname(__FILE__) . '/../Data.Equivalence/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor.Contravariant/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Op/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Predicate/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Divide = (function ()  use (&$PS__Data_Predicate, &$PS__Data_HeytingAlgebra, &$PS__Data_Op, &$PS__Data_Semigroup, &$PS__Data_Equivalence, &$PS__Data_Comparison, &$PS__Data_Ordering, &$PS__Control_Category) {
    $Divide = function ($Contravariant0, $divide) {
        return [
            'Contravariant0' => $Contravariant0,
            'divide' => $divide
        ];
    };
    $dividePredicate = $Divide(function ()  use (&$PS__Data_Predicate) {
        return $PS__Data_Predicate['contravariantPredicate'];
    }, function ($f)  use (&$PS__Data_HeytingAlgebra) {
        return function ($v)  use (&$f, &$PS__Data_HeytingAlgebra) {
            return function ($v1)  use (&$f, &$PS__Data_HeytingAlgebra, &$v) {
                return function ($a)  use (&$f, &$PS__Data_HeytingAlgebra, &$v, &$v1) {
                    $v2 = $f($a);
                    return $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($v($v2['value0']))($v1($v2['value1']));
                };
            };
        };
    });
    $divideOp = function ($dictSemigroup)  use (&$Divide, &$PS__Data_Op, &$PS__Data_Semigroup) {
        return $Divide(function ()  use (&$PS__Data_Op) {
            return $PS__Data_Op['contravariantOp'];
        }, function ($f)  use (&$PS__Data_Semigroup, &$dictSemigroup) {
            return function ($v)  use (&$f, &$PS__Data_Semigroup, &$dictSemigroup) {
                return function ($v1)  use (&$f, &$PS__Data_Semigroup, &$dictSemigroup, &$v) {
                    return function ($a)  use (&$f, &$PS__Data_Semigroup, &$dictSemigroup, &$v, &$v1) {
                        $v2 = $f($a);
                        return $PS__Data_Semigroup['append']($dictSemigroup)($v($v2['value0']))($v1($v2['value1']));
                    };
                };
            };
        });
    };
    $divideEquivalence = $Divide(function ()  use (&$PS__Data_Equivalence) {
        return $PS__Data_Equivalence['contravariantEquivalence'];
    }, function ($f)  use (&$PS__Data_HeytingAlgebra) {
        return function ($v)  use (&$f, &$PS__Data_HeytingAlgebra) {
            return function ($v1)  use (&$f, &$PS__Data_HeytingAlgebra, &$v) {
                return function ($a)  use (&$f, &$PS__Data_HeytingAlgebra, &$v, &$v1) {
                    return function ($b)  use (&$f, &$a, &$PS__Data_HeytingAlgebra, &$v, &$v1) {
                        $v2 = $f($a);
                        $v3 = $f($b);
                        return $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($v($v2['value0'])($v3['value0']))($v1($v2['value1'])($v3['value1']));
                    };
                };
            };
        };
    });
    $divideComparison = $Divide(function ()  use (&$PS__Data_Comparison) {
        return $PS__Data_Comparison['contravariantComparison'];
    }, function ($f)  use (&$PS__Data_Semigroup, &$PS__Data_Ordering) {
        return function ($v)  use (&$f, &$PS__Data_Semigroup, &$PS__Data_Ordering) {
            return function ($v1)  use (&$f, &$PS__Data_Semigroup, &$PS__Data_Ordering, &$v) {
                return function ($a)  use (&$f, &$PS__Data_Semigroup, &$PS__Data_Ordering, &$v, &$v1) {
                    return function ($b)  use (&$f, &$a, &$PS__Data_Semigroup, &$PS__Data_Ordering, &$v, &$v1) {
                        $v2 = $f($a);
                        $v3 = $f($b);
                        return $PS__Data_Semigroup['append']($PS__Data_Ordering['semigroupOrdering'])($v($v2['value0'])($v3['value0']))($v1($v2['value1'])($v3['value1']));
                    };
                };
            };
        };
    });
    $divide = function ($dict) {
        return $dict['divide'];
    };
    $divided = function ($dictDivide)  use (&$divide, &$PS__Control_Category) {
        return $divide($dictDivide)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    };
    return [
        'divide' => $divide,
        'Divide' => $Divide,
        'divided' => $divided,
        'divideComparison' => $divideComparison,
        'divideEquivalence' => $divideEquivalence,
        'dividePredicate' => $dividePredicate,
        'divideOp' => $divideOp
    ];
})();
