<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Identity/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Distributive = (function ()  use (&$PS__Data_Identity, &$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_Newtype, &$PS__Data_Function, &$PS__Control_Category) {
    $Distributive = function ($Functor0, $collect, $distribute) {
        return [
            'Functor0' => $Functor0,
            'collect' => $collect,
            'distribute' => $distribute
        ];
    };
    $distributiveIdentity = $Distributive(function ()  use (&$PS__Data_Identity) {
        return $PS__Data_Identity['functorIdentity'];
    }, function ($dictFunctor)  use (&$PS__Control_Semigroupoid, &$PS__Data_Identity, &$PS__Data_Functor, &$PS__Data_Newtype) {
        return function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Identity, &$PS__Data_Functor, &$dictFunctor, &$PS__Data_Newtype) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Identity['Identity'])($PS__Data_Functor['map']($dictFunctor)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity']))($f)));
        };
    }, function ($dictFunctor)  use (&$PS__Control_Semigroupoid, &$PS__Data_Identity, &$PS__Data_Functor, &$PS__Data_Newtype) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Identity['Identity'])($PS__Data_Functor['map']($dictFunctor)($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity'])));
    });
    $distribute = function ($dict) {
        return $dict['distribute'];
    };
    $distributiveFunction = $Distributive(function ()  use (&$PS__Data_Functor) {
        return $PS__Data_Functor['functorFn'];
    }, function ($dictFunctor)  use (&$PS__Control_Semigroupoid, &$distribute, &$distributiveFunction, &$PS__Data_Functor) {
        return function ($f)  use (&$PS__Control_Semigroupoid, &$distribute, &$distributiveFunction, &$dictFunctor, &$PS__Data_Functor) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($distribute($distributiveFunction)($dictFunctor))($PS__Data_Functor['map']($dictFunctor)($f));
        };
    }, function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Function) {
        return function ($a)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Function) {
            return function ($e)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Function, &$a) {
                return $PS__Data_Functor['map']($dictFunctor)(function ($v)  use (&$PS__Data_Function, &$e) {
                    return $PS__Data_Function['apply']($v)($e);
                })($a);
            };
        };
    });
    $cotraverse = function ($dictDistributive)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$distribute) {
        return function ($dictFunctor)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictDistributive, &$distribute) {
            return function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictDistributive, &$distribute, &$dictFunctor) {
                return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($dictDistributive['Functor0']())($f))($distribute($dictDistributive)($dictFunctor));
            };
        };
    };
    $collectDefault = function ($dictDistributive)  use (&$PS__Control_Semigroupoid, &$distribute, &$PS__Data_Functor) {
        return function ($dictFunctor)  use (&$PS__Control_Semigroupoid, &$distribute, &$dictDistributive, &$PS__Data_Functor) {
            return function ($f)  use (&$PS__Control_Semigroupoid, &$distribute, &$dictDistributive, &$dictFunctor, &$PS__Data_Functor) {
                return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($distribute($dictDistributive)($dictFunctor))($PS__Data_Functor['map']($dictFunctor)($f));
            };
        };
    };
    $collect = function ($dict) {
        return $dict['collect'];
    };
    $distributeDefault = function ($dictDistributive)  use (&$collect, &$PS__Control_Category) {
        return function ($dictFunctor)  use (&$collect, &$dictDistributive, &$PS__Control_Category) {
            return $collect($dictDistributive)($dictFunctor)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        };
    };
    return [
        'collect' => $collect,
        'distribute' => $distribute,
        'Distributive' => $Distributive,
        'distributeDefault' => $distributeDefault,
        'collectDefault' => $collectDefault,
        'cotraverse' => $cotraverse,
        'distributiveIdentity' => $distributiveIdentity,
        'distributiveFunction' => $distributiveFunction
    ];
})();
