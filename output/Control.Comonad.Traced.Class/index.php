<?php
require_once(dirname(__FILE__) . '/../Control.Comonad/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad.Traced.Trans/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Comonad_Traced_Class = (function ()  use (&$PS__Data_Function, &$PS__Control_Comonad, &$PS__Data_Functor, &$PS__Data_Tuple, &$PS__Control_Comonad_Traced_Trans, &$PS__Control_Semigroupoid) {
    $ComonadTraced = function ($Comonad0, $track) {
        return [
            'Comonad0' => $Comonad0,
            'track' => $track
        ];
    };
    $track = function ($dict) {
        return $dict['track'];
    };
    $tracks = function ($dictComonadTraced)  use (&$track, &$PS__Data_Function, &$PS__Control_Comonad) {
        return function ($f)  use (&$track, &$dictComonadTraced, &$PS__Data_Function, &$PS__Control_Comonad) {
            return function ($w)  use (&$track, &$dictComonadTraced, &$PS__Data_Function, &$f, &$PS__Control_Comonad) {
                return $track($dictComonadTraced)($PS__Data_Function['apply']($f)($PS__Control_Comonad['extract']($dictComonadTraced['Comonad0']())($w)))($w);
            };
        };
    };
    $listens = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Tuple) {
        return function ($f)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Tuple) {
            return function ($v)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Tuple, &$f) {
                return $PS__Data_Functor['map']($dictFunctor)(function ($g)  use (&$PS__Data_Tuple, &$f) {
                    return function ($t)  use (&$PS__Data_Tuple, &$g, &$f) {
                        return $PS__Data_Tuple['Tuple']['constructor']($g($t), $f($t));
                    };
                })($v);
            };
        };
    };
    $listen = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Data_Tuple) {
            return $PS__Data_Functor['map']($dictFunctor)(function ($f)  use (&$PS__Data_Tuple) {
                return function ($t)  use (&$PS__Data_Tuple, &$f) {
                    return $PS__Data_Tuple['Tuple']['constructor']($f($t), $t);
                };
            })($v);
        };
    };
    $comonadTracedTracedT = function ($dictComonad)  use (&$ComonadTraced, &$PS__Control_Comonad_Traced_Trans, &$PS__Control_Comonad) {
        return function ($dictMonoid)  use (&$ComonadTraced, &$PS__Control_Comonad_Traced_Trans, &$dictComonad, &$PS__Control_Comonad) {
            return $ComonadTraced(function ()  use (&$PS__Control_Comonad_Traced_Trans, &$dictComonad, &$dictMonoid) {
                return $PS__Control_Comonad_Traced_Trans['comonadTracedT']($dictComonad)($dictMonoid);
            }, function ($t)  use (&$PS__Control_Comonad, &$dictComonad) {
                return function ($v)  use (&$PS__Control_Comonad, &$dictComonad, &$t) {
                    return $PS__Control_Comonad['extract']($dictComonad)($v)($t);
                };
            });
        };
    };
    $censor = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Control_Semigroupoid) {
        return function ($f)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Control_Semigroupoid) {
            return function ($v)  use (&$PS__Data_Functor, &$dictFunctor, &$PS__Control_Semigroupoid, &$f) {
                return $PS__Data_Functor['map']($dictFunctor)(function ($v1)  use (&$PS__Control_Semigroupoid, &$f) {
                    return $PS__Control_Semigroupoid['composeFlipped']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($v1);
                })($v);
            };
        };
    };
    return [
        'track' => $track,
        'ComonadTraced' => $ComonadTraced,
        'tracks' => $tracks,
        'listen' => $listen,
        'listens' => $listens,
        'censor' => $censor,
        'comonadTracedTracedT' => $comonadTracedTracedT
    ];
})();
