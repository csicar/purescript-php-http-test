<?php
require_once(dirname(__FILE__) . '/../Data.EuclideanRing/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
$PS__Data_DivisionRing = (function ()  use (&$PS__Data_Semiring, &$PS__Data_Ring, &$PS__Data_EuclideanRing) {
    $DivisionRing = function ($Ring0, $recip) {
        return [
            'Ring0' => $Ring0,
            'recip' => $recip
        ];
    };
    $recip = function ($dict) {
        return $dict['recip'];
    };
    $rightDiv = function ($dictDivisionRing)  use (&$PS__Data_Semiring, &$recip) {
        return function ($a)  use (&$PS__Data_Semiring, &$dictDivisionRing, &$recip) {
            return function ($b)  use (&$PS__Data_Semiring, &$dictDivisionRing, &$a, &$recip) {
                return $PS__Data_Semiring['mul'](($dictDivisionRing['Ring0']())['Semiring0']())($a)($recip($dictDivisionRing)($b));
            };
        };
    };
    $leftDiv = function ($dictDivisionRing)  use (&$PS__Data_Semiring, &$recip) {
        return function ($a)  use (&$PS__Data_Semiring, &$dictDivisionRing, &$recip) {
            return function ($b)  use (&$PS__Data_Semiring, &$dictDivisionRing, &$recip, &$a) {
                return $PS__Data_Semiring['mul'](($dictDivisionRing['Ring0']())['Semiring0']())($recip($dictDivisionRing)($b))($a);
            };
        };
    };
    $divisionringNumber = $DivisionRing(function ()  use (&$PS__Data_Ring) {
        return $PS__Data_Ring['ringNumber'];
    }, function ($x)  use (&$PS__Data_EuclideanRing) {
        return $PS__Data_EuclideanRing['div']($PS__Data_EuclideanRing['euclideanRingNumber'])(1.0)($x);
    });
    return [
        'DivisionRing' => $DivisionRing,
        'recip' => $recip,
        'leftDiv' => $leftDiv,
        'rightDiv' => $rightDiv,
        'divisionringNumber' => $divisionringNumber
    ];
})();
