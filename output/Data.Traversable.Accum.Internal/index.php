<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable.Accum/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Traversable_Accum_Internal = (function ()  use (&$PS__Data_Functor, &$PS__Control_Apply, &$PS__Control_Applicative) {
    $StateR = function ($x) {
        return $x;
    };
    $StateL = function ($x) {
        return $x;
    };
    $stateR = function ($v) {
        return $v;
    };
    $stateL = function ($v) {
        return $v;
    };
    $functorStateR = $PS__Data_Functor['Functor'](function ($f)  use (&$stateR) {
        return function ($k)  use (&$stateR, &$f) {
            return function ($s)  use (&$stateR, &$k, &$f) {
                $v = $stateR($k)($s);
                return [
                    'accum' => $v['accum'],
                    'value' => $f($v['value'])
                ];
            };
        };
    });
    $functorStateL = $PS__Data_Functor['Functor'](function ($f)  use (&$stateL) {
        return function ($k)  use (&$stateL, &$f) {
            return function ($s)  use (&$stateL, &$k, &$f) {
                $v = $stateL($k)($s);
                return [
                    'accum' => $v['accum'],
                    'value' => $f($v['value'])
                ];
            };
        };
    });
    $applyStateR = $PS__Control_Apply['Apply'](function ()  use (&$functorStateR) {
        return $functorStateR;
    }, function ($f)  use (&$stateR) {
        return function ($x)  use (&$stateR, &$f) {
            return function ($s)  use (&$stateR, &$x, &$f) {
                $v = $stateR($x)($s);
                $v1 = $stateR($f)($v['accum']);
                return [
                    'accum' => $v1['accum'],
                    'value' => $v1['value']($v['value'])
                ];
            };
        };
    });
    $applyStateL = $PS__Control_Apply['Apply'](function ()  use (&$functorStateL) {
        return $functorStateL;
    }, function ($f)  use (&$stateL) {
        return function ($x)  use (&$stateL, &$f) {
            return function ($s)  use (&$stateL, &$f, &$x) {
                $v = $stateL($f)($s);
                $v1 = $stateL($x)($v['accum']);
                return [
                    'accum' => $v1['accum'],
                    'value' => $v['value']($v1['value'])
                ];
            };
        };
    });
    $applicativeStateR = $PS__Control_Applicative['Applicative'](function ()  use (&$applyStateR) {
        return $applyStateR;
    }, function ($a) {
        return function ($s)  use (&$a) {
            return [
                'accum' => $s,
                'value' => $a
            ];
        };
    });
    $applicativeStateL = $PS__Control_Applicative['Applicative'](function ()  use (&$applyStateL) {
        return $applyStateL;
    }, function ($a) {
        return function ($s)  use (&$a) {
            return [
                'accum' => $s,
                'value' => $a
            ];
        };
    });
    return [
        'StateL' => $StateL,
        'stateL' => $stateL,
        'StateR' => $StateR,
        'stateR' => $stateR,
        'functorStateL' => $functorStateL,
        'applyStateL' => $applyStateL,
        'applicativeStateL' => $applicativeStateL,
        'functorStateR' => $functorStateR,
        'applyStateR' => $applyStateR,
        'applicativeStateR' => $applicativeStateR
    ];
})();
