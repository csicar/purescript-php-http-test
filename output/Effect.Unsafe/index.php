<?php
require_once(dirname(__FILE__) . '/../Effect/index.php');
$PS__Effect_Unsafe = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    return [
        'unsafePerformEffect' => $__foreign['unsafePerformEffect']
    ];
})();
