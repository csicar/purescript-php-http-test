<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Biapplicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Biapply/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Bifunctor_Joker = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Bifunctor, &$PS__Control_Biapply, &$PS__Control_Apply, &$PS__Control_Biapplicative, &$PS__Control_Applicative) {
    $Joker = function ($x) {
        return $x;
    };
    $showJoker = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Joker ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $ordJoker = function ($dictOrd) {
        return $dictOrd;
    };
    $newtypeJoker = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Joker);
    $functorJoker = function ($dictFunctor)  use (&$PS__Data_Functor) {
        return $PS__Data_Functor['Functor'](function ($g)  use (&$PS__Data_Functor, &$dictFunctor) {
            return function ($v)  use (&$PS__Data_Functor, &$dictFunctor, &$g) {
                return $PS__Data_Functor['map']($dictFunctor)($g)($v);
            };
        });
    };
    $eqJoker = function ($dictEq) {
        return $dictEq;
    };
    $bifunctorJoker = function ($dictFunctor)  use (&$PS__Data_Bifunctor, &$PS__Data_Functor) {
        return $PS__Data_Bifunctor['Bifunctor'](function ($v)  use (&$PS__Data_Functor, &$dictFunctor) {
            return function ($g)  use (&$PS__Data_Functor, &$dictFunctor) {
                return function ($v1)  use (&$PS__Data_Functor, &$dictFunctor, &$g) {
                    return $PS__Data_Functor['map']($dictFunctor)($g)($v1);
                };
            };
        });
    };
    $biapplyJoker = function ($dictApply)  use (&$PS__Control_Biapply, &$bifunctorJoker, &$PS__Control_Apply) {
        return $PS__Control_Biapply['Biapply'](function ()  use (&$bifunctorJoker, &$dictApply) {
            return $bifunctorJoker($dictApply['Functor0']());
        }, function ($v)  use (&$PS__Control_Apply, &$dictApply) {
            return function ($v1)  use (&$PS__Control_Apply, &$dictApply, &$v) {
                return $PS__Control_Apply['apply']($dictApply)($v)($v1);
            };
        });
    };
    $biapplicativeJoker = function ($dictApplicative)  use (&$PS__Control_Biapplicative, &$biapplyJoker, &$PS__Control_Applicative) {
        return $PS__Control_Biapplicative['Biapplicative'](function ()  use (&$biapplyJoker, &$dictApplicative) {
            return $biapplyJoker($dictApplicative['Apply0']());
        }, function ($v)  use (&$PS__Control_Applicative, &$dictApplicative) {
            return function ($b)  use (&$PS__Control_Applicative, &$dictApplicative) {
                return $PS__Control_Applicative['pure']($dictApplicative)($b);
            };
        });
    };
    return [
        'Joker' => $Joker,
        'newtypeJoker' => $newtypeJoker,
        'eqJoker' => $eqJoker,
        'ordJoker' => $ordJoker,
        'showJoker' => $showJoker,
        'functorJoker' => $functorJoker,
        'bifunctorJoker' => $bifunctorJoker,
        'biapplyJoker' => $biapplyJoker,
        'biapplicativeJoker' => $biapplicativeJoker
    ];
})();
