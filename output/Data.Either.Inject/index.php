<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Either/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Either_Inject = (function ()  use (&$PS__Control_Category, &$PS__Data_Maybe, &$PS__Data_Either, &$PS__Data_Function, &$PS__Control_Semigroupoid) {
    $Inject = function ($inj, $prj) {
        return [
            'inj' => $inj,
            'prj' => $prj
        ];
    };
    $prj = function ($dict) {
        return $dict['prj'];
    };
    $injectReflexive = $Inject($PS__Control_Category['identity']($PS__Control_Category['categoryFn']), $PS__Data_Maybe['Just']['create']);
    $injectLeft = $Inject($PS__Data_Either['Left']['create'], $PS__Data_Either['either']($PS__Data_Maybe['Just']['create'])($PS__Data_Function['const']($PS__Data_Maybe['Nothing']())));
    $inj = function ($dict) {
        return $dict['inj'];
    };
    $injectRight = function ($dictInject)  use (&$Inject, &$PS__Control_Semigroupoid, &$PS__Data_Either, &$inj, &$PS__Data_Function, &$PS__Data_Maybe, &$prj) {
        return $Inject($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Either['Right']['create'])($inj($dictInject)), $PS__Data_Either['either']($PS__Data_Function['const']($PS__Data_Maybe['Nothing']()))($prj($dictInject)));
    };
    return [
        'inj' => $inj,
        'prj' => $prj,
        'Inject' => $Inject,
        'injectLeft' => $injectLeft,
        'injectRight' => $injectRight,
        'injectReflexive' => $injectReflexive
    ];
})();
