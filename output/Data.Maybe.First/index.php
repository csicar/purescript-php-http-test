<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Extend/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadZero/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor.Invariant/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Maybe_First = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Maybe, &$PS__Data_Newtype, &$PS__Data_Monoid, &$PS__Control_Alt, &$PS__Control_Plus, &$PS__Control_Alternative, &$PS__Control_MonadZero) {
    $First = function ($x) {
        return $x;
    };
    $showFirst = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Maybe) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$PS__Data_Maybe, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('First (')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Maybe['showMaybe']($dictShow))($v))(')'));
        });
    };
    $semigroupFirst = $PS__Data_Semigroup['Semigroup'](function ($v) {
        return function ($v1)  use (&$v) {
            if ($v['type'] === 'Just') {
                return $v;
            };
            return $v1;
        };
    });
    $ordFirst = function ($dictOrd)  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['ordMaybe']($dictOrd);
    };
    $ord1First = $PS__Data_Maybe['ord1Maybe'];
    $newtypeFirst = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $First);
    $monoidFirst = $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupFirst) {
        return $semigroupFirst;
    }, $PS__Data_Maybe['Nothing']());
    $monadFirst = $PS__Data_Maybe['monadMaybe'];
    $invariantFirst = $PS__Data_Maybe['invariantMaybe'];
    $functorFirst = $PS__Data_Maybe['functorMaybe'];
    $extendFirst = $PS__Data_Maybe['extendMaybe'];
    $eqFirst = function ($dictEq)  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['eqMaybe']($dictEq);
    };
    $eq1First = $PS__Data_Maybe['eq1Maybe'];
    $boundedFirst = function ($dictBounded)  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['boundedMaybe']($dictBounded);
    };
    $bindFirst = $PS__Data_Maybe['bindMaybe'];
    $applyFirst = $PS__Data_Maybe['applyMaybe'];
    $applicativeFirst = $PS__Data_Maybe['applicativeMaybe'];
    $altFirst = $PS__Control_Alt['Alt'](function ()  use (&$functorFirst) {
        return $functorFirst;
    }, $PS__Data_Semigroup['append']($semigroupFirst));
    $plusFirst = $PS__Control_Plus['Plus'](function ()  use (&$altFirst) {
        return $altFirst;
    }, $PS__Data_Monoid['mempty']($monoidFirst));
    $alternativeFirst = $PS__Control_Alternative['Alternative'](function ()  use (&$applicativeFirst) {
        return $applicativeFirst;
    }, function ()  use (&$plusFirst) {
        return $plusFirst;
    });
    $monadZeroFirst = $PS__Control_MonadZero['MonadZero'](function ()  use (&$alternativeFirst) {
        return $alternativeFirst;
    }, function ()  use (&$monadFirst) {
        return $monadFirst;
    });
    return [
        'First' => $First,
        'newtypeFirst' => $newtypeFirst,
        'eqFirst' => $eqFirst,
        'eq1First' => $eq1First,
        'ordFirst' => $ordFirst,
        'ord1First' => $ord1First,
        'boundedFirst' => $boundedFirst,
        'functorFirst' => $functorFirst,
        'invariantFirst' => $invariantFirst,
        'applyFirst' => $applyFirst,
        'applicativeFirst' => $applicativeFirst,
        'bindFirst' => $bindFirst,
        'monadFirst' => $monadFirst,
        'extendFirst' => $extendFirst,
        'showFirst' => $showFirst,
        'semigroupFirst' => $semigroupFirst,
        'monoidFirst' => $monoidFirst,
        'altFirst' => $altFirst,
        'plusFirst' => $plusFirst,
        'alternativeFirst' => $alternativeFirst,
        'monadZeroFirst' => $monadZeroFirst
    ];
})();
