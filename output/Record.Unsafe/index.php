<?php
$PS__Record_Unsafe = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    return [
        'unsafeHas' => $__foreign['unsafeHas'],
        'unsafeGet' => $__foreign['unsafeGet'],
        'unsafeSet' => $__foreign['unsafeSet'],
        'unsafeDelete' => $__foreign['unsafeDelete']
    ];
})();
