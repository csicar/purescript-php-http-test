<?php
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Pattern/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_String_Common = (function ()  use (&$PS__Data_Eq, &$PS__Data_Ordering) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $__null = function ($s)  use (&$PS__Data_Eq) {
        return $PS__Data_Eq['eq']($PS__Data_Eq['eqString'])($s)('');
    };
    $localeCompare = $__foreign['_localeCompare']($PS__Data_Ordering['LT']())($PS__Data_Ordering['EQ']())($PS__Data_Ordering['GT']());
    return [
        'null' => $__null,
        'localeCompare' => $localeCompare,
        'replace' => $__foreign['replace'],
        'replaceAll' => $__foreign['replaceAll'],
        'split' => $__foreign['split'],
        'toLower' => $__foreign['toLower'],
        'toUpper' => $__foreign['toUpper'],
        'trim' => $__foreign['trim'],
        'joinWith' => $__foreign['joinWith']
    ];
})();
