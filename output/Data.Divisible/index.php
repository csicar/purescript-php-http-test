<?php
require_once(dirname(__FILE__) . '/../Data.Comparison/index.php');
require_once(dirname(__FILE__) . '/../Data.Divide/index.php');
require_once(dirname(__FILE__) . '/../Data.Equivalence/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Op/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Predicate/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Divisible = (function ()  use (&$PS__Data_Divide, &$PS__Data_Function, &$PS__Data_Op, &$PS__Data_Monoid, &$PS__Data_Equivalence, &$PS__Data_Comparison, &$PS__Data_Ordering) {
    $Divisible = function ($Divide0, $conquer) {
        return [
            'Divide0' => $Divide0,
            'conquer' => $conquer
        ];
    };
    $divisiblePredicate = $Divisible(function ()  use (&$PS__Data_Divide) {
        return $PS__Data_Divide['dividePredicate'];
    }, $PS__Data_Function['const'](true));
    $divisibleOp = function ($dictMonoid)  use (&$Divisible, &$PS__Data_Divide, &$PS__Data_Function, &$PS__Data_Op, &$PS__Data_Monoid) {
        return $Divisible(function ()  use (&$PS__Data_Divide, &$dictMonoid) {
            return $PS__Data_Divide['divideOp']($dictMonoid['Semigroup0']());
        }, $PS__Data_Function['apply']($PS__Data_Op['Op'])($PS__Data_Function['const']($PS__Data_Monoid['mempty']($dictMonoid))));
    };
    $divisibleEquivalence = $Divisible(function ()  use (&$PS__Data_Divide) {
        return $PS__Data_Divide['divideEquivalence'];
    }, $PS__Data_Function['apply']($PS__Data_Equivalence['Equivalence'])(function ($v) {
        return function ($v1) {
            return true;
        };
    }));
    $divisibleComparison = $Divisible(function ()  use (&$PS__Data_Divide) {
        return $PS__Data_Divide['divideComparison'];
    }, $PS__Data_Function['apply']($PS__Data_Comparison['Comparison'])(function ($v)  use (&$PS__Data_Ordering) {
        return function ($v1)  use (&$PS__Data_Ordering) {
            return $PS__Data_Ordering['EQ']();
        };
    }));
    $conquer = function ($dict) {
        return $dict['conquer'];
    };
    return [
        'conquer' => $conquer,
        'Divisible' => $Divisible,
        'divisibleComparison' => $divisibleComparison,
        'divisibleEquivalence' => $divisibleEquivalence,
        'divisiblePredicate' => $divisiblePredicate,
        'divisibleOp' => $divisibleOp
    ];
})();
