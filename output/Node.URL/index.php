<?php
require_once(dirname(__FILE__) . '/../Data.Nullable/index.php');
$PS__Node_URL = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    return [
        'parse' => $__foreign['parse'],
        'format' => $__foreign['format'],
        'resolve' => $__foreign['resolve'],
        'parseQueryString' => $__foreign['parseQueryString'],
        'toQueryString' => $__foreign['toQueryString']
    ];
})();
