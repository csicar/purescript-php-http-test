<?php
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Effect/index.php');
$PS__Effect_Console = (function ()  use (&$PS__Data_Show) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $warnShow = function ($dictShow)  use (&$__foreign, &$PS__Data_Show) {
        return function ($a)  use (&$__foreign, &$PS__Data_Show, &$dictShow) {
            return $__foreign['warn']($PS__Data_Show['show']($dictShow)($a));
        };
    };
    $logShow = function ($dictShow)  use (&$__foreign, &$PS__Data_Show) {
        return function ($a)  use (&$__foreign, &$PS__Data_Show, &$dictShow) {
            return $__foreign['log']($PS__Data_Show['show']($dictShow)($a));
        };
    };
    $infoShow = function ($dictShow)  use (&$__foreign, &$PS__Data_Show) {
        return function ($a)  use (&$__foreign, &$PS__Data_Show, &$dictShow) {
            return $__foreign['info']($PS__Data_Show['show']($dictShow)($a));
        };
    };
    $errorShow = function ($dictShow)  use (&$__foreign, &$PS__Data_Show) {
        return function ($a)  use (&$__foreign, &$PS__Data_Show, &$dictShow) {
            return $__foreign['error']($PS__Data_Show['show']($dictShow)($a));
        };
    };
    return [
        'logShow' => $logShow,
        'warnShow' => $warnShow,
        'errorShow' => $errorShow,
        'infoShow' => $infoShow,
        'log' => $__foreign['log'],
        'warn' => $__foreign['warn'],
        'error' => $__foreign['error'],
        'info' => $__foreign['info']
    ];
})();
