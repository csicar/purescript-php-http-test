<?php

$exports["log"] = function ($s) {
  return function () use (&$s) {
    echo($s);
    return [];
  };
};

$exports["warn"] = function ($s) {
  return function () use (&$s) {
    echo($s);
    return [];
  };
};

$exports["error"] = function ($s) {
  return function () use (&$s) {
    die($s);
    return [];
  };
};

$exports["info"] = function ($s) {
  return function () use (&$s) {
    echo($s);
    return [];
  };
};
