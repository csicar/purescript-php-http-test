<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
$PS__Control_Category = (function ()  use (&$PS__Control_Semigroupoid) {
    $Category = function ($Semigroupoid0, $identity) {
        return [
            'Semigroupoid0' => $Semigroupoid0,
            'identity' => $identity
        ];
    };
    $identity = function ($dict) {
        return $dict['identity'];
    };
    $categoryFn = $Category(function ()  use (&$PS__Control_Semigroupoid) {
        return $PS__Control_Semigroupoid['semigroupoidFn'];
    }, function ($x) {
        return $x;
    });
    return [
        'Category' => $Category,
        'identity' => $identity,
        'categoryFn' => $categoryFn
    ];
})();
