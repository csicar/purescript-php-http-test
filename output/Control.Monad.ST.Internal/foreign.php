<?php

$exports['map_'] = function ($f) {
  return function ($a) use (&$f) {
    return function () use (&$f, &$a) {
      return $f($a());
    };
  };
};

$exports['pure_'] = function ($a) {
  return function () use (&$a) {
    return $a;
  };
};

$exports['bind_'] = function ($a) {
  return function ($f) use (&$a) {
    return function () use (&$a, &$f) {
      return $f($a())();
    };
  };
};

$exports['run'] = function ($f) {
  return $f();
};

$exports['while'] = function ($f) {
  return function ($a) use (&$f) {
    return function () use (&$a, &$f) {
      while ($f()) {
        $a();
      }
    };
  };
};

$exports['for'] = function ($lo) {
  return function ($hi) use (&$lo) {
    return function ($f) use (&$hi, &$lo) {
      return function () use (&$hi, &$lo, &$f) {
        for ($i = $lo; $i < $hi; $i++) {
          $f($i)();
        }
      };
    };
  };
};

$exports['foreach'] = function ($as) {
  return function ($f) use (&$as) {
    return function () use (&$f, &$as) {
      for ($i = 0, $l = count($as); $i < $l; $i++) {
        $f($as[$i])();
      }
    };
  };
};

$exports['new'] = function ($val) {
  return function () use (&$val) {
    return [ 'value' => $val ];
  };
};

$exports['read'] = function (&$ref) {
  return function () use (&$ref) {
    return $ref['value'];
  };
};

$exports['modify\''] = function ($f) {
  return function (&$ref) use (&$f) {
    return function () use (&$ref, &$f) {
      $t = $f($ref['value']);
      $ref['value'] = $t['state'];
      return $t['value'];
    };
  };
};

$exports['write'] = function ($a) {
  return function (&$ref) use (&$a) {
    return function () use (&$a, &$ref) {
      $ref['value'] = $a;
      return $ref;
    };
  };
};
