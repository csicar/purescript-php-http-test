<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Partial.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_ST_Internal = (function ()  use (&$PS__Data_Functor, &$PS__Control_Monad, &$PS__Control_Bind, &$PS__Control_Apply, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$PS__Partial_Unsafe, &$PS__Data_Unit) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $modify = function ($f)  use (&$__foreign) {
        return $__foreign['modify\''](function ($s)  use (&$f) {
            $s__prime = $f($s);
            return [
                'state' => $s__prime,
                'value' => $s__prime
            ];
        });
    };
    $functorST = $PS__Data_Functor['Functor']($__foreign['map_']);
    $monadST = $PS__Control_Monad['Monad'](function ()  use (&$applicativeST) {
        return $applicativeST;
    }, function ()  use (&$bindST) {
        return $bindST;
    });
    $bindST = $PS__Control_Bind['Bind'](function ()  use (&$applyST) {
        return $applyST;
    }, $__foreign['bind_']);
    $applyST = $PS__Control_Apply['Apply'](function ()  use (&$functorST) {
        return $functorST;
    }, $PS__Control_Monad['ap']($monadST));
    $applicativeST = $PS__Control_Applicative['Applicative'](function ()  use (&$applyST) {
        return $applyST;
    }, $__foreign['pure_']);
    $monadRecST = $PS__Control_Monad_Rec_Class['MonadRec'](function ()  use (&$monadST) {
        return $monadST;
    }, function ($f)  use (&$PS__Partial_Unsafe, &$PS__Control_Bind, &$bindST, &$__foreign, &$PS__Data_Functor, &$functorST, &$PS__Control_Applicative, &$applicativeST, &$PS__Data_Unit) {
        return function ($a)  use (&$PS__Partial_Unsafe, &$PS__Control_Bind, &$bindST, &$__foreign, &$f, &$PS__Data_Functor, &$functorST, &$PS__Control_Applicative, &$applicativeST, &$PS__Data_Unit) {
            $isLooping = function ($v) {
                if ($v['type'] === 'Loop') {
                    return true;
                };
                return false;
            };
            $fromDone = $PS__Partial_Unsafe['unsafePartial'](function ($dictPartial) {
                return function ($v)  use (&$dictPartial) {
                    $___unused = function ($dictPartial1) {
                        return function ($__local_var__6) {
                            return $__local_var__6;
                        };
                    };
                    return $___unused($dictPartial)((function ()  use (&$v) {
                        if ($v['type'] === 'Done') {
                            return $v['value0'];
                        };
                        throw new \Exception('Failed pattern match at Control.Monad.ST.Internal line 54, column 32 - line 54, column 46: ' . var_dump([ $v['constructor']['name'] ]));
                    })());
                };
            });
            return $PS__Control_Bind['bind']($bindST)($PS__Control_Bind['bindFlipped']($bindST)($__foreign['new'])($f($a)))(function ($v)  use (&$PS__Control_Bind, &$bindST, &$__foreign, &$PS__Data_Functor, &$functorST, &$isLooping, &$f, &$PS__Control_Applicative, &$applicativeST, &$PS__Data_Unit, &$fromDone) {
                return $PS__Control_Bind['discard']($PS__Control_Bind['discardUnit'])($bindST)($__foreign['while']($PS__Data_Functor['map']($functorST)($isLooping)($__foreign['read']($v)))($PS__Control_Bind['bind']($bindST)($__foreign['read']($v))(function ($v1)  use (&$PS__Control_Bind, &$bindST, &$f, &$PS__Data_Functor, &$functorST, &$__foreign, &$v, &$PS__Control_Applicative, &$applicativeST, &$PS__Data_Unit) {
                    if ($v1['type'] === 'Loop') {
                        return $PS__Control_Bind['bind']($bindST)($f($v1['value0']))(function ($v2)  use (&$PS__Data_Functor, &$functorST, &$__foreign, &$v) {
                            return $PS__Data_Functor['void']($functorST)($__foreign['write']($v2)($v));
                        });
                    };
                    if ($v1['type'] === 'Done') {
                        return $PS__Control_Applicative['pure']($applicativeST)($PS__Data_Unit['unit']);
                    };
                    throw new \Exception('Failed pattern match at Control.Monad.ST.Internal line 46, column 18 - line 50, column 28: ' . var_dump([ $v1['constructor']['name'] ]));
                })))(function ()  use (&$PS__Data_Functor, &$functorST, &$fromDone, &$__foreign, &$v) {
                    return $PS__Data_Functor['map']($functorST)($fromDone)($__foreign['read']($v));
                });
            });
        };
    });
    return [
        'modify' => $modify,
        'functorST' => $functorST,
        'applyST' => $applyST,
        'applicativeST' => $applicativeST,
        'bindST' => $bindST,
        'monadST' => $monadST,
        'monadRecST' => $monadRecST,
        'map_' => $__foreign['map_'],
        'pure_' => $__foreign['pure_'],
        'bind_' => $__foreign['bind_'],
        'run' => $__foreign['run'],
        'while' => $__foreign['while'],
        'for' => $__foreign['for'],
        'foreach' => $__foreign['foreach'],
        'new' => $__foreign['new'],
        'read' => $__foreign['read'],
        'modify\'' => $__foreign['modify\''],
        'write' => $__foreign['write']
    ];
})();
