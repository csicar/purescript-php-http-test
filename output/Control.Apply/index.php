<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
$PS__Control_Apply = (function ()  use (&$PS__Data_Functor, &$PS__Data_Function, &$PS__Control_Category) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Apply = function ($Functor0, $apply) {
        return [
            'Functor0' => $Functor0,
            'apply' => $apply
        ];
    };
    $applyFn = $Apply(function ()  use (&$PS__Data_Functor) {
        return $PS__Data_Functor['functorFn'];
    }, function ($f) {
        return function ($g)  use (&$f) {
            return function ($x)  use (&$f, &$g) {
                return $f($x)($g($x));
            };
        };
    });
    $applyArray = $Apply(function ()  use (&$PS__Data_Functor) {
        return $PS__Data_Functor['functorArray'];
    }, $__foreign['arrayApply']);
    $apply = function ($dict) {
        return $dict['apply'];
    };
    $applyFirst = function ($dictApply)  use (&$apply, &$PS__Data_Functor, &$PS__Data_Function) {
        return function ($a)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$PS__Data_Function) {
            return function ($b)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$PS__Data_Function, &$a) {
                return $apply($dictApply)($PS__Data_Functor['map']($dictApply['Functor0']())($PS__Data_Function['const'])($a))($b);
            };
        };
    };
    $applySecond = function ($dictApply)  use (&$apply, &$PS__Data_Functor, &$PS__Data_Function, &$PS__Control_Category) {
        return function ($a)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$PS__Data_Function, &$PS__Control_Category) {
            return function ($b)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$PS__Data_Function, &$PS__Control_Category, &$a) {
                return $apply($dictApply)($PS__Data_Functor['map']($dictApply['Functor0']())($PS__Data_Function['const']($PS__Control_Category['identity']($PS__Control_Category['categoryFn'])))($a))($b);
            };
        };
    };
    $lift2 = function ($dictApply)  use (&$apply, &$PS__Data_Functor) {
        return function ($f)  use (&$apply, &$dictApply, &$PS__Data_Functor) {
            return function ($a)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$f) {
                return function ($b)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$f, &$a) {
                    return $apply($dictApply)($PS__Data_Functor['map']($dictApply['Functor0']())($f)($a))($b);
                };
            };
        };
    };
    $lift3 = function ($dictApply)  use (&$apply, &$PS__Data_Functor) {
        return function ($f)  use (&$apply, &$dictApply, &$PS__Data_Functor) {
            return function ($a)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$f) {
                return function ($b)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$f, &$a) {
                    return function ($c)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$f, &$a, &$b) {
                        return $apply($dictApply)($apply($dictApply)($PS__Data_Functor['map']($dictApply['Functor0']())($f)($a))($b))($c);
                    };
                };
            };
        };
    };
    $lift4 = function ($dictApply)  use (&$apply, &$PS__Data_Functor) {
        return function ($f)  use (&$apply, &$dictApply, &$PS__Data_Functor) {
            return function ($a)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$f) {
                return function ($b)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$f, &$a) {
                    return function ($c)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$f, &$a, &$b) {
                        return function ($d)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$f, &$a, &$b, &$c) {
                            return $apply($dictApply)($apply($dictApply)($apply($dictApply)($PS__Data_Functor['map']($dictApply['Functor0']())($f)($a))($b))($c))($d);
                        };
                    };
                };
            };
        };
    };
    $lift5 = function ($dictApply)  use (&$apply, &$PS__Data_Functor) {
        return function ($f)  use (&$apply, &$dictApply, &$PS__Data_Functor) {
            return function ($a)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$f) {
                return function ($b)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$f, &$a) {
                    return function ($c)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$f, &$a, &$b) {
                        return function ($d)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$f, &$a, &$b, &$c) {
                            return function ($e)  use (&$apply, &$dictApply, &$PS__Data_Functor, &$f, &$a, &$b, &$c, &$d) {
                                return $apply($dictApply)($apply($dictApply)($apply($dictApply)($apply($dictApply)($PS__Data_Functor['map']($dictApply['Functor0']())($f)($a))($b))($c))($d))($e);
                            };
                        };
                    };
                };
            };
        };
    };
    return [
        'Apply' => $Apply,
        'apply' => $apply,
        'applyFirst' => $applyFirst,
        'applySecond' => $applySecond,
        'lift2' => $lift2,
        'lift3' => $lift3,
        'lift4' => $lift4,
        'lift5' => $lift5,
        'applyFn' => $applyFn,
        'applyArray' => $applyArray
    ];
})();
