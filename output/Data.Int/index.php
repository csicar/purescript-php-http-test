<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Boolean/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.CommutativeRing/index.php');
require_once(dirname(__FILE__) . '/../Data.DivisionRing/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.EuclideanRing/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Int.Bits/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Global/index.php');
require_once(dirname(__FILE__) . '/../Math/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Int = (function ()  use (&$PS__Data_Show, &$PS__Data_HeytingAlgebra, &$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Data_Eq, &$PS__Data_Int_Bits, &$PS__Global, &$PS__Data_Ring, &$PS__Data_Bounded, &$PS__Control_Semigroupoid, &$PS__Math, &$PS__Data_Ordering, &$PS__Data_Semiring, &$PS__Data_DivisionRing, &$PS__Control_Category, &$PS__Data_CommutativeRing, &$PS__Data_EuclideanRing) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Radix = function ($x) {
        return $x;
    };
    $Even = function () {
        return [
            'type' => 'Even'
        ];
    };
    $Odd = function () {
        return [
            'type' => 'Odd'
        ];
    };
    $showParity = $PS__Data_Show['Show'](function ($v) {
        if ($v['type'] === 'Even') {
            return 'Even';
        };
        if ($v['type'] === 'Odd') {
            return 'Odd';
        };
        throw new \Exception('Failed pattern match at Data.Int line 112, column 1 - line 112, column 35: ' . var_dump([ $v['constructor']['name'] ]));
    });
    $radix = function ($n)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Boolean) {
        if ($PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Ord['greaterThanOrEq']($PS__Data_Ord['ordInt'])($n)(2))($PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])($n)(36))) {
            return $PS__Data_Maybe['Just']['constructor']($n);
        };
        if ($PS__Data_Boolean['otherwise']) {
            return $PS__Data_Maybe['Nothing']();
        };
        throw new \Exception('Failed pattern match at Data.Int line 193, column 1 - line 193, column 28: ' . var_dump([ $n['constructor']['name'] ]));
    };
    $odd = function ($x)  use (&$PS__Data_Eq, &$PS__Data_Int_Bits) {
        return $PS__Data_Eq['notEq']($PS__Data_Eq['eqInt'])($PS__Data_Int_Bits['and']($x)(1))(0);
    };
    $octal = 8;
    $hexadecimal = 16;
    $fromStringAs = $__foreign['fromStringAsImpl']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $fromString = $fromStringAs(10);
    $fromNumber = $__foreign['fromNumberImpl']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $unsafeClamp = function ($x)  use (&$PS__Data_Eq, &$PS__Global, &$PS__Data_Ring, &$PS__Data_Ord, &$__foreign, &$PS__Data_Bounded, &$PS__Data_Boolean, &$PS__Data_Maybe, &$fromNumber) {
        if ($PS__Data_Eq['eq']($PS__Data_Eq['eqNumber'])($x)($PS__Global['infinity'])) {
            return 0;
        };
        if ($PS__Data_Eq['eq']($PS__Data_Eq['eqNumber'])($x)($PS__Data_Ring['negate']($PS__Data_Ring['ringNumber'])($PS__Global['infinity']))) {
            return 0;
        };
        if ($PS__Data_Ord['greaterThanOrEq']($PS__Data_Ord['ordNumber'])($x)($__foreign['toNumber']($PS__Data_Bounded['top']($PS__Data_Bounded['boundedInt'])))) {
            return $PS__Data_Bounded['top']($PS__Data_Bounded['boundedInt']);
        };
        if ($PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordNumber'])($x)($__foreign['toNumber']($PS__Data_Bounded['bottom']($PS__Data_Bounded['boundedInt'])))) {
            return $PS__Data_Bounded['bottom']($PS__Data_Bounded['boundedInt']);
        };
        if ($PS__Data_Boolean['otherwise']) {
            return $PS__Data_Maybe['fromMaybe'](0)($fromNumber($x));
        };
        throw new \Exception('Failed pattern match at Data.Int line 66, column 1 - line 66, column 29: ' . var_dump([ $x['constructor']['name'] ]));
    };
    $round = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unsafeClamp)($PS__Math['round']);
    $floor = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unsafeClamp)($PS__Math['floor']);
    $even = function ($x)  use (&$PS__Data_Eq, &$PS__Data_Int_Bits) {
        return $PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($PS__Data_Int_Bits['and']($x)(1))(0);
    };
    $parity = function ($n)  use (&$even, &$Even, &$Odd) {
        $__local_var__14 = $even($n);
        if ($__local_var__14) {
            return $Even();
        };
        return $Odd();
    };
    $eqParity = $PS__Data_Eq['Eq'](function ($x) {
        return function ($y)  use (&$x) {
            if ($x['type'] === 'Even' && $y['type'] === 'Even') {
                return true;
            };
            if ($x['type'] === 'Odd' && $y['type'] === 'Odd') {
                return true;
            };
            return false;
        };
    });
    $ordParity = $PS__Data_Ord['Ord'](function ()  use (&$eqParity) {
        return $eqParity;
    }, function ($x)  use (&$PS__Data_Ordering) {
        return function ($y)  use (&$x, &$PS__Data_Ordering) {
            if ($x['type'] === 'Even' && $y['type'] === 'Even') {
                return $PS__Data_Ordering['EQ']();
            };
            if ($x['type'] === 'Even') {
                return $PS__Data_Ordering['LT']();
            };
            if ($y['type'] === 'Even') {
                return $PS__Data_Ordering['GT']();
            };
            if ($x['type'] === 'Odd' && $y['type'] === 'Odd') {
                return $PS__Data_Ordering['EQ']();
            };
            throw new \Exception('Failed pattern match at Data.Int line 110, column 8 - line 110, column 40: ' . var_dump([ $x['constructor']['name'], $y['constructor']['name'] ]));
        };
    });
    $semiringParity = $PS__Data_Semiring['Semiring'](function ($x)  use (&$PS__Data_Eq, &$eqParity, &$Even, &$Odd) {
        return function ($y)  use (&$PS__Data_Eq, &$eqParity, &$x, &$Even, &$Odd) {
            $__local_var__19 = $PS__Data_Eq['eq']($eqParity)($x)($y);
            if ($__local_var__19) {
                return $Even();
            };
            return $Odd();
        };
    }, function ($v)  use (&$Odd, &$Even) {
        return function ($v1)  use (&$v, &$Odd, &$Even) {
            if ($v['type'] === 'Odd' && $v1['type'] === 'Odd') {
                return $Odd();
            };
            return $Even();
        };
    }, $Odd(), $Even());
    $ringParity = $PS__Data_Ring['Ring'](function ()  use (&$semiringParity) {
        return $semiringParity;
    }, $PS__Data_Semiring['add']($semiringParity));
    $divisionRingParity = $PS__Data_DivisionRing['DivisionRing'](function ()  use (&$ringParity) {
        return $ringParity;
    }, $PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    $decimal = 10;
    $commutativeRingParity = $PS__Data_CommutativeRing['CommutativeRing'](function ()  use (&$ringParity) {
        return $ringParity;
    });
    $euclideanRingParity = $PS__Data_EuclideanRing['EuclideanRing'](function ()  use (&$commutativeRingParity) {
        return $commutativeRingParity;
    }, function ($v) {
        if ($v['type'] === 'Even') {
            return 0;
        };
        if ($v['type'] === 'Odd') {
            return 1;
        };
        throw new \Exception('Failed pattern match at Data.Int line 132, column 1 - line 132, column 53: ' . var_dump([ $v['constructor']['name'] ]));
    }, function ($x) {
        return function ($v)  use (&$x) {
            return $x;
        };
    }, function ($v)  use (&$Even) {
        return function ($v1)  use (&$Even) {
            return $Even();
        };
    });
    $ceil = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unsafeClamp)($PS__Math['ceil']);
    $boundedParity = $PS__Data_Bounded['Bounded'](function ()  use (&$ordParity) {
        return $ordParity;
    }, $Even(), $Odd());
    $binary = 2;
    $base36 = 36;
    return [
        'fromNumber' => $fromNumber,
        'ceil' => $ceil,
        'floor' => $floor,
        'round' => $round,
        'fromString' => $fromString,
        'radix' => $radix,
        'binary' => $binary,
        'octal' => $octal,
        'decimal' => $decimal,
        'hexadecimal' => $hexadecimal,
        'base36' => $base36,
        'fromStringAs' => $fromStringAs,
        'Even' => $Even,
        'Odd' => $Odd,
        'parity' => $parity,
        'even' => $even,
        'odd' => $odd,
        'eqParity' => $eqParity,
        'ordParity' => $ordParity,
        'showParity' => $showParity,
        'boundedParity' => $boundedParity,
        'semiringParity' => $semiringParity,
        'ringParity' => $ringParity,
        'commutativeRingParity' => $commutativeRingParity,
        'euclideanRingParity' => $euclideanRingParity,
        'divisionRingParity' => $divisionRingParity,
        'toNumber' => $__foreign['toNumber'],
        'toStringAs' => $__foreign['toStringAs'],
        'quot' => $__foreign['quot'],
        'rem' => $__foreign['rem'],
        'pow' => $__foreign['pow']
    ];
})();
