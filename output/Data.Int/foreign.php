<?php

$exports['fromNumberImpl'] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($n) use (&$just, &$nothing) {
      /* jshint bitwise: false */
      return ($n | 0) === $n ? $just($n) : $nothing;
    };
  };
};

$exports['toNumber'] = function ($n) {
  return $n;
};

$exports['fromStringAsImpl'] = function ($just) {
  return function ($nothing) use (&$jsut) {
    return function ($radix) use (&$just, &$nothing) {
      $digits;
      if ($radix < 11) {
        $digits = "[0-" . ($radix - 1) . "]";
      } else if ($radix === 11) {
        $digits = "[0-9a]";
      } else {
        $digits = "[0-9a-" . chr(86 + $radix) . "]";
      }
      $pattern = "^[\\+\\-]?" . $digits . "+$"; //, "i";

      return function ($s) use (&$pattern, &$radix, &$nothing, &$just) {
        /* jshint bitwise: false */
        if (preg_match($pattern, $s)) {
          $i = intval($s, $radix);
          return ($i | 0) === $i ? $just($i) : $nothing;
        } else {
          return $nothing;
        }
      };
    };
  };
};

$exports['toStringAs'] = function ($radix) {
  return function ($i) use (&$radix) {
    return intval($i, $radix);
  };
};


$exports['quot'] = function ($x) {
  return function ($y) use (&$x) {
    return $x / $y | 0;
  };
};

$exports['rem'] = function ($x) {
  return function ($y) use (&$x) {
    return $x % $y;
  };
};

$exports['pow'] = function ($x) {
  return function ($y) use (&$x) {
    return pow($x,$y) | 0;
  };
};
