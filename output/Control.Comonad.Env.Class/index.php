<?php
require_once(dirname(__FILE__) . '/../Control.Comonad/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad.Env.Trans/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
$PS__Control_Comonad_Env_Class = (function ()  use (&$PS__Data_Tuple, &$PS__Control_Comonad_Env_Trans) {
    $ComonadAsk = function ($Comonad0, $ask) {
        return [
            'Comonad0' => $Comonad0,
            'ask' => $ask
        ];
    };
    $ComonadEnv = function ($ComonadAsk0, $local) {
        return [
            'ComonadAsk0' => $ComonadAsk0,
            'local' => $local
        ];
    };
    $local = function ($dict) {
        return $dict['local'];
    };
    $comonadAskTuple = $ComonadAsk(function ()  use (&$PS__Data_Tuple) {
        return $PS__Data_Tuple['comonadTuple'];
    }, $PS__Data_Tuple['fst']);
    $comonadEnvTuple = $ComonadEnv(function ()  use (&$comonadAskTuple) {
        return $comonadAskTuple;
    }, function ($f)  use (&$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Tuple, &$f) {
            return $PS__Data_Tuple['Tuple']['constructor']($f($v['value0']), $v['value1']);
        };
    });
    $comonadAskEnvT = function ($dictComonad)  use (&$ComonadAsk, &$PS__Control_Comonad_Env_Trans, &$PS__Data_Tuple) {
        return $ComonadAsk(function ()  use (&$PS__Control_Comonad_Env_Trans, &$dictComonad) {
            return $PS__Control_Comonad_Env_Trans['comonadEnvT']($dictComonad);
        }, function ($v)  use (&$PS__Data_Tuple) {
            return $PS__Data_Tuple['fst']($v);
        });
    };
    $comonadEnvEnvT = function ($dictComonad)  use (&$ComonadEnv, &$comonadAskEnvT, &$PS__Data_Tuple) {
        return $ComonadEnv(function ()  use (&$comonadAskEnvT, &$dictComonad) {
            return $comonadAskEnvT($dictComonad);
        }, function ($f)  use (&$PS__Data_Tuple) {
            return function ($v)  use (&$PS__Data_Tuple, &$f) {
                return $PS__Data_Tuple['Tuple']['constructor']($f($v['value0']), $v['value1']);
            };
        });
    };
    $ask = function ($dict) {
        return $dict['ask'];
    };
    $asks = function ($dictComonadEnv)  use (&$ask) {
        return function ($f)  use (&$ask, &$dictComonadEnv) {
            return function ($x)  use (&$f, &$ask, &$dictComonadEnv) {
                return $f($ask($dictComonadEnv['ComonadAsk0']())($x));
            };
        };
    };
    return [
        'ask' => $ask,
        'local' => $local,
        'ComonadAsk' => $ComonadAsk,
        'asks' => $asks,
        'ComonadEnv' => $ComonadEnv,
        'comonadAskTuple' => $comonadAskTuple,
        'comonadEnvTuple' => $comonadEnvTuple,
        'comonadAskEnvT' => $comonadAskEnvT,
        'comonadEnvEnvT' => $comonadEnvEnvT
    ];
})();
