<?php

$exports['and'] = function ($n1) {
  return function ($n2) {
    /* jshint bitwise: false */
    return n1 & n2;
  };
};

$exports['or'] = function ($n1) {
  return function ($n2) {
    /* jshint bitwise: false */
    return n1 | n2;
  };
};

$exports['xor'] = function ($n1) {
  return function ($n2) {
    /* jshint bitwise: false */
    return n1 ^ n2;
  };
};

$exports['shl'] = function ($n1) {
  return function ($n2) {
    /* jshint bitwise: false */
    return n1 << n2;
  };
};

$exports['shr'] = function ($n1) {
  return function ($n2) {
    /* jshint bitwise: false */
    return n1 >> n2;
  };
};

$exports['zshr'] = function ($n1) {
  return function ($n2) {
    if($n2 == 0) return $n1;
    return ($n1 >> $n2) & ~(1<<(8*PHP_INT_SIZE-1)>>($n2-1));
  };
};

$exports['complement'] = function ($n) {
  /* jshint bitwise: $false */
  return ~n;
};
