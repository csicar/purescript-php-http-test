<?php
$PS__Data_Int_Bits = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    return [
        'and' => $__foreign['and'],
        'or' => $__foreign['or'],
        'xor' => $__foreign['xor'],
        'shl' => $__foreign['shl'],
        'shr' => $__foreign['shr'],
        'zshr' => $__foreign['zshr'],
        'complement' => $__foreign['complement']
    ];
})();
