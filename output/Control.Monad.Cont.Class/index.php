<?php
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Cont_Class = (function () {
    $MonadCont = function ($Monad0, $callCC) {
        return [
            'Monad0' => $Monad0,
            'callCC' => $callCC
        ];
    };
    $callCC = function ($dict) {
        return $dict['callCC'];
    };
    return [
        'MonadCont' => $MonadCont,
        'callCC' => $callCC
    ];
})();
