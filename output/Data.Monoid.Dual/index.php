<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Monoid_Dual = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Monoid, &$PS__Data_Functor, &$PS__Data_Eq, &$PS__Data_Ord, &$PS__Control_Apply, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Control_Monad) {
    $Dual = function ($x) {
        return $x;
    };
    $showDual = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Dual ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $semigroupDual = function ($dictSemigroup)  use (&$PS__Data_Semigroup) {
        return $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$PS__Data_Semigroup, &$dictSemigroup) {
            return function ($v1)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$v) {
                return $PS__Data_Semigroup['append']($dictSemigroup)($v1)($v);
            };
        });
    };
    $ordDual = function ($dictOrd) {
        return $dictOrd;
    };
    $monoidDual = function ($dictMonoid)  use (&$PS__Data_Monoid, &$semigroupDual) {
        return $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupDual, &$dictMonoid) {
            return $semigroupDual($dictMonoid['Semigroup0']());
        }, $PS__Data_Monoid['mempty']($dictMonoid));
    };
    $functorDual = $PS__Data_Functor['Functor'](function ($f) {
        return function ($m)  use (&$f) {
            return $f($m);
        };
    });
    $eqDual = function ($dictEq) {
        return $dictEq;
    };
    $eq1Dual = $PS__Data_Eq['Eq1'](function ($dictEq)  use (&$PS__Data_Eq, &$eqDual) {
        return $PS__Data_Eq['eq']($eqDual($dictEq));
    });
    $ord1Dual = $PS__Data_Ord['Ord1'](function ()  use (&$eq1Dual) {
        return $eq1Dual;
    }, function ($dictOrd)  use (&$PS__Data_Ord, &$ordDual) {
        return $PS__Data_Ord['compare']($ordDual($dictOrd));
    });
    $boundedDual = function ($dictBounded) {
        return $dictBounded;
    };
    $applyDual = $PS__Control_Apply['Apply'](function ()  use (&$functorDual) {
        return $functorDual;
    }, function ($v) {
        return function ($v1)  use (&$v) {
            return $v($v1);
        };
    });
    $bindDual = $PS__Control_Bind['Bind'](function ()  use (&$applyDual) {
        return $applyDual;
    }, function ($v) {
        return function ($f)  use (&$v) {
            return $f($v);
        };
    });
    $applicativeDual = $PS__Control_Applicative['Applicative'](function ()  use (&$applyDual) {
        return $applyDual;
    }, $Dual);
    $monadDual = $PS__Control_Monad['Monad'](function ()  use (&$applicativeDual) {
        return $applicativeDual;
    }, function ()  use (&$bindDual) {
        return $bindDual;
    });
    return [
        'Dual' => $Dual,
        'eqDual' => $eqDual,
        'eq1Dual' => $eq1Dual,
        'ordDual' => $ordDual,
        'ord1Dual' => $ord1Dual,
        'boundedDual' => $boundedDual,
        'showDual' => $showDual,
        'functorDual' => $functorDual,
        'applyDual' => $applyDual,
        'applicativeDual' => $applicativeDual,
        'bindDual' => $bindDual,
        'monadDual' => $monadDual,
        'semigroupDual' => $semigroupDual,
        'monoidDual' => $monoidDual
    ];
})();
