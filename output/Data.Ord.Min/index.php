<?php
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Ord_Min = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Ord, &$PS__Data_Newtype, &$PS__Data_Monoid, &$PS__Data_Bounded) {
    $Min = function ($x) {
        return $x;
    };
    $showMin = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Min ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $semigroupMin = function ($dictOrd)  use (&$PS__Data_Semigroup, &$PS__Data_Ord) {
        return $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$PS__Data_Ord, &$dictOrd) {
            return function ($v1)  use (&$PS__Data_Ord, &$dictOrd, &$v) {
                return $PS__Data_Ord['min']($dictOrd)($v)($v1);
            };
        });
    };
    $newtypeMin = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Min);
    $monoidMin = function ($dictBounded)  use (&$PS__Data_Monoid, &$semigroupMin, &$PS__Data_Bounded) {
        return $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupMin, &$dictBounded) {
            return $semigroupMin($dictBounded['Ord0']());
        }, $PS__Data_Bounded['top']($dictBounded));
    };
    $eqMin = function ($dictEq) {
        return $dictEq;
    };
    $ordMin = function ($dictOrd)  use (&$PS__Data_Ord, &$eqMin) {
        return $PS__Data_Ord['Ord'](function ()  use (&$eqMin, &$dictOrd) {
            return $eqMin($dictOrd['Eq0']());
        }, function ($v)  use (&$PS__Data_Ord, &$dictOrd) {
            return function ($v1)  use (&$PS__Data_Ord, &$dictOrd, &$v) {
                return $PS__Data_Ord['compare']($dictOrd)($v)($v1);
            };
        });
    };
    return [
        'Min' => $Min,
        'newtypeMin' => $newtypeMin,
        'eqMin' => $eqMin,
        'ordMin' => $ordMin,
        'semigroupMin' => $semigroupMin,
        'monoidMin' => $monoidMin,
        'showMin' => $showMin
    ];
})();
