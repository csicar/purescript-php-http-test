<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Except.Trans/index.php');
require_once(dirname(__FILE__) . '/../Data.Boolean/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Identity/index.php');
require_once(dirname(__FILE__) . '/../Foreign/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Foreign_Keys = (function ()  use (&$PS__Foreign, &$PS__Data_Function, &$PS__Data_Eq, &$PS__Control_Applicative, &$PS__Control_Monad_Except_Trans, &$PS__Data_Identity, &$PS__Data_Boolean) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $keys = function ($value)  use (&$PS__Foreign, &$PS__Data_Function, &$PS__Data_Eq, &$PS__Control_Applicative, &$PS__Control_Monad_Except_Trans, &$PS__Data_Identity, &$__foreign, &$PS__Data_Boolean) {
        if ($PS__Foreign['isNull']($value)) {
            return $PS__Data_Function['apply']($PS__Foreign['fail'])($PS__Foreign['TypeMismatch']['constructor']('object', 'null'));
        };
        if ($PS__Foreign['isUndefined']($value)) {
            return $PS__Data_Function['apply']($PS__Foreign['fail'])($PS__Foreign['TypeMismatch']['constructor']('object', 'undefined'));
        };
        if ($PS__Data_Eq['eq']($PS__Data_Eq['eqString'])($PS__Foreign['typeOf']($value))('object')) {
            return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($PS__Control_Monad_Except_Trans['applicativeExceptT']($PS__Data_Identity['monadIdentity'])))($__foreign['unsafeKeys']($value));
        };
        if ($PS__Data_Boolean['otherwise']) {
            return $PS__Data_Function['apply']($PS__Foreign['fail'])($PS__Foreign['TypeMismatch']['constructor']('object', $PS__Foreign['typeOf']($value)));
        };
        throw new \Exception('Failed pattern match at Foreign.Keys line 15, column 1 - line 15, column 36: ' . var_dump([ $value['constructor']['name'] ]));
    };
    return [
        'keys' => $keys
    ];
})();
