<?php

$exports['unsafeKeys'] = function ($value) {
  $keys = [];
  foreach ($value as $key => $prop) {
    $keys[] = $prop;
  }
  return $keys;
};
