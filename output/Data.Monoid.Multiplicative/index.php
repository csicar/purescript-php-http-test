<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Monoid_Multiplicative = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Semiring, &$PS__Data_Monoid, &$PS__Data_Functor, &$PS__Data_Eq, &$PS__Data_Ord, &$PS__Control_Apply, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Control_Monad) {
    $Multiplicative = function ($x) {
        return $x;
    };
    $showMultiplicative = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Multiplicative ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $semigroupMultiplicative = function ($dictSemiring)  use (&$PS__Data_Semigroup, &$PS__Data_Semiring) {
        return $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$PS__Data_Semiring, &$dictSemiring) {
            return function ($v1)  use (&$PS__Data_Semiring, &$dictSemiring, &$v) {
                return $PS__Data_Semiring['mul']($dictSemiring)($v)($v1);
            };
        });
    };
    $ordMultiplicative = function ($dictOrd) {
        return $dictOrd;
    };
    $monoidMultiplicative = function ($dictSemiring)  use (&$PS__Data_Monoid, &$semigroupMultiplicative, &$PS__Data_Semiring) {
        return $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupMultiplicative, &$dictSemiring) {
            return $semigroupMultiplicative($dictSemiring);
        }, $PS__Data_Semiring['one']($dictSemiring));
    };
    $functorMultiplicative = $PS__Data_Functor['Functor'](function ($f) {
        return function ($m)  use (&$f) {
            return $f($m);
        };
    });
    $eqMultiplicative = function ($dictEq) {
        return $dictEq;
    };
    $eq1Multiplicative = $PS__Data_Eq['Eq1'](function ($dictEq)  use (&$PS__Data_Eq, &$eqMultiplicative) {
        return $PS__Data_Eq['eq']($eqMultiplicative($dictEq));
    });
    $ord1Multiplicative = $PS__Data_Ord['Ord1'](function ()  use (&$eq1Multiplicative) {
        return $eq1Multiplicative;
    }, function ($dictOrd)  use (&$PS__Data_Ord, &$ordMultiplicative) {
        return $PS__Data_Ord['compare']($ordMultiplicative($dictOrd));
    });
    $boundedMultiplicative = function ($dictBounded) {
        return $dictBounded;
    };
    $applyMultiplicative = $PS__Control_Apply['Apply'](function ()  use (&$functorMultiplicative) {
        return $functorMultiplicative;
    }, function ($v) {
        return function ($v1)  use (&$v) {
            return $v($v1);
        };
    });
    $bindMultiplicative = $PS__Control_Bind['Bind'](function ()  use (&$applyMultiplicative) {
        return $applyMultiplicative;
    }, function ($v) {
        return function ($f)  use (&$v) {
            return $f($v);
        };
    });
    $applicativeMultiplicative = $PS__Control_Applicative['Applicative'](function ()  use (&$applyMultiplicative) {
        return $applyMultiplicative;
    }, $Multiplicative);
    $monadMultiplicative = $PS__Control_Monad['Monad'](function ()  use (&$applicativeMultiplicative) {
        return $applicativeMultiplicative;
    }, function ()  use (&$bindMultiplicative) {
        return $bindMultiplicative;
    });
    return [
        'Multiplicative' => $Multiplicative,
        'eqMultiplicative' => $eqMultiplicative,
        'eq1Multiplicative' => $eq1Multiplicative,
        'ordMultiplicative' => $ordMultiplicative,
        'ord1Multiplicative' => $ord1Multiplicative,
        'boundedMultiplicative' => $boundedMultiplicative,
        'showMultiplicative' => $showMultiplicative,
        'functorMultiplicative' => $functorMultiplicative,
        'applyMultiplicative' => $applyMultiplicative,
        'applicativeMultiplicative' => $applicativeMultiplicative,
        'bindMultiplicative' => $bindMultiplicative,
        'monadMultiplicative' => $monadMultiplicative,
        'semigroupMultiplicative' => $semigroupMultiplicative,
        'monoidMultiplicative' => $monoidMultiplicative
    ];
})();
