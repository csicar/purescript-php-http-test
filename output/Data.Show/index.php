<?php
require_once(dirname(__FILE__) . '/../Data.Symbol/index.php');
require_once(dirname(__FILE__) . '/../Record.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Type.Data.RowList/index.php');
$PS__Data_Show = (function ()  use (&$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Show = function ($show) {
        return [
            'show' => $show
        ];
    };
    $ShowRecordFields = function ($showRecordFields) {
        return [
            'showRecordFields' => $showRecordFields
        ];
    };
    $showString = $Show($__foreign['showStringImpl']);
    $showRecordFieldsNil = $ShowRecordFields(function ($v) {
        return function ($v1) {
            return [  ];
        };
    });
    $showRecordFields = function ($dict) {
        return $dict['showRecordFields'];
    };
    $showRecord = function ($dictRowToList)  use (&$Show, &$showRecordFields, &$PS__Type_Data_RowList, &$__foreign) {
        return function ($dictShowRecordFields)  use (&$Show, &$showRecordFields, &$PS__Type_Data_RowList, &$__foreign) {
            return $Show(function ($record)  use (&$showRecordFields, &$dictShowRecordFields, &$PS__Type_Data_RowList, &$__foreign) {
                $v = $showRecordFields($dictShowRecordFields)($PS__Type_Data_RowList['RLProxy']())($record);
                if (count($v) === 0) {
                    return '{}';
                };
                return $__foreign['join'](' ')([ '{', $__foreign['join'](', ')($v), '}' ]);
            });
        };
    };
    $showNumber = $Show($__foreign['showNumberImpl']);
    $showInt = $Show($__foreign['showIntImpl']);
    $showChar = $Show($__foreign['showCharImpl']);
    $showBoolean = $Show(function ($v) {
        if ($v) {
            return 'true';
        };
        if (!$v) {
            return 'false';
        };
        throw new \Exception('Failed pattern match at Data.Show line 20, column 1 - line 20, column 37: ' . var_dump([ $v['constructor']['name'] ]));
    });
    $show = function ($dict) {
        return $dict['show'];
    };
    $showArray = function ($dictShow)  use (&$Show, &$__foreign, &$show) {
        return $Show($__foreign['showArrayImpl']($show($dictShow)));
    };
    $showRecordFieldsCons = function ($dictIsSymbol)  use (&$ShowRecordFields, &$showRecordFields, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$__foreign, &$show) {
        return function ($dictShowRecordFields)  use (&$ShowRecordFields, &$showRecordFields, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$__foreign, &$show) {
            return function ($dictShow)  use (&$ShowRecordFields, &$showRecordFields, &$dictShowRecordFields, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$__foreign, &$show) {
                return $ShowRecordFields(function ($v)  use (&$showRecordFields, &$dictShowRecordFields, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$__foreign, &$show, &$dictShow) {
                    return function ($record)  use (&$showRecordFields, &$dictShowRecordFields, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$__foreign, &$show, &$dictShow) {
                        $tail = $showRecordFields($dictShowRecordFields)($PS__Type_Data_RowList['RLProxy']())($record);
                        $key = $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($PS__Data_Symbol['SProxy']());
                        $focus = $PS__Record_Unsafe['unsafeGet']($key)($record);
                        return $__foreign['cons']($__foreign['join'](': ')([ $key, $show($dictShow)($focus) ]))($tail);
                    };
                });
            };
        };
    };
    return [
        'Show' => $Show,
        'show' => $show,
        'ShowRecordFields' => $ShowRecordFields,
        'showRecordFields' => $showRecordFields,
        'showBoolean' => $showBoolean,
        'showInt' => $showInt,
        'showNumber' => $showNumber,
        'showChar' => $showChar,
        'showString' => $showString,
        'showArray' => $showArray,
        'showRecord' => $showRecord,
        'showRecordFieldsNil' => $showRecordFieldsNil,
        'showRecordFieldsCons' => $showRecordFieldsCons
    ];
})();
