<?php

$exports["showIntImpl"] = function ($n) {
  return strval($n);
};
$exports["showNumberImpl"] = function ($n) {
  $old = ini_get('serialize_precision');
  ini_set("serialize_precision", -1);
  $export = json_encode($n);
  if ($export == round($n)) {
    $str = $export.".0";
  } else {
    $str = $export;
  }
  ini_set('serialize_precision', $old);
  return $str;
};

$exports["showCharImpl"] = function ($c) {
  $code = ord($c);
  if ($code < 0x20 || $code === 0x7F) {
    switch ($c) {
      case "\x07": return "'\\a'";
      case "\b": return "'\\b'";
      case "\f": return "'\\f'";
      case "\n": return "'\\n'";
      case "\r": return "'\\r'";
      case "\t": return "'\\t'";
      case "\v": return "'\\v'";
    }
    return "'\\" + chr(10) + "'";
  }
  return $c === "'" || $c === "\\" ? "'\\" + $c + "'" : "'" + $c + "'";
};

$exports["showStringImpl"] = function ($s) {
  return addslashes($s);
  // TODO: correct version like this:
  // $l = count($s);
  // return "\"" + s.replace(
  //   /[\0-\x1F\x7F"\\]/g,
  //   function (c, i) {
  //     switch (c) {
  //       case "\"":
  //       case "\\":
  //         return "\\" + c;
  //       case "\x07": return "\\a";
  //       case "\b": return "\\b";
  //       case "\f": return "\\f";
  //       case "\n": return "\\n";
  //       case "\r": return "\\r";
  //       case "\t": return "\\t";
  //       case "\v": return "\\v";
  //     }
  //     var k = i + 1;
  //     var empty = k < l && s[k] >= "0" && s[k] <= "9" ? "\\&" : "";
  //     return "\\" + c.charCodeAt(0).toString(10) + empty;
  //   }
  // ) + "\"";
};

$exports["showArrayImpl"] = function ($f) {
  return function ($xs) use (&$f) {
    $ss = [];
    for ($i = 0, $l = count($xs); $i < $l; $i++) {
      $ss[$i] = $f($xs[$i]);
    }
    return "[" . implode(",", $ss) . "]";
  };
};

$exports["cons"] = function ($head) {
  return function ($tail) use (&$head) {
    return array_merge([$head], $tail);
  };
};

$exports["join"] = function ($separator) {
  return function ($xs) use (&$seperator) {
    return implode($separator, $xs);
  };
};
