<?php
require_once(dirname(__FILE__) . '/../Data.Either/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Regex/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Regex.Flags/index.php');
require_once(dirname(__FILE__) . '/../Partial.Unsafe/index.php');
$PS__Data_String_Regex_Unsafe = (function ()  use (&$PS__Partial_Unsafe, &$PS__Data_Either, &$PS__Data_String_Regex) {
    $unsafeRegex = function ($s)  use (&$PS__Partial_Unsafe, &$PS__Data_Either, &$PS__Data_String_Regex) {
        return function ($f)  use (&$PS__Partial_Unsafe, &$PS__Data_Either, &$PS__Data_String_Regex, &$s) {
            return $PS__Partial_Unsafe['unsafePartial'](function ($dictPartial)  use (&$PS__Data_Either) {
                return $PS__Data_Either['fromRight']($dictPartial);
            })($PS__Data_String_Regex['regex']($s)($f));
        };
    };
    return [
        'unsafeRegex' => $unsafeRegex
    ];
})();
