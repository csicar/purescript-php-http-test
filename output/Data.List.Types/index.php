<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad/index.php');
require_once(dirname(__FILE__) . '/../Control.Extend/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadPlus/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadZero/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.FoldableWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.FunctorWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.TraversableWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable1/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_List_Types = (function ()  use (&$PS__Data_Newtype, &$PS__Data_NonEmpty, &$PS__Data_Foldable, &$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$PS__Data_Monoid, &$PS__Data_Function, &$PS__Data_FoldableWithIndex, &$PS__Data_Tuple, &$PS__Data_Semiring, &$PS__Data_Ring, &$PS__Data_Functor, &$PS__Data_FunctorWithIndex, &$PS__Data_Show, &$PS__Data_Traversable, &$PS__Control_Category, &$PS__Control_Apply, &$PS__Control_Applicative, &$PS__Data_TraversableWithIndex, &$PS__Data_Unfoldable1, &$PS__Data_Unfoldable, &$PS__Control_Extend, &$PS__Data_Eq, &$PS__Data_HeytingAlgebra, &$PS__Data_Ord, &$PS__Data_Ordering, &$PS__Control_Comonad, &$PS__Control_Bind, &$PS__Control_Monad, &$PS__Control_Alt, &$PS__Control_Plus, &$PS__Control_Alternative, &$PS__Control_MonadZero, &$PS__Control_MonadPlus, &$PS__Data_Semigroup_Traversable) {
    $Nil = function () {
        return [
            'type' => 'Nil'
        ];
    };
    $Cons = [
        'constructor' => function ($value0, $value1) {
            return [
                'type' => 'Cons',
                'value0' => $value0,
                'value1' => $value1
            ];
        },
        'create' => function ($value0)  use (&$Cons) {
            return function ($value1)  use (&$Cons, &$value0) {
                return $Cons['constructor']($value0, $value1);
            };
        }
    ];
    $NonEmptyList = function ($x) {
        return $x;
    };
    $toList = function ($v)  use (&$Cons) {
        return $Cons['constructor']($v['value0'], $v['value1']);
    };
    $newtypeNonEmptyList = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $NonEmptyList);
    $nelCons = function ($a)  use (&$PS__Data_NonEmpty, &$Cons) {
        return function ($v)  use (&$PS__Data_NonEmpty, &$a, &$Cons) {
            return $PS__Data_NonEmpty['NonEmpty']['constructor']($a, $Cons['constructor']($v['value0'], $v['value1']));
        };
    };
    $foldableList = $PS__Data_Foldable['Foldable'](function ($dictMonoid)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
        return function ($f)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Monoid) {
            return $PS__Data_Foldable['foldl']($foldableList)(function ($acc)  use (&$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$dictMonoid, &$f) {
                return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($acc))($f);
            })($PS__Data_Monoid['mempty']($dictMonoid));
        };
    }, function ($f) {
        $go = function ($_dollar_copy_b)  use (&$f) {
            return function ($_dollar_copy_v)  use (&$_dollar_copy_b, &$f) {
                $_dollar_tco_var_b = $_dollar_copy_b;
                $_dollar_tco_done = false;
                $_dollar_tco_result = NULL;
                $_dollar_tco_loop = function ($b, $v)  use (&$_dollar_tco_done, &$_dollar_tco_var_b, &$f, &$_dollar_copy_v) {
                    if ($v['type'] === 'Nil') {
                        $_dollar_tco_done = true;
                        return $b;
                    };
                    if ($v['type'] === 'Cons') {
                        $_dollar_tco_var_b = $f($b)($v['value0']);
                        $_dollar_copy_v = $v['value1'];
                        return;
                    };
                    throw new \Exception('Failed pattern match at Data.List.Types line 81, column 12 - line 83, column 30: ' . var_dump([ $v['constructor']['name'] ]));
                };
                while (!$_dollar_tco_done) {
                    $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_b, $_dollar_copy_v);
                };
                return $_dollar_tco_result;
            };
        };
        return $go;
    }, function ($f)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Cons, &$Nil, &$PS__Control_Semigroupoid) {
        return function ($b)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Cons, &$Nil, &$PS__Control_Semigroupoid, &$f) {
            $rev = $PS__Data_Foldable['foldl']($foldableList)($PS__Data_Function['flip']($Cons['create']))($Nil());
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Foldable['foldl']($foldableList)($PS__Data_Function['flip']($f))($b))($rev);
        };
    });
    $foldableNonEmptyList = $PS__Data_NonEmpty['foldableNonEmpty']($foldableList);
    $foldableWithIndexList = $PS__Data_FoldableWithIndex['FoldableWithIndex'](function ()  use (&$foldableList) {
        return $foldableList;
    }, function ($dictMonoid)  use (&$PS__Data_FoldableWithIndex, &$foldableWithIndexList, &$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
        return function ($f)  use (&$PS__Data_FoldableWithIndex, &$foldableWithIndexList, &$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Monoid) {
            return $PS__Data_FoldableWithIndex['foldlWithIndex']($foldableWithIndexList)(function ($i)  use (&$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$dictMonoid, &$f) {
                return function ($acc)  use (&$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$dictMonoid, &$f, &$i) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($acc))($f($i));
                };
            })($PS__Data_Monoid['mempty']($dictMonoid));
        };
    }, function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Tuple, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Semiring) {
        return function ($acc)  use (&$PS__Control_Semigroupoid, &$PS__Data_Tuple, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Semiring, &$f) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Tuple['snd'])($PS__Data_Foldable['foldl']($foldableList)(function ($v)  use (&$PS__Data_Tuple, &$PS__Data_Semiring, &$f) {
                return function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Semiring, &$v, &$f) {
                    return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($v['value0'])(1), $f($v['value0'])($v['value1'])($a));
                };
            })($PS__Data_Tuple['Tuple']['constructor'](0, $acc)));
        };
    }, function ($f)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Tuple, &$PS__Data_Semiring, &$Cons, &$Nil, &$PS__Data_Function, &$PS__Data_Ring) {
        return function ($b)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Tuple, &$PS__Data_Semiring, &$Cons, &$Nil, &$PS__Data_Function, &$PS__Data_Ring, &$f) {
            return function ($xs)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Tuple, &$PS__Data_Semiring, &$Cons, &$Nil, &$PS__Data_Function, &$PS__Data_Ring, &$f, &$b) {
                $v = (function ()  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Tuple, &$PS__Data_Semiring, &$Cons, &$Nil, &$xs) {
                    $rev = $PS__Data_Foldable['foldl']($foldableList)(function ($v1)  use (&$PS__Data_Tuple, &$PS__Data_Semiring, &$Cons) {
                        return function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Semiring, &$v1, &$Cons) {
                            return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($v1['value0'])(1), $Cons['constructor']($a, $v1['value1']));
                        };
                    });
                    return $rev($PS__Data_Tuple['Tuple']['constructor'](0, $Nil()))($xs);
                })();
                return $PS__Data_Function['apply']($PS__Data_Tuple['snd'])($PS__Data_Foldable['foldl']($foldableList)(function ($v1)  use (&$PS__Data_Tuple, &$PS__Data_Ring, &$f) {
                    return function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Ring, &$v1, &$f) {
                        return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v1['value0'])(1), $f($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v1['value0'])(1))($a)($v1['value1']));
                    };
                })($PS__Data_Tuple['Tuple']['constructor']($v['value0'], $b))($v['value1']));
            };
        };
    });
    $functorList = $PS__Data_Functor['Functor'](function ($f)  use (&$PS__Data_Foldable, &$foldableList, &$Cons, &$Nil) {
        return $PS__Data_Foldable['foldr']($foldableList)(function ($x)  use (&$Cons, &$f) {
            return function ($acc)  use (&$Cons, &$f, &$x) {
                return $Cons['constructor']($f($x), $acc);
            };
        })($Nil());
    });
    $functorNonEmptyList = $PS__Data_NonEmpty['functorNonEmpty']($functorList);
    $functorWithIndexList = $PS__Data_FunctorWithIndex['FunctorWithIndex'](function ()  use (&$functorList) {
        return $functorList;
    }, function ($f)  use (&$PS__Data_FoldableWithIndex, &$foldableWithIndexList, &$Cons, &$Nil) {
        return $PS__Data_FoldableWithIndex['foldrWithIndex']($foldableWithIndexList)(function ($i)  use (&$Cons, &$f) {
            return function ($x)  use (&$Cons, &$f, &$i) {
                return function ($acc)  use (&$Cons, &$f, &$i, &$x) {
                    return $Cons['constructor']($f($i)($x), $acc);
                };
            };
        })($Nil());
    });
    $semigroupList = $PS__Data_Semigroup['Semigroup'](function ($xs)  use (&$PS__Data_Foldable, &$foldableList, &$Cons) {
        return function ($ys)  use (&$PS__Data_Foldable, &$foldableList, &$Cons, &$xs) {
            return $PS__Data_Foldable['foldr']($foldableList)($Cons['create'])($ys)($xs);
        };
    });
    $monoidList = $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupList) {
        return $semigroupList;
    }, $Nil());
    $semigroupNonEmptyList = $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$PS__Data_NonEmpty, &$PS__Data_Semigroup, &$semigroupList, &$toList) {
        return function ($as__prime)  use (&$PS__Data_NonEmpty, &$v, &$PS__Data_Semigroup, &$semigroupList, &$toList) {
            return $PS__Data_NonEmpty['NonEmpty']['constructor']($v['value0'], $PS__Data_Semigroup['append']($semigroupList)($v['value1'])($toList($as__prime)));
        };
    });
    $showList = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Monoid, &$PS__Data_Functor, &$functorList) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Monoid, &$PS__Data_Functor, &$functorList, &$PS__Data_Show, &$dictShow) {
            if ($v['type'] === 'Nil') {
                return 'Nil';
            };
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Foldable['intercalate']($foldableList)($PS__Data_Monoid['monoidString'])(' : ')($PS__Data_Functor['map']($functorList)($PS__Data_Show['show']($dictShow))($v)))(' : Nil)'));
        });
    };
    $showNonEmptyList = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_NonEmpty, &$showList) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$PS__Data_NonEmpty, &$dictShow, &$showList) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(NonEmptyList ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_NonEmpty['showNonEmpty']($dictShow)($showList($dictShow)))($v))(')'));
        });
    };
    $traversableList = $PS__Data_Traversable['Traversable'](function ()  use (&$foldableList) {
        return $foldableList;
    }, function ()  use (&$functorList) {
        return $functorList;
    }, function ($dictApplicative)  use (&$PS__Data_Traversable, &$traversableList, &$PS__Control_Category) {
        return $PS__Data_Traversable['traverse']($traversableList)($dictApplicative)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    }, function ($dictApplicative)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Cons, &$Nil, &$PS__Control_Apply, &$PS__Control_Applicative) {
        return function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictApplicative, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Cons, &$Nil, &$PS__Control_Apply, &$PS__Control_Applicative) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Foldable['foldl']($foldableList)($PS__Data_Function['flip']($Cons['create']))($Nil())))($PS__Data_Foldable['foldl']($foldableList)(function ($acc)  use (&$PS__Control_Semigroupoid, &$PS__Control_Apply, &$dictApplicative, &$PS__Data_Function, &$Cons, &$f) {
                return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Apply['lift2']($dictApplicative['Apply0']())($PS__Data_Function['flip']($Cons['create']))($acc))($f);
            })($PS__Control_Applicative['pure']($dictApplicative)($Nil())));
        };
    });
    $traversableNonEmptyList = $PS__Data_NonEmpty['traversableNonEmpty']($traversableList);
    $traversableWithIndexList = $PS__Data_TraversableWithIndex['TraversableWithIndex'](function ()  use (&$foldableWithIndexList) {
        return $foldableWithIndexList;
    }, function ()  use (&$functorWithIndexList) {
        return $functorWithIndexList;
    }, function ()  use (&$traversableList) {
        return $traversableList;
    }, function ($dictApplicative)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Cons, &$Nil, &$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_FoldableWithIndex, &$foldableWithIndexList, &$PS__Control_Apply, &$PS__Control_Applicative) {
        return function ($f)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Cons, &$Nil, &$PS__Control_Semigroupoid, &$PS__Data_Functor, &$dictApplicative, &$PS__Data_FoldableWithIndex, &$foldableWithIndexList, &$PS__Control_Apply, &$PS__Control_Applicative) {
            $rev = $PS__Data_Foldable['foldl']($foldableList)($PS__Data_Function['flip']($Cons['create']))($Nil());
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($rev))($PS__Data_FoldableWithIndex['foldlWithIndex']($foldableWithIndexList)(function ($i)  use (&$PS__Control_Semigroupoid, &$PS__Control_Apply, &$dictApplicative, &$PS__Data_Function, &$Cons, &$f) {
                return function ($acc)  use (&$PS__Control_Semigroupoid, &$PS__Control_Apply, &$dictApplicative, &$PS__Data_Function, &$Cons, &$f, &$i) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Apply['lift2']($dictApplicative['Apply0']())($PS__Data_Function['flip']($Cons['create']))($acc))($f($i));
                };
            })($PS__Control_Applicative['pure']($dictApplicative)($Nil())));
        };
    });
    $unfoldable1List = $PS__Data_Unfoldable1['Unfoldable1'](function ($f)  use (&$Cons, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Nil) {
        return function ($b)  use (&$f, &$Cons, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Nil) {
            $go = function ($_dollar_copy_source)  use (&$f, &$Cons, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Nil) {
                return function ($_dollar_copy_memo)  use (&$_dollar_copy_source, &$f, &$Cons, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Nil) {
                    $_dollar_tco_var_source = $_dollar_copy_source;
                    $_dollar_tco_done = false;
                    $_dollar_tco_result = NULL;
                    $_dollar_tco_loop = function ($source, $memo)  use (&$f, &$_dollar_tco_var_source, &$_dollar_copy_memo, &$Cons, &$_dollar_tco_done, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Nil) {
                        $v = $f($source);
                        if ($v['value1']['type'] === 'Just') {
                            $_dollar_tco_var_source = $v['value1']['value0'];
                            $_dollar_copy_memo = $Cons['constructor']($v['value0'], $memo);
                            return;
                        };
                        if ($v['value1']['type'] === 'Nothing') {
                            $_dollar_tco_done = true;
                            return $PS__Data_Foldable['foldl']($foldableList)($PS__Data_Function['flip']($Cons['create']))($Nil())($Cons['constructor']($v['value0'], $memo));
                        };
                        throw new \Exception('Failed pattern match at Data.List.Types line 105, column 22 - line 107, column 61: ' . var_dump([ $v['constructor']['name'] ]));
                    };
                    while (!$_dollar_tco_done) {
                        $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_source, $_dollar_copy_memo);
                    };
                    return $_dollar_tco_result;
                };
            };
            return $go($b)($Nil());
        };
    });
    $unfoldableList = $PS__Data_Unfoldable['Unfoldable'](function ()  use (&$unfoldable1List) {
        return $unfoldable1List;
    }, function ($f)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Cons, &$Nil) {
        return function ($b)  use (&$f, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Cons, &$Nil) {
            $go = function ($_dollar_copy_source)  use (&$f, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Cons, &$Nil) {
                return function ($_dollar_copy_memo)  use (&$_dollar_copy_source, &$f, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Cons, &$Nil) {
                    $_dollar_tco_var_source = $_dollar_copy_source;
                    $_dollar_tco_done = false;
                    $_dollar_tco_result = NULL;
                    $_dollar_tco_loop = function ($source, $memo)  use (&$f, &$_dollar_tco_done, &$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$Cons, &$Nil, &$_dollar_tco_var_source, &$_dollar_copy_memo) {
                        $v = $f($source);
                        if ($v['type'] === 'Nothing') {
                            $_dollar_tco_done = true;
                            return $PS__Data_Foldable['foldl']($foldableList)($PS__Data_Function['flip']($Cons['create']))($Nil())($memo);
                        };
                        if ($v['type'] === 'Just') {
                            $_dollar_tco_var_source = $v['value0']['value1'];
                            $_dollar_copy_memo = $Cons['constructor']($v['value0']['value0'], $memo);
                            return;
                        };
                        throw new \Exception('Failed pattern match at Data.List.Types line 112, column 22 - line 114, column 52: ' . var_dump([ $v['constructor']['name'] ]));
                    };
                    while (!$_dollar_tco_done) {
                        $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_source, $_dollar_copy_memo);
                    };
                    return $_dollar_tco_result;
                };
            };
            return $go($b)($Nil());
        };
    });
    $foldable1NonEmptyList = $PS__Data_NonEmpty['foldable1NonEmpty']($foldableList);
    $extendNonEmptyList = $PS__Control_Extend['Extend'](function ()  use (&$functorNonEmptyList) {
        return $functorNonEmptyList;
    }, function ($f)  use (&$Cons, &$PS__Data_NonEmpty, &$PS__Data_Foldable, &$foldableList, &$Nil) {
        return function ($v)  use (&$Cons, &$f, &$PS__Data_NonEmpty, &$PS__Data_Foldable, &$foldableList, &$Nil) {
            $go = function ($a)  use (&$Cons, &$f, &$PS__Data_NonEmpty) {
                return function ($v1)  use (&$Cons, &$f, &$PS__Data_NonEmpty, &$a) {
                    return [
                        'val' => $Cons['constructor']($f($PS__Data_NonEmpty['NonEmpty']['constructor']($a, $v1['acc'])), $v1['val']),
                        'acc' => $Cons['constructor']($a, $v1['acc'])
                    ];
                };
            };
            return $PS__Data_NonEmpty['NonEmpty']['constructor']($f($v), ($PS__Data_Foldable['foldr']($foldableList)($go)([
                'val' => $Nil(),
                'acc' => $Nil()
            ])($v['value1']))['val']);
        };
    });
    $extendList = $PS__Control_Extend['Extend'](function ()  use (&$functorList) {
        return $functorList;
    }, function ($f)  use (&$Nil, &$Cons, &$PS__Data_Foldable, &$foldableList) {
        return function ($v)  use (&$Nil, &$Cons, &$f, &$PS__Data_Foldable, &$foldableList) {
            if ($v['type'] === 'Nil') {
                return $Nil();
            };
            if ($v['type'] === 'Cons') {
                $go = function ($a__prime)  use (&$Cons, &$f) {
                    return function ($v1)  use (&$Cons, &$a__prime, &$f) {
                        $acc__prime = $Cons['constructor']($a__prime, $v1['acc']);
                        return [
                            'val' => $Cons['constructor']($f($acc__prime), $v1['val']),
                            'acc' => $acc__prime
                        ];
                    };
                };
                return $Cons['constructor']($f($v), ($PS__Data_Foldable['foldr']($foldableList)($go)([
                    'val' => $Nil(),
                    'acc' => $Nil()
                ])($v['value1']))['val']);
            };
            throw new \Exception('Failed pattern match at Data.List.Types line 152, column 1 - line 152, column 35: ' . var_dump([ $f['constructor']['name'], $v['constructor']['name'] ]));
        };
    });
    $eq1List = $PS__Data_Eq['Eq1'](function ($dictEq)  use (&$PS__Data_Function, &$PS__Data_HeytingAlgebra, &$PS__Data_Eq) {
        return function ($xs)  use (&$PS__Data_Function, &$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$dictEq) {
            return function ($ys)  use (&$PS__Data_Function, &$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$dictEq, &$xs) {
                $go = function ($v)  use (&$PS__Data_Function, &$go, &$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$dictEq) {
                    return function ($v1)  use (&$v, &$PS__Data_Function, &$go, &$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$dictEq) {
                        return function ($v2)  use (&$v, &$v1, &$PS__Data_Function, &$go, &$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$dictEq) {
                            if (!$v2) {
                                return false;
                            };
                            if ($v['type'] === 'Nil' && $v1['type'] === 'Nil') {
                                return $v2;
                            };
                            if ($v['type'] === 'Cons' && $v1['type'] === 'Cons') {
                                return $PS__Data_Function['apply']($go($v['value1'])($v1['value1']))($PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($v2)($PS__Data_Eq['eq']($dictEq)($v1['value0'])($v['value0'])));
                            };
                            return false;
                        };
                    };
                };
                return $go($xs)($ys)(true);
            };
        };
    });
    $eqList = function ($dictEq)  use (&$PS__Data_Eq, &$eq1List) {
        return $PS__Data_Eq['Eq']($PS__Data_Eq['eq1']($eq1List)($dictEq));
    };
    $eqNonEmptyList = function ($dictEq)  use (&$PS__Data_NonEmpty, &$eq1List) {
        return $PS__Data_NonEmpty['eqNonEmpty']($eq1List)($dictEq);
    };
    $ord1List = $PS__Data_Ord['Ord1'](function ()  use (&$eq1List) {
        return $eq1List;
    }, function ($dictOrd)  use (&$PS__Data_Ordering, &$PS__Data_Ord) {
        return function ($xs)  use (&$PS__Data_Ordering, &$PS__Data_Ord, &$dictOrd) {
            return function ($ys)  use (&$PS__Data_Ordering, &$PS__Data_Ord, &$dictOrd, &$xs) {
                $go = function ($_dollar_copy_v)  use (&$PS__Data_Ordering, &$PS__Data_Ord, &$dictOrd) {
                    return function ($_dollar_copy_v1)  use (&$_dollar_copy_v, &$PS__Data_Ordering, &$PS__Data_Ord, &$dictOrd) {
                        $_dollar_tco_var_v = $_dollar_copy_v;
                        $_dollar_tco_done = false;
                        $_dollar_tco_result = NULL;
                        $_dollar_tco_loop = function ($v, $v1)  use (&$_dollar_tco_done, &$PS__Data_Ordering, &$PS__Data_Ord, &$dictOrd, &$_dollar_tco_var_v, &$_dollar_copy_v1) {
                            if ($v['type'] === 'Nil' && $v1['type'] === 'Nil') {
                                $_dollar_tco_done = true;
                                return $PS__Data_Ordering['EQ']();
                            };
                            if ($v['type'] === 'Nil') {
                                $_dollar_tco_done = true;
                                return $PS__Data_Ordering['LT']();
                            };
                            if ($v1['type'] === 'Nil') {
                                $_dollar_tco_done = true;
                                return $PS__Data_Ordering['GT']();
                            };
                            if ($v['type'] === 'Cons' && $v1['type'] === 'Cons') {
                                $v2 = $PS__Data_Ord['compare']($dictOrd)($v['value0'])($v1['value0']);
                                if ($v2['type'] === 'EQ') {
                                    $_dollar_tco_var_v = $v['value1'];
                                    $_dollar_copy_v1 = $v1['value1'];
                                    return;
                                };
                                $_dollar_tco_done = true;
                                return $v2;
                            };
                            throw new \Exception('Failed pattern match at Data.List.Types line 55, column 5 - line 55, column 20: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
                        };
                        while (!$_dollar_tco_done) {
                            $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_v, $_dollar_copy_v1);
                        };
                        return $_dollar_tco_result;
                    };
                };
                return $go($xs)($ys);
            };
        };
    });
    $ordList = function ($dictOrd)  use (&$PS__Data_Ord, &$eqList, &$ord1List) {
        return $PS__Data_Ord['Ord'](function ()  use (&$eqList, &$dictOrd) {
            return $eqList($dictOrd['Eq0']());
        }, $PS__Data_Ord['compare1']($ord1List)($dictOrd));
    };
    $ordNonEmptyList = function ($dictOrd)  use (&$PS__Data_NonEmpty, &$ord1List) {
        return $PS__Data_NonEmpty['ordNonEmpty']($ord1List)($dictOrd);
    };
    $comonadNonEmptyList = $PS__Control_Comonad['Comonad'](function ()  use (&$extendNonEmptyList) {
        return $extendNonEmptyList;
    }, function ($v) {
        return $v['value0'];
    });
    $applyList = $PS__Control_Apply['Apply'](function ()  use (&$functorList) {
        return $functorList;
    }, function ($v)  use (&$Nil, &$PS__Data_Semigroup, &$semigroupList, &$PS__Data_Functor, &$functorList, &$PS__Control_Apply, &$applyList) {
        return function ($v1)  use (&$v, &$Nil, &$PS__Data_Semigroup, &$semigroupList, &$PS__Data_Functor, &$functorList, &$PS__Control_Apply, &$applyList) {
            if ($v['type'] === 'Nil') {
                return $Nil();
            };
            if ($v['type'] === 'Cons') {
                return $PS__Data_Semigroup['append']($semigroupList)($PS__Data_Functor['map']($functorList)($v['value0'])($v1))($PS__Control_Apply['apply']($applyList)($v['value1'])($v1));
            };
            throw new \Exception('Failed pattern match at Data.List.Types line 127, column 1 - line 127, column 33: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
        };
    });
    $applyNonEmptyList = $PS__Control_Apply['Apply'](function ()  use (&$functorNonEmptyList) {
        return $functorNonEmptyList;
    }, function ($v)  use (&$PS__Data_NonEmpty, &$PS__Data_Semigroup, &$semigroupList, &$PS__Control_Apply, &$applyList, &$Cons, &$Nil) {
        return function ($v1)  use (&$PS__Data_NonEmpty, &$v, &$PS__Data_Semigroup, &$semigroupList, &$PS__Control_Apply, &$applyList, &$Cons, &$Nil) {
            return $PS__Data_NonEmpty['NonEmpty']['constructor']($v['value0']($v1['value0']), $PS__Data_Semigroup['append']($semigroupList)($PS__Control_Apply['apply']($applyList)($v['value1'])($Cons['constructor']($v1['value0'], $Nil())))($PS__Control_Apply['apply']($applyList)($Cons['constructor']($v['value0'], $v['value1']))($v1['value1'])));
        };
    });
    $bindList = $PS__Control_Bind['Bind'](function ()  use (&$applyList) {
        return $applyList;
    }, function ($v)  use (&$Nil, &$PS__Data_Semigroup, &$semigroupList, &$PS__Control_Bind, &$bindList) {
        return function ($v1)  use (&$v, &$Nil, &$PS__Data_Semigroup, &$semigroupList, &$PS__Control_Bind, &$bindList) {
            if ($v['type'] === 'Nil') {
                return $Nil();
            };
            if ($v['type'] === 'Cons') {
                return $PS__Data_Semigroup['append']($semigroupList)($v1($v['value0']))($PS__Control_Bind['bind']($bindList)($v['value1'])($v1));
            };
            throw new \Exception('Failed pattern match at Data.List.Types line 134, column 1 - line 134, column 31: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
        };
    });
    $bindNonEmptyList = $PS__Control_Bind['Bind'](function ()  use (&$applyNonEmptyList) {
        return $applyNonEmptyList;
    }, function ($v)  use (&$PS__Data_NonEmpty, &$PS__Data_Semigroup, &$semigroupList, &$PS__Control_Bind, &$bindList, &$PS__Control_Semigroupoid, &$toList) {
        return function ($f)  use (&$v, &$PS__Data_NonEmpty, &$PS__Data_Semigroup, &$semigroupList, &$PS__Control_Bind, &$bindList, &$PS__Control_Semigroupoid, &$toList) {
            $v1 = $f($v['value0']);
            return $PS__Data_NonEmpty['NonEmpty']['constructor']($v1['value0'], $PS__Data_Semigroup['append']($semigroupList)($v1['value1'])($PS__Control_Bind['bind']($bindList)($v['value1'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($toList)($f))));
        };
    });
    $applicativeList = $PS__Control_Applicative['Applicative'](function ()  use (&$applyList) {
        return $applyList;
    }, function ($a)  use (&$Cons, &$Nil) {
        return $Cons['constructor']($a, $Nil());
    });
    $monadList = $PS__Control_Monad['Monad'](function ()  use (&$applicativeList) {
        return $applicativeList;
    }, function ()  use (&$bindList) {
        return $bindList;
    });
    $altNonEmptyList = $PS__Control_Alt['Alt'](function ()  use (&$functorNonEmptyList) {
        return $functorNonEmptyList;
    }, $PS__Data_Semigroup['append']($semigroupNonEmptyList));
    $altList = $PS__Control_Alt['Alt'](function ()  use (&$functorList) {
        return $functorList;
    }, $PS__Data_Semigroup['append']($semigroupList));
    $plusList = $PS__Control_Plus['Plus'](function ()  use (&$altList) {
        return $altList;
    }, $Nil());
    $alternativeList = $PS__Control_Alternative['Alternative'](function ()  use (&$applicativeList) {
        return $applicativeList;
    }, function ()  use (&$plusList) {
        return $plusList;
    });
    $monadZeroList = $PS__Control_MonadZero['MonadZero'](function ()  use (&$alternativeList) {
        return $alternativeList;
    }, function ()  use (&$monadList) {
        return $monadList;
    });
    $monadPlusList = $PS__Control_MonadPlus['MonadPlus'](function ()  use (&$monadZeroList) {
        return $monadZeroList;
    });
    $applicativeNonEmptyList = $PS__Control_Applicative['Applicative'](function ()  use (&$applyNonEmptyList) {
        return $applyNonEmptyList;
    }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($NonEmptyList)($PS__Data_NonEmpty['singleton']($plusList)));
    $monadNonEmptyList = $PS__Control_Monad['Monad'](function ()  use (&$applicativeNonEmptyList) {
        return $applicativeNonEmptyList;
    }, function ()  use (&$bindNonEmptyList) {
        return $bindNonEmptyList;
    });
    $traversable1NonEmptyList = $PS__Data_Semigroup_Traversable['Traversable1'](function ()  use (&$foldable1NonEmptyList) {
        return $foldable1NonEmptyList;
    }, function ()  use (&$traversableNonEmptyList) {
        return $traversableNonEmptyList;
    }, function ($dictApply)  use (&$PS__Data_Semigroup_Traversable, &$traversable1NonEmptyList, &$PS__Control_Category) {
        return $PS__Data_Semigroup_Traversable['traverse1']($traversable1NonEmptyList)($dictApply)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    }, function ($dictApply)  use (&$PS__Data_Functor, &$PS__Data_Foldable, &$foldableList, &$PS__Control_Semigroupoid, &$PS__Control_Apply, &$PS__Data_Function, &$nelCons, &$PS__Control_Applicative, &$applicativeNonEmptyList) {
        return function ($f)  use (&$PS__Data_Functor, &$dictApply, &$PS__Data_Foldable, &$foldableList, &$PS__Control_Semigroupoid, &$PS__Control_Apply, &$PS__Data_Function, &$nelCons, &$PS__Control_Applicative, &$applicativeNonEmptyList) {
            return function ($v)  use (&$PS__Data_Functor, &$dictApply, &$PS__Data_Foldable, &$foldableList, &$PS__Control_Semigroupoid, &$PS__Control_Apply, &$PS__Data_Function, &$nelCons, &$f, &$PS__Control_Applicative, &$applicativeNonEmptyList) {
                return $PS__Data_Functor['mapFlipped']($dictApply['Functor0']())($PS__Data_Foldable['foldl']($foldableList)(function ($acc)  use (&$PS__Control_Semigroupoid, &$PS__Control_Apply, &$dictApply, &$PS__Data_Function, &$nelCons, &$f) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Apply['lift2']($dictApply)($PS__Data_Function['flip']($nelCons))($acc))($f);
                })($PS__Data_Functor['map']($dictApply['Functor0']())($PS__Control_Applicative['pure']($applicativeNonEmptyList))($f($v['value0'])))($v['value1']))(function ($v1)  use (&$PS__Data_Foldable, &$foldableList, &$PS__Data_Function, &$nelCons, &$PS__Control_Applicative, &$applicativeNonEmptyList) {
                    return $PS__Data_Foldable['foldl']($foldableList)($PS__Data_Function['flip']($nelCons))($PS__Control_Applicative['pure']($applicativeNonEmptyList)($v1['value0']))($v1['value1']);
                });
            };
        };
    });
    return [
        'Nil' => $Nil,
        'Cons' => $Cons,
        'NonEmptyList' => $NonEmptyList,
        'toList' => $toList,
        'nelCons' => $nelCons,
        'showList' => $showList,
        'eqList' => $eqList,
        'eq1List' => $eq1List,
        'ordList' => $ordList,
        'ord1List' => $ord1List,
        'semigroupList' => $semigroupList,
        'monoidList' => $monoidList,
        'functorList' => $functorList,
        'functorWithIndexList' => $functorWithIndexList,
        'foldableList' => $foldableList,
        'foldableWithIndexList' => $foldableWithIndexList,
        'unfoldable1List' => $unfoldable1List,
        'unfoldableList' => $unfoldableList,
        'traversableList' => $traversableList,
        'traversableWithIndexList' => $traversableWithIndexList,
        'applyList' => $applyList,
        'applicativeList' => $applicativeList,
        'bindList' => $bindList,
        'monadList' => $monadList,
        'altList' => $altList,
        'plusList' => $plusList,
        'alternativeList' => $alternativeList,
        'monadZeroList' => $monadZeroList,
        'monadPlusList' => $monadPlusList,
        'extendList' => $extendList,
        'newtypeNonEmptyList' => $newtypeNonEmptyList,
        'eqNonEmptyList' => $eqNonEmptyList,
        'ordNonEmptyList' => $ordNonEmptyList,
        'showNonEmptyList' => $showNonEmptyList,
        'functorNonEmptyList' => $functorNonEmptyList,
        'applyNonEmptyList' => $applyNonEmptyList,
        'applicativeNonEmptyList' => $applicativeNonEmptyList,
        'bindNonEmptyList' => $bindNonEmptyList,
        'monadNonEmptyList' => $monadNonEmptyList,
        'altNonEmptyList' => $altNonEmptyList,
        'extendNonEmptyList' => $extendNonEmptyList,
        'comonadNonEmptyList' => $comonadNonEmptyList,
        'semigroupNonEmptyList' => $semigroupNonEmptyList,
        'foldableNonEmptyList' => $foldableNonEmptyList,
        'traversableNonEmptyList' => $traversableNonEmptyList,
        'foldable1NonEmptyList' => $foldable1NonEmptyList,
        'traversable1NonEmptyList' => $traversable1NonEmptyList
    ];
})();
