<?php
require_once(dirname(__FILE__) . '/../Data.Enum/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
$PS__Data_Char = (function ()  use (&$PS__Data_Enum) {
    $toCharCode = $PS__Data_Enum['fromEnum']($PS__Data_Enum['boundedEnumChar']);
    $fromCharCode = $PS__Data_Enum['toEnum']($PS__Data_Enum['boundedEnumChar']);
    return [
        'toCharCode' => $toCharCode,
        'fromCharCode' => $fromCharCode
    ];
})();
