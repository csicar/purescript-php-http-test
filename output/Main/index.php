<?php
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Effect/index.php');
require_once(dirname(__FILE__) . '/../Effect.Class/index.php');
require_once(dirname(__FILE__) . '/../Effect.Class.Console/index.php');
require_once(dirname(__FILE__) . '/../Foreign.Object/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
require_once(dirname(__FILE__) . '/../Server/index.php');
$PS__Main = (function ()  use (&$PS__Foreign_Object, &$PS__Server, &$PS__Control_Bind, &$PS__Effect, &$PS__Data_Show, &$PS__Data_Function, &$PS__Effect_Class_Console, &$PS__Effect_Class, &$PS__Data_Semigroup) {
    $port = 8080;
    $handleRsp = function ($v)  use (&$PS__Foreign_Object, &$PS__Server) {
        $header = $PS__Foreign_Object['singleton']('Content-Type')('text/html');
        return $PS__Server['response'](200)($header)('<button>Hello World!</button>');
    };
    $main = $PS__Control_Bind['bind']($PS__Effect['bindEffect'])($PS__Server['createLoop'])(function ($v)  use (&$PS__Control_Bind, &$PS__Effect, &$PS__Server, &$handleRsp, &$PS__Data_Show, &$port, &$PS__Data_Function, &$PS__Effect_Class_Console, &$PS__Effect_Class, &$PS__Data_Semigroup) {
        return $PS__Control_Bind['bind']($PS__Effect['bindEffect'])($PS__Server['createServer']($handleRsp))(function ($v1)  use (&$PS__Control_Bind, &$PS__Effect, &$PS__Server, &$PS__Data_Show, &$port, &$v, &$PS__Data_Function, &$PS__Effect_Class_Console, &$PS__Effect_Class, &$PS__Data_Semigroup) {
            return $PS__Control_Bind['bind']($PS__Effect['bindEffect'])($PS__Server['createSocket']($PS__Data_Show['show']($PS__Data_Show['showInt'])($port))($v))(function ($v2)  use (&$PS__Control_Bind, &$PS__Effect, &$PS__Server, &$v1, &$PS__Data_Function, &$PS__Effect_Class_Console, &$PS__Effect_Class, &$PS__Data_Semigroup, &$PS__Data_Show, &$port, &$v) {
                return $PS__Control_Bind['discard']($PS__Control_Bind['discardUnit'])($PS__Effect['bindEffect'])($PS__Server['listen']($v1)($v2))(function ()  use (&$PS__Control_Bind, &$PS__Effect, &$PS__Data_Function, &$PS__Effect_Class_Console, &$PS__Effect_Class, &$PS__Data_Semigroup, &$PS__Data_Show, &$port, &$PS__Server, &$v) {
                    return $PS__Control_Bind['discard']($PS__Control_Bind['discardUnit'])($PS__Effect['bindEffect'])($PS__Data_Function['apply']($PS__Effect_Class_Console['log']($PS__Effect_Class['monadEffectEffect']))($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('Server running on port ')($PS__Data_Show['show']($PS__Data_Show['showInt'])($port))))(function ()  use (&$PS__Server, &$v) {
                        return $PS__Server['runLoop']($v);
                    });
                });
            });
        });
    });
    return [
        'handleRsp' => $handleRsp,
        'port' => $port,
        'main' => $main
    ];
})();
