<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.BooleanAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor.Contravariant/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Predicate = (function ()  use (&$PS__Data_Newtype, &$PS__Data_HeytingAlgebra, &$PS__Data_Functor_Contravariant, &$PS__Control_Semigroupoid, &$PS__Data_BooleanAlgebra) {
    $Predicate = function ($x) {
        return $x;
    };
    $newtypePredicate = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Predicate);
    $heytingAlgebraPredicate = $PS__Data_HeytingAlgebra['heytingAlgebraFunction']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean']);
    $contravariantPredicate = $PS__Data_Functor_Contravariant['Contravariant'](function ($f)  use (&$PS__Control_Semigroupoid) {
        return function ($v)  use (&$PS__Control_Semigroupoid, &$f) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($v)($f);
        };
    });
    $booleanAlgebraPredicate = $PS__Data_BooleanAlgebra['booleanAlgebraFn']($PS__Data_BooleanAlgebra['booleanAlgebraBoolean']);
    return [
        'Predicate' => $Predicate,
        'newtypePredicate' => $newtypePredicate,
        'heytingAlgebraPredicate' => $heytingAlgebraPredicate,
        'booleanAlgebraPredicate' => $booleanAlgebraPredicate,
        'contravariantPredicate' => $contravariantPredicate
    ];
})();
