<?php
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Gen_Class = (function () {
    $MonadGen = function ($Monad0, $chooseBool, $chooseFloat, $chooseInt, $resize, $sized) {
        return [
            'Monad0' => $Monad0,
            'chooseBool' => $chooseBool,
            'chooseFloat' => $chooseFloat,
            'chooseInt' => $chooseInt,
            'resize' => $resize,
            'sized' => $sized
        ];
    };
    $sized = function ($dict) {
        return $dict['sized'];
    };
    $resize = function ($dict) {
        return $dict['resize'];
    };
    $chooseInt = function ($dict) {
        return $dict['chooseInt'];
    };
    $chooseFloat = function ($dict) {
        return $dict['chooseFloat'];
    };
    $chooseBool = function ($dict) {
        return $dict['chooseBool'];
    };
    return [
        'chooseBool' => $chooseBool,
        'chooseFloat' => $chooseFloat,
        'chooseInt' => $chooseInt,
        'resize' => $resize,
        'sized' => $sized,
        'MonadGen' => $MonadGen
    ];
})();
