<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Either/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Error_Class = (function ()  use (&$PS__Data_Maybe, &$PS__Data_Function, &$PS__Data_Either, &$PS__Data_Unit, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$PS__Control_Applicative, &$PS__Control_Bind) {
    $MonadThrow = function ($Monad0, $throwError) {
        return [
            'Monad0' => $Monad0,
            'throwError' => $throwError
        ];
    };
    $MonadError = function ($MonadThrow0, $catchError) {
        return [
            'MonadThrow0' => $MonadThrow0,
            'catchError' => $catchError
        ];
    };
    $throwError = function ($dict) {
        return $dict['throwError'];
    };
    $monadThrowMaybe = $MonadThrow(function ()  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['monadMaybe'];
    }, $PS__Data_Function['const']($PS__Data_Maybe['Nothing']()));
    $monadThrowEither = $MonadThrow(function ()  use (&$PS__Data_Either) {
        return $PS__Data_Either['monadEither'];
    }, $PS__Data_Either['Left']['create']);
    $monadErrorMaybe = $MonadError(function ()  use (&$monadThrowMaybe) {
        return $monadThrowMaybe;
    }, function ($v)  use (&$PS__Data_Unit, &$PS__Data_Maybe) {
        return function ($v1)  use (&$v, &$PS__Data_Unit, &$PS__Data_Maybe) {
            if ($v['type'] === 'Nothing') {
                return $v1($PS__Data_Unit['unit']);
            };
            if ($v['type'] === 'Just') {
                return $PS__Data_Maybe['Just']['constructor']($v['value0']);
            };
            throw new \Exception('Failed pattern match at Control.Monad.Error.Class line 76, column 1 - line 76, column 50: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
        };
    });
    $monadErrorEither = $MonadError(function ()  use (&$monadThrowEither) {
        return $monadThrowEither;
    }, function ($v)  use (&$PS__Data_Either) {
        return function ($v1)  use (&$v, &$PS__Data_Either) {
            if ($v['type'] === 'Left') {
                return $v1($v['value0']);
            };
            if ($v['type'] === 'Right') {
                return $PS__Data_Either['Right']['constructor']($v['value0']);
            };
            throw new \Exception('Failed pattern match at Control.Monad.Error.Class line 69, column 1 - line 69, column 53: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
        };
    });
    $catchError = function ($dict) {
        return $dict['catchError'];
    };
    $catchJust = function ($dictMonadError)  use (&$throwError, &$catchError) {
        return function ($p)  use (&$throwError, &$dictMonadError, &$catchError) {
            return function ($act)  use (&$p, &$throwError, &$dictMonadError, &$catchError) {
                return function ($handler)  use (&$p, &$throwError, &$dictMonadError, &$catchError, &$act) {
                    $handle = function ($e)  use (&$p, &$throwError, &$dictMonadError, &$handler) {
                        $v = $p($e);
                        if ($v['type'] === 'Nothing') {
                            return $throwError($dictMonadError['MonadThrow0']())($e);
                        };
                        if ($v['type'] === 'Just') {
                            return $handler($v['value0']);
                        };
                        throw new \Exception('Failed pattern match at Control.Monad.Error.Class line 54, column 5 - line 56, column 26: ' . var_dump([ $v['constructor']['name'] ]));
                    };
                    return $catchError($dictMonadError)($act)($handle);
                };
            };
        };
    };
    $__try = function ($dictMonadError)  use (&$catchError, &$PS__Data_Functor, &$PS__Data_Either, &$PS__Control_Semigroupoid, &$PS__Control_Applicative) {
        return function ($a)  use (&$catchError, &$dictMonadError, &$PS__Data_Functor, &$PS__Data_Either, &$PS__Control_Semigroupoid, &$PS__Control_Applicative) {
            return $catchError($dictMonadError)($PS__Data_Functor['map']((((($dictMonadError['MonadThrow0']())['Monad0']())['Bind1']())['Apply0']())['Functor0']())($PS__Data_Either['Right']['create'])($a))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Applicative['pure']((($dictMonadError['MonadThrow0']())['Monad0']())['Applicative0']()))($PS__Data_Either['Left']['create']));
        };
    };
    $withResource = function ($dictMonadError)  use (&$PS__Control_Bind, &$PS__Data_Function, &$__try, &$PS__Data_Either, &$throwError, &$PS__Control_Applicative) {
        return function ($acquire)  use (&$PS__Control_Bind, &$dictMonadError, &$PS__Data_Function, &$__try, &$PS__Data_Either, &$throwError, &$PS__Control_Applicative) {
            return function ($release)  use (&$PS__Control_Bind, &$dictMonadError, &$acquire, &$PS__Data_Function, &$__try, &$PS__Data_Either, &$throwError, &$PS__Control_Applicative) {
                return function ($kleisli)  use (&$PS__Control_Bind, &$dictMonadError, &$acquire, &$PS__Data_Function, &$__try, &$release, &$PS__Data_Either, &$throwError, &$PS__Control_Applicative) {
                    return $PS__Control_Bind['bind']((($dictMonadError['MonadThrow0']())['Monad0']())['Bind1']())($acquire)(function ($v)  use (&$PS__Control_Bind, &$dictMonadError, &$PS__Data_Function, &$__try, &$kleisli, &$release, &$PS__Data_Either, &$throwError, &$PS__Control_Applicative) {
                        return $PS__Control_Bind['bind']((($dictMonadError['MonadThrow0']())['Monad0']())['Bind1']())($PS__Data_Function['apply']($__try($dictMonadError))($kleisli($v)))(function ($v1)  use (&$PS__Control_Bind, &$dictMonadError, &$release, &$v, &$PS__Data_Either, &$throwError, &$PS__Control_Applicative) {
                            return $PS__Control_Bind['discard']($PS__Control_Bind['discardUnit'])((($dictMonadError['MonadThrow0']())['Monad0']())['Bind1']())($release($v))(function ()  use (&$PS__Data_Either, &$throwError, &$dictMonadError, &$PS__Control_Applicative, &$v1) {
                                return $PS__Data_Either['either']($throwError($dictMonadError['MonadThrow0']()))($PS__Control_Applicative['pure']((($dictMonadError['MonadThrow0']())['Monad0']())['Applicative0']()))($v1);
                            });
                        });
                    });
                };
            };
        };
    };
    return [
        'catchError' => $catchError,
        'throwError' => $throwError,
        'MonadThrow' => $MonadThrow,
        'MonadError' => $MonadError,
        'catchJust' => $catchJust,
        'try' => $__try,
        'withResource' => $withResource,
        'monadThrowEither' => $monadThrowEither,
        'monadErrorEither' => $monadErrorEither,
        'monadThrowMaybe' => $monadThrowMaybe,
        'monadErrorMaybe' => $monadErrorMaybe
    ];
})();
