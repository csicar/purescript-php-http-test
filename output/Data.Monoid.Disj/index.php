<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Monoid_Disj = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Semiring, &$PS__Data_HeytingAlgebra, &$PS__Data_Monoid, &$PS__Data_Functor, &$PS__Data_Eq, &$PS__Data_Ord, &$PS__Control_Apply, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Control_Monad) {
    $Disj = function ($x) {
        return $x;
    };
    $showDisj = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Disj ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $semiringDisj = function ($dictHeytingAlgebra)  use (&$PS__Data_Semiring, &$PS__Data_HeytingAlgebra) {
        return $PS__Data_Semiring['Semiring'](function ($v)  use (&$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra) {
            return function ($v1)  use (&$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$v) {
                return $PS__Data_HeytingAlgebra['disj']($dictHeytingAlgebra)($v)($v1);
            };
        }, function ($v)  use (&$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra) {
            return function ($v1)  use (&$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$v) {
                return $PS__Data_HeytingAlgebra['conj']($dictHeytingAlgebra)($v)($v1);
            };
        }, $PS__Data_HeytingAlgebra['tt']($dictHeytingAlgebra), $PS__Data_HeytingAlgebra['ff']($dictHeytingAlgebra));
    };
    $semigroupDisj = function ($dictHeytingAlgebra)  use (&$PS__Data_Semigroup, &$PS__Data_HeytingAlgebra) {
        return $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra) {
            return function ($v1)  use (&$PS__Data_HeytingAlgebra, &$dictHeytingAlgebra, &$v) {
                return $PS__Data_HeytingAlgebra['disj']($dictHeytingAlgebra)($v)($v1);
            };
        });
    };
    $ordDisj = function ($dictOrd) {
        return $dictOrd;
    };
    $monoidDisj = function ($dictHeytingAlgebra)  use (&$PS__Data_Monoid, &$semigroupDisj, &$PS__Data_HeytingAlgebra) {
        return $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupDisj, &$dictHeytingAlgebra) {
            return $semigroupDisj($dictHeytingAlgebra);
        }, $PS__Data_HeytingAlgebra['ff']($dictHeytingAlgebra));
    };
    $functorDisj = $PS__Data_Functor['Functor'](function ($f) {
        return function ($m)  use (&$f) {
            return $f($m);
        };
    });
    $eqDisj = function ($dictEq) {
        return $dictEq;
    };
    $eq1Disj = $PS__Data_Eq['Eq1'](function ($dictEq)  use (&$PS__Data_Eq, &$eqDisj) {
        return $PS__Data_Eq['eq']($eqDisj($dictEq));
    });
    $ord1Disj = $PS__Data_Ord['Ord1'](function ()  use (&$eq1Disj) {
        return $eq1Disj;
    }, function ($dictOrd)  use (&$PS__Data_Ord, &$ordDisj) {
        return $PS__Data_Ord['compare']($ordDisj($dictOrd));
    });
    $boundedDisj = function ($dictBounded) {
        return $dictBounded;
    };
    $applyDisj = $PS__Control_Apply['Apply'](function ()  use (&$functorDisj) {
        return $functorDisj;
    }, function ($v) {
        return function ($v1)  use (&$v) {
            return $v($v1);
        };
    });
    $bindDisj = $PS__Control_Bind['Bind'](function ()  use (&$applyDisj) {
        return $applyDisj;
    }, function ($v) {
        return function ($f)  use (&$v) {
            return $f($v);
        };
    });
    $applicativeDisj = $PS__Control_Applicative['Applicative'](function ()  use (&$applyDisj) {
        return $applyDisj;
    }, $Disj);
    $monadDisj = $PS__Control_Monad['Monad'](function ()  use (&$applicativeDisj) {
        return $applicativeDisj;
    }, function ()  use (&$bindDisj) {
        return $bindDisj;
    });
    return [
        'Disj' => $Disj,
        'eqDisj' => $eqDisj,
        'eq1Disj' => $eq1Disj,
        'ordDisj' => $ordDisj,
        'ord1Disj' => $ord1Disj,
        'boundedDisj' => $boundedDisj,
        'showDisj' => $showDisj,
        'functorDisj' => $functorDisj,
        'applyDisj' => $applyDisj,
        'applicativeDisj' => $applicativeDisj,
        'bindDisj' => $bindDisj,
        'monadDisj' => $monadDisj,
        'semigroupDisj' => $semigroupDisj,
        'monoidDisj' => $monoidDisj,
        'semiringDisj' => $semiringDisj
    ];
})();
