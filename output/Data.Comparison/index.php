<?php
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor.Contravariant/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Comparison = (function ()  use (&$PS__Data_Semigroup, &$PS__Data_Ordering, &$PS__Data_Newtype, &$PS__Data_Monoid, &$PS__Data_Ord, &$PS__Data_Functor_Contravariant, &$PS__Data_Function) {
    $Comparison = function ($x) {
        return $x;
    };
    $semigroupComparison = $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Ordering) {
        return function ($v1)  use (&$PS__Data_Semigroup, &$PS__Data_Ordering, &$v) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupFn']($PS__Data_Semigroup['semigroupFn']($PS__Data_Ordering['semigroupOrdering'])))($v)($v1);
        };
    });
    $newtypeComparison = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Comparison);
    $monoidComparison = $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupComparison) {
        return $semigroupComparison;
    }, function ($v)  use (&$PS__Data_Ordering) {
        return function ($v1)  use (&$PS__Data_Ordering) {
            return $PS__Data_Ordering['EQ']();
        };
    });
    $defaultComparison = function ($dictOrd)  use (&$PS__Data_Ord) {
        return $PS__Data_Ord['compare']($dictOrd);
    };
    $contravariantComparison = $PS__Data_Functor_Contravariant['Contravariant'](function ($f)  use (&$PS__Data_Function) {
        return function ($v)  use (&$PS__Data_Function, &$f) {
            return $PS__Data_Function['on']($v)($f);
        };
    });
    return [
        'Comparison' => $Comparison,
        'defaultComparison' => $defaultComparison,
        'newtypeComparison' => $newtypeComparison,
        'contravariantComparison' => $contravariantComparison,
        'semigroupComparison' => $semigroupComparison,
        'monoidComparison' => $monoidComparison
    ];
})();
