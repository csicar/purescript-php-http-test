<?php

$exports['nan'] = NAN;

$exports['isNaN'] = function($a) {
  return is_nan($a);
};

$exports['infinity'] = INF;

$exports['isFinite'] = function($a) {
  return is_finite($a);
};

$exports['readInt'] = function ($radix) {
  return function ($n) use (&$radix) {
    return intval($n, $radix);
  };
};

$exports['readFloat'] = function($a) {
  return floatval($a);
};
