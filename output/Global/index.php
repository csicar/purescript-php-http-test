<?php
$PS__Global = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    return [
        'nan' => $__foreign['nan'],
        'isNaN' => $__foreign['isNaN'],
        'infinity' => $__foreign['infinity'],
        'isFinite' => $__foreign['isFinite'],
        'readInt' => $__foreign['readInt'],
        'readFloat' => $__foreign['readFloat']
    ];
})();
