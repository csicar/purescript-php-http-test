<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Extend/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifoldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor/index.php');
require_once(dirname(__FILE__) . '/../Data.Bitraversable/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor.Invariant/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Either = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Maybe, &$PS__Control_Semigroupoid, &$PS__Data_Functor, &$PS__Data_Functor_Invariant, &$PS__Data_Foldable, &$PS__Data_Monoid, &$PS__Data_Traversable, &$PS__Control_Applicative, &$PS__Control_Extend, &$PS__Data_Eq, &$PS__Data_Ord, &$PS__Data_Ordering, &$PS__Data_Function, &$PS__Control_Alt, &$PS__Data_Bounded, &$PS__Data_Bifunctor, &$PS__Data_Bifoldable, &$PS__Data_Bitraversable, &$PS__Control_Apply, &$PS__Control_Bind, &$PS__Control_Monad) {
    $Left = [
        'constructor' => function ($value0) {
            return [
                'type' => 'Left',
                'value0' => $value0
            ];
        },
        'create' => function ($value0)  use (&$Left) {
            return $Left['constructor']($value0);
        }
    ];
    $Right = [
        'constructor' => function ($value0) {
            return [
                'type' => 'Right',
                'value0' => $value0
            ];
        },
        'create' => function ($value0)  use (&$Right) {
            return $Right['constructor']($value0);
        }
    ];
    $showEither = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return function ($dictShow1)  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$dictShow) {
            return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow, &$dictShow1) {
                if ($v['type'] === 'Left') {
                    return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Left ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v['value0']))(')'));
                };
                if ($v['type'] === 'Right') {
                    return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Right ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow1)($v['value0']))(')'));
                };
                throw new \Exception('Failed pattern match at Data.Either line 157, column 1 - line 157, column 61: ' . var_dump([ $v['constructor']['name'] ]));
            });
        };
    };
    $note__prime = function ($f)  use (&$PS__Data_Maybe, &$PS__Control_Semigroupoid, &$Left, &$Right) {
        return $PS__Data_Maybe['maybe\'']($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($Left['create'])($f))($Right['create']);
    };
    $note = function ($a)  use (&$PS__Data_Maybe, &$Left, &$Right) {
        return $PS__Data_Maybe['maybe']($Left['constructor']($a))($Right['create']);
    };
    $functorEither = $PS__Data_Functor['Functor'](function ($f)  use (&$Left, &$Right) {
        return function ($m)  use (&$Left, &$Right, &$f) {
            if ($m['type'] === 'Left') {
                return $Left['constructor']($m['value0']);
            };
            if ($m['type'] === 'Right') {
                return $Right['constructor']($f($m['value0']));
            };
            throw new \Exception('Failed pattern match at Data.Either line 35, column 8 - line 35, column 52: ' . var_dump([ $m['constructor']['name'] ]));
        };
    });
    $invariantEither = $PS__Data_Functor_Invariant['Invariant']($PS__Data_Functor_Invariant['imapF']($functorEither));
    $fromRight = function ($dictPartial) {
        return function ($v)  use (&$dictPartial) {
            $___unused = function ($dictPartial1) {
                return function ($__local_var__63) {
                    return $__local_var__63;
                };
            };
            return $___unused($dictPartial)((function ()  use (&$v) {
                if ($v['type'] === 'Right') {
                    return $v['value0'];
                };
                throw new \Exception('Failed pattern match at Data.Either line 243, column 1 - line 243, column 52: ' . var_dump([ $v['constructor']['name'] ]));
            })());
        };
    };
    $fromLeft = function ($dictPartial) {
        return function ($v)  use (&$dictPartial) {
            $___unused = function ($dictPartial1) {
                return function ($__local_var__67) {
                    return $__local_var__67;
                };
            };
            return $___unused($dictPartial)((function ()  use (&$v) {
                if ($v['type'] === 'Left') {
                    return $v['value0'];
                };
                throw new \Exception('Failed pattern match at Data.Either line 238, column 1 - line 238, column 51: ' . var_dump([ $v['constructor']['name'] ]));
            })());
        };
    };
    $foldableEither = $PS__Data_Foldable['Foldable'](function ($dictMonoid)  use (&$PS__Data_Monoid) {
        return function ($f)  use (&$PS__Data_Monoid, &$dictMonoid) {
            return function ($v)  use (&$PS__Data_Monoid, &$dictMonoid, &$f) {
                if ($v['type'] === 'Left') {
                    return $PS__Data_Monoid['mempty']($dictMonoid);
                };
                if ($v['type'] === 'Right') {
                    return $f($v['value0']);
                };
                throw new \Exception('Failed pattern match at Data.Either line 181, column 1 - line 181, column 47: ' . var_dump([ $f['constructor']['name'], $v['constructor']['name'] ]));
            };
        };
    }, function ($v) {
        return function ($z)  use (&$v) {
            return function ($v1)  use (&$z, &$v) {
                if ($v1['type'] === 'Left') {
                    return $z;
                };
                if ($v1['type'] === 'Right') {
                    return $v($z)($v1['value0']);
                };
                throw new \Exception('Failed pattern match at Data.Either line 181, column 1 - line 181, column 47: ' . var_dump([ $v['constructor']['name'], $z['constructor']['name'], $v1['constructor']['name'] ]));
            };
        };
    }, function ($v) {
        return function ($z)  use (&$v) {
            return function ($v1)  use (&$z, &$v) {
                if ($v1['type'] === 'Left') {
                    return $z;
                };
                if ($v1['type'] === 'Right') {
                    return $v($v1['value0'])($z);
                };
                throw new \Exception('Failed pattern match at Data.Either line 181, column 1 - line 181, column 47: ' . var_dump([ $v['constructor']['name'], $z['constructor']['name'], $v1['constructor']['name'] ]));
            };
        };
    });
    $traversableEither = $PS__Data_Traversable['Traversable'](function ()  use (&$foldableEither) {
        return $foldableEither;
    }, function ()  use (&$functorEither) {
        return $functorEither;
    }, function ($dictApplicative)  use (&$PS__Control_Applicative, &$Left, &$PS__Data_Functor, &$Right) {
        return function ($v)  use (&$PS__Control_Applicative, &$dictApplicative, &$Left, &$PS__Data_Functor, &$Right) {
            if ($v['type'] === 'Left') {
                return $PS__Control_Applicative['pure']($dictApplicative)($Left['constructor']($v['value0']));
            };
            if ($v['type'] === 'Right') {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($Right['create'])($v['value0']);
            };
            throw new \Exception('Failed pattern match at Data.Either line 197, column 1 - line 197, column 53: ' . var_dump([ $v['constructor']['name'] ]));
        };
    }, function ($dictApplicative)  use (&$PS__Control_Applicative, &$Left, &$PS__Data_Functor, &$Right) {
        return function ($v)  use (&$PS__Control_Applicative, &$dictApplicative, &$Left, &$PS__Data_Functor, &$Right) {
            return function ($v1)  use (&$PS__Control_Applicative, &$dictApplicative, &$Left, &$PS__Data_Functor, &$Right, &$v) {
                if ($v1['type'] === 'Left') {
                    return $PS__Control_Applicative['pure']($dictApplicative)($Left['constructor']($v1['value0']));
                };
                if ($v1['type'] === 'Right') {
                    return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($Right['create'])($v($v1['value0']));
                };
                throw new \Exception('Failed pattern match at Data.Either line 197, column 1 - line 197, column 53: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
            };
        };
    });
    $extendEither = $PS__Control_Extend['Extend'](function ()  use (&$functorEither) {
        return $functorEither;
    }, function ($v)  use (&$Left, &$Right) {
        return function ($v1)  use (&$Left, &$Right, &$v) {
            if ($v1['type'] === 'Left') {
                return $Left['constructor']($v1['value0']);
            };
            return $Right['constructor']($v($v1));
        };
    });
    $eqEither = function ($dictEq)  use (&$PS__Data_Eq) {
        return function ($dictEq1)  use (&$PS__Data_Eq, &$dictEq) {
            return $PS__Data_Eq['Eq'](function ($x)  use (&$PS__Data_Eq, &$dictEq, &$dictEq1) {
                return function ($y)  use (&$x, &$PS__Data_Eq, &$dictEq, &$dictEq1) {
                    if ($x['type'] === 'Left' && $y['type'] === 'Left') {
                        return $PS__Data_Eq['eq']($dictEq)($x['value0'])($y['value0']);
                    };
                    if ($x['type'] === 'Right' && $y['type'] === 'Right') {
                        return $PS__Data_Eq['eq']($dictEq1)($x['value0'])($y['value0']);
                    };
                    return false;
                };
            });
        };
    };
    $ordEither = function ($dictOrd)  use (&$PS__Data_Ord, &$eqEither, &$PS__Data_Ordering) {
        return function ($dictOrd1)  use (&$PS__Data_Ord, &$eqEither, &$dictOrd, &$PS__Data_Ordering) {
            return $PS__Data_Ord['Ord'](function ()  use (&$eqEither, &$dictOrd, &$dictOrd1) {
                return $eqEither($dictOrd['Eq0']())($dictOrd1['Eq0']());
            }, function ($x)  use (&$PS__Data_Ord, &$dictOrd, &$PS__Data_Ordering, &$dictOrd1) {
                return function ($y)  use (&$x, &$PS__Data_Ord, &$dictOrd, &$PS__Data_Ordering, &$dictOrd1) {
                    if ($x['type'] === 'Left' && $y['type'] === 'Left') {
                        return $PS__Data_Ord['compare']($dictOrd)($x['value0'])($y['value0']);
                    };
                    if ($x['type'] === 'Left') {
                        return $PS__Data_Ordering['LT']();
                    };
                    if ($y['type'] === 'Left') {
                        return $PS__Data_Ordering['GT']();
                    };
                    if ($x['type'] === 'Right' && $y['type'] === 'Right') {
                        return $PS__Data_Ord['compare']($dictOrd1)($x['value0'])($y['value0']);
                    };
                    throw new \Exception('Failed pattern match at Data.Either line 173, column 8 - line 173, column 64: ' . var_dump([ $x['constructor']['name'], $y['constructor']['name'] ]));
                };
            });
        };
    };
    $eq1Either = function ($dictEq)  use (&$PS__Data_Eq, &$eqEither) {
        return $PS__Data_Eq['Eq1'](function ($dictEq1)  use (&$PS__Data_Eq, &$eqEither, &$dictEq) {
            return $PS__Data_Eq['eq']($eqEither($dictEq)($dictEq1));
        });
    };
    $ord1Either = function ($dictOrd)  use (&$PS__Data_Ord, &$eq1Either, &$ordEither) {
        return $PS__Data_Ord['Ord1'](function ()  use (&$eq1Either, &$dictOrd) {
            return $eq1Either($dictOrd['Eq0']());
        }, function ($dictOrd1)  use (&$PS__Data_Ord, &$ordEither, &$dictOrd) {
            return $PS__Data_Ord['compare']($ordEither($dictOrd)($dictOrd1));
        });
    };
    $either = function ($v) {
        return function ($v1)  use (&$v) {
            return function ($v2)  use (&$v, &$v1) {
                if ($v2['type'] === 'Left') {
                    return $v($v2['value0']);
                };
                if ($v2['type'] === 'Right') {
                    return $v1($v2['value0']);
                };
                throw new \Exception('Failed pattern match at Data.Either line 220, column 1 - line 220, column 64: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'], $v2['constructor']['name'] ]));
            };
        };
    };
    $hush = $either($PS__Data_Function['const']($PS__Data_Maybe['Nothing']()))($PS__Data_Maybe['Just']['create']);
    $isLeft = $either($PS__Data_Function['const'](true))($PS__Data_Function['const'](false));
    $isRight = $either($PS__Data_Function['const'](false))($PS__Data_Function['const'](true));
    $choose = function ($dictAlt)  use (&$PS__Control_Alt, &$PS__Data_Functor, &$Left, &$Right) {
        return function ($a)  use (&$PS__Control_Alt, &$dictAlt, &$PS__Data_Functor, &$Left, &$Right) {
            return function ($b)  use (&$PS__Control_Alt, &$dictAlt, &$PS__Data_Functor, &$Left, &$a, &$Right) {
                return $PS__Control_Alt['alt']($dictAlt)($PS__Data_Functor['map']($dictAlt['Functor0']())($Left['create'])($a))($PS__Data_Functor['map']($dictAlt['Functor0']())($Right['create'])($b));
            };
        };
    };
    $boundedEither = function ($dictBounded)  use (&$PS__Data_Bounded, &$ordEither, &$Left, &$Right) {
        return function ($dictBounded1)  use (&$PS__Data_Bounded, &$ordEither, &$dictBounded, &$Left, &$Right) {
            return $PS__Data_Bounded['Bounded'](function ()  use (&$ordEither, &$dictBounded, &$dictBounded1) {
                return $ordEither($dictBounded['Ord0']())($dictBounded1['Ord0']());
            }, $Left['constructor']($PS__Data_Bounded['bottom']($dictBounded)), $Right['constructor']($PS__Data_Bounded['top']($dictBounded1)));
        };
    };
    $bifunctorEither = $PS__Data_Bifunctor['Bifunctor'](function ($v)  use (&$Left, &$Right) {
        return function ($v1)  use (&$Left, &$v, &$Right) {
            return function ($v2)  use (&$Left, &$v, &$Right, &$v1) {
                if ($v2['type'] === 'Left') {
                    return $Left['constructor']($v($v2['value0']));
                };
                if ($v2['type'] === 'Right') {
                    return $Right['constructor']($v1($v2['value0']));
                };
                throw new \Exception('Failed pattern match at Data.Either line 40, column 1 - line 40, column 45: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'], $v2['constructor']['name'] ]));
            };
        };
    });
    $bifoldableEither = $PS__Data_Bifoldable['Bifoldable'](function ($dictMonoid) {
        return function ($v) {
            return function ($v1)  use (&$v) {
                return function ($v2)  use (&$v, &$v1) {
                    if ($v2['type'] === 'Left') {
                        return $v($v2['value0']);
                    };
                    if ($v2['type'] === 'Right') {
                        return $v1($v2['value0']);
                    };
                    throw new \Exception('Failed pattern match at Data.Either line 189, column 1 - line 189, column 47: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'], $v2['constructor']['name'] ]));
                };
            };
        };
    }, function ($v) {
        return function ($v1)  use (&$v) {
            return function ($z)  use (&$v, &$v1) {
                return function ($v2)  use (&$v, &$z, &$v1) {
                    if ($v2['type'] === 'Left') {
                        return $v($z)($v2['value0']);
                    };
                    if ($v2['type'] === 'Right') {
                        return $v1($z)($v2['value0']);
                    };
                    throw new \Exception('Failed pattern match at Data.Either line 189, column 1 - line 189, column 47: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'], $z['constructor']['name'], $v2['constructor']['name'] ]));
                };
            };
        };
    }, function ($v) {
        return function ($v1)  use (&$v) {
            return function ($z)  use (&$v, &$v1) {
                return function ($v2)  use (&$v, &$z, &$v1) {
                    if ($v2['type'] === 'Left') {
                        return $v($v2['value0'])($z);
                    };
                    if ($v2['type'] === 'Right') {
                        return $v1($v2['value0'])($z);
                    };
                    throw new \Exception('Failed pattern match at Data.Either line 189, column 1 - line 189, column 47: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'], $z['constructor']['name'], $v2['constructor']['name'] ]));
                };
            };
        };
    });
    $bitraversableEither = $PS__Data_Bitraversable['Bitraversable'](function ()  use (&$bifoldableEither) {
        return $bifoldableEither;
    }, function ()  use (&$bifunctorEither) {
        return $bifunctorEither;
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$Left, &$Right) {
        return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$Left, &$Right) {
            if ($v['type'] === 'Left') {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($Left['create'])($v['value0']);
            };
            if ($v['type'] === 'Right') {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($Right['create'])($v['value0']);
            };
            throw new \Exception('Failed pattern match at Data.Either line 203, column 1 - line 203, column 53: ' . var_dump([ $v['constructor']['name'] ]));
        };
    }, function ($dictApplicative)  use (&$PS__Data_Functor, &$Left, &$Right) {
        return function ($v)  use (&$PS__Data_Functor, &$dictApplicative, &$Left, &$Right) {
            return function ($v1)  use (&$PS__Data_Functor, &$dictApplicative, &$Left, &$v, &$Right) {
                return function ($v2)  use (&$PS__Data_Functor, &$dictApplicative, &$Left, &$v, &$Right, &$v1) {
                    if ($v2['type'] === 'Left') {
                        return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($Left['create'])($v($v2['value0']));
                    };
                    if ($v2['type'] === 'Right') {
                        return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($Right['create'])($v1($v2['value0']));
                    };
                    throw new \Exception('Failed pattern match at Data.Either line 203, column 1 - line 203, column 53: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'], $v2['constructor']['name'] ]));
                };
            };
        };
    });
    $applyEither = $PS__Control_Apply['Apply'](function ()  use (&$functorEither) {
        return $functorEither;
    }, function ($v)  use (&$Left, &$PS__Data_Functor, &$functorEither) {
        return function ($v1)  use (&$v, &$Left, &$PS__Data_Functor, &$functorEither) {
            if ($v['type'] === 'Left') {
                return $Left['constructor']($v['value0']);
            };
            if ($v['type'] === 'Right') {
                return $PS__Data_Functor['map']($functorEither)($v['value0'])($v1);
            };
            throw new \Exception('Failed pattern match at Data.Either line 76, column 1 - line 76, column 41: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
        };
    });
    $bindEither = $PS__Control_Bind['Bind'](function ()  use (&$applyEither) {
        return $applyEither;
    }, $either(function ($e)  use (&$Left) {
        return function ($v)  use (&$Left, &$e) {
            return $Left['constructor']($e);
        };
    })(function ($a) {
        return function ($f)  use (&$a) {
            return $f($a);
        };
    }));
    $semigroupEither = function ($dictSemigroup)  use (&$PS__Data_Semigroup, &$PS__Control_Apply, &$applyEither, &$PS__Data_Functor, &$functorEither) {
        return $PS__Data_Semigroup['Semigroup'](function ($x)  use (&$PS__Control_Apply, &$applyEither, &$PS__Data_Functor, &$functorEither, &$PS__Data_Semigroup, &$dictSemigroup) {
            return function ($y)  use (&$PS__Control_Apply, &$applyEither, &$PS__Data_Functor, &$functorEither, &$PS__Data_Semigroup, &$dictSemigroup, &$x) {
                return $PS__Control_Apply['apply']($applyEither)($PS__Data_Functor['map']($functorEither)($PS__Data_Semigroup['append']($dictSemigroup))($x))($y);
            };
        });
    };
    $applicativeEither = $PS__Control_Applicative['Applicative'](function ()  use (&$applyEither) {
        return $applyEither;
    }, $Right['create']);
    $monadEither = $PS__Control_Monad['Monad'](function ()  use (&$applicativeEither) {
        return $applicativeEither;
    }, function ()  use (&$bindEither) {
        return $bindEither;
    });
    $altEither = $PS__Control_Alt['Alt'](function ()  use (&$functorEither) {
        return $functorEither;
    }, function ($v) {
        return function ($v1)  use (&$v) {
            if ($v['type'] === 'Left') {
                return $v1;
            };
            return $v;
        };
    });
    return [
        'Left' => $Left,
        'Right' => $Right,
        'either' => $either,
        'choose' => $choose,
        'isLeft' => $isLeft,
        'isRight' => $isRight,
        'fromLeft' => $fromLeft,
        'fromRight' => $fromRight,
        'note' => $note,
        'note\'' => $note__prime,
        'hush' => $hush,
        'functorEither' => $functorEither,
        'invariantEither' => $invariantEither,
        'bifunctorEither' => $bifunctorEither,
        'applyEither' => $applyEither,
        'applicativeEither' => $applicativeEither,
        'altEither' => $altEither,
        'bindEither' => $bindEither,
        'monadEither' => $monadEither,
        'extendEither' => $extendEither,
        'showEither' => $showEither,
        'eqEither' => $eqEither,
        'eq1Either' => $eq1Either,
        'ordEither' => $ordEither,
        'ord1Either' => $ord1Either,
        'boundedEither' => $boundedEither,
        'foldableEither' => $foldableEither,
        'bifoldableEither' => $bifoldableEither,
        'traversableEither' => $traversableEither,
        'bitraversableEither' => $bitraversableEither,
        'semigroupEither' => $semigroupEither
    ];
})();
