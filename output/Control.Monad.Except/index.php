<?php
require_once(dirname(__FILE__) . '/../Control.Monad.Error.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Except.Trans/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Either/index.php');
require_once(dirname(__FILE__) . '/../Data.Identity/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Except = (function ()  use (&$PS__Control_Monad_Except_Trans, &$PS__Data_Identity, &$PS__Control_Semigroupoid, &$PS__Data_Newtype) {
    $withExcept = $PS__Control_Monad_Except_Trans['withExceptT']($PS__Data_Identity['functorIdentity']);
    $runExcept = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity']))($PS__Control_Monad_Except_Trans['runExceptT']);
    $mapExcept = function ($f)  use (&$PS__Control_Monad_Except_Trans, &$PS__Control_Semigroupoid, &$PS__Data_Identity, &$PS__Data_Newtype) {
        return $PS__Control_Monad_Except_Trans['mapExceptT']($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Identity['Identity'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity']))));
    };
    return [
        'runExcept' => $runExcept,
        'mapExcept' => $mapExcept,
        'withExcept' => $withExcept
    ];
})();
