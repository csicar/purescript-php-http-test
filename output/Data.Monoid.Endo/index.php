<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Monoid_Endo = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Control_Semigroupoid, &$PS__Data_Monoid, &$PS__Control_Category) {
    $Endo = function ($x) {
        return $x;
    };
    $showEndo = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Endo ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $semigroupEndo = function ($dictSemigroupoid)  use (&$PS__Data_Semigroup, &$PS__Control_Semigroupoid) {
        return $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$PS__Control_Semigroupoid, &$dictSemigroupoid) {
            return function ($v1)  use (&$PS__Control_Semigroupoid, &$dictSemigroupoid, &$v) {
                return $PS__Control_Semigroupoid['compose']($dictSemigroupoid)($v)($v1);
            };
        });
    };
    $ordEndo = function ($dictOrd) {
        return $dictOrd;
    };
    $monoidEndo = function ($dictCategory)  use (&$PS__Data_Monoid, &$semigroupEndo, &$PS__Control_Category) {
        return $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupEndo, &$dictCategory) {
            return $semigroupEndo($dictCategory['Semigroupoid0']());
        }, $PS__Control_Category['identity']($dictCategory));
    };
    $eqEndo = function ($dictEq) {
        return $dictEq;
    };
    $boundedEndo = function ($dictBounded) {
        return $dictBounded;
    };
    return [
        'Endo' => $Endo,
        'eqEndo' => $eqEndo,
        'ordEndo' => $ordEndo,
        'boundedEndo' => $boundedEndo,
        'showEndo' => $showEndo,
        'semigroupEndo' => $semigroupEndo,
        'monoidEndo' => $monoidEndo
    ];
})();
