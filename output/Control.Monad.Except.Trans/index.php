<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Cont.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Error.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Reader.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.State.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Trans.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Writer.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadPlus/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadZero/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Either/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Effect.Class/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Except_Trans = (function ()  use (&$PS__Data_Either, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Newtype, &$PS__Control_Monad_Trans_Class, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Control_Semigroupoid, &$PS__Control_Monad, &$PS__Control_Apply, &$PS__Control_Monad_Reader_Class, &$PS__Control_Monad_Cont_Class, &$PS__Effect_Class, &$PS__Control_Monad_Rec_Class, &$PS__Control_Monad_State_Class, &$PS__Control_Monad_Writer_Class, &$PS__Data_Tuple, &$PS__Control_Category, &$PS__Control_Monad_Error_Class, &$PS__Control_Alt, &$PS__Data_Semigroup, &$PS__Control_Plus, &$PS__Data_Monoid, &$PS__Control_Alternative, &$PS__Control_MonadZero, &$PS__Control_MonadPlus) {
    $ExceptT = function ($x) {
        return $x;
    };
    $withExceptT = function ($dictFunctor)  use (&$PS__Data_Either, &$PS__Data_Function, &$ExceptT, &$PS__Data_Functor) {
        return function ($f)  use (&$PS__Data_Either, &$PS__Data_Function, &$ExceptT, &$PS__Data_Functor, &$dictFunctor) {
            return function ($v)  use (&$PS__Data_Either, &$PS__Data_Function, &$ExceptT, &$PS__Data_Functor, &$dictFunctor, &$f) {
                $mapLeft = function ($v1)  use (&$PS__Data_Either) {
                    return function ($v2)  use (&$PS__Data_Either, &$v1) {
                        if ($v2['type'] === 'Right') {
                            return $PS__Data_Either['Right']['constructor']($v2['value0']);
                        };
                        if ($v2['type'] === 'Left') {
                            return $PS__Data_Either['Left']['constructor']($v1($v2['value0']));
                        };
                        throw new \Exception('Failed pattern match at Control.Monad.Except.Trans line 42, column 3 - line 42, column 32: ' . var_dump([ $v1['constructor']['name'], $v2['constructor']['name'] ]));
                    };
                };
                return $PS__Data_Function['apply']($ExceptT)($PS__Data_Functor['map']($dictFunctor)($mapLeft($f))($v));
            };
        };
    };
    $runExceptT = function ($v) {
        return $v;
    };
    $newtypeExceptT = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $ExceptT);
    $monadTransExceptT = $PS__Control_Monad_Trans_Class['MonadTrans'](function ($dictMonad)  use (&$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Either) {
        return function ($m)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Either) {
            return $PS__Control_Bind['bind']($dictMonad['Bind1']())($m)(function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonad, &$PS__Data_Either) {
                return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($PS__Data_Either['Right']['constructor']($v));
            });
        };
    });
    $mapExceptT = function ($f) {
        return function ($v)  use (&$f) {
            return $f($v);
        };
    };
    $functorExceptT = function ($dictFunctor)  use (&$PS__Data_Functor, &$mapExceptT, &$PS__Data_Either) {
        return $PS__Data_Functor['Functor'](function ($f)  use (&$mapExceptT, &$PS__Data_Functor, &$dictFunctor, &$PS__Data_Either) {
            return $mapExceptT($PS__Data_Functor['map']($dictFunctor)($PS__Data_Functor['map']($PS__Data_Either['functorEither'])($f)));
        });
    };
    $except = function ($dictApplicative)  use (&$PS__Control_Semigroupoid, &$ExceptT, &$PS__Control_Applicative) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($ExceptT)($PS__Control_Applicative['pure']($dictApplicative));
    };
    $monadExceptT = function ($dictMonad)  use (&$PS__Control_Monad, &$applicativeExceptT, &$bindExceptT) {
        return $PS__Control_Monad['Monad'](function ()  use (&$applicativeExceptT, &$dictMonad) {
            return $applicativeExceptT($dictMonad);
        }, function ()  use (&$bindExceptT, &$dictMonad) {
            return $bindExceptT($dictMonad);
        });
    };
    $bindExceptT = function ($dictMonad)  use (&$PS__Control_Bind, &$applyExceptT, &$PS__Data_Either, &$PS__Control_Semigroupoid, &$PS__Control_Applicative) {
        return $PS__Control_Bind['Bind'](function ()  use (&$applyExceptT, &$dictMonad) {
            return $applyExceptT($dictMonad);
        }, function ($v)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Data_Either, &$PS__Control_Semigroupoid, &$PS__Control_Applicative) {
            return function ($k)  use (&$PS__Control_Bind, &$dictMonad, &$v, &$PS__Data_Either, &$PS__Control_Semigroupoid, &$PS__Control_Applicative) {
                return $PS__Control_Bind['bind']($dictMonad['Bind1']())($v)($PS__Data_Either['either']($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($PS__Data_Either['Left']['create']))(function ($a)  use (&$k) {
                    $v1 = $k($a);
                    return $v1;
                }));
            };
        });
    };
    $applyExceptT = function ($dictMonad)  use (&$PS__Control_Apply, &$functorExceptT, &$PS__Control_Monad, &$monadExceptT) {
        return $PS__Control_Apply['Apply'](function ()  use (&$functorExceptT, &$dictMonad) {
            return $functorExceptT((($dictMonad['Bind1']())['Apply0']())['Functor0']());
        }, $PS__Control_Monad['ap']($monadExceptT($dictMonad)));
    };
    $applicativeExceptT = function ($dictMonad)  use (&$PS__Control_Applicative, &$applyExceptT, &$PS__Control_Semigroupoid, &$ExceptT, &$PS__Data_Either) {
        return $PS__Control_Applicative['Applicative'](function ()  use (&$applyExceptT, &$dictMonad) {
            return $applyExceptT($dictMonad);
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($ExceptT)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($PS__Data_Either['Right']['create'])));
    };
    $monadAskExceptT = function ($dictMonadAsk)  use (&$PS__Control_Monad_Reader_Class, &$monadExceptT, &$PS__Control_Monad_Trans_Class, &$monadTransExceptT) {
        return $PS__Control_Monad_Reader_Class['MonadAsk'](function ()  use (&$monadExceptT, &$dictMonadAsk) {
            return $monadExceptT($dictMonadAsk['Monad0']());
        }, $PS__Control_Monad_Trans_Class['lift']($monadTransExceptT)($dictMonadAsk['Monad0']())($PS__Control_Monad_Reader_Class['ask']($dictMonadAsk)));
    };
    $monadReaderExceptT = function ($dictMonadReader)  use (&$PS__Control_Monad_Reader_Class, &$monadAskExceptT, &$mapExceptT) {
        return $PS__Control_Monad_Reader_Class['MonadReader'](function ()  use (&$monadAskExceptT, &$dictMonadReader) {
            return $monadAskExceptT($dictMonadReader['MonadAsk0']());
        }, function ($f)  use (&$mapExceptT, &$PS__Control_Monad_Reader_Class, &$dictMonadReader) {
            return $mapExceptT($PS__Control_Monad_Reader_Class['local']($dictMonadReader)($f));
        });
    };
    $monadContExceptT = function ($dictMonadCont)  use (&$PS__Control_Monad_Cont_Class, &$monadExceptT, &$PS__Data_Function, &$ExceptT, &$PS__Data_Either) {
        return $PS__Control_Monad_Cont_Class['MonadCont'](function ()  use (&$monadExceptT, &$dictMonadCont) {
            return $monadExceptT($dictMonadCont['Monad0']());
        }, function ($f)  use (&$PS__Data_Function, &$ExceptT, &$PS__Control_Monad_Cont_Class, &$dictMonadCont, &$PS__Data_Either) {
            return $PS__Data_Function['apply']($ExceptT)($PS__Control_Monad_Cont_Class['callCC']($dictMonadCont)(function ($c)  use (&$f, &$PS__Data_Function, &$ExceptT, &$PS__Data_Either) {
                $v = $f(function ($a)  use (&$PS__Data_Function, &$ExceptT, &$c, &$PS__Data_Either) {
                    return $PS__Data_Function['apply']($ExceptT)($c($PS__Data_Either['Right']['constructor']($a)));
                });
                return $v;
            }));
        });
    };
    $monadEffectExceptT = function ($dictMonadEffect)  use (&$PS__Effect_Class, &$monadExceptT, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransExceptT) {
        return $PS__Effect_Class['MonadEffect'](function ()  use (&$monadExceptT, &$dictMonadEffect) {
            return $monadExceptT($dictMonadEffect['Monad0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Trans_Class['lift']($monadTransExceptT)($dictMonadEffect['Monad0']()))($PS__Effect_Class['liftEffect']($dictMonadEffect)));
    };
    $monadRecExceptT = function ($dictMonadRec)  use (&$PS__Control_Monad_Rec_Class, &$monadExceptT, &$PS__Control_Semigroupoid, &$ExceptT, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Data_Either) {
        return $PS__Control_Monad_Rec_Class['MonadRec'](function ()  use (&$monadExceptT, &$dictMonadRec) {
            return $monadExceptT($dictMonadRec['Monad0']());
        }, function ($f)  use (&$PS__Control_Semigroupoid, &$ExceptT, &$PS__Control_Monad_Rec_Class, &$dictMonadRec, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Data_Either) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($ExceptT)($PS__Control_Monad_Rec_Class['tailRecM']($dictMonadRec)(function ($a)  use (&$PS__Control_Bind, &$dictMonadRec, &$f, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$PS__Data_Either) {
                return $PS__Control_Bind['bind'](($dictMonadRec['Monad0']())['Bind1']())((function ()  use (&$f, &$a) {
                    $v = $f($a);
                    return $v;
                })())(function ($m__prime)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$PS__Data_Either) {
                    return $PS__Control_Applicative['pure'](($dictMonadRec['Monad0']())['Applicative0']())((function ()  use (&$m__prime, &$PS__Control_Monad_Rec_Class, &$PS__Data_Either) {
                        if ($m__prime['type'] === 'Left') {
                            return $PS__Control_Monad_Rec_Class['Done']['constructor']($PS__Data_Either['Left']['constructor']($m__prime['value0']));
                        };
                        if ($m__prime['type'] === 'Right' && $m__prime['value0']['type'] === 'Loop') {
                            return $PS__Control_Monad_Rec_Class['Loop']['constructor']($m__prime['value0']['value0']);
                        };
                        if ($m__prime['type'] === 'Right' && $m__prime['value0']['type'] === 'Done') {
                            return $PS__Control_Monad_Rec_Class['Done']['constructor']($PS__Data_Either['Right']['constructor']($m__prime['value0']['value0']));
                        };
                        throw new \Exception('Failed pattern match at Control.Monad.Except.Trans line 74, column 14 - line 77, column 43: ' . var_dump([ $m__prime['constructor']['name'] ]));
                    })());
                });
            }));
        });
    };
    $monadStateExceptT = function ($dictMonadState)  use (&$PS__Control_Monad_State_Class, &$monadExceptT, &$PS__Control_Monad_Trans_Class, &$monadTransExceptT) {
        return $PS__Control_Monad_State_Class['MonadState'](function ()  use (&$monadExceptT, &$dictMonadState) {
            return $monadExceptT($dictMonadState['Monad0']());
        }, function ($f)  use (&$PS__Control_Monad_Trans_Class, &$monadTransExceptT, &$dictMonadState, &$PS__Control_Monad_State_Class) {
            return $PS__Control_Monad_Trans_Class['lift']($monadTransExceptT)($dictMonadState['Monad0']())($PS__Control_Monad_State_Class['state']($dictMonadState)($f));
        });
    };
    $monadTellExceptT = function ($dictMonadTell)  use (&$PS__Control_Monad_Writer_Class, &$monadExceptT, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransExceptT) {
        return $PS__Control_Monad_Writer_Class['MonadTell'](function ()  use (&$monadExceptT, &$dictMonadTell) {
            return $monadExceptT($dictMonadTell['Monad0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Trans_Class['lift']($monadTransExceptT)($dictMonadTell['Monad0']()))($PS__Control_Monad_Writer_Class['tell']($dictMonadTell)));
    };
    $monadWriterExceptT = function ($dictMonadWriter)  use (&$PS__Control_Monad_Writer_Class, &$monadTellExceptT, &$mapExceptT, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Functor, &$PS__Data_Either, &$PS__Data_Tuple, &$PS__Control_Category) {
        return $PS__Control_Monad_Writer_Class['MonadWriter'](function ()  use (&$monadTellExceptT, &$dictMonadWriter) {
            return $monadTellExceptT($dictMonadWriter['MonadTell0']());
        }, $mapExceptT(function ($m)  use (&$PS__Control_Bind, &$dictMonadWriter, &$PS__Control_Monad_Writer_Class, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Functor, &$PS__Data_Either, &$PS__Data_Tuple) {
            return $PS__Control_Bind['bind']((($dictMonadWriter['MonadTell0']())['Monad0']())['Bind1']())($PS__Control_Monad_Writer_Class['listen']($dictMonadWriter)($m))(function ($v)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$dictMonadWriter, &$PS__Data_Functor, &$PS__Data_Either, &$PS__Data_Tuple) {
                return $PS__Data_Function['apply']($PS__Control_Applicative['pure']((($dictMonadWriter['MonadTell0']())['Monad0']())['Applicative0']()))($PS__Data_Functor['map']($PS__Data_Either['functorEither'])(function ($r)  use (&$PS__Data_Tuple, &$v) {
                    return $PS__Data_Tuple['Tuple']['constructor']($r, $v['value1']);
                })($v['value0']));
            });
        }), $mapExceptT(function ($m)  use (&$PS__Control_Monad_Writer_Class, &$dictMonadWriter, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Data_Tuple, &$PS__Data_Either, &$PS__Control_Category) {
            return $PS__Control_Monad_Writer_Class['pass']($dictMonadWriter)($PS__Control_Bind['bind']((($dictMonadWriter['MonadTell0']())['Monad0']())['Bind1']())($m)(function ($v)  use (&$PS__Control_Applicative, &$dictMonadWriter, &$PS__Data_Tuple, &$PS__Data_Either, &$PS__Control_Category) {
                return $PS__Control_Applicative['pure']((($dictMonadWriter['MonadTell0']())['Monad0']())['Applicative0']())((function ()  use (&$v, &$PS__Data_Tuple, &$PS__Data_Either, &$PS__Control_Category) {
                    if ($v['type'] === 'Left') {
                        return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_Either['Left']['constructor']($v['value0']), $PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
                    };
                    if ($v['type'] === 'Right') {
                        return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_Either['Right']['constructor']($v['value0']['value0']), $v['value0']['value1']);
                    };
                    throw new \Exception('Failed pattern match at Control.Monad.Except.Trans line 136, column 10 - line 138, column 44: ' . var_dump([ $v['constructor']['name'] ]));
                })());
            }));
        }));
    };
    $monadThrowExceptT = function ($dictMonad)  use (&$PS__Control_Monad_Error_Class, &$monadExceptT, &$PS__Control_Semigroupoid, &$ExceptT, &$PS__Control_Applicative, &$PS__Data_Either) {
        return $PS__Control_Monad_Error_Class['MonadThrow'](function ()  use (&$monadExceptT, &$dictMonad) {
            return $monadExceptT($dictMonad);
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($ExceptT)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($PS__Data_Either['Left']['create'])));
    };
    $monadErrorExceptT = function ($dictMonad)  use (&$PS__Control_Monad_Error_Class, &$monadThrowExceptT, &$PS__Control_Bind, &$PS__Data_Either, &$PS__Control_Semigroupoid, &$PS__Control_Applicative) {
        return $PS__Control_Monad_Error_Class['MonadError'](function ()  use (&$monadThrowExceptT, &$dictMonad) {
            return $monadThrowExceptT($dictMonad);
        }, function ($v)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Data_Either, &$PS__Control_Semigroupoid, &$PS__Control_Applicative) {
            return function ($k)  use (&$PS__Control_Bind, &$dictMonad, &$v, &$PS__Data_Either, &$PS__Control_Semigroupoid, &$PS__Control_Applicative) {
                return $PS__Control_Bind['bind']($dictMonad['Bind1']())($v)($PS__Data_Either['either'](function ($a)  use (&$k) {
                    $v1 = $k($a);
                    return $v1;
                })($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Applicative['pure']($dictMonad['Applicative0']()))($PS__Data_Either['Right']['create'])));
            };
        });
    };
    $altExceptT = function ($dictSemigroup)  use (&$PS__Control_Alt, &$functorExceptT, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Data_Either, &$PS__Data_Semigroup) {
        return function ($dictMonad)  use (&$PS__Control_Alt, &$functorExceptT, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Data_Either, &$PS__Data_Semigroup, &$dictSemigroup) {
            return $PS__Control_Alt['Alt'](function ()  use (&$functorExceptT, &$dictMonad) {
                return $functorExceptT((($dictMonad['Bind1']())['Apply0']())['Functor0']());
            }, function ($v)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Control_Applicative, &$PS__Data_Either, &$PS__Data_Semigroup, &$dictSemigroup) {
                return function ($v1)  use (&$PS__Control_Bind, &$dictMonad, &$v, &$PS__Control_Applicative, &$PS__Data_Either, &$PS__Data_Semigroup, &$dictSemigroup) {
                    return $PS__Control_Bind['bind']($dictMonad['Bind1']())($v)(function ($v2)  use (&$PS__Control_Applicative, &$dictMonad, &$PS__Data_Either, &$PS__Control_Bind, &$v1, &$PS__Data_Semigroup, &$dictSemigroup) {
                        if ($v2['type'] === 'Right') {
                            return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($PS__Data_Either['Right']['constructor']($v2['value0']));
                        };
                        if ($v2['type'] === 'Left') {
                            return $PS__Control_Bind['bind']($dictMonad['Bind1']())($v1)(function ($v3)  use (&$PS__Control_Applicative, &$dictMonad, &$PS__Data_Either, &$PS__Data_Semigroup, &$dictSemigroup, &$v2) {
                                if ($v3['type'] === 'Right') {
                                    return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($PS__Data_Either['Right']['constructor']($v3['value0']));
                                };
                                if ($v3['type'] === 'Left') {
                                    return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($PS__Data_Either['Left']['constructor']($PS__Data_Semigroup['append']($dictSemigroup)($v2['value0'])($v3['value0'])));
                                };
                                throw new \Exception('Failed pattern match at Control.Monad.Except.Trans line 86, column 9 - line 88, column 49: ' . var_dump([ $v3['constructor']['name'] ]));
                            });
                        };
                        throw new \Exception('Failed pattern match at Control.Monad.Except.Trans line 82, column 5 - line 88, column 49: ' . var_dump([ $v2['constructor']['name'] ]));
                    });
                };
            });
        };
    };
    $plusExceptT = function ($dictMonoid)  use (&$PS__Control_Plus, &$altExceptT, &$PS__Control_Monad_Error_Class, &$monadThrowExceptT, &$PS__Data_Monoid) {
        return function ($dictMonad)  use (&$PS__Control_Plus, &$altExceptT, &$dictMonoid, &$PS__Control_Monad_Error_Class, &$monadThrowExceptT, &$PS__Data_Monoid) {
            return $PS__Control_Plus['Plus'](function ()  use (&$altExceptT, &$dictMonoid, &$dictMonad) {
                return $altExceptT($dictMonoid['Semigroup0']())($dictMonad);
            }, $PS__Control_Monad_Error_Class['throwError']($monadThrowExceptT($dictMonad))($PS__Data_Monoid['mempty']($dictMonoid)));
        };
    };
    $alternativeExceptT = function ($dictMonoid)  use (&$PS__Control_Alternative, &$applicativeExceptT, &$plusExceptT) {
        return function ($dictMonad)  use (&$PS__Control_Alternative, &$applicativeExceptT, &$plusExceptT, &$dictMonoid) {
            return $PS__Control_Alternative['Alternative'](function ()  use (&$applicativeExceptT, &$dictMonad) {
                return $applicativeExceptT($dictMonad);
            }, function ()  use (&$plusExceptT, &$dictMonoid, &$dictMonad) {
                return $plusExceptT($dictMonoid)($dictMonad);
            });
        };
    };
    $monadZeroExceptT = function ($dictMonoid)  use (&$PS__Control_MonadZero, &$alternativeExceptT, &$monadExceptT) {
        return function ($dictMonad)  use (&$PS__Control_MonadZero, &$alternativeExceptT, &$dictMonoid, &$monadExceptT) {
            return $PS__Control_MonadZero['MonadZero'](function ()  use (&$alternativeExceptT, &$dictMonoid, &$dictMonad) {
                return $alternativeExceptT($dictMonoid)($dictMonad);
            }, function ()  use (&$monadExceptT, &$dictMonad) {
                return $monadExceptT($dictMonad);
            });
        };
    };
    $monadPlusExceptT = function ($dictMonoid)  use (&$PS__Control_MonadPlus, &$monadZeroExceptT) {
        return function ($dictMonad)  use (&$PS__Control_MonadPlus, &$monadZeroExceptT, &$dictMonoid) {
            return $PS__Control_MonadPlus['MonadPlus'](function ()  use (&$monadZeroExceptT, &$dictMonoid, &$dictMonad) {
                return $monadZeroExceptT($dictMonoid)($dictMonad);
            });
        };
    };
    return [
        'ExceptT' => $ExceptT,
        'runExceptT' => $runExceptT,
        'withExceptT' => $withExceptT,
        'mapExceptT' => $mapExceptT,
        'except' => $except,
        'newtypeExceptT' => $newtypeExceptT,
        'functorExceptT' => $functorExceptT,
        'applyExceptT' => $applyExceptT,
        'applicativeExceptT' => $applicativeExceptT,
        'bindExceptT' => $bindExceptT,
        'monadExceptT' => $monadExceptT,
        'monadRecExceptT' => $monadRecExceptT,
        'altExceptT' => $altExceptT,
        'plusExceptT' => $plusExceptT,
        'alternativeExceptT' => $alternativeExceptT,
        'monadPlusExceptT' => $monadPlusExceptT,
        'monadZeroExceptT' => $monadZeroExceptT,
        'monadTransExceptT' => $monadTransExceptT,
        'monadEffectExceptT' => $monadEffectExceptT,
        'monadContExceptT' => $monadContExceptT,
        'monadThrowExceptT' => $monadThrowExceptT,
        'monadErrorExceptT' => $monadErrorExceptT,
        'monadAskExceptT' => $monadAskExceptT,
        'monadReaderExceptT' => $monadReaderExceptT,
        'monadStateExceptT' => $monadStateExceptT,
        'monadTellExceptT' => $monadTellExceptT,
        'monadWriterExceptT' => $monadWriterExceptT
    ];
})();
