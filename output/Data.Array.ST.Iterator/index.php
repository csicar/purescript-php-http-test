<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.ST/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.ST.Internal/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.ST.Ref/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Array.ST/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Array_ST_Iterator = (function ()  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Control_Applicative, &$PS__Data_Semiring, &$PS__Data_Functor, &$PS__Data_HeytingAlgebra, &$PS__Data_Array_ST, &$PS__Data_Function, &$PS__Control_Semigroupoid, &$PS__Data_Maybe) {
    $Iterator = [
        'constructor' => function ($value0, $value1) {
            return [
                'type' => 'Iterator',
                'value0' => $value0,
                'value1' => $value1
            ];
        },
        'create' => function ($value0)  use (&$Iterator) {
            return function ($value1)  use (&$Iterator, &$value0) {
                return $Iterator['constructor']($value0, $value1);
            };
        }
    ];
    $peek = function ($v)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Control_Applicative) {
        return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Control_Monad_ST_Internal['read']($v['value1']))(function ($v1)  use (&$PS__Control_Applicative, &$PS__Control_Monad_ST_Internal, &$v) {
            return $PS__Control_Applicative['pure']($PS__Control_Monad_ST_Internal['applicativeST'])($v['value0']($v1));
        });
    };
    $next = function ($v)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Semiring, &$PS__Control_Applicative) {
        return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Control_Monad_ST_Internal['read']($v['value1']))(function ($v1)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Semiring, &$v, &$PS__Control_Applicative) {
            return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Control_Monad_ST_Internal['modify'](function ($v2)  use (&$PS__Data_Semiring) {
                return $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($v2)(1);
            })($v['value1']))(function ($v2)  use (&$PS__Control_Applicative, &$PS__Control_Monad_ST_Internal, &$v, &$v1) {
                return $PS__Control_Applicative['pure']($PS__Control_Monad_ST_Internal['applicativeST'])($v['value0']($v1));
            });
        });
    };
    $pushWhile = function ($p)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Functor, &$PS__Data_HeytingAlgebra, &$peek, &$PS__Data_Array_ST, &$PS__Data_Function, &$next) {
        return function ($iter)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Functor, &$PS__Data_HeytingAlgebra, &$peek, &$p, &$PS__Data_Array_ST, &$PS__Data_Function, &$next) {
            return function ($array)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Functor, &$PS__Data_HeytingAlgebra, &$peek, &$iter, &$p, &$PS__Data_Array_ST, &$PS__Data_Function, &$next) {
                return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Control_Monad_ST_Internal['new'](false))(function ($v)  use (&$PS__Control_Monad_ST_Internal, &$PS__Data_Functor, &$PS__Data_HeytingAlgebra, &$PS__Control_Bind, &$peek, &$iter, &$p, &$PS__Data_Array_ST, &$array, &$PS__Data_Function, &$next) {
                    return $PS__Control_Monad_ST_Internal['while']($PS__Data_Functor['map']($PS__Control_Monad_ST_Internal['functorST'])($PS__Data_HeytingAlgebra['not']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean']))($PS__Control_Monad_ST_Internal['read']($v)))($PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($peek($iter))(function ($v1)  use (&$p, &$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST, &$array, &$PS__Data_Function, &$PS__Data_Functor, &$next, &$iter, &$v) {
                        if ($v1['type'] === 'Just' && $p($v1['value0'])) {
                            return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Array_ST['push']($v1['value0'])($array))(function ($v2)  use (&$PS__Data_Function, &$PS__Data_Functor, &$PS__Control_Monad_ST_Internal, &$next, &$iter) {
                                return $PS__Data_Function['apply']($PS__Data_Functor['void']($PS__Control_Monad_ST_Internal['functorST']))($next($iter));
                            });
                        };
                        return $PS__Data_Function['apply']($PS__Data_Functor['void']($PS__Control_Monad_ST_Internal['functorST']))($PS__Control_Monad_ST_Internal['write'](true)($v));
                    }));
                });
            };
        };
    };
    $pushAll = $pushWhile($PS__Data_Function['const'](true));
    $iterator = function ($f)  use (&$PS__Data_Functor, &$PS__Control_Monad_ST_Internal, &$Iterator) {
        return $PS__Data_Functor['map']($PS__Control_Monad_ST_Internal['functorST'])($Iterator['create']($f))($PS__Control_Monad_ST_Internal['new'](0));
    };
    $iterate = function ($iter)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Functor, &$PS__Data_HeytingAlgebra, &$next, &$PS__Data_Function) {
        return function ($f)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Functor, &$PS__Data_HeytingAlgebra, &$next, &$iter, &$PS__Data_Function) {
            return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Control_Monad_ST_Internal['new'](false))(function ($v)  use (&$PS__Control_Monad_ST_Internal, &$PS__Data_Functor, &$PS__Data_HeytingAlgebra, &$PS__Control_Bind, &$next, &$iter, &$f, &$PS__Data_Function) {
                return $PS__Control_Monad_ST_Internal['while']($PS__Data_Functor['map']($PS__Control_Monad_ST_Internal['functorST'])($PS__Data_HeytingAlgebra['not']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean']))($PS__Control_Monad_ST_Internal['read']($v)))($PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($next($iter))(function ($v1)  use (&$f, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Control_Monad_ST_Internal, &$v) {
                    if ($v1['type'] === 'Just') {
                        return $f($v1['value0']);
                    };
                    if ($v1['type'] === 'Nothing') {
                        return $PS__Data_Function['apply']($PS__Data_Functor['void']($PS__Control_Monad_ST_Internal['functorST']))($PS__Control_Monad_ST_Internal['write'](true)($v));
                    };
                    throw new \Exception('Failed pattern match at Data.Array.ST.Iterator line 42, column 5 - line 44, column 47: ' . var_dump([ $v1['constructor']['name'] ]));
                }));
            });
        };
    };
    $exhausted = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Functor['map']($PS__Control_Monad_ST_Internal['functorST'])($PS__Data_Maybe['isNothing']))($peek);
    return [
        'iterator' => $iterator,
        'iterate' => $iterate,
        'next' => $next,
        'peek' => $peek,
        'exhausted' => $exhausted,
        'pushWhile' => $pushWhile,
        'pushAll' => $pushAll
    ];
})();
