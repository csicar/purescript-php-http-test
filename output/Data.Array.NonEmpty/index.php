<?php
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Array/index.php');
require_once(dirname(__FILE__) . '/../Data.Array.NonEmpty.Internal/index.php');
require_once(dirname(__FILE__) . '/../Data.Bifunctor/index.php');
require_once(dirname(__FILE__) . '/../Data.Boolean/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable1/index.php');
require_once(dirname(__FILE__) . '/../Partial.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
require_once(dirname(__FILE__) . '/../Unsafe.Coerce/index.php');
$PS__Data_Array_NonEmpty = (function ()  use (&$PS__Unsafe_Coerce, &$PS__Control_Semigroupoid, &$PS__Data_Array, &$PS__Data_Eq, &$PS__Data_Bifunctor, &$PS__Data_Tuple, &$PS__Data_Function, &$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Boolean, &$PS__Control_Bind, &$PS__Data_Array_NonEmpty_Internal, &$PS__Data_Functor, &$PS__Data_Semigroup, &$PS__Partial_Unsafe, &$PS__Data_NonEmpty, &$PS__Data_Ring, &$PS__Data_Semiring, &$PS__Data_Unfoldable1) {
    $unsafeFromArrayF = $PS__Unsafe_Coerce['unsafeCoerce'];
    $unsafeFromArray = $PS__Unsafe_Coerce['unsafeCoerce'];
    $toArray = $PS__Unsafe_Coerce['unsafeCoerce'];
    $unionBy__prime = function ($eq)  use (&$PS__Control_Semigroupoid, &$unsafeFromArray, &$PS__Data_Array, &$toArray) {
        return function ($xs)  use (&$PS__Control_Semigroupoid, &$unsafeFromArray, &$PS__Data_Array, &$eq, &$toArray) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unsafeFromArray)($PS__Data_Array['unionBy']($eq)($toArray($xs)));
        };
    };
    $union__prime = function ($dictEq)  use (&$unionBy__prime, &$PS__Data_Eq) {
        return $unionBy__prime($PS__Data_Eq['eq']($dictEq));
    };
    $unionBy = function ($eq)  use (&$PS__Control_Semigroupoid, &$unionBy__prime, &$toArray) {
        return function ($xs)  use (&$PS__Control_Semigroupoid, &$unionBy__prime, &$eq, &$toArray) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unionBy__prime($eq)($xs))($toArray);
        };
    };
    $union = function ($dictEq)  use (&$unionBy, &$PS__Data_Eq) {
        return $unionBy($PS__Data_Eq['eq']($dictEq));
    };
    $unzip = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Bifunctor['bimap']($PS__Data_Tuple['bifunctorTuple'])($unsafeFromArray)($unsafeFromArray))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Array['unzip'])($toArray));
    $updateAt = function ($i)  use (&$PS__Control_Semigroupoid, &$unsafeFromArrayF, &$PS__Data_Array, &$toArray) {
        return function ($x)  use (&$PS__Control_Semigroupoid, &$unsafeFromArrayF, &$PS__Data_Array, &$i, &$toArray) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unsafeFromArrayF)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Array['updateAt']($i)($x))($toArray));
        };
    };
    $zip = function ($xs)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Array, &$toArray) {
        return function ($ys)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Array, &$toArray, &$xs) {
            return $PS__Data_Function['apply']($unsafeFromArray)($PS__Data_Array['zip']($toArray($xs))($toArray($ys)));
        };
    };
    $zipWith = function ($f)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Array, &$toArray) {
        return function ($xs)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Array, &$f, &$toArray) {
            return function ($ys)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Array, &$f, &$toArray, &$xs) {
                return $PS__Data_Function['apply']($unsafeFromArray)($PS__Data_Array['zipWith']($f)($toArray($xs))($toArray($ys)));
            };
        };
    };
    $zipWithA = function ($dictApplicative)  use (&$PS__Data_Function, &$unsafeFromArrayF, &$PS__Data_Array, &$toArray) {
        return function ($f)  use (&$PS__Data_Function, &$unsafeFromArrayF, &$PS__Data_Array, &$dictApplicative, &$toArray) {
            return function ($xs)  use (&$PS__Data_Function, &$unsafeFromArrayF, &$PS__Data_Array, &$dictApplicative, &$f, &$toArray) {
                return function ($ys)  use (&$PS__Data_Function, &$unsafeFromArrayF, &$PS__Data_Array, &$dictApplicative, &$f, &$toArray, &$xs) {
                    return $PS__Data_Function['apply']($unsafeFromArrayF)($PS__Data_Array['zipWithA']($dictApplicative)($f)($toArray($xs))($toArray($ys)));
                };
            };
        };
    };
    $some = function ($dictAlternative)  use (&$PS__Control_Semigroupoid, &$unsafeFromArrayF, &$PS__Data_Array) {
        return function ($dictLazy)  use (&$PS__Control_Semigroupoid, &$unsafeFromArrayF, &$PS__Data_Array, &$dictAlternative) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unsafeFromArrayF)($PS__Data_Array['some']($dictAlternative)($dictLazy));
        };
    };
    $snoc__prime = function ($xs)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Array) {
        return function ($x)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Array, &$xs) {
            return $PS__Data_Function['apply']($unsafeFromArray)($PS__Data_Array['snoc']($xs)($x));
        };
    };
    $snoc = function ($xs)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Array, &$toArray) {
        return function ($x)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Array, &$toArray, &$xs) {
            return $PS__Data_Function['apply']($unsafeFromArray)($PS__Data_Array['snoc']($toArray($xs))($x));
        };
    };
    $singleton = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unsafeFromArray)($PS__Data_Array['singleton']);
    $replicate = function ($i)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Array, &$PS__Data_Ord) {
        return function ($x)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Array, &$PS__Data_Ord, &$i) {
            return $PS__Data_Function['apply']($unsafeFromArray)($PS__Data_Array['replicate']($PS__Data_Ord['max']($PS__Data_Ord['ordInt'])(1)($i))($x));
        };
    };
    $range = function ($x)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Array) {
        return function ($y)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Array, &$x) {
            return $PS__Data_Function['apply']($unsafeFromArray)($PS__Data_Array['range']($x)($y));
        };
    };
    $modifyAt = function ($i)  use (&$PS__Control_Semigroupoid, &$unsafeFromArrayF, &$PS__Data_Array, &$toArray) {
        return function ($f)  use (&$PS__Control_Semigroupoid, &$unsafeFromArrayF, &$PS__Data_Array, &$i, &$toArray) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unsafeFromArrayF)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Array['modifyAt']($i)($f))($toArray));
        };
    };
    $intersectBy__prime = function ($eq)  use (&$PS__Data_Array, &$toArray) {
        return function ($xs)  use (&$PS__Data_Array, &$eq, &$toArray) {
            return $PS__Data_Array['intersectBy']($eq)($toArray($xs));
        };
    };
    $intersectBy = function ($eq)  use (&$PS__Control_Semigroupoid, &$intersectBy__prime, &$toArray) {
        return function ($xs)  use (&$PS__Control_Semigroupoid, &$intersectBy__prime, &$eq, &$toArray) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($intersectBy__prime($eq)($xs))($toArray);
        };
    };
    $intersect__prime = function ($dictEq)  use (&$intersectBy__prime, &$PS__Data_Eq) {
        return $intersectBy__prime($PS__Data_Eq['eq']($dictEq));
    };
    $intersect = function ($dictEq)  use (&$intersectBy, &$PS__Data_Eq) {
        return $intersectBy($PS__Data_Eq['eq']($dictEq));
    };
    $insertAt = function ($i)  use (&$PS__Control_Semigroupoid, &$unsafeFromArrayF, &$PS__Data_Array, &$toArray) {
        return function ($x)  use (&$PS__Control_Semigroupoid, &$unsafeFromArrayF, &$PS__Data_Array, &$i, &$toArray) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unsafeFromArrayF)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Array['insertAt']($i)($x))($toArray));
        };
    };
    $fromFoldable1 = function ($dictFoldable1)  use (&$PS__Control_Semigroupoid, &$unsafeFromArray, &$PS__Data_Array) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unsafeFromArray)($PS__Data_Array['fromFoldable']($dictFoldable1['Foldable0']()));
    };
    $fromArray = function ($xs)  use (&$PS__Data_Ord, &$PS__Data_Array, &$PS__Data_Maybe, &$unsafeFromArray, &$PS__Data_Boolean) {
        if ($PS__Data_Ord['greaterThan']($PS__Data_Ord['ordInt'])($PS__Data_Array['length']($xs))(0)) {
            return $PS__Data_Maybe['Just']['constructor']($unsafeFromArray($xs));
        };
        if ($PS__Data_Boolean['otherwise']) {
            return $PS__Data_Maybe['Nothing']();
        };
        throw new \Exception('Failed pattern match at Data.Array.NonEmpty line 134, column 1 - line 134, column 58: ' . var_dump([ $xs['constructor']['name'] ]));
    };
    $fromFoldable = function ($dictFoldable)  use (&$PS__Control_Semigroupoid, &$fromArray, &$PS__Data_Array) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($fromArray)($PS__Data_Array['fromFoldable']($dictFoldable));
    };
    $difference__prime = function ($dictEq)  use (&$PS__Data_Function, &$PS__Data_Array, &$toArray) {
        return function ($xs)  use (&$PS__Data_Function, &$PS__Data_Array, &$dictEq, &$toArray) {
            return $PS__Data_Function['apply']($PS__Data_Array['difference']($dictEq))($toArray($xs));
        };
    };
    $cons__prime = function ($x)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Array) {
        return function ($xs)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Array, &$x) {
            return $PS__Data_Function['apply']($unsafeFromArray)($PS__Data_Array['cons']($x)($xs));
        };
    };
    $fromNonEmpty = function ($v)  use (&$cons__prime) {
        return $cons__prime($v['value0'])($v['value1']);
    };
    $concatMap = $PS__Data_Function['flip']($PS__Control_Bind['bind']($PS__Data_Array_NonEmpty_Internal['bindNonEmptyArray']));
    $concat = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unsafeFromArray)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Array['concat'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($toArray)($PS__Data_Functor['map']($PS__Data_Array_NonEmpty_Internal['functorNonEmptyArray'])($toArray))));
    $appendArray = function ($xs)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Semigroup, &$toArray) {
        return function ($ys)  use (&$PS__Data_Function, &$unsafeFromArray, &$PS__Data_Semigroup, &$toArray, &$xs) {
            return $PS__Data_Function['apply']($unsafeFromArray)($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupArray'])($toArray($xs))($ys));
        };
    };
    $alterAt = function ($i)  use (&$PS__Control_Semigroupoid, &$PS__Data_Array, &$toArray) {
        return function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Array, &$i, &$toArray) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Array['alterAt']($i)($f))($toArray);
        };
    };
    $adaptMaybe = function ($f)  use (&$PS__Data_Function, &$PS__Partial_Unsafe, &$PS__Control_Semigroupoid, &$PS__Data_Maybe, &$toArray) {
        return $PS__Data_Function['apply']($PS__Partial_Unsafe['unsafePartial'])(function ($dictPartial)  use (&$PS__Control_Semigroupoid, &$PS__Data_Maybe, &$f, &$toArray) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['fromJust']($dictPartial))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($toArray));
        });
    };
    $head = $adaptMaybe($PS__Data_Array['head']);
    $init = $adaptMaybe($PS__Data_Array['init']);
    $last = $adaptMaybe($PS__Data_Array['last']);
    $tail = $adaptMaybe($PS__Data_Array['tail']);
    $uncons = $adaptMaybe($PS__Data_Array['uncons']);
    $toNonEmpty = $PS__Control_Semigroupoid['composeFlipped']($PS__Control_Semigroupoid['semigroupoidFn'])($uncons)(function ($v)  use (&$PS__Data_NonEmpty) {
        return $PS__Data_NonEmpty['NonEmpty']['constructor']($v['head'], $v['tail']);
    });
    $unsnoc = $adaptMaybe($PS__Data_Array['unsnoc']);
    $adaptAny = function ($f)  use (&$PS__Control_Semigroupoid, &$toArray) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($toArray);
    };
    $catMaybes = $adaptAny($PS__Data_Array['catMaybes']);
    $__delete = function ($dictEq)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return function ($x)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array, &$dictEq) {
            return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['delete']($dictEq)($x));
        };
    };
    $deleteAt = function ($i)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['deleteAt']($i));
    };
    $deleteBy = function ($f)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return function ($x)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array, &$f) {
            return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['deleteBy']($f)($x));
        };
    };
    $difference = function ($dictEq)  use (&$PS__Data_Function, &$adaptAny, &$difference__prime) {
        return function ($xs)  use (&$PS__Data_Function, &$adaptAny, &$difference__prime, &$dictEq) {
            return $PS__Data_Function['apply']($adaptAny)($difference__prime($dictEq)($xs));
        };
    };
    $drop = function ($i)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['drop']($i));
    };
    $dropEnd = function ($i)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['dropEnd']($i));
    };
    $dropWhile = function ($f)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['dropWhile']($f));
    };
    $elemIndex = function ($dictEq)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return function ($x)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array, &$dictEq) {
            return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['elemIndex']($dictEq)($x));
        };
    };
    $elemLastIndex = function ($dictEq)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return function ($x)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array, &$dictEq) {
            return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['elemLastIndex']($dictEq)($x));
        };
    };
    $filter = function ($f)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['filter']($f));
    };
    $filterA = function ($dictApplicative)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return function ($f)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array, &$dictApplicative) {
            return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['filterA']($dictApplicative)($f));
        };
    };
    $findIndex = function ($x)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['findIndex']($x));
    };
    $findLastIndex = function ($x)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['findLastIndex']($x));
    };
    $foldM = function ($dictMonad)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return function ($f)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array, &$dictMonad) {
            return function ($acc)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array, &$dictMonad, &$f) {
                return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['foldM']($dictMonad)($f)($acc));
            };
        };
    };
    $foldRecM = function ($dictMonadRec)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return function ($f)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array, &$dictMonadRec) {
            return function ($acc)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array, &$dictMonadRec, &$f) {
                return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['foldRecM']($dictMonadRec)($f)($acc));
            };
        };
    };
    $index = $adaptAny($PS__Data_Array['index']);
    $length = $adaptAny($PS__Data_Array['length']);
    $mapMaybe = function ($f)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['mapMaybe']($f));
    };
    $partition = function ($f)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['partition']($f));
    };
    $slice = function ($start)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return function ($end)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array, &$start) {
            return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['slice']($start)($end));
        };
    };
    $span = function ($f)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['span']($f));
    };
    $take = function ($i)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['take']($i));
    };
    $takeEnd = function ($i)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['takeEnd']($i));
    };
    $takeWhile = function ($f)  use (&$PS__Data_Function, &$adaptAny, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($adaptAny)($PS__Data_Array['takeWhile']($f));
    };
    $toUnfoldable = function ($dictUnfoldable)  use (&$adaptAny, &$PS__Data_Array) {
        return $adaptAny($PS__Data_Array['toUnfoldable']($dictUnfoldable));
    };
    $unsafeAdapt = function ($f)  use (&$PS__Control_Semigroupoid, &$unsafeFromArray, &$adaptAny) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($unsafeFromArray)($adaptAny($f));
    };
    $cons = function ($x)  use (&$PS__Data_Function, &$unsafeAdapt, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($unsafeAdapt)($PS__Data_Array['cons']($x));
    };
    $insert = function ($dictOrd)  use (&$PS__Data_Function, &$unsafeAdapt, &$PS__Data_Array) {
        return function ($x)  use (&$PS__Data_Function, &$unsafeAdapt, &$PS__Data_Array, &$dictOrd) {
            return $PS__Data_Function['apply']($unsafeAdapt)($PS__Data_Array['insert']($dictOrd)($x));
        };
    };
    $insertBy = function ($f)  use (&$PS__Data_Function, &$unsafeAdapt, &$PS__Data_Array) {
        return function ($x)  use (&$PS__Data_Function, &$unsafeAdapt, &$PS__Data_Array, &$f) {
            return $PS__Data_Function['apply']($unsafeAdapt)($PS__Data_Array['insertBy']($f)($x));
        };
    };
    $modifyAtIndices = function ($dictFoldable)  use (&$PS__Data_Function, &$unsafeAdapt, &$PS__Data_Array) {
        return function ($is)  use (&$PS__Data_Function, &$unsafeAdapt, &$PS__Data_Array, &$dictFoldable) {
            return function ($f)  use (&$PS__Data_Function, &$unsafeAdapt, &$PS__Data_Array, &$dictFoldable, &$is) {
                return $PS__Data_Function['apply']($unsafeAdapt)($PS__Data_Array['modifyAtIndices']($dictFoldable)($is)($f));
            };
        };
    };
    $nub = function ($dictOrd)  use (&$unsafeAdapt, &$PS__Data_Array) {
        return $unsafeAdapt($PS__Data_Array['nub']($dictOrd));
    };
    $nubBy = function ($f)  use (&$PS__Data_Function, &$unsafeAdapt, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($unsafeAdapt)($PS__Data_Array['nubBy']($f));
    };
    $nubByEq = function ($f)  use (&$PS__Data_Function, &$unsafeAdapt, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($unsafeAdapt)($PS__Data_Array['nubByEq']($f));
    };
    $nubEq = function ($dictEq)  use (&$unsafeAdapt, &$PS__Data_Array) {
        return $unsafeAdapt($PS__Data_Array['nubEq']($dictEq));
    };
    $reverse = $unsafeAdapt($PS__Data_Array['reverse']);
    $sort = function ($dictOrd)  use (&$unsafeAdapt, &$PS__Data_Array) {
        return $unsafeAdapt($PS__Data_Array['sort']($dictOrd));
    };
    $sortBy = function ($f)  use (&$PS__Data_Function, &$unsafeAdapt, &$PS__Data_Array) {
        return $PS__Data_Function['apply']($unsafeAdapt)($PS__Data_Array['sortBy']($f));
    };
    $sortWith = function ($dictOrd)  use (&$PS__Data_Function, &$unsafeAdapt, &$PS__Data_Array) {
        return function ($f)  use (&$PS__Data_Function, &$unsafeAdapt, &$PS__Data_Array, &$dictOrd) {
            return $PS__Data_Function['apply']($unsafeAdapt)($PS__Data_Array['sortWith']($dictOrd)($f));
        };
    };
    $updateAtIndices = function ($dictFoldable)  use (&$PS__Data_Function, &$unsafeAdapt, &$PS__Data_Array) {
        return function ($pairs)  use (&$PS__Data_Function, &$unsafeAdapt, &$PS__Data_Array, &$dictFoldable) {
            return $PS__Data_Function['apply']($unsafeAdapt)($PS__Data_Array['updateAtIndices']($dictFoldable)($pairs));
        };
    };
    $unsafeIndex = function ($dictPartial)  use (&$adaptAny, &$PS__Data_Array) {
        return $adaptAny($PS__Data_Array['unsafeIndex']($dictPartial));
    };
    $toUnfoldable1 = function ($dictUnfoldable1)  use (&$length, &$PS__Data_Function, &$PS__Data_Tuple, &$PS__Partial_Unsafe, &$unsafeIndex, &$PS__Data_Ord, &$PS__Data_Ring, &$PS__Data_Maybe, &$PS__Data_Semiring, &$PS__Data_Unfoldable1) {
        return function ($xs)  use (&$length, &$PS__Data_Function, &$PS__Data_Tuple, &$PS__Partial_Unsafe, &$unsafeIndex, &$PS__Data_Ord, &$PS__Data_Ring, &$PS__Data_Maybe, &$PS__Data_Semiring, &$PS__Data_Unfoldable1, &$dictUnfoldable1) {
            $len = $length($xs);
            $f = function ($i)  use (&$PS__Data_Function, &$PS__Data_Tuple, &$PS__Partial_Unsafe, &$unsafeIndex, &$xs, &$PS__Data_Ord, &$PS__Data_Ring, &$len, &$PS__Data_Maybe, &$PS__Data_Semiring) {
                return $PS__Data_Function['apply']($PS__Data_Tuple['Tuple']['create']($PS__Partial_Unsafe['unsafePartial'](function ($dictPartial)  use (&$unsafeIndex) {
                    return $unsafeIndex($dictPartial);
                })($xs)($i)))((function ()  use (&$PS__Data_Ord, &$i, &$PS__Data_Ring, &$len, &$PS__Data_Maybe, &$PS__Data_Semiring) {
                    $__local_var__38 = $PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($i)($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($len)(1));
                    if ($__local_var__38) {
                        return $PS__Data_Maybe['Just']['constructor']($PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($i)(1));
                    };
                    return $PS__Data_Maybe['Nothing']();
                })());
            };
            return $PS__Data_Unfoldable1['unfoldr1']($dictUnfoldable1)($f)(0);
        };
    };
    return [
        'fromArray' => $fromArray,
        'fromNonEmpty' => $fromNonEmpty,
        'toArray' => $toArray,
        'toNonEmpty' => $toNonEmpty,
        'fromFoldable' => $fromFoldable,
        'fromFoldable1' => $fromFoldable1,
        'toUnfoldable' => $toUnfoldable,
        'toUnfoldable1' => $toUnfoldable1,
        'singleton' => $singleton,
        'range' => $range,
        'replicate' => $replicate,
        'some' => $some,
        'length' => $length,
        'cons' => $cons,
        'cons\'' => $cons__prime,
        'snoc' => $snoc,
        'snoc\'' => $snoc__prime,
        'appendArray' => $appendArray,
        'insert' => $insert,
        'insertBy' => $insertBy,
        'head' => $head,
        'last' => $last,
        'tail' => $tail,
        'init' => $init,
        'uncons' => $uncons,
        'unsnoc' => $unsnoc,
        'index' => $index,
        'elemIndex' => $elemIndex,
        'elemLastIndex' => $elemLastIndex,
        'findIndex' => $findIndex,
        'findLastIndex' => $findLastIndex,
        'insertAt' => $insertAt,
        'deleteAt' => $deleteAt,
        'updateAt' => $updateAt,
        'updateAtIndices' => $updateAtIndices,
        'modifyAt' => $modifyAt,
        'modifyAtIndices' => $modifyAtIndices,
        'alterAt' => $alterAt,
        'reverse' => $reverse,
        'concat' => $concat,
        'concatMap' => $concatMap,
        'filter' => $filter,
        'partition' => $partition,
        'filterA' => $filterA,
        'mapMaybe' => $mapMaybe,
        'catMaybes' => $catMaybes,
        'sort' => $sort,
        'sortBy' => $sortBy,
        'sortWith' => $sortWith,
        'slice' => $slice,
        'take' => $take,
        'takeEnd' => $takeEnd,
        'takeWhile' => $takeWhile,
        'drop' => $drop,
        'dropEnd' => $dropEnd,
        'dropWhile' => $dropWhile,
        'span' => $span,
        'nub' => $nub,
        'nubBy' => $nubBy,
        'nubEq' => $nubEq,
        'nubByEq' => $nubByEq,
        'union' => $union,
        'union\'' => $union__prime,
        'unionBy' => $unionBy,
        'unionBy\'' => $unionBy__prime,
        'delete' => $__delete,
        'deleteBy' => $deleteBy,
        'difference' => $difference,
        'difference\'' => $difference__prime,
        'intersect' => $intersect,
        'intersect\'' => $intersect__prime,
        'intersectBy' => $intersectBy,
        'intersectBy\'' => $intersectBy__prime,
        'zipWith' => $zipWith,
        'zipWithA' => $zipWithA,
        'zip' => $zip,
        'unzip' => $unzip,
        'foldM' => $foldM,
        'foldRecM' => $foldRecM,
        'unsafeIndex' => $unsafeIndex
    ];
})();
