<?php
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Effect/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Effect_Ref = (function ()  use (&$PS__Data_Function, &$PS__Data_Functor, &$PS__Effect) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $modify = function ($f)  use (&$__foreign) {
        return $__foreign['modify\''](function ($s)  use (&$f) {
            $s__prime = $f($s);
            return [
                'state' => $s__prime,
                'value' => $s__prime
            ];
        });
    };
    $modify_ = function ($f)  use (&$PS__Data_Function, &$PS__Data_Functor, &$PS__Effect, &$modify) {
        return function ($s)  use (&$PS__Data_Function, &$PS__Data_Functor, &$PS__Effect, &$modify, &$f) {
            return $PS__Data_Function['apply']($PS__Data_Functor['void']($PS__Effect['functorEffect']))($modify($f)($s));
        };
    };
    return [
        'modify' => $modify,
        'modify_' => $modify_,
        'new' => $__foreign['new'],
        'read' => $__foreign['read'],
        'modify\'' => $__foreign['modify\''],
        'write' => $__foreign['write']
    ];
})();
