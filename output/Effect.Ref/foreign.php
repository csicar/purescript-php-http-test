<?php

$exports["new"] = function ($val) {
  return function () use (&$val) {
    return [ "value" => $val ];
  };
};

$exports["read"] = function (&$ref) {
  return function () use (&$ref) {
    return $ref["value"];
  };
};

$exports["modify'"] = function ($f) {
  return function (&$ref) use (&$f) {
    return function () use (&$f, &$ref) {
      $t = $f($ref["value"]);
      $ref["value"] = $t["state"];
      return $t["value"];
    };
  };
};

$exports["write"] = function ($val) {
  return function (&$ref) use (&$val) {
    return function () use (&$val, &$ref) {
      $ref["value"] = $val;
      return [];
    };
  };
};
