<?php

$exports['fold1Impl'] = function ($f) {
  return function ($xs) use (&$f) {
    $acc = $xs[0];
    $len = count($xs);
    for ($i = 1; $i < $len; $i++) {
      $acc = $f($acc)($xs[$i]);
    }
    return $acc;
  };
};

$exports['traverse1Impl'] = (function () {
  $Cont_Id = function(){};
  $Cont = function ($fn) use (&$Cont_Id) {
    return ["fn" => $fn, "Cont_Id" => $Cont_Id];
  };

  $ConsCell = function ($head, $tail) {
    return ["head" => $head, "tail" => $tail];
  };

  $finalCell = function ($head) use (&$ConsCell) {
    return $ConsCell($head, []);
  };

  $consList = function($x) use (&$ConsCell) {
    return function ($xs) use (&$ConsCell, &$x) {
      return $ConsCell($x, $xs);
    };
  };

  $listToArray = function($list) {
    $result = [];
    $count = 0;
    $xs = $list;
    while ($xs !== []) {
      $result[$count++] = $xs['head'];
      $xs = $xs['tail'];
    }
    return $result;
  };

  return function ($apply) use (&$consList, &$x, &$ys, &$finalCell, &$Cont, &$Cont_Id) {
    return function ($map) use (&$apply, &$consList, &$x, &$ys, &$finalCell, &$Cont, &$Cont_Id) {
      return function ($f) use (&$apply, &$map, &$consList, &$x, &$ys, &$finalCell, &$Cont, &$Cont_Id) {
        $buildFrom = function ($x, $ys) use (&$apply, &$map, &$consList, &$f) {
          return $apply($map($consList)($f($x)))($ys);
        };

        $go = function ($acc, $currentLen, $xs) use (&$Cont, &$buildFrom, &$go) {
          if ($currentLen === 0) {
            return $acc;
          } else {
            $last = $xs[$currentLen - 1];
            return $Cont(function () {
              $built = $go($buildFrom($last, $acc), $currentLen - 1, $xs);
              return $built;
            });
          }
        };

        return function ($array) use (&$map, &$finalCell, &$f, &$go, &$Cont_Id) {
          $acc = $map($finalCell)($f($array[count($array) - 1]));
          $result = $go($acc, count($array) - 1, $array);
          while (is_array($result) && $result['Cont_Id'] === $Cont_Id) {
            $result = $result['fn']();
          }

          return $map($listToArray)($result);
        };
      };
    };
  };
})();
