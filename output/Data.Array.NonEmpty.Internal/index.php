<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.FoldableWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.FunctorWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.TraversableWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable1/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Array_NonEmpty_Internal = (function ()  use (&$PS__Data_Unfoldable1, &$PS__Data_TraversableWithIndex, &$PS__Data_Traversable, &$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Ord, &$PS__Control_Monad, &$PS__Data_FunctorWithIndex, &$PS__Data_Functor, &$PS__Data_FoldableWithIndex, &$PS__Data_Foldable, &$PS__Data_Semigroup_Foldable, &$PS__Data_Semigroup_Traversable, &$PS__Control_Apply, &$PS__Data_Eq, &$PS__Control_Bind, &$PS__Control_Applicative, &$PS__Control_Alt) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $NonEmptyArray = function ($x) {
        return $x;
    };
    $unfoldable1NonEmptyArray = $PS__Data_Unfoldable1['unfoldable1Array'];
    $traversableWithIndexNonEmptyArray = $PS__Data_TraversableWithIndex['traversableWithIndexArray'];
    $traversableNonEmptyArray = $PS__Data_Traversable['traversableArray'];
    $showNonEmptyArray = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(NonEmptyArray ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Show['showArray']($dictShow))($v))(')'));
        });
    };
    $semigroupNonEmptyArray = $PS__Data_Semigroup['semigroupArray'];
    $ordNonEmptyArray = function ($dictOrd)  use (&$PS__Data_Ord) {
        return $PS__Data_Ord['ordArray']($dictOrd);
    };
    $ord1NonEmptyArray = $PS__Data_Ord['ord1Array'];
    $monadNonEmptyArray = $PS__Control_Monad['monadArray'];
    $functorWithIndexNonEmptyArray = $PS__Data_FunctorWithIndex['functorWithIndexArray'];
    $functorNonEmptyArray = $PS__Data_Functor['functorArray'];
    $foldableWithIndexNonEmptyArray = $PS__Data_FoldableWithIndex['foldableWithIndexArray'];
    $foldableNonEmptyArray = $PS__Data_Foldable['foldableArray'];
    $foldable1NonEmptyArray = $PS__Data_Semigroup_Foldable['Foldable1'](function ()  use (&$foldableNonEmptyArray) {
        return $foldableNonEmptyArray;
    }, function ($dictSemigroup)  use (&$__foreign, &$PS__Data_Semigroup) {
        return $__foreign['fold1Impl']($PS__Data_Semigroup['append']($dictSemigroup));
    }, function ($dictSemigroup)  use (&$PS__Data_Semigroup_Foldable, &$foldable1NonEmptyArray, &$functorNonEmptyArray) {
        return $PS__Data_Semigroup_Foldable['foldMap1Default']($foldable1NonEmptyArray)($functorNonEmptyArray)($dictSemigroup);
    });
    $traversable1NonEmptyArray = $PS__Data_Semigroup_Traversable['Traversable1'](function ()  use (&$foldable1NonEmptyArray) {
        return $foldable1NonEmptyArray;
    }, function ()  use (&$traversableNonEmptyArray) {
        return $traversableNonEmptyArray;
    }, function ($dictApply)  use (&$PS__Data_Semigroup_Traversable, &$traversable1NonEmptyArray) {
        return $PS__Data_Semigroup_Traversable['sequence1Default']($traversable1NonEmptyArray)($dictApply);
    }, function ($dictApply)  use (&$__foreign, &$PS__Control_Apply, &$PS__Data_Functor) {
        return $__foreign['traverse1Impl']($PS__Control_Apply['apply']($dictApply))($PS__Data_Functor['map']($dictApply['Functor0']()));
    });
    $eqNonEmptyArray = function ($dictEq)  use (&$PS__Data_Eq) {
        return $PS__Data_Eq['eqArray']($dictEq);
    };
    $eq1NonEmptyArray = $PS__Data_Eq['eq1Array'];
    $bindNonEmptyArray = $PS__Control_Bind['bindArray'];
    $applyNonEmptyArray = $PS__Control_Apply['applyArray'];
    $applicativeNonEmptyArray = $PS__Control_Applicative['applicativeArray'];
    $altNonEmptyArray = $PS__Control_Alt['altArray'];
    return [
        'showNonEmptyArray' => $showNonEmptyArray,
        'eqNonEmptyArray' => $eqNonEmptyArray,
        'eq1NonEmptyArray' => $eq1NonEmptyArray,
        'ordNonEmptyArray' => $ordNonEmptyArray,
        'ord1NonEmptyArray' => $ord1NonEmptyArray,
        'semigroupNonEmptyArray' => $semigroupNonEmptyArray,
        'functorNonEmptyArray' => $functorNonEmptyArray,
        'functorWithIndexNonEmptyArray' => $functorWithIndexNonEmptyArray,
        'foldableNonEmptyArray' => $foldableNonEmptyArray,
        'foldableWithIndexNonEmptyArray' => $foldableWithIndexNonEmptyArray,
        'foldable1NonEmptyArray' => $foldable1NonEmptyArray,
        'unfoldable1NonEmptyArray' => $unfoldable1NonEmptyArray,
        'traversableNonEmptyArray' => $traversableNonEmptyArray,
        'traversableWithIndexNonEmptyArray' => $traversableWithIndexNonEmptyArray,
        'traversable1NonEmptyArray' => $traversable1NonEmptyArray,
        'applyNonEmptyArray' => $applyNonEmptyArray,
        'applicativeNonEmptyArray' => $applicativeNonEmptyArray,
        'bindNonEmptyArray' => $bindNonEmptyArray,
        'monadNonEmptyArray' => $monadNonEmptyArray,
        'altNonEmptyArray' => $altNonEmptyArray
    ];
})();
