<?php
require_once(dirname(__FILE__) . '/../Node.Buffer/index.php');
$PS__Node_Buffer_Unsafe = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    return [
        'slice' => $__foreign['slice']
    ];
})();
