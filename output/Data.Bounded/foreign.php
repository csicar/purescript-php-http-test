<?php

$exports["topInt"] = 2147483647;
$exports["bottomInt"] = -2147483648;

$exports["topChar"] = chr(65535);
$exports["bottomChar"] = chr(0);

$exports["topNumber"] = INF;
$exports["bottomNumber"] = -INF;
