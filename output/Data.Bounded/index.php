<?php
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
$PS__Data_Bounded = (function ()  use (&$PS__Data_Ord, &$PS__Data_Unit, &$PS__Data_Ordering) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Bounded = function ($Ord0, $bottom, $top) {
        return [
            'Ord0' => $Ord0,
            'bottom' => $bottom,
            'top' => $top
        ];
    };
    $top = function ($dict) {
        return $dict['top'];
    };
    $boundedUnit = $Bounded(function ()  use (&$PS__Data_Ord) {
        return $PS__Data_Ord['ordUnit'];
    }, $PS__Data_Unit['unit'], $PS__Data_Unit['unit']);
    $boundedOrdering = $Bounded(function ()  use (&$PS__Data_Ord) {
        return $PS__Data_Ord['ordOrdering'];
    }, $PS__Data_Ordering['LT'](), $PS__Data_Ordering['GT']());
    $boundedNumber = $Bounded(function ()  use (&$PS__Data_Ord) {
        return $PS__Data_Ord['ordNumber'];
    }, $__foreign['bottomNumber'], $__foreign['topNumber']);
    $boundedInt = $Bounded(function ()  use (&$PS__Data_Ord) {
        return $PS__Data_Ord['ordInt'];
    }, $__foreign['bottomInt'], $__foreign['topInt']);
    $boundedChar = $Bounded(function ()  use (&$PS__Data_Ord) {
        return $PS__Data_Ord['ordChar'];
    }, $__foreign['bottomChar'], $__foreign['topChar']);
    $boundedBoolean = $Bounded(function ()  use (&$PS__Data_Ord) {
        return $PS__Data_Ord['ordBoolean'];
    }, false, true);
    $bottom = function ($dict) {
        return $dict['bottom'];
    };
    return [
        'Bounded' => $Bounded,
        'bottom' => $bottom,
        'top' => $top,
        'boundedBoolean' => $boundedBoolean,
        'boundedInt' => $boundedInt,
        'boundedChar' => $boundedChar,
        'boundedOrdering' => $boundedOrdering,
        'boundedUnit' => $boundedUnit,
        'boundedNumber' => $boundedNumber
    ];
})();
