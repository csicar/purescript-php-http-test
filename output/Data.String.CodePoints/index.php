<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Array/index.php');
require_once(dirname(__FILE__) . '/../Data.Boolean/index.php');
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Enum/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.EuclideanRing/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Int/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.String.CodeUnits/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Common/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Pattern/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_String_CodePoints = (function ()  use (&$PS__Data_Semiring, &$PS__Data_Ring, &$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_String_Common, &$PS__Data_Int, &$PS__Data_HeytingAlgebra, &$PS__Data_Ord, &$PS__Data_String_CodeUnits, &$PS__Data_Maybe, &$PS__Data_Enum, &$PS__Data_String_Unsafe, &$PS__Data_Functor, &$PS__Data_Tuple, &$PS__Data_Unfoldable, &$PS__Control_Semigroupoid, &$PS__Data_Array, &$PS__Data_Bounded, &$PS__Data_EuclideanRing, &$PS__Data_Eq, &$PS__Data_Boolean) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $CodePoint = function ($x) {
        return $x;
    };
    $unsurrogate = function ($lead)  use (&$PS__Data_Semiring, &$PS__Data_Ring) {
        return function ($trail)  use (&$PS__Data_Semiring, &$PS__Data_Ring, &$lead) {
            return $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($PS__Data_Semiring['mul']($PS__Data_Semiring['semiringInt'])($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($lead)(55296))(1024))($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($trail)(56320)))(65536);
        };
    };
    $showCodePoint = $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_String_Common, &$PS__Data_Int) {
        return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(CodePoint 0x')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_String_Common['toUpper']($PS__Data_Int['toStringAs']($PS__Data_Int['hexadecimal'])($v)))(')'));
    });
    $isTrail = function ($cu)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Ord) {
        return $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])(56320)($cu))($PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])($cu)(57343));
    };
    $isLead = function ($cu)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Ord) {
        return $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])(55296)($cu))($PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])($cu)(56319));
    };
    $uncons = function ($s)  use (&$PS__Data_String_CodeUnits, &$PS__Data_Maybe, &$PS__Data_Enum, &$PS__Data_String_Unsafe, &$PS__Data_HeytingAlgebra, &$isLead, &$isTrail, &$unsurrogate) {
        $v = $PS__Data_String_CodeUnits['length']($s);
        if ($v === 0) {
            return $PS__Data_Maybe['Nothing']();
        };
        if ($v === 1) {
            return $PS__Data_Maybe['Just']['constructor']([
                'head' => $PS__Data_Enum['fromEnum']($PS__Data_Enum['boundedEnumChar'])($PS__Data_String_Unsafe['charAt'](0)($s)),
                'tail' => ''
            ]);
        };
        $cu1 = $PS__Data_Enum['fromEnum']($PS__Data_Enum['boundedEnumChar'])($PS__Data_String_Unsafe['charAt'](1)($s));
        $cu0 = $PS__Data_Enum['fromEnum']($PS__Data_Enum['boundedEnumChar'])($PS__Data_String_Unsafe['charAt'](0)($s));
        $__local_var__21 = $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($isLead($cu0))($isTrail($cu1));
        if ($__local_var__21) {
            return $PS__Data_Maybe['Just']['constructor']([
                'head' => $unsurrogate($cu0)($cu1),
                'tail' => $PS__Data_String_CodeUnits['drop'](2)($s)
            ]);
        };
        return $PS__Data_Maybe['Just']['constructor']([
            'head' => $cu0,
            'tail' => $PS__Data_String_CodeUnits['drop'](1)($s)
        ]);
    };
    $unconsButWithTuple = function ($s)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Tuple, &$uncons) {
        return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($v)  use (&$PS__Data_Tuple) {
            return $PS__Data_Tuple['Tuple']['constructor']($v['head'], $v['tail']);
        })($uncons($s));
    };
    $toCodePointArrayFallback = function ($s)  use (&$PS__Data_Unfoldable, &$unconsButWithTuple) {
        return $PS__Data_Unfoldable['unfoldr']($PS__Data_Unfoldable['unfoldableArray'])($unconsButWithTuple)($s);
    };
    $unsafeCodePointAt0Fallback = function ($s)  use (&$PS__Data_Enum, &$PS__Data_String_Unsafe, &$PS__Data_HeytingAlgebra, &$isLead, &$isTrail, &$unsurrogate) {
        $cu1 = $PS__Data_Enum['fromEnum']($PS__Data_Enum['boundedEnumChar'])($PS__Data_String_Unsafe['charAt'](1)($s));
        $cu0 = $PS__Data_Enum['fromEnum']($PS__Data_Enum['boundedEnumChar'])($PS__Data_String_Unsafe['charAt'](0)($s));
        $__local_var__25 = $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($isLead($cu0))($isTrail($cu1));
        if ($__local_var__25) {
            return $unsurrogate($cu0)($cu1);
        };
        return $cu0;
    };
    $unsafeCodePointAt0 = $__foreign['_unsafeCodePointAt0']($unsafeCodePointAt0Fallback);
    $toCodePointArray = $__foreign['_toCodePointArray']($toCodePointArrayFallback)($unsafeCodePointAt0);
    $length = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Array['length'])($toCodePointArray);
    $lastIndexOf = function ($p)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$length, &$PS__Data_String_CodeUnits) {
        return function ($s)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$length, &$PS__Data_String_CodeUnits, &$p) {
            return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($i)  use (&$length, &$PS__Data_String_CodeUnits, &$s) {
                return $length($PS__Data_String_CodeUnits['take']($i)($s));
            })($PS__Data_String_CodeUnits['lastIndexOf']($p)($s));
        };
    };
    $indexOf = function ($p)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$length, &$PS__Data_String_CodeUnits) {
        return function ($s)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$length, &$PS__Data_String_CodeUnits, &$p) {
            return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($i)  use (&$length, &$PS__Data_String_CodeUnits, &$s) {
                return $length($PS__Data_String_CodeUnits['take']($i)($s));
            })($PS__Data_String_CodeUnits['indexOf']($p)($s));
        };
    };
    $fromCharCode = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_String_CodeUnits['singleton'])($PS__Data_Enum['toEnumWithDefaults']($PS__Data_Enum['boundedEnumChar'])($PS__Data_Bounded['bottom']($PS__Data_Bounded['boundedChar']))($PS__Data_Bounded['top']($PS__Data_Bounded['boundedChar'])));
    $singletonFallback = function ($v)  use (&$PS__Data_Ord, &$fromCharCode, &$PS__Data_Semiring, &$PS__Data_EuclideanRing, &$PS__Data_Ring, &$PS__Data_Semigroup) {
        if ($PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])($v)(65535)) {
            return $fromCharCode($v);
        };
        $lead = $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($PS__Data_EuclideanRing['div']($PS__Data_EuclideanRing['euclideanRingInt'])($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(65536))(1024))(55296);
        $trail = $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($PS__Data_EuclideanRing['mod']($PS__Data_EuclideanRing['euclideanRingInt'])($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($v)(65536))(1024))(56320);
        return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($fromCharCode($lead))($fromCharCode($trail));
    };
    $fromCodePointArray = $__foreign['_fromCodePointArray']($singletonFallback);
    $singleton = $__foreign['_singleton']($singletonFallback);
    $takeFallback = function ($n)  use (&$PS__Data_Ord, &$uncons, &$PS__Data_Semigroup, &$singleton, &$takeFallback, &$PS__Data_Ring) {
        return function ($v)  use (&$PS__Data_Ord, &$n, &$uncons, &$PS__Data_Semigroup, &$singleton, &$takeFallback, &$PS__Data_Ring) {
            if ($PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($n)(1)) {
                return '';
            };
            $v1 = $uncons($v);
            if ($v1['type'] === 'Just') {
                return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($singleton($v1['value0']['head']))($takeFallback($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($n)(1))($v1['value0']['tail']));
            };
            return $v;
        };
    };
    $take = $__foreign['_take']($takeFallback);
    $lastIndexOf__prime = function ($p)  use (&$PS__Data_String_CodeUnits, &$take, &$PS__Data_Functor, &$PS__Data_Maybe, &$length) {
        return function ($i)  use (&$PS__Data_String_CodeUnits, &$take, &$PS__Data_Functor, &$PS__Data_Maybe, &$length, &$p) {
            return function ($s)  use (&$PS__Data_String_CodeUnits, &$take, &$i, &$PS__Data_Functor, &$PS__Data_Maybe, &$length, &$p) {
                $i__prime = $PS__Data_String_CodeUnits['length']($take($i)($s));
                return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($k)  use (&$length, &$PS__Data_String_CodeUnits, &$s) {
                    return $length($PS__Data_String_CodeUnits['take']($k)($s));
                })($PS__Data_String_CodeUnits['lastIndexOf\'']($p)($i__prime)($s));
            };
        };
    };
    $splitAt = function ($i)  use (&$take, &$PS__Data_String_CodeUnits) {
        return function ($s)  use (&$take, &$i, &$PS__Data_String_CodeUnits) {
            $before = $take($i)($s);
            return [
                'before' => $before,
                'after' => $PS__Data_String_CodeUnits['drop']($PS__Data_String_CodeUnits['length']($before))($s)
            ];
        };
    };
    $eqCodePoint = $PS__Data_Eq['Eq'](function ($x)  use (&$PS__Data_Eq) {
        return function ($y)  use (&$PS__Data_Eq, &$x) {
            return $PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($x)($y);
        };
    });
    $ordCodePoint = $PS__Data_Ord['Ord'](function ()  use (&$eqCodePoint) {
        return $eqCodePoint;
    }, function ($x)  use (&$PS__Data_Ord) {
        return function ($y)  use (&$PS__Data_Ord, &$x) {
            return $PS__Data_Ord['compare']($PS__Data_Ord['ordInt'])($x)($y);
        };
    });
    $drop = function ($n)  use (&$PS__Data_String_CodeUnits, &$take) {
        return function ($s)  use (&$PS__Data_String_CodeUnits, &$take, &$n) {
            return $PS__Data_String_CodeUnits['drop']($PS__Data_String_CodeUnits['length']($take($n)($s)))($s);
        };
    };
    $indexOf__prime = function ($p)  use (&$drop, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Semiring, &$length, &$PS__Data_String_CodeUnits) {
        return function ($i)  use (&$drop, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Semiring, &$length, &$PS__Data_String_CodeUnits, &$p) {
            return function ($s)  use (&$drop, &$i, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Semiring, &$length, &$PS__Data_String_CodeUnits, &$p) {
                $s__prime = $drop($i)($s);
                return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($k)  use (&$PS__Data_Semiring, &$i, &$length, &$PS__Data_String_CodeUnits, &$s__prime) {
                    return $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($i)($length($PS__Data_String_CodeUnits['take']($k)($s__prime)));
                })($PS__Data_String_CodeUnits['indexOf']($p)($s__prime));
            };
        };
    };
    $countTail = function ($_dollar_copy_p)  use (&$uncons, &$PS__Data_Semiring) {
        return function ($_dollar_copy_s)  use (&$_dollar_copy_p, &$uncons, &$PS__Data_Semiring) {
            return function ($_dollar_copy_accum)  use (&$_dollar_copy_p, &$_dollar_copy_s, &$uncons, &$PS__Data_Semiring) {
                $_dollar_tco_var_p = $_dollar_copy_p;
                $_dollar_tco_var_s = $_dollar_copy_s;
                $_dollar_tco_done = false;
                $_dollar_tco_result = NULL;
                $_dollar_tco_loop = function ($p, $s, $accum)  use (&$uncons, &$_dollar_tco_var_p, &$_dollar_tco_var_s, &$_dollar_copy_accum, &$PS__Data_Semiring, &$_dollar_tco_done) {
                    $v = $uncons($s);
                    if ($v['type'] === 'Just') {
                        $__local_var__38 = $p($v['value0']['head']);
                        if ($__local_var__38) {
                            $_dollar_tco_var_p = $p;
                            $_dollar_tco_var_s = $v['value0']['tail'];
                            $_dollar_copy_accum = $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($accum)(1);
                            return;
                        };
                        $_dollar_tco_done = true;
                        return $accum;
                    };
                    $_dollar_tco_done = true;
                    return $accum;
                };
                while (!$_dollar_tco_done) {
                    $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_p, $_dollar_tco_var_s, $_dollar_copy_accum);
                };
                return $_dollar_tco_result;
            };
        };
    };
    $countFallback = function ($p)  use (&$countTail) {
        return function ($s)  use (&$countTail, &$p) {
            return $countTail($p)($s)(0);
        };
    };
    $countPrefix = $__foreign['_countPrefix']($countFallback)($unsafeCodePointAt0);
    $dropWhile = function ($p)  use (&$drop, &$countPrefix) {
        return function ($s)  use (&$drop, &$countPrefix, &$p) {
            return $drop($countPrefix($p)($s))($s);
        };
    };
    $takeWhile = function ($p)  use (&$take, &$countPrefix) {
        return function ($s)  use (&$take, &$countPrefix, &$p) {
            return $take($countPrefix($p)($s))($s);
        };
    };
    $codePointFromChar = $PS__Control_Semigroupoid['composeFlipped']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Enum['fromEnum']($PS__Data_Enum['boundedEnumChar']))($CodePoint);
    $codePointAtFallback = function ($_dollar_copy_n)  use (&$uncons, &$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_Ring) {
        return function ($_dollar_copy_s)  use (&$_dollar_copy_n, &$uncons, &$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_Ring) {
            $_dollar_tco_var_n = $_dollar_copy_n;
            $_dollar_tco_done = false;
            $_dollar_tco_result = NULL;
            $_dollar_tco_loop = function ($n, $s)  use (&$uncons, &$PS__Data_Eq, &$_dollar_tco_done, &$PS__Data_Maybe, &$_dollar_tco_var_n, &$PS__Data_Ring, &$_dollar_copy_s) {
                $v = $uncons($s);
                if ($v['type'] === 'Just') {
                    $__local_var__43 = $PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($n)(0);
                    if ($__local_var__43) {
                        $_dollar_tco_done = true;
                        return $PS__Data_Maybe['Just']['constructor']($v['value0']['head']);
                    };
                    $_dollar_tco_var_n = $PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($n)(1);
                    $_dollar_copy_s = $v['value0']['tail'];
                    return;
                };
                $_dollar_tco_done = true;
                return $PS__Data_Maybe['Nothing']();
            };
            while (!$_dollar_tco_done) {
                $_dollar_tco_result = $_dollar_tco_loop($_dollar_tco_var_n, $_dollar_copy_s);
            };
            return $_dollar_tco_result;
        };
    };
    $codePointAt = function ($v)  use (&$PS__Data_Ord, &$PS__Data_Maybe, &$unsafeCodePointAt0, &$__foreign, &$codePointAtFallback) {
        return function ($v1)  use (&$PS__Data_Ord, &$v, &$PS__Data_Maybe, &$unsafeCodePointAt0, &$__foreign, &$codePointAtFallback) {
            if ($PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($v)(0)) {
                return $PS__Data_Maybe['Nothing']();
            };
            if ($v === 0 && $v1 === '') {
                return $PS__Data_Maybe['Nothing']();
            };
            if ($v === 0) {
                return $PS__Data_Maybe['Just']['constructor']($unsafeCodePointAt0($v1));
            };
            return $__foreign['_codePointAt']($codePointAtFallback)($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']())($unsafeCodePointAt0)($v)($v1);
        };
    };
    $boundedCodePoint = $PS__Data_Bounded['Bounded'](function ()  use (&$ordCodePoint) {
        return $ordCodePoint;
    }, 0, 1114111);
    $boundedEnumCodePoint = $PS__Data_Enum['BoundedEnum'](function ()  use (&$boundedCodePoint) {
        return $boundedCodePoint;
    }, function ()  use (&$enumCodePoint) {
        return $enumCodePoint;
    }, $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])(1114111)(1), function ($v) {
        return $v;
    }, function ($n)  use (&$PS__Data_HeytingAlgebra, &$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Boolean) {
        if ($PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Ord['greaterThanOrEq']($PS__Data_Ord['ordInt'])($n)(0))($PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])($n)(1114111))) {
            return $PS__Data_Maybe['Just']['constructor']($n);
        };
        if ($PS__Data_Boolean['otherwise']) {
            return $PS__Data_Maybe['Nothing']();
        };
        throw new \Exception('Failed pattern match at Data.String.CodePoints line 63, column 1 - line 63, column 55: ' . var_dump([ $n['constructor']['name'] ]));
    });
    $enumCodePoint = $PS__Data_Enum['Enum'](function ()  use (&$ordCodePoint) {
        return $ordCodePoint;
    }, $PS__Data_Enum['defaultPred']($PS__Data_Enum['toEnum']($boundedEnumCodePoint))($PS__Data_Enum['fromEnum']($boundedEnumCodePoint)), $PS__Data_Enum['defaultSucc']($PS__Data_Enum['toEnum']($boundedEnumCodePoint))($PS__Data_Enum['fromEnum']($boundedEnumCodePoint)));
    return [
        'codePointFromChar' => $codePointFromChar,
        'singleton' => $singleton,
        'fromCodePointArray' => $fromCodePointArray,
        'toCodePointArray' => $toCodePointArray,
        'codePointAt' => $codePointAt,
        'uncons' => $uncons,
        'length' => $length,
        'countPrefix' => $countPrefix,
        'indexOf' => $indexOf,
        'indexOf\'' => $indexOf__prime,
        'lastIndexOf' => $lastIndexOf,
        'lastIndexOf\'' => $lastIndexOf__prime,
        'take' => $take,
        'takeWhile' => $takeWhile,
        'drop' => $drop,
        'dropWhile' => $dropWhile,
        'splitAt' => $splitAt,
        'eqCodePoint' => $eqCodePoint,
        'ordCodePoint' => $ordCodePoint,
        'showCodePoint' => $showCodePoint,
        'boundedCodePoint' => $boundedCodePoint,
        'enumCodePoint' => $enumCodePoint,
        'boundedEnumCodePoint' => $boundedEnumCodePoint
    ];
})();
