<?php
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_String_Pattern = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Newtype, &$PS__Data_Eq, &$PS__Data_Ord) {
    $Replacement = function ($x) {
        return $x;
    };
    $Pattern = function ($x) {
        return $x;
    };
    $showReplacement = $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show) {
        return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Replacement ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Show['showString'])($v))(')'));
    });
    $showPattern = $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show) {
        return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Pattern ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($PS__Data_Show['showString'])($v))(')'));
    });
    $newtypeReplacement = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Replacement);
    $newtypePattern = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Pattern);
    $eqReplacement = $PS__Data_Eq['Eq'](function ($x)  use (&$PS__Data_Eq) {
        return function ($y)  use (&$PS__Data_Eq, &$x) {
            return $PS__Data_Eq['eq']($PS__Data_Eq['eqString'])($x)($y);
        };
    });
    $ordReplacement = $PS__Data_Ord['Ord'](function ()  use (&$eqReplacement) {
        return $eqReplacement;
    }, function ($x)  use (&$PS__Data_Ord) {
        return function ($y)  use (&$PS__Data_Ord, &$x) {
            return $PS__Data_Ord['compare']($PS__Data_Ord['ordString'])($x)($y);
        };
    });
    $eqPattern = $PS__Data_Eq['Eq'](function ($x)  use (&$PS__Data_Eq) {
        return function ($y)  use (&$PS__Data_Eq, &$x) {
            return $PS__Data_Eq['eq']($PS__Data_Eq['eqString'])($x)($y);
        };
    });
    $ordPattern = $PS__Data_Ord['Ord'](function ()  use (&$eqPattern) {
        return $eqPattern;
    }, function ($x)  use (&$PS__Data_Ord) {
        return function ($y)  use (&$PS__Data_Ord, &$x) {
            return $PS__Data_Ord['compare']($PS__Data_Ord['ordString'])($x)($y);
        };
    });
    return [
        'Pattern' => $Pattern,
        'Replacement' => $Replacement,
        'eqPattern' => $eqPattern,
        'ordPattern' => $ordPattern,
        'newtypePattern' => $newtypePattern,
        'showPattern' => $showPattern,
        'eqReplacement' => $eqReplacement,
        'ordReplacement' => $ordReplacement,
        'newtypeReplacement' => $newtypeReplacement,
        'showReplacement' => $showReplacement
    ];
})();
