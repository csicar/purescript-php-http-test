<?php
$PS__Partial = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $crash = function ($dictPartial)  use (&$__foreign) {
        return $__foreign['crashWith']($dictPartial)('Partial.crash: partial function');
    };
    return [
        'crash' => $crash,
        'crashWith' => $__foreign['crashWith']
    ];
})();
