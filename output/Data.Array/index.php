<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Lazy/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.ST/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.ST.Internal/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Array.NonEmpty.Internal/index.php');
require_once(dirname(__FILE__) . '/../Data.Array.ST/index.php');
require_once(dirname(__FILE__) . '/../Data.Array.ST.Iterator/index.php');
require_once(dirname(__FILE__) . '/../Data.Boolean/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable/index.php');
require_once(dirname(__FILE__) . '/../Partial.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
require_once(dirname(__FILE__) . '/../Unsafe.Coerce/index.php');
$PS__Data_Array = (function ()  use (&$PS__Data_Traversable, &$PS__Data_Tuple, &$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST, &$PS__Data_Foldable, &$PS__Data_Maybe, &$PS__Data_Function, &$PS__Data_Ord, &$PS__Partial_Unsafe, &$PS__Data_Semiring, &$PS__Data_Boolean, &$PS__Data_Unfoldable, &$PS__Data_Ring, &$PS__Data_Eq, &$PS__Data_HeytingAlgebra, &$PS__Control_Apply, &$PS__Data_Functor, &$PS__Control_Lazy, &$PS__Control_Alt, &$PS__Control_Applicative, &$PS__Control_Bind, &$PS__Data_Array_ST_Iterator, &$PS__Control_Semigroupoid, &$PS__Data_Ordering, &$PS__Unsafe_Coerce, &$PS__Control_Monad_Rec_Class, &$PS__Data_Semigroup, &$PS__Control_Category) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $zipWithA = function ($dictApplicative)  use (&$PS__Data_Traversable, &$__foreign) {
        return function ($f)  use (&$PS__Data_Traversable, &$dictApplicative, &$__foreign) {
            return function ($xs)  use (&$PS__Data_Traversable, &$dictApplicative, &$__foreign, &$f) {
                return function ($ys)  use (&$PS__Data_Traversable, &$dictApplicative, &$__foreign, &$f, &$xs) {
                    return $PS__Data_Traversable['sequence']($PS__Data_Traversable['traversableArray'])($dictApplicative)($__foreign['zipWith']($f)($xs)($ys));
                };
            };
        };
    };
    $zip = $__foreign['zipWith']($PS__Data_Tuple['Tuple']['create']);
    $updateAtIndices = function ($dictFoldable)  use (&$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST, &$PS__Data_Foldable) {
        return function ($us)  use (&$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST, &$PS__Data_Foldable, &$dictFoldable) {
            return function ($xs)  use (&$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST, &$PS__Data_Foldable, &$dictFoldable, &$us) {
                return $PS__Control_Monad_ST_Internal['run']($PS__Data_Array_ST['withArray'](function ($res)  use (&$PS__Data_Foldable, &$PS__Control_Monad_ST_Internal, &$dictFoldable, &$PS__Data_Array_ST, &$us) {
                    return $PS__Data_Foldable['traverse_']($PS__Control_Monad_ST_Internal['applicativeST'])($dictFoldable)(function ($v)  use (&$PS__Data_Array_ST, &$res) {
                        return $PS__Data_Array_ST['poke']($v['value0'])($v['value1'])($res);
                    })($us);
                })($xs));
            };
        };
    };
    $updateAt = $__foreign['_updateAt']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $unsafeIndex = function ($dictPartial)  use (&$__foreign) {
        return $__foreign['unsafeIndexImpl'];
    };
    $uncons = $__foreign['uncons\'']($PS__Data_Function['const']($PS__Data_Maybe['Nothing']()))(function ($x)  use (&$PS__Data_Maybe) {
        return function ($xs)  use (&$PS__Data_Maybe, &$x) {
            return $PS__Data_Maybe['Just']['constructor']([
                'head' => $x,
                'tail' => $xs
            ]);
        };
    });
    $toUnfoldable = function ($dictUnfoldable)  use (&$__foreign, &$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Partial_Unsafe, &$unsafeIndex, &$PS__Data_Semiring, &$PS__Data_Boolean, &$PS__Data_Unfoldable) {
        return function ($xs)  use (&$__foreign, &$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Partial_Unsafe, &$unsafeIndex, &$PS__Data_Semiring, &$PS__Data_Boolean, &$PS__Data_Unfoldable, &$dictUnfoldable) {
            $len = $__foreign['length']($xs);
            $f = function ($i)  use (&$PS__Data_Ord, &$len, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Partial_Unsafe, &$unsafeIndex, &$xs, &$PS__Data_Semiring, &$PS__Data_Boolean) {
                if ($PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($i)($len)) {
                    return $PS__Data_Maybe['Just']['constructor']($PS__Data_Tuple['Tuple']['constructor']($PS__Partial_Unsafe['unsafePartial'](function ($dictPartial)  use (&$unsafeIndex, &$xs, &$i) {
                        return $unsafeIndex($dictPartial)($xs)($i);
                    }), $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($i)(1)));
                };
                if ($PS__Data_Boolean['otherwise']) {
                    return $PS__Data_Maybe['Nothing']();
                };
                throw new \Exception('Failed pattern match at Data.Array line 143, column 3 - line 145, column 26: ' . var_dump([ $i['constructor']['name'] ]));
            };
            return $PS__Data_Unfoldable['unfoldr']($dictUnfoldable)($f)(0);
        };
    };
    $takeEnd = function ($n)  use (&$__foreign, &$PS__Data_Ring) {
        return function ($xs)  use (&$__foreign, &$PS__Data_Ring, &$n) {
            return $__foreign['drop']($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($__foreign['length']($xs))($n))($xs);
        };
    };
    $tail = $__foreign['uncons\'']($PS__Data_Function['const']($PS__Data_Maybe['Nothing']()))(function ($v)  use (&$PS__Data_Maybe) {
        return function ($xs)  use (&$PS__Data_Maybe) {
            return $PS__Data_Maybe['Just']['constructor']($xs);
        };
    });
    $sortBy = function ($comp)  use (&$PS__Data_Ring, &$__foreign) {
        return function ($xs)  use (&$comp, &$PS__Data_Ring, &$__foreign) {
            $comp__prime = function ($x)  use (&$comp, &$PS__Data_Ring) {
                return function ($y)  use (&$comp, &$x, &$PS__Data_Ring) {
                    $v = $comp($x)($y);
                    if ($v['type'] === 'GT') {
                        return 1;
                    };
                    if ($v['type'] === 'EQ') {
                        return 0;
                    };
                    if ($v['type'] === 'LT') {
                        return $PS__Data_Ring['negate']($PS__Data_Ring['ringInt'])(1);
                    };
                    throw new \Exception('Failed pattern match at Data.Array line 702, column 15 - line 707, column 1: ' . var_dump([ $v['constructor']['name'] ]));
                };
            };
            return $__foreign['sortImpl']($comp__prime)($xs);
        };
    };
    $sortWith = function ($dictOrd)  use (&$sortBy, &$PS__Data_Ord) {
        return function ($f)  use (&$sortBy, &$PS__Data_Ord, &$dictOrd) {
            return $sortBy($PS__Data_Ord['comparing']($dictOrd)($f));
        };
    };
    $sort = function ($dictOrd)  use (&$sortBy, &$PS__Data_Ord) {
        return function ($xs)  use (&$sortBy, &$PS__Data_Ord, &$dictOrd) {
            return $sortBy($PS__Data_Ord['compare']($dictOrd))($xs);
        };
    };
    $singleton = function ($a) {
        return [ $a ];
    };
    $__null = function ($xs)  use (&$PS__Data_Eq, &$__foreign) {
        return $PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($__foreign['length']($xs))(0);
    };
    $nubByEq = function ($eq)  use (&$uncons, &$__foreign, &$nubByEq, &$PS__Data_HeytingAlgebra) {
        return function ($xs)  use (&$uncons, &$__foreign, &$nubByEq, &$eq, &$PS__Data_HeytingAlgebra) {
            $v = $uncons($xs);
            if ($v['type'] === 'Just') {
                return $__foreign['cons']($v['value0']['head'])($nubByEq($eq)($__foreign['filter'](function ($y)  use (&$PS__Data_HeytingAlgebra, &$eq, &$v) {
                    return $PS__Data_HeytingAlgebra['not']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($eq($v['value0']['head'])($y));
                })($v['value0']['tail'])));
            };
            if ($v['type'] === 'Nothing') {
                return [  ];
            };
            throw new \Exception('Failed pattern match at Data.Array line 929, column 3 - line 931, column 18: ' . var_dump([ $v['constructor']['name'] ]));
        };
    };
    $nubEq = function ($dictEq)  use (&$nubByEq, &$PS__Data_Eq) {
        return $nubByEq($PS__Data_Eq['eq']($dictEq));
    };
    $modifyAtIndices = function ($dictFoldable)  use (&$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST, &$PS__Data_Foldable) {
        return function ($is)  use (&$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST, &$PS__Data_Foldable, &$dictFoldable) {
            return function ($f)  use (&$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST, &$PS__Data_Foldable, &$dictFoldable, &$is) {
                return function ($xs)  use (&$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST, &$PS__Data_Foldable, &$dictFoldable, &$f, &$is) {
                    return $PS__Control_Monad_ST_Internal['run']($PS__Data_Array_ST['withArray'](function ($res)  use (&$PS__Data_Foldable, &$PS__Control_Monad_ST_Internal, &$dictFoldable, &$PS__Data_Array_ST, &$f, &$is) {
                        return $PS__Data_Foldable['traverse_']($PS__Control_Monad_ST_Internal['applicativeST'])($dictFoldable)(function ($i)  use (&$PS__Data_Array_ST, &$f, &$res) {
                            return $PS__Data_Array_ST['modify']($i)($f)($res);
                        })($is);
                    })($xs));
                };
            };
        };
    };
    $mapWithIndex = function ($f)  use (&$__foreign, &$PS__Data_Ring) {
        return function ($xs)  use (&$__foreign, &$f, &$PS__Data_Ring) {
            return $__foreign['zipWith']($f)($__foreign['range'](0)($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($__foreign['length']($xs))(1)))($xs);
        };
    };
    $some = function ($dictAlternative)  use (&$PS__Control_Apply, &$PS__Data_Functor, &$__foreign, &$PS__Control_Lazy, &$many) {
        return function ($dictLazy)  use (&$PS__Control_Apply, &$dictAlternative, &$PS__Data_Functor, &$__foreign, &$PS__Control_Lazy, &$many) {
            return function ($v)  use (&$PS__Control_Apply, &$dictAlternative, &$PS__Data_Functor, &$__foreign, &$PS__Control_Lazy, &$dictLazy, &$many) {
                return $PS__Control_Apply['apply'](($dictAlternative['Applicative0']())['Apply0']())($PS__Data_Functor['map']((($dictAlternative['Plus1']())['Alt0']())['Functor0']())($__foreign['cons'])($v))($PS__Control_Lazy['defer']($dictLazy)(function ($v1)  use (&$many, &$dictAlternative, &$dictLazy, &$v) {
                    return $many($dictAlternative)($dictLazy)($v);
                }));
            };
        };
    };
    $many = function ($dictAlternative)  use (&$PS__Control_Alt, &$some, &$PS__Control_Applicative) {
        return function ($dictLazy)  use (&$PS__Control_Alt, &$dictAlternative, &$some, &$PS__Control_Applicative) {
            return function ($v)  use (&$PS__Control_Alt, &$dictAlternative, &$some, &$dictLazy, &$PS__Control_Applicative) {
                return $PS__Control_Alt['alt'](($dictAlternative['Plus1']())['Alt0']())($some($dictAlternative)($dictLazy)($v))($PS__Control_Applicative['pure']($dictAlternative['Applicative0']())([  ]));
            };
        };
    };
    $insertAt = $__foreign['_insertAt']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $init = function ($xs)  use (&$__null, &$PS__Data_Maybe, &$PS__Data_Boolean, &$__foreign, &$PS__Data_Semiring, &$PS__Data_Ring) {
        if ($__null($xs)) {
            return $PS__Data_Maybe['Nothing']();
        };
        if ($PS__Data_Boolean['otherwise']) {
            return $PS__Data_Maybe['Just']['constructor']($__foreign['slice']($PS__Data_Semiring['zero']($PS__Data_Semiring['semiringInt']))($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($__foreign['length']($xs))($PS__Data_Semiring['one']($PS__Data_Semiring['semiringInt'])))($xs));
        };
        throw new \Exception('Failed pattern match at Data.Array line 323, column 1 - line 323, column 45: ' . var_dump([ $xs['constructor']['name'] ]));
    };
    $index = $__foreign['indexImpl']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $last = function ($xs)  use (&$index, &$PS__Data_Ring, &$__foreign) {
        return $index($xs)($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($__foreign['length']($xs))(1));
    };
    $unsnoc = function ($xs)  use (&$PS__Control_Apply, &$PS__Data_Maybe, &$PS__Data_Functor, &$init, &$last) {
        return $PS__Control_Apply['apply']($PS__Data_Maybe['applyMaybe'])($PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])(function ($v) {
            return function ($v1)  use (&$v) {
                return [
                    'init' => $v,
                    'last' => $v1
                ];
            };
        })($init($xs)))($last($xs));
    };
    $modifyAt = function ($i)  use (&$updateAt, &$PS__Data_Maybe, &$index) {
        return function ($f)  use (&$updateAt, &$i, &$PS__Data_Maybe, &$index) {
            return function ($xs)  use (&$updateAt, &$i, &$f, &$PS__Data_Maybe, &$index) {
                $go = function ($x)  use (&$updateAt, &$i, &$f, &$xs) {
                    return $updateAt($i)($f($x))($xs);
                };
                return $PS__Data_Maybe['maybe']($PS__Data_Maybe['Nothing']())($go)($index($xs)($i));
            };
        };
    };
    $span = function ($p)  use (&$index, &$PS__Data_Semiring, &$PS__Data_Maybe, &$__foreign) {
        return function ($arr)  use (&$index, &$p, &$PS__Data_Semiring, &$PS__Data_Maybe, &$__foreign) {
            $go = function ($_dollar_copy_i)  use (&$index, &$arr, &$p, &$PS__Data_Semiring, &$PS__Data_Maybe) {
                $_dollar_tco_done = false;
                $_dollar_tco_result = NULL;
                $_dollar_tco_loop = function ($i)  use (&$index, &$arr, &$p, &$_dollar_copy_i, &$PS__Data_Semiring, &$_dollar_tco_done, &$PS__Data_Maybe) {
                    $v = $index($arr)($i);
                    if ($v['type'] === 'Just') {
                        $__local_var__75 = $p($v['value0']);
                        if ($__local_var__75) {
                            $_dollar_copy_i = $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($i)(1);
                            return;
                        };
                        $_dollar_tco_done = true;
                        return $PS__Data_Maybe['Just']['constructor']($i);
                    };
                    if ($v['type'] === 'Nothing') {
                        $_dollar_tco_done = true;
                        return $PS__Data_Maybe['Nothing']();
                    };
                    throw new \Exception('Failed pattern match at Data.Array line 834, column 5 - line 836, column 25: ' . var_dump([ $v['constructor']['name'] ]));
                };
                while (!$_dollar_tco_done) {
                    $_dollar_tco_result = $_dollar_tco_loop($_dollar_copy_i);
                };
                return $_dollar_tco_result;
            };
            $breakIndex = $go(0);
            if ($breakIndex['type'] === 'Just' && $breakIndex['value0'] === 0) {
                return [
                    'init' => [  ],
                    'rest' => $arr
                ];
            };
            if ($breakIndex['type'] === 'Just') {
                return [
                    'init' => $__foreign['slice'](0)($breakIndex['value0'])($arr),
                    'rest' => $__foreign['slice']($breakIndex['value0'])($__foreign['length']($arr))($arr)
                ];
            };
            if ($breakIndex['type'] === 'Nothing') {
                return [
                    'init' => $arr,
                    'rest' => [  ]
                ];
            };
            throw new \Exception('Failed pattern match at Data.Array line 821, column 3 - line 827, column 30: ' . var_dump([ $breakIndex['constructor']['name'] ]));
        };
    };
    $takeWhile = function ($p)  use (&$span) {
        return function ($xs)  use (&$span, &$p) {
            return ($span($p)($xs))['init'];
        };
    };
    $unzip = function ($xs)  use (&$PS__Control_Monad_ST_Internal, &$PS__Control_Bind, &$PS__Data_Array_ST, &$PS__Data_Array_ST_Iterator, &$index, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Control_Applicative, &$PS__Data_Tuple) {
        return $PS__Control_Monad_ST_Internal['run']($PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Array_ST['empty'])(function ($v)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST, &$PS__Data_Array_ST_Iterator, &$index, &$xs, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Control_Applicative, &$PS__Data_Tuple) {
            return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Array_ST['empty'])(function ($v1)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST_Iterator, &$index, &$xs, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Array_ST, &$v, &$PS__Control_Applicative, &$PS__Data_Tuple) {
                return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Array_ST_Iterator['iterator'](function ($v2)  use (&$index, &$xs) {
                    return $index($xs)($v2);
                }))(function ($v2)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST_Iterator, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Array_ST, &$v, &$v1, &$PS__Control_Applicative, &$PS__Data_Tuple) {
                    return $PS__Control_Bind['discard']($PS__Control_Bind['discardUnit'])($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Array_ST_Iterator['iterate']($v2)(function ($v3)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Array_ST, &$v, &$v1) {
                        return $PS__Control_Bind['discard']($PS__Control_Bind['discardUnit'])($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Function['apply']($PS__Data_Functor['void']($PS__Control_Monad_ST_Internal['functorST']))($PS__Data_Array_ST['push']($v3['value0'])($v)))(function ()  use (&$PS__Data_Function, &$PS__Data_Functor, &$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST, &$v3, &$v1) {
                            return $PS__Data_Function['apply']($PS__Data_Functor['void']($PS__Control_Monad_ST_Internal['functorST']))($PS__Data_Array_ST['push']($v3['value1'])($v1));
                        });
                    }))(function ()  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST, &$v, &$v1, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
                        return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Array_ST['unsafeFreeze']($v))(function ($v3)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST, &$v1, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Tuple) {
                            return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Array_ST['unsafeFreeze']($v1))(function ($v4)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$PS__Control_Monad_ST_Internal, &$PS__Data_Tuple, &$v3) {
                                return $PS__Data_Function['apply']($PS__Control_Applicative['pure']($PS__Control_Monad_ST_Internal['applicativeST']))($PS__Data_Tuple['Tuple']['constructor']($v3, $v4));
                            });
                        });
                    });
                });
            });
        }));
    };
    $head = function ($xs)  use (&$index) {
        return $index($xs)(0);
    };
    $nubBy = function ($comp)  use (&$sortBy, &$PS__Data_Tuple, &$mapWithIndex, &$head, &$PS__Data_Function, &$PS__Data_Functor, &$sortWith, &$PS__Data_Ord, &$PS__Control_Monad_ST_Internal, &$PS__Control_Bind, &$PS__Data_Array_ST, &$singleton, &$PS__Control_Semigroupoid, &$PS__Partial_Unsafe, &$PS__Data_Maybe, &$last, &$PS__Control_Applicative, &$PS__Data_Eq, &$PS__Data_Ordering) {
        return function ($xs)  use (&$sortBy, &$comp, &$PS__Data_Tuple, &$mapWithIndex, &$head, &$PS__Data_Function, &$PS__Data_Functor, &$sortWith, &$PS__Data_Ord, &$PS__Control_Monad_ST_Internal, &$PS__Control_Bind, &$PS__Data_Array_ST, &$singleton, &$PS__Control_Semigroupoid, &$PS__Partial_Unsafe, &$PS__Data_Maybe, &$last, &$PS__Control_Applicative, &$PS__Data_Eq, &$PS__Data_Ordering) {
            $indexedAndSorted = $sortBy(function ($x)  use (&$comp, &$PS__Data_Tuple) {
                return function ($y)  use (&$comp, &$PS__Data_Tuple, &$x) {
                    return $comp($PS__Data_Tuple['snd']($x))($PS__Data_Tuple['snd']($y));
                };
            })($mapWithIndex($PS__Data_Tuple['Tuple']['create'])($xs));
            $v = $head($indexedAndSorted);
            if ($v['type'] === 'Nothing') {
                return [  ];
            };
            if ($v['type'] === 'Just') {
                return $PS__Data_Function['apply']($PS__Data_Functor['map']($PS__Data_Functor['functorArray'])($PS__Data_Tuple['snd']))($PS__Data_Function['apply']($sortWith($PS__Data_Ord['ordInt'])($PS__Data_Tuple['fst']))($PS__Control_Monad_ST_Internal['run']($PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Function['apply']($PS__Data_Array_ST['unsafeThaw'])($singleton($v['value0'])))(function ($v1)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$indexedAndSorted, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$PS__Data_Tuple, &$PS__Partial_Unsafe, &$PS__Data_Maybe, &$last, &$PS__Data_Array_ST, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Eq, &$PS__Data_Ordering, &$comp) {
                    return $PS__Control_Bind['discard']($PS__Control_Bind['discardUnit'])($PS__Control_Monad_ST_Internal['bindST'])($PS__Control_Monad_ST_Internal['foreach']($indexedAndSorted)(function ($v2)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Functor, &$PS__Control_Semigroupoid, &$PS__Data_Tuple, &$PS__Partial_Unsafe, &$PS__Data_Maybe, &$last, &$PS__Data_Array_ST, &$v1, &$PS__Data_Function, &$PS__Control_Applicative, &$PS__Data_Eq, &$PS__Data_Ordering, &$comp) {
                        return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Functor['map']($PS__Control_Monad_ST_Internal['functorST'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Tuple['snd'])($PS__Partial_Unsafe['unsafePartial'](function ($dictPartial)  use (&$PS__Control_Semigroupoid, &$PS__Data_Maybe, &$last) {
                            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['fromJust']($dictPartial))($last);
                        })))($PS__Data_Array_ST['unsafeFreeze']($v1)))(function ($v3)  use (&$PS__Data_Function, &$PS__Control_Applicative, &$PS__Control_Monad_ST_Internal, &$PS__Data_Eq, &$PS__Data_Ordering, &$comp, &$v2, &$PS__Data_Functor, &$PS__Data_Array_ST, &$v1) {
                            return $PS__Data_Function['apply']($PS__Control_Applicative['when']($PS__Control_Monad_ST_Internal['applicativeST'])($PS__Data_Eq['notEq']($PS__Data_Ordering['eqOrdering'])($comp($v3)($v2['value1']))($PS__Data_Ordering['EQ']())))($PS__Data_Function['apply']($PS__Data_Functor['void']($PS__Control_Monad_ST_Internal['functorST']))($PS__Data_Array_ST['push']($v2)($v1)));
                        });
                    }))(function ()  use (&$PS__Data_Array_ST, &$v1) {
                        return $PS__Data_Array_ST['unsafeFreeze']($v1);
                    });
                }))));
            };
            throw new \Exception('Failed pattern match at Data.Array line 903, column 17 - line 911, column 29: ' . var_dump([ $v['constructor']['name'] ]));
        };
    };
    $nub = function ($dictOrd)  use (&$nubBy, &$PS__Data_Ord) {
        return $nubBy($PS__Data_Ord['compare']($dictOrd));
    };
    $groupBy = function ($op)  use (&$PS__Control_Monad_ST_Internal, &$PS__Control_Bind, &$PS__Data_Array_ST, &$PS__Data_Array_ST_Iterator, &$index, &$PS__Data_Functor, &$PS__Unsafe_Coerce) {
        return function ($xs)  use (&$PS__Control_Monad_ST_Internal, &$PS__Control_Bind, &$PS__Data_Array_ST, &$PS__Data_Array_ST_Iterator, &$index, &$PS__Data_Functor, &$op, &$PS__Unsafe_Coerce) {
            return $PS__Control_Monad_ST_Internal['run']($PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Array_ST['empty'])(function ($v)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST_Iterator, &$index, &$xs, &$PS__Data_Functor, &$PS__Data_Array_ST, &$op, &$PS__Unsafe_Coerce) {
                return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Array_ST_Iterator['iterator'](function ($v1)  use (&$index, &$xs) {
                    return $index($xs)($v1);
                }))(function ($v1)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST_Iterator, &$PS__Data_Functor, &$PS__Data_Array_ST, &$op, &$PS__Unsafe_Coerce, &$v) {
                    return $PS__Control_Bind['discard']($PS__Control_Bind['discardUnit'])($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Array_ST_Iterator['iterate']($v1)(function ($x)  use (&$PS__Data_Functor, &$PS__Control_Monad_ST_Internal, &$PS__Control_Bind, &$PS__Data_Array_ST, &$PS__Data_Array_ST_Iterator, &$op, &$v1, &$PS__Unsafe_Coerce, &$v) {
                        return $PS__Data_Functor['void']($PS__Control_Monad_ST_Internal['functorST'])($PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Array_ST['empty'])(function ($v2)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST_Iterator, &$op, &$x, &$v1, &$PS__Data_Array_ST, &$PS__Unsafe_Coerce, &$v) {
                            return $PS__Control_Bind['discard']($PS__Control_Bind['discardUnit'])($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Array_ST_Iterator['pushWhile']($op($x))($v1)($v2))(function ()  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST, &$x, &$v2, &$PS__Unsafe_Coerce, &$v) {
                                return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Array_ST['push']($x)($v2))(function ($v3)  use (&$PS__Control_Bind, &$PS__Control_Monad_ST_Internal, &$PS__Data_Array_ST, &$v2, &$PS__Unsafe_Coerce, &$v) {
                                    return $PS__Control_Bind['bind']($PS__Control_Monad_ST_Internal['bindST'])($PS__Data_Array_ST['unsafeFreeze']($v2))(function ($v4)  use (&$PS__Data_Array_ST, &$PS__Unsafe_Coerce, &$v) {
                                        return $PS__Data_Array_ST['push']($PS__Unsafe_Coerce['unsafeCoerce']($v4))($v);
                                    });
                                });
                            });
                        }));
                    }))(function ()  use (&$PS__Data_Array_ST, &$v) {
                        return $PS__Data_Array_ST['unsafeFreeze']($v);
                    });
                });
            }));
        };
    };
    $group = function ($dictEq)  use (&$groupBy, &$PS__Data_Eq) {
        return function ($xs)  use (&$groupBy, &$PS__Data_Eq, &$dictEq) {
            return $groupBy($PS__Data_Eq['eq']($dictEq))($xs);
        };
    };
    $group__prime = function ($dictOrd)  use (&$PS__Control_Semigroupoid, &$group, &$sort) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($group($dictOrd['Eq0']()))($sort($dictOrd));
    };
    $fromFoldable = function ($dictFoldable)  use (&$__foreign, &$PS__Data_Foldable) {
        return $__foreign['fromFoldableImpl']($PS__Data_Foldable['foldr']($dictFoldable));
    };
    $foldRecM = function ($dictMonadRec)  use (&$PS__Data_Ord, &$__foreign, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class, &$PS__Data_Boolean, &$PS__Control_Bind, &$PS__Partial_Unsafe, &$unsafeIndex, &$PS__Data_Semiring) {
        return function ($f)  use (&$PS__Data_Ord, &$__foreign, &$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$PS__Data_Boolean, &$PS__Control_Bind, &$PS__Partial_Unsafe, &$unsafeIndex, &$PS__Data_Semiring) {
            return function ($a)  use (&$PS__Data_Ord, &$__foreign, &$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$PS__Data_Boolean, &$PS__Control_Bind, &$f, &$PS__Partial_Unsafe, &$unsafeIndex, &$PS__Data_Semiring) {
                return function ($array)  use (&$PS__Data_Ord, &$__foreign, &$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$PS__Data_Boolean, &$PS__Control_Bind, &$f, &$PS__Partial_Unsafe, &$unsafeIndex, &$PS__Data_Semiring, &$a) {
                    $go = function ($res)  use (&$PS__Data_Ord, &$__foreign, &$array, &$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$PS__Data_Boolean, &$PS__Control_Bind, &$f, &$PS__Partial_Unsafe, &$unsafeIndex, &$PS__Data_Semiring) {
                        return function ($i)  use (&$PS__Data_Ord, &$__foreign, &$array, &$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$res, &$PS__Data_Boolean, &$PS__Control_Bind, &$f, &$PS__Partial_Unsafe, &$unsafeIndex, &$PS__Data_Semiring) {
                            if ($PS__Data_Ord['greaterThanOrEq']($PS__Data_Ord['ordInt'])($i)($__foreign['length']($array))) {
                                return $PS__Control_Applicative['pure'](($dictMonadRec['Monad0']())['Applicative0']())($PS__Control_Monad_Rec_Class['Done']['constructor']($res));
                            };
                            if ($PS__Data_Boolean['otherwise']) {
                                return $PS__Control_Bind['bind'](($dictMonadRec['Monad0']())['Bind1']())($f($res)($PS__Partial_Unsafe['unsafePartial'](function ($dictPartial)  use (&$unsafeIndex, &$array, &$i) {
                                    return $unsafeIndex($dictPartial)($array)($i);
                                })))(function ($v)  use (&$PS__Control_Applicative, &$dictMonadRec, &$PS__Control_Monad_Rec_Class, &$PS__Data_Semiring, &$i) {
                                    return $PS__Control_Applicative['pure'](($dictMonadRec['Monad0']())['Applicative0']())($PS__Control_Monad_Rec_Class['Loop']['constructor']([
                                        'a' => $v,
                                        'b' => $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($i)(1)
                                    ]));
                                });
                            };
                            throw new \Exception('Failed pattern match at Data.Array line 1098, column 3 - line 1102, column 42: ' . var_dump([ $res['constructor']['name'], $i['constructor']['name'] ]));
                        };
                    };
                    return $PS__Control_Monad_Rec_Class['tailRecM2']($dictMonadRec)($go)($a)(0);
                };
            };
        };
    };
    $foldM = function ($dictMonad)  use (&$__foreign, &$PS__Control_Applicative, &$PS__Control_Bind, &$foldM) {
        return function ($f)  use (&$__foreign, &$PS__Control_Applicative, &$dictMonad, &$PS__Control_Bind, &$foldM) {
            return function ($a)  use (&$__foreign, &$PS__Control_Applicative, &$dictMonad, &$PS__Control_Bind, &$f, &$foldM) {
                return $__foreign['uncons\''](function ($v)  use (&$PS__Control_Applicative, &$dictMonad, &$a) {
                    return $PS__Control_Applicative['pure']($dictMonad['Applicative0']())($a);
                })(function ($b)  use (&$PS__Control_Bind, &$dictMonad, &$f, &$a, &$foldM) {
                    return function ($bs)  use (&$PS__Control_Bind, &$dictMonad, &$f, &$a, &$b, &$foldM) {
                        return $PS__Control_Bind['bind']($dictMonad['Bind1']())($f($a)($b))(function ($a__prime)  use (&$foldM, &$dictMonad, &$f, &$bs) {
                            return $foldM($dictMonad)($f)($a__prime)($bs);
                        });
                    };
                });
            };
        };
    };
    $findLastIndex = $__foreign['findLastIndexImpl']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $insertBy = function ($cmp)  use (&$PS__Data_Maybe, &$PS__Data_Semiring, &$findLastIndex, &$PS__Data_Eq, &$PS__Data_Ordering, &$PS__Partial_Unsafe, &$insertAt) {
        return function ($x)  use (&$PS__Data_Maybe, &$PS__Data_Semiring, &$findLastIndex, &$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$PS__Partial_Unsafe, &$insertAt) {
            return function ($ys)  use (&$PS__Data_Maybe, &$PS__Data_Semiring, &$findLastIndex, &$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$x, &$PS__Partial_Unsafe, &$insertAt) {
                $i = $PS__Data_Maybe['maybe'](0)(function ($v)  use (&$PS__Data_Semiring) {
                    return $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($v)(1);
                })($findLastIndex(function ($y)  use (&$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$x) {
                    return $PS__Data_Eq['eq']($PS__Data_Ordering['eqOrdering'])($cmp($x)($y))($PS__Data_Ordering['GT']());
                })($ys));
                return $PS__Partial_Unsafe['unsafePartial'](function ($dictPartial)  use (&$PS__Data_Maybe, &$insertAt, &$i, &$x, &$ys) {
                    return $PS__Data_Maybe['fromJust']($dictPartial)($insertAt($i)($x)($ys));
                });
            };
        };
    };
    $insert = function ($dictOrd)  use (&$insertBy, &$PS__Data_Ord) {
        return $insertBy($PS__Data_Ord['compare']($dictOrd));
    };
    $findIndex = $__foreign['findIndexImpl']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $intersectBy = function ($eq)  use (&$__foreign, &$PS__Data_Maybe, &$findIndex) {
        return function ($xs)  use (&$__foreign, &$PS__Data_Maybe, &$findIndex, &$eq) {
            return function ($ys)  use (&$__foreign, &$PS__Data_Maybe, &$findIndex, &$eq, &$xs) {
                return $__foreign['filter'](function ($x)  use (&$PS__Data_Maybe, &$findIndex, &$eq, &$ys) {
                    return $PS__Data_Maybe['isJust']($findIndex($eq($x))($ys));
                })($xs);
            };
        };
    };
    $intersect = function ($dictEq)  use (&$intersectBy, &$PS__Data_Eq) {
        return $intersectBy($PS__Data_Eq['eq']($dictEq));
    };
    $elemLastIndex = function ($dictEq)  use (&$findLastIndex, &$PS__Data_Eq) {
        return function ($x)  use (&$findLastIndex, &$PS__Data_Eq, &$dictEq) {
            return $findLastIndex(function ($v)  use (&$PS__Data_Eq, &$dictEq, &$x) {
                return $PS__Data_Eq['eq']($dictEq)($v)($x);
            });
        };
    };
    $elemIndex = function ($dictEq)  use (&$findIndex, &$PS__Data_Eq) {
        return function ($x)  use (&$findIndex, &$PS__Data_Eq, &$dictEq) {
            return $findIndex(function ($v)  use (&$PS__Data_Eq, &$dictEq, &$x) {
                return $PS__Data_Eq['eq']($dictEq)($v)($x);
            });
        };
    };
    $dropWhile = function ($p)  use (&$span) {
        return function ($xs)  use (&$span, &$p) {
            return ($span($p)($xs))['rest'];
        };
    };
    $dropEnd = function ($n)  use (&$__foreign, &$PS__Data_Ring) {
        return function ($xs)  use (&$__foreign, &$PS__Data_Ring, &$n) {
            return $__foreign['take']($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($__foreign['length']($xs))($n))($xs);
        };
    };
    $deleteAt = $__foreign['_deleteAt']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $deleteBy = function ($v)  use (&$PS__Data_Maybe, &$PS__Data_Function, &$PS__Partial_Unsafe, &$deleteAt, &$findIndex) {
        return function ($v1)  use (&$PS__Data_Maybe, &$PS__Data_Function, &$PS__Partial_Unsafe, &$deleteAt, &$findIndex, &$v) {
            return function ($v2)  use (&$PS__Data_Maybe, &$PS__Data_Function, &$PS__Partial_Unsafe, &$deleteAt, &$findIndex, &$v, &$v1) {
                if (count($v2) === 0) {
                    return [  ];
                };
                return $PS__Data_Maybe['maybe']($v2)(function ($i)  use (&$PS__Data_Function, &$PS__Partial_Unsafe, &$PS__Data_Maybe, &$deleteAt, &$v2) {
                    return $PS__Data_Function['apply']($PS__Partial_Unsafe['unsafePartial'])(function ($dictPartial)  use (&$PS__Data_Maybe, &$deleteAt, &$i, &$v2) {
                        return $PS__Data_Maybe['fromJust']($dictPartial)($deleteAt($i)($v2));
                    });
                })($findIndex($v($v1))($v2));
            };
        };
    };
    $unionBy = function ($eq)  use (&$PS__Data_Semigroup, &$PS__Data_Foldable, &$PS__Data_Function, &$deleteBy, &$nubByEq) {
        return function ($xs)  use (&$PS__Data_Semigroup, &$PS__Data_Foldable, &$PS__Data_Function, &$deleteBy, &$eq, &$nubByEq) {
            return function ($ys)  use (&$PS__Data_Semigroup, &$xs, &$PS__Data_Foldable, &$PS__Data_Function, &$deleteBy, &$eq, &$nubByEq) {
                return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupArray'])($xs)($PS__Data_Foldable['foldl']($PS__Data_Foldable['foldableArray'])($PS__Data_Function['flip']($deleteBy($eq)))($nubByEq($eq)($ys))($xs));
            };
        };
    };
    $union = function ($dictEq)  use (&$unionBy, &$PS__Data_Eq) {
        return $unionBy($PS__Data_Eq['eq']($dictEq));
    };
    $__delete = function ($dictEq)  use (&$deleteBy, &$PS__Data_Eq) {
        return $deleteBy($PS__Data_Eq['eq']($dictEq));
    };
    $difference = function ($dictEq)  use (&$PS__Data_Foldable, &$__delete) {
        return $PS__Data_Foldable['foldr']($PS__Data_Foldable['foldableArray'])($__delete($dictEq));
    };
    $concatMap = $PS__Data_Function['flip']($PS__Control_Bind['bind']($PS__Control_Bind['bindArray']));
    $mapMaybe = function ($f)  use (&$concatMap, &$PS__Control_Semigroupoid, &$PS__Data_Maybe, &$singleton) {
        return $concatMap($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['maybe']([  ])($singleton))($f));
    };
    $filterA = function ($dictApplicative)  use (&$PS__Control_Semigroupoid, &$PS__Data_Traversable, &$PS__Data_Functor, &$PS__Data_Tuple, &$mapMaybe, &$PS__Data_Maybe) {
        return function ($p)  use (&$PS__Control_Semigroupoid, &$PS__Data_Traversable, &$dictApplicative, &$PS__Data_Functor, &$PS__Data_Tuple, &$mapMaybe, &$PS__Data_Maybe) {
            return $PS__Control_Semigroupoid['composeFlipped']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Traversable['traverse']($PS__Data_Traversable['traversableArray'])($dictApplicative)(function ($x)  use (&$PS__Data_Functor, &$dictApplicative, &$PS__Data_Tuple, &$p) {
                return $PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($PS__Data_Tuple['Tuple']['create']($x))($p($x));
            }))($PS__Data_Functor['map'](($dictApplicative['Apply0']())['Functor0']())($mapMaybe(function ($v)  use (&$PS__Data_Maybe) {
                if ($v['value1']) {
                    return $PS__Data_Maybe['Just']['constructor']($v['value0']);
                };
                return $PS__Data_Maybe['Nothing']();
            })));
        };
    };
    $catMaybes = $mapMaybe($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    $alterAt = function ($i)  use (&$deleteAt, &$updateAt, &$PS__Data_Maybe, &$index) {
        return function ($f)  use (&$deleteAt, &$i, &$updateAt, &$PS__Data_Maybe, &$index) {
            return function ($xs)  use (&$f, &$deleteAt, &$i, &$updateAt, &$PS__Data_Maybe, &$index) {
                $go = function ($x)  use (&$f, &$deleteAt, &$i, &$xs, &$updateAt) {
                    $v = $f($x);
                    if ($v['type'] === 'Nothing') {
                        return $deleteAt($i)($xs);
                    };
                    if ($v['type'] === 'Just') {
                        return $updateAt($i)($v['value0'])($xs);
                    };
                    throw new \Exception('Failed pattern match at Data.Array line 544, column 10 - line 546, column 32: ' . var_dump([ $v['constructor']['name'] ]));
                };
                return $PS__Data_Maybe['maybe']($PS__Data_Maybe['Nothing']())($go)($index($xs)($i));
            };
        };
    };
    return [
        'fromFoldable' => $fromFoldable,
        'toUnfoldable' => $toUnfoldable,
        'singleton' => $singleton,
        'some' => $some,
        'many' => $many,
        'null' => $__null,
        'insert' => $insert,
        'insertBy' => $insertBy,
        'head' => $head,
        'last' => $last,
        'tail' => $tail,
        'init' => $init,
        'uncons' => $uncons,
        'unsnoc' => $unsnoc,
        'index' => $index,
        'elemIndex' => $elemIndex,
        'elemLastIndex' => $elemLastIndex,
        'findIndex' => $findIndex,
        'findLastIndex' => $findLastIndex,
        'insertAt' => $insertAt,
        'deleteAt' => $deleteAt,
        'updateAt' => $updateAt,
        'updateAtIndices' => $updateAtIndices,
        'modifyAt' => $modifyAt,
        'modifyAtIndices' => $modifyAtIndices,
        'alterAt' => $alterAt,
        'concatMap' => $concatMap,
        'filterA' => $filterA,
        'mapMaybe' => $mapMaybe,
        'catMaybes' => $catMaybes,
        'mapWithIndex' => $mapWithIndex,
        'sort' => $sort,
        'sortBy' => $sortBy,
        'sortWith' => $sortWith,
        'takeEnd' => $takeEnd,
        'takeWhile' => $takeWhile,
        'dropEnd' => $dropEnd,
        'dropWhile' => $dropWhile,
        'span' => $span,
        'group' => $group,
        'group\'' => $group__prime,
        'groupBy' => $groupBy,
        'nub' => $nub,
        'nubEq' => $nubEq,
        'nubBy' => $nubBy,
        'nubByEq' => $nubByEq,
        'union' => $union,
        'unionBy' => $unionBy,
        'delete' => $__delete,
        'deleteBy' => $deleteBy,
        'difference' => $difference,
        'intersect' => $intersect,
        'intersectBy' => $intersectBy,
        'zipWithA' => $zipWithA,
        'zip' => $zip,
        'unzip' => $unzip,
        'foldM' => $foldM,
        'foldRecM' => $foldRecM,
        'unsafeIndex' => $unsafeIndex,
        'range' => $__foreign['range'],
        'replicate' => $__foreign['replicate'],
        'length' => $__foreign['length'],
        'cons' => $__foreign['cons'],
        'snoc' => $__foreign['snoc'],
        'reverse' => $__foreign['reverse'],
        'concat' => $__foreign['concat'],
        'filter' => $__foreign['filter'],
        'partition' => $__foreign['partition'],
        'slice' => $__foreign['slice'],
        'take' => $__foreign['take'],
        'drop' => $__foreign['drop'],
        'zipWith' => $__foreign['zipWith']
    ];
})();
