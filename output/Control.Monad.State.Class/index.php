<?php
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_State_Class = (function ()  use (&$PS__Data_Tuple, &$PS__Data_Unit) {
    $MonadState = function ($Monad0, $state) {
        return [
            'Monad0' => $Monad0,
            'state' => $state
        ];
    };
    $state = function ($dict) {
        return $dict['state'];
    };
    $put = function ($dictMonadState)  use (&$state, &$PS__Data_Tuple, &$PS__Data_Unit) {
        return function ($s)  use (&$state, &$dictMonadState, &$PS__Data_Tuple, &$PS__Data_Unit) {
            return $state($dictMonadState)(function ($v)  use (&$PS__Data_Tuple, &$PS__Data_Unit, &$s) {
                return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_Unit['unit'], $s);
            });
        };
    };
    $modify_ = function ($dictMonadState)  use (&$state, &$PS__Data_Tuple, &$PS__Data_Unit) {
        return function ($f)  use (&$state, &$dictMonadState, &$PS__Data_Tuple, &$PS__Data_Unit) {
            return $state($dictMonadState)(function ($s)  use (&$PS__Data_Tuple, &$PS__Data_Unit, &$f) {
                return $PS__Data_Tuple['Tuple']['constructor']($PS__Data_Unit['unit'], $f($s));
            });
        };
    };
    $modify = function ($dictMonadState)  use (&$state, &$PS__Data_Tuple) {
        return function ($f)  use (&$state, &$dictMonadState, &$PS__Data_Tuple) {
            return $state($dictMonadState)(function ($s)  use (&$f, &$PS__Data_Tuple) {
                $s__prime = $f($s);
                return $PS__Data_Tuple['Tuple']['constructor']($s__prime, $s__prime);
            });
        };
    };
    $gets = function ($dictMonadState)  use (&$state, &$PS__Data_Tuple) {
        return function ($f)  use (&$state, &$dictMonadState, &$PS__Data_Tuple) {
            return $state($dictMonadState)(function ($s)  use (&$PS__Data_Tuple, &$f) {
                return $PS__Data_Tuple['Tuple']['constructor']($f($s), $s);
            });
        };
    };
    $get = function ($dictMonadState)  use (&$state, &$PS__Data_Tuple) {
        return $state($dictMonadState)(function ($s)  use (&$PS__Data_Tuple) {
            return $PS__Data_Tuple['Tuple']['constructor']($s, $s);
        });
    };
    return [
        'state' => $state,
        'MonadState' => $MonadState,
        'get' => $get,
        'gets' => $gets,
        'put' => $put,
        'modify' => $modify,
        'modify_' => $modify_
    ];
})();
