<?php
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
$PS__Data_Ordering = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Eq) {
    $LT = function () {
        return [
            'type' => 'LT'
        ];
    };
    $GT = function () {
        return [
            'type' => 'GT'
        ];
    };
    $EQ = function () {
        return [
            'type' => 'EQ'
        ];
    };
    $showOrdering = $PS__Data_Show['Show'](function ($v) {
        if ($v['type'] === 'LT') {
            return 'LT';
        };
        if ($v['type'] === 'GT') {
            return 'GT';
        };
        if ($v['type'] === 'EQ') {
            return 'EQ';
        };
        throw new \Exception('Failed pattern match at Data.Ordering line 26, column 1 - line 26, column 39: ' . var_dump([ $v['constructor']['name'] ]));
    });
    $semigroupOrdering = $PS__Data_Semigroup['Semigroup'](function ($v)  use (&$LT, &$GT) {
        return function ($v1)  use (&$v, &$LT, &$GT) {
            if ($v['type'] === 'LT') {
                return $LT();
            };
            if ($v['type'] === 'GT') {
                return $GT();
            };
            if ($v['type'] === 'EQ') {
                return $v1;
            };
            throw new \Exception('Failed pattern match at Data.Ordering line 21, column 1 - line 21, column 49: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
        };
    });
    $invert = function ($v)  use (&$LT, &$EQ, &$GT) {
        if ($v['type'] === 'GT') {
            return $LT();
        };
        if ($v['type'] === 'EQ') {
            return $EQ();
        };
        if ($v['type'] === 'LT') {
            return $GT();
        };
        throw new \Exception('Failed pattern match at Data.Ordering line 33, column 1 - line 33, column 31: ' . var_dump([ $v['constructor']['name'] ]));
    };
    $eqOrdering = $PS__Data_Eq['Eq'](function ($v) {
        return function ($v1)  use (&$v) {
            if ($v['type'] === 'LT' && $v1['type'] === 'LT') {
                return true;
            };
            if ($v['type'] === 'GT' && $v1['type'] === 'GT') {
                return true;
            };
            if ($v['type'] === 'EQ' && $v1['type'] === 'EQ') {
                return true;
            };
            return false;
        };
    });
    return [
        'LT' => $LT,
        'GT' => $GT,
        'EQ' => $EQ,
        'invert' => $invert,
        'eqOrdering' => $eqOrdering,
        'semigroupOrdering' => $semigroupOrdering,
        'showOrdering' => $showOrdering
    ];
})();
