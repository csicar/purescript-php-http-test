<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Alternative/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Cont.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Error.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Reader.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Rec.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.State.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Trans.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Writer.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadPlus/index.php');
require_once(dirname(__FILE__) . '/../Control.MonadZero/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Distributive/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Effect.Class/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Reader_Trans = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$PS__Control_Monad_Trans_Class, &$PS__Data_Function, &$PS__Data_Functor, &$PS__Data_Distributive, &$PS__Control_Apply, &$PS__Control_Bind, &$PS__Data_Semigroup, &$PS__Control_Applicative, &$PS__Control_Monad, &$PS__Control_Monad_Reader_Class, &$PS__Control_Monad_Cont_Class, &$PS__Effect_Class, &$PS__Control_Monad_Rec_Class, &$PS__Control_Monad_State_Class, &$PS__Control_Monad_Writer_Class, &$PS__Control_Monad_Error_Class, &$PS__Data_Monoid, &$PS__Control_Alt, &$PS__Control_Plus, &$PS__Control_Alternative, &$PS__Control_MonadZero, &$PS__Control_MonadPlus) {
    $ReaderT = function ($x) {
        return $x;
    };
    $withReaderT = function ($f)  use (&$PS__Control_Semigroupoid) {
        return function ($v)  use (&$PS__Control_Semigroupoid, &$f) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($v)($f);
        };
    };
    $runReaderT = function ($v) {
        return $v;
    };
    $newtypeReaderT = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $ReaderT);
    $monadTransReaderT = $PS__Control_Monad_Trans_Class['MonadTrans'](function ($dictMonad)  use (&$PS__Control_Semigroupoid, &$ReaderT, &$PS__Data_Function) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($ReaderT)($PS__Data_Function['const']);
    });
    $mapReaderT = function ($f)  use (&$PS__Control_Semigroupoid) {
        return function ($v)  use (&$PS__Control_Semigroupoid, &$f) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($v);
        };
    };
    $functorReaderT = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Control_Semigroupoid, &$mapReaderT) {
        return $PS__Data_Functor['Functor']($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($mapReaderT)($PS__Data_Functor['map']($dictFunctor)));
    };
    $distributiveReaderT = function ($dictDistributive)  use (&$PS__Data_Distributive, &$functorReaderT, &$PS__Control_Semigroupoid, &$distributiveReaderT, &$PS__Data_Functor) {
        return $PS__Data_Distributive['Distributive'](function ()  use (&$functorReaderT, &$dictDistributive) {
            return $functorReaderT($dictDistributive['Functor0']());
        }, function ($dictFunctor)  use (&$PS__Control_Semigroupoid, &$PS__Data_Distributive, &$distributiveReaderT, &$dictDistributive, &$PS__Data_Functor) {
            return function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Distributive, &$distributiveReaderT, &$dictDistributive, &$dictFunctor, &$PS__Data_Functor) {
                return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Distributive['distribute']($distributiveReaderT($dictDistributive))($dictFunctor))($PS__Data_Functor['map']($dictFunctor)($f));
            };
        }, function ($dictFunctor)  use (&$PS__Data_Distributive, &$dictDistributive) {
            return function ($a)  use (&$PS__Data_Distributive, &$dictDistributive, &$dictFunctor) {
                return function ($e)  use (&$PS__Data_Distributive, &$dictDistributive, &$dictFunctor, &$a) {
                    return $PS__Data_Distributive['collect']($dictDistributive)($dictFunctor)(function ($r)  use (&$e) {
                        return $r($e);
                    })($a);
                };
            };
        });
    };
    $applyReaderT = function ($dictApply)  use (&$PS__Control_Apply, &$functorReaderT) {
        return $PS__Control_Apply['Apply'](function ()  use (&$functorReaderT, &$dictApply) {
            return $functorReaderT($dictApply['Functor0']());
        }, function ($v)  use (&$PS__Control_Apply, &$dictApply) {
            return function ($v1)  use (&$PS__Control_Apply, &$dictApply, &$v) {
                return function ($r)  use (&$PS__Control_Apply, &$dictApply, &$v, &$v1) {
                    return $PS__Control_Apply['apply']($dictApply)($v($r))($v1($r));
                };
            };
        });
    };
    $bindReaderT = function ($dictBind)  use (&$PS__Control_Bind, &$applyReaderT) {
        return $PS__Control_Bind['Bind'](function ()  use (&$applyReaderT, &$dictBind) {
            return $applyReaderT($dictBind['Apply0']());
        }, function ($v)  use (&$PS__Control_Bind, &$dictBind) {
            return function ($k)  use (&$PS__Control_Bind, &$dictBind, &$v) {
                return function ($r)  use (&$PS__Control_Bind, &$dictBind, &$v, &$k) {
                    return $PS__Control_Bind['bind']($dictBind)($v($r))(function ($a)  use (&$k, &$r) {
                        $v1 = $k($a);
                        return $v1($r);
                    });
                };
            };
        });
    };
    $semigroupReaderT = function ($dictApply)  use (&$PS__Data_Semigroup, &$PS__Control_Apply, &$applyReaderT) {
        return function ($dictSemigroup)  use (&$PS__Data_Semigroup, &$PS__Control_Apply, &$applyReaderT, &$dictApply) {
            return $PS__Data_Semigroup['Semigroup']($PS__Control_Apply['lift2']($applyReaderT($dictApply))($PS__Data_Semigroup['append']($dictSemigroup)));
        };
    };
    $applicativeReaderT = function ($dictApplicative)  use (&$PS__Control_Applicative, &$applyReaderT, &$PS__Control_Semigroupoid, &$ReaderT, &$PS__Data_Function) {
        return $PS__Control_Applicative['Applicative'](function ()  use (&$applyReaderT, &$dictApplicative) {
            return $applyReaderT($dictApplicative['Apply0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($ReaderT)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Function['const'])($PS__Control_Applicative['pure']($dictApplicative))));
    };
    $monadReaderT = function ($dictMonad)  use (&$PS__Control_Monad, &$applicativeReaderT, &$bindReaderT) {
        return $PS__Control_Monad['Monad'](function ()  use (&$applicativeReaderT, &$dictMonad) {
            return $applicativeReaderT($dictMonad['Applicative0']());
        }, function ()  use (&$bindReaderT, &$dictMonad) {
            return $bindReaderT($dictMonad['Bind1']());
        });
    };
    $monadAskReaderT = function ($dictMonad)  use (&$PS__Control_Monad_Reader_Class, &$monadReaderT, &$PS__Control_Applicative) {
        return $PS__Control_Monad_Reader_Class['MonadAsk'](function ()  use (&$monadReaderT, &$dictMonad) {
            return $monadReaderT($dictMonad);
        }, $PS__Control_Applicative['pure']($dictMonad['Applicative0']()));
    };
    $monadReaderReaderT = function ($dictMonad)  use (&$PS__Control_Monad_Reader_Class, &$monadAskReaderT, &$withReaderT) {
        return $PS__Control_Monad_Reader_Class['MonadReader'](function ()  use (&$monadAskReaderT, &$dictMonad) {
            return $monadAskReaderT($dictMonad);
        }, $withReaderT);
    };
    $monadContReaderT = function ($dictMonadCont)  use (&$PS__Control_Monad_Cont_Class, &$monadReaderT, &$PS__Control_Semigroupoid, &$ReaderT, &$PS__Data_Function) {
        return $PS__Control_Monad_Cont_Class['MonadCont'](function ()  use (&$monadReaderT, &$dictMonadCont) {
            return $monadReaderT($dictMonadCont['Monad0']());
        }, function ($f)  use (&$PS__Control_Monad_Cont_Class, &$dictMonadCont, &$PS__Control_Semigroupoid, &$ReaderT, &$PS__Data_Function) {
            return function ($r)  use (&$PS__Control_Monad_Cont_Class, &$dictMonadCont, &$f, &$PS__Control_Semigroupoid, &$ReaderT, &$PS__Data_Function) {
                return $PS__Control_Monad_Cont_Class['callCC']($dictMonadCont)(function ($c)  use (&$f, &$PS__Control_Semigroupoid, &$ReaderT, &$PS__Data_Function, &$r) {
                    $v = $f($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($ReaderT)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Function['const'])($c)));
                    return $v($r);
                });
            };
        });
    };
    $monadEffectReader = function ($dictMonadEffect)  use (&$PS__Effect_Class, &$monadReaderT, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransReaderT) {
        return $PS__Effect_Class['MonadEffect'](function ()  use (&$monadReaderT, &$dictMonadEffect) {
            return $monadReaderT($dictMonadEffect['Monad0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Trans_Class['lift']($monadTransReaderT)($dictMonadEffect['Monad0']()))($PS__Effect_Class['liftEffect']($dictMonadEffect)));
    };
    $monadRecReaderT = function ($dictMonadRec)  use (&$PS__Control_Monad_Rec_Class, &$monadReaderT, &$PS__Control_Bind, &$PS__Control_Applicative) {
        return $PS__Control_Monad_Rec_Class['MonadRec'](function ()  use (&$monadReaderT, &$dictMonadRec) {
            return $monadReaderT($dictMonadRec['Monad0']());
        }, function ($k)  use (&$PS__Control_Bind, &$dictMonadRec, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class) {
            return function ($a)  use (&$k, &$PS__Control_Bind, &$dictMonadRec, &$PS__Control_Applicative, &$PS__Control_Monad_Rec_Class) {
                $k__prime = function ($r)  use (&$k, &$PS__Control_Bind, &$dictMonadRec, &$PS__Control_Applicative) {
                    return function ($a__prime)  use (&$k, &$PS__Control_Bind, &$dictMonadRec, &$PS__Control_Applicative, &$r) {
                        $v = $k($a__prime);
                        return $PS__Control_Bind['bindFlipped'](($dictMonadRec['Monad0']())['Bind1']())($PS__Control_Applicative['pure'](($dictMonadRec['Monad0']())['Applicative0']()))($v($r));
                    };
                };
                return function ($r)  use (&$PS__Control_Monad_Rec_Class, &$dictMonadRec, &$k__prime, &$a) {
                    return $PS__Control_Monad_Rec_Class['tailRecM']($dictMonadRec)($k__prime($r))($a);
                };
            };
        });
    };
    $monadStateReaderT = function ($dictMonadState)  use (&$PS__Control_Monad_State_Class, &$monadReaderT, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransReaderT) {
        return $PS__Control_Monad_State_Class['MonadState'](function ()  use (&$monadReaderT, &$dictMonadState) {
            return $monadReaderT($dictMonadState['Monad0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Trans_Class['lift']($monadTransReaderT)($dictMonadState['Monad0']()))($PS__Control_Monad_State_Class['state']($dictMonadState)));
    };
    $monadTellReaderT = function ($dictMonadTell)  use (&$PS__Control_Monad_Writer_Class, &$monadReaderT, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransReaderT) {
        return $PS__Control_Monad_Writer_Class['MonadTell'](function ()  use (&$monadReaderT, &$dictMonadTell) {
            return $monadReaderT($dictMonadTell['Monad0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Trans_Class['lift']($monadTransReaderT)($dictMonadTell['Monad0']()))($PS__Control_Monad_Writer_Class['tell']($dictMonadTell)));
    };
    $monadWriterReaderT = function ($dictMonadWriter)  use (&$PS__Control_Monad_Writer_Class, &$monadTellReaderT, &$mapReaderT) {
        return $PS__Control_Monad_Writer_Class['MonadWriter'](function ()  use (&$monadTellReaderT, &$dictMonadWriter) {
            return $monadTellReaderT($dictMonadWriter['MonadTell0']());
        }, $mapReaderT($PS__Control_Monad_Writer_Class['listen']($dictMonadWriter)), $mapReaderT($PS__Control_Monad_Writer_Class['pass']($dictMonadWriter)));
    };
    $monadThrowReaderT = function ($dictMonadThrow)  use (&$PS__Control_Monad_Error_Class, &$monadReaderT, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransReaderT) {
        return $PS__Control_Monad_Error_Class['MonadThrow'](function ()  use (&$monadReaderT, &$dictMonadThrow) {
            return $monadReaderT($dictMonadThrow['Monad0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Trans_Class['lift']($monadTransReaderT)($dictMonadThrow['Monad0']()))($PS__Control_Monad_Error_Class['throwError']($dictMonadThrow)));
    };
    $monadErrorReaderT = function ($dictMonadError)  use (&$PS__Control_Monad_Error_Class, &$monadThrowReaderT) {
        return $PS__Control_Monad_Error_Class['MonadError'](function ()  use (&$monadThrowReaderT, &$dictMonadError) {
            return $monadThrowReaderT($dictMonadError['MonadThrow0']());
        }, function ($v)  use (&$PS__Control_Monad_Error_Class, &$dictMonadError) {
            return function ($h)  use (&$PS__Control_Monad_Error_Class, &$dictMonadError, &$v) {
                return function ($r)  use (&$PS__Control_Monad_Error_Class, &$dictMonadError, &$v, &$h) {
                    return $PS__Control_Monad_Error_Class['catchError']($dictMonadError)($v($r))(function ($e)  use (&$h, &$r) {
                        $v1 = $h($e);
                        return $v1($r);
                    });
                };
            };
        });
    };
    $monoidReaderT = function ($dictApplicative)  use (&$PS__Data_Monoid, &$semigroupReaderT, &$PS__Control_Applicative, &$applicativeReaderT) {
        return function ($dictMonoid)  use (&$PS__Data_Monoid, &$semigroupReaderT, &$dictApplicative, &$PS__Control_Applicative, &$applicativeReaderT) {
            return $PS__Data_Monoid['Monoid'](function ()  use (&$semigroupReaderT, &$dictApplicative, &$dictMonoid) {
                return $semigroupReaderT($dictApplicative['Apply0']())($dictMonoid['Semigroup0']());
            }, $PS__Control_Applicative['pure']($applicativeReaderT($dictApplicative))($PS__Data_Monoid['mempty']($dictMonoid)));
        };
    };
    $altReaderT = function ($dictAlt)  use (&$PS__Control_Alt, &$functorReaderT) {
        return $PS__Control_Alt['Alt'](function ()  use (&$functorReaderT, &$dictAlt) {
            return $functorReaderT($dictAlt['Functor0']());
        }, function ($v)  use (&$PS__Control_Alt, &$dictAlt) {
            return function ($v1)  use (&$PS__Control_Alt, &$dictAlt, &$v) {
                return function ($r)  use (&$PS__Control_Alt, &$dictAlt, &$v, &$v1) {
                    return $PS__Control_Alt['alt']($dictAlt)($v($r))($v1($r));
                };
            };
        });
    };
    $plusReaderT = function ($dictPlus)  use (&$PS__Control_Plus, &$altReaderT, &$PS__Data_Function) {
        return $PS__Control_Plus['Plus'](function ()  use (&$altReaderT, &$dictPlus) {
            return $altReaderT($dictPlus['Alt0']());
        }, $PS__Data_Function['const']($PS__Control_Plus['empty']($dictPlus)));
    };
    $alternativeReaderT = function ($dictAlternative)  use (&$PS__Control_Alternative, &$applicativeReaderT, &$plusReaderT) {
        return $PS__Control_Alternative['Alternative'](function ()  use (&$applicativeReaderT, &$dictAlternative) {
            return $applicativeReaderT($dictAlternative['Applicative0']());
        }, function ()  use (&$plusReaderT, &$dictAlternative) {
            return $plusReaderT($dictAlternative['Plus1']());
        });
    };
    $monadZeroReaderT = function ($dictMonadZero)  use (&$PS__Control_MonadZero, &$alternativeReaderT, &$monadReaderT) {
        return $PS__Control_MonadZero['MonadZero'](function ()  use (&$alternativeReaderT, &$dictMonadZero) {
            return $alternativeReaderT($dictMonadZero['Alternative1']());
        }, function ()  use (&$monadReaderT, &$dictMonadZero) {
            return $monadReaderT($dictMonadZero['Monad0']());
        });
    };
    $monadPlusReaderT = function ($dictMonadPlus)  use (&$PS__Control_MonadPlus, &$monadZeroReaderT) {
        return $PS__Control_MonadPlus['MonadPlus'](function ()  use (&$monadZeroReaderT, &$dictMonadPlus) {
            return $monadZeroReaderT($dictMonadPlus['MonadZero0']());
        });
    };
    return [
        'ReaderT' => $ReaderT,
        'runReaderT' => $runReaderT,
        'withReaderT' => $withReaderT,
        'mapReaderT' => $mapReaderT,
        'newtypeReaderT' => $newtypeReaderT,
        'functorReaderT' => $functorReaderT,
        'applyReaderT' => $applyReaderT,
        'applicativeReaderT' => $applicativeReaderT,
        'altReaderT' => $altReaderT,
        'plusReaderT' => $plusReaderT,
        'alternativeReaderT' => $alternativeReaderT,
        'bindReaderT' => $bindReaderT,
        'monadReaderT' => $monadReaderT,
        'monadZeroReaderT' => $monadZeroReaderT,
        'semigroupReaderT' => $semigroupReaderT,
        'monoidReaderT' => $monoidReaderT,
        'monadPlusReaderT' => $monadPlusReaderT,
        'monadTransReaderT' => $monadTransReaderT,
        'monadEffectReader' => $monadEffectReader,
        'monadContReaderT' => $monadContReaderT,
        'monadThrowReaderT' => $monadThrowReaderT,
        'monadErrorReaderT' => $monadErrorReaderT,
        'monadAskReaderT' => $monadAskReaderT,
        'monadReaderReaderT' => $monadReaderReaderT,
        'monadStateReaderT' => $monadStateReaderT,
        'monadTellReaderT' => $monadTellReaderT,
        'monadWriterReaderT' => $monadWriterReaderT,
        'distributiveReaderT' => $distributiveReaderT,
        'monadRecReaderT' => $monadRecReaderT
    ];
})();
