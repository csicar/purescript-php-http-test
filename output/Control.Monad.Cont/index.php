<?php
require_once(dirname(__FILE__) . '/../Control.Monad.Cont.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Cont.Trans/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Identity/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Cont = (function ()  use (&$PS__Control_Monad_Cont_Trans, &$PS__Control_Semigroupoid, &$PS__Data_Identity, &$PS__Data_Newtype) {
    $withCont = function ($f)  use (&$PS__Control_Monad_Cont_Trans, &$PS__Control_Semigroupoid, &$PS__Data_Identity, &$PS__Data_Newtype) {
        return $PS__Control_Monad_Cont_Trans['withContT']($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Identity['Identity']))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity'])))));
    };
    $runCont = function ($cc)  use (&$PS__Data_Newtype, &$PS__Data_Identity, &$PS__Control_Monad_Cont_Trans, &$PS__Control_Semigroupoid) {
        return function ($k)  use (&$PS__Data_Newtype, &$PS__Data_Identity, &$PS__Control_Monad_Cont_Trans, &$cc, &$PS__Control_Semigroupoid) {
            return $PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity'])($PS__Control_Monad_Cont_Trans['runContT']($cc)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Identity['Identity'])($k)));
        };
    };
    $mapCont = function ($f)  use (&$PS__Control_Monad_Cont_Trans, &$PS__Control_Semigroupoid, &$PS__Data_Identity, &$PS__Data_Newtype) {
        return $PS__Control_Monad_Cont_Trans['mapContT']($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Identity['Identity'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity']))));
    };
    $cont = function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$PS__Data_Identity) {
        return function ($c)  use (&$f, &$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$PS__Data_Identity) {
            return $f($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity']))($c));
        };
    };
    return [
        'cont' => $cont,
        'runCont' => $runCont
    ];
})();
