<?php
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Trans_Class = (function () {
    $MonadTrans = function ($lift) {
        return [
            'lift' => $lift
        ];
    };
    $lift = function ($dict) {
        return $dict['lift'];
    };
    return [
        'lift' => $lift,
        'MonadTrans' => $MonadTrans
    ];
})();
