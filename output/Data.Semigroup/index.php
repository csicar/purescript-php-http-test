<?php
require_once(dirname(__FILE__) . '/../Data.Symbol/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Data.Void/index.php');
require_once(dirname(__FILE__) . '/../Record.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Type.Data.RowList/index.php');
$PS__Data_Semigroup = (function ()  use (&$PS__Data_Void, &$PS__Data_Unit, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Semigroup = function ($append) {
        return [
            'append' => $append
        ];
    };
    $SemigroupRecord = function ($appendRecord) {
        return [
            'appendRecord' => $appendRecord
        ];
    };
    $semigroupVoid = $Semigroup(function ($v)  use (&$PS__Data_Void) {
        return $PS__Data_Void['absurd'];
    });
    $semigroupUnit = $Semigroup(function ($v)  use (&$PS__Data_Unit) {
        return function ($v1)  use (&$PS__Data_Unit) {
            return $PS__Data_Unit['unit'];
        };
    });
    $semigroupString = $Semigroup($__foreign['concatString']);
    $semigroupRecordNil = $SemigroupRecord(function ($v) {
        return function ($v1) {
            return function ($v2) {
                return [];
            };
        };
    });
    $semigroupArray = $Semigroup($__foreign['concatArray']);
    $appendRecord = function ($dict) {
        return $dict['appendRecord'];
    };
    $semigroupRecord = function ($dictRowToList)  use (&$Semigroup, &$appendRecord, &$PS__Type_Data_RowList) {
        return function ($dictSemigroupRecord)  use (&$Semigroup, &$appendRecord, &$PS__Type_Data_RowList) {
            return $Semigroup($appendRecord($dictSemigroupRecord)($PS__Type_Data_RowList['RLProxy']()));
        };
    };
    $append = function ($dict) {
        return $dict['append'];
    };
    $semigroupFn = function ($dictSemigroup)  use (&$Semigroup, &$append) {
        return $Semigroup(function ($f)  use (&$append, &$dictSemigroup) {
            return function ($g)  use (&$append, &$dictSemigroup, &$f) {
                return function ($x)  use (&$append, &$dictSemigroup, &$f, &$g) {
                    return $append($dictSemigroup)($f($x))($g($x));
                };
            };
        });
    };
    $semigroupRecordCons = function ($dictIsSymbol)  use (&$SemigroupRecord, &$appendRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$append) {
        return function ($dictCons)  use (&$SemigroupRecord, &$appendRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$append) {
            return function ($dictSemigroupRecord)  use (&$SemigroupRecord, &$appendRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$append) {
                return function ($dictSemigroup)  use (&$SemigroupRecord, &$appendRecord, &$dictSemigroupRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$append) {
                    return $SemigroupRecord(function ($v)  use (&$appendRecord, &$dictSemigroupRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$append, &$dictSemigroup) {
                        return function ($ra)  use (&$appendRecord, &$dictSemigroupRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$append, &$dictSemigroup) {
                            return function ($rb)  use (&$appendRecord, &$dictSemigroupRecord, &$PS__Type_Data_RowList, &$ra, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$append, &$dictSemigroup) {
                                $tail = $appendRecord($dictSemigroupRecord)($PS__Type_Data_RowList['RLProxy']())($ra)($rb);
                                $key = $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($PS__Data_Symbol['SProxy']());
                                $insert = $PS__Record_Unsafe['unsafeSet']($key);
                                $get = $PS__Record_Unsafe['unsafeGet']($key);
                                return $insert($append($dictSemigroup)($get($ra))($get($rb)))($tail);
                            };
                        };
                    });
                };
            };
        };
    };
    return [
        'Semigroup' => $Semigroup,
        'append' => $append,
        'SemigroupRecord' => $SemigroupRecord,
        'appendRecord' => $appendRecord,
        'semigroupString' => $semigroupString,
        'semigroupUnit' => $semigroupUnit,
        'semigroupVoid' => $semigroupVoid,
        'semigroupFn' => $semigroupFn,
        'semigroupArray' => $semigroupArray,
        'semigroupRecord' => $semigroupRecord,
        'semigroupRecordNil' => $semigroupRecordNil,
        'semigroupRecordCons' => $semigroupRecordCons
    ];
})();
