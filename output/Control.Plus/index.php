<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
$PS__Control_Plus = (function ()  use (&$PS__Control_Alt) {
    $Plus = function ($Alt0, $empty) {
        return [
            'Alt0' => $Alt0,
            'empty' => $empty
        ];
    };
    $plusArray = $Plus(function ()  use (&$PS__Control_Alt) {
        return $PS__Control_Alt['altArray'];
    }, [  ]);
    $empty = function ($dict) {
        return $dict['empty'];
    };
    return [
        'Plus' => $Plus,
        'empty' => $empty,
        'plusArray' => $plusArray
    ];
})();
