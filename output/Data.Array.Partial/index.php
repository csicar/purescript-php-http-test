<?php
require_once(dirname(__FILE__) . '/../Data.Array/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Array_Partial = (function ()  use (&$PS__Data_Array, &$PS__Data_Ring) {
    $tail = function ($dictPartial)  use (&$PS__Data_Array) {
        return function ($xs)  use (&$PS__Data_Array) {
            return $PS__Data_Array['slice'](1)($PS__Data_Array['length']($xs))($xs);
        };
    };
    $last = function ($dictPartial)  use (&$PS__Data_Array, &$PS__Data_Ring) {
        return function ($xs)  use (&$PS__Data_Array, &$dictPartial, &$PS__Data_Ring) {
            return $PS__Data_Array['unsafeIndex']($dictPartial)($xs)($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($PS__Data_Array['length']($xs))(1));
        };
    };
    $init = function ($dictPartial)  use (&$PS__Data_Array, &$PS__Data_Ring) {
        return function ($xs)  use (&$PS__Data_Array, &$PS__Data_Ring) {
            return $PS__Data_Array['slice'](0)($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($PS__Data_Array['length']($xs))(1))($xs);
        };
    };
    $head = function ($dictPartial)  use (&$PS__Data_Array) {
        return function ($xs)  use (&$PS__Data_Array, &$dictPartial) {
            return $PS__Data_Array['unsafeIndex']($dictPartial)($xs)(0);
        };
    };
    return [
        'head' => $head,
        'tail' => $tail,
        'last' => $last,
        'init' => $init
    ];
})();
