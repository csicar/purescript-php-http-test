<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Foldable/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.FunctorWithIndex/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe.First/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe.Last/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Additive/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Conj/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Disj/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Dual/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Endo/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Multiplicative/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_FoldableWithIndex = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Control_Apply, &$PS__Control_Applicative, &$PS__Data_Unit, &$PS__Data_Function, &$PS__Data_Foldable, &$PS__Control_Bind, &$PS__Data_Semigroup, &$PS__Data_Monoid, &$PS__Data_FunctorWithIndex, &$PS__Data_Newtype, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Data_Maybe, &$PS__Data_Monoid_Disj, &$PS__Data_Monoid_Conj) {
    $Tuple = [
        'constructor' => function ($value0, $value1) {
            return [
                'type' => 'Tuple',
                'value0' => $value0,
                'value1' => $value1
            ];
        },
        'create' => function ($value0)  use (&$Tuple) {
            return function ($value1)  use (&$Tuple, &$value0) {
                return $Tuple['constructor']($value0, $value1);
            };
        }
    ];
    $FoldableWithIndex = function ($Foldable0, $foldMapWithIndex, $foldlWithIndex, $foldrWithIndex) {
        return [
            'Foldable0' => $Foldable0,
            'foldMapWithIndex' => $foldMapWithIndex,
            'foldlWithIndex' => $foldlWithIndex,
            'foldrWithIndex' => $foldrWithIndex
        ];
    };
    $foldrWithIndex = function ($dict) {
        return $dict['foldrWithIndex'];
    };
    $traverseWithIndex_ = function ($dictApplicative)  use (&$foldrWithIndex, &$PS__Control_Semigroupoid, &$PS__Control_Apply, &$PS__Control_Applicative, &$PS__Data_Unit) {
        return function ($dictFoldableWithIndex)  use (&$foldrWithIndex, &$PS__Control_Semigroupoid, &$PS__Control_Apply, &$dictApplicative, &$PS__Control_Applicative, &$PS__Data_Unit) {
            return function ($f)  use (&$foldrWithIndex, &$dictFoldableWithIndex, &$PS__Control_Semigroupoid, &$PS__Control_Apply, &$dictApplicative, &$PS__Control_Applicative, &$PS__Data_Unit) {
                return $foldrWithIndex($dictFoldableWithIndex)(function ($i)  use (&$PS__Control_Semigroupoid, &$PS__Control_Apply, &$dictApplicative, &$f) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Apply['applySecond']($dictApplicative['Apply0']()))($f($i));
                })($PS__Control_Applicative['pure']($dictApplicative)($PS__Data_Unit['unit']));
            };
        };
    };
    $forWithIndex_ = function ($dictApplicative)  use (&$PS__Data_Function, &$traverseWithIndex_) {
        return function ($dictFoldableWithIndex)  use (&$PS__Data_Function, &$traverseWithIndex_, &$dictApplicative) {
            return $PS__Data_Function['flip']($traverseWithIndex_($dictApplicative)($dictFoldableWithIndex));
        };
    };
    $foldrDefault = function ($dictFoldableWithIndex)  use (&$foldrWithIndex, &$PS__Data_Function) {
        return function ($f)  use (&$foldrWithIndex, &$dictFoldableWithIndex, &$PS__Data_Function) {
            return $foldrWithIndex($dictFoldableWithIndex)($PS__Data_Function['const']($f));
        };
    };
    $foldlWithIndex = function ($dict) {
        return $dict['foldlWithIndex'];
    };
    $foldlDefault = function ($dictFoldableWithIndex)  use (&$foldlWithIndex, &$PS__Data_Function) {
        return function ($f)  use (&$foldlWithIndex, &$dictFoldableWithIndex, &$PS__Data_Function) {
            return $foldlWithIndex($dictFoldableWithIndex)($PS__Data_Function['const']($f));
        };
    };
    $foldableWithIndexMultiplicative = $FoldableWithIndex(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableMultiplicative'];
    }, function ($dictMonoid)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$dictMonoid, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Foldable['foldMap']($PS__Data_Foldable['foldableMultiplicative'])($dictMonoid))($f($PS__Data_Unit['unit']));
        };
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldl']($PS__Data_Foldable['foldableMultiplicative']))($f($PS__Data_Unit['unit']));
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldr']($PS__Data_Foldable['foldableMultiplicative']))($f($PS__Data_Unit['unit']));
    });
    $foldableWithIndexMaybe = $FoldableWithIndex(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableMaybe'];
    }, function ($dictMonoid)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$dictMonoid, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Foldable['foldMap']($PS__Data_Foldable['foldableMaybe'])($dictMonoid))($f($PS__Data_Unit['unit']));
        };
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldl']($PS__Data_Foldable['foldableMaybe']))($f($PS__Data_Unit['unit']));
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldr']($PS__Data_Foldable['foldableMaybe']))($f($PS__Data_Unit['unit']));
    });
    $foldableWithIndexLast = $FoldableWithIndex(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableLast'];
    }, function ($dictMonoid)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$dictMonoid, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Foldable['foldMap']($PS__Data_Foldable['foldableLast'])($dictMonoid))($f($PS__Data_Unit['unit']));
        };
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldl']($PS__Data_Foldable['foldableLast']))($f($PS__Data_Unit['unit']));
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldr']($PS__Data_Foldable['foldableLast']))($f($PS__Data_Unit['unit']));
    });
    $foldableWithIndexFirst = $FoldableWithIndex(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableFirst'];
    }, function ($dictMonoid)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$dictMonoid, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Foldable['foldMap']($PS__Data_Foldable['foldableFirst'])($dictMonoid))($f($PS__Data_Unit['unit']));
        };
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldl']($PS__Data_Foldable['foldableFirst']))($f($PS__Data_Unit['unit']));
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldr']($PS__Data_Foldable['foldableFirst']))($f($PS__Data_Unit['unit']));
    });
    $foldableWithIndexDual = $FoldableWithIndex(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableDual'];
    }, function ($dictMonoid)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$dictMonoid, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Foldable['foldMap']($PS__Data_Foldable['foldableDual'])($dictMonoid))($f($PS__Data_Unit['unit']));
        };
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldl']($PS__Data_Foldable['foldableDual']))($f($PS__Data_Unit['unit']));
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldr']($PS__Data_Foldable['foldableDual']))($f($PS__Data_Unit['unit']));
    });
    $foldableWithIndexDisj = $FoldableWithIndex(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableDisj'];
    }, function ($dictMonoid)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$dictMonoid, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Foldable['foldMap']($PS__Data_Foldable['foldableDisj'])($dictMonoid))($f($PS__Data_Unit['unit']));
        };
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldl']($PS__Data_Foldable['foldableDisj']))($f($PS__Data_Unit['unit']));
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldr']($PS__Data_Foldable['foldableDisj']))($f($PS__Data_Unit['unit']));
    });
    $foldableWithIndexConj = $FoldableWithIndex(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableConj'];
    }, function ($dictMonoid)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$dictMonoid, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Foldable['foldMap']($PS__Data_Foldable['foldableConj'])($dictMonoid))($f($PS__Data_Unit['unit']));
        };
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldl']($PS__Data_Foldable['foldableConj']))($f($PS__Data_Unit['unit']));
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldr']($PS__Data_Foldable['foldableConj']))($f($PS__Data_Unit['unit']));
    });
    $foldableWithIndexAdditive = $FoldableWithIndex(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableAdditive'];
    }, function ($dictMonoid)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$dictMonoid, &$PS__Data_Unit) {
            return $PS__Data_Function['apply']($PS__Data_Foldable['foldMap']($PS__Data_Foldable['foldableAdditive'])($dictMonoid))($f($PS__Data_Unit['unit']));
        };
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldl']($PS__Data_Foldable['foldableAdditive']))($f($PS__Data_Unit['unit']));
    }, function ($f)  use (&$PS__Data_Function, &$PS__Data_Foldable, &$PS__Data_Unit) {
        return $PS__Data_Function['apply']($PS__Data_Foldable['foldr']($PS__Data_Foldable['foldableAdditive']))($f($PS__Data_Unit['unit']));
    });
    $foldWithIndexM = function ($dictFoldableWithIndex)  use (&$foldlWithIndex, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative) {
        return function ($dictMonad)  use (&$foldlWithIndex, &$dictFoldableWithIndex, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative) {
            return function ($f)  use (&$foldlWithIndex, &$dictFoldableWithIndex, &$PS__Control_Bind, &$dictMonad, &$PS__Data_Function, &$PS__Control_Applicative) {
                return function ($a0)  use (&$foldlWithIndex, &$dictFoldableWithIndex, &$PS__Control_Bind, &$dictMonad, &$PS__Data_Function, &$f, &$PS__Control_Applicative) {
                    return $foldlWithIndex($dictFoldableWithIndex)(function ($i)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Data_Function, &$f) {
                        return function ($ma)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Data_Function, &$f, &$i) {
                            return function ($b)  use (&$PS__Control_Bind, &$dictMonad, &$ma, &$PS__Data_Function, &$f, &$i) {
                                return $PS__Control_Bind['bind']($dictMonad['Bind1']())($ma)($PS__Data_Function['flip']($f($i))($b));
                            };
                        };
                    })($PS__Control_Applicative['pure']($dictMonad['Applicative0']())($a0));
                };
            };
        };
    };
    $foldMapWithIndexDefaultR = function ($dictFoldableWithIndex)  use (&$foldrWithIndex, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
        return function ($dictMonoid)  use (&$foldrWithIndex, &$dictFoldableWithIndex, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
            return function ($f)  use (&$foldrWithIndex, &$dictFoldableWithIndex, &$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Monoid) {
                return $foldrWithIndex($dictFoldableWithIndex)(function ($i)  use (&$PS__Data_Semigroup, &$dictMonoid, &$f) {
                    return function ($x)  use (&$PS__Data_Semigroup, &$dictMonoid, &$f, &$i) {
                        return function ($acc)  use (&$PS__Data_Semigroup, &$dictMonoid, &$f, &$i, &$x) {
                            return $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($f($i)($x))($acc);
                        };
                    };
                })($PS__Data_Monoid['mempty']($dictMonoid));
            };
        };
    };
    $foldableWithIndexArray = $FoldableWithIndex(function ()  use (&$PS__Data_Foldable) {
        return $PS__Data_Foldable['foldableArray'];
    }, function ($dictMonoid)  use (&$foldMapWithIndexDefaultR, &$foldableWithIndexArray) {
        return $foldMapWithIndexDefaultR($foldableWithIndexArray)($dictMonoid);
    }, function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Foldable, &$PS__Data_FunctorWithIndex, &$Tuple) {
        return function ($z)  use (&$PS__Control_Semigroupoid, &$PS__Data_Foldable, &$f, &$PS__Data_FunctorWithIndex, &$Tuple) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Foldable['foldl']($PS__Data_Foldable['foldableArray'])(function ($y)  use (&$f) {
                return function ($v)  use (&$f, &$y) {
                    return $f($v['value0'])($y)($v['value1']);
                };
            })($z))($PS__Data_FunctorWithIndex['mapWithIndex']($PS__Data_FunctorWithIndex['functorWithIndexArray'])($Tuple['create']));
        };
    }, function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Foldable, &$PS__Data_FunctorWithIndex, &$Tuple) {
        return function ($z)  use (&$PS__Control_Semigroupoid, &$PS__Data_Foldable, &$f, &$PS__Data_FunctorWithIndex, &$Tuple) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Foldable['foldr']($PS__Data_Foldable['foldableArray'])(function ($v)  use (&$f) {
                return function ($y)  use (&$f, &$v) {
                    return $f($v['value0'])($v['value1'])($y);
                };
            })($z))($PS__Data_FunctorWithIndex['mapWithIndex']($PS__Data_FunctorWithIndex['functorWithIndexArray'])($Tuple['create']));
        };
    });
    $foldMapWithIndexDefaultL = function ($dictFoldableWithIndex)  use (&$foldlWithIndex, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
        return function ($dictMonoid)  use (&$foldlWithIndex, &$dictFoldableWithIndex, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
            return function ($f)  use (&$foldlWithIndex, &$dictFoldableWithIndex, &$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Monoid) {
                return $foldlWithIndex($dictFoldableWithIndex)(function ($i)  use (&$PS__Data_Semigroup, &$dictMonoid, &$f) {
                    return function ($acc)  use (&$PS__Data_Semigroup, &$dictMonoid, &$f, &$i) {
                        return function ($x)  use (&$PS__Data_Semigroup, &$dictMonoid, &$acc, &$f, &$i) {
                            return $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($acc)($f($i)($x));
                        };
                    };
                })($PS__Data_Monoid['mempty']($dictMonoid));
            };
        };
    };
    $foldMapWithIndex = function ($dict) {
        return $dict['foldMapWithIndex'];
    };
    $foldlWithIndexDefault = function ($dictFoldableWithIndex)  use (&$PS__Data_Newtype, &$foldMapWithIndex, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$PS__Data_Function) {
        return function ($c)  use (&$PS__Data_Newtype, &$foldMapWithIndex, &$dictFoldableWithIndex, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$PS__Data_Function) {
            return function ($u)  use (&$PS__Data_Newtype, &$foldMapWithIndex, &$dictFoldableWithIndex, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$PS__Data_Function, &$c) {
                return function ($xs)  use (&$PS__Data_Newtype, &$foldMapWithIndex, &$dictFoldableWithIndex, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$PS__Data_Function, &$c, &$u) {
                    return $PS__Data_Newtype['unwrap']($PS__Data_Newtype['newtypeEndo'])($PS__Data_Newtype['unwrap']($PS__Data_Newtype['newtypeDual'])($foldMapWithIndex($dictFoldableWithIndex)($PS__Data_Monoid_Dual['monoidDual']($PS__Data_Monoid_Endo['monoidEndo']($PS__Control_Category['categoryFn'])))(function ($i)  use (&$PS__Control_Semigroupoid, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Data_Function, &$c) {
                        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Dual['Dual'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Endo['Endo'])($PS__Data_Function['flip']($c($i))));
                    })($xs)))($u);
                };
            };
        };
    };
    $foldrWithIndexDefault = function ($dictFoldableWithIndex)  use (&$PS__Data_Newtype, &$foldMapWithIndex, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid) {
        return function ($c)  use (&$PS__Data_Newtype, &$foldMapWithIndex, &$dictFoldableWithIndex, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid) {
            return function ($u)  use (&$PS__Data_Newtype, &$foldMapWithIndex, &$dictFoldableWithIndex, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$c) {
                return function ($xs)  use (&$PS__Data_Newtype, &$foldMapWithIndex, &$dictFoldableWithIndex, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$c, &$u) {
                    return $PS__Data_Newtype['unwrap']($PS__Data_Newtype['newtypeEndo'])($foldMapWithIndex($dictFoldableWithIndex)($PS__Data_Monoid_Endo['monoidEndo']($PS__Control_Category['categoryFn']))(function ($i)  use (&$PS__Control_Semigroupoid, &$PS__Data_Monoid_Endo, &$c) {
                        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Endo['Endo'])($c($i));
                    })($xs))($u);
                };
            };
        };
    };
    $surroundMapWithIndex = function ($dictFoldableWithIndex)  use (&$PS__Data_Semigroup, &$PS__Data_Newtype, &$foldMapWithIndex, &$PS__Data_Monoid_Endo, &$PS__Control_Category) {
        return function ($dictSemigroup)  use (&$PS__Data_Semigroup, &$PS__Data_Newtype, &$foldMapWithIndex, &$dictFoldableWithIndex, &$PS__Data_Monoid_Endo, &$PS__Control_Category) {
            return function ($d)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$PS__Data_Newtype, &$foldMapWithIndex, &$dictFoldableWithIndex, &$PS__Data_Monoid_Endo, &$PS__Control_Category) {
                return function ($t)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$d, &$PS__Data_Newtype, &$foldMapWithIndex, &$dictFoldableWithIndex, &$PS__Data_Monoid_Endo, &$PS__Control_Category) {
                    return function ($f)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$d, &$t, &$PS__Data_Newtype, &$foldMapWithIndex, &$dictFoldableWithIndex, &$PS__Data_Monoid_Endo, &$PS__Control_Category) {
                        $joined = function ($i)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$d, &$t) {
                            return function ($a)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$d, &$t, &$i) {
                                return function ($m)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$d, &$t, &$i, &$a) {
                                    return $PS__Data_Semigroup['append']($dictSemigroup)($d)($PS__Data_Semigroup['append']($dictSemigroup)($t($i)($a))($m));
                                };
                            };
                        };
                        return $PS__Data_Newtype['unwrap']($PS__Data_Newtype['newtypeEndo'])($foldMapWithIndex($dictFoldableWithIndex)($PS__Data_Monoid_Endo['monoidEndo']($PS__Control_Category['categoryFn']))($joined)($f))($d);
                    };
                };
            };
        };
    };
    $foldMapDefault = function ($dictFoldableWithIndex)  use (&$foldMapWithIndex, &$PS__Data_Function) {
        return function ($dictMonoid)  use (&$foldMapWithIndex, &$dictFoldableWithIndex, &$PS__Data_Function) {
            return function ($f)  use (&$foldMapWithIndex, &$dictFoldableWithIndex, &$dictMonoid, &$PS__Data_Function) {
                return $foldMapWithIndex($dictFoldableWithIndex)($dictMonoid)($PS__Data_Function['const']($f));
            };
        };
    };
    $findWithIndex = function ($dictFoldableWithIndex)  use (&$PS__Data_Maybe, &$foldlWithIndex) {
        return function ($p)  use (&$PS__Data_Maybe, &$foldlWithIndex, &$dictFoldableWithIndex) {
            $go = function ($v)  use (&$p, &$PS__Data_Maybe) {
                return function ($v1)  use (&$p, &$v, &$PS__Data_Maybe) {
                    return function ($v2)  use (&$v1, &$p, &$v, &$PS__Data_Maybe) {
                        if ($v1['type'] === 'Nothing' && $p($v)($v2)) {
                            return $PS__Data_Maybe['Just']['constructor']([
                                'index' => $v,
                                'value' => $v2
                            ]);
                        };
                        return $v1;
                    };
                };
            };
            return $foldlWithIndex($dictFoldableWithIndex)($go)($PS__Data_Maybe['Nothing']());
        };
    };
    $anyWithIndex = function ($dictFoldableWithIndex)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$foldMapWithIndex, &$PS__Data_Monoid_Disj) {
        return function ($dictHeytingAlgebra)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$foldMapWithIndex, &$dictFoldableWithIndex, &$PS__Data_Monoid_Disj) {
            return function ($t)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$foldMapWithIndex, &$dictFoldableWithIndex, &$PS__Data_Monoid_Disj, &$dictHeytingAlgebra) {
                return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Newtype['unwrap']($PS__Data_Newtype['newtypeDisj']))($foldMapWithIndex($dictFoldableWithIndex)($PS__Data_Monoid_Disj['monoidDisj']($dictHeytingAlgebra))(function ($i)  use (&$PS__Control_Semigroupoid, &$PS__Data_Monoid_Disj, &$t) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Disj['Disj'])($t($i));
                }));
            };
        };
    };
    $allWithIndex = function ($dictFoldableWithIndex)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$foldMapWithIndex, &$PS__Data_Monoid_Conj) {
        return function ($dictHeytingAlgebra)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$foldMapWithIndex, &$dictFoldableWithIndex, &$PS__Data_Monoid_Conj) {
            return function ($t)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$foldMapWithIndex, &$dictFoldableWithIndex, &$PS__Data_Monoid_Conj, &$dictHeytingAlgebra) {
                return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Newtype['unwrap']($PS__Data_Newtype['newtypeConj']))($foldMapWithIndex($dictFoldableWithIndex)($PS__Data_Monoid_Conj['monoidConj']($dictHeytingAlgebra))(function ($i)  use (&$PS__Control_Semigroupoid, &$PS__Data_Monoid_Conj, &$t) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Conj['Conj'])($t($i));
                }));
            };
        };
    };
    return [
        'FoldableWithIndex' => $FoldableWithIndex,
        'foldrWithIndex' => $foldrWithIndex,
        'foldlWithIndex' => $foldlWithIndex,
        'foldMapWithIndex' => $foldMapWithIndex,
        'foldrWithIndexDefault' => $foldrWithIndexDefault,
        'foldlWithIndexDefault' => $foldlWithIndexDefault,
        'foldMapWithIndexDefaultR' => $foldMapWithIndexDefaultR,
        'foldMapWithIndexDefaultL' => $foldMapWithIndexDefaultL,
        'foldWithIndexM' => $foldWithIndexM,
        'traverseWithIndex_' => $traverseWithIndex_,
        'forWithIndex_' => $forWithIndex_,
        'surroundMapWithIndex' => $surroundMapWithIndex,
        'allWithIndex' => $allWithIndex,
        'anyWithIndex' => $anyWithIndex,
        'findWithIndex' => $findWithIndex,
        'foldrDefault' => $foldrDefault,
        'foldlDefault' => $foldlDefault,
        'foldMapDefault' => $foldMapDefault,
        'foldableWithIndexArray' => $foldableWithIndexArray,
        'foldableWithIndexMaybe' => $foldableWithIndexMaybe,
        'foldableWithIndexFirst' => $foldableWithIndexFirst,
        'foldableWithIndexLast' => $foldableWithIndexLast,
        'foldableWithIndexAdditive' => $foldableWithIndexAdditive,
        'foldableWithIndexDual' => $foldableWithIndexDual,
        'foldableWithIndexDisj' => $foldableWithIndexDisj,
        'foldableWithIndexConj' => $foldableWithIndexConj,
        'foldableWithIndexMultiplicative' => $foldableWithIndexMultiplicative
    ];
})();
