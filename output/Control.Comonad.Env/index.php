<?php
require_once(dirname(__FILE__) . '/../Control.Comonad.Env.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Comonad.Env.Trans/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Identity/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Comonad_Env = (function ()  use (&$PS__Control_Comonad_Env_Trans, &$PS__Data_Functor, &$PS__Data_Tuple, &$PS__Data_Newtype, &$PS__Data_Identity, &$PS__Data_Function) {
    $withEnv = $PS__Control_Comonad_Env_Trans['withEnvT'];
    $runEnv = function ($v)  use (&$PS__Data_Functor, &$PS__Data_Tuple, &$PS__Data_Newtype, &$PS__Data_Identity) {
        return $PS__Data_Functor['map']($PS__Data_Tuple['functorTuple'])($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity']))($v);
    };
    $mapEnv = $PS__Data_Functor['map']($PS__Control_Comonad_Env_Trans['functorEnvT']($PS__Data_Identity['functorIdentity']));
    $env = function ($e)  use (&$PS__Data_Function, &$PS__Control_Comonad_Env_Trans, &$PS__Data_Tuple) {
        return function ($a)  use (&$PS__Data_Function, &$PS__Control_Comonad_Env_Trans, &$PS__Data_Tuple, &$e) {
            return $PS__Data_Function['apply']($PS__Control_Comonad_Env_Trans['EnvT'])($PS__Data_Function['apply']($PS__Data_Tuple['Tuple']['create']($e))($a));
        };
    };
    return [
        'runEnv' => $runEnv,
        'withEnv' => $withEnv,
        'mapEnv' => $mapEnv,
        'env' => $env
    ];
})();
