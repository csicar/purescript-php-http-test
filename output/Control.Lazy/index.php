<?php
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
$PS__Control_Lazy = (function ()  use (&$PS__Data_Unit) {
    $Lazy = function ($defer) {
        return [
            'defer' => $defer
        ];
    };
    $lazyUnit = $Lazy(function ($v)  use (&$PS__Data_Unit) {
        return $PS__Data_Unit['unit'];
    });
    $lazyFn = $Lazy(function ($f)  use (&$PS__Data_Unit) {
        return function ($x)  use (&$f, &$PS__Data_Unit) {
            return $f($PS__Data_Unit['unit'])($x);
        };
    });
    $defer = function ($dict) {
        return $dict['defer'];
    };
    $fix = function ($dictLazy)  use (&$defer) {
        return function ($f)  use (&$defer, &$dictLazy) {
            $go = $defer($dictLazy)(function ($v)  use (&$f, &$go) {
                return $f($go);
            });
            return $go;
        };
    };
    return [
        'defer' => $defer,
        'Lazy' => $Lazy,
        'fix' => $fix,
        'lazyFn' => $lazyFn,
        'lazyUnit' => $lazyUnit
    ];
})();
