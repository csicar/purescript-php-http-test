<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Array.NonEmpty/index.php');
require_once(dirname(__FILE__) . '/../Data.Either/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Data.String/index.php');
require_once(dirname(__FILE__) . '/../Data.String.CodeUnits/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Pattern/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Regex.Flags/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_String_Regex = (function ()  use (&$PS__Data_Show, &$PS__Data_Maybe, &$PS__Data_Semigroup, &$PS__Data_Function, &$PS__Data_Either, &$PS__Data_String_Regex_Flags, &$PS__Data_String_CodeUnits, &$PS__Control_Semigroupoid) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $showRegex = $PS__Data_Show['Show']($__foreign['showRegex\'']);
    $search = $__foreign['_search']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $renderFlags = function ($v)  use (&$PS__Data_Semigroup) {
        return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])((function ()  use (&$v) {
            if ($v['value0']['global']) {
                return 'g';
            };
            return '';
        })())($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])((function ()  use (&$v) {
            if ($v['value0']['ignoreCase']) {
                return 'i';
            };
            return '';
        })())($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])((function ()  use (&$v) {
            if ($v['value0']['multiline']) {
                return 'm';
            };
            return '';
        })())($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])((function ()  use (&$v) {
            if ($v['value0']['sticky']) {
                return 'y';
            };
            return '';
        })())((function ()  use (&$v) {
            if ($v['value0']['unicode']) {
                return 'u';
            };
            return '';
        })()))));
    };
    $regex = function ($s)  use (&$PS__Data_Function, &$__foreign, &$PS__Data_Either, &$renderFlags) {
        return function ($f)  use (&$PS__Data_Function, &$__foreign, &$PS__Data_Either, &$s, &$renderFlags) {
            return $PS__Data_Function['apply']($__foreign['regex\'']($PS__Data_Either['Left']['create'])($PS__Data_Either['Right']['create'])($s))($renderFlags($f));
        };
    };
    $parseFlags = function ($s)  use (&$PS__Data_String_Regex_Flags, &$PS__Data_String_CodeUnits) {
        return $PS__Data_String_Regex_Flags['RegexFlags']['constructor']([
            'global' => $PS__Data_String_CodeUnits['contains']('g')($s),
            'ignoreCase' => $PS__Data_String_CodeUnits['contains']('i')($s),
            'multiline' => $PS__Data_String_CodeUnits['contains']('m')($s),
            'sticky' => $PS__Data_String_CodeUnits['contains']('y')($s),
            'unicode' => $PS__Data_String_CodeUnits['contains']('u')($s)
        ]);
    };
    $match = $__foreign['_match']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $flags = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_String_Regex_Flags['RegexFlags']['create'])($__foreign['flags\'']);
    return [
        'regex' => $regex,
        'flags' => $flags,
        'renderFlags' => $renderFlags,
        'parseFlags' => $parseFlags,
        'match' => $match,
        'search' => $search,
        'showRegex' => $showRegex,
        'source' => $__foreign['source'],
        'test' => $__foreign['test'],
        'replace' => $__foreign['replace'],
        'replace\'' => $__foreign['replace\''],
        'split' => $__foreign['split']
    ];
})();
