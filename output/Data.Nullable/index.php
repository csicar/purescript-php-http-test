<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Function.Uncurried/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Nullable = (function ()  use (&$PS__Data_Maybe, &$PS__Data_Function_Uncurried, &$PS__Data_Show, &$PS__Control_Semigroupoid, &$PS__Data_Eq, &$PS__Data_Function, &$PS__Data_Ord) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $toNullable = $PS__Data_Maybe['maybe']($__foreign['null'])($__foreign['notNull']);
    $toMaybe = function ($n)  use (&$PS__Data_Function_Uncurried, &$__foreign, &$PS__Data_Maybe) {
        return $PS__Data_Function_Uncurried['runFn3']($__foreign['nullable'])($n)($PS__Data_Maybe['Nothing']())($PS__Data_Maybe['Just']['create']);
    };
    $showNullable = function ($dictShow)  use (&$PS__Data_Show, &$PS__Control_Semigroupoid, &$PS__Data_Maybe, &$toMaybe) {
        return $PS__Data_Show['Show']($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['maybe']('null')($PS__Data_Show['show']($dictShow)))($toMaybe));
    };
    $eqNullable = function ($dictEq)  use (&$PS__Data_Eq, &$PS__Data_Function, &$PS__Data_Maybe, &$toMaybe) {
        return $PS__Data_Eq['Eq']($PS__Data_Function['on']($PS__Data_Eq['eq']($PS__Data_Maybe['eqMaybe']($dictEq)))($toMaybe));
    };
    $ordNullable = function ($dictOrd)  use (&$PS__Data_Ord, &$eqNullable, &$PS__Data_Function, &$PS__Data_Maybe, &$toMaybe) {
        return $PS__Data_Ord['Ord'](function ()  use (&$eqNullable, &$dictOrd) {
            return $eqNullable($dictOrd['Eq0']());
        }, $PS__Data_Function['on']($PS__Data_Ord['compare']($PS__Data_Maybe['ordMaybe']($dictOrd)))($toMaybe));
    };
    $eq1Nullable = $PS__Data_Eq['Eq1'](function ($dictEq)  use (&$PS__Data_Eq, &$eqNullable) {
        return $PS__Data_Eq['eq']($eqNullable($dictEq));
    });
    $ord1Nullable = $PS__Data_Ord['Ord1'](function ()  use (&$eq1Nullable) {
        return $eq1Nullable;
    }, function ($dictOrd)  use (&$PS__Data_Ord, &$ordNullable) {
        return $PS__Data_Ord['compare']($ordNullable($dictOrd));
    });
    return [
        'toMaybe' => $toMaybe,
        'toNullable' => $toNullable,
        'showNullable' => $showNullable,
        'eqNullable' => $eqNullable,
        'eq1Nullable' => $eq1Nullable,
        'ordNullable' => $ordNullable,
        'ord1Nullable' => $ord1Nullable
    ];
})();
