<?php
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Tuple_Nested = (function ()  use (&$PS__Data_Tuple, &$PS__Data_Unit) {
    $uncurry9 = function ($f__prime) {
        return function ($v)  use (&$f__prime) {
            return $f__prime($v['value0'])($v['value1']['value0'])($v['value1']['value1']['value0'])($v['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value0']);
        };
    };
    $uncurry8 = function ($f__prime) {
        return function ($v)  use (&$f__prime) {
            return $f__prime($v['value0'])($v['value1']['value0'])($v['value1']['value1']['value0'])($v['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value0']);
        };
    };
    $uncurry7 = function ($f__prime) {
        return function ($v)  use (&$f__prime) {
            return $f__prime($v['value0'])($v['value1']['value0'])($v['value1']['value1']['value0'])($v['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value1']['value1']['value0']);
        };
    };
    $uncurry6 = function ($f__prime) {
        return function ($v)  use (&$f__prime) {
            return $f__prime($v['value0'])($v['value1']['value0'])($v['value1']['value1']['value0'])($v['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value1']['value0']);
        };
    };
    $uncurry5 = function ($f) {
        return function ($v)  use (&$f) {
            return $f($v['value0'])($v['value1']['value0'])($v['value1']['value1']['value0'])($v['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value0']);
        };
    };
    $uncurry4 = function ($f) {
        return function ($v)  use (&$f) {
            return $f($v['value0'])($v['value1']['value0'])($v['value1']['value1']['value0'])($v['value1']['value1']['value1']['value0']);
        };
    };
    $uncurry3 = function ($f) {
        return function ($v)  use (&$f) {
            return $f($v['value0'])($v['value1']['value0'])($v['value1']['value1']['value0']);
        };
    };
    $uncurry2 = function ($f) {
        return function ($v)  use (&$f) {
            return $f($v['value0'])($v['value1']['value0']);
        };
    };
    $uncurry10 = function ($f__prime) {
        return function ($v)  use (&$f__prime) {
            return $f__prime($v['value0'])($v['value1']['value0'])($v['value1']['value1']['value0'])($v['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value0'])($v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value0']);
        };
    };
    $uncurry1 = function ($f) {
        return function ($v)  use (&$f) {
            return $f($v['value0']);
        };
    };
    $tuple9 = function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Unit) {
        return function ($b)  use (&$PS__Data_Tuple, &$a, &$PS__Data_Unit) {
            return function ($c)  use (&$PS__Data_Tuple, &$a, &$b, &$PS__Data_Unit) {
                return function ($d)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$PS__Data_Unit) {
                    return function ($e)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$PS__Data_Unit) {
                        return function ($f)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$PS__Data_Unit) {
                            return function ($g)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$PS__Data_Unit) {
                                return function ($h)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$PS__Data_Unit) {
                                    return function ($i)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h, &$PS__Data_Unit) {
                                        return $PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $PS__Data_Tuple['Tuple']['constructor']($d, $PS__Data_Tuple['Tuple']['constructor']($e, $PS__Data_Tuple['Tuple']['constructor']($f, $PS__Data_Tuple['Tuple']['constructor']($g, $PS__Data_Tuple['Tuple']['constructor']($h, $PS__Data_Tuple['Tuple']['constructor']($i, $PS__Data_Unit['unit'])))))))));
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    $tuple8 = function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Unit) {
        return function ($b)  use (&$PS__Data_Tuple, &$a, &$PS__Data_Unit) {
            return function ($c)  use (&$PS__Data_Tuple, &$a, &$b, &$PS__Data_Unit) {
                return function ($d)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$PS__Data_Unit) {
                    return function ($e)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$PS__Data_Unit) {
                        return function ($f)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$PS__Data_Unit) {
                            return function ($g)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$PS__Data_Unit) {
                                return function ($h)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$PS__Data_Unit) {
                                    return $PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $PS__Data_Tuple['Tuple']['constructor']($d, $PS__Data_Tuple['Tuple']['constructor']($e, $PS__Data_Tuple['Tuple']['constructor']($f, $PS__Data_Tuple['Tuple']['constructor']($g, $PS__Data_Tuple['Tuple']['constructor']($h, $PS__Data_Unit['unit']))))))));
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    $tuple7 = function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Unit) {
        return function ($b)  use (&$PS__Data_Tuple, &$a, &$PS__Data_Unit) {
            return function ($c)  use (&$PS__Data_Tuple, &$a, &$b, &$PS__Data_Unit) {
                return function ($d)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$PS__Data_Unit) {
                    return function ($e)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$PS__Data_Unit) {
                        return function ($f)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$PS__Data_Unit) {
                            return function ($g)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$PS__Data_Unit) {
                                return $PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $PS__Data_Tuple['Tuple']['constructor']($d, $PS__Data_Tuple['Tuple']['constructor']($e, $PS__Data_Tuple['Tuple']['constructor']($f, $PS__Data_Tuple['Tuple']['constructor']($g, $PS__Data_Unit['unit'])))))));
                            };
                        };
                    };
                };
            };
        };
    };
    $tuple6 = function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Unit) {
        return function ($b)  use (&$PS__Data_Tuple, &$a, &$PS__Data_Unit) {
            return function ($c)  use (&$PS__Data_Tuple, &$a, &$b, &$PS__Data_Unit) {
                return function ($d)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$PS__Data_Unit) {
                    return function ($e)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$PS__Data_Unit) {
                        return function ($f)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$PS__Data_Unit) {
                            return $PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $PS__Data_Tuple['Tuple']['constructor']($d, $PS__Data_Tuple['Tuple']['constructor']($e, $PS__Data_Tuple['Tuple']['constructor']($f, $PS__Data_Unit['unit']))))));
                        };
                    };
                };
            };
        };
    };
    $tuple5 = function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Unit) {
        return function ($b)  use (&$PS__Data_Tuple, &$a, &$PS__Data_Unit) {
            return function ($c)  use (&$PS__Data_Tuple, &$a, &$b, &$PS__Data_Unit) {
                return function ($d)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$PS__Data_Unit) {
                    return function ($e)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$PS__Data_Unit) {
                        return $PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $PS__Data_Tuple['Tuple']['constructor']($d, $PS__Data_Tuple['Tuple']['constructor']($e, $PS__Data_Unit['unit'])))));
                    };
                };
            };
        };
    };
    $tuple4 = function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Unit) {
        return function ($b)  use (&$PS__Data_Tuple, &$a, &$PS__Data_Unit) {
            return function ($c)  use (&$PS__Data_Tuple, &$a, &$b, &$PS__Data_Unit) {
                return function ($d)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$PS__Data_Unit) {
                    return $PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $PS__Data_Tuple['Tuple']['constructor']($d, $PS__Data_Unit['unit']))));
                };
            };
        };
    };
    $tuple3 = function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Unit) {
        return function ($b)  use (&$PS__Data_Tuple, &$a, &$PS__Data_Unit) {
            return function ($c)  use (&$PS__Data_Tuple, &$a, &$b, &$PS__Data_Unit) {
                return $PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $PS__Data_Unit['unit'])));
            };
        };
    };
    $tuple2 = function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Unit) {
        return function ($b)  use (&$PS__Data_Tuple, &$a, &$PS__Data_Unit) {
            return $PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Unit['unit']));
        };
    };
    $tuple10 = function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Unit) {
        return function ($b)  use (&$PS__Data_Tuple, &$a, &$PS__Data_Unit) {
            return function ($c)  use (&$PS__Data_Tuple, &$a, &$b, &$PS__Data_Unit) {
                return function ($d)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$PS__Data_Unit) {
                    return function ($e)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$PS__Data_Unit) {
                        return function ($f)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$PS__Data_Unit) {
                            return function ($g)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$PS__Data_Unit) {
                                return function ($h)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$PS__Data_Unit) {
                                    return function ($i)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h, &$PS__Data_Unit) {
                                        return function ($j)  use (&$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h, &$i, &$PS__Data_Unit) {
                                            return $PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $PS__Data_Tuple['Tuple']['constructor']($d, $PS__Data_Tuple['Tuple']['constructor']($e, $PS__Data_Tuple['Tuple']['constructor']($f, $PS__Data_Tuple['Tuple']['constructor']($g, $PS__Data_Tuple['Tuple']['constructor']($h, $PS__Data_Tuple['Tuple']['constructor']($i, $PS__Data_Tuple['Tuple']['constructor']($j, $PS__Data_Unit['unit']))))))))));
                                        };
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    $tuple1 = function ($a)  use (&$PS__Data_Tuple, &$PS__Data_Unit) {
        return $PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Unit['unit']);
    };
    $over9 = function ($o)  use (&$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Tuple, &$o) {
            return $PS__Data_Tuple['Tuple']['constructor']($v['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($o($v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value0']), $v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1'])))))))));
        };
    };
    $over8 = function ($o)  use (&$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Tuple, &$o) {
            return $PS__Data_Tuple['Tuple']['constructor']($v['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($o($v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value0']), $v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']))))))));
        };
    };
    $over7 = function ($o)  use (&$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Tuple, &$o) {
            return $PS__Data_Tuple['Tuple']['constructor']($v['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($o($v['value1']['value1']['value1']['value1']['value1']['value1']['value0']), $v['value1']['value1']['value1']['value1']['value1']['value1']['value1'])))))));
        };
    };
    $over6 = function ($o)  use (&$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Tuple, &$o) {
            return $PS__Data_Tuple['Tuple']['constructor']($v['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($o($v['value1']['value1']['value1']['value1']['value1']['value0']), $v['value1']['value1']['value1']['value1']['value1']['value1']))))));
        };
    };
    $over5 = function ($o)  use (&$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Tuple, &$o) {
            return $PS__Data_Tuple['Tuple']['constructor']($v['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($o($v['value1']['value1']['value1']['value1']['value0']), $v['value1']['value1']['value1']['value1']['value1'])))));
        };
    };
    $over4 = function ($o)  use (&$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Tuple, &$o) {
            return $PS__Data_Tuple['Tuple']['constructor']($v['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($o($v['value1']['value1']['value1']['value0']), $v['value1']['value1']['value1']['value1']))));
        };
    };
    $over3 = function ($o)  use (&$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Tuple, &$o) {
            return $PS__Data_Tuple['Tuple']['constructor']($v['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($o($v['value1']['value1']['value0']), $v['value1']['value1']['value1'])));
        };
    };
    $over2 = function ($o)  use (&$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Tuple, &$o) {
            return $PS__Data_Tuple['Tuple']['constructor']($v['value0'], $PS__Data_Tuple['Tuple']['constructor']($o($v['value1']['value0']), $v['value1']['value1']));
        };
    };
    $over10 = function ($o)  use (&$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Tuple, &$o) {
            return $PS__Data_Tuple['Tuple']['constructor']($v['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value0'], $PS__Data_Tuple['Tuple']['constructor']($o($v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value0']), $v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']))))))))));
        };
    };
    $over1 = function ($o)  use (&$PS__Data_Tuple) {
        return function ($v)  use (&$PS__Data_Tuple, &$o) {
            return $PS__Data_Tuple['Tuple']['constructor']($o($v['value0']), $v['value1']);
        };
    };
    $get9 = function ($v) {
        return $v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value0'];
    };
    $get8 = function ($v) {
        return $v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value0'];
    };
    $get7 = function ($v) {
        return $v['value1']['value1']['value1']['value1']['value1']['value1']['value0'];
    };
    $get6 = function ($v) {
        return $v['value1']['value1']['value1']['value1']['value1']['value0'];
    };
    $get5 = function ($v) {
        return $v['value1']['value1']['value1']['value1']['value0'];
    };
    $get4 = function ($v) {
        return $v['value1']['value1']['value1']['value0'];
    };
    $get3 = function ($v) {
        return $v['value1']['value1']['value0'];
    };
    $get2 = function ($v) {
        return $v['value1']['value0'];
    };
    $get10 = function ($v) {
        return $v['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value1']['value0'];
    };
    $get1 = function ($v) {
        return $v['value0'];
    };
    $curry9 = function ($z)  use (&$PS__Data_Tuple) {
        return function ($f__prime)  use (&$PS__Data_Tuple, &$z) {
            return function ($a)  use (&$f__prime, &$PS__Data_Tuple, &$z) {
                return function ($b)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$z) {
                    return function ($c)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$z) {
                        return function ($d)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$z) {
                            return function ($e)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$z) {
                                return function ($f)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$z) {
                                    return function ($g)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$z) {
                                        return function ($h)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$z) {
                                            return function ($i)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h, &$z) {
                                                return $f__prime($PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $PS__Data_Tuple['Tuple']['constructor']($d, $PS__Data_Tuple['Tuple']['constructor']($e, $PS__Data_Tuple['Tuple']['constructor']($f, $PS__Data_Tuple['Tuple']['constructor']($g, $PS__Data_Tuple['Tuple']['constructor']($h, $PS__Data_Tuple['Tuple']['constructor']($i, $z))))))))));
                                            };
                                        };
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    $curry8 = function ($z)  use (&$PS__Data_Tuple) {
        return function ($f__prime)  use (&$PS__Data_Tuple, &$z) {
            return function ($a)  use (&$f__prime, &$PS__Data_Tuple, &$z) {
                return function ($b)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$z) {
                    return function ($c)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$z) {
                        return function ($d)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$z) {
                            return function ($e)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$z) {
                                return function ($f)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$z) {
                                    return function ($g)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$z) {
                                        return function ($h)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$z) {
                                            return $f__prime($PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $PS__Data_Tuple['Tuple']['constructor']($d, $PS__Data_Tuple['Tuple']['constructor']($e, $PS__Data_Tuple['Tuple']['constructor']($f, $PS__Data_Tuple['Tuple']['constructor']($g, $PS__Data_Tuple['Tuple']['constructor']($h, $z)))))))));
                                        };
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    $curry7 = function ($z)  use (&$PS__Data_Tuple) {
        return function ($f__prime)  use (&$PS__Data_Tuple, &$z) {
            return function ($a)  use (&$f__prime, &$PS__Data_Tuple, &$z) {
                return function ($b)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$z) {
                    return function ($c)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$z) {
                        return function ($d)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$z) {
                            return function ($e)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$z) {
                                return function ($f)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$z) {
                                    return function ($g)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$z) {
                                        return $f__prime($PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $PS__Data_Tuple['Tuple']['constructor']($d, $PS__Data_Tuple['Tuple']['constructor']($e, $PS__Data_Tuple['Tuple']['constructor']($f, $PS__Data_Tuple['Tuple']['constructor']($g, $z))))))));
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    $curry6 = function ($z)  use (&$PS__Data_Tuple) {
        return function ($f__prime)  use (&$PS__Data_Tuple, &$z) {
            return function ($a)  use (&$f__prime, &$PS__Data_Tuple, &$z) {
                return function ($b)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$z) {
                    return function ($c)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$z) {
                        return function ($d)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$z) {
                            return function ($e)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$z) {
                                return function ($f)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$z) {
                                    return $f__prime($PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $PS__Data_Tuple['Tuple']['constructor']($d, $PS__Data_Tuple['Tuple']['constructor']($e, $PS__Data_Tuple['Tuple']['constructor']($f, $z)))))));
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    $curry5 = function ($z)  use (&$PS__Data_Tuple) {
        return function ($f)  use (&$PS__Data_Tuple, &$z) {
            return function ($a)  use (&$f, &$PS__Data_Tuple, &$z) {
                return function ($b)  use (&$f, &$PS__Data_Tuple, &$a, &$z) {
                    return function ($c)  use (&$f, &$PS__Data_Tuple, &$a, &$b, &$z) {
                        return function ($d)  use (&$f, &$PS__Data_Tuple, &$a, &$b, &$c, &$z) {
                            return function ($e)  use (&$f, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$z) {
                                return $f($PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $PS__Data_Tuple['Tuple']['constructor']($d, $PS__Data_Tuple['Tuple']['constructor']($e, $z))))));
                            };
                        };
                    };
                };
            };
        };
    };
    $curry4 = function ($z)  use (&$PS__Data_Tuple) {
        return function ($f)  use (&$PS__Data_Tuple, &$z) {
            return function ($a)  use (&$f, &$PS__Data_Tuple, &$z) {
                return function ($b)  use (&$f, &$PS__Data_Tuple, &$a, &$z) {
                    return function ($c)  use (&$f, &$PS__Data_Tuple, &$a, &$b, &$z) {
                        return function ($d)  use (&$f, &$PS__Data_Tuple, &$a, &$b, &$c, &$z) {
                            return $f($PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $PS__Data_Tuple['Tuple']['constructor']($d, $z)))));
                        };
                    };
                };
            };
        };
    };
    $curry3 = function ($z)  use (&$PS__Data_Tuple) {
        return function ($f)  use (&$PS__Data_Tuple, &$z) {
            return function ($a)  use (&$f, &$PS__Data_Tuple, &$z) {
                return function ($b)  use (&$f, &$PS__Data_Tuple, &$a, &$z) {
                    return function ($c)  use (&$f, &$PS__Data_Tuple, &$a, &$b, &$z) {
                        return $f($PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $z))));
                    };
                };
            };
        };
    };
    $curry2 = function ($z)  use (&$PS__Data_Tuple) {
        return function ($f)  use (&$PS__Data_Tuple, &$z) {
            return function ($a)  use (&$f, &$PS__Data_Tuple, &$z) {
                return function ($b)  use (&$f, &$PS__Data_Tuple, &$a, &$z) {
                    return $f($PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $z)));
                };
            };
        };
    };
    $curry10 = function ($z)  use (&$PS__Data_Tuple) {
        return function ($f__prime)  use (&$PS__Data_Tuple, &$z) {
            return function ($a)  use (&$f__prime, &$PS__Data_Tuple, &$z) {
                return function ($b)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$z) {
                    return function ($c)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$z) {
                        return function ($d)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$z) {
                            return function ($e)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$z) {
                                return function ($f)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$z) {
                                    return function ($g)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$z) {
                                        return function ($h)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$z) {
                                            return function ($i)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h, &$z) {
                                                return function ($j)  use (&$f__prime, &$PS__Data_Tuple, &$a, &$b, &$c, &$d, &$e, &$f, &$g, &$h, &$i, &$z) {
                                                    return $f__prime($PS__Data_Tuple['Tuple']['constructor']($a, $PS__Data_Tuple['Tuple']['constructor']($b, $PS__Data_Tuple['Tuple']['constructor']($c, $PS__Data_Tuple['Tuple']['constructor']($d, $PS__Data_Tuple['Tuple']['constructor']($e, $PS__Data_Tuple['Tuple']['constructor']($f, $PS__Data_Tuple['Tuple']['constructor']($g, $PS__Data_Tuple['Tuple']['constructor']($h, $PS__Data_Tuple['Tuple']['constructor']($i, $PS__Data_Tuple['Tuple']['constructor']($j, $z)))))))))));
                                                };
                                            };
                                        };
                                    };
                                };
                            };
                        };
                    };
                };
            };
        };
    };
    $curry1 = function ($z)  use (&$PS__Data_Tuple) {
        return function ($f)  use (&$PS__Data_Tuple, &$z) {
            return function ($a)  use (&$f, &$PS__Data_Tuple, &$z) {
                return $f($PS__Data_Tuple['Tuple']['constructor']($a, $z));
            };
        };
    };
    return [
        'tuple1' => $tuple1,
        'tuple2' => $tuple2,
        'tuple3' => $tuple3,
        'tuple4' => $tuple4,
        'tuple5' => $tuple5,
        'tuple6' => $tuple6,
        'tuple7' => $tuple7,
        'tuple8' => $tuple8,
        'tuple9' => $tuple9,
        'tuple10' => $tuple10,
        'get1' => $get1,
        'get2' => $get2,
        'get3' => $get3,
        'get4' => $get4,
        'get5' => $get5,
        'get6' => $get6,
        'get7' => $get7,
        'get8' => $get8,
        'get9' => $get9,
        'get10' => $get10,
        'over1' => $over1,
        'over2' => $over2,
        'over3' => $over3,
        'over4' => $over4,
        'over5' => $over5,
        'over6' => $over6,
        'over7' => $over7,
        'over8' => $over8,
        'over9' => $over9,
        'over10' => $over10,
        'uncurry1' => $uncurry1,
        'uncurry2' => $uncurry2,
        'uncurry3' => $uncurry3,
        'uncurry4' => $uncurry4,
        'uncurry5' => $uncurry5,
        'uncurry6' => $uncurry6,
        'uncurry7' => $uncurry7,
        'uncurry8' => $uncurry8,
        'uncurry9' => $uncurry9,
        'uncurry10' => $uncurry10,
        'curry1' => $curry1,
        'curry2' => $curry2,
        'curry3' => $curry3,
        'curry4' => $curry4,
        'curry5' => $curry5,
        'curry6' => $curry6,
        'curry7' => $curry7,
        'curry8' => $curry8,
        'curry9' => $curry9,
        'curry10' => $curry10
    ];
})();
