<?php
require_once(dirname(__FILE__) . '/../Control.Alt/index.php');
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Plus/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe.First/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe.Last/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Additive/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Conj/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Disj/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Dual/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Endo/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid.Multiplicative/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Foldable = (function ()  use (&$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_Semiring, &$PS__Control_Semigroupoid, &$PS__Control_Alt, &$PS__Control_Plus, &$PS__Control_Apply, &$PS__Control_Applicative, &$PS__Data_Unit, &$PS__Data_Function, &$PS__Control_Category, &$PS__Data_Semigroup, &$PS__Data_Monoid, &$PS__Data_Ordering, &$PS__Data_Ord, &$PS__Data_Newtype, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Control_Bind, &$PS__Data_Functor, &$PS__Data_Monoid_Disj, &$PS__Data_HeytingAlgebra, &$PS__Data_Monoid_Conj) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Foldable = function ($foldMap, $foldl, $foldr) {
        return [
            'foldMap' => $foldMap,
            'foldl' => $foldl,
            'foldr' => $foldr
        ];
    };
    $foldr = function ($dict) {
        return $dict['foldr'];
    };
    $indexr = function ($dictFoldable)  use (&$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_Semiring, &$PS__Control_Semigroupoid, &$foldr) {
        return function ($idx)  use (&$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_Semiring, &$PS__Control_Semigroupoid, &$foldr, &$dictFoldable) {
            $go = function ($a)  use (&$PS__Data_Eq, &$idx, &$PS__Data_Maybe, &$PS__Data_Semiring) {
                return function ($cursor)  use (&$PS__Data_Eq, &$idx, &$PS__Data_Maybe, &$a, &$PS__Data_Semiring) {
                    if ($cursor['elem']['type'] === 'Just') {
                        return $cursor;
                    };
                    $__local_var__106 = $PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($cursor['pos'])($idx);
                    if ($__local_var__106) {
                        return [
                            'elem' => $PS__Data_Maybe['Just']['constructor']($a),
                            'pos' => $cursor['pos']
                        ];
                    };
                    return [
                        'pos' => $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($cursor['pos'])(1),
                        'elem' => $cursor['elem']
                    ];
                };
            };
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])(function ($v) {
                return $v['elem'];
            })($foldr($dictFoldable)($go)([
                'elem' => $PS__Data_Maybe['Nothing'](),
                'pos' => 0
            ]));
        };
    };
    $__null = function ($dictFoldable)  use (&$foldr) {
        return $foldr($dictFoldable)(function ($v) {
            return function ($v1) {
                return false;
            };
        })(true);
    };
    $oneOf = function ($dictFoldable)  use (&$foldr, &$PS__Control_Alt, &$PS__Control_Plus) {
        return function ($dictPlus)  use (&$foldr, &$dictFoldable, &$PS__Control_Alt, &$PS__Control_Plus) {
            return $foldr($dictFoldable)($PS__Control_Alt['alt']($dictPlus['Alt0']()))($PS__Control_Plus['empty']($dictPlus));
        };
    };
    $oneOfMap = function ($dictFoldable)  use (&$foldr, &$PS__Control_Semigroupoid, &$PS__Control_Alt, &$PS__Control_Plus) {
        return function ($dictPlus)  use (&$foldr, &$dictFoldable, &$PS__Control_Semigroupoid, &$PS__Control_Alt, &$PS__Control_Plus) {
            return function ($f)  use (&$foldr, &$dictFoldable, &$PS__Control_Semigroupoid, &$PS__Control_Alt, &$dictPlus, &$PS__Control_Plus) {
                return $foldr($dictFoldable)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Alt['alt']($dictPlus['Alt0']()))($f))($PS__Control_Plus['empty']($dictPlus));
            };
        };
    };
    $traverse_ = function ($dictApplicative)  use (&$foldr, &$PS__Control_Semigroupoid, &$PS__Control_Apply, &$PS__Control_Applicative, &$PS__Data_Unit) {
        return function ($dictFoldable)  use (&$foldr, &$PS__Control_Semigroupoid, &$PS__Control_Apply, &$dictApplicative, &$PS__Control_Applicative, &$PS__Data_Unit) {
            return function ($f)  use (&$foldr, &$dictFoldable, &$PS__Control_Semigroupoid, &$PS__Control_Apply, &$dictApplicative, &$PS__Control_Applicative, &$PS__Data_Unit) {
                return $foldr($dictFoldable)($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Apply['applySecond']($dictApplicative['Apply0']()))($f))($PS__Control_Applicative['pure']($dictApplicative)($PS__Data_Unit['unit']));
            };
        };
    };
    $for_ = function ($dictApplicative)  use (&$PS__Data_Function, &$traverse_) {
        return function ($dictFoldable)  use (&$PS__Data_Function, &$traverse_, &$dictApplicative) {
            return $PS__Data_Function['flip']($traverse_($dictApplicative)($dictFoldable));
        };
    };
    $sequence_ = function ($dictApplicative)  use (&$traverse_, &$PS__Control_Category) {
        return function ($dictFoldable)  use (&$traverse_, &$dictApplicative, &$PS__Control_Category) {
            return $traverse_($dictApplicative)($dictFoldable)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        };
    };
    $foldl = function ($dict) {
        return $dict['foldl'];
    };
    $indexl = function ($dictFoldable)  use (&$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_Semiring, &$PS__Control_Semigroupoid, &$foldl) {
        return function ($idx)  use (&$PS__Data_Eq, &$PS__Data_Maybe, &$PS__Data_Semiring, &$PS__Control_Semigroupoid, &$foldl, &$dictFoldable) {
            $go = function ($cursor)  use (&$PS__Data_Eq, &$idx, &$PS__Data_Maybe, &$PS__Data_Semiring) {
                return function ($a)  use (&$cursor, &$PS__Data_Eq, &$idx, &$PS__Data_Maybe, &$PS__Data_Semiring) {
                    if ($cursor['elem']['type'] === 'Just') {
                        return $cursor;
                    };
                    $__local_var__109 = $PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($cursor['pos'])($idx);
                    if ($__local_var__109) {
                        return [
                            'elem' => $PS__Data_Maybe['Just']['constructor']($a),
                            'pos' => $cursor['pos']
                        ];
                    };
                    return [
                        'pos' => $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($cursor['pos'])(1),
                        'elem' => $cursor['elem']
                    ];
                };
            };
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])(function ($v) {
                return $v['elem'];
            })($foldl($dictFoldable)($go)([
                'elem' => $PS__Data_Maybe['Nothing'](),
                'pos' => 0
            ]));
        };
    };
    $intercalate = function ($dictFoldable)  use (&$PS__Data_Semigroup, &$foldl, &$PS__Data_Monoid) {
        return function ($dictMonoid)  use (&$PS__Data_Semigroup, &$foldl, &$dictFoldable, &$PS__Data_Monoid) {
            return function ($sep)  use (&$PS__Data_Semigroup, &$dictMonoid, &$foldl, &$dictFoldable, &$PS__Data_Monoid) {
                return function ($xs)  use (&$PS__Data_Semigroup, &$dictMonoid, &$sep, &$foldl, &$dictFoldable, &$PS__Data_Monoid) {
                    $go = function ($v)  use (&$PS__Data_Semigroup, &$dictMonoid, &$sep) {
                        return function ($x)  use (&$v, &$PS__Data_Semigroup, &$dictMonoid, &$sep) {
                            if ($v['init']) {
                                return [
                                    'init' => false,
                                    'acc' => $x
                                ];
                            };
                            return [
                                'init' => false,
                                'acc' => $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($v['acc'])($PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($sep)($x))
                            ];
                        };
                    };
                    return ($foldl($dictFoldable)($go)([
                        'init' => true,
                        'acc' => $PS__Data_Monoid['mempty']($dictMonoid)
                    ])($xs))['acc'];
                };
            };
        };
    };
    $length = function ($dictFoldable)  use (&$foldl, &$PS__Data_Semiring) {
        return function ($dictSemiring)  use (&$foldl, &$dictFoldable, &$PS__Data_Semiring) {
            return $foldl($dictFoldable)(function ($c)  use (&$PS__Data_Semiring, &$dictSemiring) {
                return function ($v)  use (&$PS__Data_Semiring, &$dictSemiring, &$c) {
                    return $PS__Data_Semiring['add']($dictSemiring)($PS__Data_Semiring['one']($dictSemiring))($c);
                };
            })($PS__Data_Semiring['zero']($dictSemiring));
        };
    };
    $maximumBy = function ($dictFoldable)  use (&$PS__Data_Maybe, &$PS__Data_Eq, &$PS__Data_Ordering, &$foldl) {
        return function ($cmp)  use (&$PS__Data_Maybe, &$PS__Data_Eq, &$PS__Data_Ordering, &$foldl, &$dictFoldable) {
            $max__prime = function ($v)  use (&$PS__Data_Maybe, &$PS__Data_Eq, &$PS__Data_Ordering, &$cmp) {
                return function ($v1)  use (&$v, &$PS__Data_Maybe, &$PS__Data_Eq, &$PS__Data_Ordering, &$cmp) {
                    if ($v['type'] === 'Nothing') {
                        return $PS__Data_Maybe['Just']['constructor']($v1);
                    };
                    if ($v['type'] === 'Just') {
                        return $PS__Data_Maybe['Just']['constructor']((function ()  use (&$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$v, &$v1) {
                            $__local_var__116 = $PS__Data_Eq['eq']($PS__Data_Ordering['eqOrdering'])($cmp($v['value0'])($v1))($PS__Data_Ordering['GT']());
                            if ($__local_var__116) {
                                return $v['value0'];
                            };
                            return $v1;
                        })());
                    };
                    throw new \Exception('Failed pattern match at Data.Foldable line 376, column 3 - line 376, column 27: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
                };
            };
            return $foldl($dictFoldable)($max__prime)($PS__Data_Maybe['Nothing']());
        };
    };
    $maximum = function ($dictOrd)  use (&$maximumBy, &$PS__Data_Ord) {
        return function ($dictFoldable)  use (&$maximumBy, &$PS__Data_Ord, &$dictOrd) {
            return $maximumBy($dictFoldable)($PS__Data_Ord['compare']($dictOrd));
        };
    };
    $minimumBy = function ($dictFoldable)  use (&$PS__Data_Maybe, &$PS__Data_Eq, &$PS__Data_Ordering, &$foldl) {
        return function ($cmp)  use (&$PS__Data_Maybe, &$PS__Data_Eq, &$PS__Data_Ordering, &$foldl, &$dictFoldable) {
            $min__prime = function ($v)  use (&$PS__Data_Maybe, &$PS__Data_Eq, &$PS__Data_Ordering, &$cmp) {
                return function ($v1)  use (&$v, &$PS__Data_Maybe, &$PS__Data_Eq, &$PS__Data_Ordering, &$cmp) {
                    if ($v['type'] === 'Nothing') {
                        return $PS__Data_Maybe['Just']['constructor']($v1);
                    };
                    if ($v['type'] === 'Just') {
                        return $PS__Data_Maybe['Just']['constructor']((function ()  use (&$PS__Data_Eq, &$PS__Data_Ordering, &$cmp, &$v, &$v1) {
                            $__local_var__120 = $PS__Data_Eq['eq']($PS__Data_Ordering['eqOrdering'])($cmp($v['value0'])($v1))($PS__Data_Ordering['LT']());
                            if ($__local_var__120) {
                                return $v['value0'];
                            };
                            return $v1;
                        })());
                    };
                    throw new \Exception('Failed pattern match at Data.Foldable line 389, column 3 - line 389, column 27: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
                };
            };
            return $foldl($dictFoldable)($min__prime)($PS__Data_Maybe['Nothing']());
        };
    };
    $minimum = function ($dictOrd)  use (&$minimumBy, &$PS__Data_Ord) {
        return function ($dictFoldable)  use (&$minimumBy, &$PS__Data_Ord, &$dictOrd) {
            return $minimumBy($dictFoldable)($PS__Data_Ord['compare']($dictOrd));
        };
    };
    $product = function ($dictFoldable)  use (&$foldl, &$PS__Data_Semiring) {
        return function ($dictSemiring)  use (&$foldl, &$dictFoldable, &$PS__Data_Semiring) {
            return $foldl($dictFoldable)($PS__Data_Semiring['mul']($dictSemiring))($PS__Data_Semiring['one']($dictSemiring));
        };
    };
    $sum = function ($dictFoldable)  use (&$foldl, &$PS__Data_Semiring) {
        return function ($dictSemiring)  use (&$foldl, &$dictFoldable, &$PS__Data_Semiring) {
            return $foldl($dictFoldable)($PS__Data_Semiring['add']($dictSemiring))($PS__Data_Semiring['zero']($dictSemiring));
        };
    };
    $foldableMultiplicative = $Foldable(function ($dictMonoid) {
        return function ($f) {
            return function ($v)  use (&$f) {
                return $f($v);
            };
        };
    }, function ($f) {
        return function ($z)  use (&$f) {
            return function ($v)  use (&$f, &$z) {
                return $f($z)($v);
            };
        };
    }, function ($f) {
        return function ($z)  use (&$f) {
            return function ($v)  use (&$f, &$z) {
                return $f($v)($z);
            };
        };
    });
    $foldableMaybe = $Foldable(function ($dictMonoid)  use (&$PS__Data_Monoid) {
        return function ($f)  use (&$PS__Data_Monoid, &$dictMonoid) {
            return function ($v)  use (&$PS__Data_Monoid, &$dictMonoid, &$f) {
                if ($v['type'] === 'Nothing') {
                    return $PS__Data_Monoid['mempty']($dictMonoid);
                };
                if ($v['type'] === 'Just') {
                    return $f($v['value0']);
                };
                throw new \Exception('Failed pattern match at Data.Foldable line 129, column 1 - line 129, column 41: ' . var_dump([ $f['constructor']['name'], $v['constructor']['name'] ]));
            };
        };
    }, function ($v) {
        return function ($z)  use (&$v) {
            return function ($v1)  use (&$z, &$v) {
                if ($v1['type'] === 'Nothing') {
                    return $z;
                };
                if ($v1['type'] === 'Just') {
                    return $v($z)($v1['value0']);
                };
                throw new \Exception('Failed pattern match at Data.Foldable line 129, column 1 - line 129, column 41: ' . var_dump([ $v['constructor']['name'], $z['constructor']['name'], $v1['constructor']['name'] ]));
            };
        };
    }, function ($v) {
        return function ($z)  use (&$v) {
            return function ($v1)  use (&$z, &$v) {
                if ($v1['type'] === 'Nothing') {
                    return $z;
                };
                if ($v1['type'] === 'Just') {
                    return $v($v1['value0'])($z);
                };
                throw new \Exception('Failed pattern match at Data.Foldable line 129, column 1 - line 129, column 41: ' . var_dump([ $v['constructor']['name'], $z['constructor']['name'], $v1['constructor']['name'] ]));
            };
        };
    });
    $foldableDual = $Foldable(function ($dictMonoid) {
        return function ($f) {
            return function ($v)  use (&$f) {
                return $f($v);
            };
        };
    }, function ($f) {
        return function ($z)  use (&$f) {
            return function ($v)  use (&$f, &$z) {
                return $f($z)($v);
            };
        };
    }, function ($f) {
        return function ($z)  use (&$f) {
            return function ($v)  use (&$f, &$z) {
                return $f($v)($z);
            };
        };
    });
    $foldableDisj = $Foldable(function ($dictMonoid) {
        return function ($f) {
            return function ($v)  use (&$f) {
                return $f($v);
            };
        };
    }, function ($f) {
        return function ($z)  use (&$f) {
            return function ($v)  use (&$f, &$z) {
                return $f($z)($v);
            };
        };
    }, function ($f) {
        return function ($z)  use (&$f) {
            return function ($v)  use (&$f, &$z) {
                return $f($v)($z);
            };
        };
    });
    $foldableConj = $Foldable(function ($dictMonoid) {
        return function ($f) {
            return function ($v)  use (&$f) {
                return $f($v);
            };
        };
    }, function ($f) {
        return function ($z)  use (&$f) {
            return function ($v)  use (&$f, &$z) {
                return $f($z)($v);
            };
        };
    }, function ($f) {
        return function ($z)  use (&$f) {
            return function ($v)  use (&$f, &$z) {
                return $f($v)($z);
            };
        };
    });
    $foldableAdditive = $Foldable(function ($dictMonoid) {
        return function ($f) {
            return function ($v)  use (&$f) {
                return $f($v);
            };
        };
    }, function ($f) {
        return function ($z)  use (&$f) {
            return function ($v)  use (&$f, &$z) {
                return $f($z)($v);
            };
        };
    }, function ($f) {
        return function ($z)  use (&$f) {
            return function ($v)  use (&$f, &$z) {
                return $f($v)($z);
            };
        };
    });
    $foldMapDefaultR = function ($dictFoldable)  use (&$foldr, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
        return function ($dictMonoid)  use (&$foldr, &$dictFoldable, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
            return function ($f)  use (&$foldr, &$dictFoldable, &$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Monoid) {
                return $foldr($dictFoldable)(function ($x)  use (&$PS__Data_Semigroup, &$dictMonoid, &$f) {
                    return function ($acc)  use (&$PS__Data_Semigroup, &$dictMonoid, &$f, &$x) {
                        return $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($f($x))($acc);
                    };
                })($PS__Data_Monoid['mempty']($dictMonoid));
            };
        };
    };
    $foldableArray = $Foldable(function ($dictMonoid)  use (&$foldMapDefaultR, &$foldableArray) {
        return $foldMapDefaultR($foldableArray)($dictMonoid);
    }, $__foreign['foldlArray'], $__foreign['foldrArray']);
    $foldMapDefaultL = function ($dictFoldable)  use (&$foldl, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
        return function ($dictMonoid)  use (&$foldl, &$dictFoldable, &$PS__Data_Semigroup, &$PS__Data_Monoid) {
            return function ($f)  use (&$foldl, &$dictFoldable, &$PS__Data_Semigroup, &$dictMonoid, &$PS__Data_Monoid) {
                return $foldl($dictFoldable)(function ($acc)  use (&$PS__Data_Semigroup, &$dictMonoid, &$f) {
                    return function ($x)  use (&$PS__Data_Semigroup, &$dictMonoid, &$acc, &$f) {
                        return $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($acc)($f($x));
                    };
                })($PS__Data_Monoid['mempty']($dictMonoid));
            };
        };
    };
    $foldMap = function ($dict) {
        return $dict['foldMap'];
    };
    $foldableFirst = $Foldable(function ($dictMonoid)  use (&$foldMap, &$foldableMaybe) {
        return function ($f)  use (&$foldMap, &$foldableMaybe, &$dictMonoid) {
            return function ($v)  use (&$foldMap, &$foldableMaybe, &$dictMonoid, &$f) {
                return $foldMap($foldableMaybe)($dictMonoid)($f)($v);
            };
        };
    }, function ($f)  use (&$foldl, &$foldableMaybe) {
        return function ($z)  use (&$foldl, &$foldableMaybe, &$f) {
            return function ($v)  use (&$foldl, &$foldableMaybe, &$f, &$z) {
                return $foldl($foldableMaybe)($f)($z)($v);
            };
        };
    }, function ($f)  use (&$foldr, &$foldableMaybe) {
        return function ($z)  use (&$foldr, &$foldableMaybe, &$f) {
            return function ($v)  use (&$foldr, &$foldableMaybe, &$f, &$z) {
                return $foldr($foldableMaybe)($f)($z)($v);
            };
        };
    });
    $foldableLast = $Foldable(function ($dictMonoid)  use (&$foldMap, &$foldableMaybe) {
        return function ($f)  use (&$foldMap, &$foldableMaybe, &$dictMonoid) {
            return function ($v)  use (&$foldMap, &$foldableMaybe, &$dictMonoid, &$f) {
                return $foldMap($foldableMaybe)($dictMonoid)($f)($v);
            };
        };
    }, function ($f)  use (&$foldl, &$foldableMaybe) {
        return function ($z)  use (&$foldl, &$foldableMaybe, &$f) {
            return function ($v)  use (&$foldl, &$foldableMaybe, &$f, &$z) {
                return $foldl($foldableMaybe)($f)($z)($v);
            };
        };
    }, function ($f)  use (&$foldr, &$foldableMaybe) {
        return function ($z)  use (&$foldr, &$foldableMaybe, &$f) {
            return function ($v)  use (&$foldr, &$foldableMaybe, &$f, &$z) {
                return $foldr($foldableMaybe)($f)($z)($v);
            };
        };
    });
    $foldlDefault = function ($dictFoldable)  use (&$PS__Data_Newtype, &$foldMap, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$PS__Data_Function) {
        return function ($c)  use (&$PS__Data_Newtype, &$foldMap, &$dictFoldable, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$PS__Data_Function) {
            return function ($u)  use (&$PS__Data_Newtype, &$foldMap, &$dictFoldable, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$PS__Data_Function, &$c) {
                return function ($xs)  use (&$PS__Data_Newtype, &$foldMap, &$dictFoldable, &$PS__Data_Monoid_Dual, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$PS__Data_Function, &$c, &$u) {
                    return $PS__Data_Newtype['unwrap']($PS__Data_Newtype['newtypeEndo'])($PS__Data_Newtype['unwrap']($PS__Data_Newtype['newtypeDual'])($foldMap($dictFoldable)($PS__Data_Monoid_Dual['monoidDual']($PS__Data_Monoid_Endo['monoidEndo']($PS__Control_Category['categoryFn'])))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Dual['Dual'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Endo['Endo'])($PS__Data_Function['flip']($c))))($xs)))($u);
                };
            };
        };
    };
    $foldrDefault = function ($dictFoldable)  use (&$PS__Data_Newtype, &$foldMap, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid) {
        return function ($c)  use (&$PS__Data_Newtype, &$foldMap, &$dictFoldable, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid) {
            return function ($u)  use (&$PS__Data_Newtype, &$foldMap, &$dictFoldable, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$c) {
                return function ($xs)  use (&$PS__Data_Newtype, &$foldMap, &$dictFoldable, &$PS__Data_Monoid_Endo, &$PS__Control_Category, &$PS__Control_Semigroupoid, &$c, &$u) {
                    return $PS__Data_Newtype['unwrap']($PS__Data_Newtype['newtypeEndo'])($foldMap($dictFoldable)($PS__Data_Monoid_Endo['monoidEndo']($PS__Control_Category['categoryFn']))($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Monoid_Endo['Endo'])($c))($xs))($u);
                };
            };
        };
    };
    $surroundMap = function ($dictFoldable)  use (&$PS__Data_Semigroup, &$PS__Data_Newtype, &$foldMap, &$PS__Data_Monoid_Endo, &$PS__Control_Category) {
        return function ($dictSemigroup)  use (&$PS__Data_Semigroup, &$PS__Data_Newtype, &$foldMap, &$dictFoldable, &$PS__Data_Monoid_Endo, &$PS__Control_Category) {
            return function ($d)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$PS__Data_Newtype, &$foldMap, &$dictFoldable, &$PS__Data_Monoid_Endo, &$PS__Control_Category) {
                return function ($t)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$d, &$PS__Data_Newtype, &$foldMap, &$dictFoldable, &$PS__Data_Monoid_Endo, &$PS__Control_Category) {
                    return function ($f)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$d, &$t, &$PS__Data_Newtype, &$foldMap, &$dictFoldable, &$PS__Data_Monoid_Endo, &$PS__Control_Category) {
                        $joined = function ($a)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$d, &$t) {
                            return function ($m)  use (&$PS__Data_Semigroup, &$dictSemigroup, &$d, &$t, &$a) {
                                return $PS__Data_Semigroup['append']($dictSemigroup)($d)($PS__Data_Semigroup['append']($dictSemigroup)($t($a))($m));
                            };
                        };
                        return $PS__Data_Newtype['unwrap']($PS__Data_Newtype['newtypeEndo'])($foldMap($dictFoldable)($PS__Data_Monoid_Endo['monoidEndo']($PS__Control_Category['categoryFn']))($joined)($f))($d);
                    };
                };
            };
        };
    };
    $surround = function ($dictFoldable)  use (&$surroundMap, &$PS__Control_Category) {
        return function ($dictSemigroup)  use (&$surroundMap, &$dictFoldable, &$PS__Control_Category) {
            return function ($d)  use (&$surroundMap, &$dictFoldable, &$dictSemigroup, &$PS__Control_Category) {
                return $surroundMap($dictFoldable)($dictSemigroup)($d)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
            };
        };
    };
    $foldM = function ($dictFoldable)  use (&$foldl, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative) {
        return function ($dictMonad)  use (&$foldl, &$dictFoldable, &$PS__Control_Bind, &$PS__Data_Function, &$PS__Control_Applicative) {
            return function ($f)  use (&$foldl, &$dictFoldable, &$PS__Control_Bind, &$dictMonad, &$PS__Data_Function, &$PS__Control_Applicative) {
                return function ($a0)  use (&$foldl, &$dictFoldable, &$PS__Control_Bind, &$dictMonad, &$PS__Data_Function, &$f, &$PS__Control_Applicative) {
                    return $foldl($dictFoldable)(function ($ma)  use (&$PS__Control_Bind, &$dictMonad, &$PS__Data_Function, &$f) {
                        return function ($b)  use (&$PS__Control_Bind, &$dictMonad, &$ma, &$PS__Data_Function, &$f) {
                            return $PS__Control_Bind['bind']($dictMonad['Bind1']())($ma)($PS__Data_Function['flip']($f)($b));
                        };
                    })($PS__Control_Applicative['pure']($dictMonad['Applicative0']())($a0));
                };
            };
        };
    };
    $fold = function ($dictFoldable)  use (&$foldMap, &$PS__Control_Category) {
        return function ($dictMonoid)  use (&$foldMap, &$dictFoldable, &$PS__Control_Category) {
            return $foldMap($dictFoldable)($dictMonoid)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        };
    };
    $findMap = function ($dictFoldable)  use (&$foldl, &$PS__Data_Maybe) {
        return function ($p)  use (&$foldl, &$dictFoldable, &$PS__Data_Maybe) {
            $go = function ($v)  use (&$p) {
                return function ($v1)  use (&$v, &$p) {
                    if ($v['type'] === 'Nothing') {
                        return $p($v1);
                    };
                    return $v;
                };
            };
            return $foldl($dictFoldable)($go)($PS__Data_Maybe['Nothing']());
        };
    };
    $find = function ($dictFoldable)  use (&$PS__Data_Maybe, &$foldl) {
        return function ($p)  use (&$PS__Data_Maybe, &$foldl, &$dictFoldable) {
            $go = function ($v)  use (&$p, &$PS__Data_Maybe) {
                return function ($v1)  use (&$v, &$p, &$PS__Data_Maybe) {
                    if ($v['type'] === 'Nothing' && $p($v1)) {
                        return $PS__Data_Maybe['Just']['constructor']($v1);
                    };
                    return $v;
                };
            };
            return $foldl($dictFoldable)($go)($PS__Data_Maybe['Nothing']());
        };
    };
    $any = function ($dictFoldable)  use (&$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Monoid_Disj, &$foldMap) {
        return function ($dictHeytingAlgebra)  use (&$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Monoid_Disj, &$foldMap, &$dictFoldable) {
            return $PS__Data_Newtype['alaF']($PS__Data_Functor['functorFn'])($PS__Data_Functor['functorFn'])($PS__Data_Newtype['newtypeDisj'])($PS__Data_Newtype['newtypeDisj'])($PS__Data_Monoid_Disj['Disj'])($foldMap($dictFoldable)($PS__Data_Monoid_Disj['monoidDisj']($dictHeytingAlgebra)));
        };
    };
    $elem = function ($dictFoldable)  use (&$PS__Control_Semigroupoid, &$any, &$PS__Data_HeytingAlgebra, &$PS__Data_Eq) {
        return function ($dictEq)  use (&$PS__Control_Semigroupoid, &$any, &$dictFoldable, &$PS__Data_HeytingAlgebra, &$PS__Data_Eq) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($any($dictFoldable)($PS__Data_HeytingAlgebra['heytingAlgebraBoolean']))($PS__Data_Eq['eq']($dictEq));
        };
    };
    $notElem = function ($dictFoldable)  use (&$PS__Control_Semigroupoid, &$PS__Data_HeytingAlgebra, &$elem) {
        return function ($dictEq)  use (&$PS__Control_Semigroupoid, &$PS__Data_HeytingAlgebra, &$elem, &$dictFoldable) {
            return function ($x)  use (&$PS__Control_Semigroupoid, &$PS__Data_HeytingAlgebra, &$elem, &$dictFoldable, &$dictEq) {
                return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_HeytingAlgebra['not']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean']))($elem($dictFoldable)($dictEq)($x));
            };
        };
    };
    $or = function ($dictFoldable)  use (&$any, &$PS__Control_Category) {
        return function ($dictHeytingAlgebra)  use (&$any, &$dictFoldable, &$PS__Control_Category) {
            return $any($dictFoldable)($dictHeytingAlgebra)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        };
    };
    $all = function ($dictFoldable)  use (&$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Monoid_Conj, &$foldMap) {
        return function ($dictHeytingAlgebra)  use (&$PS__Data_Newtype, &$PS__Data_Functor, &$PS__Data_Monoid_Conj, &$foldMap, &$dictFoldable) {
            return $PS__Data_Newtype['alaF']($PS__Data_Functor['functorFn'])($PS__Data_Functor['functorFn'])($PS__Data_Newtype['newtypeConj'])($PS__Data_Newtype['newtypeConj'])($PS__Data_Monoid_Conj['Conj'])($foldMap($dictFoldable)($PS__Data_Monoid_Conj['monoidConj']($dictHeytingAlgebra)));
        };
    };
    $and = function ($dictFoldable)  use (&$all, &$PS__Control_Category) {
        return function ($dictHeytingAlgebra)  use (&$all, &$dictFoldable, &$PS__Control_Category) {
            return $all($dictFoldable)($dictHeytingAlgebra)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        };
    };
    return [
        'Foldable' => $Foldable,
        'foldr' => $foldr,
        'foldl' => $foldl,
        'foldMap' => $foldMap,
        'foldrDefault' => $foldrDefault,
        'foldlDefault' => $foldlDefault,
        'foldMapDefaultL' => $foldMapDefaultL,
        'foldMapDefaultR' => $foldMapDefaultR,
        'fold' => $fold,
        'foldM' => $foldM,
        'traverse_' => $traverse_,
        'for_' => $for_,
        'sequence_' => $sequence_,
        'oneOf' => $oneOf,
        'oneOfMap' => $oneOfMap,
        'intercalate' => $intercalate,
        'surroundMap' => $surroundMap,
        'surround' => $surround,
        'and' => $and,
        'or' => $or,
        'all' => $all,
        'any' => $any,
        'sum' => $sum,
        'product' => $product,
        'elem' => $elem,
        'notElem' => $notElem,
        'indexl' => $indexl,
        'indexr' => $indexr,
        'find' => $find,
        'findMap' => $findMap,
        'maximum' => $maximum,
        'maximumBy' => $maximumBy,
        'minimum' => $minimum,
        'minimumBy' => $minimumBy,
        'null' => $__null,
        'length' => $length,
        'foldableArray' => $foldableArray,
        'foldableMaybe' => $foldableMaybe,
        'foldableFirst' => $foldableFirst,
        'foldableLast' => $foldableLast,
        'foldableAdditive' => $foldableAdditive,
        'foldableDual' => $foldableDual,
        'foldableDisj' => $foldableDisj,
        'foldableConj' => $foldableConj,
        'foldableMultiplicative' => $foldableMultiplicative
    ];
})();
