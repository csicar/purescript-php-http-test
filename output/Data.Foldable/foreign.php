<?php

$exports["foldrArray"] = function ($f) {
  return function ($init) use (&$f) {
    return function ($xs) use (&$f, $init) {
      $acc = $init;
      $len = count($xs);
      for ($i = $len - 1; $i >= 0; $i--) {
        $acc = $f($xs[$i])($acc);
      }
      return $acc;
    };
  };
};

$exports["foldlArray"] = function ($f) {
  return function ($init) use (&$f) {
    return function ($xs) use (&$f, $init) {
      $acc = $init;
      $len = count($xs);
      for ($i = 0; $i < $len; $i++) {
        $acc = $f($acc)($xs[$i]);
      }
      return $acc;
    };
  };
};
