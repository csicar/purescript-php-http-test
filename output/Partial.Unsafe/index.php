<?php
require_once(dirname(__FILE__) . '/../Partial/index.php');
$PS__Partial_Unsafe = (function ()  use (&$PS__Partial) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $unsafePartialBecause = function ($v)  use (&$__foreign) {
        return function ($x)  use (&$__foreign) {
            return $__foreign['unsafePartial'](function ($dictPartial)  use (&$x) {
                return $x($dictPartial);
            });
        };
    };
    $unsafeCrashWith = function ($msg)  use (&$__foreign, &$PS__Partial) {
        return $__foreign['unsafePartial'](function ($dictPartial)  use (&$PS__Partial, &$msg) {
            return $PS__Partial['crashWith']($dictPartial)($msg);
        });
    };
    return [
        'unsafePartialBecause' => $unsafePartialBecause,
        'unsafeCrashWith' => $unsafeCrashWith,
        'unsafePartial' => $__foreign['unsafePartial']
    ];
})();
