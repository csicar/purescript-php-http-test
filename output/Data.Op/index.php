<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor.Contravariant/index.php');
require_once(dirname(__FILE__) . '/../Data.Monoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Op = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Data_Semigroup, &$PS__Data_Newtype, &$PS__Data_Monoid, &$PS__Data_Functor_Contravariant, &$PS__Control_Category) {
    $Op = function ($x) {
        return $x;
    };
    $semigroupoidOp = $PS__Control_Semigroupoid['Semigroupoid'](function ($v)  use (&$PS__Control_Semigroupoid) {
        return function ($v1)  use (&$PS__Control_Semigroupoid, &$v) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($v1)($v);
        };
    });
    $semigroupOp = function ($dictSemigroup)  use (&$PS__Data_Semigroup) {
        return $PS__Data_Semigroup['semigroupFn']($dictSemigroup);
    };
    $newtypeOp = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Op);
    $monoidOp = function ($dictMonoid)  use (&$PS__Data_Monoid) {
        return $PS__Data_Monoid['monoidFn']($dictMonoid);
    };
    $contravariantOp = $PS__Data_Functor_Contravariant['Contravariant'](function ($f)  use (&$PS__Control_Semigroupoid) {
        return function ($v)  use (&$PS__Control_Semigroupoid, &$f) {
            return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($v)($f);
        };
    });
    $categoryOp = $PS__Control_Category['Category'](function ()  use (&$semigroupoidOp) {
        return $semigroupoidOp;
    }, $PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    return [
        'Op' => $Op,
        'newtypeOp' => $newtypeOp,
        'semigroupOp' => $semigroupOp,
        'monoidOp' => $monoidOp,
        'semigroupoidOp' => $semigroupoidOp,
        'categoryOp' => $categoryOp,
        'contravariantOp' => $contravariantOp
    ];
})();
