<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Comparison/index.php');
require_once(dirname(__FILE__) . '/../Data.Divide/index.php');
require_once(dirname(__FILE__) . '/../Data.Either/index.php');
require_once(dirname(__FILE__) . '/../Data.Equivalence/index.php');
require_once(dirname(__FILE__) . '/../Data.Op/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Predicate/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Decide = (function ()  use (&$PS__Data_Divide, &$PS__Control_Semigroupoid, &$PS__Data_Either, &$PS__Data_Ordering, &$PS__Control_Category) {
    $Decide = function ($Divide0, $choose) {
        return [
            'Divide0' => $Divide0,
            'choose' => $choose
        ];
    };
    $choosePredicate = $Decide(function ()  use (&$PS__Data_Divide) {
        return $PS__Data_Divide['dividePredicate'];
    }, function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Either) {
        return function ($v)  use (&$PS__Control_Semigroupoid, &$PS__Data_Either, &$f) {
            return function ($v1)  use (&$PS__Control_Semigroupoid, &$PS__Data_Either, &$v, &$f) {
                return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Either['either']($v)($v1))($f);
            };
        };
    });
    $chooseOp = function ($dictSemigroup)  use (&$Decide, &$PS__Data_Divide, &$PS__Control_Semigroupoid, &$PS__Data_Either) {
        return $Decide(function ()  use (&$PS__Data_Divide, &$dictSemigroup) {
            return $PS__Data_Divide['divideOp']($dictSemigroup);
        }, function ($f)  use (&$PS__Control_Semigroupoid, &$PS__Data_Either) {
            return function ($v)  use (&$PS__Control_Semigroupoid, &$PS__Data_Either, &$f) {
                return function ($v1)  use (&$PS__Control_Semigroupoid, &$PS__Data_Either, &$v, &$f) {
                    return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Either['either']($v)($v1))($f);
                };
            };
        });
    };
    $chooseEquivalence = $Decide(function ()  use (&$PS__Data_Divide) {
        return $PS__Data_Divide['divideEquivalence'];
    }, function ($f) {
        return function ($v)  use (&$f) {
            return function ($v1)  use (&$f, &$v) {
                return function ($a)  use (&$f, &$v, &$v1) {
                    return function ($b)  use (&$f, &$a, &$v, &$v1) {
                        $v2 = $f($a);
                        if ($v2['type'] === 'Left') {
                            $v3 = $f($b);
                            if ($v3['type'] === 'Left') {
                                return $v($v2['value0'])($v3['value0']);
                            };
                            if ($v3['type'] === 'Right') {
                                return false;
                            };
                            throw new \Exception('Failed pattern match at Data.Decide line 27, column 15 - line 29, column 23: ' . var_dump([ $v3['constructor']['name'] ]));
                        };
                        if ($v2['type'] === 'Right') {
                            $v3 = $f($b);
                            if ($v3['type'] === 'Left') {
                                return false;
                            };
                            if ($v3['type'] === 'Right') {
                                return $v1($v2['value0'])($v3['value0']);
                            };
                            throw new \Exception('Failed pattern match at Data.Decide line 30, column 16 - line 32, column 23: ' . var_dump([ $v3['constructor']['name'] ]));
                        };
                        throw new \Exception('Failed pattern match at Data.Decide line 26, column 66 - line 32, column 23: ' . var_dump([ $v2['constructor']['name'] ]));
                    };
                };
            };
        };
    });
    $chooseComparison = $Decide(function ()  use (&$PS__Data_Divide) {
        return $PS__Data_Divide['divideComparison'];
    }, function ($f)  use (&$PS__Data_Ordering) {
        return function ($v)  use (&$f, &$PS__Data_Ordering) {
            return function ($v1)  use (&$f, &$v, &$PS__Data_Ordering) {
                return function ($a)  use (&$f, &$v, &$PS__Data_Ordering, &$v1) {
                    return function ($b)  use (&$f, &$a, &$v, &$PS__Data_Ordering, &$v1) {
                        $v2 = $f($a);
                        if ($v2['type'] === 'Left') {
                            $v3 = $f($b);
                            if ($v3['type'] === 'Left') {
                                return $v($v2['value0'])($v3['value0']);
                            };
                            if ($v3['type'] === 'Right') {
                                return $PS__Data_Ordering['LT']();
                            };
                            throw new \Exception('Failed pattern match at Data.Decide line 18, column 15 - line 20, column 20: ' . var_dump([ $v3['constructor']['name'] ]));
                        };
                        if ($v2['type'] === 'Right') {
                            $v3 = $f($b);
                            if ($v3['type'] === 'Left') {
                                return $PS__Data_Ordering['GT']();
                            };
                            if ($v3['type'] === 'Right') {
                                return $v1($v2['value0'])($v3['value0']);
                            };
                            throw new \Exception('Failed pattern match at Data.Decide line 21, column 16 - line 23, column 23: ' . var_dump([ $v3['constructor']['name'] ]));
                        };
                        throw new \Exception('Failed pattern match at Data.Decide line 17, column 63 - line 23, column 23: ' . var_dump([ $v2['constructor']['name'] ]));
                    };
                };
            };
        };
    });
    $choose = function ($dict) {
        return $dict['choose'];
    };
    $chosen = function ($dictDecide)  use (&$choose, &$PS__Control_Category) {
        return $choose($dictDecide)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    };
    return [
        'choose' => $choose,
        'Decide' => $Decide,
        'chosen' => $chosen,
        'chooseComparison' => $chooseComparison,
        'chooseEquivalence' => $chooseEquivalence,
        'choosePredicate' => $choosePredicate,
        'chooseOp' => $chooseOp
    ];
})();
