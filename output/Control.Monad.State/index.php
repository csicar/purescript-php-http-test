<?php
require_once(dirname(__FILE__) . '/../Control.Monad.State.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.State.Trans/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Identity/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_State = (function ()  use (&$PS__Control_Monad_State_Trans, &$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$PS__Data_Identity) {
    $withState = $PS__Control_Monad_State_Trans['withStateT'];
    $runState = function ($v)  use (&$PS__Control_Semigroupoid, &$PS__Data_Newtype, &$PS__Data_Identity) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity']))($v);
    };
    $mapState = function ($f)  use (&$PS__Control_Monad_State_Trans, &$PS__Control_Semigroupoid, &$PS__Data_Identity, &$PS__Data_Newtype) {
        return $PS__Control_Monad_State_Trans['mapStateT']($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Identity['Identity'])($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($f)($PS__Data_Newtype['unwrap']($PS__Data_Identity['newtypeIdentity']))));
    };
    $execState = function ($v) {
        return function ($s)  use (&$v) {
            $v1 = $v($s);
            return $v1['value1'];
        };
    };
    $evalState = function ($v) {
        return function ($s)  use (&$v) {
            $v1 = $v($s);
            return $v1['value0'];
        };
    };
    return [
        'runState' => $runState,
        'evalState' => $evalState,
        'execState' => $execState,
        'mapState' => $mapState,
        'withState' => $withState
    ];
})();
