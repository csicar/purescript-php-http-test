<?php

// module Math

$exports['abs'] = function($a) {
  return abs($a);
};

$exports['acos'] = function($a) {
  return acos($a);
};

$exports['asin'] = function($a) {
  return asin($a);
};

$exports['atan'] = function($a) {
  return atan($a);
};

$exports['atan2'] = function ($y) {
  return function ($x) use (&$y) {
    return atan2($y, $x);
  };
};

$exports['ceil'] = function($a) {
  return ceil($a);
};

$exports['cos'] = function($a) {
  return cos($a);
};

$exports['exp'] = function($a) {
  return exp($a);
};

$exports['floor'] = function($a) {
  return floor($a);
};

$exports['trunc'] = function($a) {
    return $n < 0 ? ceil($n) : floor($n);
};

$exports['log'] = function($a) {
  return log($a);
};

$exports['max'] = function ($n1) {
  return function ($n2) use (&$n1) {
    return max($n1, $n2);
  };
};

$exports['min'] = function ($n1) {
  return function ($n2) use (&$n1) {
    return min($n1, $n2);
  };
};

$exports['pow'] = function ($n) {
  return function ($p) use (&$n) {
    return pow($n, $p);
  };
};

$exports['remainder'] = function ($n) {
  return function ($m) use (&$n) {
    return n % m;
  };
};

$exports['round'] = function($a) {
  return round($a);
};

$exports['sin'] = function($a) {
  return sin($a);
};

$exports['sqrt'] = function($a) {
  return sqrt($a);
};

$exports['tan'] = function($a) {
  return tan($a);
};

$exports['e'] = M_E;

$exports['ln2'] = M_LN2;

$exports['ln10'] = M_LN10;

$exports['log2e'] = M_LOG2E;

$exports['log10e'] = M_LOG10E;

$exports['pi'] = M_PI;

$exports['tau'] = 2 * M_PI;

$exports['sqrt1_2'] = M_SQRT1_2;

$exports['sqrt2'] = M_SQRT2;
