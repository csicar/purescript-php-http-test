<?php
$PS__Math = (function () {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    return [
        'abs' => $__foreign['abs'],
        'acos' => $__foreign['acos'],
        'asin' => $__foreign['asin'],
        'atan' => $__foreign['atan'],
        'atan2' => $__foreign['atan2'],
        'ceil' => $__foreign['ceil'],
        'cos' => $__foreign['cos'],
        'exp' => $__foreign['exp'],
        'floor' => $__foreign['floor'],
        'log' => $__foreign['log'],
        'max' => $__foreign['max'],
        'min' => $__foreign['min'],
        'pow' => $__foreign['pow'],
        'round' => $__foreign['round'],
        'sin' => $__foreign['sin'],
        'sqrt' => $__foreign['sqrt'],
        'tan' => $__foreign['tan'],
        'trunc' => $__foreign['trunc'],
        'remainder' => $__foreign['remainder'],
        'e' => $__foreign['e'],
        'ln2' => $__foreign['ln2'],
        'ln10' => $__foreign['ln10'],
        'log2e' => $__foreign['log2e'],
        'log10e' => $__foreign['log10e'],
        'pi' => $__foreign['pi'],
        'tau' => $__foreign['tau'],
        'sqrt1_2' => $__foreign['sqrt1_2'],
        'sqrt2' => $__foreign['sqrt2']
    ];
})();
