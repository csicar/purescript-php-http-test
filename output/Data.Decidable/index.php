<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
require_once(dirname(__FILE__) . '/../Data.Comparison/index.php');
require_once(dirname(__FILE__) . '/../Data.Decide/index.php');
require_once(dirname(__FILE__) . '/../Data.Divisible/index.php');
require_once(dirname(__FILE__) . '/../Data.Equivalence/index.php');
require_once(dirname(__FILE__) . '/../Data.Op/index.php');
require_once(dirname(__FILE__) . '/../Data.Predicate/index.php');
require_once(dirname(__FILE__) . '/../Data.Void/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Decidable = (function ()  use (&$PS__Control_Category, &$PS__Data_Decide, &$PS__Data_Divisible, &$PS__Data_Void) {
    $Decidable = function ($Decide0, $Divisible1, $lose) {
        return [
            'Decide0' => $Decide0,
            'Divisible1' => $Divisible1,
            'lose' => $lose
        ];
    };
    $lose = function ($dict) {
        return $dict['lose'];
    };
    $lost = function ($dictDecidable)  use (&$lose, &$PS__Control_Category) {
        return $lose($dictDecidable)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    };
    $decidablePredicate = $Decidable(function ()  use (&$PS__Data_Decide) {
        return $PS__Data_Decide['choosePredicate'];
    }, function ()  use (&$PS__Data_Divisible) {
        return $PS__Data_Divisible['divisiblePredicate'];
    }, function ($f)  use (&$PS__Data_Void) {
        return function ($a)  use (&$PS__Data_Void, &$f) {
            return $PS__Data_Void['absurd']($f($a));
        };
    });
    $decidableOp = function ($dictMonoid)  use (&$Decidable, &$PS__Data_Decide, &$PS__Data_Divisible, &$PS__Data_Void) {
        return $Decidable(function ()  use (&$PS__Data_Decide, &$dictMonoid) {
            return $PS__Data_Decide['chooseOp']($dictMonoid['Semigroup0']());
        }, function ()  use (&$PS__Data_Divisible, &$dictMonoid) {
            return $PS__Data_Divisible['divisibleOp']($dictMonoid);
        }, function ($f)  use (&$PS__Data_Void) {
            return function ($a)  use (&$PS__Data_Void, &$f) {
                return $PS__Data_Void['absurd']($f($a));
            };
        });
    };
    $decidableEquivalence = $Decidable(function ()  use (&$PS__Data_Decide) {
        return $PS__Data_Decide['chooseEquivalence'];
    }, function ()  use (&$PS__Data_Divisible) {
        return $PS__Data_Divisible['divisibleEquivalence'];
    }, function ($f)  use (&$PS__Data_Void) {
        return function ($a)  use (&$PS__Data_Void, &$f) {
            return $PS__Data_Void['absurd']($f($a));
        };
    });
    $decidableComparison = $Decidable(function ()  use (&$PS__Data_Decide) {
        return $PS__Data_Decide['chooseComparison'];
    }, function ()  use (&$PS__Data_Divisible) {
        return $PS__Data_Divisible['divisibleComparison'];
    }, function ($f)  use (&$PS__Data_Void) {
        return function ($a)  use (&$PS__Data_Void, &$f) {
            return function ($v)  use (&$PS__Data_Void, &$f, &$a) {
                return $PS__Data_Void['absurd']($f($a));
            };
        };
    });
    return [
        'lose' => $lose,
        'Decidable' => $Decidable,
        'lost' => $lost,
        'decidableComparison' => $decidableComparison,
        'decidableEquivalence' => $decidableEquivalence,
        'decidablePredicate' => $decidablePredicate,
        'decidableOp' => $decidableOp
    ];
})();
