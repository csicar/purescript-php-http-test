<?php
//TODO do actual implementation!
$exports['showImpl'] = function($buf) {
  return 'Buffer: <' . (var_export($buf)) . '>';
};

$exports['create'] = function ($size) {
  return function() use (&$size) {
    $buffer = "";
    return $buffer;
  };
};

$exports['fromArray'] = function ($octets) {
  return function() use (&$octets) {
    return pack("c*", ...$octets);
  };
};

$exports['fromStringImpl'] = function ($str) {
  return function ($encoding) use (&$str) {
    return function() use (&$str, &$encoding) {
      return $str;
      // TODO actual wit encoding!
      // return Buffer.from(str, encoding);
    };
  };
};

$exports['fromArrayBuffer'] = function($ab) {
  return function() use (&$ab) {
    return $ab;
  };
};

$exports['toArrayBuffer'] = function($buff) {
  return function() {
    return buff.buffer.slice(buff.byteOffset, buff.byteOffset + buff.byteLength);
  };
};

$exports['readImpl'] = function ($ty) {
  return function ($offset) {
    return function ($buf) {
      return function() {
        return buf['read' + ty](offset);
      };
    };
  };
};

$exports['readStringImpl'] = function ($enc) {
  return function ($start) {
    return function ($end) {
      return function ($buff) {
        return function() {
          return buff.toString(enc, start, end);
        };
      };
    };
  };
};

$exports['toStringImpl'] = function ($enc) {
  return function ($buff) {
    return function() {
      return buff.toString(enc);
    };
  };
};

$exports['writeImpl'] = function ($ty) {
  return function ($value) {
    return function ($offset) {
      return function ($buf) {
        return function () {
          buf['write' + ty](value, offset);
          return [];
        };
      };
    };
  };
};

$exports['writeStringImpl'] = function ($encoding) {
  return function ($offset) {
    return function ($length) {
      return function ($value) {
        return function ($buff) {
          return function () {
            return buff.write(value, offset, length, encoding);
          };
        };
      };
    };
  };
};

$exports['toArray'] = function ($buff) {
  return function () {
    $json = buff.toJSON();
    return json.data || json;
  };
};

$exports['getAtOffsetImpl'] = function ($just) {
  return function ($nothing) {
    return function ($offset) {
      return function ($buff) {
        return function () {
          $octet = buff[offset];
          return octet == null ? nothing
                               : just(octet);
        };
      };
    };
  };
};

$exports['setAtOffset'] = function ($value) {
  return function ($offset) use (&$value) {
    return function ($buff) use (&$value, &$offset) {
      return function () use (&$value, &$offset, &$buff) {
        $buff[$offset] = $value;
        return [];
      };
    };
  };
};

$exports['size'] = function ($buff) {
  return function () {
    return buff.length;
  };
};



$exports['concat'] = function ($buffs) {
  return function () {
    return Buffer.concat(buffs);
  };
};

$exports['concat\''] = function ($buffs) {
  return function ($totalLength) {
    return function () {
      return Buffer.concat(buffs, totalLength);
    };
  };
};

$exports['copy'] = function ($srcStart) {
  return function ($srcEnd) {
    return function ($src) {
      return function ($targStart) {
        return function ($targ) {
          return function () {
            return src.copy(targ, targStart, srcStart, srcEnd);
          };
        };
      };
    };
  };
};

$exports['fill'] = function ($octet) {
  return function ($start) {
    return function ($end) {
      return function ($buf) {
        return function () {
          buf.fill(octet, start, end);
          return [];
        };
      };
    };
  };
};
