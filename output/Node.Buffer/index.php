<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.ArrayBuffer.Types/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Effect/index.php');
require_once(dirname(__FILE__) . '/../Node.Encoding/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Node_Buffer = (function ()  use (&$PS__Control_Semigroupoid, &$PS__Node_Encoding, &$PS__Data_Show, &$PS__Data_Maybe) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $UInt8 = function () {
        return [
            'type' => 'UInt8'
        ];
    };
    $UInt16LE = function () {
        return [
            'type' => 'UInt16LE'
        ];
    };
    $UInt16BE = function () {
        return [
            'type' => 'UInt16BE'
        ];
    };
    $UInt32LE = function () {
        return [
            'type' => 'UInt32LE'
        ];
    };
    $UInt32BE = function () {
        return [
            'type' => 'UInt32BE'
        ];
    };
    $Int8 = function () {
        return [
            'type' => 'Int8'
        ];
    };
    $Int16LE = function () {
        return [
            'type' => 'Int16LE'
        ];
    };
    $Int16BE = function () {
        return [
            'type' => 'Int16BE'
        ];
    };
    $Int32LE = function () {
        return [
            'type' => 'Int32LE'
        ];
    };
    $Int32BE = function () {
        return [
            'type' => 'Int32BE'
        ];
    };
    $FloatLE = function () {
        return [
            'type' => 'FloatLE'
        ];
    };
    $FloatBE = function () {
        return [
            'type' => 'FloatBE'
        ];
    };
    $DoubleLE = function () {
        return [
            'type' => 'DoubleLE'
        ];
    };
    $DoubleBE = function () {
        return [
            'type' => 'DoubleBE'
        ];
    };
    $writeString = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($__foreign['writeStringImpl'])($PS__Node_Encoding['encodingToNode']);
    $toString = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($__foreign['toStringImpl'])($PS__Node_Encoding['encodingToNode']);
    $showBufferValueType = $PS__Data_Show['Show'](function ($v) {
        if ($v['type'] === 'UInt8') {
            return 'UInt8';
        };
        if ($v['type'] === 'UInt16LE') {
            return 'UInt16LE';
        };
        if ($v['type'] === 'UInt16BE') {
            return 'UInt16BE';
        };
        if ($v['type'] === 'UInt32LE') {
            return 'UInt32LE';
        };
        if ($v['type'] === 'UInt32BE') {
            return 'UInt32BE';
        };
        if ($v['type'] === 'Int8') {
            return 'Int8';
        };
        if ($v['type'] === 'Int16LE') {
            return 'Int16LE';
        };
        if ($v['type'] === 'Int16BE') {
            return 'Int16BE';
        };
        if ($v['type'] === 'Int32LE') {
            return 'Int32LE';
        };
        if ($v['type'] === 'Int32BE') {
            return 'Int32BE';
        };
        if ($v['type'] === 'FloatLE') {
            return 'FloatLE';
        };
        if ($v['type'] === 'FloatBE') {
            return 'FloatBE';
        };
        if ($v['type'] === 'DoubleLE') {
            return 'DoubleLE';
        };
        if ($v['type'] === 'DoubleBE') {
            return 'DoubleBE';
        };
        throw new \Exception('Failed pattern match at Node.Buffer line 65, column 1 - line 65, column 53: ' . var_dump([ $v['constructor']['name'] ]));
    });
    $write = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($__foreign['writeImpl'])($PS__Data_Show['show']($showBufferValueType));
    $showBuffer = $PS__Data_Show['Show']($__foreign['showImpl']);
    $readString = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($__foreign['readStringImpl'])($PS__Node_Encoding['encodingToNode']);
    $read = $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($__foreign['readImpl'])($PS__Data_Show['show']($showBufferValueType));
    $getAtOffset = $__foreign['getAtOffsetImpl']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $fromString = function ($str)  use (&$PS__Control_Semigroupoid, &$__foreign, &$PS__Node_Encoding) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($__foreign['fromStringImpl']($str))($PS__Node_Encoding['encodingToNode']);
    };
    return [
        'UInt8' => $UInt8,
        'UInt16LE' => $UInt16LE,
        'UInt16BE' => $UInt16BE,
        'UInt32LE' => $UInt32LE,
        'UInt32BE' => $UInt32BE,
        'Int8' => $Int8,
        'Int16LE' => $Int16LE,
        'Int16BE' => $Int16BE,
        'Int32LE' => $Int32LE,
        'Int32BE' => $Int32BE,
        'FloatLE' => $FloatLE,
        'FloatBE' => $FloatBE,
        'DoubleLE' => $DoubleLE,
        'DoubleBE' => $DoubleBE,
        'fromString' => $fromString,
        'read' => $read,
        'readString' => $readString,
        'toString' => $toString,
        'write' => $write,
        'writeString' => $writeString,
        'getAtOffset' => $getAtOffset,
        'showBuffer' => $showBuffer,
        'showBufferValueType' => $showBufferValueType,
        'create' => $__foreign['create'],
        'fromArray' => $__foreign['fromArray'],
        'fromArrayBuffer' => $__foreign['fromArrayBuffer'],
        'toArrayBuffer' => $__foreign['toArrayBuffer'],
        'toArray' => $__foreign['toArray'],
        'setAtOffset' => $__foreign['setAtOffset'],
        'size' => $__foreign['size'],
        'concat' => $__foreign['concat'],
        'concat\'' => $__foreign['concat\''],
        'copy' => $__foreign['copy'],
        'fill' => $__foreign['fill']
    ];
})();
