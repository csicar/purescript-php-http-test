<?php

$exports['fromCharArray'] = function ($a) {
  return implode("", $a);
};

$exports['toCharArray'] = function ($s) {
  return explode("", $s);
};

$exports['singleton'] = function ($c) {
  return $c;
};

$exports['_charAt'] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($i) use (&$just, &$nothing) {
      return function ($s) use (&$just, &$nothing, &$i) {
        return $i >= 0 && $i < count($s) ? $just(substr($s, $i, 1)) : $nothing;
      };
    };
  };
};

$exports['_toChar'] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($s) use (&$just, &$nothing) {
      return count($s) === 1 ? $just($s) : $nothing;
    };
  };
};

$exports['length'] = function ($s) {
  return count($s);
};

$exports['countPrefix'] = function ($p) {
  return function ($s) use (&$p) {
    $i = 0;
    while ($i < count($s) && $p(substr($s, $i, 1))) $i++;
    return $i;
  };
};

$exports['_indexOf'] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($x) use (&$just, &$nothing) {
      return function ($s) use (&$just, &$nothing, &$x) {
        $i = strpos($s, $x);
        return $i === FALSE ? $nothing : $just($i);
      };
    };
  };
};

$exports['_indexOf\''] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($x) use (&$just, &$nothing) {
      return function ($startAt) use (&$just, &$nothing, &$x) {
        return function ($s) use (&$just, &$nothing, &$x, &$startAt) {
          if ($startAt < 0 || $startAt > count($s)) return $nothing;
          $i = strpos($s, $startAt); //s.indexOf(x, startAt);
          return $i === FALSE ? $nothing : $just($i);
        };
      };
    };
  };
};

$exports['_lastIndexOf'] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($x) use (&$just, &$nothing) {
      return function ($s) use (&$just, &$nothing, &$x) {
        $i = strrpos($s, $x);
        return $i === FALSE ? $nothing : $just($i);
      };
    };
  };
};

$exports['_lastIndexOf\''] = function ($just) {
  return function ($nothing) use (&$just) {
    return function ($x) use (&$just, &$nothing) {
      return function ($startAt) use (&$just, &$nothing, &$x) {
        return function ($s) use (&$just, &$nothing, &$x, &$startAt) {
          if ($startAt < 0 || $startAt > count($s)) return $nothing;
          $i = strrpos($s, $startAt); //s.indexOf(x, startAt);
          return $i === FALSE ? $nothing : $just($i);
        };
      };
    };
  };
};

$exports['take'] = function ($n) {
  return function ($s) use (&$n) {
    return substr($s, 0, n);
  };
};

$exports['drop'] = function ($n) {
  return function ($s) use (&$n) {
    return substr($s, $n+1);
  };
};

$exports['_slice'] = function ($b) {
  return function ($e) use (&$b) {
    return function ($s) use (&$b, &$e) {
      return substr($s, $b, $e-$b);
    };
  };
};

$exports['splitAt'] = function ($i) {
  return function ($s) use (&$i) {
    return [
      "before" => substr($s, 0, $i),
      "after" => substr($s, $i)
    ];
  };
};
