<?php
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Boolean/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Semiring/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Pattern/index.php');
require_once(dirname(__FILE__) . '/../Data.String.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_String_CodeUnits = (function ()  use (&$PS__Data_Maybe, &$PS__Data_String_Unsafe, &$PS__Data_Semiring, &$PS__Data_Ring, &$PS__Data_Ord, &$PS__Data_Boolean, &$PS__Data_HeytingAlgebra, &$PS__Data_Eq, &$PS__Data_Function, &$PS__Control_Semigroupoid) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $uncons = function ($v)  use (&$PS__Data_Maybe, &$PS__Data_String_Unsafe, &$PS__Data_Semiring, &$__foreign) {
        if ($v === '') {
            return $PS__Data_Maybe['Nothing']();
        };
        return $PS__Data_Maybe['Just']['constructor']([
            'head' => $PS__Data_String_Unsafe['charAt']($PS__Data_Semiring['zero']($PS__Data_Semiring['semiringInt']))($v),
            'tail' => $__foreign['drop']($PS__Data_Semiring['one']($PS__Data_Semiring['semiringInt']))($v)
        ]);
    };
    $toChar = $__foreign['_toChar']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $takeWhile = function ($p)  use (&$__foreign) {
        return function ($s)  use (&$__foreign, &$p) {
            return $__foreign['take']($__foreign['countPrefix']($p)($s))($s);
        };
    };
    $takeRight = function ($i)  use (&$__foreign, &$PS__Data_Ring) {
        return function ($s)  use (&$__foreign, &$PS__Data_Ring, &$i) {
            return $__foreign['drop']($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($__foreign['length']($s))($i))($s);
        };
    };
    $slice = function ($b)  use (&$__foreign, &$PS__Data_Ord, &$PS__Data_Semiring, &$PS__Data_Boolean, &$PS__Data_HeytingAlgebra, &$PS__Data_Maybe) {
        return function ($e)  use (&$__foreign, &$PS__Data_Ord, &$PS__Data_Semiring, &$PS__Data_Boolean, &$b, &$PS__Data_HeytingAlgebra, &$PS__Data_Maybe) {
            return function ($s)  use (&$__foreign, &$PS__Data_Ord, &$PS__Data_Semiring, &$PS__Data_Boolean, &$e, &$b, &$PS__Data_HeytingAlgebra, &$PS__Data_Maybe) {
                $l = $__foreign['length']($s);
                $norm = function ($x)  use (&$PS__Data_Ord, &$PS__Data_Semiring, &$l, &$PS__Data_Boolean) {
                    if ($PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($x)(0)) {
                        return $PS__Data_Semiring['add']($PS__Data_Semiring['semiringInt'])($l)($x);
                    };
                    if ($PS__Data_Boolean['otherwise']) {
                        return $x;
                    };
                    throw new \Exception('Failed pattern match at Data.String.CodeUnits line 314, column 5 - line 315, column 27: ' . var_dump([ $x['constructor']['name'] ]));
                };
                $e__prime = $norm($e);
                $b__prime = $norm($b);
                $__local_var__7 = $PS__Data_HeytingAlgebra['disj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($b__prime)(0))($PS__Data_HeytingAlgebra['disj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Ord['greaterThanOrEq']($PS__Data_Ord['ordInt'])($b__prime)($l))($PS__Data_HeytingAlgebra['disj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Ord['lessThan']($PS__Data_Ord['ordInt'])($e__prime)(0))($PS__Data_HeytingAlgebra['disj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($PS__Data_Ord['greaterThanOrEq']($PS__Data_Ord['ordInt'])($e__prime)($l))($PS__Data_Ord['greaterThan']($PS__Data_Ord['ordInt'])($b__prime)($e__prime)))));
                if ($__local_var__7) {
                    return $PS__Data_Maybe['Nothing']();
                };
                return $PS__Data_Maybe['Just']['constructor']($__foreign['_slice']($b)($e)($s));
            };
        };
    };
    $lastIndexOf__prime = $__foreign['_lastIndexOf\'']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $lastIndexOf = $__foreign['_lastIndexOf']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $stripSuffix = function ($v)  use (&$lastIndexOf, &$PS__Data_Eq, &$PS__Data_Ring, &$__foreign, &$PS__Data_Function, &$PS__Data_Maybe) {
        return function ($str)  use (&$lastIndexOf, &$v, &$PS__Data_Eq, &$PS__Data_Ring, &$__foreign, &$PS__Data_Function, &$PS__Data_Maybe) {
            $v1 = $lastIndexOf($v)($str);
            if ($v1['type'] === 'Just' && $PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($v1['value0'])($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($__foreign['length']($str))($__foreign['length']($v)))) {
                return $PS__Data_Function['apply']($PS__Data_Maybe['Just']['create'])($__foreign['take']($v1['value0'])($str));
            };
            return $PS__Data_Maybe['Nothing']();
        };
    };
    $indexOf__prime = $__foreign['_indexOf\'']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $indexOf = $__foreign['_indexOf']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    $stripPrefix = function ($v)  use (&$indexOf, &$PS__Data_Function, &$PS__Data_Maybe, &$__foreign) {
        return function ($str)  use (&$indexOf, &$v, &$PS__Data_Function, &$PS__Data_Maybe, &$__foreign) {
            $v1 = $indexOf($v)($str);
            if ($v1['type'] === 'Just' && $v1['value0'] === 0) {
                return $PS__Data_Function['apply']($PS__Data_Maybe['Just']['create'])($__foreign['drop']($__foreign['length']($v))($str));
            };
            return $PS__Data_Maybe['Nothing']();
        };
    };
    $dropWhile = function ($p)  use (&$__foreign) {
        return function ($s)  use (&$__foreign, &$p) {
            return $__foreign['drop']($__foreign['countPrefix']($p)($s))($s);
        };
    };
    $dropRight = function ($i)  use (&$__foreign, &$PS__Data_Ring) {
        return function ($s)  use (&$__foreign, &$PS__Data_Ring, &$i) {
            return $__foreign['take']($PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($__foreign['length']($s))($i))($s);
        };
    };
    $contains = function ($pat)  use (&$PS__Control_Semigroupoid, &$PS__Data_Maybe, &$indexOf) {
        return $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Data_Maybe['isJust'])($indexOf($pat));
    };
    $charAt = $__foreign['_charAt']($PS__Data_Maybe['Just']['create'])($PS__Data_Maybe['Nothing']());
    return [
        'stripPrefix' => $stripPrefix,
        'stripSuffix' => $stripSuffix,
        'contains' => $contains,
        'charAt' => $charAt,
        'toChar' => $toChar,
        'uncons' => $uncons,
        'indexOf' => $indexOf,
        'indexOf\'' => $indexOf__prime,
        'lastIndexOf' => $lastIndexOf,
        'lastIndexOf\'' => $lastIndexOf__prime,
        'takeRight' => $takeRight,
        'takeWhile' => $takeWhile,
        'dropRight' => $dropRight,
        'dropWhile' => $dropWhile,
        'slice' => $slice,
        'singleton' => $__foreign['singleton'],
        'fromCharArray' => $__foreign['fromCharArray'],
        'toCharArray' => $__foreign['toCharArray'],
        'length' => $__foreign['length'],
        'countPrefix' => $__foreign['countPrefix'],
        'take' => $__foreign['take'],
        'drop' => $__foreign['drop'],
        'splitAt' => $__foreign['splitAt']
    ];
})();
