<?php
$PS__Type_Equality = (function () {
    $TypeEquals = function ($from, $to) {
        return [
            'from' => $from,
            'to' => $to
        ];
    };
    $to = function ($dict) {
        return $dict['to'];
    };
    $refl = $TypeEquals(function ($a) {
        return $a;
    }, function ($a) {
        return $a;
    });
    $from = function ($dict) {
        return $dict['from'];
    };
    return [
        'TypeEquals' => $TypeEquals,
        'to' => $to,
        'from' => $from,
        'refl' => $refl
    ];
})();
