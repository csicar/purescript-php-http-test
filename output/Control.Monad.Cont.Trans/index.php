<?php
require_once(dirname(__FILE__) . '/../Control.Applicative/index.php');
require_once(dirname(__FILE__) . '/../Control.Apply/index.php');
require_once(dirname(__FILE__) . '/../Control.Bind/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Cont.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Reader.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.State.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Monad.Trans.Class/index.php');
require_once(dirname(__FILE__) . '/../Control.Semigroupoid/index.php');
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Effect.Class/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Control_Monad_Cont_Trans = (function ()  use (&$PS__Data_Newtype, &$PS__Control_Monad_Trans_Class, &$PS__Control_Bind, &$PS__Data_Functor, &$PS__Data_Function, &$PS__Control_Apply, &$PS__Control_Applicative, &$PS__Control_Monad, &$PS__Control_Monad_Reader_Class, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Cont_Class, &$PS__Effect_Class, &$PS__Control_Monad_State_Class) {
    $ContT = function ($x) {
        return $x;
    };
    $withContT = function ($f) {
        return function ($v)  use (&$f) {
            return function ($k)  use (&$v, &$f) {
                return $v($f($k));
            };
        };
    };
    $runContT = function ($v) {
        return function ($k)  use (&$v) {
            return $v($k);
        };
    };
    $newtypeContT = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $ContT);
    $monadTransContT = $PS__Control_Monad_Trans_Class['MonadTrans'](function ($dictMonad)  use (&$PS__Control_Bind) {
        return function ($m)  use (&$PS__Control_Bind, &$dictMonad) {
            return function ($k)  use (&$PS__Control_Bind, &$dictMonad, &$m) {
                return $PS__Control_Bind['bind']($dictMonad['Bind1']())($m)($k);
            };
        };
    });
    $mapContT = function ($f) {
        return function ($v)  use (&$f) {
            return function ($k)  use (&$f, &$v) {
                return $f($v($k));
            };
        };
    };
    $functorContT = function ($dictFunctor)  use (&$PS__Data_Functor, &$PS__Data_Function) {
        return $PS__Data_Functor['Functor'](function ($f)  use (&$PS__Data_Function) {
            return function ($v)  use (&$PS__Data_Function, &$f) {
                return function ($k)  use (&$v, &$PS__Data_Function, &$f) {
                    return $v(function ($a)  use (&$PS__Data_Function, &$k, &$f) {
                        return $PS__Data_Function['apply']($k)($f($a));
                    });
                };
            };
        });
    };
    $applyContT = function ($dictApply)  use (&$PS__Control_Apply, &$functorContT) {
        return $PS__Control_Apply['Apply'](function ()  use (&$functorContT, &$dictApply) {
            return $functorContT($dictApply['Functor0']());
        }, function ($v) {
            return function ($v1)  use (&$v) {
                return function ($k)  use (&$v, &$v1) {
                    return $v(function ($g)  use (&$v1, &$k) {
                        return $v1(function ($a)  use (&$k, &$g) {
                            return $k($g($a));
                        });
                    });
                };
            };
        });
    };
    $bindContT = function ($dictBind)  use (&$PS__Control_Bind, &$applyContT) {
        return $PS__Control_Bind['Bind'](function ()  use (&$applyContT, &$dictBind) {
            return $applyContT($dictBind['Apply0']());
        }, function ($v) {
            return function ($k)  use (&$v) {
                return function ($k__prime)  use (&$v, &$k) {
                    return $v(function ($a)  use (&$k, &$k__prime) {
                        $v1 = $k($a);
                        return $v1($k__prime);
                    });
                };
            };
        });
    };
    $applicativeContT = function ($dictApplicative)  use (&$PS__Control_Applicative, &$applyContT) {
        return $PS__Control_Applicative['Applicative'](function ()  use (&$applyContT, &$dictApplicative) {
            return $applyContT($dictApplicative['Apply0']());
        }, function ($a) {
            return function ($k)  use (&$a) {
                return $k($a);
            };
        });
    };
    $monadContT = function ($dictMonad)  use (&$PS__Control_Monad, &$applicativeContT, &$bindContT) {
        return $PS__Control_Monad['Monad'](function ()  use (&$applicativeContT, &$dictMonad) {
            return $applicativeContT($dictMonad['Applicative0']());
        }, function ()  use (&$bindContT, &$dictMonad) {
            return $bindContT($dictMonad['Bind1']());
        });
    };
    $monadAskContT = function ($dictMonadAsk)  use (&$PS__Control_Monad_Reader_Class, &$monadContT, &$PS__Control_Monad_Trans_Class, &$monadTransContT) {
        return $PS__Control_Monad_Reader_Class['MonadAsk'](function ()  use (&$monadContT, &$dictMonadAsk) {
            return $monadContT($dictMonadAsk['Monad0']());
        }, $PS__Control_Monad_Trans_Class['lift']($monadTransContT)($dictMonadAsk['Monad0']())($PS__Control_Monad_Reader_Class['ask']($dictMonadAsk)));
    };
    $monadReaderContT = function ($dictMonadReader)  use (&$PS__Control_Monad_Reader_Class, &$monadAskContT, &$PS__Control_Bind, &$PS__Control_Semigroupoid, &$PS__Data_Function) {
        return $PS__Control_Monad_Reader_Class['MonadReader'](function ()  use (&$monadAskContT, &$dictMonadReader) {
            return $monadAskContT($dictMonadReader['MonadAsk0']());
        }, function ($f)  use (&$PS__Control_Bind, &$dictMonadReader, &$PS__Control_Monad_Reader_Class, &$PS__Control_Semigroupoid, &$PS__Data_Function) {
            return function ($v)  use (&$PS__Control_Bind, &$dictMonadReader, &$PS__Control_Monad_Reader_Class, &$f, &$PS__Control_Semigroupoid, &$PS__Data_Function) {
                return function ($k)  use (&$PS__Control_Bind, &$dictMonadReader, &$PS__Control_Monad_Reader_Class, &$f, &$v, &$PS__Control_Semigroupoid, &$PS__Data_Function) {
                    return $PS__Control_Bind['bind']((($dictMonadReader['MonadAsk0']())['Monad0']())['Bind1']())($PS__Control_Monad_Reader_Class['ask']($dictMonadReader['MonadAsk0']()))(function ($v1)  use (&$PS__Control_Monad_Reader_Class, &$dictMonadReader, &$f, &$v, &$PS__Control_Semigroupoid, &$PS__Data_Function, &$k) {
                        return $PS__Control_Monad_Reader_Class['local']($dictMonadReader)($f)($v($PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Reader_Class['local']($dictMonadReader)($PS__Data_Function['const']($v1)))($k)));
                    });
                };
            };
        });
    };
    $monadContContT = function ($dictMonad)  use (&$PS__Control_Monad_Cont_Class, &$monadContT) {
        return $PS__Control_Monad_Cont_Class['MonadCont'](function ()  use (&$monadContT, &$dictMonad) {
            return $monadContT($dictMonad);
        }, function ($f) {
            return function ($k)  use (&$f) {
                $v = $f(function ($a)  use (&$k) {
                    return function ($v1)  use (&$k, &$a) {
                        return $k($a);
                    };
                });
                return $v($k);
            };
        });
    };
    $monadEffectContT = function ($dictMonadEffect)  use (&$PS__Effect_Class, &$monadContT, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransContT) {
        return $PS__Effect_Class['MonadEffect'](function ()  use (&$monadContT, &$dictMonadEffect) {
            return $monadContT($dictMonadEffect['Monad0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Trans_Class['lift']($monadTransContT)($dictMonadEffect['Monad0']()))($PS__Effect_Class['liftEffect']($dictMonadEffect)));
    };
    $monadStateContT = function ($dictMonadState)  use (&$PS__Control_Monad_State_Class, &$monadContT, &$PS__Control_Semigroupoid, &$PS__Control_Monad_Trans_Class, &$monadTransContT) {
        return $PS__Control_Monad_State_Class['MonadState'](function ()  use (&$monadContT, &$dictMonadState) {
            return $monadContT($dictMonadState['Monad0']());
        }, $PS__Control_Semigroupoid['compose']($PS__Control_Semigroupoid['semigroupoidFn'])($PS__Control_Monad_Trans_Class['lift']($monadTransContT)($dictMonadState['Monad0']()))($PS__Control_Monad_State_Class['state']($dictMonadState)));
    };
    return [
        'ContT' => $ContT,
        'runContT' => $runContT,
        'mapContT' => $mapContT,
        'withContT' => $withContT,
        'newtypeContT' => $newtypeContT,
        'monadContContT' => $monadContContT,
        'functorContT' => $functorContT,
        'applyContT' => $applyContT,
        'applicativeContT' => $applicativeContT,
        'bindContT' => $bindContT,
        'monadContT' => $monadContT,
        'monadTransContT' => $monadTransContT,
        'monadEffectContT' => $monadEffectContT,
        'monadAskContT' => $monadAskContT,
        'monadReaderContT' => $monadReaderContT,
        'monadStateContT' => $monadStateContT
    ];
})();
