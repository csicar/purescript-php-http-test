<?php
require_once(dirname(__FILE__) . '/../Data.HeytingAlgebra/index.php');
require_once(dirname(__FILE__) . '/../Data.Symbol/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Data.Void/index.php');
require_once(dirname(__FILE__) . '/../Record.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Type.Data.RowList/index.php');
$PS__Data_Eq = (function ()  use (&$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$PS__Data_HeytingAlgebra) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Eq = function ($eq) {
        return [
            'eq' => $eq
        ];
    };
    $Eq1 = function ($eq1) {
        return [
            'eq1' => $eq1
        ];
    };
    $EqRecord = function ($eqRecord) {
        return [
            'eqRecord' => $eqRecord
        ];
    };
    $eqVoid = $Eq(function ($v) {
        return function ($v1) {
            return true;
        };
    });
    $eqUnit = $Eq(function ($v) {
        return function ($v1) {
            return true;
        };
    });
    $eqString = $Eq($__foreign['refEq']);
    $eqRowNil = $EqRecord(function ($v) {
        return function ($v1) {
            return function ($v2) {
                return true;
            };
        };
    });
    $eqRecord = function ($dict) {
        return $dict['eqRecord'];
    };
    $eqRec = function ($dictRowToList)  use (&$Eq, &$eqRecord, &$PS__Type_Data_RowList) {
        return function ($dictEqRecord)  use (&$Eq, &$eqRecord, &$PS__Type_Data_RowList) {
            return $Eq($eqRecord($dictEqRecord)($PS__Type_Data_RowList['RLProxy']()));
        };
    };
    $eqNumber = $Eq($__foreign['refEq']);
    $eqInt = $Eq($__foreign['refEq']);
    $eqChar = $Eq($__foreign['refEq']);
    $eqBoolean = $Eq($__foreign['refEq']);
    $eq1 = function ($dict) {
        return $dict['eq1'];
    };
    $eq = function ($dict) {
        return $dict['eq'];
    };
    $eqArray = function ($dictEq)  use (&$Eq, &$__foreign, &$eq) {
        return $Eq($__foreign['eqArrayImpl']($eq($dictEq)));
    };
    $eq1Array = $Eq1(function ($dictEq)  use (&$eq, &$eqArray) {
        return $eq($eqArray($dictEq));
    });
    $eqRowCons = function ($dictEqRecord)  use (&$EqRecord, &$eqRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$PS__Data_HeytingAlgebra, &$eq) {
        return function ($dictCons)  use (&$EqRecord, &$eqRecord, &$dictEqRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$PS__Data_HeytingAlgebra, &$eq) {
            return function ($dictIsSymbol)  use (&$EqRecord, &$eqRecord, &$dictEqRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$PS__Data_HeytingAlgebra, &$eq) {
                return function ($dictEq)  use (&$EqRecord, &$eqRecord, &$dictEqRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$PS__Data_HeytingAlgebra, &$eq) {
                    return $EqRecord(function ($v)  use (&$eqRecord, &$dictEqRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$PS__Data_HeytingAlgebra, &$eq, &$dictEq) {
                        return function ($ra)  use (&$eqRecord, &$dictEqRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$PS__Data_HeytingAlgebra, &$eq, &$dictEq) {
                            return function ($rb)  use (&$eqRecord, &$dictEqRecord, &$PS__Type_Data_RowList, &$ra, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$PS__Data_HeytingAlgebra, &$eq, &$dictEq) {
                                $tail = $eqRecord($dictEqRecord)($PS__Type_Data_RowList['RLProxy']())($ra)($rb);
                                $key = $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($PS__Data_Symbol['SProxy']());
                                $get = $PS__Record_Unsafe['unsafeGet']($key);
                                return $PS__Data_HeytingAlgebra['conj']($PS__Data_HeytingAlgebra['heytingAlgebraBoolean'])($eq($dictEq)($get($ra))($get($rb)))($tail);
                            };
                        };
                    });
                };
            };
        };
    };
    $notEq = function ($dictEq)  use (&$eq, &$eqBoolean) {
        return function ($x)  use (&$eq, &$eqBoolean, &$dictEq) {
            return function ($y)  use (&$eq, &$eqBoolean, &$dictEq, &$x) {
                return $eq($eqBoolean)($eq($dictEq)($x)($y))(false);
            };
        };
    };
    $notEq1 = function ($dictEq1)  use (&$eq, &$eqBoolean, &$eq1) {
        return function ($dictEq)  use (&$eq, &$eqBoolean, &$eq1, &$dictEq1) {
            return function ($x)  use (&$eq, &$eqBoolean, &$eq1, &$dictEq1, &$dictEq) {
                return function ($y)  use (&$eq, &$eqBoolean, &$eq1, &$dictEq1, &$dictEq, &$x) {
                    return $eq($eqBoolean)($eq1($dictEq1)($dictEq)($x)($y))(false);
                };
            };
        };
    };
    return [
        'Eq' => $Eq,
        'eq' => $eq,
        'notEq' => $notEq,
        'Eq1' => $Eq1,
        'eq1' => $eq1,
        'notEq1' => $notEq1,
        'EqRecord' => $EqRecord,
        'eqRecord' => $eqRecord,
        'eqBoolean' => $eqBoolean,
        'eqInt' => $eqInt,
        'eqNumber' => $eqNumber,
        'eqChar' => $eqChar,
        'eqString' => $eqString,
        'eqUnit' => $eqUnit,
        'eqVoid' => $eqVoid,
        'eqArray' => $eqArray,
        'eqRec' => $eqRec,
        'eq1Array' => $eq1Array,
        'eqRowNil' => $eqRowNil,
        'eqRowCons' => $eqRowCons
    ];
})();
