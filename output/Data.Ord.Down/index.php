<?php
require_once(dirname(__FILE__) . '/../Data.Bounded/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.Newtype/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Show/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Ord_Down = (function ()  use (&$PS__Data_Show, &$PS__Data_Semigroup, &$PS__Data_Newtype, &$PS__Data_Ord, &$PS__Data_Ordering, &$PS__Data_Bounded) {
    $Down = function ($x) {
        return $x;
    };
    $showDown = function ($dictShow)  use (&$PS__Data_Show, &$PS__Data_Semigroup) {
        return $PS__Data_Show['Show'](function ($v)  use (&$PS__Data_Semigroup, &$PS__Data_Show, &$dictShow) {
            return $PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])('(Down ')($PS__Data_Semigroup['append']($PS__Data_Semigroup['semigroupString'])($PS__Data_Show['show']($dictShow)($v))(')'));
        });
    };
    $newtypeDown = $PS__Data_Newtype['Newtype'](function ($n) {
        return $n;
    }, $Down);
    $eqDown = function ($dictEq) {
        return $dictEq;
    };
    $ordDown = function ($dictOrd)  use (&$PS__Data_Ord, &$eqDown, &$PS__Data_Ordering) {
        return $PS__Data_Ord['Ord'](function ()  use (&$eqDown, &$dictOrd) {
            return $eqDown($dictOrd['Eq0']());
        }, function ($v)  use (&$PS__Data_Ordering, &$PS__Data_Ord, &$dictOrd) {
            return function ($v1)  use (&$PS__Data_Ordering, &$PS__Data_Ord, &$dictOrd, &$v) {
                return $PS__Data_Ordering['invert']($PS__Data_Ord['compare']($dictOrd)($v)($v1));
            };
        });
    };
    $boundedDown = function ($dictBounded)  use (&$PS__Data_Bounded, &$ordDown) {
        return $PS__Data_Bounded['Bounded'](function ()  use (&$ordDown, &$dictBounded) {
            return $ordDown($dictBounded['Ord0']());
        }, $PS__Data_Bounded['top']($dictBounded), $PS__Data_Bounded['bottom']($dictBounded));
    };
    return [
        'Down' => $Down,
        'newtypeDown' => $newtypeDown,
        'eqDown' => $eqDown,
        'ordDown' => $ordDown,
        'boundedDown' => $boundedDown,
        'showDown' => $showDown
    ];
})();
