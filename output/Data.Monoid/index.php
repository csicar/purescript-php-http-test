<?php
require_once(dirname(__FILE__) . '/../Data.Boolean/index.php');
require_once(dirname(__FILE__) . '/../Data.Eq/index.php');
require_once(dirname(__FILE__) . '/../Data.EuclideanRing/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ordering/index.php');
require_once(dirname(__FILE__) . '/../Data.Semigroup/index.php');
require_once(dirname(__FILE__) . '/../Data.Symbol/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Record.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Type.Data.RowList/index.php');
$PS__Data_Monoid = (function ()  use (&$PS__Data_Semigroup, &$PS__Data_Unit, &$PS__Data_Ordering, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$PS__Data_Ord, &$PS__Data_Eq, &$PS__Data_EuclideanRing, &$PS__Data_Boolean) {
    $Monoid = function ($Semigroup0, $mempty) {
        return [
            'Semigroup0' => $Semigroup0,
            'mempty' => $mempty
        ];
    };
    $MonoidRecord = function ($SemigroupRecord0, $memptyRecord) {
        return [
            'SemigroupRecord0' => $SemigroupRecord0,
            'memptyRecord' => $memptyRecord
        ];
    };
    $monoidUnit = $Monoid(function ()  use (&$PS__Data_Semigroup) {
        return $PS__Data_Semigroup['semigroupUnit'];
    }, $PS__Data_Unit['unit']);
    $monoidString = $Monoid(function ()  use (&$PS__Data_Semigroup) {
        return $PS__Data_Semigroup['semigroupString'];
    }, '');
    $monoidRecordNil = $MonoidRecord(function ()  use (&$PS__Data_Semigroup) {
        return $PS__Data_Semigroup['semigroupRecordNil'];
    }, function ($v) {
        return [];
    });
    $monoidOrdering = $Monoid(function ()  use (&$PS__Data_Ordering) {
        return $PS__Data_Ordering['semigroupOrdering'];
    }, $PS__Data_Ordering['EQ']());
    $monoidArray = $Monoid(function ()  use (&$PS__Data_Semigroup) {
        return $PS__Data_Semigroup['semigroupArray'];
    }, [  ]);
    $memptyRecord = function ($dict) {
        return $dict['memptyRecord'];
    };
    $monoidRecord = function ($dictRowToList)  use (&$Monoid, &$PS__Data_Semigroup, &$memptyRecord, &$PS__Type_Data_RowList) {
        return function ($dictMonoidRecord)  use (&$Monoid, &$PS__Data_Semigroup, &$dictRowToList, &$memptyRecord, &$PS__Type_Data_RowList) {
            return $Monoid(function ()  use (&$PS__Data_Semigroup, &$dictRowToList, &$dictMonoidRecord) {
                return $PS__Data_Semigroup['semigroupRecord']($dictRowToList)($dictMonoidRecord['SemigroupRecord0']());
            }, $memptyRecord($dictMonoidRecord)($PS__Type_Data_RowList['RLProxy']()));
        };
    };
    $mempty = function ($dict) {
        return $dict['mempty'];
    };
    $monoidFn = function ($dictMonoid)  use (&$Monoid, &$PS__Data_Semigroup, &$mempty) {
        return $Monoid(function ()  use (&$PS__Data_Semigroup, &$dictMonoid) {
            return $PS__Data_Semigroup['semigroupFn']($dictMonoid['Semigroup0']());
        }, function ($v)  use (&$mempty, &$dictMonoid) {
            return $mempty($dictMonoid);
        });
    };
    $monoidRecordCons = function ($dictIsSymbol)  use (&$MonoidRecord, &$PS__Data_Semigroup, &$memptyRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$mempty) {
        return function ($dictMonoid)  use (&$MonoidRecord, &$PS__Data_Semigroup, &$dictIsSymbol, &$memptyRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$mempty) {
            return function ($dictCons)  use (&$MonoidRecord, &$PS__Data_Semigroup, &$dictIsSymbol, &$dictMonoid, &$memptyRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$mempty) {
                return function ($dictMonoidRecord)  use (&$MonoidRecord, &$PS__Data_Semigroup, &$dictIsSymbol, &$dictCons, &$dictMonoid, &$memptyRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$PS__Record_Unsafe, &$mempty) {
                    return $MonoidRecord(function ()  use (&$PS__Data_Semigroup, &$dictIsSymbol, &$dictCons, &$dictMonoidRecord, &$dictMonoid) {
                        return $PS__Data_Semigroup['semigroupRecordCons']($dictIsSymbol)($dictCons)($dictMonoidRecord['SemigroupRecord0']())($dictMonoid['Semigroup0']());
                    }, function ($v)  use (&$memptyRecord, &$dictMonoidRecord, &$PS__Type_Data_RowList, &$PS__Data_Symbol, &$dictIsSymbol, &$PS__Record_Unsafe, &$mempty, &$dictMonoid) {
                        $tail = $memptyRecord($dictMonoidRecord)($PS__Type_Data_RowList['RLProxy']());
                        $key = $PS__Data_Symbol['reflectSymbol']($dictIsSymbol)($PS__Data_Symbol['SProxy']());
                        $insert = $PS__Record_Unsafe['unsafeSet']($key);
                        return $insert($mempty($dictMonoid))($tail);
                    });
                };
            };
        };
    };
    $power = function ($dictMonoid)  use (&$PS__Data_Ord, &$mempty, &$PS__Data_Eq, &$PS__Data_EuclideanRing, &$PS__Data_Semigroup, &$PS__Data_Boolean) {
        return function ($x)  use (&$PS__Data_Ord, &$mempty, &$dictMonoid, &$PS__Data_Eq, &$PS__Data_EuclideanRing, &$PS__Data_Semigroup, &$PS__Data_Boolean) {
            $go = function ($p)  use (&$PS__Data_Ord, &$mempty, &$dictMonoid, &$PS__Data_Eq, &$x, &$PS__Data_EuclideanRing, &$go, &$PS__Data_Semigroup, &$PS__Data_Boolean) {
                if ($PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])($p)(0)) {
                    return $mempty($dictMonoid);
                };
                if ($PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($p)(1)) {
                    return $x;
                };
                if ($PS__Data_Eq['eq']($PS__Data_Eq['eqInt'])($PS__Data_EuclideanRing['mod']($PS__Data_EuclideanRing['euclideanRingInt'])($p)(2))(0)) {
                    $x__prime = $go($PS__Data_EuclideanRing['div']($PS__Data_EuclideanRing['euclideanRingInt'])($p)(2));
                    return $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($x__prime)($x__prime);
                };
                if ($PS__Data_Boolean['otherwise']) {
                    $x__prime = $go($PS__Data_EuclideanRing['div']($PS__Data_EuclideanRing['euclideanRingInt'])($p)(2));
                    return $PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($x__prime)($PS__Data_Semigroup['append']($dictMonoid['Semigroup0']())($x__prime)($x));
                };
                throw new \Exception('Failed pattern match at Data.Monoid line 66, column 3 - line 66, column 17: ' . var_dump([ $p['constructor']['name'] ]));
            };
            return $go;
        };
    };
    $guard = function ($dictMonoid)  use (&$mempty) {
        return function ($v)  use (&$mempty, &$dictMonoid) {
            return function ($v1)  use (&$v, &$mempty, &$dictMonoid) {
                if ($v) {
                    return $v1;
                };
                if (!$v) {
                    return $mempty($dictMonoid);
                };
                throw new \Exception('Failed pattern match at Data.Monoid line 74, column 1 - line 74, column 49: ' . var_dump([ $v['constructor']['name'], $v1['constructor']['name'] ]));
            };
        };
    };
    return [
        'Monoid' => $Monoid,
        'mempty' => $mempty,
        'power' => $power,
        'guard' => $guard,
        'MonoidRecord' => $MonoidRecord,
        'memptyRecord' => $memptyRecord,
        'monoidUnit' => $monoidUnit,
        'monoidOrdering' => $monoidOrdering,
        'monoidFn' => $monoidFn,
        'monoidString' => $monoidString,
        'monoidArray' => $monoidArray,
        'monoidRecord' => $monoidRecord,
        'monoidRecordNil' => $monoidRecordNil,
        'monoidRecordCons' => $monoidRecordCons
    ];
})();
