<?php
require_once(dirname(__FILE__) . '/../Control.Category/index.php');
$PS__Data_Bifunctor = (function ()  use (&$PS__Control_Category) {
    $Bifunctor = function ($bimap) {
        return [
            'bimap' => $bimap
        ];
    };
    $bimap = function ($dict) {
        return $dict['bimap'];
    };
    $lmap = function ($dictBifunctor)  use (&$bimap, &$PS__Control_Category) {
        return function ($f)  use (&$bimap, &$dictBifunctor, &$PS__Control_Category) {
            return $bimap($dictBifunctor)($f)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
        };
    };
    $rmap = function ($dictBifunctor)  use (&$bimap, &$PS__Control_Category) {
        return $bimap($dictBifunctor)($PS__Control_Category['identity']($PS__Control_Category['categoryFn']));
    };
    return [
        'bimap' => $bimap,
        'Bifunctor' => $Bifunctor,
        'lmap' => $lmap,
        'rmap' => $rmap
    ];
})();
