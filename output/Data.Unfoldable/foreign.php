<?php

$exports['unfoldrArrayImpl'] = function ($isNothing) {
  return function ($fromJust) use (&$isNothing) {
    return function ($fst) use (&$fromJust, &$isNothing) {
      return function ($snd) use (&$fst, &$fromJust, &$isNothing) {
        return function ($f) use (&$snd, &$fst, &$fromJust, &$isNothing) {
          return function ($b) use (&$f, &$snd, &$fst, &$fromJust, &$isNothing) {
            $result = [];
            $value = b;
            while (true) { // eslint-disable-line no-constant-condition
              $maybe = $f($value);
              if ($isNothing($maybe)) return $result;
              $tuple = $fromJust($maybe);
              $result[] = $fst($tuple);
              $value = $snd($tuple);
            }
          };
        };
      };
    };
  };
};
