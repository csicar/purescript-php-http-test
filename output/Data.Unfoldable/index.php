<?php
require_once(dirname(__FILE__) . '/../Data.Function/index.php');
require_once(dirname(__FILE__) . '/../Data.Functor/index.php');
require_once(dirname(__FILE__) . '/../Data.Maybe/index.php');
require_once(dirname(__FILE__) . '/../Data.Ord/index.php');
require_once(dirname(__FILE__) . '/../Data.Ring/index.php');
require_once(dirname(__FILE__) . '/../Data.Traversable/index.php');
require_once(dirname(__FILE__) . '/../Data.Tuple/index.php');
require_once(dirname(__FILE__) . '/../Data.Unfoldable1/index.php');
require_once(dirname(__FILE__) . '/../Data.Unit/index.php');
require_once(dirname(__FILE__) . '/../Partial.Unsafe/index.php');
require_once(dirname(__FILE__) . '/../Prelude/index.php');
$PS__Data_Unfoldable = (function ()  use (&$PS__Data_Unfoldable1, &$PS__Data_Maybe, &$PS__Partial_Unsafe, &$PS__Data_Tuple, &$PS__Data_Ord, &$PS__Data_Ring, &$PS__Data_Traversable, &$PS__Data_Function, &$PS__Data_Unit, &$PS__Data_Functor) {
    $exports = [];
    require(dirname(__FILE__) . '/foreign.php');
    $__foreign = $exports;
    $Unfoldable = function ($Unfoldable10, $unfoldr) {
        return [
            'Unfoldable10' => $Unfoldable10,
            'unfoldr' => $unfoldr
        ];
    };
    $unfoldr = function ($dict) {
        return $dict['unfoldr'];
    };
    $unfoldableArray = $Unfoldable(function ()  use (&$PS__Data_Unfoldable1) {
        return $PS__Data_Unfoldable1['unfoldable1Array'];
    }, $__foreign['unfoldrArrayImpl']($PS__Data_Maybe['isNothing'])($PS__Partial_Unsafe['unsafePartial'](function ($dictPartial)  use (&$PS__Data_Maybe) {
        return $PS__Data_Maybe['fromJust']($dictPartial);
    }))($PS__Data_Tuple['fst'])($PS__Data_Tuple['snd']));
    $replicate = function ($dictUnfoldable)  use (&$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Ring, &$unfoldr) {
        return function ($n)  use (&$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Ring, &$unfoldr, &$dictUnfoldable) {
            return function ($v)  use (&$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Tuple, &$PS__Data_Ring, &$unfoldr, &$dictUnfoldable, &$n) {
                $step = function ($i)  use (&$PS__Data_Ord, &$PS__Data_Maybe, &$PS__Data_Tuple, &$v, &$PS__Data_Ring) {
                    $__local_var__7 = $PS__Data_Ord['lessThanOrEq']($PS__Data_Ord['ordInt'])($i)(0);
                    if ($__local_var__7) {
                        return $PS__Data_Maybe['Nothing']();
                    };
                    return $PS__Data_Maybe['Just']['constructor']($PS__Data_Tuple['Tuple']['constructor']($v, $PS__Data_Ring['sub']($PS__Data_Ring['ringInt'])($i)(1)));
                };
                return $unfoldr($dictUnfoldable)($step)($n);
            };
        };
    };
    $replicateA = function ($dictApplicative)  use (&$PS__Data_Traversable, &$replicate) {
        return function ($dictUnfoldable)  use (&$PS__Data_Traversable, &$dictApplicative, &$replicate) {
            return function ($dictTraversable)  use (&$PS__Data_Traversable, &$dictApplicative, &$replicate, &$dictUnfoldable) {
                return function ($n)  use (&$PS__Data_Traversable, &$dictTraversable, &$dictApplicative, &$replicate, &$dictUnfoldable) {
                    return function ($m)  use (&$PS__Data_Traversable, &$dictTraversable, &$dictApplicative, &$replicate, &$dictUnfoldable, &$n) {
                        return $PS__Data_Traversable['sequence']($dictTraversable)($dictApplicative)($replicate($dictUnfoldable)($n)($m));
                    };
                };
            };
        };
    };
    $none = function ($dictUnfoldable)  use (&$unfoldr, &$PS__Data_Function, &$PS__Data_Maybe, &$PS__Data_Unit) {
        return $unfoldr($dictUnfoldable)($PS__Data_Function['const']($PS__Data_Maybe['Nothing']()))($PS__Data_Unit['unit']);
    };
    $fromMaybe = function ($dictUnfoldable)  use (&$unfoldr, &$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Function, &$PS__Data_Tuple) {
        return $unfoldr($dictUnfoldable)(function ($b)  use (&$PS__Data_Functor, &$PS__Data_Maybe, &$PS__Data_Function, &$PS__Data_Tuple) {
            return $PS__Data_Functor['map']($PS__Data_Maybe['functorMaybe'])($PS__Data_Function['flip']($PS__Data_Tuple['Tuple']['create'])($PS__Data_Maybe['Nothing']()))($b);
        });
    };
    return [
        'Unfoldable' => $Unfoldable,
        'unfoldr' => $unfoldr,
        'replicate' => $replicate,
        'replicateA' => $replicateA,
        'none' => $none,
        'fromMaybe' => $fromMaybe,
        'unfoldableArray' => $unfoldableArray
    ];
})();
