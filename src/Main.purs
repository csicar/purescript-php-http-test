module Main where

import Effect
import Prelude
import Server

import Effect.Class.Console (log)
import Foreign.Object (singleton)

handleRsp :: Request -> Response
handleRsp _ = response 200 header "<button>Hello World!</button>"
  where
    header = singleton "Content-Type" "text/html"

port = 8080

main :: Effect Unit
main = do
  loop <- createLoop
  server <- createServer handleRsp
  socket <- createSocket (show port) loop
  listen server socket
  log $ "Server running on port " <> show port
  runLoop loop
