<?php
use Psr\Http\Message\ServerRequestInterface;
use React\EventLoop\Factory;
use React\Http\Response;
use React\Http\Server;

require './vendor/autoload.php';

$exports['response'] = function($code) {
	return function($header) use (&$code) {
		return function ($body) use (&$code, &$header) {
			return new Response($code, $header, $body);
		};
	};
};

$exports['createLoop'] = function() {
	return React\EventLoop\Factory::create();
};

$exports['createServer'] = function($handler) {
	return function() use (&$handler) {
		return new Server($handler);
	};
};

$exports['createSocket'] = function($addr) {
	return function($loop) use (&$addr) {
		return function() use (&$loop, &$addr) {
			return new \React\Socket\Server($addr, $loop);
		};
	};
};

$exports['listen'] = function($server) {
	return function($socket) use (&$server) {
		return function() use (&$server, &$socket) {
			$server->listen($socket);
		};
	};
};

$exports['runLoop'] = function($loop) {
	return function() use (&$loop) {
		$loop->run();
	};
};
