module Server where

import Prelude

import Effect (Effect)
import Foreign.Object (Object)

foreign import data Loop :: Type
foreign import data Server :: Type
foreign import data Socket :: Type
foreign import data Request :: Type
foreign import data Response :: Type

foreign import response :: Int -> Object String -> String -> Response

foreign import createLoop :: Effect Loop

foreign import createServer :: (Request -> Response) -> Effect Server 

foreign import createSocket :: String -> Loop -> Effect Socket

foreign import listen :: Server -> Socket -> Effect Unit

foreign import runLoop :: Loop -> Effect Unit