<?php

use Psr\Http\Message\ServerRequestInterface;
use React\EventLoop\Factory;
use React\Http\Response;
use React\Http\Server;
require './vendor/autoload.php';

$loop = React\EventLoop\Factory::create();
require(dirname(__FILE__) . "/output/Main/index.php");
$PS__Main["main"]([]);

$loop->run();